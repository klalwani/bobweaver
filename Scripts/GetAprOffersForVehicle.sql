-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mihir Dave
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE GetAprOffersForVehicle
	@agileVehicleId  int,
	@vehicleVin varchar(200),
	@modelTranslatedId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
    [Filter1].[AgileVehicleId] AS [AgileVehicleId], 
    IsNull([Filter1].[Id], N'') AS [Id],
    CAST( [Filter1].[ProgramId1] AS nvarchar(max)) AS [ProgramId], 
    IsNull([Extent3].[TranslatedName], [Filter1].[Name]) AS [ProgramName],
    [Filter1].[Name] AS [Name], 
    [Filter1].[Amount] AS [Amount], 
    [Filter1].[Type] AS [Type], 
    [Filter1].[Disclaimer] AS [Disclaimer], 
    IsNull([Extent3].[DisplaySection], 0) AS [DisplaySection], 
    cast(0 as bit) AS [IsCashCoupon]
    FROM   (SELECT [Extent1].[AgileVehicleId] AS [AgileVehicleId], [Extent2].[ProgramId] AS [ProgramId1], [Extent2].[Name] AS [Name], [Extent2].[Type] AS [Type], [Extent2].[Id] AS [Id], [Extent2].[Disclaimer] AS [Disclaimer], [Extent2].[Amount] AS [Amount]
        FROM  [dbo].[AgileVehiclesPrograms] AS [Extent1]
        INNER JOIN [dbo].[Programs] AS [Extent2] ON [Extent1].[ProgramId] = [Extent2].[ProgramId]
        WHERE ([Extent1].[IsActive] = 1) AND ([Extent2].[Type] = N'APR')) AS [Filter1]
    LEFT OUTER JOIN [dbo].[IncentiveTranslateds] AS [Extent3] ON ([Extent3].[IsActive] = 1) 
	AND 
	(
		(
			(
				([Extent3].[Vin] = @vehicleVin) 
				OR (([Extent3].[Vin] IS NULL) AND (@vehicleVin IS NULL))
			) AND (0 = [Extent3].[ModelId])
		)
		OR (([Extent3].[Vin] = N'') AND ([Extent3].[ModelId] = @modelTranslatedId)) 
		OR (([Extent3].[Vin] = N'') AND ([Extent3].[ModelId] = 0))) 
	AND (([Filter1].[Name] = [Extent3].[ProgramName]) OR (([Filter1].[Name] IS NULL) AND ([Extent3].[ProgramName] IS NULL))) 
	AND (([Filter1].[Type] = [Extent3].[Type]) OR (([Filter1].[Type] IS NULL) AND ([Extent3].[Type] IS NULL)))
    WHERE [Filter1].[AgileVehicleId] = @agileVehicleId
END
GO
