-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mihir Dave
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create or ALTER PROCEDURE GetOffersForVehicleTest 
	-- Add the parameters for the stored procedure here
	@agileVehicleId  int,
	@vehicleVin varchar(200),
	@modelTranslatedId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
    [JoinedPrograms].[AgileVehicleId] AS [AgileVehicleId], 
    IsNull([JoinedPrograms].[Id], N'') AS [Id],
    CAST([JoinedPrograms].[ProgramId1] AS nvarchar(max)) AS [ProgramId], 
    IsNull([IncentiveTranslated].[TranslatedName], [JoinedPrograms].[Name]) AS [Name], 
    [JoinedPrograms].[Name] AS [ProgramName], 
    [JoinedPrograms].[Amount] AS [Amount], 
    [JoinedPrograms].[Type] AS [Type], 
    [JoinedPrograms].[Disclaimer] AS [Disclaimer], 
    IsNull([IncentiveTranslated].[DisplaySection], 0) AS [DisplaySection], 
    cast(0 as bit) AS [IsCashCoupon],
	[JoinedPrograms].[IsFmcc] AS [IsFmcc],
	[JoinedPrograms].[Conditional] AS [Conditional],
	CASE WHEN (([IncentiveTranslated].[Id] = [JoinedPrograms].[Id]) OR (([IncentiveTranslated].[Id] IS NULL) AND ([JoinedPrograms].[Id] IS NULL)))
				THEN 1 ELSE 0 END AS [IdBit]
    FROM   (SELECT 
				[AVP].[AgileVehicleId] AS [AgileVehicleId], 
				[Programs].[ProgramId] AS [ProgramId1], 
				[Programs].[Name] AS [Name], 
				[Programs].[Type] AS [Type], 
				[Programs].[Id] AS [Id], 
				[Programs].[Disclaimer] AS [Disclaimer], 
				[Programs].[Amount] AS [Amount], 
				[Programs].[Conditional] AS [Conditional],
				[Programs].[IsFmcc] As [IsFmcc]
			FROM  [dbo].[AgileVehiclesPrograms] AS [AVP]
			INNER JOIN [dbo].[Programs] AS [Programs] ON [AVP].[ProgramId] = [Programs].[ProgramId]
			WHERE [AVP].[IsActive] = 1 ) AS [JoinedPrograms]
			LEFT OUTER JOIN [dbo].[IncentiveTranslateds] AS [IncentiveTranslated]
			ON ([IncentiveTranslated].[IsCashCoupon] = 0) 
			AND ([IncentiveTranslated].[IsActive] = 1) 
			AND (
					((([IncentiveTranslated].[Vin] = @vehicleVin) OR (([IncentiveTranslated].[Vin] IS NULL) AND (@vehicleVin IS NULL))) AND ([IncentiveTranslated].[ModelId] = 0))
					OR (([IncentiveTranslated].[Vin] = N'') AND ([IncentiveTranslated].[ModelId] = @modelTranslatedId)) 
					OR (([IncentiveTranslated].[Vin] = N'') AND ([IncentiveTranslated].[ModelId] = 0))
					OR (CHARINDEX(@vehicleVin, [IncentiveTranslated].[IncludeMasterIncentiveVins]) > 0)
				) 
			AND (CHARINDEX(@vehicleVin, [IncentiveTranslated].[ExcludeVINs]) = 0) 
			AND (([JoinedPrograms].[Name] = [IncentiveTranslated].[ProgramName]) OR (([JoinedPrograms].[Name] IS NULL) AND ([IncentiveTranslated].[ProgramName] IS NULL))) 
			AND (([JoinedPrograms].[Type] = [IncentiveTranslated].[Type]) OR (([JoinedPrograms].[Type] IS NULL) AND ([IncentiveTranslated].[Type] IS NULL)))
    WHERE ([JoinedPrograms].[AgileVehicleId] = @agileVehicleId) 
		AND (
				[JoinedPrograms].[Type] = N'Cash'
				OR (([IncentiveTranslated].[Id] = [JoinedPrograms].[Id]) OR (([IncentiveTranslated].[Id] IS NULL) AND ([JoinedPrograms].[Id] IS NULL)))
			)
END
GO