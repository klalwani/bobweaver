-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mihir Dave
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE GetConditionalVehiclePrograms
@agileVehicleId  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
    [Project1].[Id], 
    [Project1].[ProgramId] AS [ProgramId], 
    [Project1].[Name] AS [Name], 
    [Project1].[Amount] AS [Amount], 
    [Project1].[Type] AS [Type], 
    [Project1].[Disclaimer] AS [Disclaimer]
    FROM ( SELECT 
        LEN([Extent2].[Name]) AS [C1],
        IsNull([Extent2].[Id], N'') AS [Id], 
        CAST([Extent2].[ProgramId] AS nvarchar(max)) AS [ProgramId], 
        [Extent2].[Name] AS [Name], 
        [Extent2].[Type] AS [Type], 
        [Extent2].[Disclaimer] AS [Disclaimer], 
        [Extent2].[Amount] AS [Amount]
        FROM  [dbo].[AgileVehiclesPrograms] AS [Extent1]
        INNER JOIN [dbo].[Programs] AS [Extent2] ON [Extent1].[ProgramId] = [Extent2].[ProgramId]
        WHERE ([Extent1].[IsActive] = 1) AND ([Extent1].[AgileVehicleId] = @agileVehicleId) AND ([Extent2].[Conditional] = N'true') AND ([Extent1].[IsActive] = 1)
    )  AS [Project1]
    ORDER BY [Project1].[C1] ASC
END
GO
