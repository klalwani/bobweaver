USE [AgileDealer-BobWeaver]
GO
/****** Object:  StoredProcedure [dbo].[GetPageMetaData]    Script Date: 18-03-2020 00:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mihir Dave
-- Create date: 15-03-2020
-- Description:	This Sp is used to get page MetaData.
-- =============================================
CREATE OR ALTER     PROCEDURE [dbo].[GetPageMetaData]
	@languageId int = 1, 
	@dealerId int = 1
AS
BEGIN
	SET NOCOUNT ON;

	select 
		[PageMetaDatas].[PageMetaDataId] AS [PageMetaDataId], 
		[PageMetaDatas].[PageName] AS [PageName],
		CASE WHEN ([PageMetaDatas_Trans].[Title] IS NULL) THEN [PageMetaDatas].[Title] ELSE [PageMetaDatas_Trans].[Title] END AS [Title], 
		CASE WHEN ([PageMetaDatas_Trans].[Description] IS NULL) THEN [PageMetaDatas].[Title] ELSE [PageMetaDatas_Trans].[Description] END AS [Description], 
		[PageMetaDatas].[ParentLink] AS [ParentLink], 
		[PageMetaDatas].[AmpLink] AS [AmpLink]
	FROM  [dbo].[PageMetaDatas]
	LEFT OUTER JOIN [dbo].[PageMetaDatas_Trans] ON ([PageMetaDatas_Trans].[LanguageId] = @languageId) AND ([PageMetaDatas].[PageMetaDataId] = [PageMetaDatas_Trans].[PageMetaDataId])
	WHERE ([PageMetaDatas].[DealerID] = @dealerId);

	-- return languages
	select * from [Languages];

	-- return Custom redirectUrls
	select * from [CustomRedirectUrls];

	-- return Locations
	select * from [Locations];

	-- return footer
	select top 1 * from [Footers] where DealerID = @dealerId;

	-- return menus
	select 
		[Menus].[MenuId] AS [MenuId], 
		CASE WHEN ([Menu_Trans].[Title] IS NOT NULL) THEN [Menu_Trans].[Title] ELSE  [Menus].[Title] END AS [Title], 
		[Menus].[Url] AS [Url], 
		[Menus].[DisplayOrderId] AS [DisplayOrderId], 
		[Menus].[Name] AS [Name], 
		[Menus].[ParentId] AS [ParentId], 
		[Menus].[ShowOnMenu] AS [ShowOnMenu], 
		CASE WHEN ([Menu_Trans].[LanguageId] IS NOT NULL) THEN [Menu_Trans].[LanguageId] ELSE @languageId END AS [LanguageId], 
		[Menus].[DealerID] AS [DealerID]
	FROM  [dbo].[Menus]
	LEFT OUTER JOIN [dbo].[Menu_Trans] ON [Menus].[MenuId] = [Menu_Trans].[MenuId]
	WHERE ([Menus].[IsActive] = 1) AND ([Menus].[DealerID] = @dealerId);

	-- return dealers
	select top 1 * from Dealers;

	-- return slider data for homePage.
	SELECT 
        [HomePageSlides].[SlideNumber] AS [SlideNumber], 
        [HomePageSlides].[SlideImageName] AS [SlideImageName], 
        [HomePageSlides].[SlideImagePath] AS [SlideImagePath], 
        [HomePageSlides].[SlideLink] AS [SlideLink],
		CASE WHEN ([HomePageSlides].[SlideNumber] = 0) THEN N'active' ELSE N'' END AS [Active]
        FROM [dbo].[HomePageSlides]
        WHERE ([HomePageSlides].[SlideEntryId] = (SELECT top 1 SlideEntryId FROM HomePageSliderEntries WHERE DealerID = 1 AND IsActive=1)) AND ([HomePageSlides].[DealerID] = 1)    
		ORDER BY [HomePageSlides].[SlideNumber] ASC;
END
