-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE GetTranslatedIncentive
	@vehicleVin varchar(200),
	@modelTranslatedId int,
	@chromeStyleId varchar(200),
	@chromeStyleIdYear varchar(200),
	@engineId varchar(100),
	@trimId varchar(100),
	@year varchar(10),
	@bodyStyleId varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
    [Project1].[IncentiveTranslatedId] AS [IncentiveTranslatedId], 
    [Project1].[DisplaySection] AS [DisplaySection], 
    [Project1].[Position] AS [Position], 
    [Project1].[TranslatedName] AS [TranslatedName], 
    [Project1].[ProgramName] AS [ProgramName], 
    [Project1].[Type] AS [Type], 
    [Project1].[Id] AS [Id], 
    [Project1].[Detail] AS [Detail], 
    [Project1].[Disclaimer] AS [Disclaimer], 
    [Project1].[StartDate] AS [StartDate], 
    [Project1].[EndDate] AS [EndDate], 
    [Project1].[IsActive] AS [IsActive], 
    [Project1].[Vin] AS [Vin], 
    [Project1].[ModelId] AS [ModelId], 
    [Project1].[IsCashCoupon] AS [IsCashCoupon], 
    [Project1].[IsCustomDiscount] AS [IsCustomDiscount], 
    [Project1].[Amount] AS [Amount], 
    [Project1].[ExcludeEngineCodes] AS [ExcludeEngineCodes], 
    [Project1].[ExcludeVINs] AS [ExcludeVINs], 
    [Project1].[HasSetDateRange] AS [HasSetDateRange], 
    [Project1].[excludeTrimId] AS [excludeTrimId], 
    [Project1].[ExcludeYear] AS [ExcludeYear], 
    [Project1].[HideOtherIncentives] AS [HideOtherIncentives], 
    [Project1].[DiscountType] AS [DiscountType], 
    [Project1].[HideDiscounts] AS [HideDiscounts], 
    [Project1].[excludeBodyStyleId] AS [excludeBodyStyleId], 
    [Project1].[IsMasterIncentive] AS [IsMasterIncentive], 
    [Project1].[ChromeStyleId] AS [ChromeStyleId], 
    [Project1].[ChromeStyleIdYear] AS [ChromeStyleIdYear], 
    [Project1].[IncludeMasterIncentiveVins] AS [IncludeMasterIncentiveVins], 
    [Project1].[CreatedID] AS [CreatedID], 
    [Project1].[CreatedDt] AS [CreatedDt], 
    [Project1].[ModifiedID] AS [ModifiedID], 
    [Project1].[ModifiedDt] AS [ModifiedDt], 
    [Project1].[DealerID] AS [DealerID],
	[Project1].[IsExcluded] AS [IsExcluded]
    FROM ( SELECT 
        [Extent1].[IncentiveTranslatedId] AS [IncentiveTranslatedId], 
        [Extent1].[DisplaySection] AS [DisplaySection], 
        [Extent1].[Position] AS [Position], 
        [Extent1].[TranslatedName] AS [TranslatedName], 
        [Extent1].[ProgramName] AS [ProgramName], 
        [Extent1].[Type] AS [Type], 
        [Extent1].[Id] AS [Id], 
        [Extent1].[Detail] AS [Detail], 
        [Extent1].[Disclaimer] AS [Disclaimer], 
        [Extent1].[StartDate] AS [StartDate], 
        [Extent1].[EndDate] AS [EndDate], 
        [Extent1].[IsActive] AS [IsActive], 
        [Extent1].[Vin] AS [Vin], 
        [Extent1].[ModelId] AS [ModelId], 
        [Extent1].[IsCashCoupon] AS [IsCashCoupon], 
        [Extent1].[IsCustomDiscount] AS [IsCustomDiscount], 
        [Extent1].[Amount] AS [Amount], 
        [Extent1].[ExcludeEngineCodes] AS [ExcludeEngineCodes], 
        [Extent1].[ExcludeVINs] AS [ExcludeVINs], 
        [Extent1].[HasSetDateRange] AS [HasSetDateRange], 
        [Extent1].[excludeTrimId] AS [excludeTrimId], 
        [Extent1].[ExcludeYear] AS [ExcludeYear], 
        [Extent1].[HideOtherIncentives] AS [HideOtherIncentives], 
        [Extent1].[DiscountType] AS [DiscountType], 
        [Extent1].[HideDiscounts] AS [HideDiscounts], 
        [Extent1].[excludeBodyStyleId] AS [excludeBodyStyleId], 
        [Extent1].[IsMasterIncentive] AS [IsMasterIncentive], 
        [Extent1].[ChromeStyleId] AS [ChromeStyleId], 
        [Extent1].[ChromeStyleIdYear] AS [ChromeStyleIdYear], 
        [Extent1].[IncludeMasterIncentiveVins] AS [IncludeMasterIncentiveVins], 
        [Extent1].[CreatedID] AS [CreatedID], 
        [Extent1].[CreatedDt] AS [CreatedDt], 
        [Extent1].[ModifiedID] AS [ModifiedID], 
        [Extent1].[ModifiedDt] AS [ModifiedDt], 
        [Extent1].[DealerID] AS [DealerID],
		CAST(
             CASE
                  WHEN CHARINDEX(@engineId,[Extent1].[ExcludeEngineCodes]) > 0
						OR CHARINDEX(@vehicleVin,[Extent1].[ExcludeVINs]) > 0
						OR CHARINDEX(@trimId,[Extent1].[excludeTrimId]) > 0
						OR CHARINDEX(@year,[Extent1].[excludeYear]) > 0
						OR CHARINDEX(@bodyStyleId,[Extent1].[excludebodystyleid]) > 0
                     THEN 1
                  ELSE 0
             END AS bit)
		AS [IsExcluded]
        FROM [dbo].[IncentiveTranslateds] AS [Extent1]
        WHERE (1 = [Extent1].[IsCashCoupon])
		AND ([Extent1].[IsActive] = 1) 
		AND (
				(
					(
						([Extent1].[Vin] = @vehicleVin) 
						OR (([Extent1].[Vin] IS NULL) AND (@vehicleVin IS NULL))
					)
					AND ([Extent1].[ModelId] = 0)
				) 
				OR (
					([Extent1].[Vin] = N'') 
					AND ([Extent1].[ModelId] = @modelTranslatedId) 
					AND (
							(@chromeStyleId = (CASE WHEN ( NOT (([Extent1].[ChromeStyleId] IS NULL) OR (( CAST(LEN([Extent1].[ChromeStyleId]) AS int)) = 0))) THEN [Extent1].[ChromeStyleId] ELSE @chromeStyleId END)) 
							OR (
								(@chromeStyleId IS NULL) AND (CASE WHEN ( NOT (([Extent1].[ChromeStyleId] IS NULL) OR (( CAST(LEN([Extent1].[ChromeStyleId]) AS int)) = 0))) THEN [Extent1].[ChromeStyleId] ELSE @chromeStyleId END IS NULL)
								)
						) 
					AND (@chromeStyleIdYear = (CASE WHEN ([Extent1].[ChromeStyleIdYear] <> 0) THEN [Extent1].[ChromeStyleIdYear] ELSE @chromeStyleIdYear END))
				) 
				OR (([Extent1].[Vin] = N'') AND (0 = [Extent1].[ModelId])) 
				OR (CHARINDEX(@vehicleVin, [Extent1].[IncludeMasterIncentiveVins]) > 0)
			)
    )  AS [Project1]
    ORDER BY [Project1].[Position] ASC
END
GO
