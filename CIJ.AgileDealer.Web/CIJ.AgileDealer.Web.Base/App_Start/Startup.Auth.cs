﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using System;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;


namespace CJI.AgileDealer.Web.Base
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(AppDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            //// Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");



            ////https://agiledealer-demo.azurewebsites.net
            //app.UseGoogleAuthentication(
            //   clientId: "1036006166321-7fc3ibnbltf4l4tj7cdf4u78qtjdu2t1.apps.googleusercontent.com",
            //   clientSecret: "Jkz-zwRKBSfwMRTm-RSSVi6H");

            ////https://agiledealer-demo.azurewebsites.net
            //app.UseFacebookAuthentication(
            //   appId: "535135733307176",
            //   appSecret: "2f093f73970c7e68599cc57de3e9daac");



            //local host
            //app.UseGoogleAuthentication(
            //   clientId: "798767542593-fq3fs4qtbs2h80qp4tcrkgvs7vmqvmer.apps.googleusercontent.com",
            //   clientSecret: "Xkrx6VFsr93GiZR0GkwyOf6m");

            app.UseGoogleAuthentication(
               clientId: "946738903698-idl0p4k2v3ifjjk7vk0qqh8npr8lv4st.apps.googleusercontent.com",
               clientSecret: "iodGNeg9WWB-PROyF86bHuK4");

            ////local host
            //app.UseFacebookAuthentication(
            //   appId: "700675263388467",
            //   appSecret: "e86a312d96c75b0a6a0344b918f0c127");

        }
    }
}
