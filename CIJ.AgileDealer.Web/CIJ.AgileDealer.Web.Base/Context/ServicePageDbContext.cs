﻿using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Home;
using AgileDealer.Data.Entities.Nagivation;
using Microsoft.AspNet.Identity.EntityFramework;
using CJI.AgileDealer.Web.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.AboutUs;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.ErrorLog;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.SiteMap;
using AgileDealer.Data.Entities.Vehicles;
using Microsoft.AspNet.Identity;
using Location = AgileDealer.Data.Entities.Content.Location;
using AgileDealer.Data.Entities.Localization;
using System.Configuration;
using AgileDealer.Data.Entities.Parts;
using AgileDealer.Data.Entities.CMS;
using System.Data.Entity.Validation;
using AgileDealer.Data.Entities.Jobs;

namespace CJI.AgileDealer.Web.Base.Context
{
    public class ServicePageDbContext : IdentityDbContext<ApplicationUser>
    {
        static bool isMigrationEnable = false;
        static ServicePageDbContext()
        {
            isMigrationEnable = (ConfigurationManager.AppSettings["EnableMigration"] ?? "false").Trim().Equals("true", StringComparison.OrdinalIgnoreCase);
        }

        public ServicePageDbContext()
            : base("AgileDealer")
        {
            if (isMigrationEnable){
                Database.SetInitializer<ServicePageDbContext>(new CreateDatabaseIfNotExists<ServicePageDbContext>());
            }
            else{
                Database.SetInitializer<ServicePageDbContext>(null);
            }
        }
        public static ServicePageDbContext Create()
        {
            return new ServicePageDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Applicant>().Property(p => p.ApplicantId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			// configure 1 to zero or 1 to 1
			modelBuilder.Entity<ContentPage>()
		   .HasOptional(o => o.PostData)
		   .WithRequired(o => o.ContentPage);

			base.OnModelCreating(modelBuilder);

        }

        public int SaveChanges(ApplicationUser user)
        {
            this.ChangeTracker.DetectChanges();

            // do audit here
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            var changedItem = objectContext.ObjectStateManager.
                GetObjectStateEntries(EntityState.Modified | EntityState.Deleted | EntityState.Added);

            SetAuditFields(changedItem, user);

            return base.SaveChanges();
        }

        public void SaveMigration()
        {
            try
            {
                this.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }
        private void SetAuditFields(IEnumerable<ObjectStateEntry> changedItem, ApplicationUser user)
        {
            if (changedItem != null)
            {
                foreach (var item in changedItem)
                {
                    if (item.State == EntityState.Modified || item.State == EntityState.Deleted)
                    {
                        ((EntityBase)item.Entity).ModifiedDt = DateTime.Now;
                        ((EntityBase)item.Entity).ModifiedID = user.Id;
                    }
                    else if (item.State == EntityState.Added)
                    {
                        ((EntityBase)item.Entity).ModifiedID = user.Id;
                        ((EntityBase)item.Entity).CreatedID = user.Id;
                        ((EntityBase)item.Entity).ModifiedDt = DateTime.Now;
                        ((EntityBase)item.Entity).CreatedDt = DateTime.Now;
                        ((EntityBase)item.Entity).DealerID = 1;
                    }
                }
            }
        }


        public DbSet<LocationType> LocationTypes { get; set; }
        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<DisplayPage> DisplayPages { get; set; }
        public DbSet<Hours> Hours { get; set; }
        public DbSet<Location> Locations { get; set; }

        public DbSet<Manufacture> Manufactures { get; set; }
        public DbSet<PageLayout> PageLayouts { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceArea> ServiceAreas { get; set; }
        public DbSet<ServicePage> ServicePages { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<Special> Specials { get; set; }
        public DbSet<StaffPage> StaffPages { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Employee_Department> EmployeeDepartments { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Footer> Footers { get; set; }
        public DbSet<Header> Headers { get; set; }
        public DbSet<HourType> HourTypes { get; set; }

        public DbSet<BlogTag> BlogTags { get; set; }
        public DbSet<BlogCategory> BlogCategoryies { get; set; }
        public DbSet<BlogEntry> BlogEntrys { get; set; }
        public DbSet<BlogEntry_BlogCategory> BlogEntries_BlogCategories { get; set; }
        public DbSet<BlogEntry_BlogTag> BlogEntries_BlogTags { get; set; }

        public DbSet<HomePage> HomePages { get; set; }
        public DbSet<HomePageButton> HomePageButtons { get; set; }
        public DbSet<HomePageButtonType> HomePageButtonTypes { get; set; }
        public DbSet<HomePageButtonWidget> HomePageButtonWidgets { get; set; }
        public DbSet<HomePageReview> HomePageReviews { get; set; }
        public DbSet<HomePageSlide> HomePageSlides { get; set; }
        public DbSet<HomePageSliderEntry> HomePageSliderEntrys { get; set; }
        public DbSet<HomePageThirdPartyWidget> HomePageThirdPartyWidgets { get; set; }
        public DbSet<HomePageVehicle> HomePageVehicles { get; set; }

        public DbSet<HomePageVehicleCategory> HomePageVehicleCategorys { get; set; }

        public DbSet<ExteriorColor> ExteriorColors { get; set; }
        public DbSet<InteriorColor> InteriorColors { get; set; }
        public DbSet<Make> Makes { get; set; }

        public DbSet<Trim> Trims { get; set; }

        public DbSet<Model> Models { get; set; }
        public DbSet<ModelTranslated> ModelTranslateds { get; set; }

        public DbSet<BodyStyleType> BodyStyleTypes { get; set; }
        public DbSet<BodyStyle> BodyStyles { get; set; }

        public DbSet<Drive> Drives { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<EngineType> EngineTypes { get; set; }
        public DbSet<FuelType> FuelTypes { get; set; }
        public DbSet<Mileage> Mileages { get; set; }

        public DbSet<AgileVehicle> AgileVehicles { get; set; }
        public DbSet<AgileVehicleImage> AgileVehicleImages { get; set; }

        //public DbSet<HomeNetVehicle> HomeNetVehicles { get; set; }
        public DbSet<HomeNetVehicle> Vehicles { get; set; }

        public DbSet<VechicleViewHistory> VehicleViewHistories { get; set; }
        
        public DbSet<Menu> Menus { get; set; }

        public DbSet<MenuExclusion> MenuExclusions { get; set; }
        public DbSet<MenuExNavigation> MenuExNavigations { get; set; }
        public DbSet<Navigation> Navigations { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<RoleExMenu> RoleExMenus { get; set; } 
 
        public DbSet<HomeNetPull> HomeNetPulls { get; set; }

        public DbSet<DealersHomeNetDealerName> DealersHomeNetDealerNames { get; set; } 
 
        public DbSet<AboutUs> AboutUses { get; set; }

        public DbSet<CustomerQuestion> CustomerQuestions { get; set; }

        public DbSet<PageMetaData> PageMetaDatas { get; set; }

        public DbSet<PageVariable> PageVariables { get; set; }

        public DbSet<Price> Prices { get; set; }
        public DbSet<ExteriorColorTranslated> ExteriorColorTranslateds { get; set; }
        public DbSet<EngineTypeTranslated> EngineTypeTranslateds { get; set; }
        public DbSet<VehicleInfoPage> VehicleInfoPages { get; set; }
        public DbSet<VehicleFeedBack> VehicleFeedBacks { get; set; }

        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<ApplicantInfo> ApplicantInfos { get; set; }
        public DbSet<ContactInfo> ContactInfos { get; set; }
        public DbSet<EmploymentInfo> EmploymentInfos { get; set; }
        public DbSet<ApplicantType> ApplicantTypes { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Year> Years { get; set; }
        public DbSet<Month> Months { get; set; }
        public DbSet<Date> Dates { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<ResidenceStatus> ResidenceStatuses { get; set; }


        public DbSet<ScheduleService> ScheduleServices { get; set; }
        public DbSet<CurrentIssue> CurrentIssues { get; set; }
        public DbSet<ServiceRequested> ServiceRequesteds { get; set; }
        public DbSet<ScheduleTime> ScheduleTimes { get; set; }
        public DbSet<ScheduleServiceRequested> ScheduleServiceRequesteds { get; set; }
        public DbSet<ScheduleCurrentIssue> ScheduleCurrentIssues { get; set; }
        public DbSet<DealerServiceSpecial> DealerServiceSpecials { get; set; }
        public DbSet<DealerShip> DealerShips { get; set; }

        public DbSet<BuyerSession> BuyerSessions { get; set; }
        public DbSet<BuyerPageVisited> BuyerPageVisiteds { get; set; }
        public DbSet<BuyerVehicleDisplayPage> BuyerVehicleDisplayPages { get; set; }
        public DbSet<FormsTables> FormsTables { get; set; }
        public DbSet<PeriodType> PeriodTypes { get; set; }
        public DbSet<MakeTranslated> MakeTranslateds { get; set; }
        public DbSet<FordVehicleContent> FordVehicleContents { get; set; }
        public DbSet<PrivateOffer> PrivateOffers { get; set; }
        public DbSet<SpecialOffer> SpecialOffers { get; set; }
        public DbSet<VinEligibility> VinEligibilities { get; set; }
        public DbSet<NamePlate> NamePlates { get; set; }
        public DbSet<TrimList> TrimLists { get; set; }
        public DbSet<NamePlateTrim> NamePlateTrims { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupList> GroupLists { get; set; }
        public DbSet<GroupType> GroupTypes { get; set; }
        public DbSet<GroupSubType> GroupSubTypes { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramType> ProgramTypes { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<AgileVehiclesProgram> AgileVehiclesPrograms { get; set; }
        public DbSet<CompetitiveModel> CompetitiveModels { get; set; }
        public DbSet<CompetitiveMake> CompetitiveMakes { get; set; }
        public DbSet<VehicleStandardBody> VehicleStandardBodies { get; set; }
        public DbSet<VehicleModelTrim> VehicleModelTrims { get; set; }
        public DbSet<Sitemap> SiteMaps { get; set; }
        public DbSet<SiteMapLocation> SiteMapLocations { get; set; }
        public DbSet<UploadInformation> UploadInformations { get; set; }
        public DbSet<VehicleCriteriaMapping> VehicleCriteriaMappings { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<AprTerm> AprTerms { get; set; }
        public DbSet<LeaseTerm> LeaseTerms { get; set; }
        public DbSet<AprTermList> AprTermList { get; set; }
        public DbSet<LeaseTermList> LeaseTermList { get; set; }
        public DbSet<VehicleIncentiveData> VehicleIncentiveDatas { get; set; }
        public DbSet<NotificationSetting> NotificationSettings { get; set; }

        public DbSet<CustomRedirectUrl> CustomRedirectUrls { get; set; }
        public DbSet<PartRequest> PartRequests { get; set; }

		public DbSet<ContentPage> ContentPages { get; set; }

		public DbSet<ContentPagePostData> ContentPagePostData { get; set; }
		public DbSet<TopPriorityProgram> TopPriorityPrograms { get; set; }
        public DbSet<JobTracking> JobTrackings { get; set; }
        public DbSet<JobScheduler> JobSchedulers { get; set; }
        public DbSet<IncentiveTranslated> IncentiveTranslateds { get; set; }
        public DbSet<AgileVehicleFactoryCode> AgileVehicleFactoryCodes { get; set; }

        public DbSet<HideIncentiveProgram> HideIncentivePrograms { get; set; }
        public DbSet<MissingIncentiveProgram> MissingIncentivePrograms { get; set; }

        public DbSet<VehicleFeatureMaster> VehicleFeatureMaster { get; set; }
        public DbSet<VehicleFeatures> VehicleFeatures { get; set; }
        public DbSet<EmployeePriceSetting> EmployeePriceSettings { get; set; }
        public DbSet<EmployeePriceMakeBasedSetting> EmployeePriceMakeBasedSettings { get; set; }
        #region localization
        public DbSet<Language> Languages { get; set; }
        public DbSet<ServiceRequested_Trans> ServiceRequested_Trans { get; set; }
        public DbSet<CurrentIssue_Trans> CurrentIssue_Trans { get; set; }
        public DbSet<ServicePage_Trans> ServicePage_Trans { get; set; }
        public DbSet<Hour_Trans> Hour_Translations { get; set; }
        public DbSet<Service_Trans> Service_Trans { get; set; }
        public DbSet<Special_Trans> Special_Trans { get; set; }
        public DbSet<DealerServiceSpecial_Trans> DealerServiceSpecial_Trans { get; set; }
        public DbSet<PeriodType_Trans> PeriodType_Trans { get; set; }
        public DbSet<Notification_Trans> Notification_Trans { get; set; }
        public DbSet<Month_Trans> Month_Trans { get; set; }
        public DbSet<PageMetaDatas_Trans> PageMetaDatas_Trans { get; set; }
        public DbSet<BlogEntry_Trans> BlogEntry_Trans { get; set; }
        public DbSet<AboutUs_Trans> AboutUs_Trans { get; set; }
        public DbSet<Header_Trans> Header_Trans { get; set; }
		public DbSet<Menu_Trans> Menu_Trans { get; set; }
		public DbSet<ContentPage_Trans> ContentPage_Trans { get; set; }
		#endregion
	}
}
