﻿using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Blog;
using CJI.AgileDealer.Web.Base.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CJI.AgileDealer.Web.Base.Context
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext()
            : base("AgileDealer", throwIfV1Schema: false)
        {
            Database.SetInitializer<AppDbContext>(new CreateDatabaseIfNotExists<AppDbContext>());
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public int SaveChanges(ApplicationUser user)
        {
            this.ChangeTracker.DetectChanges();

            // do audit here
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            var changedItem = objectContext.ObjectStateManager.
                GetObjectStateEntries(EntityState.Modified | EntityState.Deleted | EntityState.Added);

            return base.SaveChanges();
        }
        
    }
}
