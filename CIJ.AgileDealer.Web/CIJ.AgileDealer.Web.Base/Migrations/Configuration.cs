using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.AboutUs;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Home;
using AgileDealer.Data.Entities.Nagivation;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace CJI.AgileDealer.Web.Base.Migrations
{
    internal sealed class Configuration2 :
        DbMigrationsConfiguration<ServicePageDbContext>
    {
        private MasterDataGenerator _masterDataGenerator;
        private MasterDataGenerator MasterDataGenerator
        {
            get
            {
                if (_masterDataGenerator == null)
                {
                    _masterDataGenerator = new MasterDataGenerator();
                }

                return _masterDataGenerator;
            }
            set
            {
                _masterDataGenerator = value;
            }
        }

        public Configuration2()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }



        protected override void Seed(ServicePageDbContext context)
        {
            var db = context;

            ////#region Localization
            //MasterDataGenerator.CreateLanguage();
            ////#endregion


            //MasterDataGenerator.CreateManufacturer();
            //MasterDataGenerator.CreateDealer_HomeNetVehicle(db);
            //MasterDataGenerator.CreateHomeDataForDealer(db);


            //////Service Content
            //MasterDataGenerator.CreateServiceContent(db);

            //////Employee
            //MasterDataGenerator.CreateEmployee(context);

            //////////About Us and Directions
            //MasterDataGenerator.CreateAboutUs(db, null);
            //MasterDataGenerator.Create_ContactUs_Hours(db);

            //////VehicleInfoPageHelper
            //MasterDataGenerator.CreateVehicleInfoPage();

            //////Blog Entries
             //MasterDataGenerator.CreateBlogEntries(context);

            //////Master Data Of Applicant
            //MasterDataGenerator.CreateMasterDataForApplicant();

            ////// Schedule Service
            //MasterDataGenerator.CreateCurrentIssue();
            //MasterDataGenerator.CreateServiceRequested();
            //MasterDataGenerator.CreateScheduleTime();

            //////Service Specials
            //MasterDataGenerator.CreateServiceSpecial();

            //////Period Type
            //MasterDataGenerator.CreatePeriodType();

            ////////Map Data for Vehicles
            //MasterDataGenerator.CreateVehicleStandardBody(db);
            //MasterDataGenerator.CreateMappingForSearchCriteria();
            //MasterDataGenerator.CreateSegmentForVehicle();

            ////////VCP
            //MasterDataGenerator.CreateFordVehicleContent();

            //MasterDataGenerator.CreateRoles(db);

            //////Map Data for Incentives
            //MasterDataGenerator.CreateCompetitiveModel(db);

            //////Create Vehicle Standard Body
            //MasterDataGenerator.CreateVehicleStandardBody(db);

            //MasterDataGenerator.CreateSiteMapData();
            //MasterDataGenerator.CreateSiteMapLocationData();

            //MasterDataGenerator.CreatePageMetaData(db);
            //MasterDataGenerator.CreateMappingForSearchCriteria();
            //MasterDataGenerator.CreateSegmentForVehicle();

            //////Notification Settings
            //MasterDataGenerator.CreateNotificationSettings();

            //////Upload Information
            //MasterDataGenerator.CreateUploadInformation(db);

            //MasterDataGenerator.CreateCustomRedirectUrls();

        }
    }
}
