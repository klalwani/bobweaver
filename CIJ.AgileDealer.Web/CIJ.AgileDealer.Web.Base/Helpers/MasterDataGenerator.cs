﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.AboutUs;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Home;
using AgileDealer.Data.Entities.Nagivation;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.SiteMap;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.Base;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using AgileDealer.Data.Entities.Localization;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public class MasterDataGenerator
    {
        #region Home Data

        public void CreateUploadInformation(ServicePageDbContext context)
        {
            context.UploadInformations.AddOrUpdate(x => x.FolderName,
                new UploadInformation { MethodName = "UploadStaffImage", FolderName = "content/staff/" },
                new UploadInformation { MethodName = "UploadVehicleImage", FolderName = "content/vehicleimages/" },
                new UploadInformation { MethodName = "UploadBlogImage", FolderName = "content/blogimages/" },
                new UploadInformation { MethodName = "UploadSildeImage", FolderName = "content/slideimages/" }
                );
            SaveChange(context);
        }

        public void CreateLanguage()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.Languages.AddOrUpdate(x => x.Description, new Language
                {
                    Name = "en-US",
                    Order = 1,
                    Description = "English",
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                    ModifiedID = string.Empty,
                    CreatedID = string.Empty,
                    Active=true,
                    DealerID=1
                });
                //,
                //    new Language
                //    {
                //        Name = "es-MX",
                //        Order = 2,
                //        Description = "Mexico",
                //        CreatedDt = DateTime.Now,
                //        ModifiedDt = DateTime.Now,
                //        ModifiedID = string.Empty,
                //        CreatedID = string.Empty
                //    })
                    
                db.SaveChanges();
            }
        }
        public void CreateHomeDataForDealer(ServicePageDbContext context)
        {
            CreateDataForDealerAndService(context);
            CreateDealerHomeNet(context);
            CreateDataHome(context);
           
            CreateHeader(context);
            CreateFooter(context);
           // PopulateDataNavigation(context);
            CreatePageMetaData(context);
        }

        public void CreateManufacturer()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.Manufactures.AddOrUpdate(x => x.ManufactureID, new Manufacture { ManufactureID = 1, Name = "Auto", Description = "", Order = 0, Active = true, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now });
                db.SaveChanges();
            }
        }

        private void CreateDataForDealerAndService(ServicePageDbContext context)
        {
            var db = context;
            var dealer = db.Dealers.First();
            var user = new ApplicationUser { Id = Guid.NewGuid().ToString() };

            #region setup dealer & service

            db.LocationTypes.AddOrUpdate(
                new LocationType { LocationTypeID = 1, Name = "Main", Active = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
            );

            db.ServiceTypes.AddOrUpdate(
                new ServiceType { ServiceTypeID = 1, Name = "Oil-Change", Active = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new ServiceType { ServiceTypeID = 2, Name = "Brake", Active = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new ServiceType { ServiceTypeID = 3, Name = "Tire", Active = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
            );

            db.SaveChanges(user);

            var loc = db.LocationTypes.FirstOrDefault(a => a.Name == "Main");

            var locList = new List<Location>
            {
                new Location{
                    LocationID = 1,
                    LocationTypeID = 1,
                    DealerID = 1,
                    Name = "",
                    Description = "",
                    Street1 = "2174 West Market Street",
                    Street2 = "Pottsville, PA 17901",
                    City = "Pottsville",
                    State = "PA",
                    Zip = "17901",
                    MainPhone = "(844) 242-2972",
                    SalesPhone = "(844) 242-2972",
                    ServicePhone = "",
                    PartsPhone = "",
                    CreatedID = "",
                    CreatedDt = DateTime.Now,
                    ModifiedID = "",
                    ModifiedDt = DateTime.Now}
            };

            dealer.Locations = locList;

            db.PageLayouts.AddOrUpdate(new PageLayout { PageLayoutID = 1, Name = "3 column horizontal", Order = 1, Active = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });

            db.SaveChanges(user);

            var pageLayout = db.PageLayouts.First();

            db.DisplayPages.AddOrUpdate(new DisplayPage { DisplayPageID = 1, DealerID = dealer.DealerID, Name = "Service", Description = "", PageLayoutID = pageLayout.PageLayoutID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });

            db.SaveChanges(user);
            #endregion
        }

        private void CreateHeader(ServicePageDbContext context)
        {
            var dealer = context.Dealers.First();
            context.Headers.Add(new Header
            {
                HeaderID = 1,
                DealerID = 1,
                SiteLogo = "https://agiledealer.blob.core.windows.net/generic/dealership/bob-weaver-logo.png",
                SiteLogoAltText = "Bob Weaver Auto logo",
                SiteLink = "",
                PersonalizationText = "",
                PersonalizationLogo = "",
                PeronsalizationAltText = "",
                CTAImage = "",
                CTAImageAltText = "",
                CTALink = "",
                ExitSignButton = "",
                CreatedID = "",
                CreatedDt = DateTime.Now,
                ModifiedID = "",
                ModifiedDt = DateTime.Now }

            );
            context.SaveChanges();
        }

        private void CreateFooter(ServicePageDbContext context)
        {
            var dealer = context.Dealers.First();
            context.Footers.AddOrUpdate(
                new Footer
                {
                    FooterID = 1,
                    DealerID = 1,
                    FacebookLinkImage = "https://agiledealer.blob.core.windows.net/generic/social/facebook.png",
                    FacebookLinkImageAltText = "Bob Weaver Facebook Page",
                    FacebookLink = "https://www.facebook.com/pages/Bob-Weaver-GM-Chrysler/131643886374",
                    GooglePlusImage = "https://agiledealer.blob.core.windows.net/generic/social/googleplus.png",
                    GooglePlusImageAltText = "Bob Weaver Google Plus Page",
                    GooglePlusLink = "https://plus.google.com/+BobWeaverGMChryslerPottsville",
                    TwitterImage = "https://agiledealer.blob.core.windows.net/generic/social/twitter.png",
                    TwitterImageAltText = "Bob Weaver Twitter Page",
                    TwitterLink = "https://twitter.com/bobweaverauto",
                    InstagramLinkImage = "https://agiledealer.blob.core.windows.net/gatorford/content/icons/instagram-logo.png",
                    InstagramLinkImageAltText = "Bob Weaver Instagram Page",
                    InstagramLink = "https://www.instagram.com/bobweaverauto/",
                    CreatedID = "",
                    CreatedDt = DateTime.Now, ModifiedID = "",
                    ModifiedDt = DateTime.Now }

            );
            SaveChange(context);
        }

        private void SaveChange(ServicePageDbContext db)
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                ExceptionHelper.LogException("MasterDataGenerator Error: ", ex, "SaveChange", System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.Base", "Entity Validation Failed - errors follow:\n" + sb);
            }
        }

        private void CreateDataHome(ServicePageDbContext context)
        {
            var db = context;

            db.HomePages.AddOrUpdate(
                new HomePage { HomePageId = 1, DealerID = 1, TopMessage = "Home 1", SearchBoxFilterText = "Home page 1", BioHeading = "Welcome to Bob Weaver Auto", BioText = "Welcome to Bob Weaver Auto" }
                );
            db.HomePageReviews.AddOrUpdate(
                 new HomePageReview { HomePageReviewId = 1, HomePageId = 1, StartRating = 5, ReviewText = "Competitive pricing, friendly attitude. Our salesman, Nate Snyder was very knowledgeable and put forth an excellent effort before and after the sale. Overall a positive experience. We will be repeat customers.", ReviewName = "Alan Marshall", ReviewRelateUrl = "Home.aspx", DealerID=1  }
                , new HomePageReview { HomePageReviewId = 2, HomePageId = 1, StartRating = 5, ReviewText = "Outten serviced our jeep 20 times for the same problem. Coolant loss. They called in the Jeep expert and his documented decision was -it is normal for a 2015 Jeep Wrangler to use 1 quart of coolant every 718 miles. Final decision!! We needed help. Call Bob Weavers service department. They found the leek in two hours. Cant say enough.", ReviewName = "Happy mail lady", ReviewRelateUrl = "Home.aspx", DealerID = 1 }
                , new HomePageReview { HomePageReviewId = 3, HomePageId = 1, StartRating = 5, ReviewText = "I was so pleased with the service I received. I have never had such an amazing experience with the service department in ANY dealership ever. Chris is an absolutely incredible service manager and worked to achieve an awesome outcome. He is truly an asset to your company. He is to be commended. Thank you so very much. I appreciate it.", ReviewName = "Barbara Dehner", ReviewRelateUrl = "Home.aspx", DealerID = 1 }
                );
            db.HomePageVehicleCategorys.AddOrUpdate(
                new HomePageVehicleCategory { HomePageId = 1, HomePageVehicleCatoryId = 1, HomePageVehicleCategoryName = "Trucks", DealerID=1 },
                new HomePageVehicleCategory { HomePageId = 1, HomePageVehicleCatoryId = 2, HomePageVehicleCategoryName = "Crossovers and SUVs", DealerID = 1 },
                new HomePageVehicleCategory { HomePageId = 1, HomePageVehicleCatoryId = 3, HomePageVehicleCategoryName = "Vans", DealerID = 1 }
                );
            db.HomePageVehicles.AddOrUpdate(
                 new HomePageVehicle { HomePageVehicleId = 1, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-F-150-Gray.jpg", VehicleName = "F-150", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/f-150", DealerID=1 }
                , new HomePageVehicle { HomePageVehicleId = 2, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-F-250-Super-Duty-Blue.jpg", VehicleName = "F-250", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/f-250-super-duty", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 3, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-F-350-Brown.jpg", VehicleName = "F-350", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/f-350-super-duty", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 4, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Transit-Connect-Blue.png", VehicleName = "Transit Connect", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/transit-connect", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 5, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Transit-White.jpg", VehicleName = "Transit Wagon", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/transit-wagon", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 6, HomePageVehicleCatoryId = 1, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Transit-Van-White.jpg", VehicleName = "Transit Cargo Van", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/transit-cargo-van", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 7, HomePageVehicleCatoryId = 2, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Escape-Blue.jpg", VehicleName = "Escape", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/escape", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 8, HomePageVehicleCatoryId = 2, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Edge-Gray.jpg", VehicleName = "Edge", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/edge", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 9, HomePageVehicleCatoryId = 2, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Flex-Red.png", VehicleName = "Flex", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/flex", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 10, HomePageVehicleCatoryId = 2, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Explorer-Gray.jpg", VehicleName = "Explorer", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/explorer", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 11, HomePageVehicleCatoryId = 2, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Expedition-Black.png", VehicleName = "Expedition", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/expedition", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 12, HomePageVehicleCatoryId = 3, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Mustang-Red.jpg", VehicleName = "Ford Mustang", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/mustang", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 13, HomePageVehicleCatoryId = 3, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Fusion-White.jpg", VehicleName = "Ford Fusion", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/fusion", DealerID = 1 }
                , new HomePageVehicle { HomePageVehicleId = 14, HomePageVehicleCatoryId = 3, VehicleImage = "https://agiledealer.blob.core.windows.net/generic/content/medium/Ford-Taurus-Blue.png", VehicleName = "Ford Taurus", VehicleText = "", VehicleRelativeUrl = "/vcp/new/ford/taurus", DealerID = 1 }

                );
            db.HomePageButtonTypes.AddOrUpdate(
                new HomePageButtonType { HomePageButtonTypeId = 1, Name = "Four Button", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
                new HomePageButtonType { HomePageButtonTypeId = 2, Name = "Two Button", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                );
            db.HomePageButtonWidgets.AddOrUpdate(
                new HomePageButtonWidget { HomePageId = 1, HomePageButtonWidgetId = 1, HomePageButtonTypeId = 1, HomePageButtonWidgetName = "Four Widget", DealerID = 1 },
                new HomePageButtonWidget { HomePageId = 1, HomePageButtonWidgetId = 2, HomePageButtonTypeId = 2, HomePageButtonWidgetName = "Two Widget", DealerID = 1 }
                );
            db.HomePageButtons.AddOrUpdate(
                new HomePageButton { HomePageButtonId = 1, ButtonImageNumber = 1, HomePageButtonWidgetId = 1, ButtonImagePath = @"Content/Images/Home/FourButton/1.jpg", ButtonImageText = "Search New", DealerID = 1 },
                new HomePageButton { HomePageButtonId = 2, ButtonImageNumber = 2, HomePageButtonWidgetId = 1, ButtonImagePath = @"Content/Images/Home/FourButton/2.jpg", ButtonImageText = "Search Used", DealerID = 1 },
                new HomePageButton { HomePageButtonId = 3, ButtonImageNumber = 3, HomePageButtonWidgetId = 1, ButtonImagePath = @"Content/Images/Home/FourButton/3.jpg", ButtonImageText = "Specials", DealerID = 1 },
                new HomePageButton { HomePageButtonId = 4, ButtonImageNumber = 4, HomePageButtonWidgetId = 1, ButtonImagePath = @"Content/Images/Home/FourButton/4.jpg", ButtonImageText = "Schedule Service", DealerID = 1 },

                new HomePageButton { HomePageButtonId = 5, ButtonImageNumber = 1, HomePageButtonWidgetId = 2, ButtonImagePath = @"Content/Images/Home/TwoButton/NA_Homepage.jpg", ButtonImageText = "Search New", DealerID = 1 },
                new HomePageButton { HomePageButtonId = 6, ButtonImageNumber = 2, HomePageButtonWidgetId = 2, ButtonImagePath = @"Content/Images/Home/TwoButton/NA_Homepage.jpg", ButtonImageText = "Search Used", DealerID = 1 }
                );

            db.HomePageSliderEntrys.AddOrUpdate(
                new HomePageSliderEntry { SliderEntryId = 1, HomePageId = 1, IsActive = true, HomePageSliderName = "Home Page Slide 1", HomePagePosition = "Position 1", DealerID=1 }
                );
            db.HomePageSlides.AddOrUpdate(
                 new HomePageSlide { SlideId = 1, SlideEntryId = 1, SlideName = "",  SlideNumber = 1, SlideImageName = "2018 Ford F-150", SlideImagePath = "https://storage.googleapis.com/brightonfordco/slides/2018-f-150-motor-trend-truck-of-the-year.jpg", SlideLink = " /vcp/new/ford/f-150", SlideRelativeUrl = "", DealerID=1 }
                , new HomePageSlide { SlideId = 2, SlideEntryId = 1, SlideName = "", SlideNumber = 2, SlideImageName = "Free Maintenance Book", SlideImagePath = "https://storage.googleapis.com/brightonfordco/slides/free-maintenance-book-brighton-ford.jpg", SlideLink = " /new", SlideRelativeUrl = "", DealerID = 1 }
                , new HomePageSlide { SlideId = 3, SlideEntryId = 1, SlideName = "", SlideNumber = 3, SlideImageName = "Free Maintenance Book", SlideImagePath = "https://storage.googleapis.com/brightonfordco/slides/2018-ford-focus.jpg", SlideLink = " /vcp/new/ford/focus", SlideRelativeUrl = "", DealerID = 1 }
                , new HomePageSlide { SlideId = 4, SlideEntryId = 1, SlideName = "", SlideNumber = 4, SlideImageName = "Free Maintenance Book", SlideImagePath = "https://storage.googleapis.com/brightonfordco/slides/ford-truck-month.jpg", SlideLink = " /vehicle-department/new/truck", SlideRelativeUrl = "", DealerID = 1 }

            );

            SaveChange(db);
        }

        public void CreateDealer_HomeNetVehicle(ServicePageDbContext context)
        {
            var db = context;
            var manufacturer = db.Manufactures.First();
            db.Dealers.AddOrUpdate(d => d.DealerID, new Dealer
            {
                DealerID = 1,
                Name = "Bob Weaver",
                ManufactureID = manufacturer.ManufactureID,
                QuestionEmailAddress = "admin@savvydealer.com",
                EleadTrackAdress = "admin@savvydealer.com",
                ImageServer = "https://bobweaver.blob.core.windows.net/images/",
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now,
                VcpImageStockServer = "https://storage.googleapis.com/agiledealer",
                ImageServerGeneric = "https://agiledealer.blob.core.windows.net/generic/",
                VehicleLookupLink = "http://www.carfax.com/VehicleHistory/p/Report.cfx?partner=DVW_1&vin={Vin}",
                VehicleLookupLogo = @"http://www.carfaxonline.com/assets/subscriber/showmethecarfax.jpg",
                JsonId = @"<script type=""application/ld+json"">
{
  ""@context"": ""http://schema.org/"",
  ""@type"": ""AutoDealer"",
  ""name"": ""Bob Weaver"",
  ""alternateName"": ""Bob Weaver Auto"",  
  ""telephone"": ""(844) 242-2972"",
  ""url"": ""https://www.bobweaverauto.com/"",
  ""logo"": ""https://agiledealer.blob.core.windows.net/generic/dealership/lafayette-ford-lincoln-logo.png"",
  ""description"": ""@@DESCRIPTION"",
  ""contactPoint"" : [{
      ""@type"" : ""ContactPoint"",
      ""telephone"" : ""+1-844-242-2972"",
      ""contactType"" : ""Sales""
  }],
  ""address"": {
    ""@type"": ""PostalAddress"",
    ""streetAddress"": ""2174 West Market Street"",
    ""addressLocality"": ""Pottsville"",
    ""addressRegion"": ""PA"",
    ""postalCode"": ""17901"",
    ""addressCountry "": ""US""
  },
  ""hasMap"": ""https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3025.9281665408894!2d-76.22792118459618!3d40.6755516793359!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c5ee603347747b%3A0x28e935925ade4662!2s2174+W+Market+St%2C+Pottsville%2C+PA+17901!5e0!3m2!1sen!2sus!4v1532550872088"",  
  ""sameAs"" : [ 
     ""'https://www.facebook.com/pages/Bob-Weaver-GM-Chrysler/131643886374"" , 
     ""https://plus.google.com/+BobWeaverGMChryslerPottsville"", 
     ""https://twitter.com/bobweaverauto"", 
],
  ""geo"": {
    ""@type"": ""GeoCoordinates"",
    ""latitude"": 40.6755516793359,
    ""longitude"": -76.2279211845961
  },  
  ""openingHoursSpecification"": [
    {
      ""@type"": ""OpeningHoursSpecification"",
      ""dayOfWeek"": [
        ""Monday"",
        ""Tuesday"",
        ""Wednesday"",
        ""Thursday""      
      ],
      ""opens"": ""9:00"",
      ""closes"": ""20:00""
    },
	 {
      ""@type"": ""OpeningHoursSpecification"",
      ""dayOfWeek"": [
        ""Friday""
      ],
      ""opens"": ""9:00"",
      ""closes"": ""18:00""
    },
    {
      ""@type"": ""OpeningHoursSpecification"",
      ""dayOfWeek"": [
        ""Saturday""
      ],
      ""opens"": ""9:00"",
      ""closes"": ""15:00""
    }   
  ]
}
</script>
"
            });
            SaveChange(db);
        }

        private void CreateDealerHomeNet(ServicePageDbContext context)
        {
            context.DealersHomeNetDealerNames.RemoveRange(context.DealersHomeNetDealerNames.ToList());
            SaveChange(context);
            context.DealersHomeNetDealerNames.AddOrUpdate(a => a.HomeNetDealerName, new DealersHomeNetDealerName()
            {
                DealerID = 1,
                HomeNetDealerName = "Bob Weaver Auto",
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });
            SaveChange(context);
        }

        private void PopulateDataNavigation(ServicePageDbContext context)
        {
            int displayorder = 0;
            context.Navigations.AddOrUpdate(c => c.Name,
                new Navigation
                {
                    Name = "Commercial & Fleet Vehicles",
                    Title = "Commercial & Fleet Vehicles",
                    Description = "Commercial & Fleet Vehicles",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 1,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Schedule Test Drive",
                    Title = "Schedule Test Drive",
                    Description = "Schedule Test Drive",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 2,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "New Vehicle Specials",
                    Title = "New Vehicle Specials",
                    Description = "New Vehicle Specials",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 3,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Lease a New Ford",
                    Title = "Lease a New Ford",
                    Description = "Lease a New Ford",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 4,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Demonstrator Specials",
                    Title = "Lifted & Custom Vehicles",
                    Description = "Lifted & Custom Vehicles",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 5,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "High Performance Vehicles",
                    Title = "High Performance Vehicles",
                    Description = "High Performance Vehicles",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 6,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Truck Finder",
                    Title = "Truck Finder",
                    Description = "Truck Finder",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 7,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Commercial",
                    Title = "Commercial",
                    Description = "Commercial",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 8,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Model Information",
                    Title = "Model Information",
                    Description = "Model Information",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 9,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "SYNC Training Tampa FL",
                    Title = "SYNC Training Tampa FL",
                    Description = "SYNC Training Tampa FL",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 10,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Over 30 MPG",
                    Title = "Over 30 MPG",
                    Description = "Over 30 MPG",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 11,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                },
                new Navigation
                {
                    Name = "Calculate Your Trade",
                    Title = "Calculate Your Trade",
                    Description = "Calculate Your Trade",
                    ActionLink = "#",
                    Type = "leftNav",
                    Orientation = "vertical",
                    DisplayOrderId = displayorder + 12,
                    IsActive = true,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                }
                );
        }

        public void CreatePageMetaData(ServicePageDbContext context)
        {
            var dealer = context.Dealers.First();
            context.PageMetaDatas.AddOrUpdate(c => c.PageMetaDataId,
                new PageMetaData { PageMetaDataId = 1, DealerID = 1, PageName = "~/default.aspx", Title = "New and Used Car Dealer in Pottsville, PA", ParentLink = " /", AmpLink = "", Description = "Visit Bob Weaver Auto for a variety of new and used cars in the greater Pottsville area or call at (844) 242-2972.", CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
            );
            context.SaveChanges();
        }
        #endregion

        #region Vehicle Infor Page
        public void CreateVehicleInfoPage()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.VehicleInfoPages.AddOrUpdate(
                      new VehicleInfoPage { VehicleInfoPageId = 1, FullNamePlaceHolder = "Full Name", PhonePlaceHolder = "Phone", EmailPlaceHolder = "Email", MessagePlaceHolder = "Message", TitlePlaceHolder = "Check Availability", ButtonPlaceHolder = "SEND", DealerLogo = "https://agiledealer.blob.core.windows.net/generic/dealership/brighton-ford-co-logo.jpg", ExpectInformation = "Thank you for contacting us.  We will be in touch shortly.", DealerID = 1, IsNew = true }
                    , new VehicleInfoPage { VehicleInfoPageId = 2, FullNamePlaceHolder = "Full Name", PhonePlaceHolder = "Phone", EmailPlaceHolder = "Email", MessagePlaceHolder = "Message", TitlePlaceHolder = "Check Availability", ButtonPlaceHolder = "SEND", DealerLogo = "https://agiledealer.blob.core.windows.net/generic/dealership/brighton-ford-co-logo.jpg", ExpectInformation = "Thank you for contacting us.  We will be in touch shortly.", DealerID = 1, IsNew = false }
                );

                db.SaveChanges();
            }
        }
        #endregion

        #region Applicants

        public void CreateMasterDataForApplicant()
        {
            CreateNotification();
            CreateApplicantType();
            CreateState();
            CreateResidenceStatus();
            CreateYear();
            CreateMonth();
            CreateDate();
        }

        public void CreateApplicantType()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.ApplicantTypes.AddOrUpdate(
                    new ApplicantType { ApplicantTypeId = 1, Name = "Finance", Description = "Finance" },
                    new ApplicantType { ApplicantTypeId = 2, Name = "Not Sure", Description = "Not Sure" },
                    new ApplicantType { ApplicantTypeId = 3, Name = "Lease", Description = "Lease" }
                    );
                db.SaveChanges();
            }
        }

        public void CreateNotification()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.Notifications.AddOrUpdate(
                    new Notification { NotificationId = 1, Title = "Easy", Content = "We will work with you no matter your credit situation. Complete the secure online application, and our specialists will pair you with a plan at all credit levels.", Class = "panel-info", Glyphicon = "glyphicon-thumbs-up", PageIndex = 1,DealerID=1 },
                    new Notification { NotificationId = 2, Title = "Private", Content = "Your information is private and protected. We do not share or sell your personal information to third parties. Our staff will use it only for the purpose of facilitating financing.", Class = "panel-success", Glyphicon = "glyphicon-lock", PageIndex = 1, DealerID = 1 },
                    new Notification { NotificationId = 3, Title = "Secure", Content = "Our entire website uses Secure Socket Layer (SSL) to create a secure connection from your browser to us. This link ensures that your information remains safe and secure.", Class = "panel-warning", Glyphicon = "glyphicon-certificate", PageIndex = 1, DealerID = 1 },
                    new Notification { NotificationId = 4, Title = "Easy", Content = "We will work with you no matter your credit situation. Complete the secure online application, and our specialists will pair you with a plan at all credit levels.", Class = "panel-info", Glyphicon = "glyphicon-thumbs-up", PageIndex = 2, DealerID = 1 },
                    new Notification { NotificationId = 5, Title = "Private", Content = "Your information is private and protected. We do not share or sell your personal information to third parties. Our staff will use it only for the purpose of facilitating financing.", Class = "panel-success", Glyphicon = "glyphicon-lock", PageIndex = 2, DealerID = 1 },
                    new Notification { NotificationId = 6, Title = "Secure", Content = "Our entire website uses Secure Socket Layer (SSL) to create a secure connection from your browser to us. This link ensures that your information remains safe and secure.", Class = "panel-warning", Glyphicon = "glyphicon-certificate", PageIndex = 2, DealerID = 1 },
                    new Notification { NotificationId = 7, Title = "Need Single Application?", Content = "OOPS! If you meant for this to be a single applicant request, you can still:<button type='submit' class='btn btn-default'>Go Back</button>", Class = "panel-danger", Glyphicon = "glyphicon-question-sign", PageIndex = 3, DealerID = 1 },
                    new Notification { NotificationId = 8, Title = "Thank you for joining with us, your application is being processed!", Content = "Your information is private and protected. We do not share or sell your personal information to third parties. Our staff will use it only for the purpose of facilitating financing.", Class = "panel-success", Glyphicon = "glyphicon-lock", PageIndex = 4, DealerID = 1 }
                    );
                db.SaveChanges();
            }
        }

        public void CreateState()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.States.AddOrUpdate(
                    new State { StateId = 1, Name = "Florida", Description = "Florida", Code = "FL", IsActive = true, IsDisabled = false,DealerID = 1 },
                    new State { StateId = 2, Name = "", Description = "", Code = "", IsActive = true, IsDisabled = true, DealerID = 1 },
                    new State { StateId = 3, Name = "Alaska", Description = "Alaska", Code = "AK", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 4, Name = "Alabama", Description = "Alabama", Code = "AL", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 5, Name = "Arkansas", Description = "Arkansas", Code = "AR", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 6, Name = "Arizona", Description = "Arizona", Code = "AZ", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 7, Name = "California", Description = "California", Code = "CA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 8, Name = "Colorado", Description = "Colorado", Code = "CO", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 9, Name = "Connecticut", Description = "Connecticut", Code = "CT", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 10, Name = "District of Co", Description = "District of Co", Code = "DC", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 11, Name = "Delaware", Description = "Delaware", Code = "DE", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 12, Name = "Florida", Description = "Florida", Code = "FL", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 13, Name = "Georgia", Description = "Georgia", Code = "GA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 14, Name = "Hawaii", Description = "Hawaii", Code = "HI", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 15, Name = "Iowa", Description = "Iowa", Code = "IA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 16, Name = "Idaho", Description = "Idaho", Code = "ID", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 17, Name = "Illinois", Description = "Illinois", Code = "IL", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 18, Name = "Indiana", Description = "Indiana", Code = "IN", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 19, Name = "Kansas", Description = "Kansas", Code = "KS", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 20, Name = "Kentucky", Description = "Kentucky", Code = "KY", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 21, Name = "Louisiana", Description = "Louisiana", Code = "LA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 22, Name = "Massachusetts", Description = "Massachusetts", Code = "MA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 23, Name = "Maryland", Description = "Maryland", Code = "MD", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 24, Name = "Maine", Description = "Maine", Code = "ME", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 25, Name = "Michigan", Description = "Michigan", Code = "MI", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 26, Name = "Minnesota", Description = "Minnesota", Code = "MN", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 27, Name = "Missouri", Description = "Missouri", Code = "MO", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 28, Name = "Mississippi", Description = "Mississippi", Code = "MS", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 29, Name = "Montana", Description = "Montana", Code = "MT", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 30, Name = "North Carolina", Description = "North Carolina", Code = "NC", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 31, Name = "North Dakota", Description = "North Dakota", Code = "ND", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 32, Name = "Nebraska", Description = "Nebraska", Code = "NE", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 33, Name = "New Hampshire", Description = "New Hampshire", Code = "NH", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 34, Name = "New Jersey", Description = "New Jersey", Code = "NJ", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 35, Name = "New Mexico", Description = "New Mexico", Code = "NM", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 36, Name = "Nevada", Description = "Nevada", Code = "NV", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 37, Name = "New York", Description = "New York", Code = "NY", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 38, Name = "Ohio", Description = "Ohio", Code = "OH", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 39, Name = "Oklahoma", Description = "Oklahoma", Code = "OK", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 40, Name = "Oregon", Description = "Oregon", Code = "OR", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 41, Name = "Pennsylvania", Description = "Pennsylvania", Code = "PA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 42, Name = "Puerto Rico", Description = "Puerto Rico", Code = "PR", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 43, Name = "Rhode Island", Description = "Rhode Island", Code = "RI", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 44, Name = "South Carolina", Description = "South Carolina", Code = "SC", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 45, Name = "South Dakota", Description = "South Dakota", Code = "SD", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 46, Name = "Tennessee", Description = "Tennessee", Code = "TN", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 47, Name = "Texas", Description = "Texas", Code = "TX", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 48, Name = "Utah", Description = "Utah", Code = "UT", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 49, Name = "Virginia", Description = "Virginia", Code = "VA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 50, Name = "Vermont", Description = "Vermont", Code = "VT", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 51, Name = "Washington", Description = "Washington", Code = "WA", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 52, Name = "Wisconsin", Description = "Wisconsin", Code = "WI", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 53, Name = "West Virginia", Description = "West Virginia", Code = "WV", IsActive = true, IsDisabled = false, DealerID = 1 },
                    new State { StateId = 54, Name = "Wyoming", Description = "Wyoming", Code = "WY", IsActive = true, IsDisabled = false, DealerID = 1 }
                    );
                db.SaveChanges();
            }
        }

        public void CreateResidenceStatus()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.ResidenceStatuses.AddOrUpdate(
                    new ResidenceStatus { ResidenceStatusId = 1, Name = "Own, all mortgages paid", Description = "Own, all mortgages paid", IsDisabled = false, IsActive = true,DealerID=1 },
                    new ResidenceStatus { ResidenceStatusId = 2, Name = "Own, have a mortgage", Description = "Own, have a mortgage", IsDisabled = false, IsActive = true, DealerID = 1 },
                    new ResidenceStatus { ResidenceStatusId = 3, Name = "Renting/Leasing", Description = "Renting/Leasing", IsDisabled = false, IsActive = true, DealerID = 1 },
                    new ResidenceStatus { ResidenceStatusId = 4, Name = "Living w/family (no rent)", Description = "Living w/family (no rent)", IsDisabled = false, IsActive = true, DealerID = 1 },
                    new ResidenceStatus { ResidenceStatusId = 5, Name = "Living w/family (paying rent)", Description = "Living w/family (paying rent)", IsDisabled = false, IsActive = true, DealerID = 1 }
                    );
                db.SaveChanges();
            }

        }

        public void CreateYear()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> yearList = Enumerable.Range(1920, DateTime.Now.Year - 1920 + 1).OrderByDescending(x => x).ToList();
                foreach (int year in yearList)
                {
                    db.Years.AddOrUpdate(new Year { YearId = year, Name = year.ToString(), Description = year.ToString(), IsDisabled = false, IsActive = true });
                }
                db.SaveChanges();
            }
        }

        public void CreateMonth()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.Months.AddOrUpdate(
                    new Month { MonthId = 1, Name = "January", Description = "January", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 2, Name = "February", Description = "February", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 3, Name = "March", Description = "March", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 4, Name = "April", Description = "April", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 5, Name = "May", Description = "May", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 6, Name = "June", Description = "June", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 7, Name = "July", Description = "July", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 8, Name = "August", Description = "August", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 9, Name = "September", Description = "September", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 10, Name = "October", Description = "October", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 11, Name = "November", Description = "November", IsActive = true, IsDisabled = false },
                    new Month { MonthId = 12, Name = "December", Description = "December", IsActive = true, IsDisabled = false }
                    );
                db.SaveChanges();
            }
        }

        public void CreateDate()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> dateList = Enumerable.Range(1, 31).ToList();
                foreach (int date in dateList)
                {
                    db.Dates.AddOrUpdate(new Date { DateId = date, Name = date.ToString(), Description = date.ToString(), IsDisabled = false, IsActive = true });
                }
                db.SaveChanges();
            }
        }

        #endregion

        #region Model

        #endregion

        #region Engine



        #endregion

        #region Schedule Services
        public void CreateServiceRequested()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.ServiceRequesteds.AddOrUpdate(
                    new ServiceRequested { ServiceRequestedId = 1, Name = "Routine Maintenance", Description = "Routine Maintenance", IsActive = true },
                    new ServiceRequested { ServiceRequestedId = 2, Name = "Vehicle Check-Up", Description = "Vehicle Check-Up" },
                    new ServiceRequested { ServiceRequestedId = 3, Name = "Oil Change", Description = "Oil Change" },
                    new ServiceRequested { ServiceRequestedId = 4, Name = "Tire Service", Description = "Tire Service" },
                    new ServiceRequested { ServiceRequestedId = 5, Name = "Brake Service", Description = "Brake Service" },
                    new ServiceRequested { ServiceRequestedId = 6, Name = "Battery Service", Description = "Battery Service" },
                    new ServiceRequested { ServiceRequestedId = 7, Name = "Engine Service", Description = "Engine Service" },
                    new ServiceRequested { ServiceRequestedId = 8, Name = "Transmission Service", Description = "Transmission Service" },
                    new ServiceRequested { ServiceRequestedId = 9, Name = "Other", Description = "Other" }
                    );
                db.SaveChanges();
            }
        }

        public void CreateCurrentIssue()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.CurrentIssues.AddOrUpdate(
                    new CurrentIssue { CurrentIssueId = 1, Name = "Service Engine Light", Description = "Service Engine Light", IsActive = true },
                    new CurrentIssue { CurrentIssueId = 2, Name = "Low Fluids", Description = "Low Fluids" },
                    new CurrentIssue { CurrentIssueId = 3, Name = "Wipers Needed", Description = "Wipers Needed" },
                    new CurrentIssue { CurrentIssueId = 4, Name = "Electrical Problems", Description = "Electrical Problems" },
                    new CurrentIssue { CurrentIssueId = 5, Name = "Brake Noise", Description = "Brake Noise" },
                    new CurrentIssue { CurrentIssueId = 6, Name = "Engine Noise", Description = "Engine Noise" },
                    new CurrentIssue { CurrentIssueId = 7, Name = "Transmission Noise", Description = "Transmission Noise" },
                    new CurrentIssue { CurrentIssueId = 8, Name = "Other", Description = "Other" }
                    );
                db.SaveChanges();
            }
        }

        public void CreateScheduleTime()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.ScheduleTimes.AddOrUpdate(
                    //Weekly
                    new ScheduleTime { ScheduleTimeId = 1, Hour = "9:00 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 2, Hour = "9:30 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 3, Hour = "10:00 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 4, Hour = "10:30 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 5, Hour = "11:00 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 6, Hour = "11:30 am", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 7, Hour = "Noon", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 8, Hour = "12:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 9, Hour = "1:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 10, Hour = "1:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 11, Hour = "2:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 12, Hour = "2:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 13, Hour = "3:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 14, Hour = "3:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 15, Hour = "4:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 16, Hour = "4:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 17, Hour = "5:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 18, Hour = "5:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 19, Hour = "6:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 20, Hour = "6:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 21, Hour = "7:00 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 22, Hour = "7:30 pm", IsActive = true, IsSaturday = false },
                    new ScheduleTime { ScheduleTimeId = 23, Hour = "8:00 pm", IsActive = true, IsSaturday = false },


                    //Saturday
                    new ScheduleTime { ScheduleTimeId = 24, Hour = "9:00 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 25, Hour = "9:30 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 26, Hour = "10:00 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 27, Hour = "10:30 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 28, Hour = "11:00 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 29, Hour = "11:30 am", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 30, Hour = "Noon", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 31, Hour = "12:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 32, Hour = "1:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 33, Hour = "1:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 34, Hour = "2:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 35, Hour = "2:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 36, Hour = "3:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 37, Hour = "3:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 38, Hour = "4:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 39, Hour = "4:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 40, Hour = "5:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 41, Hour = "5:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 42, Hour = "6:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 43, Hour = "6:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 44, Hour = "7:00 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 45, Hour = "7:30 pm", IsActive = true, IsSaturday = true },
                    new ScheduleTime { ScheduleTimeId = 46, Hour = "8:00 pm", IsActive = true, IsSaturday = true }

                    );
                db.SaveChanges();
            }
        }

        public void CreateServiceSpecial()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var dealer = db.Dealers.FirstOrDefault();
                if (dealer != null)
                    db.DealerServiceSpecials.AddOrUpdate(
                        new DealerServiceSpecial { DealerServiceSpecialId = 1, DealerID = dealer.DealerID, ServiceSpecialsImage = "", Title = "", Description = "", Order = 1 }
                        );
                db.SaveChanges();
            }
        }

        #endregion

        #region Period Type
        public void CreatePeriodType()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.PeriodTypes.AddOrUpdate(
                    new PeriodType { PeriodTypeId = 1, PeriodTypeName = "12 Months", TermInMonths = 12 },
                    new PeriodType { PeriodTypeId = 2, PeriodTypeName = "24 Months", TermInMonths = 24 },
                    new PeriodType { PeriodTypeId = 3, PeriodTypeName = "36 Months", TermInMonths = 36 },
                    new PeriodType { PeriodTypeId = 4, PeriodTypeName = "48 Months", TermInMonths = 48 },
                    new PeriodType { PeriodTypeId = 5, PeriodTypeName = "60 Months", TermInMonths = 60 },
                    new PeriodType { PeriodTypeId = 6, PeriodTypeName = "72 Months", TermInMonths = 72 }
                    );
                db.SaveChanges();
            }
        }
        #endregion

        #region VCP
        public void CreateFordVehicleContent()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.FordVehicleContents.AddOrUpdate(

                );
                db.SaveChanges();
            }
        }
        #endregion

        #region Account
        public void CreateRoles(ServicePageDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            List<string> roleList = new List<string>() { "Dealer", "Admin", "User" };
            foreach (string role in roleList)
            {
                if (!roleManager.RoleExists(role))
                {
                    roleManager.Create(new IdentityRole(role));
                }
            }
        }

        #endregion

        #region Blog
        public void CreateBlogEntries(ServicePageDbContext context)
        {
            var db = context;
            db.BlogEntrys.RemoveRange(db.BlogEntrys);
            db.BlogTags.RemoveRange(db.BlogTags);
            db.BlogCategoryies.RemoveRange(db.BlogCategoryies);
            db.BlogEntries_BlogCategories.RemoveRange(db.BlogEntries_BlogCategories);
            db.BlogEntries_BlogTags.RemoveRange(db.BlogEntries_BlogTags);
            db.SaveChanges();

            db.BlogEntrys.AddOrUpdate(

                                    );

            db.BlogCategoryies.AddOrUpdate(
              new BlogCategory() { BlogCategoryId = 1, CategoryText = "Review", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID=1 },
            new BlogCategory() { BlogCategoryId = 2, CategoryText = "Comparison", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogCategory() { BlogCategoryId = 3, CategoryText = "Used Buying Guide", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogCategory() { BlogCategoryId = 4, CategoryText = "Upgrade", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogCategory() { BlogCategoryId = 5, CategoryText = "Blog Post", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }

            );

            db.BlogTags.AddOrUpdate(
            new BlogTag() { BlogTagId = 1, TagText = "BaseModel", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID=1 },
            new BlogTag() { BlogTagId = 2, TagText = "BaseModelUsed", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 3, TagText = "2014", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 4, TagText = "2015", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 5, TagText = "2016", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 6, TagText = "2017", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 7, TagText = "Used", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 8, TagText = "New", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 9, TagText = "Used", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 10, TagText = "Car", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 11, TagText = "SUV", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 12, TagText = "Truck", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 13, TagText = "Van", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 14, TagText = "Ford", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 15, TagText = "GMC", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 16, TagText = "E-350", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 17, TagText = "E-Series", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 18, TagText = "E-Series Wagon", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 19, TagText = "Edge", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 20, TagText = "Escape", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 21, TagText = "Expedition", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 22, TagText = "Explorer", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 23, TagText = "F-150", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 24, TagText = "F-150 King Ranch", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 25, TagText = "F-150 SuperCab", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 26, TagText = "F-150 SuperCrew", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 27, TagText = "F-250", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 28, TagText = "F-250 ", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 29, TagText = "F-250 SuperCab", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 30, TagText = "F-250 SuperCrew", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 31, TagText = "F-350", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 32, TagText = "F-450", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 33, TagText = "F-550", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 34, TagText = "F-650", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 35, TagText = "F-750", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 36, TagText = "Fiesta", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 37, TagText = "Flex", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 38, TagText = "Focus", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 39, TagText = "Fusion", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 40, TagText = "Mustang", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 41, TagText = "Super Duty", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 42, TagText = "Super Duty F-250", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 43, TagText = "Taurus", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 44, TagText = "Transit", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 45, TagText = "Transit Connect", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 46, TagText = "Transit-250", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 47, TagText = "Yukon", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 48, TagText = "2018", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
            new BlogTag() { BlogTagId = 49, TagText = "2019", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
            );
            db.BlogEntries_BlogCategories.AddOrUpdate(

                );
            db.BlogEntries_BlogTags.AddOrUpdate(

                );

            SaveChange(db);
        }
        #endregion

        #region ContactUs
        public void Create_ContactUs_Hours(ServicePageDbContext db)
        {
            var dealer = db.Dealers.First();

            var hourTypeService = 1;
            var hourTypeSale = 2;
            var hourTypePart = 3;

            var hourTypeServiceShort = 4;
            var hourTypeSaleShort = 5;
            var hourTypePartShort = 6;

            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypeSale, Name = "Sale", Order = 1, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });
            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypeService, Name = "Service", Order = 2, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });
            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypePart, Name = "Parts", Order = 3, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });

            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypeServiceShort, Name = "ServiceShort", Order = 2, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });
            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypeSaleShort, Name = "SaleShort", Order = 1, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });
            db.HourTypes.AddOrUpdate(x => x.HourTypeID, new HourType { HourTypeID = hourTypePartShort, Name = "PartShort", Order = 3, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now });

            db.Hours.AddOrUpdate(x => x.HoursID,
                  //Normal
                  new Hours { HoursID = 1, DealerID = 1, LongDayName = "", ShortDayName = "Mon", Order = 0, StartTime = "9:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 2, DealerID = 1, LongDayName = "", ShortDayName = "Tues", Order = 1, StartTime = "9:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 3, DealerID = 1, LongDayName = "", ShortDayName = "Wed", Order = 2, StartTime = "9:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 4, DealerID = 1, LongDayName = "", ShortDayName = "Thurs", Order = 3, StartTime = "9:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 5, DealerID = 1, LongDayName = "", ShortDayName = "Fri", Order = 4, StartTime = "9:00 AM", EndTime = "6:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 6, DealerID = 1, LongDayName = "", ShortDayName = "Sat", Order = 5, StartTime = "9:00 AM", EndTime = "3:00 PM", OperationStatus = null, HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 7, DealerID = 1, LongDayName = "", ShortDayName = "Sun", Order = 6, StartTime = "Closed", EndTime = "", OperationStatus = "Closed", HourTypeID = hourTypeSale, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }

                , new Hours { HoursID = 8, DealerID = 1, LongDayName = "", ShortDayName = "Mon", Order = 7, StartTime = "7:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 9, DealerID = 1, LongDayName = "", ShortDayName = "Tues", Order = 8, StartTime = "7:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 10, DealerID = 1, LongDayName = "", ShortDayName = "Wed", Order = 9, StartTime = "7:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 11, DealerID = 1, LongDayName = "", ShortDayName = "Thurs", Order = 10, StartTime = "7:00 AM", EndTime = "8:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 12, DealerID = 1, LongDayName = "", ShortDayName = "Fri", Order = 11, StartTime = "7:00 AM", EndTime = "6:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 13, DealerID = 1, LongDayName = "", ShortDayName = "Sat", Order = 12, StartTime = "9:00 AM", EndTime = "3:00 PM", OperationStatus = null, HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now }
                , new Hours { HoursID = 14, DealerID = 1, LongDayName = "", ShortDayName = "Sun", Order = 13, StartTime = "Closed", EndTime = "", OperationStatus = "Closed", HourTypeID = hourTypeService, CreatedID = "", CreatedDt = DateTime.Now, ModifiedID = "", ModifiedDt = DateTime.Now },


               // Short
                new Hours { HoursID = 15, HourTypeID = hourTypeSaleShort, Order = 1, ShortDayName = "Mon-Thu", StartTime = "9:00 AM", EndTime = "8:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new Hours { HoursID = 16, HourTypeID = hourTypeSaleShort, Order = 2, ShortDayName = "Fri", StartTime = "9:00 AM", EndTime = "6:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new Hours { HoursID = 17, HourTypeID = hourTypeSaleShort, Order = 3, ShortDayName = "Sat", StartTime = "9:00 AM", EndTime = "3:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new Hours { HoursID = 18, HourTypeID = hourTypeSaleShort, Order = 4, ShortDayName = "Sun", StartTime = "Closed", EndTime = "", OperationStatus = "Closed", DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },


                new Hours { HoursID = 19, HourTypeID = hourTypeServiceShort, Order = 1, ShortDayName = "Mon-Thu", StartTime = "7:00 AM", EndTime = "8:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                 new Hours { HoursID = 20, HourTypeID = hourTypeServiceShort, Order = 2, ShortDayName = "Fri", StartTime = "7:00 AM", EndTime = "6:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new Hours { HoursID = 21, HourTypeID = hourTypeServiceShort, Order = 3, ShortDayName = "Sat", StartTime = "9:00 AM", EndTime = "3:00 PM", OperationStatus = null, DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now },
                new Hours { HoursID = 22, HourTypeID = hourTypeServiceShort, Order = 4, ShortDayName = "Sun", StartTime = "", EndTime = "", OperationStatus = "Closed", DealerID = dealer.DealerID, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }

                );

            db.SaveChanges();
        }
        #endregion

        #region About Us
        public void CreateAboutUs(ServicePageDbContext context, Dealer dealer)
        {
            var db = context;

            db.AboutUses.AddOrUpdate(
                new AboutUs
                {
                    AboutUsId = 1,
                    DealerID = 1,
                    Content = "<div class='col-lg-9 col-md-9 col-sm-12 col-xs-12'></div>",
                    Title = "Bob Weaver Auto",
                    CreatedID = "",
                    CreatedDt = DateTime.Now,
                    ModifiedID = "",
                    ModifiedDt = DateTime.Now
                }
            );
            db.SaveChanges();
        }
        #endregion

        #region Employee
        public void CreateEmployee(ServicePageDbContext context)
        {
            var db = context;
            context.Departments.AddOrUpdate(
                new Department
                {
                    DepartmentId = 1,
                    Description = "Commercial",
                    Active = true,
                    CreatedDt = DateTime.Now,
                    CreatedID = "1",
                    ModifiedDt = DateTime.Now,
                    Name = "Commercial",
                    Order = 1,
                },
                new Department
                {
                    DepartmentId = 2,
                    Description = "Sale",
                    Active = true,
                    CreatedDt = DateTime.Now,
                    CreatedID = "2",
                    ModifiedDt = DateTime.Now,
                    Name = "Sale",
                    Order = 2,
                },
                new Department
                {
                    DepartmentId = 3,
                    Description = "Service",
                    Active = true,
                    CreatedDt = DateTime.Now,
                    CreatedID = "3",
                    ModifiedDt = DateTime.Now,
                    Name = "Service",
                    Order = 3,
                },
                new Department
                {
                    DepartmentId = 4,
                    Description = "Finance & Marketing",
                    Active = true,
                    CreatedDt = DateTime.Now,
                    CreatedID = "4",
                    ModifiedDt = DateTime.Now,
                    Name = "Finance & Marketing",
                    Order = 4,
                },
                new Department
                {
                    DepartmentId = 5,
                    Description = "General",
                    Active = true,
                    CreatedDt = DateTime.Now,
                    CreatedID = "5",
                    ModifiedDt = DateTime.Now,
                    Name = "General",
                    Order = 5,
                }
            );

            //context.Employees.AddOrUpdate(x => x.EmployeeId
            //     , new Employee { EmployeeId = 1, IsDeleted = false, DealerID = 1, FirstName = "Richard ", LastName = "Molden", Bio = "A Gator Ford employee since 2000, Richard’s favorite Ford is the F150. He is also the self-proclaimed #1 fan of the Tampa Bay Buccaneers.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/richard-molden.jpg", OfficePhone = "813-980-3673 ", Ext = "180", CellPhone = "813-980-3673 ", Email = "rmolden@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 2, IsDeleted = false, DealerID = 1, FirstName = "Brandon", LastName = "Selig", Bio = "Brandon joined Gator Ford in 2014. A fan of F250 diesel platinum trucks, Brandon also enjoys golf, riding jet skis and football.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/brandon-selig.jpg", OfficePhone = "813-980-3673", Ext = "111", CellPhone = "813-980-3674", Email = "bselig@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 3, IsDeleted = false, DealerID = 1, FirstName = "John", LastName = "Stevens", Bio = "John has proudly served as a member of the National Guard since 1998 and joined Gator Ford in 2004. His favorite Ford is the F150 truck.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/john-stevens.jpg", OfficePhone = "813-980-3673", Ext = "150", CellPhone = "813-980-3675", Email = "jstevens@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 4, IsDeleted = false, DealerID = 1, FirstName = "Pat", LastName = "Cooper", Bio = "A fan of Ford trucks, Pat joined Gator Ford in 2009 with a wealth of automotive knowledge. In his free time, Pat likes to golf and enjoys playing steel darts.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/pat-cooper.jpg", OfficePhone = "813-980-3673", Ext = "181", CellPhone = "813-980-3676", Email = "pcooper@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 5, IsDeleted = false, DealerID = 1, FirstName = "Jeff", LastName = "Meyers", Bio = "Jeff joined Gator Ford in 2012 and enjoys the team environment among his co-workers. Away from work, Jeff can be found playing pool and spending time with his six kids.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/jeff-meyers.jpg", OfficePhone = "813-980-3673", Ext = "182", CellPhone = "813-980-3677", Email = "jmeyers@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 6, IsDeleted = false, DealerID = 1, FirstName = "Hank", LastName = "Allen", Bio = "Originally from Detroit, Hank prides himself on being from a Ford family. He joined Gator in 2014 and spends his free time walking and spoiling his granddaughter.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/hank-allen.jpg", OfficePhone = "813-980-3673", Ext = "140", CellPhone = "813-980-3678", Email = "hallen@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 7, IsDeleted = false, DealerID = 1, FirstName = "Danny", LastName = "DeGiglio", Bio = "A car business veteran of over 25 years, Danny joined Gator Ford in 2010 and was named Salesman of the Year for four straight years. Danny enjoys all sports, especially baseball and football.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/danny-degiglio.jpg", OfficePhone = "813-980-3673", Ext = "161", CellPhone = "813-980-3679", Email = "ddegiglio@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 8, IsDeleted = false, DealerID = 1, FirstName = "Frankie", LastName = "Morales", Bio = "Frankie joined Gator in 2010 and is a truck expert, especially the F150. When he’s not talking trucks, Frankie enjoys reading and spending time with his family.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/frankie-morales.jpg", OfficePhone = "813-980-3673", Ext = "103", CellPhone = "813-980-3680", Email = "fmorales@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 9, IsDeleted = false, DealerID = 1, FirstName = "Angela", LastName = "Traina", Bio = "Angela joined Gator Ford in 2014. She’s an F250 fan and a true horsepower expert, since she’s a horseback rider and has three horses of her own.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/agela-traina.jpg", OfficePhone = "813-980-3673", Ext = "167", CellPhone = "813-980-3681", Email = "atraina@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 10, IsDeleted = false, DealerID = 1, FirstName = "Preston", LastName = "Odom", Bio = "Preston lists the F250 Platinum as his favorite truck, and he’s been selling them since 2016. When not at Gator Ford he enjoys fishing and is a big hockey fan.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/preston_odom.jpg", OfficePhone = "813-980-3673", Ext = "158", CellPhone = "813-980-3682", Email = "podum@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 11, IsDeleted = false, DealerID = 1, FirstName = "Ray ", LastName = "Owens", Bio = "Ray has been selling Fords at Gator since 2015, including fleet sales. Ray is an Army Special Forces veteran, a world traveler and spends his free time golfing.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/ray-owens.jpg", OfficePhone = "813-980-3673", Ext = "142", CellPhone = "813-980-3683", Email = "rowens@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 12, IsDeleted = false, DealerID = 1, FirstName = "Tank", LastName = "Sherman", Bio = "Tank joined Gator in 2015 with over 30 years of car and truck experience, and has been Master Certified longer than anyone in the southeastern U.S.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/tank-sherman.jpg", OfficePhone = "813-980-3673", Ext = "123", CellPhone = "813-980-3684", Email = "tsherman@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 13, IsDeleted = false, DealerID = 1, FirstName = "Arnold", LastName = "Little", Bio = "Arnold likes talking trucks, especially the F150. He joined Gator Ford in 2015 and spends his free time outdoors, grilling and fishing with his son.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/arnold-little.jpg", OfficePhone = "813-980-3673", Ext = "145", CellPhone = "813-980-3685", Email = "alittle@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 14, IsDeleted = false, DealerID = 1, FirstName = "Wade", LastName = "Patterson", Bio = "Wade is fond of F250 diesel trucks, and he’s been at Gator since 2015. He also enjoys boating, clay shooting and his charity work with the Shriners.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/wade-patterson.jpg", OfficePhone = "813-980-3673", Ext = "104", CellPhone = "813-980-3686", Email = "wpatterson@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 15, IsDeleted = false, DealerID = 1, FirstName = "Juan", LastName = "Castellanos", Bio = "Juan joined Gator in 2016 and loves showing customers his favorite F150’s. Away from work he plays baseball and slow-pitch softball and enjoys fantasy football.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/juan-castellanos.jpg", OfficePhone = "813-980-3673", Ext = "107", CellPhone = "813-980-3687", Email = "jcastellanos@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 16, IsDeleted = true, DealerID = 1, FirstName = "Daniel", LastName = "Cavallo", Bio = "Daniel has been assisting our online shoppers since joining Gator in 2014. He keeps busy outside of work with abstract painting, cooking and fire spinning.", Photo = "", OfficePhone = "813-980-3673", Ext = "211", CellPhone = "813-980-3688", Email = "dcavallo@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 17, IsDeleted = false, DealerID = 1, FirstName = "Juan", LastName = "Silva", Bio = "Juan has been with Gator since 2015 and is a fan of the Ford Mustang.  Away from work he enjoys both playing and watching basketball and football.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/juan-silva.jpg", OfficePhone = "813-980-3673", Ext = "144", CellPhone = "813-980-3689", Email = "jsilva@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 18, IsDeleted = false, DealerID = 1, FirstName = "K.C.", LastName = "Crothers", Bio = "Since joining Gator in 2001, K.C. has assisted many business owners with their truck purchases. An avid sports enthusiast, you can find K.C. cheering on his favorite teams all year long.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/k-c-crothers.jpg", OfficePhone = "813-980-3673", Ext = "106", CellPhone = "813-980-3690", Email = "k.ccrothers@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 19, IsDeleted = false, DealerID = 1, FirstName = "Don", LastName = "Fisch", Bio = "Don is a truck guy who joined Gator Ford in 2014. When not selling trucks, he is also a scuba instructor and enjoys target and clay shooting", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/don-fisch.jpg", OfficePhone = "813-980-3673", Ext = "114", CellPhone = "813-980-3691", Email = "dfisch@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 20, IsDeleted = false, DealerID = 1, FirstName = "Tom", LastName = "Armiger", Bio = "Tom joined Gator in 2002 and truly enjoys his customers and sales team. In his free time, Tom can be found cheering on the Bucs and spending time with family.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/tom-armiger.jpg", OfficePhone = "813-980-3673", Ext = "110", CellPhone = "813-980-3692", Email = "tarmiger@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 21, IsDeleted = false, DealerID = 1, FirstName = "Bryan", LastName = "Farley", Bio = "A Gator salesman since 2014, Bryan’s dream truck is the Black Ops F150. Away from work he enjoys the beach and photography.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/bryan-farley.jpg", OfficePhone = "813-980-3673", Ext = "120", CellPhone = "813-980-3693", Email = "bfarley@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 22, IsDeleted = false, DealerID = 1, FirstName = "Tim ", LastName = "O'Shea", Bio = "Tim has been with Gator since 2010 and is very knowledgeable about all our Ford trucks. He can also be found fishing, grilling and enjoying the outdoors.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/tim-oshea.jpg", OfficePhone = "813-980-3673", Ext = "112", CellPhone = "813-980-3694", Email = "to'shea@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 23, IsDeleted = false, DealerID = 1, FirstName = "Steve", LastName = "Jenkins", Bio = "Steve brought his truck knowledge to Gator Ford in 2015. A fan of the F250 4x4’s, Steve also enjoys motocross and riding dirt bikes.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/steve-jenkins.jpg", OfficePhone = "813-980-3673", Ext = "168", CellPhone = "813-980-3695", Email = "sjenkins@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 24, IsDeleted = false, DealerID = 1, FirstName = "Ron", LastName = "Melvin", Bio = "Ron has been with Gator Ford since 2001 and is a big fan of Ford’s Super Duty trucks. When he’s not at the dealership, Ron enjoys the outdoors, fishing and the beach.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/ron_melvin.jpg", OfficePhone = "813-980-3673", Ext = "141", CellPhone = "813-980-3696", Email = "rmelvin@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 25, IsDeleted = false, DealerID = 1, FirstName = "Mark", LastName = "Nikolich", Bio = "Mark joined Gator in 2012 and oversees the Parts Department. A fan of the F150 truck, Mark enjoys music, grilling and bike riding in his free time.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/mark-nikolich.jpg", OfficePhone = "813-980-3673", Ext = "178", CellPhone = "813-980-3697", Email = "mnikolich@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 26, IsDeleted = false, DealerID = 1, FirstName = "Matt", LastName = "Cullen", Bio = "Matt has been with Gator since 2007 and heads up the Service Department. The Mustang is his favorite Ford, and he enjoys biking and traveling.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/matt-cullen.jpg", OfficePhone = "813-980-3673", Ext = "153", CellPhone = "813-980-3698", Email = "mcullen@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 27, IsDeleted = false, DealerID = 1, FirstName = "Kevin", LastName = "Barker", Bio = "A fan of the Ford Lightning truck, Matt joined Gator’s service team in 2014. Outside of work he can often be found golfing and saltwater fishing.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/kevin-barker.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3699", Email = "kbarker@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 28, IsDeleted = false, DealerID = 1, FirstName = "Karen", LastName = "Sullivan", Bio = "Karen has been with Gator Ford since 2000 and really enjoys the team atmosphere at the dealership. When not at work, she likes line dancing and spending time with her grandkids.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/karen-sullivan.jpg", OfficePhone = "813-980-3673", Ext = "132", CellPhone = "813-980-3700", Email = "ksullivan@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 29, IsDeleted = false, DealerID = 1, FirstName = "Manny ", LastName = "Tovar", Bio = "Manny joined Gator in 2001. A fan of fast cars, Manny likes the Ford GT and watching NASCAR races. He also enjoys music and playing blues guitar.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/manny-tovar.jpg", OfficePhone = "813-980-3673", Ext = "162", CellPhone = "813-980-3701", Email = "mtovar@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 30, IsDeleted = false, DealerID = 1, FirstName = "Steven", LastName = "Carter", Bio = "Steven has been assisting our service customers since 2016. He enjoys F250 trucks, and spends his free time boating and fishing.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/steven-carter.jpg", OfficePhone = "813-980-3673", Ext = "164", CellPhone = "813-980-3702", Email = "scarter@gatorford.com", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }

            //   , new Employee { EmployeeId = 31, IsDeleted = false, DealerID = 1, FirstName = "Jeff", LastName = "Adams", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/jeff_adams.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 32, IsDeleted = false, DealerID = 1, FirstName = "Anthony", LastName = "Drake", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/anthony_drake.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 33, IsDeleted = false, DealerID = 1, FirstName = "June", LastName = "Rivera", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/june_rivera.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 34, IsDeleted = false, DealerID = 1, FirstName = "Mike", LastName = "Rusch", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/mike_rusch.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 35, IsDeleted = false, DealerID = 1, FirstName = "Larry", LastName = "Smith", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/larry_smith.jpg", OfficePhone = "813-980-3673", Ext = "", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //   , new Employee { EmployeeId = 36, IsDeleted = false, DealerID = 1, FirstName = "Jeff", LastName = "Rieth", Bio = "I am here to assist you with your needs. Feel free to call or email me at anytime.", Photo = "https://agiledealer.blob.core.windows.net/generic/content/staff/Jeff_Reith.jpg", OfficePhone = "813-980-3673", Ext = "267", CellPhone = "813-980-3673", Email = "", CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }

            //   );
            //db.EmployeeDepartments.RemoveRange(context.EmployeeDepartments.ToList());
            //db.SaveChanges();
            //context.EmployeeDepartments.AddOrUpdate(x => x.Id
            //    , new Employee_Department { Id = 1, EmployeeId = 1, DepartmentId = 4, Title = "Finance Director", Sequence = "1", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 2, EmployeeId = 2, DepartmentId = 4, Title = "Finance Manager", Sequence = "2", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 3, EmployeeId = 3, DepartmentId = 4, Title = "Finance Manager/Sales Trainer", Sequence = "3", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 4, EmployeeId = 4, DepartmentId = 2, Title = "Sales Manager", Sequence = "4", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 5, EmployeeId = 5, DepartmentId = 2, Title = "Sales Manager", Sequence = "5", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 6, EmployeeId = 6, DepartmentId = 2, Title = "Sales Consultant", Sequence = "6", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 7, EmployeeId = 7, DepartmentId = 2, Title = "Sales Consultant", Sequence = "7", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 8, EmployeeId = 8, DepartmentId = 2, Title = "Sales Consultant, Bilingual", Sequence = "8", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 9, EmployeeId = 9, DepartmentId = 2, Title = "Sales Consultant", Sequence = "9", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 10, EmployeeId = 10, DepartmentId = 2, Title = "Internet Consultant", Sequence = "10", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 11, EmployeeId = 11, DepartmentId = 2, Title = "Sales Consultant", Sequence = "11", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 12, EmployeeId = 12, DepartmentId = 2, Title = "Sales Consultant", Sequence = "12", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 13, EmployeeId = 13, DepartmentId = 2, Title = "Sales Consultant", Sequence = "13", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 14, EmployeeId = 14, DepartmentId = 2, Title = "Sales Consultant", Sequence = "14", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 15, EmployeeId = 15, DepartmentId = 2, Title = "Sales Consultant, Bilingual", Sequence = "15", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 16, EmployeeId = 16, DepartmentId = 2, Title = "Internet Consultant", Sequence = "16", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 17, EmployeeId = 17, DepartmentId = 2, Title = "Internet Consultant", Sequence = "17", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 18, EmployeeId = 18, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "18", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 19, EmployeeId = 19, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "19", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 20, EmployeeId = 20, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "20", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 21, EmployeeId = 21, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "21", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 22, EmployeeId = 22, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "22", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 23, EmployeeId = 23, DepartmentId = 1, Title = "Commercial Account Manager", Sequence = "23", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 24, EmployeeId = 24, DepartmentId = 5, Title = "General Manager", Sequence = "24", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 25, EmployeeId = 25, DepartmentId = 3, Title = "Parts Manager", Sequence = "25", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 26, EmployeeId = 26, DepartmentId = 3, Title = "Service Manager", Sequence = "26", IsManager = true, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 27, EmployeeId = 27, DepartmentId = 3, Title = "Service Advisor", Sequence = "27", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 28, EmployeeId = 28, DepartmentId = 3, Title = "Service Advisor", Sequence = "28", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 29, EmployeeId = 29, DepartmentId = 3, Title = "Service Advisor", Sequence = "29", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 30, EmployeeId = 30, DepartmentId = 3, Title = "Service Advisor", Sequence = "30", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }

            //    , new Employee_Department { Id = 31, EmployeeId = 31, DepartmentId = 3, Title = "Parts", Sequence = "31", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 32, EmployeeId = 32, DepartmentId = 3, Title = "Parts", Sequence = "32", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 33, EmployeeId = 33, DepartmentId = 3, Title = "Parts", Sequence = "33", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 34, EmployeeId = 34, DepartmentId = 3, Title = "Parts", Sequence = "34", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 35, EmployeeId = 35, DepartmentId = 3, Title = "Parts", Sequence = "35", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    , new Employee_Department { Id = 36, EmployeeId = 36, DepartmentId = 3, Title = "Parts", Sequence = "35", IsManager = false, CreatedID = "1", CreatedDt = DateTime.Now, ModifiedID = "1", ModifiedDt = DateTime.Now }
            //    );


            db.SaveChanges();
        }
        #endregion

        #region Service Content
        public void CreateServiceContent(ServicePageDbContext db)
        {
            var displayPage = db.DisplayPages.FirstOrDefault();
            var dealer = db.Dealers.FirstOrDefault();
            var servicePage = new ServicePage
            {
                ServicePageID = 1,
                BrakeContent = @"",
                TireContent = @"",
                DisplayPage = displayPage,
                Dealer = dealer,
                BrakeImage = "~/Content/Images/UI/brake.png"
                ,
                TireImage = "~/Content/Images/UI/tires.png"
                ,
                OilImage = "~/Content/Images/UI/oilchange.png",
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            };

            if (!db.ServicePages.Any(a => a.DealerID == dealer.DealerID))
                db.ServicePages.Add(servicePage);

            db.SaveChanges();


            //TODO: add hours for serivce type here


            var svcTypeOil = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Oil-Change", StringComparison.OrdinalIgnoreCase));
            var svcTypeBrake = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Brake", StringComparison.OrdinalIgnoreCase));
            var svcTypeTire = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Tire", StringComparison.OrdinalIgnoreCase));
            var svcList = new List<Service>();

            svcList.Add(new Service
            {
                Price = 22.99m,
                Title = "Synthetic Blend only",
                Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                Description = "Synthetic Blend only.",
                Image = @"~/Content/Images/UI/oilchange.png",
                ServiceType = svcTypeOil,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });

            svcList.Add(new Service
            {
                Price = 27.99m,
                Title = "Synthetic Oil with 27pts inspection",
                Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                Description = "Synthetic Oil with 27pts inspection",
                Image = @"~/Content/Images/UI/oilchange.png",
                ServiceType = svcTypeOil,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });

            svcList.Add(new Service
            {
                Price = 39.99m,
                Title = "Full Synthetic Oil with Tire Rotation & 27pts inspection",
                Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                Description = "Full Synthetic Oil with Tire Rotation & 27pts inspection",
                Image = @"~/Content/Images/UI/oilchange.png",
                ServiceType = svcTypeOil,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });


            svcList.Add(new Service
            {
                Price = 59.95m,
                Title = "Diesen Oil Changes",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"Tax supplies and environmental fee extra. May not be used on pervious purchases. See dealer for details. Offer expires 06/30/2016",
                Image = @"~/Content/Images/UI/oilchange.png",
                ServiceType = svcTypeOil,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            svcList.Add(new Service
            {
                Price = 22.99m,
                Title = "Standard Brake Service",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"Inspection of brake components Lifetime parts warranty on brake shoes and disc pads, 
            and a 12-month, 12,000-mile labor warranty",
                Image = @"~/Content/Images/UI/brake.png",
                ServiceType = svcTypeBrake,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            svcList.Add(new Service
            {
                Price = 27.99m,
                Title = "Standard Auto Brake Service* With fluid exchange",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"Brake fluid exchange / bleeding brakes (which removes brake fluid contaminants and air)
            Lifetime parts warranty for brake disc pads and shoes
            24,000-mile/24 month labor warranty",
                Image = @"~/Content/Images/UI/brake.png",
                ServiceType = svcTypeBrake,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            svcList.Add(new Service
            {
                Price = 39.99m,
                Title = "Lifetime Auto Brake Service",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"Lifetime warranty for brake disc pads, brake shoes, brake installation hardware, calipers and/or wheel cylinders and shoes
            Lifetime warranty on labor
            12,000-mile/12-month warranty on drums and rotors",
                Image = @"~/Content/Images/UI/brake.png",
                ServiceType = svcTypeBrake,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });


            svcList.Add(new Service
            {
                Price = 22.99m,
                Title = "Basic Tire Installation Package",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"This package includes Tire mounting, Valve Stems, Lifetime Balance and Rotation every 7,500 miles, & 50 mile lug re-torque",
                Image = @"~/Content/Images/UI/tires.png",
                ServiceType = svcTypeTire,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            svcList.Add(new Service
            {
                Price = 27.99m,
                Title = "Value Tire Installation Package including Road Hazard Protection",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"This package includes Tire mounting, Valve Stems, Lifetime Balance and Rotation every 7,500 miles, 50 mile lug re-torque, & Road Hazard Warranty.",
                Image = @"~/Content/Images/UI/tires.png",
                ServiceType = svcTypeTire,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            svcList.Add(new Service
            {
                Price = 39.99m,
                Title = "Lifetime Balance/Rotation",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = @"This package includes tire rotation and balance every 7,500 miles for the life of the tires that the package is originally purchased on. ",
                Image = @"~/Content/Images/UI/tires.png",
                ServiceType = svcTypeTire,
                Dealer = dealer,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });


            var oldsvc = db.Services.ToList();
            if (oldsvc.Count > 0)
            {
                db.Services.RemoveRange(oldsvc);
                db.SaveChanges();
            }


            if (!db.Services.Any(a => a.DealerID == dealer.DealerID))
                db.Services.AddRange(svcList);


            var spcList = new List<Special>();

            spcList.Add(new Special
            {
                Price = 10.99m,
                Title = "Discount Synthetic Oil with Tire Rotation & 27pts inspection",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = "Discount Synthetic Oil with Tire Rotation & 27pts inspection.",
                Image = @"~/Content/Images/UI/oilchange_special.png",
                ServiceType = svcTypeOil,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });


            spcList.Add(new Special
            {
                Price = 10.99m,
                Title = "Limited Time Specials",
                Disclaimer = "Prices shown on services are national pricing. Please check with your local store for local pricing that may vary.",
                Description = "Standard brake service special.",
                Image = @"~/Content/Images/UI/brake_special.png",
                ServiceType = svcTypeBrake,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
            });


            spcList.Add(new Special
            {
                Price = 299.99m,
                Title = "Buy 3 get one free",
                Disclaimer = "Limited Time Promotions Special offers, rebates and coupons on products and services!.",
                Description = "Buy 3 get one free with paid installation on select in-stock tires(goodyear, continental, coopertires, more...)",
                Image = @"~/Content/Images/UI/tires_special.png",
                ServiceType = svcTypeTire,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now

            });

            foreach (var svc in spcList)
            {
                svc.Expire = DateTime.Now.AddDays(100);
                svc.Dealer = dealer;
            }

            var oldspec = db.Specials.ToList();
            if (oldspec.Count > 0)
            {
                db.Specials.RemoveRange(oldspec);
                db.SaveChanges();
            }

            if (!db.Specials.Any(a => a.DealerID == dealer.DealerID))
                db.Specials.AddRange(spcList);

            db.SaveChanges();
        }
        #endregion

        #region Master Data For Vehicle

        //Vehicle Standard Body
        public void CreateVehicleStandardBody(ServicePageDbContext context)
        {
            Dictionary<string, string> mapList = new Dictionary<string, string>();

            mapList.Add("Convertible", "car");
            mapList.Add("Coupe", "car");
            mapList.Add("Hatchback", "car");
            mapList.Add("Minivan", "van");
            mapList.Add("Pickup", "truck");
            mapList.Add("Sedan", "car");
            mapList.Add("SUV", "suv");
            mapList.Add("Van", "van");
            mapList.Add("Wagon", "suv");
            mapList.Add("Specialty", "commercial");


            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<string> bodyStandardNameList = db.Vehicles.Where(a => !string.IsNullOrEmpty(a.standardbody)).Select(x => x.standardbody).Distinct().ToList();
                if (bodyStandardNameList.Any())
                {
                    foreach (string bodyStyle in bodyStandardNameList)
                    {
                        string translatedBodyStyleName = mapList.Where(x => x.Key == bodyStyle).Select(y => y.Value).FirstOrDefault();

                        db.VehicleStandardBodies.AddOrUpdate(x => x.Name,
                            new VehicleStandardBody()
                            {
                                Name = bodyStyle,
                                TranslatedBodyStyleName = translatedBodyStyleName,
                                CreatedDt = DateTime.Now,
                                ModifiedDt = DateTime.Now
                            }
                       );
                    }
                }

                db.SaveChanges();
            }
        }
        //Engine
        public void CreateMappingForSearchCriteria()
        {

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.VehicleCriteriaMappings.RemoveRange(db.VehicleCriteriaMappings);
                db.SaveChanges();
                db.VehicleCriteriaMappings.AddOrUpdate(x => x.BadName,
                    new VehicleCriteriaMapping { VehicleCriteriaMappingId = 1, BadName = "4 door truck", GoodName = "Crew Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 2, BadName = "Extended Cab", GoodName = "Extended Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 3, BadName = "Full size", GoodName = "Fully-size", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 4, BadName = "Mini van", GoodName = "Mini-van", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 5, BadName = "2 door truck", GoodName = "Regular Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 6, BadName = "4 door  truck", GoodName = "Crew Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 7, BadName = "Extended Cab", GoodName = "Extended Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 8, BadName = "Full size", GoodName = "Fully-size", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 9, BadName = "Mini van", GoodName = "Mini-van", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 10, BadName = "2 door truck", GoodName = "Regular Cab", FilterType = "BodyStyle", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 11, BadName = "8 Cylinder", GoodName = "V8 V-8", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 12, BadName = "6.7 Liter", GoodName = "V8 V-8", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 13, BadName = "6 Cylinder", GoodName = "V6 V-6", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 14, BadName = "4 Cylinder", GoodName = "V4 V-4", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 15, BadName = "10 Cylinder", GoodName = "V10 V-10", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 16, BadName = "diesel", GoodName = "Diesel Fuel", FilterType = "FuelType", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 17, BadName = "gas", GoodName = "Gasoline Fuel", FilterType = "FuelType", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 18, BadName = "gasoline", GoodName = "Gasoline Fuel", FilterType = "FuelType", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 19, BadName = "F150", GoodName = "F-150", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 20, BadName = "F250", GoodName = "F-250", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 21, BadName = "F350", GoodName = "F-350", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 22, BadName = "F450", GoodName = "F-450", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 23, BadName = "F550", GoodName = "F-550", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 24, BadName = "F650", GoodName = "F-650", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 25, BadName = "F750", GoodName = "F-750", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 26, BadName = "SuperDuty", GoodName = "Super Duty", FilterType = "Model", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 27, BadName = "under $20,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 28, BadName = "under 20,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 29, BadName = "under $25,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 30, BadName = "under 25,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 31, BadName = "under 10,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 32, BadName = "under $10,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 33, BadName = "under 10000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 34, BadName = "under 20000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 35, BadName = "under 25000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 36, BadName = "under $10000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 37, BadName = "under $20000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 38, BadName = "under $25000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 39, BadName = "under $20,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 40, BadName = "under 20,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 41, BadName = "under $25,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 42, BadName = "under 25,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 43, BadName = "under 10,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 44, BadName = "under $10,000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 45, BadName = "under 10000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 46, BadName = "under 20000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 47, BadName = "under 25000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 48, BadName = "under $10000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 49, BadName = "under $20000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 50, BadName = "under $25000", GoodName = "$20K - $25k", FilterType = "Price", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 51, BadName = "I4,I-4,V4,V-4,L4,L-4,F4,F-4", GoodName = "I4 I-4 V4 V-4 L4 L-4 F4 F-4", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 52, BadName = "I5,I-5,V5,V-5", GoodName = "V5 V-5 I5 I-5", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 53, BadName = "I6,I-6,V6,V-6,L6,L-6,F6,F-6", GoodName = "I6 I-6 V6 V-6 L6 L-6 F6 F-6", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 54, BadName = "I7,I-7,L7,L-7,F7,F-7", GoodName = "I7 I-7 L7 L-7 F7 F-7", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 55, BadName = "I8,I-8,V8,V-8,L8,L-8,F8,F-8,W8,W-8", GoodName = "I8 I-8 V8 V-8 L8 L-8 F8 F-8 W8 W-8", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 56, BadName = "I9,I-9,L9,L-9,F9,F-9", GoodName = "I9 I-9 L9 L-9 F9 F-9", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 57, BadName = "I10,I-10,L10,L-10,V10,V-10,F10,F-10", GoodName = "I10 I-10 L10 L-10 V10 V-10 F10 F-10", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 58, BadName = "I12,I-12,V12,V-12,L12,L-12,F12,F-12,W12,W-12", GoodName = "I12 I-12 V12 V-12 L12 L-12 F12 F-12 W12 W-12", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 59, BadName = "I14,I-14,V14,V-14,L14,L-14", GoodName = "I14 I-14 V14 V-14 L14 L-14", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 60, BadName = "I14,I-14,V14,V-14,L14,L-14", GoodName = "I14 I-14 V14 V-14 L14 L-14", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 61, BadName = "V16,V-16,F16,F-16,W16,W-16", GoodName = "V16 V-16 F16 F-16 W16 W-16", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 62, BadName = "V18,V-18,W18,W-18", GoodName = "V18 V-18 W18 W-18", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 63, BadName = "V20,V-20", GoodName = "V20 V-20", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }
                    , new VehicleCriteriaMapping { VehicleCriteriaMappingId = 64, BadName = "V24,V-24", GoodName = "V24 V-24", FilterType = "Engine", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now }

                    );
                db.SaveChanges();
            }
        }

        public void CreateSegmentForVehicle()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.Segments.AddOrUpdate(x => x.Name
                 , new Segment { SegmentId = 1, Name = "car", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID=1 }
                 , new Segment { SegmentId = 2, Name = "truck", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                 , new Segment { SegmentId = 3, Name = "suv", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                 , new Segment { SegmentId = 4, Name = "van", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                 , new Segment { SegmentId = 5, Name = "commercial-truck", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                 , new Segment { SegmentId = 6, Name = "crossover", CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                 );
                db.SaveChanges();
            }
        }

        #endregion

        #region Incentive Data
        public void CreateCompetitiveModel(ServicePageDbContext context)
        {
            var db = context;
            List<CompetitiveModel> models = new List<CompetitiveModel>()
            {
                new CompetitiveModel { Name = "Super Duty", Description = "Super Duty", Segment = "commercial-truck", MakeName = "Ford", DealerID=1 }
            };

            foreach (CompetitiveModel model in models)
            {
                if (!db.CompetitiveModels.Any(x => x.Name == model.Name))
                {
                    db.CompetitiveModels.AddOrUpdate(model);
                }
            }
            db.SaveChanges();
        }
        #endregion

        #region SiteMap

        public void CreateSiteMapData()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.SiteMaps.AddOrUpdate(new Sitemap { SitemapId = 1, Title = "Home", Description = "Home" }
                                    , new Sitemap { SitemapId = 2, Title = "New", Description = "New cars" }
                                    , new Sitemap { SitemapId = 3, Title = "Used", Description = "Used" }
                                    , new Sitemap { SitemapId = 4, Title = "Commerical", Description = "Commerical Fleet" }
                                    , new Sitemap { SitemapId = 5, Title = "Finance", Description = "Finance" }
                                    , new Sitemap { SitemapId = 6, Title = "Service & Parts", Description = "Service" }
                                    , new Sitemap { SitemapId = 7, Title = "Research", Description = "Research" }
                                    , new Sitemap { SitemapId = 8, Title = "About Us", Description = "About Our Dealership" }
                                    );
                db.SaveChanges();
            }
        }
        public static string ConvertDateToW3CTime(DateTime date)
        {
            //Get the utc offset from the date value
            var utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(date);
            string w3CTime = date.ToUniversalTime().ToString("yyyy-mm-dd");
            //append the offset e.g. z=0, add 1 hour is +01:00
            w3CTime += utcOffset == TimeSpan.Zero ? "Z" :
                String.Format("{0}{1:00}:{2:00}", (utcOffset > TimeSpan.Zero ? "+" : "-")
                , utcOffset.Hours, utcOffset.Minutes);

            return w3CTime;
        }

        public void CreateSiteMapLocationData()
        {
            string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd");
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.SiteMapLocations.AddOrUpdate(x => x.Url, new SiteMapLocation { Url = "/new", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 2 }
, new SiteMapLocation { Url = "/vehicle-department/new/truck", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 2 }
, new SiteMapLocation { Url = "/suv-department/new/crossover–suv", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 2 }
, new SiteMapLocation { Url = "/vehicle-department/new/van", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 2 }
, new SiteMapLocation { Url = "/used", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 2 }
, new SiteMapLocation { Url = "/vehicle-department/used-certified/truck", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 3 }
, new SiteMapLocation { Url = "/suv-department/used-certified/crossover–suv", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 3 }
, new SiteMapLocation { Url = "/vehicle-department/used-certified/van", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 3 }
, new SiteMapLocation { Url = "/finance", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 4 }
, new SiteMapLocation { Url = "/finance/car-loan", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 5 }
, new SiteMapLocation { Url = "/finance/trade-in-car", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 5 }
, new SiteMapLocation { Url = "/service", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 5 }
, new SiteMapLocation { Url = "/service/appointment", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 6 }
, new SiteMapLocation { Url = "/service/specials", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 6 }
, new SiteMapLocation { Url = "/dealership-info", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 6 }
, new SiteMapLocation { Url = "/dealership-info/hours-and-directions", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 8 }
, new SiteMapLocation { Url = "/dealership-info/staff", ChangeFrequency = SiteMapLocation.eChangeFrequency.daily, LastModified = dateTime, Priority = 1, SitemapId = 8 }
            );
                db.SaveChanges();
            }
        }


        private Sitemap GetSitemap(string title, string description)
        {
            Sitemap newSiteMap;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                if (!db.SiteMaps.Any(x => x.Title == title && x.Description == description))
                {
                    newSiteMap = new Sitemap()
                    {
                        Title = title,
                        Description = description
                    };
                    db.SiteMaps.Add(newSiteMap);
                    db.SaveChanges();
                }
                else
                {
                    newSiteMap = db.SiteMaps.First(x => x.Title == title && x.Description == description);
                }
            }
            return newSiteMap;
        }

        private SiteMapLocation GetSiteMapLocation(string url, Sitemap siteMap, ServicePageDbContext db)
        {
            string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd");
            SiteMapLocation newLocation;

            if (!db.SiteMapLocations.Any(x => x.Url == url))
            {
                newLocation = new SiteMapLocation()
                {
                    Url = url,
                    ChangeFrequency = SiteMapLocation.eChangeFrequency.daily,
                    Priority = 1,
                    LastModified = dateTime,
                    SitemapId = siteMap.SitemapId
                };
                db.SiteMapLocations.Add(newLocation);
            }
            else
            {
                newLocation = db.SiteMapLocations.First(x => x.Url == url);
                newLocation.LastModified = dateTime;
            }
            db.SaveChanges();

            return newLocation;
        }

        public void CreateSiteMap()
        {
            SiteMapNodeCollection siteMapNodes = SiteMap.RootNode.ChildNodes;
            string host = @"https://agiledealer-demo.azurewebsites.net";
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                foreach (SiteMapNode siteMapNode in siteMapNodes)
                {
                    string title = siteMapNode.Title;
                    string description = siteMapNode.Description;

                    Sitemap newSiteMap = GetSitemap(title, description);

                    if (siteMapNode.HasChildNodes)
                    {
                        foreach (SiteMapNode node in siteMapNode.ChildNodes)
                        {
                            string newHost = host + node.Url;
                            SiteMapLocation newLocation = GetSiteMapLocation(newHost, newSiteMap, db);
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        #endregion

        #region Notification Settings
        public void CreateNotificationSettings()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.NotificationSettings.AddOrUpdate(
                    new NotificationSetting() { NotificationSettingId = 1, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "AllLead", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now,DealerID=1 },
                    new NotificationSetting() { NotificationSettingId = 2, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "Contact", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
                    new NotificationSetting() { NotificationSettingId = 3, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "Finance", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
                    new NotificationSetting() { NotificationSettingId = 4, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "Service", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
                    new NotificationSetting() { NotificationSettingId = 5, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "Availability", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 },
                    new NotificationSetting() { NotificationSettingId = 6, AdfEmail = "admin@savvydealer.com", PlainTextEmail = "admin@savvydealer.com", LeadType = "PartRequest", HasAdfEmail = true, HasPlainTextEmail = true, CreatedDt = DateTime.Now, ModifiedDt = DateTime.Now, DealerID = 1 }
                    );
                db.SaveChanges();
            }
        }
        #endregion

        #region CustomRedirectUrls

        public void CreateCustomRedirectUrls()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                db.CustomRedirectUrls.AddOrUpdate(x => x.Name,
                   new CustomRedirectUrl { Name = "/", OldUrl = "/", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm", OldUrl = "/used-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm", OldUrl = "/new-inventory/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/service/index.htm", OldUrl = "/service/index.htm", NewUrl = "/service", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/?tcdkwid=295220935&tcdcmpid=108251&tcdadid=163944773402", OldUrl = "/?tcdkwid=295220935&tcdcmpid=108251&tcdadid=163944773402", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/mobile/", OldUrl = "/mobile/", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/finance", OldUrl = "/finance", NewUrl = "/finance", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/dealership/staff.htm", OldUrl = "/dealership/staff.htm", NewUrl = "/dealership-info/staff", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?reset=InventoryListing", OldUrl = "/new-inventory/index.htm?reset=InventoryListing", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/?tcdkwid=295220937&tcdcmpid=108251&tcdadid=163944773402", OldUrl = "/?tcdkwid=295220937&tcdcmpid=108251&tcdadid=163944773402", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/?tcdkwid=312594477&tcdcmpid=108251&tcdadid=163944773402", OldUrl = "/?tcdkwid=312594477&tcdcmpid=108251&tcdadid=163944773402", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-f-150-sda.htm", OldUrl = "/reviews/2016-ford-f-150-sda.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?start=35", OldUrl = "/used-inventory/index.htm?start=35", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-explorer-sda.htm", OldUrl = "/reviews/2017-ford-explorer-sda.htm", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-escape-sda.htm", OldUrl = "/reviews/2017-ford-escape-sda.htm", NewUrl = "/vcp/new/ford/escape", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?sortBy=internetPrice+asc", OldUrl = "/used-inventory/index.htm?sortBy=internetPrice+asc", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/schedule-service.htm", OldUrl = "/schedule-service.htm", NewUrl = "/service/appointment", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/?tcdkwid=295220936&tcdcmpid=108251&tcdadid=163944773402", OldUrl = "/?tcdkwid=295220936&tcdcmpid=108251&tcdadid=163944773402", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?start=70", OldUrl = "/used-inventory/index.htm?start=70", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/specials/new.htm", OldUrl = "/specials/new.htm", NewUrl = "/vehicle-department-special/new/special", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F150&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F150&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/bargain-inventory/index.htm", OldUrl = "/bargain-inventory/index.htm", NewUrl = "/vehicle-department-special/used-certified/special", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/finance-application.htm", OldUrl = "/finance-application.htm", NewUrl = "/finance/car-loan", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?redirectFromMissingVDP=true", OldUrl = "/used-inventory/index.htm?redirectFromMissingVDP=true", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2001-Ford-F150-0216ff0d0a0e0ae85a0d6bae2340f57d.htm", OldUrl = "/used/Ford/2001-Ford-F150-0216ff0d0a0e0ae85a0d6bae2340f57d.htm", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?saveFacetState=true&normalBodyStyle=Sedan&=&=&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=inventory-listing1-facet-anchor-normalBodyStyle-4", OldUrl = "/used-inventory/index.htm?saveFacetState=true&normalBodyStyle=Sedan&=&=&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=inventory-listing1-facet-anchor-normalBodyStyle-4", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-edge-sda.htm", OldUrl = "/reviews/2016-ford-edge-sda.htm", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-f-150-vs-toyota-tundra-sda.htm", OldUrl = "/reviews/2017-ford-f-150-vs-toyota-tundra-sda.htm", NewUrl = "/research/detail/2017-ford-f-150-vs-toyota-tundra", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-f-150-sda.htm", OldUrl = "/reviews/2017-ford-f-150-sda.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-fusion-sda.htm", OldUrl = "/reviews/2017-ford-fusion-sda.htm", NewUrl = "/vcp/new/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/contact.htm", OldUrl = "/contact.htm", NewUrl = "/dealership/directions.htm", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/parts/index.htm", OldUrl = "/parts/index.htm", NewUrl = "/service/parts", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/dealership/directions.htm", OldUrl = "/dealership/directions.htm", NewUrl = "/dealership/directions.htm", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?normalBodyStyle=&cab=&model=&year=2017&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-3", OldUrl = "/new-inventory/index.htm?normalBodyStyle=&cab=&model=&year=2017&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-3", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/used-ford-f-150-sda.htm", OldUrl = "/reviews/used-ford-f-150-sda.htm", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?normalBodyStyle=&model=Focus&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-8", OldUrl = "/new-inventory/index.htm?normalBodyStyle=&model=Focus&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-8", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?normalBodyStyle=&cab=&model=&year=2016&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-3", OldUrl = "/new-inventory/index.htm?normalBodyStyle=&cab=&model=&year=2016&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-3", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2011-Ford-Flex-4becdc850a0e0a171812578a69228f30.htm?searchDepth=1:1", OldUrl = "/used/Ford/2011-Ford-Flex-4becdc850a0e0a171812578a69228f30.htm?searchDepth=1:1", NewUrl = "/vcp/new/ford/flex", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/employment/index.htm", OldUrl = "/employment/index.htm", NewUrl = "/dealership-info", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-Focus-8edef5d80a0e0ae736262303fa3f87c7.htm?searchDepth=1:10", OldUrl = "/new/Ford/2017-Ford-Focus-8edef5d80a0e0ae736262303fa3f87c7.htm?searchDepth=1:10", NewUrl = "/vcp/new/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=Flex&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-model-6", OldUrl = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=Flex&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-model-6", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?start=35&sortBy=internetPrice+asc", OldUrl = "/used-inventory/index.htm?start=35&sortBy=internetPrice+asc", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Explorer&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Explorer&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/no-results.htm?category=AUTO&normalBodyStyle=&model=Explorer&year=2016&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", OldUrl = "/all-inventory/no-results.htm?category=AUTO&normalBodyStyle=&model=Explorer&year=2016&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=2015+Ford+F150&redirectFromMissingVDP=true", OldUrl = "/all-inventory/index.htm?search=2015+Ford+F150&redirectFromMissingVDP=true", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/kbb-instant-cash-offer-.htm", OldUrl = "/kbb-instant-cash-offer-.htm", NewUrl = "/finance/instant-cash-offer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/models.htm", OldUrl = "/models.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-f-150-king-ranch-sda.htm", OldUrl = "/reviews/2017-ford-f-150-king-ranch-sda.htm", NewUrl = "/research/detail/2017-ford-f-150-king-ranch", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-f-150-vs-ram-1500-sda.htm", OldUrl = "/reviews/2017-ford-f-150-vs-ram-1500-sda.htm", NewUrl = "/research/detail/2017-ford-f-150-vs-ram-1500", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2004-Ford-Explorer-ef038b9b0a0e0adf2f7fe0bbc9eb5404.htm", OldUrl = "/used/Ford/2004-Ford-Explorer-ef038b9b0a0e0adf2f7fe0bbc9eb5404.htm", NewUrl = "/vcp/used/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/?_renderer=desktop&buildingPage=false&useAjaxWrap=true&locale=en_US&_variant=CONTROL", OldUrl = "/?_renderer=desktop&buildingPage=false&useAjaxWrap=true&locale=en_US&_variant=CONTROL", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F16252852", OldUrl = "/all-inventory/index.htm?search=F16252852", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=F150&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=F150&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-transit-wagon-sda.htm", OldUrl = "/reviews/2016-ford-transit-wagon-sda.htm", NewUrl = "/vcp/new/ford/transit-wagon", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-expedition-sda.htm", OldUrl = "/reviews/2017-ford-expedition-sda.htm", NewUrl = "/vcp/new/ford/expedition", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/service", OldUrl = "/service", NewUrl = "/service", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=Truck&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=inventory-listing1-facet-anchor-normalBodyStyle-6", OldUrl = "/used-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=Truck&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=inventory-listing1-facet-anchor-normalBodyStyle-6", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/2016-ford-f-150-in-elizabethtown-ky.htm", OldUrl = "/2016-ford-f-150-in-elizabethtown-ky.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/models/index.htm", OldUrl = "/models/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/parts-service-coupons/index.htm", OldUrl = "/parts-service-coupons/index.htm", NewUrl = "/service", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/service-appointment.htm", OldUrl = "/service-appointment.htm", NewUrl = "/service/appointment", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2013-Ford-F150-1f53c2b80a0e0a171812578a7147cc06.htm?searchDepth=11:25", OldUrl = "/used/Ford/2013-Ford-F150-1f53c2b80a0e0a171812578a7147cc06.htm?searchDepth=11:25", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=2:4", OldUrl = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=2:4", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/2016-ford-edge-in-elizabethtown--ky.htm", OldUrl = "/2016-ford-edge-in-elizabethtown--ky.htm", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?normalBodyStyle=&model=Fusion&year=2016&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", OldUrl = "/new-inventory/index.htm?normalBodyStyle=&model=Fusion&year=2016&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=&year=2015&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-year-2", OldUrl = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=&year=2015&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-year-2", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-focus-vs-mazda-3-sda.htm", OldUrl = "/reviews/2017-ford-focus-vs-mazda-3-sda.htm", NewUrl = "/research/detail/2017-ford-focus-vs-mazda-3", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/details.htm?vehicleId=561caf710a0e0adf3e0eb2a5c70a908f&locale=en_US&userProfileId=58c9edfee4b0522c051ec0c9", OldUrl = "/used-inventory/details.htm?vehicleId=561caf710a0e0adf3e0eb2a5c70a908f&locale=en_US&userProfileId=58c9edfee4b0522c051ec0c9", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Chevrolet/2007-Chevrolet-Silverado+1500-46c6efe00a0e0ae77948e5c80a81ec5f.htm", OldUrl = "/used/Chevrolet/2007-Chevrolet-Silverado+1500-46c6efe00a0e0ae77948e5c80a81ec5f.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/auto-lease-vs-buying-a-car.htm", OldUrl = "/auto-lease-vs-buying-a-car.htm", NewUrl = "/finance", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/dealership/about.htm", OldUrl = "/dealership/about.htm", NewUrl = "/dealership-info/hours-and-directions", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?gvModel=F-150", OldUrl = "/new-inventory/index.htm?gvModel=F-150", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F-250SD-0ada53560a0e0adf41c9e44a9c7438c7.htm", OldUrl = "/new/Ford/2017-Ford-F-250SD-0ada53560a0e0adf41c9e44a9c7438c7.htm", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/index.htm", OldUrl = "/showroom/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/details.htm?vehicleId=0216ff0d0a0e0ae85a0d6bae2340f57d&locale=en_US&userProfileId=58c9d1cde4b0522c051c5226", OldUrl = "/used-inventory/details.htm?vehicleId=0216ff0d0a0e0ae85a0d6bae2340f57d&locale=en_US&userProfileId=58c9d1cde4b0522c051c5226", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F17001251", OldUrl = "/all-inventory/index.htm?search=F17001251", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U16933230", OldUrl = "/all-inventory/index.htm?search=U16933230", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/financing/index.htm", OldUrl = "/financing/index.htm", NewUrl = "/finance", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-escape-vs-toyota-rav4-sda.htm", OldUrl = "/reviews/2017-ford-escape-vs-toyota-rav4-sda.htm", NewUrl = "/research/detail/2017-ford-escape-vs-toyota-rav4", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/specials/index.htm", OldUrl = "/specials/index.htm", NewUrl = "/vehicle-department-special/new/special", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-Focus-99653e8f0a0e0a6b278b25048f166cad.htm?searchDepth=32:93", OldUrl = "/used/Ford/2014-Ford-Focus-99653e8f0a0e0a6b278b25048f166cad.htm?searchDepth=32:93", NewUrl = "/vcp/used/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2015-Ford-Edge-b4cc42cd0a0e0ae854d002e558dc779e.htm?searchDepth=3:3", OldUrl = "/used/Ford/2015-Ford-Edge-b4cc42cd0a0e0ae854d002e558dc779e.htm?searchDepth=3:3", NewUrl = "/vcp/used/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm?searchDepth=1:32", OldUrl = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm?searchDepth=1:32", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-explorer-vs-jeep-grand-cherokee-sda.htm", OldUrl = "/reviews/2017-ford-explorer-vs-jeep-grand-cherokee-sda.htm", NewUrl = "/research/detail/2017-ford-explorer-vs-jeep-grand-cherokee", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?normalBodyStyle=Truck&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=", OldUrl = "/used-inventory/index.htm?normalBodyStyle=Truck&engine=&normalTransmission=&normalDriveLine=&odometer=&lastFacetInteracted=", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?start=105", OldUrl = "/used-inventory/index.htm?start=105", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2016-Ford-Fusion-b4cc42770a0e0ae854d002e5ab244b78.htm?searchDepth=37:59", OldUrl = "/used/Ford/2016-Ford-Fusion-b4cc42770a0e0ae854d002e5ab244b78.htm?searchDepth=37:59", NewUrl = "/vcp/used/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Jeep/2001-Jeep-Cherokee-2e4976640a0e0adf16a64d1fe77358db.htm", OldUrl = "/used/Jeep/2001-Jeep-Cherokee-2e4976640a0e0adf16a64d1fe77358db.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Edge&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Edge&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Focus&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Focus&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=Fiesta&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-model-6", OldUrl = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=Fiesta&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&lastFacetInteracted=inventory-listing1-facet-anchor-model-6", NewUrl = "/vcp/new/ford/fiesta", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?sortBy=internetPrice+asc", OldUrl = "/new-inventory/index.htm?sortBy=internetPrice+asc", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm", OldUrl = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-f-250-diesel-vs-chevrolet-silverado-2500hd-diesel-sda.htm", OldUrl = "/reviews/2016-ford-f-250-diesel-vs-chevrolet-silverado-2500hd-diesel-sda.htm", NewUrl = "/research/detail/2016-ford-f-250-diesel-vs-chevrolet-silverado-2500hd-diesel", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/specials/service.htm", OldUrl = "/specials/service.htm", NewUrl = "/service/specials", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2016-Ford-Focus+ST-fbac79af0a0e0ae736262303eb7d1c27.htm", OldUrl = "/used/Ford/2016-Ford-Focus+ST-fbac79af0a0e0ae736262303eb7d1c27.htm", NewUrl = "/vcp/used/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/GMC/2004-GMC-Sonoma-561caf710a0e0adf3e0eb2a5c70a908f.htm", OldUrl = "/used/GMC/2004-GMC-Sonoma-561caf710a0e0adf3e0eb2a5c70a908f.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=2016+Ford+F150&redirectFromMissingVDP=true", OldUrl = "/all-inventory/index.htm?search=2016+Ford+F150&redirectFromMissingVDP=true", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F15253322", OldUrl = "/all-inventory/index.htm?search=F15253322", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm", OldUrl = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm", NewUrl = "/vcp/certified/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&normalBodyStyle=&model=F-250SD&year=2017&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-4", OldUrl = "/new-inventory/index.htm?search=&normalBodyStyle=&model=F-250SD&year=2017&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-4", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm?searchDepth=3:4", OldUrl = "/new/Ford/2016-Ford-Edge-14d48f420a0e0ae7308e83ecf1c66c96.htm?searchDepth=3:4", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2006-Ford-Explorer-9e3110bc0a0e0a1725c5dc92c0464f91.htm", OldUrl = "/used/Ford/2006-Ford-Explorer-9e3110bc0a0e0a1725c5dc92c0464f91.htm", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2006-Ford-Explorer-9e3110bc0a0e0a1725c5dc92c0464f91.htm?searchDepth=11:97", OldUrl = "/used/Ford/2006-Ford-Explorer-9e3110bc0a0e0a1725c5dc92c0464f91.htm?searchDepth=11:97", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-F150-e32e43f60a0e0adf748d53f41d863662.htm?searchDepth=8:14", OldUrl = "/used/Ford/2014-Ford-F150-e32e43f60a0e0adf748d53f41d863662.htm?searchDepth=8:14", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2016-Ford-Mustang-b6922a2e0a0e0adf4ae7cb915e5e8030.htm?searchDepth=1:4", OldUrl = "/used/Ford/2016-Ford-Mustang-b6922a2e0a0e0adf4ae7cb915e5e8030.htm?searchDepth=1:4", NewUrl = "/vcp/used/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Jeep/2006-Jeep-Commander-0f70adbf0a0e0a1725c5dc92c7baddf0.htm", OldUrl = "/used/Jeep/2006-Jeep-Commander-0f70adbf0a0e0a1725c5dc92c7baddf0.htm", NewUrl = "/suv-department/used-certified/crossover–suv", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/16968067-1.insider.pro", OldUrl = "/16968067-1.insider.pro", NewUrl = "/", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F16252452", OldUrl = "/all-inventory/index.htm?search=F16252452", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/no-results.htm?category=AUTO&normalBodyStyle=&model=Expedition&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", OldUrl = "/all-inventory/no-results.htm?category=AUTO&normalBodyStyle=&model=Expedition&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", NewUrl = "/vcp/new/ford/expedition", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm?searchDepth=12:12", OldUrl = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm?searchDepth=12:12", NewUrl = "/vcp/certified/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/dealership-info", OldUrl = "/dealership-info", NewUrl = "/dealership-info/hours-and-directions", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/global-incentives/index.htm", OldUrl = "/global-incentives/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/escape", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?gvModel=Explorer", OldUrl = "/new-inventory/index.htm?gvModel=Explorer", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=F-250SD&=&=&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-5", OldUrl = "/new-inventory/index.htm?saveFacetState=true&normalBodyStyle=&cab=&model=F-250SD&=&=&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-5", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F150-0738bafd0a0e0ae8294096d143a3ab69.htm", OldUrl = "/new/Ford/2017-Ford-F150-0738bafd0a0e0ae8294096d143a3ab69.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/C-Max+Energi/", OldUrl = "/showroom/2016/Ford/C-Max+Energi/", NewUrl = "/vcp/new/ford/c-max-hybrid", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/tires.htm", OldUrl = "/tires.htm", NewUrl = "/service/tire-center", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Mustang&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Mustang&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/used/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=2014&make=Ford&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=2014&make=Ford&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2011-Ford-F150-2e4977080a0e0adf16a64d1f25726d0f.htm?searchDepth=5:16", OldUrl = "/used/Ford/2011-Ford-F150-2e4977080a0e0adf16a64d1f25726d0f.htm?searchDepth=5:16", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-Edge-e19059dd0a0e0a6b278b25048fa07193.htm?searchDepth=1:11", OldUrl = "/used/Ford/2014-Ford-Edge-e19059dd0a0e0a6b278b25048fa07193.htm?searchDepth=1:11", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=2:6", OldUrl = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=2:6", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Lincoln/2011-Lincoln-MKS-e500b41b0a0e0a6b1e2956662a489731.htm?searchDepth=75:90", OldUrl = "/used/Lincoln/2011-Lincoln-MKS-e500b41b0a0e0a6b1e2956662a489731.htm?searchDepth=75:90", NewUrl = "/car-department/used-certified/car", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Toyota/2014-Toyota-Tundra-4f90d0100a0e0a175837ce5896f6e131.htm?searchDepth=1:1", OldUrl = "/used/Toyota/2014-Toyota-Tundra-4f90d0100a0e0a175837ce5896f6e131.htm?searchDepth=1:1", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U16922291", OldUrl = "/all-inventory/index.htm?search=U16922291", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/black-book-credit-estimator/index.htm", OldUrl = "/black-book-credit-estimator/index.htm", NewUrl = "/finance/credit-estimator", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm?searchDepth=1:1", OldUrl = "/certified/Ford/2016-Ford-F-250SD-a6e974150a0e0ae75af8e0847273a1b8.htm?searchDepth=1:1", NewUrl = "/vcp/certified/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/mobile/search/view/Type/Used/page/2/", OldUrl = "/mobile/search/view/Type/Used/page/2/", NewUrl = "/used?page=2", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&normalBodyStyle=&model=Edge&year=2017&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", OldUrl = "/new-inventory/index.htm?search=&normalBodyStyle=&model=Edge&year=2017&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-year-0", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/page/blog/", OldUrl = "/page/blog/", NewUrl = "/research", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Chevrolet&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Chevrolet&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Focus&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Focus&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Chevrolet/2007-Chevrolet-Silverado+2500HD+Classic-46c6f2b10a0e0ae77948e5c8d31db245.htm?searchDepth=53:91", OldUrl = "/used/Chevrolet/2007-Chevrolet-Silverado+2500HD+Classic-46c6f2b10a0e0ae77948e5c8d31db245.htm?searchDepth=53:91", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2003-Ford-Mustang-46c6f1700a0e0ae77948e5c80dd0851b.htm?searchDepth=8:9", OldUrl = "/used/Ford/2003-Ford-Mustang-46c6f1700a0e0ae77948e5c80dd0851b.htm?searchDepth=8:9", NewUrl = "/vcp/used/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm", OldUrl = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm?searchDepth=1:15", OldUrl = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm?searchDepth=1:15", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2015-Ford-F150-ef038ac20a0e0adf2f7fe0bb760298a3.htm?searchDepth=7:15", OldUrl = "/used/Ford/2015-Ford-F150-ef038ac20a0e0adf2f7fe0bb760298a3.htm?searchDepth=7:15", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Jeep/2013-Jeep-Grand+Cherokee-5eed60910a0e0adf4ae7cb9155afc888.htm?searchDepth=1:1", OldUrl = "/used/Jeep/2013-Jeep-Grand+Cherokee-5eed60910a0e0adf4ae7cb9155afc888.htm?searchDepth=1:1", NewUrl = "/suv-department/used-certified/crossover–suv", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Nissan/2007-Nissan-Maxima-c0d979c60a0e0ae8294096d155b9c5aa.htm", OldUrl = "/used/Nissan/2007-Nissan-Maxima-c0d979c60a0e0ae8294096d155b9c5aa.htm", NewUrl = "/car-department/used-certified/car", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?listingConfigId=AUTO-new,AUTO-used&compositeType=used&year=&make=&start=0&sort=&facetbrowse=true&quick=true&preserveSelectsOnBack=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/all-inventory/index.htm?listingConfigId=AUTO-new,AUTO-used&compositeType=used&year=&make=&start=0&sort=&facetbrowse=true&quick=true&preserveSelectsOnBack=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/car-department/new/car", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U16922641", OldUrl = "/all-inventory/index.htm?search=U16922641", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U16932452", OldUrl = "/all-inventory/index.htm?search=U16932452", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/inventory/view/SortBy0/", OldUrl = "/inventory/view/SortBy0/", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Mustang&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Mustang&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&normalBodyStyle=&model=Escape&internetPrice=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", OldUrl = "/new-inventory/index.htm?search=&normalBodyStyle=&model=Escape&internetPrice=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", NewUrl = "/vcp/new/ford/escape", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=F150&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-7", OldUrl = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=F150&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-7", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?year=2016&make=Ford&model=F150&sortBy=internetPrice+asc", OldUrl = "/new-inventory/index.htm?year=2016&make=Ford&model=F150&sortBy=internetPrice+asc", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F-250SD-294287b60a0e0adf16a64d1f14e7050e.htm?searchDepth=3:12", OldUrl = "/new/Ford/2017-Ford-F-250SD-294287b60a0e0adf16a64d1f14e7050e.htm?searchDepth=3:12", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F-350SD-3dd6eedc0a0e0a1725c5dc926dcc08bc.htm?searchDepth=2:3", OldUrl = "/new/Ford/2017-Ford-F-350SD-3dd6eedc0a0e0a1725c5dc926dcc08bc.htm?searchDepth=2:3", NewUrl = "/vcp/new/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F150-42fa48c00a0e0ae736262303f6ea489e.htm?searchDepth=4:41", OldUrl = "/new/Ford/2017-Ford-F150-42fa48c00a0e0ae736262303f6ea489e.htm?searchDepth=4:41", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-f-150-vs-2010-f-150-sda.htm", OldUrl = "/reviews/2016-ford-f-150-vs-2010-f-150-sda.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Explorer/", OldUrl = "/showroom/2017/Ford/Explorer/", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Fusion+Energi/", OldUrl = "/showroom/2017/Ford/Fusion+Energi/", NewUrl = "/vcp/new/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Transit-250+Cutaway/", OldUrl = "/showroom/2017/Ford/Transit-250+Cutaway/", NewUrl = "/vcp/new/ford/transit-van", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/details.htm?vehicleId=c36ae0790a0e0ae70fea12d25d57c408&locale=en_US&userProfileId=588b4d77e4b097ed3ab32609", OldUrl = "/used-inventory/details.htm?vehicleId=c36ae0790a0e0ae70fea12d25d57c408&locale=en_US&userProfileId=588b4d77e4b097ed3ab32609", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Fusion&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Fusion&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/used/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Honda&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Honda&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=2016&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=2016&make=Ford&model=F-250SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?start=0", OldUrl = "/used-inventory/index.htm?start=0", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Chevrolet/", OldUrl = "/used/Chevrolet/", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2000-Ford-Windstar-c25910110a0e0adf47fda79ca362455c.htm", OldUrl = "/used/Ford/2000-Ford-Windstar-c25910110a0e0adf47fda79ca362455c.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2002-Ford-Thunderbird-263921160a0e0a175837ce58f7585c29.htm", OldUrl = "/used/Ford/2002-Ford-Thunderbird-263921160a0e0a175837ce58f7585c29.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2004-Ford-F150-05327c570a0e0adf47fda79cb45014b9.htm?searchDepth=3:16", OldUrl = "/used/Ford/2004-Ford-F150-05327c570a0e0adf47fda79cb45014b9.htm?searchDepth=3:16", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2008-Ford-Escape-a352867c0a0e0a6b1c382ff85f04e9ab.htm", OldUrl = "/used/Ford/2008-Ford-Escape-a352867c0a0e0a6b1c382ff85f04e9ab.htm", NewUrl = "/vcp/used/ford/escape", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm?searchDepth=2:25", OldUrl = "/used/Ford/2012-Ford-F150-41af73970a0e0adf3e0eb2a57f751840.htm?searchDepth=2:25", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=1:7", OldUrl = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=1:7", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=3:15", OldUrl = "/used/Ford/2014-Ford-F150-46c6ef3f0a0e0ae77948e5c8fa5e506d.htm?searchDepth=3:15", NewUrl = "/vcp/used/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2016-Ford-Explorer-b4cc42a60a0e0ae854d002e5f52ac8bf.htm", OldUrl = "/used/Ford/2016-Ford-Explorer-b4cc42a60a0e0ae854d002e5f52ac8bf.htm", NewUrl = "/vcp/new/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Ford/2016-Ford-Mustang-4becdf790a0e0a171812578a6313a2dc.htm?searchDepth=6:9", OldUrl = "/used/Ford/2016-Ford-Mustang-4becdf790a0e0a171812578a6313a2dc.htm?searchDepth=6:9", NewUrl = "/vcp/used/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Mercedes-Benz/2003-Mercedes-Benz-S-Class-c2590f980a0e0adf47fda79c326a4a19.htm", OldUrl = "/used/Mercedes-Benz/2003-Mercedes-Benz-S-Class-c2590f980a0e0adf47fda79c326a4a19.htm", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Mercedes-Benz/2003-Mercedes-Benz-S-Class-c2590f980a0e0adf47fda79c326a4a19.htm?searchDepth=1:1", OldUrl = "/used/Mercedes-Benz/2003-Mercedes-Benz-S-Class-c2590f980a0e0adf47fda79c326a4a19.htm?searchDepth=1:1", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used/Toyota/2014-Toyota-Tundra-4f90d0100a0e0a175837ce5896f6e131.htm?searchDepth=28:28", OldUrl = "/used/Toyota/2014-Toyota-Tundra-4f90d0100a0e0a175837ce5896f6e131.htm?searchDepth=28:28", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/vehicle-info/1995-toyota-camry-4t1sk12e5su495899", OldUrl = "/vehicle-info/1995-toyota-camry-4t1sk12e5su495899", NewUrl = "/used", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?redirectFromMissingVDP=true", OldUrl = "/all-inventory/index.htm?redirectFromMissingVDP=true", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=2016+Ford+Fusion&redirectFromMissingVDP=true", OldUrl = "/all-inventory/index.htm?search=2016+Ford+Fusion&redirectFromMissingVDP=true", NewUrl = "/vcp/new/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F16252721", OldUrl = "/all-inventory/index.htm?search=F16252721", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=F17251322", OldUrl = "/all-inventory/index.htm?search=F17251322", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U16922621", OldUrl = "/all-inventory/index.htm?search=U16922621", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/all-inventory/index.htm?search=U17921110", OldUrl = "/all-inventory/index.htm?search=U17921110", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/certified-inventory/index.htm", OldUrl = "/certified-inventory/index.htm", NewUrl = "/certified", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/certified/Ford/2013-Ford-Edge-0581f39b0a0e0ae8595b246c403b3d1a.htm", OldUrl = "/certified/Ford/2013-Ford-Edge-0581f39b0a0e0ae8595b246c403b3d1a.htm", NewUrl = "/vcp/used/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Fusion&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=&make=Ford&model=Fusion&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=2016&make=Ford&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=2016&make=Ford&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?compositeType=new&year=2017&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/new-inventory/index.htm?compositeType=new&year=2017&make=Ford&model=Escape&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?gvModel=Focus", OldUrl = "/new-inventory/index.htm?gvModel=Focus", NewUrl = "/vcp/new/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?normalBodyStyle=&model=Expedition&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", OldUrl = "/new-inventory/index.htm?normalBodyStyle=&model=Expedition&vinIncentiveEligibility_composite=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&saveFacetState=true&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", NewUrl = "/vcp/new/ford/expedition", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=Escape&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", OldUrl = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=Escape&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-3", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=F150&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-8", OldUrl = "/new-inventory/index.htm?search=&saveFacetState=true&normalBodyStyle=&cab=&model=F150&internetPrice=&cityMpg=&normalEngine=&normalTransmission=&normalDriveLine=&reset=InventoryListing&lastFacetInteracted=inventory-listing1-facet-anchor-model-8", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new-inventory/index.htm?start=35", OldUrl = "/new-inventory/index.htm?start=35", NewUrl = "/new", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2015-Ford-F150-4bed58150a0e0a6b652f5af01776dc3d.htm", OldUrl = "/new/Ford/2015-Ford-F150-4bed58150a0e0a6b652f5af01776dc3d.htm", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2016-Ford-F150-996548540a0e0adf41c9e44add5e7642.htm?searchDepth=17:20", OldUrl = "/new/Ford/2016-Ford-F150-996548540a0e0adf41c9e44add5e7642.htm?searchDepth=17:20", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2016-Ford-Focus-0738ba580a0e0ae8294096d1ac49a3f5.htm", OldUrl = "/new/Ford/2016-Ford-Focus-0738ba580a0e0ae8294096d1ac49a3f5.htm", NewUrl = "/vcp/new/ford/focus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-Edge-94050bf80a0e0adf47fda79c809a8052.htm", OldUrl = "/new/Ford/2017-Ford-Edge-94050bf80a0e0adf47fda79c809a8052.htm", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/new/Ford/2017-Ford-F150-561cb2ff0a0e0ae8192de0ebcfa8957c.htm?searchDepth=7:41", OldUrl = "/new/Ford/2017-Ford-F150-561cb2ff0a0e0ae8192de0ebcfa8957c.htm?searchDepth=7:41", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/oil-change-advice.htm", OldUrl = "/oil-change-advice.htm", NewUrl = "/service/oil-change", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-transit-van-sda.htm", OldUrl = "/reviews/2016-ford-transit-van-sda.htm", NewUrl = "/vcp/new/ford/transit-van", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-mustang-sda.htm", OldUrl = "/reviews/2017-ford-mustang-sda.htm", NewUrl = "/vcp/new/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/Edge/", OldUrl = "/showroom/2016/Ford/Edge/", NewUrl = "/vcp/new/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/F-550+Chassis/", OldUrl = "/showroom/2016/Ford/F-550+Chassis/", NewUrl = "/vcp/new/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/Flex/", OldUrl = "/showroom/2016/Ford/Flex/", NewUrl = "/vcp/new/ford/flex", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/Fusion/", OldUrl = "/showroom/2016/Ford/Fusion/", NewUrl = "/vcp/new/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2016/Ford/Mustang/", OldUrl = "/showroom/2016/Ford/Mustang/", NewUrl = "/vcp/new/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/C-Max+Hybrid/", OldUrl = "/showroom/2017/Ford/C-Max+Hybrid/", NewUrl = "/vcp/new/ford/c-max-hybrid", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/E-450+Cutaway/", OldUrl = "/showroom/2017/Ford/E-450+Cutaway/", NewUrl = "/vcp/new/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/F-150/", OldUrl = "/showroom/2017/Ford/F-150/", NewUrl = "/vcp/new/ford/f-150", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/F-350/", OldUrl = "/showroom/2017/Ford/F-350/", NewUrl = "/vcp/new/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Shelby+GT350/", OldUrl = "/showroom/2017/Ford/Shelby+GT350/", NewUrl = "/vcp/new/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/sitemap.htm", OldUrl = "/sitemap.htm", NewUrl = "/sitemap", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Chevrolet&model=Silverado+1500&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Chevrolet&model=Silverado+1500&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vehicle-department/used-certified/truck", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Edge&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=Edge&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/used/ford/edge", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F-350SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Ford&model=F-350SD&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/vcp/used/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Jeep&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Jeep&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/suv-department/used-certified/crossover–suv", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/used-inventory/index.htm?compositeType=used&year=&make=Subaru&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", OldUrl = "/used-inventory/index.htm?compositeType=used&year=&make=Subaru&model=&start=0&sort=&facetbrowse=true&quick=true&searchLinkText=SEARCH&showInvTotals=false&showRadius=false&showReset=false&showSubmit=true&facetbrowseGridUnit=BLANK&showSelections=true&dependencies=model:make,city:province,city:state&suppressAllConditions=true&newListingAlias=/new-inventory/index.htm&usedListingAlias=/used-inventory/index.htm&certifiedListingAlias=/certified-inventory/index.htm", NewUrl = "/suv-department/used-certified/crossover–suv", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/parts/accessories-portal.htm", OldUrl = "/parts/accessories-portal.htm", NewUrl = "/service/accessories", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/", OldUrl = "/reviews/", NewUrl = "/research", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-f-250-super-duty-sda.htm", OldUrl = "/reviews/2016-ford-f-250-super-duty-sda.htm", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2016-ford-f-350-super-duty-sda.htm", OldUrl = "/reviews/2016-ford-f-350-super-duty-sda.htm", NewUrl = "/vcp/new/ford/f-350-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/2017-ford-transit-sda.htm", OldUrl = "/reviews/2017-ford-transit-sda.htm", NewUrl = "/vcp/new/ford/transit-wagon", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/new-ford-reviews-sda.htm", OldUrl = "/reviews/new-ford-reviews-sda.htm", NewUrl = "/research/category/review", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/used-ford-explorer-sda.htm", OldUrl = "/reviews/used-ford-explorer-sda.htm", NewUrl = "/vcp/used/ford/explorer", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/used-ford-fusion-sda.htm", OldUrl = "/reviews/used-ford-fusion-sda.htm", NewUrl = "/vcp/new/ford/fusion", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/reviews/used-ford-taurus-sda.htm", OldUrl = "/reviews/used-ford-taurus-sda.htm", NewUrl = "/vcp/new/ford/taurus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/F-250/", OldUrl = "/showroom/2017/Ford/F-250/", NewUrl = "/vcp/new/ford/f-250-super-duty", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Fiesta/", OldUrl = "/showroom/2017/Ford/Fiesta/", NewUrl = "/vcp/new/ford/fiesta", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Mustang/", OldUrl = "/showroom/2017/Ford/Mustang/", NewUrl = "/vcp/new/ford/mustang", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Taurus/", OldUrl = "/showroom/2017/Ford/Taurus/", NewUrl = "/vcp/new/ford/taurus", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Transit-250+Cab+Chassis/", OldUrl = "/showroom/2017/Ford/Transit-250+Cab+Chassis/", NewUrl = "/vcp/new/ford/transit-wagon", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/showroom/2017/Ford/Transit-350+Cutaway/", OldUrl = "/showroom/2017/Ford/Transit-350+Cutaway/", NewUrl = "/vcp/new/ford/transit-wagon", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
                    , new CustomRedirectUrl { Name = "/specials/used.htm", OldUrl = "/specials/used.htm", NewUrl = "/vehicle-department-special/used-certified/special", IsPermanentRedirect = true, CreatedDt = DateTime.Now, CreatedID = "", ModifiedDt = DateTime.Now, ModifiedID = "" }
);


                db.SaveChanges();
            }
        }

        #endregion
    }
}
