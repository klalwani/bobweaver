﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using AgileDealer.Data.Entities.ADF;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public static class XmlHelper
    {
        public static string ReadXml<T>(T model, string xml, string root) where T : new()
        {
            string result = string.Empty;
            XmlDocument document = GetXmlDocumentByString(xml);

            if (document != null)
            {
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = root,
                    IsNullable = true
                };
                XmlNodeList tempModel = document.SelectNodes(root);
                if (tempModel != null)
                {
                    foreach (XmlNode item in tempModel)
                    {
                        try
                        {
                            result = MappingModelToXml<T>(model, item.OuterXml);
                        }
                        catch (Exception ex)
                        {
                            ExceptionHelper.LogException("XmlHelper.ReadXml Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.Base", "Read Xml errors");
                        }
                    }
                }
            }
            return result;
        }

        private static string MappingModelToXml<T>(T model, string xmlNode) where T : new()
        {
            string result = string.Empty;
            if (model != null && !string.IsNullOrEmpty(xmlNode))
            {
                try
                {

                }
                catch (Exception)
                {
                    //ignored
                }
            }


            return result;
        }

        public static string GetXmlText<T>(string filePath, string node, string root) where T : new()
        {
            string result = string.Empty;
            if (IsFileExisted(filePath))
            {
                XmlDocument document = GetXmlDocument(filePath);
                if (document != null)
                {
                    XmlNodeList tempModel = document.SelectNodes(node);
                    if (tempModel != null)
                    {
                        foreach (XmlNode item in tempModel)
                        {
                            if (item.Name == root)
                            {
                                result = item.OuterXml;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static string WriteXml<T>(T model, string filePath, string node, string root) where T : new()
        {
            string result = string.Empty;
            if (IsFileExisted(filePath))
            {
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = root,
                    IsNullable = true
                };
                XmlDocument document = GetXmlDocument(filePath);
                if (document != null)
                {
                    XmlNodeList nodeList = document.SelectNodes(node);
                    PropertyInfo[] properties = GetProperties(model);

                    if (nodeList != null && properties.Any())
                    {
                        foreach (XmlNode item in nodeList)
                        {
                            MappingChildNode(properties, item, model);
                            if (item.HasChildNodes)
                            {
                                XmlNodeList childNodeList = item.ChildNodes;
                                foreach (XmlNode child in childNodeList)
                                {
                                    MappingChildNode(properties, child, model);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        private static void MappingChildNode<T>(PropertyInfo[] properties, XmlNode childNode, T model)
        {
            PropertyInfo property = properties.FirstOrDefault(x => x.Name.ToLower() == childNode.Name);
            if (property != null)
            {
                ModifyChildNode<T>(property, childNode, model);
            }
            if (childNode.HasChildNodes)
            {
                XmlNodeList childNodeList = childNode.ChildNodes;
                foreach (XmlNode child in childNodeList)
                {
                    MappingChildNode(properties, child, model);
                }
            }
        }

        private static void ModifyChildNode<T>(PropertyInfo property, XmlNode childNode, T model)
        {
            if (property != null)
            {
                object propertyValue = property.GetValue(model, null);
                childNode.InnerText = propertyValue != null ? propertyValue.ToString() : string.Empty;
            }
        }

        private static PropertyInfo[] GetProperties<T>(T obj)
        {
            return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
        }

        public static bool IsFileExisted(string filePath)
        {
            bool isExist = false;
            if (!string.IsNullOrEmpty(filePath))
            {
                if (File.Exists(filePath))
                {
                    isExist = true;
                }
            }
            return isExist;
        }

        public static XmlDocument GetXmlDocument(string filePath)
        {
            XmlDocument document = new XmlDocument();
            if (!string.IsNullOrEmpty(filePath))
            {
                document.Load(filePath);
            }
            return document;
        }


        public static XmlDocument GetXmlDocumentByString(string xml)
        {
            XmlDocument document = new XmlDocument();
            if (!string.IsNullOrEmpty(xml))
            {
                document.LoadXml(xml);
            }
            return document;
        }

        private static T Deserialize<T>(string xml, XmlRootAttribute xRoot)
        {
            T model = default(T);
            XmlSerializer serializer = new XmlSerializer(typeof(T), xRoot);
            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                model = (T)serializer.Deserialize(memStream);
            }
            return model;
        }

        private static string GetXmlDocumentByStream(MemoryStream stream)
        {
            string result = string.Empty;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(stream);
            result = xmlDocument.OuterXml;
            return result;
        }

        public static string Serialize<T>(T serializableObject) 
        {
            string result = string.Empty;
            if (serializableObject == null) { return result; }

            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

                using (TextWriter textWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, serializableObject);
                    result = textWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.LogException("XmlHelper.Serialize() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.Base", "Serialize object type of " + typeof (T).ToString());
            }

            return result;
        }
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }
    }
}
