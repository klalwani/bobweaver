﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public static class CommonHelper
    {
        public static DateTime? ParseDateTime(string date)
        {
            return DateTime.ParseExact(date, "MM/dd/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None);
        }

        public static DateTime StartOfDay(this DateTime theDate)
        {
            return theDate.Date;
        }

        public static DateTime EndOfDay(this DateTime theDate)
        {
            return theDate.Date.AddDays(1).AddTicks(-1);
        }


        public static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}
