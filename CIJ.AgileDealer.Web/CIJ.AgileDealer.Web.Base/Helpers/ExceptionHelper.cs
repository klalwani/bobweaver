﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.ErrorLog;
using CJI.AgileDealer.Web.Base.Context;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public static class ExceptionHelper
    {
        public static ErrorLog LogException(string exceptionPrefix, Exception exception, string exceptionCategory, TraceEventType exceptionSeverity, string application, string contextQueries)
        {
            ErrorLog el = new ErrorLog();
            try
            {
                Exception active = exception;
                int loopCount = 0;
                using (ServicePageDbContext dbLocal = ServicePageDbContext.Create())
                {
                    while (active != null && loopCount < 10)
                    {
                        if (active == active.InnerException)
                            break;

                        if (active.InnerException != null && active.Message == active.InnerException.Message)
                            break;

                        el.Message = $"{exceptionPrefix} {active.Message}";
                        el.SequenceId = loopCount;
                        el.Application = application;
                        el.ContextQueries = contextQueries;
                        el.Source = active.Source;
                        el.StackTrace = active.StackTrace;
                        el.DateCreated = DateTime.Now;

                        if (active.TargetSite != null)
                            el.TargetSite = active.TargetSite.ToString();

                        dbLocal.ErrorLogs.Add(el);

                        active = active.InnerException;
                        loopCount++;
                    }

                    dbLocal.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string ouch = ex.ToString();
            }
            return el;
        }
    }
}
