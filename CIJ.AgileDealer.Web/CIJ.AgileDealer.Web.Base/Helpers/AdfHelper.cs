﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Parts;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using Price = AgileDealer.Data.Entities.ADF.Price;
using AgileDealer.Data.Entities.Applicants;
using System.Configuration;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public static class AdfHelper
    {
        static int TimeDifference = Convert.ToInt32(ConfigurationManager.AppSettings["TimeDifference"]);

        public static ADF CreateAdfInformation(CustomerQuestion customerQuestion, Dealer dealer)
        {

            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = DateTime.Now.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),

                    Vehicles = new List<Vehicle>()
                    {
                        new Vehicle()
                        {
                            Interest = string.Empty,
                            Status = string.Empty,
                            Year = string.Empty,
                            Make = string.Empty,
                            Model = string.Empty,
                            Vin = string.Empty,
                            Stock = string.Empty,
                            Trim = string.Empty,
                            Doors = string.Empty,
                            Bodystyle = string.Empty,
                            Transmission = string.Empty,
                            Odometer = new Odometer()
                            {
                                Text = string.Empty,
                                Status = string.Empty,
                                Units = string.Empty
                            },
                            ColorCombination = new ColorCombination()
                            {
                                ExteriorColor = string.Empty,
                                InteriorColor = string.Empty,
                                Preference = string.Empty,
                            },
                            Price = new Price()
                            {
                                Type = string.Empty,
                                Currency = "USD"
                            }
                        }
                    },
                    Customer = new Customer()
                    {
                        Contact = new Contact
                        {
                            PrimaryContact = 1,
                            Email = customerQuestion.Email,
                            FirstName = customerQuestion.FirstName,
                            LastName = customerQuestion.LastName,
                            Address = new Address()
                            {
                                City = string.Empty,
                                Country = string.Empty,
                                PostalCode = string.Empty,
                                RegionCode = string.Empty,
                                Street = string.Empty
                            },
                            Names = new List<Name>()
                            {
                                new Name() {Part = "first", Text = customerQuestion.FirstName},
                                new Name() {Part = "last", Text = customerQuestion.LastName},
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone() {Type = "voice", Time = "day", Text = customerQuestion.PhoneNumber}
                            }
                        },
                        Comments = customerQuestion.Questions
                    },
                    Vendor = new Vendor()
                    {
                        VendorName = dealer.Name
                    },
                    Provider = new Provider()
                    {
                        Name = new Name()
                        {
                            Type = string.Empty,
                            Part = "full",
                            Text = "AgileDealer"
                        },
                        Service = "Dealer Website - CustomerQuestion",
                        Url = "www.AgileDealer.com",
                        Email = "support@AgileDealer.com",
                        Contact = new Contact()
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new  Name()
                                {
                                    Type = "Business",
                                    Part = "full",
                                    Text = "AgileDealer"
                                }
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Type = "voice",
                                    Time = "day",
                                    Text = "(813) 501-3229"
                                },
                                new Phone()
                                {
                                    Type = "fax",
                                    Time = "day",
                                    Text = "813-501-3229"
                                }
                            },
                            Address = new Address()
                            {
                                Street = "2705 Wilson Rd",
                                City = "Lutz",
                                Country = "US",
                                PostalCode = "33559-6933",
                                RegionCode = "FL",
                            }
                        }
                    }
                }
            };

            return adf;
        }

        public static ADF CreateAdfInformation(ScheduleService scheduleService, Dealer dealer)
        {

            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = string.Format("{0:D} {1}", scheduleService.ScheduleDate, string.IsNullOrEmpty(scheduleService.SaturdayHour) ? scheduleService.WeeklyHour : scheduleService.SaturdayHour),
                    Vehicles = new List<Vehicle>()
                    {
                        new Vehicle()
                        {
                            Interest =  "buy",
                            Status = scheduleService.WarrantyStatus.ToString().ToLower(),
                            Year = scheduleService.Year,
                            Make = scheduleService.Make,
                            Model = scheduleService.Model,
                            Vin = string.Empty,
                            Stock = string.Empty,
                            Trim = string.Empty,
                            Doors = string.Empty,
                            Bodystyle = string.Empty,
                            Transmission = string.Empty,
                            Odometer = new Odometer()
                            {
                                Text = string.Empty,
                                Status = string.Empty,
                                Units = string.Empty
                            },
                            ColorCombination = new ColorCombination()
                            {
                                ExteriorColor = string.Empty,
                                InteriorColor = string.Empty,
                                Preference = string.Empty,
                            },
                            Price = new Price()
                            {
                                Type = string.Empty,
                                Currency = "USD"
                            }
                        }
                    },
                    Customer = new Customer()
                    {
                        Contact = new Contact
                        {
                            PrimaryContact = 1,
                            Email = scheduleService.Email,
                            FirstName = scheduleService.FirstName,
                            LastName = scheduleService.LastName,
                            Address = new Address()
                            {
                                City = string.Empty,
                                Country = string.Empty,
                                PostalCode = string.Empty,
                                RegionCode = string.Empty,
                                Street = string.Empty
                            },
                            Names = new List<Name>()
                            {
                                new Name() {Part = "first", Text = scheduleService.FirstName},
                                new Name() {Part = "last", Text = scheduleService.LastName},
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone() {Type = "voice", Time = "day", Text = scheduleService.PhoneNumber}
                            }
                        },
                        Comments = scheduleService.AdditionalInformation
                    },
                    Vendor = new Vendor()
                    {
                        VendorName = dealer.Name
                    },
                    Provider = new Provider()
                    {
                        Name = new Name()
                        {
                            Type = string.Empty,
                            Part = "full",
                            Text = "AgileDealer"
                        },
                        Service = "Dealer Website - ScheduleService",
                        Url = "www.AgileDealer.com",
                        Email = "support@AgileDealer.com",
                        Contact = new Contact()
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new  Name()
                                {
                                    Type = "Business",
                                    Part = "full",
                                    Text = "AgileDealer"
                                }
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Type = "voice",
                                    Time = "day",
                                    Text = "(813) 501-3229"
                                },
                                new Phone()
                                {
                                    Type = "fax",
                                    Time = "day",
                                    Text = "813-501-3229"
                                }
                            },
                            Address = new Address()
                            {
                                Street = "2705 Wilson Rd",
                                City = "Land O Lakes",
                                Country = "US",
                                PostalCode = "34638",
                                RegionCode = "FL",
                            }
                        }
                    }
                }
            };

            return adf;
        }

        public static ADF CreateAdfInformation(VehicleFeedBack vehicleFeedBack, HomeNetVehicle vehicle, Dealer dealer)
        {
            var fullName = vehicleFeedBack.Name;
            var firstName = String.Empty;
            var lastName = String.Empty;

            if (fullName.IndexOf(" ") > 0)
            {
                firstName = fullName.Substring(0, fullName.IndexOf(" ")).Trim();
                lastName = fullName.Substring(fullName.IndexOf(" ") + 1).Trim();
            }
            else
                firstName = vehicleFeedBack.Name;

            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = vehicleFeedBack.SubmittedDate.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),

                    Vehicles = new List<Vehicle>()
                    {
                        new Vehicle()
                        {
                            Interest = "buy",
                            Status = vehicle.type.ToLower(),
                            Year = vehicle.year.ToString(),
                            Make = vehicle.make,
                            Model = vehicle.model,
                            Vin = vehicle.vin,
                            Stock = vehicle.stock,
                            Trim = vehicle.trim,
                            Doors = vehicle.doors.ToString(),
                            Bodystyle = vehicle.body,
                            Transmission = vehicle.trans,
                            Odometer = new Odometer()
                            {
                                Text = string.Empty,
                                Status = string.Empty,
                                Units = string.Empty
                            },
                            ColorCombination = new ColorCombination()
                            {
                                ExteriorColor = vehicle.extcolor,
                                InteriorColor = vehicle.intcolor,
                                Preference = string.Empty,
                            },
                            Price = new Price()
                            {
                                Text = vehicle.sellingprice.ToString(),
                                Type = string.Empty,
                                Currency = "USD"
                            }
                        }
                    },
                    Customer = new Customer()
                    {
                        Contact = new Contact
                        {
                            PrimaryContact = 1,
                            Email = vehicleFeedBack.Email,
                            FirstName = firstName,
                            LastName = lastName,
                            Address = new Address()
                            {
                                City = string.Empty,
                                Country = string.Empty,
                                PostalCode = string.Empty,
                                RegionCode = string.Empty,
                                Street = string.Empty
                            },
                            Names = new List<Name>()
                            {
                                new Name() {Part = "first", Text = firstName},
                                new Name() {Part = "last", Text = lastName},
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone() {Type = "voice", Time = "day", Text = vehicleFeedBack.Phone}
                            }
                        },
                        Comments = vehicleFeedBack.Message
                    },
                    Vendor = new Vendor()
                    {
                        VendorName = dealer.Name
                    },
                    Provider = new Provider()
                    {
                        Name = new Name()
                        {
                            Type = string.Empty,
                            Part = "full",
                            Text = "AgileDealer"
                        },
                        Service = "Dealer Website - Vehicle",
                        Url = "www.AgileDealer.com",
                        Email = "support@AgileDealer.com",
                        Contact = new Contact()
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new  Name()
                                {
                                    Type = "Business",
                                    Part = "full",
                                    Text = "AgileDealer - Vehicle"
                                }
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Type = "voice",
                                    Time = "day",
                                    Text = "(813) 501-3229"
                                },
                                new Phone()
                                {
                                    Type = "fax",
                                    Time = "day",
                                    Text = "813-501-3229"
                                }
                            },
                            Address = new Address()
                            {
                                Street = "2705 Wilson Rd",
                                City = "Land O Lakes",
                                Country = "US",
                                PostalCode = "34638",
                                RegionCode = "FL",
                            }
                        }
                    }
                }
            };

            return adf;
        }

        public static ADF CreateAdfInformation(PartRequest partRequest, Dealer dealer)
        {

            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = DateTime.Now.AddHours(-TimeDifference).ToString(),

                    Vehicles = new List<Vehicle>()
                    {
                        new Vehicle()
                        {
                            Interest =  string.Empty,
                            Status = string.Empty,
                            Year = partRequest.Year,
                            Make = partRequest.Make,
                            Model = partRequest.Model,
                            Vin = partRequest.Vin,
                            Stock = string.Empty,
                            Trim = string.Empty,
                            Doors = string.Empty,
                            Bodystyle = string.Empty,
                            Transmission = string.Empty,
                            Odometer = new Odometer()
                            {
                                Text = string.Empty,
                                Status = string.Empty,
                                Units = string.Empty
                            },
                            ColorCombination = new ColorCombination()
                            {
                                ExteriorColor = string.Empty,
                                InteriorColor = string.Empty,
                                Preference = string.Empty,
                            },
                            Price = new Price()
                            {
                                Type = string.Empty,
                                Currency = "USD"
                            }
                        }
                    },
                    Customer = new Customer()
                    {
                        Contact = new Contact
                        {
                            PrimaryContact = 1,
                            Email = partRequest.Email,
                            FirstName = partRequest.FirstName,
                            LastName = partRequest.LastName,
                            Address = new Address()
                            {
                                City = string.Empty,
                                Country = string.Empty,
                                PostalCode = string.Empty,
                                RegionCode = string.Empty,
                                Street = string.Empty
                            },
                            Names = new List<Name>()
                            {
                                new Name() {Part = "first", Text = partRequest.FirstName},
                                new Name() {Part = "last", Text = partRequest.LastName},
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone() {Type = "voice", Time = "day", Text = partRequest.PhoneNumber}
                            }
                        },
                        Comments = partRequest.PartNumber
                    },
                    Vendor = new Vendor()
                    {
                        VendorName = dealer.Name
                    },
                    Provider = new Provider()
                    {
                        Name = new Name()
                        {
                            Type = string.Empty,
                            Part = "full",
                            Text = "AgileDealer"
                        },
                        Service = "Dealer Website - PartRequest",
                        Url = "www.AgileDealer.com",
                        Email = "support@AgileDealer.com",
                        Contact = new Contact()
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new  Name()
                                {
                                    Type = "Business",
                                    Part = "full",
                                    Text = "AgileDealer"
                                }
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Type = "voice",
                                    Time = "day",
                                    Text = "(813) 501-3229"
                                },
                                new Phone()
                                {
                                    Type = "fax",
                                    Time = "day",
                                    Text = "813-501-3229"
                                }
                            },
                            Address = new Address()
                            {
                                Street = "2705 Wilson Rd",
                                City = "Land O Lakes",
                                Country = "US",
                                PostalCode = "34638",
                                RegionCode = "FL",
                            }
                        }
                    }
                }
            };

            return adf;
        }


        public static ADF CreateAdfInformationContact(ContactInfo contactInfo,ApplicantInfo applicantInfo, Dealer dealer)
        {

            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = DateTime.Now.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),
                    Customer = new Customer()
                    {
                        Contact = new Contact
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new Name() {Part = "first", Text = applicantInfo.FirstName},
                                new Name() {Part = "last", Text = applicantInfo.LastName},
                            },
                            Email = applicantInfo.Email,
                            Phones = new List<Phone>()
                            {
                                new Phone() {Type = "voice", Time = "day", Text = applicantInfo.PhoneNumber}
                            },
                            Address = new Address()
                            {
                                City = contactInfo.City,
                                Country = "US",
                                PostalCode = contactInfo.ZipCode,
                                RegionCode = contactInfo.State,
                                Street = contactInfo.CurrentAddress
                            }
                        },
                        Comments = "Finance application (https://www.bobweaverauto.com/admin/leaddashboard) submitted @ " + DateTime.UtcNow.AddHours(-4).ToString(),
                    },
                    Vendor = new Vendor()
                    {
                        VendorName = dealer.Name
                    },
                    Provider = new Provider()
                    {
                        Name = new Name()
                        {
                            Type = string.Empty,
                            Part = "full",
                            Text = "AgileDealer"
                        },
                        Service = "Dealer Website - Finance",
                        Url = "www.AgileDealer.com",
                        Email = "support@AgileDealer.com",
                        Contact = new Contact()
                        {
                            PrimaryContact = 1,
                            Names = new List<Name>()
                            {
                                new  Name()
                                {
                                    Type = "Business",
                                    Part = "full",
                                    Text = "AgileDealer - Finance"
                                }
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone()
                                {
                                    Type = "voice",
                                    Time = "day",
                                    Text = "(813) 501-3229"
                                },
                                new Phone()
                                {
                                    Type = "fax",
                                    Time = "day",
                                    Text = "813-501-3229"
                                }
                            },
                            Address = new Address()
                            {
                                Street = "2705 Wilson Rd",
                                City = "Land O Lakes",
                                Country = "US",
                                PostalCode = "34638",
                                RegionCode = "FL",
                            }
                        }
                    }
                }
            };

            return adf;
        }

    }
}