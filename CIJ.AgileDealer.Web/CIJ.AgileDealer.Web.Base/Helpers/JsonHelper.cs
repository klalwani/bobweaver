﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CIJ.AgileDealer.Web.Base.Helpers
{
    public class JsonHelper
    {
        public static string Serialize<T>(T serializableObject)
        {
            string result = string.Empty;
            if (serializableObject == null) { return result; }

            try
            {
                JsonSerializer jsonSerializer = new JsonSerializer();

                using (StringWriter textWriter = new StringWriter())
                {
                    jsonSerializer.Serialize(textWriter, serializableObject);
                    result = textWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.LogException("JsonHelper.Serialize() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.Base", "Serialize object type of " + typeof(T).ToString());
            }

            return result;
        }

        public static T Deserialize<T>(string serializableObject)
        {
            T model = default(T);
            model = JsonConvert.DeserializeObject<T>(serializableObject);
            return model;
        }
    }
}
