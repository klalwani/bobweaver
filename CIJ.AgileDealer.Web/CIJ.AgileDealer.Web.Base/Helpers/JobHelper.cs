﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Enums;
using AgileDealer.Data.Entities.Jobs;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.Base.Helpers
{
    public class JobHelper
    {
        private static char[] charSeparators = { ';' };

        public JobHelper()
        {

        }

        public int Insert(JobTracking job)
        {
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    db.JobTrackings.Add(job);
                    db.SaveChanges();
                    return job.JobTrackingId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(JobTracking job)
        {
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    job.ModifiedDt = DateTime.Now;
                    db.Entry(job).State = EntityState.Modified;
                    db.SaveChanges();
                    return job.JobTrackingId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool LogTheJob(string jobName, string triggeredBy, string status)
        {
            bool result = true;
            JobTracking currentJob = GetCurrentRunningOrTriggerdJob(jobName);
            if (currentJob != null)
            {
                currentJob.Status = status;
                currentJob.ModifiedDt = DateTime.Now;

                if (status == JobStatusEnum.Running.ToString())
                {
                    currentJob.StarTime = DateTime.Now;
                }
                if (status == JobStatusEnum.Completed.ToString())
                {
                    currentJob.EndTime = DateTime.Now;
                }

                Update(currentJob);
            }
            else
            {
                JobTracking newJob = CreateJobTracking(jobName, triggeredBy, status);
                if (status == JobStatusEnum.Running.ToString())
                {
                    newJob.StarTime = DateTime.Now;
                }
                Insert(newJob);
            }
            return result;
        }

        public JobTracking CreateJobTracking(string jobName, string triggeredBy, string status)
        {
            JobTracking jobTracking = new JobTracking();
            jobTracking.JobName = jobName;
            jobTracking.TriggerBy = triggeredBy;
            jobTracking.Status = status;
            jobTracking.TriggeredTime = DateTime.Now;
            jobTracking.CreatedDt = DateTime.Now;
            jobTracking.ModifiedDt = DateTime.Now;
            return jobTracking;
        }

        public JobTracking GetJobTracking(string jobName)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.JobTrackings.FirstOrDefault(x => x.JobName == jobName);//Get the latest job
            }
        }

        public JobTracking GetCurrentRunningOrTriggerdJob(string jobName)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.JobTrackings.FirstOrDefault(x => x.JobName == jobName && (x.Status == JobStatusEnum.Triggered.ToString()
                || x.Status == JobStatusEnum.Running.ToString()));//Get the latest job
            }
        }


        public bool HasCurrentRunningJob(string jobName)
        {
            bool result = false;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                result = db.JobTrackings.Any(x => x.JobName == jobName && x.Status == JobStatusEnum.Running.ToString());
            }
            return result;
        }

        public List<JobTracking> GetJobList(int skip = 0, int take = 0)
        {
            List<JobTracking> jobTrackings;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                if (take != 0)
                {
                    jobTrackings = db.JobTrackings.OrderByDescending(x => x.CreatedDt).Skip(skip).Take(take).ToList();
                }
                else
                {
                    jobTrackings = db.JobTrackings.OrderByDescending(x => x.CreatedDt).ToList();
                }
            }
            return jobTrackings;
        }
        public static JobScheduler GetJobSchedulers(string jobName)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.JobSchedulers.FirstOrDefault(x=>x.JobName.Equals(jobName,StringComparison.OrdinalIgnoreCase));
            }
        }

        public static string[] GetScheduleTimeOfJob(string jobName,string defaultTimeList)
        {
            string[] timers = new string[10];
            if (!string.IsNullOrEmpty(jobName))
            {
                JobScheduler currentJob = GetJobSchedulers(jobName);
                if (currentJob != null)
                {
                    timers = currentJob.TimeList.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    timers = new[] {defaultTimeList};
                }
            }
            return timers;
        }
    }
}
