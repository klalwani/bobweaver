﻿using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Model;
using CJI.AgileDealer.Web.Base.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CIJ.AgileDealer.Web.Base.Repositories
{
    public class VehicleRepository
    {
        public void UploadAgileVehicleImage(string storageConnectionString, string uploadFolder,string downloadFolder, string blobContainer)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            var cacheControl = "public, max-age=604800";
            string[] fileArray = Directory.GetFiles(downloadFolder, "*.*");

            List<string> listFileName = fileArray.Select(Path.GetFileName).ToList();

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(blobContainer);

            //var list = container.ListBlobs(useFlatBlobListing: true);
            //List<string> listOfFileNames = new List<string>();
            //foreach (IListBlobItem item in list)
            //{
            //    var blobFileName = item.Uri.Segments.Last();
            //    listOfFileNames.Add(blobFileName);
            //}

            //List<string> newFileList = listFileName.Except(listOfFileNames).ToList();
            if (listFileName.Any())
            {
                foreach (var item in listFileName)
                {
                    var dir = Path.Combine(downloadFolder, item);

                    using (var fileStream = File.OpenRead(dir))
                    {
                        //Create new blob for new item
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(uploadFolder + item);
                        blockBlob.Properties.CacheControl = cacheControl;
                        blockBlob.UploadFromStream(fileStream);
                    }
                }
            }
        }
        public HomeNetVehicle GetVehicleDetail(string vin, string userId) { 
        
            HomeNetVehicle vehicle = null;

            using(var db = new ServicePageDbContext())
            {
                vehicle = db.Vehicles.SingleOrDefault(a => string.Equals(a.vin, vin, StringComparison.OrdinalIgnoreCase));

                if (vehicle != null)
                {
                    // record history
                    db.VehicleViewHistories.Add(
                        new VechicleViewHistory { Created_Dt = DateTime.Now,
                        User_Id = userId, Vehicle_Id = vehicle.Id});
                }

            }

            return vehicle;
        }

        // return top 50
        public List<VehicleSummary>  GetAlsoViewVehicles(string vin)
        {
            var alsoView = new List<VehicleSummary>();

            using (var db = new ServicePageDbContext())
            {
                // get all customers for this vin-*/
                var qcust = db.VehicleViewHistories.
                    Where(a => string.Equals(a.Vehicle.vin, vin, StringComparison.OrdinalIgnoreCase))
                    .Select(a => a.User_Id);

                // then get all vehicles viewed by these cutomers
                var qvehicle = from history in db.VehicleViewHistories
                               where qcust.Any(a=>a.Equals(history.User_Id))
                               select new VehicleSummary
                               {
                                   body = history.Vehicle.body,
                                   extcolor = history.Vehicle.extcolor,
                                   miles = history.Vehicle.miles,
                                   sellingprice = history.Vehicle.sellingprice,
                                   stockimage = history.Vehicle.stockimage,
                                   vin = history.Vehicle.vin
                               };

                alsoView = qvehicle.Distinct().Skip(0).Take(50).ToList();
            }

            return alsoView;
        }

        // return top 50
        public List<VehicleSummary> GetRecentViewVehicles(string vin)
        {
            var alsoView = new List<VehicleSummary>();

            using (var db = new  ServicePageDbContext())
            {
                // get 50 recent view vehicles
                var q = from history in db.VehicleViewHistories
                        orderby history.Id descending
                        select new VehicleSummary
                        {
                            body = history.Vehicle.body,
                            extcolor = history.Vehicle.extcolor,
                            miles = history.Vehicle.miles,
                            sellingprice = history.Vehicle.sellingprice,
                            stockimage = history.Vehicle.stockimage,
                            vin = history.Vehicle.vin
                        };

                alsoView = q.Distinct().Skip(0).Take(50).ToList();
            }

            return alsoView;
        }
    }
}