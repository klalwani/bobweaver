﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIJ.AgileDealer.Web.Base.Model;
using CJI.AgileDealer.Web.Base.Context;

namespace CIJ.AgileDealer.Web.Base.Repositories
{
    public class NavigationRepository
    {
        public List<NavigationModel> GetNavigation(int dealerId, string url)
        {
            using (var servicePageDbContext = new ServicePageDbContext())
            {
                var navigationsQuery = servicePageDbContext.Navigations.OrderBy(c => c.DisplayOrderId);
                var query = from navigation in navigationsQuery
                            join menunavigation in servicePageDbContext.MenuExNavigations on navigation.NavigationId equals
                                menunavigation.NavigationId
                            join menu in servicePageDbContext.Menus on menunavigation.MenuId equals menu.MenuId
                            where menu.Url == url && menunavigation.DealerID == dealerId
                            select new NavigationModel()
                            {
                                Name = navigation.Name,
                                CssClass = string.IsNullOrEmpty(navigation.CssClass) ? string.Empty : navigation.CssClass,
                                ActionLink = navigation.ActionLink
                            };
                List<NavigationModel> navigations = query.ToList();
                if (!navigations.Any())
                {
                    navigations = navigationsQuery.Select(c => new NavigationModel()
                    {
                        Name = c.Name,
                        CssClass = string.IsNullOrEmpty(c.CssClass) ? string.Empty : c.CssClass,
                        ActionLink = c.ActionLink
                    }).ToList();
                }
                return navigations;
            }

        }
    }
}
