﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIJ.AgileDealer.Web.Base.Model
{
    public class VehicleSummary
    {
        public string vin { get; set; }
        public string extcolor { get; set; }
        public int miles { get; set; }
        public int sellingprice { get; set; }
        public string stockimage { get; set; }
        public string body { get; set; }

    }
}
