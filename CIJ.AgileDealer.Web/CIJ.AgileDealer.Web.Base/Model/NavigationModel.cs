﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIJ.AgileDealer.Web.Base.Model
{
    public class NavigationModel
    {
        public string ActionLink { get; set; }
        public string Name { get; set; }

        public string CssClass { get; set; }
    }
}
