﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Competitives
{
    [Serializable]
    public class CompetitiveModel : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompetitiveModelId { get; set; }

        [XmlElement("Name")]
        [MaxLength(128)]
        public string Name { get; set; }

        [XmlElement("MakeName")]
        [MaxLength(128)]
        public string MakeName { get; set; }

        [XmlElement("Segment")]
        [MaxLength(128)]
        public string Segment { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }
    }
}
