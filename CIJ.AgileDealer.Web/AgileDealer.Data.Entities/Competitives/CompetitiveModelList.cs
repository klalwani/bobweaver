﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Competitives
{
    [Serializable]
    [XmlRoot("Models")]
    public class CompetitiveModelList
    {
        [NotMapped]
        [XmlElement("Model")]
        public List<CompetitiveModel> CompetitiveModels { get; set; }
    }
}
