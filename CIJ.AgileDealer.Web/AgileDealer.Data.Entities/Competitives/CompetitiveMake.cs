﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Competitives
{
    [Serializable]
    public class CompetitiveMake : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompetitiveMakeId { get; set; }

        [MaxLength(128)]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }
    }
}
