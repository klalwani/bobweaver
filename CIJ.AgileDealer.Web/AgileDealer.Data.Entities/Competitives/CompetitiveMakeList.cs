﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Competitives
{
    [Serializable]
    [XmlRoot("Makes")]  
    public class CompetitiveMakeList
    {
        [NotMapped]
        [XmlElement("Make")]
        public List<CompetitiveMake> CompetitiveMakes { get; set; }
    }
}
