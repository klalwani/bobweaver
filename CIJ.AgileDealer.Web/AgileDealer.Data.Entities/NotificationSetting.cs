﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class NotificationSetting: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NotificationSettingId { get; set; }
        public string AdfEmail { get; set; }
        public string PlainTextEmail { get; set; }
        public bool HasAdfEmail { get; set; }
        public bool HasPlainTextEmail { get; set; }
        public string LeadType { get; set; }
    }
}
