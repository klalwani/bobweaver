﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.CMS
{
    public class ContentPagePostData: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ContentPagePostDataId { get; set; }
        /// <summary>
        /// Post Data - field name that contains the email value
        /// </summary>
        [MaxLength(128)]
        public string PostEmailFieldName { get; set; }

        /// <summary>
        /// Post Data - field name that contains user name such as first name or username
        /// </summary>
        [MaxLength(128)]
        public string PostUserFieldName { get; set; }

        /// <summary>
        /// Post Data - field contain the title for the post.  This will be used as subject line to email
        /// </summary>
        [MaxLength(128)]
        public string PostTitle { get; set; }

        /// <summary>
        /// Post Data
        /// </summary>
        [MaxLength(128)]
        public string PostToMethod { get; set; }

        /// <summary>
        /// Post Data - field contain the actual email for the designed user
        /// </summary>
        [MaxLength(128)]
        public string PosToEmailValue { get; set; }

        /// <summary>
        /// Post Data - field contain the actual email for the CC user
        /// </summary>
        [MaxLength(128)]
        public string PosToEmailCCValue { get; set; }

        /// <summary>
        /// Post Data - maybe we go fancy later on by setting the format up front here
        /// </summary>
        [MaxLength(512)]
        public string PostDataFormat { get; set; }

        /// <summary>
        /// Post Data - field will be used to show the response of a post... Usually display thank message should be suffice
        /// </summary>
        public string PostResponseText { get; set; }

		/// <summary>
		/// Post Data - field contain the actual email for the BCC user
		/// </summary>
		[MaxLength(128)]
		public string PostToEmailBCCValue { get; set; }

		public virtual ContentPage ContentPage { get; set; }
    }
}
