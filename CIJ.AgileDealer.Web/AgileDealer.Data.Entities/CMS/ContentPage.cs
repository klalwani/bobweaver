﻿using AgileDealer.Data.Entities.Nagivation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.CMS
{
    public class ContentPage: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContentPageId { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }

        [MaxLength(256)]
        public string Canonical { get; set; }

		[MaxLength(256)]
		public string AmpLink { get; set; }

		public int MenuId { get; set; }

        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }

		/// <summary>
		/// Content of this page
		/// </summary>
		public string BodyContent { get; set; }
        public string BodyContentBottom { get; set; }
        public bool ShowInventory { get; set; }
        public string InventoryFilters { get; set; }

        public virtual ContentPagePostData PostData { get; set; }
        
        public bool IsActive { get; set; }

		public bool IsAdminOnly { get; set; } // True means only admin can see.  This is for previewing purposes.

	}
}
