﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Buyer
{
    public class BuyerSession : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BuyerSessionId { get; set; }
        public Guid BuyerIdentifier { get; set; }
        public string IpAddress { get; set; }
        public string SessionId { get; set; }
        public string BrowserType { get; set; }
        public string PlatformOs { get; set; }
        public string MobileDeviceManufacturer { get; set; }
        public string MobileDeviceModel { get; set; }
        public bool IsMobile { get; set; }
        public string UrlReferer { get; set; }
        public string Url { get; set; }
        public string GoogleClientId { get; set; }
    }
}
