﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Buyer
{
    public class BuyerPageVisited : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BuyerPageVisitedId { get; set; }
        
        public int BuyerSessionId { get; set; }
        public string UrlReferer { get; set; }
        public string Url { get; set; }

        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }
    }
}
