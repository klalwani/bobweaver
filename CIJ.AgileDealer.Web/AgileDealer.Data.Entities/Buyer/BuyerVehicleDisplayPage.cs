﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Buyer
{
    public class BuyerVehicleDisplayPage : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BuyVehicleDisplayPageId { get; set; }
        public int BuyerSessionId { get; set; }
        public int AgileVehicleId { get; set; }

        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }
    }
}
