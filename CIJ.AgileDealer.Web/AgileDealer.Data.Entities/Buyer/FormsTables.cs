﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Buyer
{
    public class FormsTables : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FormsTablesId { get; set; }
        public bool IsComplete { get; set; }
        public int BuyerSessionId { get; set; }

        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }
    }
}
