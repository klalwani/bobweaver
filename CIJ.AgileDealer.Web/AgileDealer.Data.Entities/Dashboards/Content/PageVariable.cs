﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Content
{
    public class PageVariable :EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PageVariableId { get; set; }

        [MaxLength(1000)]
        public string PageName { get; set; }

        [MaxLength(1000)]
        public string Title { get; set; }

        [MaxLength(1000)]
        public string ParentLink { get; set; }

        [MaxLength(1000)]
        public string AmpLink { get; set; }

        [MaxLength(2000)]
        public string Description { get; set; } 
    }
}
