﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class Header: EntityBase
    {
        public int HeaderID { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
        public string SiteLogo { get; set; }
        public string SiteLogoAltText { get; set; }
        public string SiteLink { get; set; }
        public string PersonalizationText { get; set; }
        public string PersonalizationLogo { get; set; }
        public string PeronsalizationAltText { get; set; }
        public string PeronsalizationUrl { get; set; }
        public string CTAImage { get; set; }
        public string CTAImageAltText { get; set; }
        public string CTALink { get; set; }
        public string ExitSignButton { get; set; }

    }
}
