﻿using System;

namespace AgileDealer.Data.Entities.Content
{
    public class HourType : EntityLku
    {
        public int HourTypeID { get; set; }
    }
}
