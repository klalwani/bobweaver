﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Content
{
    public class Employee_Department : Entity
    {
        public int EmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public string Title { get; set; }
        public string Sequence { get; set; }
        public bool IsManager { get; set; }
    }
}
