﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class Hours: EntityBase
    {
        public int HoursID { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        [MaxLength(32)]
        public string LongDayName { get; set; }

        [MaxLength(16)]
        public string ShortDayName { get; set; }

        public int Order { get; set; }

        [MaxLength(16)]
        public string StartTime { get; set; }

        [MaxLength(16)]
        public string EndTime { get; set; }

        [MaxLength(16)]
        public string OperationStatus { get; set; }

        public int HourTypeID { get; set; }

        [ForeignKey("HourTypeID")]
        public HourType HourType { get; set; }

        [NotMapped]
        public string Time { get; set; }

    }
}
