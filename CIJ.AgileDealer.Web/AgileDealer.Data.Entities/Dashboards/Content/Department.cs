﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Content
{
    public class Department : EntityLku
    {
        public int DepartmentId { get; set; }
        public int DealerID { get; set; }
        public int Sequence { get; set; }
    }
}
