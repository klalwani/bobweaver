﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AgileDealer.Data.Entities.Nagivation;

namespace AgileDealer.Data.Entities.Content
{
    public class Dealer: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DealerID { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(512)]
        public string Description { get; set; }

        [MaxLength(256)]
        public string QuestionEmailAddress { get; set; }

        [MaxLength(256)]
        public string EleadTrackAdress { get; set; }

        public int ManufactureID {get;set;}

        [ForeignKey("ManufactureID")]
        public Manufacture Manufacture { get; set; }

        [MaxLength(256)]
        public string ImageServer { get; set; }

        [MaxLength(1024)]
        public string VehicleLookupLink { get; set; }

        [MaxLength(1024)]
        public string VehicleLookupLogo { get; set; }

        [MaxLength(4000)]
        public string JsonId { get; set; }

        public List<Location> Locations { get; set; }

        public virtual ICollection<MenuExNavigation> MenuExNavigations { get; set; }

        public virtual ICollection<RoleExMenu> RoleExMenus { get; set; }

        public virtual ICollection<MenuExclusion> MenuExclusions { get; set; }

        [MaxLength(256)]
        public string VcpImageStockServer { get; set; }

        [MaxLength(256)]
        public string ImageServerGeneric { get; set; }
        public bool IsPrimary { get; set; }

		[MaxLength(256)]
		public string DealerLogo { get; set; }

		public string DealerLogAltText { get; set; }

		public int? IncentiveDisplayTypeId { get; set; }

        public bool ShowTargetedProgramList { get; set; } = false;
    }
}
