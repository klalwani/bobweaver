﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class Footer: EntityBase
    {
        public int FooterID { get; set; }
        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
        public string FacebookLinkImage { get; set; }
        public string FacebookLinkImageAltText { get; set; }
        public string FacebookLink { get; set; }
        public string GooglePlusImage { get; set; }
        public string GooglePlusImageAltText { get; set; }
        public string GooglePlusLink { get; set; }
        public string TwitterImage { get; set; }
        public string TwitterImageAltText { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLinkImage { get; set; }
        public string InstagramLinkImageAltText { get; set; }
        public string InstagramLink { get; set; }
    }
}
