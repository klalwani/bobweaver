﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AgileDealer.Data.Entities.Nagivation;

namespace AgileDealer.Data.Entities.Content
{
    public class PageMetaData : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PageMetaDataId { get; set; }
        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
        [MaxLength(1000)]
        public string 	PageName { get; set; }
        [MaxLength(1000)]
        public string  Title { get; set; }
        [MaxLength(1000)]
        public string 	ParentLink { get; set; }
        [MaxLength(1000)]
        public string 	AmpLink { get; set; }
        [MaxLength(1000)]
        public string 	Description { get; set; }

    }
}
