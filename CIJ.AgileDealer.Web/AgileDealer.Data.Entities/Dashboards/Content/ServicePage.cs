﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AgileDealer.Data.Entities.Content
{
    public class ServicePage: EntityBase
    {
        public int ServicePageID { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        public int DisplayPageID { get; set; }

        [ForeignKey("DisplayPageID")]
        public DisplayPage DisplayPage { get; set; }

        public string OilContent{ get; set; }

        [MaxLength(128)]
        public string OilImage { get; set; }

        public string BrakeContent { get; set; }

        [MaxLength(128)]
        public string BrakeImage { get; set; }

        public string TireContent { get; set; }

        [MaxLength(128)]
        public string TireImage { get; set; }


    }
}
