﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class DisplayPage: EntityBase
    {
        public int DisplayPageID { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(512)]
        public string Description { get; set; }

        public int PageLayoutID { get; set; }

        [ForeignKey("PageLayoutID")]
        public PageLayout PageLayout { get; set; }

        //[MaxLength(1024)]
        public string MainContent1 { get; set; }

        //[MaxLength(1024)]
        public string MainContent2 { get; set; }

        //[MaxLength(1024)]
        public string MainContent3 { get; set; }

    }
}
