﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Content
{
    public class Hour
    {
        public string ShortDayName { get; set; }
        public string Time { get; set; }

        public int HourTypeID { get; set; }
    }
}
