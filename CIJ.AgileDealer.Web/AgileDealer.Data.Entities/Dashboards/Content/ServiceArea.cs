﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class ServiceArea: EntityBase
    {
        public int ServiceAreaID { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        public int Order { get; set; }
        [MaxLength(100)]
        public string TargetCity { get; set; }
        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(16)]
        public string State { get; set; }
        [MaxLength(16)]
        public string Zip { get; set; }
    }
}
