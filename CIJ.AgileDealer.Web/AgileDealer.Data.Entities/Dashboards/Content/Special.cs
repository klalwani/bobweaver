﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AgileDealer.Data.Entities.Content
{
    public class Special: EntityBase
    {
        public int SpecialID { get; set; }
        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(128)]
        public string Description { get; set; }

        public int ServiceTypeID { get; set; }

        [ForeignKey("ServiceTypeID")]
        public ServiceType ServiceType { get; set; }

        [MaxLength(4000)]
        public string Disclaimer { get; set; }

        public decimal Price { get; set; }

        [MaxLength(128)]
        public string Image { get; set; }

        public DateTime Expire { get; set; }
    }
}
