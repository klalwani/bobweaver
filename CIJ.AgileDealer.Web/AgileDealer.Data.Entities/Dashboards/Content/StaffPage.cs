﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Migrations.Model;

namespace AgileDealer.Data.Entities.Content
{
    public class StaffPage : EntityBase                    
    {
        [Key]
        public int StaffPageId { get; set; }
        public int DisplayPageId { get; set; }
        public bool ShowOfficePhone { get; set; }
        public bool ShowPhoto { get; set; }
        public bool ShowBio { get; set; }
        public bool ShowTabs { get; set; }
        public bool ShowAllTabs { get; set; }

    }
}
