﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class Location: EntityBase
    {
        public int LocationID { get; set; }

        public int LocationTypeID { get; set; }

        [ForeignKey("LocationTypeID")]
        public LocationType LocationType { get; set; }

        public int DealerID { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

       
        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(512)]
        public string Description { get; set; }

        [MaxLength(100)]
        public string Street1 { get; set; }

        [MaxLength(100)]
        public string Street2 { get; set; }
        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(16)]
        public string State { get; set; }
        [MaxLength(16)]
        public string Zip { get; set; }

        [MaxLength(64)]
        public string MainPhone { get; set; }

        [MaxLength(64)]
        public string AlternatePhone { get; set; }

        [MaxLength(64)]
        public string SalesPhone { get; set; }

        [MaxLength(64)]
        public string ServicePhone { get; set; }

        [MaxLength(64)]
        public string PartsPhone { get; set; }
        

    }
}
