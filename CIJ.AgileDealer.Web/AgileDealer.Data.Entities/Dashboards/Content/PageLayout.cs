﻿
using System.ComponentModel.DataAnnotations;
namespace AgileDealer.Data.Entities.Content
{
    public class PageLayout: EntityLku
    {
        public int PageLayoutID { get; set; }
    
        [MaxLength(128)]
        public string Image { get; set; }
    }
}
