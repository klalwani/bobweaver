﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Content
{
    public class Employee : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }
        public int DealerID { get; set; }

        [Description("First Name")]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Description("Last Name")]
        [MaxLength(100)]
        public string LastName { get; set; }

        public string Bio { get; set; }
        public string Photo { get; set; }
        [MaxLength(64)]
        public string OfficePhone { get; set; }
        [MaxLength(10)]
        public string Ext { get; set; }
        [MaxLength(64)]
        public string CellPhone { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsNew { get; set; }
        public int? Order { get; set; }
        public string ImageName { get; set; }


        [NotMapped]
        public List<Employee_Department> Departments { get; set; }
    }
}
