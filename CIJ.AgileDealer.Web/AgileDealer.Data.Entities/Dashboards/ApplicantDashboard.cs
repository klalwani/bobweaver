﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Dashboards
{
    public class ApplicantDashboard
    {
        public int ApplicantId { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string ApplicantType { get; set; }
        public int? NumberOfApplicants { get; set; }
        public DateTime DateCreated { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public bool IsArchived { get; set; }
        public bool IsCompleted { get; set; }
    }
}
