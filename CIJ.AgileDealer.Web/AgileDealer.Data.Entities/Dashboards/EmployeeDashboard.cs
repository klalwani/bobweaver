﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Dashboards
{
    public class EmployeeDashboard
    {
        public string EmployeeId { get; set; }
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string IsNew { get; set; }
        public string IsDeleted { get; set; }
        public string DateCreated { get; set; }
    }
}
