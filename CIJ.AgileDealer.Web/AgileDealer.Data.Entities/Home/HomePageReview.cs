﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageReview : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageReviewId { get; set; }

        public int HomePageId { get; set; }

        public int StartRating { get; set; }

        public string ReviewText { get; set; }

        [MaxLength(128)]
        public string ReviewName { get; set; }

        [MaxLength(512)]
        public string ReviewRelateUrl { get; set; }
    }
}
