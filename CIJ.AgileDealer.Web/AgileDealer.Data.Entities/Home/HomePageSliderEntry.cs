﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageSliderEntry : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SliderEntryId { get; set; }
        public int HomePageId { get; set; }
        public string HomePageSliderName { get; set; }
        public string HomePagePosition { get; set; }
        public bool IsActive { get; set; }
    }
}
