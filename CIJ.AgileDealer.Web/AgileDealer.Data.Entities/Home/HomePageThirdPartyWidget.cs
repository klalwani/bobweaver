﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageThirdPartyWidget : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageThirdPartyWidgetId { get; set; }

        public int HomePageId { get; set; }

        [MaxLength(128)]
        public string WidgetName { get; set; }

        public string WidgetHtml { get; set; }

        public string HomePagePosition { get; set; }
    }
}
