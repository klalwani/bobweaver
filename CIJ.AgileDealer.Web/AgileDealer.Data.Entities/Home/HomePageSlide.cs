﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageSlide : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SlideId { get; set; }
        public int SlideEntryId { get; set; }
        public string SlideName { get; set; }
        public int SlideNumber { get; set; }
        public string SlideImageName { get; set; }
        public string SlideImagePath { get; set; }
        public string SlideLink { get; set; }
        public string SlideRelativeUrl { get; set; }

        [NotMapped]
        public bool IsFirstItem { get; set; }

        [NotMapped]
        public bool IsLastItem { get; set; }
    }
}
