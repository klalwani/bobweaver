﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePage : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageId { get; set; }

        public int DealerID { get; set; }

         
        public string TopMessage { get; set; }

        [MaxLength(128)]
        public string SearchBoxFilterText { get; set; }
        
        [MaxLength(512)]
        public string BioHeading { get; set; }

        
        public string BioText { get; set; }
    }
}
