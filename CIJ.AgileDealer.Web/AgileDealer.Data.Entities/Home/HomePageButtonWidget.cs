﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageButtonWidget : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageButtonWidgetId { get; set; }

        public int HomePageId { get; set; }
        public string HomePageButtonWidgetName { get; set; }
        public int HomePageButtonTypeId { get; set; }
    }
}
