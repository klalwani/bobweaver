﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageVehicle : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageVehicleId { get; set; }
        public int HomePageVehicleCatoryId { get; set; }
        public string VehicleImage { get; set; }
        public string VehicleName { get; set; }
        public string VehicleText { get; set; }
        public string VehicleRelativeUrl { get; set; }
    }
}
