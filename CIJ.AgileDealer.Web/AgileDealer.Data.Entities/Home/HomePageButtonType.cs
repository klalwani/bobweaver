﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageButtonType:EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageButtonTypeId { get; set; }
    }
}
