﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    public class HomePageButton : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageButtonId { get; set; }

        public int HomePageButtonWidgetId { get; set; }

        public string ButtonImagePath { get; set; }

        [MaxLength(128)]
        public string ButtonImageText { get; set; }

        public string ButtonImageRelativeUrl { get; set; }

        public int ButtonImageNumber { get; set; }
    }
}
