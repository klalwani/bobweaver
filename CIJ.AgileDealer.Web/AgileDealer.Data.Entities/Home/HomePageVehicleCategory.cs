﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Home
{
    
    public class HomePageVehicleCategory : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int HomePageVehicleCatoryId { get; set; }

        public int HomePageId { get; set; }

        [MaxLength(128)]
        public string HomePageVehicleCategoryName { get; set; }
    }
}
