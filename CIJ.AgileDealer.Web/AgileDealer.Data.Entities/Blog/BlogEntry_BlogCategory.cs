﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Blog
{
    public class BlogEntry_BlogCategory : EntityBase
    {
        [Key]
        [Column(Order = 1)]
        public int BlogEntryId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int BlogCategoryId { get; set; }
    }
}
