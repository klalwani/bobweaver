﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    [Serializable]
    public class BlogDefaultFilter
    {
        public List<int> DefaultCategory { get; set; }
        public List<int> DefaultTag { get; set; }
    }
}
