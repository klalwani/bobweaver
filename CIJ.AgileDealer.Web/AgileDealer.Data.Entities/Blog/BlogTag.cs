﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class BlogTag : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BlogTagId { get; set; }

        [MaxLength(256)]
        public string TagText { get; set; }

        [DefaultValue(true)]
        public bool IsVisible { get; set; } = true;

        public bool IsBasicTag { get; set; }

        public int? Order { get; set; }
    }
}
