﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class Post : EntityBase
    {
        public int PostID { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        // compute based on title... http://agiledealer/blog/category/
        // i.e title = "Honda 2015 vs Toyota 2015"
        // relativeUrl = "Honda-2015-vs-Toyota-2015" computed based on title
        // category = benchmark
        // url = http://agiledealer/blog/benchmark/Honda-2015-vs-Toyota-2015
        [MaxLength(200)]
        public string Relative_Url { get; set; }
        public virtual PostContent Content { get; set; }
        public PostStatus Status { get; set; }

        [MaxLength(255)]
        public string Tags { get; set; }

        public Category Category { get; set; }

        [MaxLength(64)]
        public string Ip_Address { get; set; }

        [ForeignKey("CreatedID")]
        public AppUser Author { get; set; }

        public bool Active { get; set; }
    }
}
