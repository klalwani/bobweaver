﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class Comment : EntityBase
    {
        public int CommentID { get; set; }
        public CommentStatus Status { get; set; }

        [MaxLength(1024)]
        public string ContentText { get; set; }
        public Post Post { get; set; }

        public int? ReplyToCommentID { get; set; }

        [ForeignKey("ReplyToCommentID")]

        public Comment ReplyTo { get; set; }

        public int Rate { get; set; }

        [MaxLength(64)]
        public string IpAddress { get; set; }

        [ForeignKey("CreatedID")]
        public AppUser Author { get; set; }

        public bool Active { get; set; }
    }
}
