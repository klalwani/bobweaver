﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class BlogEntryResult
    {
        public int BlogEntryId { get; set; }
        public string PostTitle { get; set; }
        public string PostUrl { get; set; }

        public DateTime DateEnter { get; set; }

        public string PostFeatureImage { get; set; }

        public string PostSummary { get; set; }
        public string OverrideImage { get; set; }
        public string PostText { get; set; }
        public int? BlogCategoryId { get; set; }
        public int? BlogTagId { get; set; }
        public string BlogCategory { get; set; }
        public string BlogTag { get; set; }

        [MaxLength(256)]
        public string Button1Text { get; set; }

        [MaxLength(1024)]
        public string Button1Path { get; set; }

        [MaxLength(256)]
        public string Button2Text { get; set; }

        [MaxLength(1024)]
        public string Button2Path { get; set; }
        public bool IsPublish { get; set; }
        public bool IsDeleted { get; set; }
        public List<int> BlogCategoryIds { get; set; }
        public List<int> BlogTagIds { get; set; }

        public string ImageName { get; set; }

        public bool IsUpdateNewImage { get; set; }
        public int? LanguageId { get; set; }
    }
}
