﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class PostContent
    {
        [Key]
        public int PostID { get; set; }
        [MaxLength(4000)]
        public string ContentText { get; set; }
      
    }
}
