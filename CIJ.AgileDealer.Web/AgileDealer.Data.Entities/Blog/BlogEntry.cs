﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Blog
{
    public class BlogEntry : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BlogEntryId { get; set; }

        public int DealerID { get; set; }

        [MaxLength(256)]
        public string PostTitle { get; set; }

        [MaxLength(512)]
        public string PostUrl { get; set; }

        public DateTime DateEnter { get; set; }

        [MaxLength(1024)]
        public string PostFeatureImage { get; set; }

        [MaxLength(4000)]
        public string PostSummary { get; set; }

        public string OverrideImage { get; set; }

        public string PostText { get; set; }

        [MaxLength(256)]
        public string Button1Text { get; set; }

        [MaxLength(1024)]
        public string Button1Path { get; set; }

        [MaxLength(256)]
        public string Button2Text { get; set; }

        [MaxLength(1024)]
        public string Button2Path { get; set; }
        public bool IsPublish { get; set; }
        public bool IsDeleted { get; set; }
        public string ImageName { get; set; }

        [NotMapped]
        public bool IsUpdateNewImage { get; set; }
        public int? LanguageId { get; set; }
    }
}
