﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class CustomRedirectUrl: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomRedirectUrlId { get; set; }

        [MaxLength(512)]
        public string Name { get; set; }

        [MaxLength(512)]
        public string OldUrl { get; set; }

        [MaxLength(512)]
        public string NewUrl { get; set; }

        public bool IsPermanentRedirect { get; set; }
    }
}
