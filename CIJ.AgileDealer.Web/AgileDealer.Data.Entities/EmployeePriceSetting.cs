﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class EmployeePriceSetting : EntityBase
    {
        [Key,Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string LabelText { get; set; }
       
        public bool IsEnabled { get; set; }

    }

    public class EmployeePriceMakeBasedSetting : EntityBase
    {
        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int PercentageAmount { get; set; }

        public int MakeId { get; set; }

        public bool IsEnabled { get; set; }
    }
}
