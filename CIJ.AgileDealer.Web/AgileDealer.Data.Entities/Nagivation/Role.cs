﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Nagivation
{
    public class Role : EntityBase
    {
        [Key]
        public int RoleId { get; set; }
        public int DealerID { get; set; }

        public virtual ICollection<RoleExMenu> RoleExMenus { get; set; }
    }
}
