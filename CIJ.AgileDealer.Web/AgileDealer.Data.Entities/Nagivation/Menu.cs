﻿using AgileDealer.Data.Entities.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.SiteMap;

namespace AgileDealer.Data.Entities.Nagivation
{
    public class Menu : EntityBase
    {
        [Key]
        public int MenuId { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(65)]
        public string Title { get; set; }
        [MaxLength(128)]
        public string Description { get; set;}
        [MaxLength(256)]
        public string Url { get; set; }

        public int? ParentId { get; set; }
        public int? DealerID { get; set; }

        public int DisplayOrderId { get; set; }

        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("ParentId")]
        public virtual Menu ParentMenu { get; set; }

        public bool ShowOnMenu { get; set; }

        public bool ShowOnSitemap { get; set; }

        public bool ShowOnSitemapXML { get; set; }

		public int? SiteMapLocationId { get; set; }

		[ForeignKey("SiteMapLocationId")]
		public SiteMapLocation SiteMapLocation { get; set; }
    }
}
