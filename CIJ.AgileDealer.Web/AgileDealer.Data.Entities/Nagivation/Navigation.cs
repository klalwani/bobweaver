﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Nagivation
{
    public class Navigation : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NavigationId { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(64)]
        public string Title { get; set; }
        [MaxLength(128)]
        public string Description { get; set; }
        [MaxLength(256)]
        public string ActionLink { get; set; }
        [MaxLength(64)]
        public string Type { get; set; }

        [MaxLength(64)]
        public string Orientation { get; set; }
        [MaxLength(64)]
        public string CssClass { get; set; }

        public int DisplayOrderId { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<MenuExNavigation> MenuExNavigations { get; set; } 
    }
}
