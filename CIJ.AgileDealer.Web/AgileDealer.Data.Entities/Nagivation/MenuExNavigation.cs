﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities.Nagivation
{
    [Table("Menu_Ex_Navigation")]
    public class MenuExNavigation : EntityBase
    {
        [Key, Column(Order = 0)]
        public int MenuId { get; set; }
        [Key, Column(Order = 1)]
        public int NavigationId { get; set; }
        [Key, Column(Order = 2)]
        public int DealerID { get; set; }

        public virtual Menu Menu { get; set; }

        public virtual Navigation Navigation { get; set; }

        public virtual Dealer Dealer { get; set; }
    }
}
