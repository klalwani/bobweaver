﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities.Nagivation
{
    [Table("Role_Ex_Menu")]
    public class RoleExMenu : EntityBase
    {
        [Key, Column(Order = 0)]
        public int RoleId { get; set; }
        [Key, Column(Order = 1)]
        public int MenuId { get; set; }
        [Key, Column(Order = 2)]
        public int DealerID { get; set; }

        public short ReadLevel { get; set; }

        public virtual Role Role { get; set; }
        public virtual Menu Menu { get; set; }
        public virtual Dealer Dealer { get; set; }
    }
}
