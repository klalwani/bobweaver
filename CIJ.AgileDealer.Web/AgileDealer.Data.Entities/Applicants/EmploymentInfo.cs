﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Helpers;

namespace AgileDealer.Data.Entities.Applicants
{
    public class EmploymentInfo : EntityBase
    {
        [SkipProperty]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmploymentInfoId { get; set; }
        public string CurrentEmployer { get; set; }
        public string CurrentEmployerTelephone { get; set; }
        public string JobTitle { get; set; }
        public double MonthlyIncome { get; set; }

       
        [NotMapped]
        public string YearAttended {
            get
            {
                if (WorkingPeriod != null)
                {
                    List<string> temp = WorkingPeriod.Split(';').ToList();
                    if (temp.Any() && temp.LastOrDefault() != null)
                        return temp.Last().ToString();
                }
                return string.Empty;
            }
        }

        [NotMapped]
        public string MonthAttended
        {
            get
            {
                if (WorkingPeriod != null)
                {
                    List<string> temp = WorkingPeriod.Split(';').ToList();
                    if (temp.Any() && temp.FirstOrDefault() != null)
                        return temp.First().ToString();
                }
                return string.Empty;
            }
        }


        [SkipProperty]
        public string WorkingPeriod { get; set; }

        public string PreviousEmployer { get; set; }
        public string AdditionalIncomeSource { get; set; }
        private double _AdditionalIncomeAmount;
        public double AdditionalIncomeAmount
        {
            get
            {
                try
                {
                    return Convert.ToDouble(_AdditionalIncomeAmount);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                try
                {
                    _AdditionalIncomeAmount = Convert.ToDouble(value);
                }
                catch
                {
                    _AdditionalIncomeAmount = 0;
                }
            }
        }
        public string IsCorrespondingEmploymentInfo { get; set; }
        public int NumberOfApplicant { get; set; }
        public bool IsActive { get; set; }

        [SkipProperty]
        public int ApplicantId { get; set; }

        [SkipProperty]
        [ForeignKey("ApplicantId")]
        public Applicant Applicant { get; set; }

        [SkipProperty]
        public int? BuyerSessionId { get; set; }

        [SkipProperty]
        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }

        public bool IsCompleted { get; set; }
    }
}
