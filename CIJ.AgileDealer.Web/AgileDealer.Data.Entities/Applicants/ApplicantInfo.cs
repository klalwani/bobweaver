﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Helpers;
using AgileDealer.Data.Entities.Vehicles;

namespace AgileDealer.Data.Entities.Applicants
{
    public class ApplicantInfo: EntityBase
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [SkipProperty]
        public int ApplicantInfoId { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string SocialSercurityNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string MonthOfBirth { get; set; }
        public string YearOfBirth { get; set; }
        public string IsCorrespondingApplicantInfo { get; set; }
        public int NumberOfApplicant { get; set; }
        public bool IsActive { get; set; }

        [SkipProperty]
        public int ApplicantId { get; set; }

        [SkipProperty]
        [ForeignKey("ApplicantId")]
        public Applicant Applicant { get; set; }

        [SkipProperty]
        public int? BuyerSessionId { get; set; }


        [SkipProperty]
        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }

        public bool IsCompleted { get; set; }

    }
}
