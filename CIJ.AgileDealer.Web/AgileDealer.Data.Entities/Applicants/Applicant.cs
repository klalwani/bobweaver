﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Helpers;

namespace AgileDealer.Data.Entities.Applicants
{
    public class Applicant:EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApplicantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string ZipCode { get; set; }
        public string Sex { get; set; }

        [SkipProperty]
        public int ApplicantTypeId { get; set; }

        [SkipProperty]
        [ForeignKey("ApplicantTypeId")]
        public ApplicantType ApplicantType { get; set; }

        [SkipProperty]
        public int? BuyerSessionId { get; set; }

        [SkipProperty]
        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }

        public bool IsCompleted { get; set; }
        public bool IsArchived { get; set; }

        [NotMapped]
        public string ApplicantTypeName {
            get
            {
                if (ApplicantTypeId > 0 && ApplicantType != null)
                {
                    return ApplicantType.Name;
                }
                return string.Empty;
            }
        }

    }
}
