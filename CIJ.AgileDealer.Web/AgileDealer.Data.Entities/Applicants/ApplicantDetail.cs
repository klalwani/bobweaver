﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Applicants
{
    public class ApplicantDetail
    {
        public ApplicantDetail()
        {
            ContactInfo = new ContactInfo();
            ApplicantInfo = new ApplicantInfo();
            EmploymentInfo = new EmploymentInfo();
        }

        public Applicant Applicant { get; set; }
        public ContactInfo ContactInfo { get; set; }
        public ApplicantInfo ApplicantInfo { get; set; }
        public EmploymentInfo EmploymentInfo { get; set; }
        public ContactInfo JoinedContactInfo { get; set; }
        public ApplicantInfo JoinedApplicantInfo { get; set; }
        public EmploymentInfo JoinedEmploymentInfo { get; set; }
    }
}
