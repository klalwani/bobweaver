﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Helpers;

namespace AgileDealer.Data.Entities.Applicants
{
    public class ContactInfo : EntityBase
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [SkipProperty]
        public int ContactInfoId { get; set; }

        public string CurrentAddress { get; set; }

        [MaxLength(128)]
        public string City { get; set; }

        [MaxLength(16)]
        public string State { get; set; }

        [MaxLength(32)]
        public string ZipCode { get; set; }

        [SkipProperty]
        public int ResidenceStatus { get; set; }

        [NotMapped]
        public string ResidenceStatusName { get; set; }

        public double MonthlyMortage { get; set; }
        public int MonthAttended { get; set; }
        public int YearAttended { get; set; }
        public string PreviousAddress { get; set; }
        public string PreviousCity { get; set; }
        public string PreviousState { get; set; }

        [MaxLength(32)]
        public string PreviousZipCode { get; set; }
        public string IsCorrespondingContactInfo { get; set; }
        public int NumberOfApplicant { get; set; }
        public bool IsActive { get; set; }

        [SkipProperty]
        public int ApplicantId { get; set; }

        [SkipProperty]
        [ForeignKey("ApplicantId")]
        public Applicant Applicant { get; set; }

        [SkipProperty]
        public int? BuyerSessionId { get; set; }

        [SkipProperty]
        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }

        [SkipProperty]
        public bool IsCompleted { get; set; }

    }
}
