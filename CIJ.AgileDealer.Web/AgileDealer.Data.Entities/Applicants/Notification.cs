﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Applicants
{
    public class Notification : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NotificationId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Class { get; set; }
        public string Glyphicon { get; set; }
        public int PageIndex { get; set; }
    }
}
