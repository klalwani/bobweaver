﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Vehicles;

namespace AgileDealer.Data.Entities
{
    public class MasterData
    {
        public int Id { get; set; }
        public int SubListId { get; set; }
        public string IdString { get; set; }
        public string Name { get; set; }
        public string SubListName { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public bool HasSubList { get; set; }
        public int Count { get; set; }

        public List<MasterData> SubList { get; set; }

    }
}
