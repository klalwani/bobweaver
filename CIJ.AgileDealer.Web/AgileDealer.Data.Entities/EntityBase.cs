﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Helpers;

namespace AgileDealer.Data.Entities
{
    [Serializable()]
    public class EntityBase
    {
        public EntityBase()
        {
            this.CreatedDt = DateTime.Now;
            this.ModifiedDt = DateTime.Now;
        }

        [SkipProperty]
        [MaxLength(128)]
        public string CreatedID { get; set; }

        public DateTime CreatedDt { get; set; }

        [SkipProperty]
        [MaxLength(128)]
        public string ModifiedID { get; set; }
        public DateTime ModifiedDt { get; set; }

        [SkipProperty]
        [DefaultValue(1)]
        public int DealerID { get; set; }
    }
}
