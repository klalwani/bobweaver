﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    public class LeaseTerm : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LeaseTermId { get; set; }

        [XmlIgnore]
        public int LeaseTermListId { get; set; }

        [XmlElement("APR")]
        public string Apr { get; set; }

        [XmlElement("Term")]
        public string Term { get; set; }
    }
}
