﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class VehicleSearch
    {
        [JsonProperty(PropertyName = "Counts")]
        public Counts Counts { get; set; }

        [JsonProperty(PropertyName = "Vehicles")]
        public Vehicles Vehicles { get; set; }

    }
}
