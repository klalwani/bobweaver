﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class IncentiveTranslated : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IncentiveTranslatedId { get; set; }
        public int DisplaySection { get; set; }
        public int Position { get; set; }
        public string TranslatedName { get; set; }
        public string ProgramName { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Detail { get; set; }
        public string Disclaimer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool IsActive { get; set; }
        public string Vin { get; set; }
        public int ModelId { get; set; }
        public bool IsCashCoupon { get; set; }
        public bool IsCustomDiscount { get; set; }
        public string Amount { get; set; }


        public string ExcludeEngineCodes { get; set; }
        public string ExcludeVINs { get; set; }

        public bool HasSetDateRange { get; set; }

        public string excludeTrimId { get; set; }
        public string ExcludeYear { get; set; }

    
        public bool HideOtherIncentives { get; set; }
        public Int16 DiscountType { get; set; }
        public bool HideDiscounts { get; set; }
        public string excludeBodyStyleId { get; set; }


        public bool IsMasterIncentive { get; set; }

        public string ChromeStyleId { get; set; }
        public int ChromeStyleIdYear { get; set; }

        public string IncludeMasterIncentiveVins { get; set; }
    }
}
