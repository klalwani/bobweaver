﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class VinEligibility : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VinEligibilityId { get; set; }
        public string Vin { get; set; }
        public string DealerCode { get; set; }
        public string SaleDate { get; set; }
        public string SaleType { get; set; }
        public string Language { get; set; }
        public string Staging { get; set; }
        public string Tier { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInt { get; set; }
        public string Strett { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public string ZipCode { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string AppContext { get; set; }
        public string PlanType { get; set; }
        public string SupplierCode { get; set; }
    }
}
