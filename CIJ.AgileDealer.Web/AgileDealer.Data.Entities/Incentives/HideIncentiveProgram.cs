﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class HideIncentiveProgram : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ProgramNumber { get; set; }
        public string VIN { get; set; }
        public int ModelId { get; set; }
        public string ModelName { get; set; }
    }
}
