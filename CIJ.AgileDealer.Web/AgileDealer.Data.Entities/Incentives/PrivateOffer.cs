﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class PrivateOffer : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrivateOfferId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInt { get; set; }
        public string Strett { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
    }
}
