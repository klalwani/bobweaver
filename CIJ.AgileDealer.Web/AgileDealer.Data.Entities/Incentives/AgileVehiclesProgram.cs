﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Vehicles;

namespace AgileDealer.Data.Entities.Incentives
{
    public class AgileVehiclesProgram : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AgileVehiclesProgramId { get; set; }
        
        public int AgileVehicleId { get; set; }
        public int ProgramId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAddedManually { get; set; }
    }
}
