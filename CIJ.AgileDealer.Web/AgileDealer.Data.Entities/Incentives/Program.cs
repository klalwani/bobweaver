﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class Program : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProgramId { get; set; }

        [XmlIgnore]
        public int ProgramTypeId { get; set; }

        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }

        [XmlElement("Detail")]
        public string Detail{ get; set; }

        [XmlElement("Disclaimer")]
        public string Disclaimer { get; set; }


        [XmlElement("StartDate")]
        public string StartDate { get; set; }

        [XmlElement("EndDate")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "ExpiryDate")]
        public string ExpiryDate { get; set; }

        [XmlElement("Amount")]
        public string Amount { get; set; }

        [XmlElement("IsFmcc")]
        public string IsFmcc { get; set; }

        [XmlElement("Conditional")]
        public string Conditional { get; set; }

        [XmlElement("Links")]
        public string Links { get; set; }

        [ForeignKey("ProgramTypeId")]
        public ProgramType ProgramType { get; set; }

        [XmlIgnore]
        public int? GroupId { get; set; }

        [XmlIgnore]
        [ForeignKey("GroupId")]
        public Group Group { get; set; }

        [XmlIgnore]
        public DateTime? Start { get; set; }

        [XmlIgnore]
        public DateTime? End { get; set; }

        [NotMapped]
        [XmlElement("APRTerms")]
        public AprTermList AprTermList { get; set; }

        [NotMapped]
        [XmlElement("LeaseTerms")]
        public LeaseTermList LeaseTermList { get; set; }

        public bool IsTargeted { get; set; }
    }
}
