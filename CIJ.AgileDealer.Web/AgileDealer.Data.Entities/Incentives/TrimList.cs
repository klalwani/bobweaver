﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class TrimList : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [XmlIgnore]
        public int TrimListId { get; set; }

        [XmlIgnore]
        public int? NamePlateId { get; set; }

        [XmlIgnore]
        [ForeignKey("NamePlateId")]
        public NamePlate NamePlate { get; set; }

        [NotMapped]
        [XmlElement("Trim")]
        public List<NamePlateTrim> Trim { get; set; }
    }
}
