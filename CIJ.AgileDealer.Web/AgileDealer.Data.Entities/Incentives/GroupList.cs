﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class  GroupList : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]   
        [XmlIgnore]
        public int GroupListId { get; set; }

        [XmlIgnore]
        public int? NamePlateId { get; set; }

        [XmlIgnore]
        [ForeignKey("NamePlateId")]
        public NamePlate NamePlate { get; set; }

        [XmlIgnore]
        public int? NamePlateTrimId { get; set; }

        [XmlIgnore]
        [ForeignKey("NamePlateTrimId")]
        public NamePlateTrim Trim { get; set; }

        [NotMapped]
        [XmlElement("Group")]
        public List<Group> Group { get; set; }
    }
}
