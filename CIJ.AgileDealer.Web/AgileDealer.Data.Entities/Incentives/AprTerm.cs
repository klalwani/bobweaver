﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class AprTerm : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AprTermId { get; set; }

        [XmlIgnore]
        public int AprTermListId { get; set; }

        [JsonProperty(PropertyName = "Apr")]
        public string Apr { get; set; }

        [JsonProperty(PropertyName = "Term")]
        public string Term { get; set; }
    }
}
