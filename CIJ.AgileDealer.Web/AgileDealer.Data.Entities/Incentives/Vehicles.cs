﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class Vehicles
    {
        [JsonProperty(PropertyName = "Vehicle")]
        public Vehicle Vehicle { get; set; }
    }
}
