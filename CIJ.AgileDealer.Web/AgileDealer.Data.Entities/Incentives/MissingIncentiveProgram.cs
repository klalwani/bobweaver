﻿namespace AgileDealer.Data.Entities.Incentives
{
    public class MissingIncentiveProgram
    {
        public int Id { get; set; }
        public string ProgramNumber { get; set; }
        public bool IsActive { get; set; }
    }
}