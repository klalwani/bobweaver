﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class Vehicle
    {
        public string NormalizeExtType { get; set; }
        public string DMIVideoMediaId { get; set; }

        [JsonConverter(typeof(Helpers.SingleValueArrayConverter<string>))]
        public string VehicleStage { get; set; }
        public string NormalizeDoorType { get; set; }
        public string NormalizeVehicleType { get; set; }
        public string NormalizeDrive { get; set; }
        public string NormalizeEngine { get; set; }
        public string NormalizeTrimColor { get; set; }
        public string ImageToken { get; set; }

        [DefaultValue("")]
        [JsonConverter(typeof(Helpers.SingleValueArrayConverter<string>))]
        public string VideoMediaURL { get; set; }
        public string NormalizeDisplacement { get; set; }
        public string StockNumber { get; set; }
        public string DealerPACode { get; set; }
        public string MatchType { get; set; }
        public string NormalizeFuel { get; set; }
        public string NormalizeTrimType { get; set; }
        public string DLRMarketingText { get; set; }
        public string NormalizeTransmission { get; set; }
        public string NormalizeModel { get; set; }
        public string NormalizeAspiration { get; set; }
        public string VehicleAttributes { get; set; }
        public string DOL { get; set; }
        public string NormalizeCylinder { get; set; }
        public string NormalizeExtPaint { get; set; }
        public string ConfigToken { get; set; }
        public string WindowStickerURL { get; set; }
        public string MatchScore { get; set; }
        public string VINIncentiveEligibility { get; set; }
        public string Vin { get; set; }
        public PaymentEstimator PaymentEstimator { get; set; }
    }
}
