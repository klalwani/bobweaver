﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class NamePlateTrim : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NamePlateTrimId { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [NotMapped]
        [XmlElement("Groups")]
        public GroupList Groups { get; set; }

        [XmlIgnore]
        public int? TrimListId { get; set; }

        [XmlIgnore]
        [ForeignKey("TrimListId")]
        public TrimList TrimList { get; set; }

    }
}
