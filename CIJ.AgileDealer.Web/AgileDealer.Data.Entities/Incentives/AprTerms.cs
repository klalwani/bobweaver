﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AgileDealer.Data.Entities.Incentives
{
    public class AprTerms
    {
        [JsonConverter(typeof(Helpers.SingleValueArrayConverter<AprTerm>))]
        public List<AprTerm> AprTerm { get; set; }
    }
}
