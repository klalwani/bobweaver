﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    public class Response
    {
        [JsonProperty(PropertyName = "ttl")]
        public string ttl { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string status { get; set; }

        [JsonProperty(PropertyName = "VehicleSearch")]
        public VehicleSearch VehicleSearch { get; set; }
    }
}
