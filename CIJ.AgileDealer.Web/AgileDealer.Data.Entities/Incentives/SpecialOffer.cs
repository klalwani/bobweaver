﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class SpecialOffer : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SpecialOfferId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string PostalCode { get; set; }
        public string AppContext { get; set; }
        public string Staging { get; set; }
        public string GeoKey { get; set; }
        public string PlanType { get; set; }
        public string Language { get; set; }
        public string SupplierCode { get; set; }
        public string Province { get; set; }
    }
}
