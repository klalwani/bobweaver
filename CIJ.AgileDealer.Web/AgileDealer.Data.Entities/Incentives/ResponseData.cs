﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class ResponseData
    {
        [JsonProperty(PropertyName = "Response")]
        public Response Response { get; set; }
    }
}
