﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class LeasePayment
    {
        public string ProgramName { get; set; }
        public string LeaseTerms { get; set; }
        public string StartDate { get; set; }
        public string MaxDownPayment { get; set; }
        public string LeaseTermsDisclaimer { get; set; }
        public string EndDate { get; set; }
        public string DefaultAnnualMileage { get; set; }
        public string FutureFee { get; set; }
        public string DefaultTerm { get; set; }
        public string ProgramId { get; set; }
        public string AcquisitionFee { get; set; }
        public string MaxCombinedValue { get; set; }
        public string MaxTradeIn { get; set; }
        public Residuals Residuals { get; set; }
    }
}
