﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class APRIncentives
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int APRIncentiveId { get; set; }
        public string MinExpiryDate { get; set; }

        [JsonConverter(typeof(Helpers.SingleValueArrayConverter<Program>))]
        public List<Program> Incentive { get; set; }
    }
}
