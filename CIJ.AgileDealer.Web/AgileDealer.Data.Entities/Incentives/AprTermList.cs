﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    public class AprTermList : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AprTermListId { get; set; }

        [XmlIgnore]
        public int ProgramId { get; set; }

        [XmlElement("APRTerm")]
        public List<AprTerm> AprTerms { get; set; }
    }
}
