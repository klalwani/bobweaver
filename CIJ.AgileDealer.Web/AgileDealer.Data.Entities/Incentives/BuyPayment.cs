﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgileDealer.Data.Entities.Incentives
{
    public class BuyPayment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BuyPaymentId { get; set; }
        public string InitiativeStartDate { get; set; }
        public string InitiativeEndDate { get; set; }
        public string MaxCustomRate { get; set; }
        public string DefaultAPRTerm { get; set; }
        public string APRTermsDisclaimer { get; set; }

        [NotMapped]
        public CashIncentives CashIncentives { get; set; }

        [NotMapped]
        public APRIncentives APRIncentives { get; set; }

        [NotMapped]
        public AprTerms AprTerms { get; set; }
    }
}
