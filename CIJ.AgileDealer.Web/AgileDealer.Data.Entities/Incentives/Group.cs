﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    [Serializable]
    public class Group : EntityBase
    {
        public Group()
        {
            //Programs = new List<Program>();
        }

        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }

        [XmlIgnore]
        public int? GroupTypeId { get; set; }

        [XmlIgnore]
        public int? GroupSubTypeId { get; set; }

        [XmlIgnore]
        public int GroupListId { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("subType")]
        public string SubType { get; set; }

        [XmlElement("Program")]
        public List<Program> Programs { get; set; }


        [XmlIgnore]
        [ForeignKey("GroupSubTypeId")]
        public GroupSubType GroupSubType { get; set; }

        [XmlIgnore]
        [ForeignKey("GroupTypeId")]
        public GroupType GroupType { get; set; }

        [XmlIgnore]
        [ForeignKey("GroupListId")]
        public GroupList GroupList { get; set; }
    }
}
