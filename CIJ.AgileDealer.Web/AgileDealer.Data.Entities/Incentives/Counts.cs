﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class Counts
    {
        public string ExactMatchScore { get; set; }
        public int Matching { get; set; }
        public int Total { get; set; }
    }
}
