﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    [XmlRoot("Nameplate")]
    [Serializable]
    public class NamePlate : EntityBase
    {
        public NamePlate()
        {
            GroupList = null;
            TrimList = null;
        }

        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NamePlateId { get; set; }

        [XmlAttribute("make")]
        public string Make { get; set; }

        [XmlAttribute("model")]
        public string Model { get; set; }

        [XmlAttribute("year")]
        public string Year { get; set; }

        [NotMapped]
        [XmlElement("Groups")]
        public GroupList GroupList { get; set; }

        [NotMapped]
        [XmlElement("Trims")]
        public TrimList TrimList { get; set; }
    }
}
