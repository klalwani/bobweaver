﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Incentives
{
    public class LeaseTermList : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LeaseTermListId { get; set; }

        [XmlIgnore]
        public int ProgramId { get; set; }

        [XmlElement("LeaseTerm")]
        public List<LeaseTerm> LeaseTerms { get; set; }
    }
}
