﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class PaymentEstimator
    {
        public string FordCreditParams { get; set; }
        public string planType { get; set; }
        public LeasePayment LeasePayment { get; set; }
        public BuyPayment BuyPayment { get; set; }

    }
}
