﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Incentives
{
    public class Residuals
    {
        public List<LeaseTerm> Residual { get; set; }
    }
}
