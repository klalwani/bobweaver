﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class VechicleViewHistory : EntityBase
    {
        public int Id { get; set; }
        public DateTime Created_Dt { get; set; }
        public string User_Id { get; set; }
        //[ForeignKey("User_Id")]
        //public AppUser User { get; set; }
        public int Vehicle_Id { get; set; }

        [ForeignKey("Vehicle_Id")]
        public HomeNetVehicle Vehicle { get; set; }
    }
}
