﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.SiteMap
{
    public class Sitemap : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SitemapId { get; set; }
        private ArrayList map;

        public Sitemap()
        {
            map = new ArrayList();
        }

        public string Title { get; set; }
        public string Description { get; set; }

        [XmlElement("url")]
        public SiteMapLocation[] Locations
        {
            get
            {
                SiteMapLocation[] items = new SiteMapLocation[map.Count];
                map.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null)
                    return;
                SiteMapLocation[] items = (SiteMapLocation[])value;
                map.Clear();
                foreach (SiteMapLocation item in items)
                    map.Add(item);
            }
        }

        public int Add(SiteMapLocation item)
        {
            return map.Add(item);
        }
    }
}
