﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.SiteMap
{
    [XmlRoot("url")]
    public class SiteMapLocation : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SiteMapLocationId { get; set; }
        public enum eChangeFrequency
        {
            always,
            hourly,
            daily,
            weekly,
            monthly,
            yearly,
            never
        }

        [XmlElement("loc")]
        public string Url { get; set; }

        [XmlElement("changefreq")]
        public eChangeFrequency? ChangeFrequency { get; set; }
        public bool ShouldSerializeChangeFrequency() { return ChangeFrequency.HasValue; }

        [XmlElement("lastmod")]
        public string LastModified { get; set; }
        public bool ShouldSerializeLastModified() { return !string.IsNullOrEmpty(LastModified); }

        [XmlElement("priority")]
        public double? Priority { get; set; }
        public bool ShouldSerializePriority() { return Priority.HasValue; }

        [XmlIgnore]
        public int SitemapId { get; set; }

        [XmlIgnore]
        [ForeignKey("SitemapId")]
        public Sitemap Sitemap { get; set; }

    }
}
