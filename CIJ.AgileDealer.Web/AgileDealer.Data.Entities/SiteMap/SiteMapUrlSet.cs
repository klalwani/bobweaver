﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.SiteMap
{
    [XmlRoot("urlset")]
    public class SiteMapUrlSet
    {
        [XmlElement("url")]
        public List<SiteMapLocation> SiteMapLocations { get; set; }
    }
}
