﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AgileDealer.Data.Entities.Helpers
{
    public class SingleValueArrayConverter<T> : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            object retVal = new Object();
            if (reader.TokenType != JsonToken.Null)
            {
                if (reader.TokenType == JsonToken.StartArray)
                {
                    retVal = serializer.Deserialize(reader, objectType);
                }
                else
                {
                    JValue jValue = new JValue(reader.Value);
                    switch (reader.TokenType)
                    {
                        case JsonToken.StartObject:
                            {
                                T instance = default(T);
                                try
                                {
                                    instance = (T)serializer.Deserialize(reader, typeof(T));
                                    retVal = new List<T>() { instance };
                                }
                                catch (Exception)
                                {
                                    retVal = string.Empty;
                                }
                            }
                            break;
                        case JsonToken.String:
                            retVal = (string)jValue;
                            break;
                        case JsonToken.Date:
                            retVal = (DateTime)jValue;
                            break;
                        case JsonToken.Boolean:
                            retVal = (bool)jValue;
                            break;
                        case JsonToken.Integer:
                            retVal = (int)jValue;
                            break;
                        default:
                            Console.WriteLine("Default case");
                            Console.WriteLine(reader.TokenType.ToString());
                            break;
                    }
                }
            }
            return retVal;
        }

        public override bool CanConvert(Type objectType)
        {
            return false;
        }
    }
}
