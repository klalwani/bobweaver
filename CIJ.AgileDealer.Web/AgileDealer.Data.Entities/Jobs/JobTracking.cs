﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Jobs
{
    public class JobTracking: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int JobTrackingId { get; set; }
        public string JobName { get; set; }
        public string TriggerBy { get; set; }
        public string Status { get; set; }
        public DateTime TriggeredTime { get; set; }
        public DateTime? StarTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
