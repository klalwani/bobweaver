﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities
{
    public class DealerServiceSpecial:EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DealerServiceSpecialId { get; set; }
        public int DealerID { get; set; }

        public string ServiceSpecialsImage { get; set; }

        [MaxLength(512)]
        public string Title { get; set; }

        public string Description { get; set; }

        public int Order { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
    }
}
