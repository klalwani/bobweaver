﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.ScheduleService
{
    public class ScheduleCurrentIssue :EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScheduleCurrentIssueId { get; set; }
        public int CurrentIssueId { get; set; }
        public int ScheduleServiceId { get; set; }

        [ForeignKey("ScheduleServiceId")]
        public ScheduleService ScheduleService { get; set; }

        [ForeignKey("CurrentIssueId")]
        public CurrentIssue CurrentIssue { get; set; }
    }
}
