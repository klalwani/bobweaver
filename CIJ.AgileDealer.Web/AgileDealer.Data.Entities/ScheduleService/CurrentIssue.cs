﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.ScheduleService
{
    public class CurrentIssue : EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CurrentIssueId { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(128)]
        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
