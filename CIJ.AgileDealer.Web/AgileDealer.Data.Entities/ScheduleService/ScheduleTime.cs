﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.ScheduleService
{
    public class ScheduleTime : EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ScheduleTimeId { get; set; }

        public string Hour { get; set; }

        public bool IsSaturday { get; set; }

        public bool IsActive { get; set; }
    }
}
