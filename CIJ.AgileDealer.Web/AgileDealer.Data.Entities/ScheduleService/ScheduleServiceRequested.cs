﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.ScheduleService
{
    public class ScheduleServiceRequested : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScheduleServiceRequestedId { get; set; }
        public int ServiceRequestedId { get; set; }
        public int ScheduleServiceId { get; set; }

        [ForeignKey("ScheduleServiceId")]
        public ScheduleService ScheduleService { get; set; }

        [ForeignKey("ServiceRequestedId")]
        public ServiceRequested ServiceRequested { get; set; }
    }
}
