﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Buyer;

namespace AgileDealer.Data.Entities.ScheduleService
{
    public class ScheduleService : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScheduleServiceId { get; set; }
        public DateTime ScheduleDate { get; set; }

        [NotMapped]
        public string ScheduleDateString { get; set; }
        public string WeeklyHour { get; set; }
        public string SaturdayHour { get; set; }

        [MaxLength(128)]
        public string FirstName { get; set; }

        [MaxLength(128)]
        public string LastName { get; set; }

        [MaxLength(64)]
        public string PhoneNumber { get; set; }

        [MaxLength(128)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Year { get; set; }

        [MaxLength(128)]
        public string Make { get; set; }

        [MaxLength(128)]
        public string Model { get; set; }

        [MaxLength(128)]
        public string Mileage { get; set; }

        public int WarrantyStatus { get; set; }

        public string AdditionalInformation { get; set; }

        [NotMapped]
        public List<int> ServiceRequestList { get; set; }

        [NotMapped]
        public List<int> CurrentIssueList { get; set; }

        public int? BuyerSessionId { get; set; }

        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }
        public bool IsCompleted { get; set; }
    }
}
