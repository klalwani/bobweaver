﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.AboutUs
{
    public class AboutUs : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AboutUsId { get; set; }

        public int DealerID { get; set; }

        public string Content { get; set; }

        public string Title { get; set; }
    }
}
