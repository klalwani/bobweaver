﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities
{
    [Table("Dealers_HomeNetDealerNames")]
    public class DealersHomeNetDealerName : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Dealers_HomeNetDealerNameId")]
        public int DealersHomeNetDealerNameId { get; set; }
        public int DealerID { get; set; }
        public string HomeNetDealerName { get; set; }
        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
    }
}
