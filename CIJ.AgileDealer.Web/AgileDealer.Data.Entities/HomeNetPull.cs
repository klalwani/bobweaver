﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    [Table("HomeNetPull")]
    public class HomeNetPull : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HomeNetPullId { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DateEnded { get; set; }

        public bool EncounteredErrors { get; set; }

        public virtual ICollection<HomeNetVehicle> HomeNetVehicles { get; set; } 
    }
}
