﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class AppUser : IdentityUser
    {
        public string HomeTown { get; set; }

        public byte[] Picuture { get; set; }
    }
}
