﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class AgileVehicle: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AgileVehicleId { get; set; }

        public string Vin { get; set; }

        public int Year { get; set; }

        public int MakeId { get; set; }

        public int ModelId { get; set; }

        public int TrimId { get; set; }
        public int DealerID { get; set; }

        public int BodyStyleId { get; set; }

        public int ExteriorColorId { get; set; }

        public int InteriorColorId { get; set; }

        public int MPG_City { get; set; }

        public int MPG_Highway { get; set; }

        [MaxLength(50)]
        public string FuelType { get; set; }

        public int? FuelTypeId { get; set; }

        public int? EngineId { get; set; }

        public int? DriveId { get; set; }

        public decimal? MSRP { get; set; }

        public decimal? Invoice { get; set; }

        public decimal? SellingPrice { get; set; }

        public decimal? InternetPrice { get; set; }

        public decimal? BookValue { get; set; }

        public DateTime? DateInStock { get; set; }

        public bool IsNew { get; set; }

        public bool IsCertified { get; set; }

        public bool IsSold { get; set; }

        public string MainImageUrl { get; set; }

        public decimal Rebate { get; set; }
        public bool IsSpecial { get; set; }

        public bool IsRentalSpecial { get; set; }
        public bool IsLift { get; set; }
        public bool IsUpfitted { get; set; }
        public decimal OverridePrice { get; set; }
        [MaxLength(150)]
        public string OverrideDescription { get; set; }

        [MaxLength(250)]
        public string EngineDescription { get; set; }

        [MaxLength(250)]
        public string DriveTrain { get; set; }

        public int Miles { get; set; }

        [MaxLength(250)]
        public string TransmissionDescription { get; set; }

        [MaxLength(250)]
        public string StockNumber { get; set; }

        [MaxLength(250)]
        public string InteriorUpholstry { get; set; }

        public DateTime? ImagesModifiedDate { get; set; }
        public DateTime? DataModifiedDate { get; set; }

        public string AutowriterDescription { get; set; }
        public string UpfittedDescription { get; set; }

        public DateTime? OverrideStartDate { get; set; }
        public DateTime? OverrideEndDate { get; set; }

        public bool HasSetDateRange { get; set; }
        public bool IsHidden { get; set; }

        [NotMapped]
        public virtual bool IsOverridePrice { get; set; }

        [NotMapped]
        public virtual decimal? CurrentPrice
        {
            get
            {
                decimal? currentValue = 0;
                if (IsOverridePrice)
                {
                    if (!HasSetDateRange)
                    {
                        currentValue = MSRP - (MSRP - OverridePrice + Math.Abs(Rebate));
                    }
                    else
                    {
                        if (OverrideStartDate <= DateTime.Now && DateTime.Now <= OverrideEndDate)
                        {
                            currentValue = MSRP - (MSRP - OverridePrice + Math.Abs(Rebate));
                        }
                        else
                        {
                            currentValue = MSRP - (MSRP - SellingPrice + Math.Abs(Rebate));
                        }
                    }
                }
                else
                {
                    currentValue = MSRP - (MSRP - SellingPrice + Math.Abs(Rebate));
                }

                return currentValue;
            }
        }

        [NotMapped]
        public virtual decimal? CurrentDealerSaving {
            get
            {
                decimal? currentValue = 0;
                if (IsOverridePrice)
                {
                    if (!HasSetDateRange)
                    {
                        currentValue = MSRP > 0 ? (MSRP - OverridePrice) : 0;
                    }
                    else
                    {
                        if (OverrideStartDate <= DateTime.Now && DateTime.Now <= OverrideEndDate)
                        {
                            currentValue = MSRP > 0 ? (MSRP - OverridePrice) : 0;
                        }
                        else
                        {
                            currentValue = MSRP > 0 ? (MSRP - SellingPrice) : 0;
                        }
                    }
                }
                else
                {
                    currentValue = MSRP > 0 ? (MSRP - SellingPrice) : 0;
                }
                
                return currentValue;
            }
        }

        [NotMapped]
        public virtual bool CurrentIsDisplayMsrp
        {
            get
            {
                bool currentValue = false;
                if (IsOverridePrice)
                {
                    if (!HasSetDateRange)
                    {
                        currentValue = (MSRP != 0 && ((MSRP - OverridePrice) + OverridePrice + Rebate) < MSRP) ? true : false;
                    }
                    else
                    {
                        if (OverrideStartDate <= DateTime.Now && DateTime.Now <= OverrideEndDate)
                        {
                            currentValue = (MSRP != 0 && ((MSRP - OverridePrice) + OverridePrice + Rebate) < MSRP) ? true : false;
                        }
                        else
                        {
                            currentValue = (MSRP != 0 && ((MSRP - SellingPrice) + SellingPrice + Rebate) < MSRP) ? true : false;
                        }
                    }
                }
                else
                {
                    currentValue = (MSRP != 0 && ((MSRP - SellingPrice) + SellingPrice + Rebate) < MSRP) ? true : false;
                }

                return currentValue;
            }
        }

        [ForeignKey("MakeId")]
        public Make Make { get; set; }

        [ForeignKey("ModelId")]
        public Model Model { get; set; }

        [ForeignKey("TrimId")]
        public Trim Trim { get; set; }

        [ForeignKey("BodyStyleId")]
        public BodyStyle BodyStyle { get; set; }

        [ForeignKey("ExteriorColorId")]
        public ExteriorColor ExteriorColor { get; set; }

        [ForeignKey("InteriorColorId")]
        public InteriorColor InteriorColor { get; set; }

        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }

        [ForeignKey("FuelTypeId")]
        public FuelType FuelTypes { get; set; }

        [ForeignKey("EngineId")]
        public Engine Engine { get; set; }

        [ForeignKey("DriveId")]
        public Drive Drive { get; set; }

        public decimal? MSRPOverride { get; set; }

        public decimal? UpfittedCost { get; set; }

        public decimal? EmployeePrice { get; set; }

        public bool HideIncentives { get; set; }
    }
}
