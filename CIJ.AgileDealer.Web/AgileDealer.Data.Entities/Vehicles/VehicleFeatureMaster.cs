﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleFeatureMaster: EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FeatureId { get; set; }

        public string FeatureName { get; set; }

        public string LabelOverride { get; set; }

        public bool IsSearchable { get; set; }
    }

    public class VehicleFeatures : EntityBase
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? FeatureId { get; set; }

        public int AgileVehicleId { get; set; }

        public string FeatureName { get; set; }
        public int MasterFeatureId { get; set; }
        public bool IsSearchable { get; set; }

    }
}
