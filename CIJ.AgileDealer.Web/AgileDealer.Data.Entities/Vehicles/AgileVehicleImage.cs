﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class AgileVehicleImage : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AgileVehicleImageID { get; set; }

        public int AgileVehicleID { get; set; }

        public int ImageOrder { get; set; }

        public string ImageName { get; set; }

        public string RelativeUrl { get; set; }

        public int XResolution { get; set; }

        public int YResolution { get; set; }
        public bool IsDeletedOnAzure { get; set; }
    }
}
