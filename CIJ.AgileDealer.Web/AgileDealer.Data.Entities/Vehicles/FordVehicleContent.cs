﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class FordVehicleContent : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FordVehicleContentId { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public string ModelTitle { get; set; }
        public string InSalePlace { get; set; }
        public string MsrpTitle { get; set; }
        public string SellingPrice { get; set; }
        public string MpgTitle { get; set; }
        public string LbsTitle { get; set; }
        public string SeatingForTitle { get; set; }
        public string PeopleAmount { get; set; }
        public string MenuTitle { get; set; }
        public string Alt { get; set; }
        public string ImageUrl { get; set; }
        [NotMapped]
        public virtual int Price
        {
            get
            {
                if (!string.IsNullOrEmpty(SellingPrice))
                    return int.Parse(SellingPrice);
                else return 0;
            }
        }
    }
}
