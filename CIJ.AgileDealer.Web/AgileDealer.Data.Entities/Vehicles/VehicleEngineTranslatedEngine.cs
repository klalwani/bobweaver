﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    [Serializable()]
    public class VehicleEngineTranslatedEngine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleEngineTranslatedEngineId { get; set; }
        public int engineTranslatedId { get; set; }
        public int engineId { get; set; }
    }
}
