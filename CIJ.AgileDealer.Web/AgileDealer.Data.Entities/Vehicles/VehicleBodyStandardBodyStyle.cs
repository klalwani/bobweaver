﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    [Serializable()]
    public class VehicleBodyStandardBodyStyle
    {
        [Key]
        public int VehicleBodyStandardBodyStyleId { get; set; }
        public int standardBodyId { get; set; }
        public int bodyStyleTypeId { get; set; }
    }
}
