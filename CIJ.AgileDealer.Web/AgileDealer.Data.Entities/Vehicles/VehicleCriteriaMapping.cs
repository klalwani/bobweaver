﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleCriteriaMapping : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleCriteriaMappingId { get; set; }
        public string BadName { get; set; }
        public string GoodName { get; set; }
        public string FilterType { get; set; }
    }
}
