﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleMasterDataCounter
    {
        public int IsNew { get; set; }
        public int IsUsed { get; set; }
        public int IsCertified { get; set; }
        public int IsLift { get; set; }
        public int IsUpfitted { get; set; }
        public int DealerId { get; set; }
        public List<VehicleKeyPairValueData> Years { get; set; }
        public List<VehicleKeyPairValueData> Makes { get; set; }
        public List<VehicleKeyPairValueData> Prices { get; set; }
        public List<VehicleKeyPairValueData> Models { get; set; }
        public List<VehicleKeyPairValueData> BodyStyles { get; set; }
        public List<VehicleKeyPairValueData> ExteriorColors { get; set; }
        public List<VehicleKeyPairValueData> FuelTypes { get; set; }
        public List<VehicleKeyPairValueData> Engines { get; set; }
        public List<VehicleKeyPairValueData> Drives { get; set; }
        public List<VehicleKeyPairValueData> Mileages { get; set; }
        public List<VehicleKeyPairValueData> Engliters { get; set; }
        public List<VehicleKeyPairValueData> VehicleFeatures { get; set; }

        public List<VehicleKeyPairValueData> Transmissions { get; set; }
    }
}
