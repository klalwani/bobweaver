﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    [Serializable()]
    public class VehicleModelTrim : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleModelTrimId { get; set; }
        public int modelId { get; set; }
        public int trimId { get; set; }
    }
}
