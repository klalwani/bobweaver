﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class BodyStyle : EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BodyStyleId { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        public int? BodyStyleTypeId { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }
        
        [ForeignKey("BodyStyleTypeId")]
        public BodyStyleType BodyStyleType { get; set; }

        
    }
}
