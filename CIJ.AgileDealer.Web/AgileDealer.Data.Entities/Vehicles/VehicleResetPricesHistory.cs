﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleResetPricesHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int AgileVehicleId { get; set; }
        public decimal OverridePrice { get; set; }
        public decimal? MSRPOverride { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
