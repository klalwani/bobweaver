﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class Engine : EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EngineId { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public int? EngineTypeId { get; set; }
        public int? EngineTypeTranslatedId { get; set; }

        [ForeignKey("EngineTypeId")]
        public EngineType EngineType { get; set; }

        [ForeignKey("EngineTypeTranslatedId")]
        public EngineTypeTranslated EngineTypeTranslated { get; set; }
    }
}
