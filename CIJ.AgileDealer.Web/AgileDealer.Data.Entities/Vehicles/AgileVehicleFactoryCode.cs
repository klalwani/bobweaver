﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities
{
    public class AgileVehicleFactoryCode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AgileVehicleId { get; set; }
        public string FactoryCodes { get; set; }
    }
}
