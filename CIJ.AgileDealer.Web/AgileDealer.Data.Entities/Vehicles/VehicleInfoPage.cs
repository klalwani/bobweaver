﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleInfoPage : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleInfoPageId { get; set; }
        public string FullNamePlaceHolder { get; set; }
        public string PhonePlaceHolder { get; set; }
        public string EmailPlaceHolder { get; set; }
        public string MessagePlaceHolder { get; set; }
        public string PostalcodePlaceHolder { get; set; }
        public string TitlePlaceHolder { get; set; }
        public string ButtonPlaceHolder { get; set; }
        public string DealerLogo { get; set; }
        public string ExpectInformation { get; set; }
        public bool IsNew { get; set; }
		[ForeignKey("DealerID")]
		public Dealer Dealer { get; set; }
    }
}
