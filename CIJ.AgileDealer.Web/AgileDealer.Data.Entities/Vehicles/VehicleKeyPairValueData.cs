﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleKeyPairValueData
    {
        public VehicleKeyPairValueData()
        {
            SubList = new List<MasterData>();
            Name = string.Empty;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public int From { get; set; }
        public int To { get; set; }

        public List<MasterData> SubList { get; set; }
    }
}
