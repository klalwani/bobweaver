﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Buyer;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleFeedBack : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleFeedBackId { get; set; }
        public string AgileVehicleId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string PostalCode { get; set; }
        public string Message { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int? BuyerSessionId { get; set; }

        [ForeignKey("BuyerSessionId")]
        public BuyerSession BuyerSession { get; set; }
        public bool IsCompleted { get; set; }

        [NotMapped]
        public HomeNetVehicle HomeNetVehicle { get; set; }

        [NotMapped]
        public virtual string Vin { get; set; }
        [NotMapped]
        public virtual string DealerName { get; set; }

      


    }
}
