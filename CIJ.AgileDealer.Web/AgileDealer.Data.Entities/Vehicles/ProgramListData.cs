﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    [Serializable()]
    public class ProgramListData
    {
        public string Id { get; set; }
        public int NamePlateTrimId { get; set; }
        public int NamePlateId { get; set; }
        public string ProgramId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string IsSelected { get; set; }
        public string ProgramName { get; set; }
        public string Amount { get; set; }
        public double DoubleAmount
        {
            get
            {
                double result = 0;
                if (double.TryParse(Amount, out result)) {
                    return result;
                };
                
                return 0;
            }
        }
        public string Disclaimer { get; set; }
        public int? DisplaySection { get; set; }
        public int? Position { get; set; }
        public bool IsCashCoupon { get; set; }
        public bool IsCustomDiscount { get; set; }
        public string Conditional { get; set; }
        public string IsFmcc { get; set; }

        public bool IdBit { get; set; }
    }
}
