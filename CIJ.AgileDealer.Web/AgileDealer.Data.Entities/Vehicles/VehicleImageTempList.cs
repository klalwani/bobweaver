﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleImageTempList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehicleImageTempListId { get; set; }
        public string Imagelist { get; set; }
        public string ChromeColorizedImageName { get; set; }
        public int AgileVehicleId { get; set; }
    }
}
