﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Vehicles
{
    public class VehicleProgramData
    {
        public int AgileVehicleId { get; set; }
        public string Vin { get; set; }
        public int ModelId { get; set; }
        public int ProgramId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string IsActive { get; set; }
        public string IsDeleted { get; set; }
        public string ProgramName { get; set; }
        public string Amount { get; set; }
        public string ImageUrl { get; set; }
        public string Disclaimer { get; set; }
        public bool IsAprParent { get; set; }
        public string Id { get; set; }
        public int DisplaySection { get; set; }
        public int? Position { get; set; }
        public string TranslatedName { get; set; }
        public bool IsCashCoupon { get; set; }

        public bool IsUpfitted { get; set; }
    }
}
