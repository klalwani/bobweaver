﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class CustomFeeds
    {
        public string ID { get; set; }
        public string ID2 { get; set; }
        public string ItemTitle { get; set; }
        public string FinalUrl { get; set; }
        public string ImageUrl { get; set; }
        public string ItemSubTitle { get; set; }
        public string ItemDescription { get; set; }
        public string ItemCategory { get; set; }
        public string Price { get; set; }
        public string SalePrice { get; set; }
        public string ContextualKeywords { get; set; }
        public string ItemAddress { get; set; }
        public string TrackingTemplate { get; set; }
        public string CustomParameter { get; set; }
        public string FinalMobileUrl { get; set; }

    }
}
