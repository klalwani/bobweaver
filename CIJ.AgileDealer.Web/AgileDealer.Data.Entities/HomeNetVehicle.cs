﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class HomeNetVehicle : EntityBase
    {
        [Key,Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public DateTime? date_inquiry { get; set; } // date user request view of this item 

        public string vin { get; set; }

        public string stock { get; set; }

        public string type { get; set; }
        
        public int miles { get; set; }

        public string certified { get; set; }

        public string description { get; set; }
        
        public string description2 { get; set; }

        public string options { get; set; }

        public DateTime? dateinstock { get; set; }

        public int sellingprice { get; set; }

        public int msrp { get; set; }

        public int bookvalue { get; set; }

        public int invoice { get; set; }

        public int internetprice { get; set; }

        public int miscprice1 { get; set; }

        public int miscprice2 { get; set; }
        
        public int miscprice3 { get; set; }

        public string isspecial { get; set; }

        public string ishomepagespecial { get; set; }
        
        public int specialprice { get; set; }

        public DateTime? specials_start_date { get; set; }

        public DateTime? specials_end_date { get; set; }

        public short specials_monthly_payment { get; set; }

        public string specials_disclaimer { get; set; }

        public short year { get; set; }

        public string make { get; set; }

        public string model { get; set; }

        public string modelnumber { get; set; }

        public string trim { get; set; }

        public string styledescription { get; set; }

        public string friendlystyle { get; set; }

        public string body { get; set; }

        public short doors { get; set; }

        public string extcolor { get; set; }
    
        public string extcolorgeneric { get; set; }

        public string extcolor_code { get; set; }

        public string intcolor { get; set; }

        public string int_colorgeneric { get; set; }

        public string int_color_code { get; set; }

        public string extcolorhexcode { get; set; }

        public string intcolorhexcode { get; set; }

        public string intupholstery { get; set; }

        public int engcyls { get; set; }

        public string engliters { get; set; }

        public string engblock { get; set; }

        public string engaspiration { get; set; }

        public string engdescription { get; set; }

        public int engcubicinches { get; set; }

        public string trans { get; set; }

        public int transspeed { get; set; }

        public string transdescription { get; set; }

        public string drivetrain { get; set; }

        public string fueltype { get; set; }

        public int epacity { get; set; }

        public int epahighway { get; set; }

        public string epaclassification { get; set; }

        public string wheelbase { get; set; }

        public string passengercapacity { get; set; }

        public string marketclass { get; set; }

        public string comment1 { get; set; }

        public string comment2 { get; set; }

        public string comment3 { get; set; }

        public string comment4 { get; set; }

        public string comment5 { get; set; }

        public string soldflag { get; set; }

        public DateTime? imagesmodifieddate { get; set; }
        
        public DateTime? datamodifieddate { get; set; }

        public DateTime? datamodifieddateDeprecated { get; set; }

        public DateTime? datecreated { get; set; }

        public string techspecs { get; set; }

        public string standardequipment { get; set; }

        public string Standard { get; set; }

        public string exteriorequipment { get; set; }

        public string interiorequipment { get; set; }

        public string mechanicalequipment { get; set; }

        public string safetyequipment { get; set; }

        public string optionalequipment { get; set; }

        public string optionalequipmentshort { get; set; }

        public string factorycodes { get; set; }

        public string packagecodes { get; set; }

        public string Package { get; set; }

        public string dealeroptions { get; set; }

        public string iolvideolink { get; set; }

        public string iolmedialink { get; set; }

        public string stockimage { get; set; }

        public string imagelist { get; set; }

        public string evoxvif { get; set; }

        public string evoxsend { get; set; }

        public string evoxcolor1 { get; set; }

        public string evoxcolor2 { get; set; }

        public string evoxcolor3 { get; set; }

        public string standardmodel { get; set; }

        public string standardbody { get; set; }

        public string standardtrim { get; set; }

        public string standardyear { get; set; }

        public string standardstyle { get; set; }

        public string daysinstock { get; set; }

        public string imagecount { get; set; }

        public string dealername { get; set; }

        public string dealeraddress { get; set; }

        public string dealercity { get; set; }

        public string dealerstate { get; set; }

        public string dealerzip { get; set; }

        public string dealercontact { get; set; }

        public string dealerphone { get; set; }

        public string dealeremail { get; set; }

        public string dealerurl { get; set; }

        public string vehicletitle { get; set; }

        public string carfaxoneowner { get; set; }

        public string carfaxhistoryreporturl { get; set; }

        public string vehiclestestvideo { get; set; }

        public string toyotapackagedescriptions { get; set; }

        public DateTime? dateremoved { get; set; }

        public string flickfusionyoutubeurl { get; set; }

        public string flickfusionuploadedvideobasicurl { get; set; }

        public string sistervideolink { get; set; }

        public string chromecolorizedimagename { get; set; }

        public string chromemultiviewext1url { get; set; }

        public string chromemultiviewext2url { get; set; }

        public string chromemultiviewext3url { get; set; }

        public string chromemultiviewext1imagename { get; set; }

        public string chromemultiviewext2imagename { get; set; }

        public string chromemultiviewext3imagename { get; set; }

        public string standardmake { get; set; }

        public int LastHomeNetPullId { get; set; }
        [ForeignKey("LastHomeNetPullId")]
        public HomeNetPull HomeNetPull { get; set; }

        public string chromestyleid { get; set; }
    }
}
