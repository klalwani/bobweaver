﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class DealerServiceSpecial_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int DealerServiceSpeciaId { get; set; }
        public string Title { get; set; }

        public string ServiceSpecialsImage { get; set; }

        public string Description { get; set; }
        [ForeignKey("DealerServiceSpeciaId")]
        public virtual DealerServiceSpecial DealerServiceSpecial { get; set; }
    }
}
