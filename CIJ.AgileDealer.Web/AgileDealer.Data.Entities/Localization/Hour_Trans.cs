﻿using AgileDealer.Data.Entities.Content;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class Hour_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int HoursId { get; set; }
        [ForeignKey("HoursId")]
        public virtual Hours Hour { get; set; }
        [MaxLength(50)]
        public string ShortName { get; set; }

        [MaxLength(16)]
        public string OperationStatus { get; set; }

        public int HourTypeID { get; set; }
    }
}
