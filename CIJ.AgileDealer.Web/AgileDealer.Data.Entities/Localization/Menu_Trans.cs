﻿using AgileDealer.Data.Entities.Localization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AgileDealer.Data.Entities.Nagivation;

namespace AgileDealer.Data.Entities.Localization
{
    public class Menu_Trans : TranslationBase
    {

        [Key]
        [Column(Order = 1)]
        public int MenuId { get; set; }

        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }

		[MaxLength(65)]
		public string Title { get; set; }
    }
 
}
