﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Blog;

namespace AgileDealer.Data.Entities.Localization
{
    public class BlogEntry_Trans: TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int BlogEntryId { get; set; }
        public string Name { get; set; }
        [ForeignKey("BlogEntryId")]
        public virtual BlogEntry BlogEntry { get; set; }

        [MaxLength(256)]
        public string PostTitle { get; set; }

        [MaxLength(512)]
        public string PostUrl { get; set; }

        [MaxLength(4000)]
        public string PostSummary { get; set; }

        public string PostText { get; set; }

        [MaxLength(256)]
        public string Button1Text { get; set; }

        [MaxLength(1024)]
        public string Button1Path { get; set; }

        [MaxLength(256)]
        public string Button2Text { get; set; }

        [MaxLength(1024)]
        public string Button2Path { get; set; }
    }
}
