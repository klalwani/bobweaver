﻿using AgileDealer.Data.Entities.Applicants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Localization
{
    public class Notification_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int NotificationId { get; set; }
        [ForeignKey("NotificationId")]
        public virtual Notification Notification { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
