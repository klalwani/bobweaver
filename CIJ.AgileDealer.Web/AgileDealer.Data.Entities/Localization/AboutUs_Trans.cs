﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class AboutUs_Trans : TranslationBase
    {
        [Key]
        [Column(Order =1 )]
        public int AboutUsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        [ForeignKey("AboutUsId")]
        public virtual AboutUs.AboutUs AboutUs { get; set; }
    }
}
