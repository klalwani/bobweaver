﻿using AgileDealer.Data.Entities.Content;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgileDealer.Data.Entities.Localization
{
    public class Header_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int HeaderId { get; set; }
        [Key]
        [Column(Order = 3)]
        public int DealerID { get; set; }
        public string PersonalizationText { get; set; }
        public string PersonalizationLogo { get; set; }
        public string PeronsalizationAltText { get; set; }
        public string CTAImage { get; set; }
        public string PeronsalizationUrl { get; set; }
        public string CTAImageAltText { get; set; }
        public string CTALink { get; set; }
        [ForeignKey("HeaderId")]
        public virtual Header Header { get; set; }
        [ForeignKey("DealerID")]
        public virtual Dealer Dealer { get; set; }
    }
}
