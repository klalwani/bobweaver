﻿using AgileDealer.Data.Entities.ScheduleService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class ServiceRequested_Trans : TranslationBase
    {
        [Key]
        [Column(Order =1)]
        public int ServiceRequestedId { get; set; }
        public string Name { get; set; }
        [ForeignKey("ServiceRequestedId")]
        public virtual ServiceRequested ServiceRequested { get; set; }
    }
}
