﻿using AgileDealer.Data.Entities.Content;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class Service_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int ServiceId { get; set; }

        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Disclaimer { get; set; }
    }
}
