﻿using AgileDealer.Data.Entities.ScheduleService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class CurrentIssue_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int CurrentIssueId { get; set; }
        public string Name { get; set; }
        [ForeignKey("CurrentIssueId")]
        public virtual CurrentIssue CurrentIssue { get; set; }
    }
}
