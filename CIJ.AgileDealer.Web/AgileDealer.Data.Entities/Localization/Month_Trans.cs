﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class Month_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int MonthId { get; set; }
        [ForeignKey("MonthId")]
        public virtual Month Month { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
