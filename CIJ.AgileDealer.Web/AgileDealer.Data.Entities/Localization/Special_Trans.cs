﻿using AgileDealer.Data.Entities.Content;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class Special_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int SpecialId { get; set; }

        [ForeignKey("SpecialId")]
        public virtual Special Special { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Disclaimer { get; set; }

        [MaxLength(128)]
        public string Image { get; set; }
    }
}
