﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AgileDealer.Data.Entities.CMS;

namespace AgileDealer.Data.Entities.Localization
{
	public class ContentPage_Trans : TranslationBase
	{
		[Key]
		[Column(Order = 1)]
		public int ContentPageId { get; set; }

		[ForeignKey("ContentPageId")]
		public virtual ContentPage ContentPage { get; set; }

		public string BodyContent { get; set; }

		public string Title { get; set; }
	}
}
