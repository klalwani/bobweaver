﻿using AgileDealer.Data.Entities.Content;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class ServicePage_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int ServicePageId { get; set; }

        [ForeignKey("ServicePageId")]
        public virtual ServicePage ServicePage { get; set; }

        public string OilContent { get; set; }

        public string BrakeContent { get; set; }

        public string TireContent { get; set; }
    }
}
