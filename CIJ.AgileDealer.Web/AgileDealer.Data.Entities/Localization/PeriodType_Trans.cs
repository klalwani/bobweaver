﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Localization
{
    public class PeriodType_Trans : TranslationBase
    {
        [Key]
        [Column(Order = 1)]
        public int PeriodTypeId { get; set; }
        public string PeriodTypeName { get; set; }
        [ForeignKey("PeriodTypeId")]
        public virtual PeriodType PeriodType { get; set; }
    }
}
