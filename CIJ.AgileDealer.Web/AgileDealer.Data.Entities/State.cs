﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities
{
    public class State : EntityLku
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StateId { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        public string Code { get; set; }

        public bool IsDisabled { get; set; }

        public bool IsActive { get; set; }
    }
}
