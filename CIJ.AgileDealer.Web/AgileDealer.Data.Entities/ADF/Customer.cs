﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Customer
    {
        public Customer()
        {
            Contact = new Contact();
        }

        [XmlElement("contact")]
        public Contact Contact { get; set; }

        [XmlElement("comments")]
        public string Comments { get; set; }
    }
}
