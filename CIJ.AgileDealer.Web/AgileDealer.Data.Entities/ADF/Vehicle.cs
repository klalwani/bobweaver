﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    public class Vehicle
    {

        [XmlAttribute("status")]
        public string Status { get; set; }

        [XmlAttribute("interest")]
        public string Interest { get; set; }

        [XmlElement("year")]
        public string Year { get; set; }

        [XmlElement("make")]
        public string Make { get; set; }

        [XmlElement("model")]
        public string Model { get; set; }

        [XmlElement("vin")]
        public string Vin { get; set; }

        [XmlElement("stock")]
        public string Stock { get; set; }

        [XmlElement("trim")]
        public string Trim { get; set; }

        [XmlElement("doors")]
        public string Doors { get; set; }

        [XmlElement("bodystyle")]
        public string Bodystyle { get; set; }

        [XmlElement("transmission")]
        public string Transmission { get; set; }

        [XmlElement("odometer")]
        public Odometer Odometer { get; set; }

        [XmlElement("colorcombination")]
        public ColorCombination ColorCombination { get; set; }

        [XmlElement("price")]
        public Price Price { get; set; }
    }
}
