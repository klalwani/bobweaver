﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Vendor
    {
        [XmlElement("vendorname")]
        public string VendorName{ get; set; }
    }
}
