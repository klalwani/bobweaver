﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [XmlRoot("adf")]
    public class ADF
    {
        public ADF()
        {
            Prospect = new Prospect();
        }

        [XmlElement("prospect")]
        public Prospect Prospect { get; set; }
    }
}
