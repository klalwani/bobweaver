﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    public class Price
    {
        [XmlText]
        public string Text { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("currency")]
        public string Currency { get; set; }
    }
}
