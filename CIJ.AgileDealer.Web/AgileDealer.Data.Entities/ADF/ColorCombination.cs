﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    public class ColorCombination
    {
        [XmlElement("interiorcolor")]
        public string InteriorColor { get; set; }

        [XmlElement("exteriorcolor")]
        public string ExteriorColor { get; set; }

        [XmlElement("preference")]
        public string Preference { get; set; }
    }
}
