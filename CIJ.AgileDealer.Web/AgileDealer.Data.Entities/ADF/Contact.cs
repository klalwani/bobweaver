﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Contact
    {
        public Contact()
        {
            Names = new List<Name>();
        }

        [XmlAttribute("primarycontact")]
        public int PrimaryContact { get; set; }

        [XmlElement("name")]
        public List<Name> Names { get; set; }

        [XmlElement("email")]
        public string Email { get; set; }

        [XmlElement("phone")]
        public List<Phone> Phones { get; set; }

        [XmlElement("address")]
        public Address Address { get; set; }

        [XmlIgnore]
        private string _firstName;

        [XmlIgnore]
        public string FirstName
        {
            get
            {
                if (string.IsNullOrEmpty(_firstName))
                    return string.Empty;
                return _firstName;
            }
            set { _firstName = value; }
        }

        [XmlIgnore]
        private string _lastName;
        [XmlIgnore]
        public string LastName
        {
            get
            {
                if (string.IsNullOrEmpty(_lastName))
                    return string.Empty;
                return _lastName;
            }
            set { _lastName = value; }
        }
    }
}
