﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Address
    {
        [XmlElement("street")]
        public string Street { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("regioncode")]
        public string RegionCode { get; set; }

        [XmlElement("postalcode")]
        public string PostalCode { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }
    }
}
