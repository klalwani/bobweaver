﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Odometer
    {
        [XmlText]
        public string Text { get; set; }

        [XmlAttribute("status")]
        public string Status { get; set; }

        [XmlAttribute("units")]
        public string Units { get; set; }
    }
}
