﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Phone
    {
        [XmlText]
        public string Text { get; set; }

        [XmlAttribute("time")]
        public string Time { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
