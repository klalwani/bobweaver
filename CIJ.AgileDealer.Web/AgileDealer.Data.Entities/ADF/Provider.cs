﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Provider
    {
        [XmlElement("name")]
        public Name Name { get; set; }

        [XmlElement("service")]
        public string Service { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        [XmlElement("email")]
        public string Email { get; set; }

        [XmlElement("contact")]
        public Contact Contact { get; set; }
    }
}
