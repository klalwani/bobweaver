﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AgileDealer.Data.Entities.ADF
{
    [Serializable]
    public class Prospect
    {
        public Prospect()
        {
            Vehicles = new List<Vehicle>();
            Customer = new Customer();
            Provider = new Provider();
            Vendor = new Vendor();
        }

        [XmlElement("requestdate")]
        public string RequestDate { get; set; }

        [XmlElement("vehicle")]
        public List<Vehicle> Vehicles { get; set; }

        [XmlElement("customer")]
        public Customer Customer { get; set; }

        [XmlElement("provider")]
        public Provider Provider { get; set; }

        [XmlElement("vendor")]
        public Vendor Vendor { get; set; }
    }
}
