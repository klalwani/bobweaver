﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Vehicles;

namespace AgileDealer.Data.Entities
{
    [Serializable()]
    public class DefaultFilter
    {
        public DefaultFilter()
        {
            DefaultTypes = new List<string>();
            DefaultModels = new List<int>();
            DefaultModelTrims = new List<VehicleModelTrim>();
            DefaultVehicleBodyStandardBodyStyle = new List<VehicleBodyStandardBodyStyle>();
            DefaultMakes = new List<int>();
            DefaultBodyStyleTypes = new List<int>();
            DefaultPrices = new List<int>();
            IsExpandType = false;
            IsExpandModel = false;
            IsExpandBodyStyle = false;
            IsLifted = false;
            IsUpfitted = false;
            InLitmitedMode = false;
            IsRentalSpecial = false;
        }


        public bool IsExpandType { get; set; }
        public bool IsExpandModel { get; set; }
        public bool IsExpandBodyStyle { get; set; }
        public bool IsLifted { get; set; }
        public bool IsUpfitted { get; set; }
        public bool IsSpecial { get; set; }

        public bool IsRentalSpecial { get; set; }
        public bool IsLimo { get; set; }
        public bool InLitmitedMode { get; set; }
        public List<string> DefaultTypes { get; set; }
        public List<int> DefaultPrices { get; set; }
        public List<int> DefaultModels { get; set; }
        public List<VehicleModelTrim> DefaultModelTrims { get; set; }
        public List<int> DefaultMakes { get; set; }
        public List<int> DefaultBodyStyleTypes { get; set; }
        public List<VehicleBodyStandardBodyStyle> DefaultVehicleBodyStandardBodyStyle { get; set; }
    }
}
