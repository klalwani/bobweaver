﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.ErrorLog
{
    [Serializable]
    public class ErrorLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ErrorLogId { get; set; }
        public int SequenceId { get; set; }
        [MaxLength(150)]
        public string Application { get; set; }

        [MaxLength(3000)]
        public string Message { get; set; }

        [MaxLength(3000)]
        public string Source { get; set; }

        public string StackTrace { get; set; }

        [MaxLength(3000)]
        public string TargetSite { get; set; }
        public string ContextQueries { get; set; }
        public int ContextQueryCount { get; set; }

        public DateTime DateCreated { get; set; }

        [DefaultValue(0)]
        public int CreatedUserId { get; set; }
    }
}
