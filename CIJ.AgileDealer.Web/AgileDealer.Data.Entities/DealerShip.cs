﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.Content;

namespace AgileDealer.Data.Entities
{
    public class DealerShip: EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DealerShipId { get; set; }
        public int DealerID { get; set; }
        public string DealerShipName { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("DealerID")]
        public Dealer Dealer { get; set; }
    }
}
