﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Enums
{
    public enum JobStatusEnum
    {
        Triggered,
        Running,
        Failed,
        Completed
    }
}
