﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Data.Entities.Parts
{
    public class PartRequest : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PartRequestId { get; set; }

        [MaxLength(512)]
        public string FirstName { get; set; }

        [MaxLength(512)]
        public string LastName { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }

        [MaxLength(64)]
        public string PhoneNumber { get; set; }

        [MaxLength(20)]
        public string PostalCode { get; set; }

        [MaxLength(20)]
        public string Year { get; set; }

        [MaxLength(20)]
        public string Make { get; set; }

        [MaxLength(20)]
        public string Model { get; set; }

        [MaxLength(20)]
        public string Vin { get; set; }

        public string PartNumber { get; set; }
        public string ContactBy { get; set; }
    }
}
