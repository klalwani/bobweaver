﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Content;
namespace AgileDealer.Data.Entities.ContactUs
{
    [Serializable]
    [Table("CustomerQuestions")]
    public class CustomerQuestion : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerQuestionId { get; set; }

        [MaxLength(255)]
        public string FirstName { get; set; }

        [MaxLength(255)]
        public string LastName { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }
        public int DealerID { get; set; }

        [MaxLength(255)]
        public string PhoneNumber { get; set; }

        [MaxLength(6)]
        public string PostalCode { get; set; }

        public string Questions { get; set; }

        [ForeignKey("DealerID")]
        [XmlIgnore]
        public Dealer Dealer { get; set; }

        public int? BuyerSessionId { get; set; }
        
        [ForeignKey("BuyerSessionId")]
        [XmlIgnore]
        public BuyerSession BuyerSession { get; set; }

        public bool IsCompleted { get; set; }
    }
}
