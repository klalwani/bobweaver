﻿using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Infra.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIJ.AgileDealer.Infra.Repositories
{
    public class VehicleRepository
    {
        public void AddVehicle(List<HomeNetVehicle> vList)
        {
            using (var db = new AppDbContext())
            {
                foreach (var v in vList)
                {
                    try
                    {
                        // mark as update if vehicle exists
                        if (db.Vehicles.Any(a => a.Id == v.Id))
                            db.Entry(v).State = EntityState.Modified;
                        else
                            db.Vehicles.Add(v);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        db.Vehicles.Remove(v);
                        // TODO: Log error
                    }
                }
            }
        }
    }
}
