﻿using AgileDealer.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace CJI.AgileDealer.Infra
{
    public class HomeNetiolRepository
    {
        GatewaySoapClient homenetClient = null;
        public HomeNetiolRepository()
        {
            homenetClient = new GatewaySoapClient("HomeNetiolGatewaySoap");
        }
        public List<HomeNetVehicle> LoadCompressedVehicles(bool isNew, DateTime updatedSince, string[] dealerIdList)
        {
            var homenetRepo = new HomeNetiolRepository();
            var vImportList = new List<HomeNetVehicle>();
            string intetragedToken = "ebfc7e87-0014-4f34-9dd3-d9b00e15955e";
            
            var result = homenetRepo.LoadCompressedVehicles(intetragedToken, dealerIdList, fieldList, updatedSince, isNew);

            if (result != null && !string.IsNullOrEmpty(result.CompressedVehicles))
                vImportList = LoadVehicleFromXML(result.CompressedVehicles);

            return vImportList;

        }
        public List<HomeNetVehicle> LoadVehicleFromXML(string xml)
        {
            var doc = new System.Xml.XmlDocument();
            var vImportList = new List<HomeNetVehicle>();
            System.Xml.XmlNodeList vehicles = null;

            //if (result != null && !string.IsNullOrEmpty(result.CompressedVehicles))
            if (!string.IsNullOrEmpty(xml))
            {
                doc.LoadXml(xml);
                vehicles = doc.SelectNodes("//F//RS//R//VS//V");
            }

            if (vehicles != null)
            {
                foreach (XmlNode v in vehicles)
                {
                    var dateInStock = GetDateAndResetAttr(v, "@_9");
                    var specials_start_date = GetDateAndResetAttr(v, "@_21");
                    var specials_end_date = GetDateAndResetAttr(v, "@_22");
                    var imagesmodifieddate = GetDateAndResetAttr(v, "@_66");
                    var datamodifieddate = GetDateAndResetAttr(v, "@_67");
                    var datamodifieddateDeprecated = GetDateAndResetAttr(v, "@_68");
                    var datecreated = GetDateAndResetAttr(v, "@_69");
                    var dateremoved = GetDateAndResetAttr(v, "@_114");


                    XmlSerializer serializer = new XmlSerializer(typeof(VehicleRaw));
                    using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(v.OuterXml)))
                    {
                        try
                        {
                            var vehicleItem = (VehicleRaw)serializer.Deserialize(memStream);
                            var vehicle = ConvertToVehicle(vehicleItem, dateInStock, specials_start_date, specials_end_date
                                , imagesmodifieddate, datamodifieddate, datamodifieddateDeprecated, datecreated, dateremoved);

                            vImportList.Add(vehicle);

                        }
                        catch (Exception ex)
                        {
                            // Log error and move next
                        }
                    }
                }
            }
            return vImportList;
        }

        private HomeNetVehicle ConvertToVehicle(VehicleRaw vehicleItem, DateTime? dateInStock, DateTime? specials_start_date,
            DateTime? specials_end_date, DateTime? imagesmodifieddate, DateTime? datamodifieddate,
            DateTime? datamodifieddateDeprecated, DateTime? datecreated, DateTime? dateremoved)
        {
            // TODO: Convert VehicleRaw to AgileDealer.Data.Entities.Vehicle
            var vehicle = new HomeNetVehicle
            {
                vin = vehicleItem.vin,
                body = vehicleItem.body,
                Id = int.Parse(vehicleItem.vehicleid),
                stockimage = vehicleItem.stockimage,
                type = vehicleItem.type,
                stock = vehicleItem.stock,
                certified = vehicleItem.certified,
                description = vehicleItem.description,
                description2 = vehicleItem.description2,
                options = vehicleItem.options,
                isspecial = vehicleItem.isspecial,
                ishomepagespecial = vehicleItem.ishomepagespecial,
                specials_disclaimer = vehicleItem.specials_disclaimer,
                make = vehicleItem.make,
                model = vehicleItem.model,
                modelnumber = vehicleItem.modelnumber,
                trim = vehicleItem.trim,
                styledescription = vehicleItem.styledescription,
                extcolor = vehicleItem.extcolor,
                passengercapacity = vehicleItem.passengercapacity,
                marketclass = vehicleItem.marketclass,
                comment1 = vehicleItem.comment1,
                comment2 = vehicleItem.comment2,
                comment3 = vehicleItem.comment3,
                comment4 = vehicleItem.comment4,
                comment5 = vehicleItem.comment5,
                soldflag = vehicleItem.soldflag,
                techspecs = vehicleItem.techspecs,
                standardequipment = vehicleItem.standardequipment,
                Standard = vehicleItem.Standard,
                exteriorequipment = vehicleItem.exteriorequipment,
                interiorequipment = vehicleItem.interiorequipment,
                mechanicalequipment = vehicleItem.mechanicalequipment,
                safetyequipment = vehicleItem.safetyequipment,
                optionalequipment = vehicleItem.optionalequipment,
                optionalequipmentshort = vehicleItem.optionalequipmentshort,
                factorycodes = vehicleItem.factorycodes,
                packagecodes = vehicleItem.packagecodes,
                Package = vehicleItem.Package,
                dealeroptions = vehicleItem.dealeroptions,
                iolvideolink = vehicleItem.iolvideolink,
                iolmedialink = vehicleItem.iolmedialink,
                imagelist = vehicleItem.imagelist,
                evoxvif = vehicleItem.evoxvif,
                evoxsend = vehicleItem.evoxsend,
                evoxcolor1 = vehicleItem.evoxcolor1,
                evoxcolor2 = vehicleItem.evoxcolor2,
                evoxcolor3 = vehicleItem.evoxcolor3,
                standardmodel = vehicleItem.standardmodel,
                standardbody = vehicleItem.standardbody,
                standardtrim = vehicleItem.standardtrim,
                standardyear = vehicleItem.standardyear,
                standardstyle = vehicleItem.standardstyle,
                daysinstock = vehicleItem.daysinstock,
                imagecount = vehicleItem.imagecount,
                dealername = vehicleItem.dealername,
                dealeraddress = vehicleItem.dealeraddress,
                dealercity = vehicleItem.dealercity,
                dealerstate = vehicleItem.dealerstate,
                dealerzip = vehicleItem.dealerzip,
                dealercontact = vehicleItem.dealercontact,
                dealerphone = vehicleItem.dealerphone,
                dealeremail = vehicleItem.dealeremail,
                dealerurl = vehicleItem.dealerurl,
                vehicletitle = vehicleItem.vehicletitle,
                carfaxoneowner = vehicleItem.carfaxoneowner,
                carfaxhistoryreporturl = vehicleItem.carfaxhistoryreporturl,
                vehiclestestvideo = vehicleItem.vehiclestestvideo,
                toyotapackagedescriptions = vehicleItem.toyotapackagedescriptions,
                flickfusionyoutubeurl = vehicleItem.flickfusionyoutubeurl,
                flickfusionuploadedvideobasicurl = vehicleItem.flickfusionuploadedvideobasicurl,
                sistervideolink = vehicleItem.sistervideolink,
                chromecolorizedimagename = vehicleItem.chromecolorizedimagename,
                chromemultiviewext1url = vehicleItem.chromemultiviewext1url,
                chromemultiviewext2url = vehicleItem.chromemultiviewext2url,
                chromemultiviewext3url = vehicleItem.chromemultiviewext3url,
                chromemultiviewext1imagename = vehicleItem.chromemultiviewext1imagename,
                chromemultiviewext2imagename = vehicleItem.chromemultiviewext2imagename,
                chromemultiviewext3imagename = vehicleItem.chromemultiviewext3imagename,
            };

            // Sang to complete the conversion here
            vehicle.miles = ConvertToInt(vehicleItem.miles);

            vehicle.dateinstock = dateInStock;
            vehicle.specials_start_date = specials_start_date;
            vehicle.specials_end_date = specials_end_date;
            vehicle.imagesmodifieddate = imagesmodifieddate;
            vehicle.datamodifieddate = datamodifieddate;
            vehicle.datamodifieddateDeprecated = datamodifieddateDeprecated;
            vehicle.datecreated = datecreated;
            vehicle.dateremoved = dateremoved;

            vehicle.sellingprice = ConvertToInt(vehicleItem.sellingprice);
            vehicle.msrp = ConvertToInt(vehicleItem.msrp);
            vehicle.bookvalue = ConvertToInt(vehicleItem.bookvalue);
            vehicle.invoice = ConvertToInt(vehicleItem.invoice);
            vehicle.internetprice = ConvertToInt(vehicleItem.internetprice);
            vehicle.miscprice1 = ConvertToInt(vehicleItem.miscprice1);
            vehicle.miscprice2 = ConvertToInt(vehicleItem.miscprice2);
            vehicle.miscprice3 = ConvertToInt(vehicleItem.miscprice3);
            vehicle.specialprice = ConvertToInt(vehicleItem.specialprice);

            vehicle.specials_monthly_payment = ConvertToShort(vehicleItem.specials_monthly_payment);
            vehicle.year = ConvertToShort(vehicleItem.year);
            vehicle.doors = ConvertToShort(vehicleItem.doors);
            vehicle.engcyls = ConvertToShort(vehicleItem.engcyls);
            vehicle.engcubicinches = ConvertToInt(vehicleItem.engcubicinches);
            vehicle.transspeed = ConvertToShort(vehicleItem.transspeed);
            vehicle.epacity = ConvertToShort(vehicleItem.epacity);
            vehicle.epahighway = ConvertToShort(vehicleItem.epahighway);

            return vehicle;
        }
        private short ConvertToShort(string value)
        {
            short retVal = 0;
            if (!string.IsNullOrEmpty(value))
            {
                if (short.TryParse(value, out retVal))
                    return retVal;
            }

            return 0;
        }
        private int ConvertToInt(string value)
        {
            int retVal = 0;
            if (!string.IsNullOrEmpty(value))
            {
                if (int.TryParse(value, out retVal))
                    return retVal;
            }

            return 0;
        }

        private DateTime? GetDateAndResetAttr(XmlNode ParentNode, string xpath)
        {
            DateTime date = DateTime.MinValue;
            var childNode = ParentNode.SelectSingleNode(xpath);
            if (childNode != null && !string.IsNullOrEmpty(childNode.Value))
            {
                DateTime.TryParse(childNode.Value, out date);
                childNode.Value = string.Empty;
                childNode = null;
                return date;
            }

            return null;
        }
        private CompressedInventoryResults LoadCompressedVehicles(string integratedToken, 
            string[] dealerIdList, string[] fieldList, DateTime updatedSince, bool isNew)
        {
            string filterType = (isNew ? "NEW": "USED");

            return homenetClient.LoadCompressedVehicles(integratedToken,
                    string.Join(",", dealerIdList),
                    string.Join(",", fieldList),
                    string.Format("{0:yyyy/MM/dd}", updatedSince), filterType);

        }

        string[] fieldList = new string[] {"vehicleid"
,"vin"
,"stock"
,"type"
,"miles"
,"certified"
,"description"
,"description2"
,"options"
,"dateinstock"
,"sellingprice"
,"msrp"
,"bookvalue"
,"invoice"
,"internetprice"
,"miscprice1"
,"miscprice2"
,"miscprice3"
,"isspecial"
,"ishomepagespecial"
,"specialprice"
,"specials_start_date"
,"specials_end_date"
,"specials_monthly_payment"
,"specials_disclaimer"
,"year"
,"make"
,"model"
,"modelnumber"
,"trim"
,"styledescription"
,"friendlystyle"
,"body"
,"doors"
,"extcolor"
,"extcolorgeneric"
,"extcolor_code"
,"intcolor"
,"int_colorgeneric"
,"int_color_code"
,"extcolorhexcode"
,"intcolorhexcode"
,"intupholstery"
,"engcyls"
,"engliters"
,"engblock"
,"engaspiration"
,"engdescription"
,"engcubicinches"
,"trans"
,"transspeed"
,"transdescription"
,"drivetrain"
,"fueltype"
,"epacity"
,"epahighway"
,"epaclassification"
,"wheelbase"
,"passengercapacity"
,"marketclass"
,"comment1"
,"comment2"
,"comment3"
,"comment4"
,"comment5"
,"soldflag"
,"imagesmodifieddate"
,"datamodifieddate"
,"datamodifieddateDeprecated"
,"datecreated"
,"techspecs"
,"standardequipment"
,"Standard"
,"exteriorequipment"
,"interiorequipment"
,"mechanicalequipment"
,"safetyequipment"
,"optionalequipment"
,"optionalequipmentshort"
,"factorycodes"
,"packagecodes"
,"Package"
,"dealeroptions"
,"iolvideolink"
,"iolmedialink"
,"stockimage"
,"imagelist"
,"evoxvif"
,"evoxsend"
,"evoxcolor1"
,"evoxcolor2"
,"evoxcolor3"
,"standardmake"
,"standardmodel"
,"standardbody"
,"standardtrim"
,"standardyear"
,"standardstyle"
,"daysinstock"
,"imagecount"
,"dealername"
,"dealeraddress"
,"dealercity"
,"dealerstate"
,"dealerzip"
,"dealercontact"
,"dealerphone"
,"dealeremail"
,"dealerurl"
,"vehicletitle"
,"carfaxoneowner"
,"carfaxhistoryreporturl"
,"vehiclestestvideo"
,"toyotapackagedescriptions"
,"dateremoved"
,"flickfusionyoutubeurl"
,"flickfusionuploadedvideobasicurl"
,"sistervideolink"
,"chromecolorizedimagename"
,"chromemultiviewext1url"
,"chromemultiviewext2url"
,"chromemultiviewext3url"
,"chromemultiviewext1imagename"
,"chromemultiviewext2imagename"
,"chromemultiviewext3imagename"
 };

    }
}
