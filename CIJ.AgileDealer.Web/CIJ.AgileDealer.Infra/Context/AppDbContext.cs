﻿using AgileDealer.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIJ.AgileDealer.Infra.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("AgileDealer")
        {
        }


        public DbSet<HomeNetVehicle> Vehicles { get; set; }


    }
}
