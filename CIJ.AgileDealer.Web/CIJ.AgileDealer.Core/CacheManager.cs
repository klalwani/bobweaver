﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace CIJ.AgileDealer.Core
{
    /// <summary>
	/// Class CacheManager
	/// </summary>
    public static class CacheManager
    {
        /// <summary>
        /// The _cache
        /// </summary>
        private static readonly ObjectCache _cache = MemoryCache.Default;
        /// <summary>
        /// The _expire time
        /// </summary>
        private static readonly int _expireTime = 30; //30 minutes

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns>``0.</returns>
        public static T GetData<T>(string key)
        {
            if (_cache.Contains(key))
            {
                object data = _cache.Get(key);
                return (T)data;
            }

            return default(T);
        }

        /// <summary>
        /// Sets the data.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        public static void SetData(string key, object data)
        {
            //remove old cached data if exists
            Clear(key);

            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(_expireTime);
            _cache.Set(key, data, cacheItemPolicy);
        }

        /// <summary>
        /// Clears the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Clear(string key)
        {
            if (_cache.Contains(key))
                _cache.Remove(key);
        }

        /// <summary>
        /// Clears all.
        /// </summary>
        public static void ClearAll()
        {
            List<string> cacheKeys = _cache.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                _cache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// Existses the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        public static bool Exists(string key)
        {
            return _cache.Contains(key);
        }
    }
}
