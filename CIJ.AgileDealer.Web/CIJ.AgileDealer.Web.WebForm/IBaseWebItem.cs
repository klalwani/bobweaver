﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm
{
    public interface IBaseWebItem
    {
        int DealerID { get;}
        string DealerName { get;}
        string DealerCity { get; }
        string DealerState { get;}
        string DealerImageGeneric { get; }
        string ImageServer { get; }
        string EleadTrackAdress { get; }
        int LanguageId { get; }
    }
}