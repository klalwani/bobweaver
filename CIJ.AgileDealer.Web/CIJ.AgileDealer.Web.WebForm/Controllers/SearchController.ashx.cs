﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
using AgileDealer.Data.Entities;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;

using Microsoft.Ajax.Utilities;
using AgileDealer.Common;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for SearchController
    /// </summary>
    public class SearchController : BaseController, IRequiresSessionState
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetVehicles":
                        await GetVehicles(context, strJson);
                        break;
                    case "GetCounter":
                        GetCounterForVehicles(context, strJson);
                        break;
                    case "GetVehicle":
                        GetVehicle(context, strJson);
                        break;
                    case "UpdateVehicle":
                        UpdateVehicle(context, strJson);
                        break;
                    case "PushVehicleHistoryIntoSession":
                        PushVehicleHistoryIntoSession(context, strJson);
                        break;
                    case "GetVehicleHistoryIntoSession":
                        GetVehicleHistoryIntoSession(context, strJson);
                        break;
                }
            }
            catch (ThreadAbortException ex1)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("SearchController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method + " with data: " + strJson);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public void PushVehicleHistoryIntoSession(HttpContext context, string strJson)
        {
            VehicleSearchCriteria filter = Deserialize<VehicleSearchCriteria>(strJson);
            if (filter != null)
            {
                HttpContext.Current.Session["Vehicle-Data"] = filter;//Store search criteria
            }
        }
        public void GetVehicleHistoryIntoSession(HttpContext context, string strJson)
        {
            bool rememberHistory = Deserialize<bool>(strJson);
            if (HttpContext.Current.Session["Vehicle-Data"] != null && rememberHistory)
            {
                var result = new
                {
                    vehicle = JsonConvert.SerializeObject(HttpContext.Current.Session["Vehicle-Data"]),
                    isSuccess = true
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
            //Get search criteria
            else
            {
                var result = new
                {
                    vehicle = string.Empty,
                    isSuccess = true
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public void UpdateVehicle(HttpContext context, string strJson)
        {
            AgileVehicle tempVehicle = Deserialize<AgileVehicle>(strJson);
            if (tempVehicle != null)
            {
                AgileVehicle vehicle = VehicleHelper.GetAgileVehicleByVin(tempVehicle.Vin, DealerID);
                if (vehicle != null)
                {
                    vehicle.IsSpecial = tempVehicle.IsSpecial;
                    vehicle.IsRentalSpecial = tempVehicle.IsRentalSpecial;
                    vehicle.HideIncentives = tempVehicle.HideIncentives;
                    vehicle.IsLift = tempVehicle.IsLift;
                    vehicle.IsUpfitted = tempVehicle.IsUpfitted;
                    vehicle.UpfittedDescription = tempVehicle.UpfittedDescription;
                    vehicle.UpfittedCost = tempVehicle.UpfittedCost;

                    vehicle.IsHidden = tempVehicle.IsHidden;
                    vehicle.OverridePrice = tempVehicle.OverridePrice;
                    vehicle.OverrideDescription = tempVehicle.OverrideDescription;
                    vehicle.AutowriterDescription = tempVehicle.AutowriterDescription;
                    vehicle.HasSetDateRange = tempVehicle.HasSetDateRange;
                    vehicle.OverrideStartDate = tempVehicle.OverrideStartDate;
                    if (tempVehicle.OverrideEndDate != null)
                        vehicle.OverrideEndDate = Convert.ToDateTime(tempVehicle.OverrideEndDate.Value.ToShortDateString() + " 23:59:59");
                    else
                        vehicle.OverrideEndDate = tempVehicle.OverrideEndDate;
                    vehicle.MSRPOverride = tempVehicle.MSRPOverride;
                    vehicle.EmployeePrice = tempVehicle.EmployeePrice;
                    bool isUpdated = VehicleHelper.UpdateAgileVehicle(vehicle, DealerID);
                    var result = new
                    {
                        isSuccess = isUpdated,
                    };
                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            }
        }

        public void GetVehicle(HttpContext context, string vin)
        {
            if (!string.IsNullOrEmpty(vin))
            {
                VehicleData vehicle = VehicleHelper.GetVehicleByVin(vin);
                if (vehicle != null)
                {
                    if (vehicle.MSRPOverride > 0)
                        vehicle.DealerSaving = (vehicle.MSRPOverride > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRPOverride > vehicle.OriginalSellingPrice) ? (vehicle.MSRPOverride - vehicle.OriginalSellingPrice) : 0;
                    else
                        vehicle.DealerSaving = (vehicle.MSRP > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRP > vehicle.OriginalSellingPrice) ? (vehicle.MSRP - vehicle.OriginalSellingPrice) : 0;


                }
                var result = new
                {
                    vehicle = vehicle,
                    isSuccess = true
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public async Task GetVehicles(HttpContext context, string strJson)
        {
            VehicleFilter filter = Deserialize<VehicleFilter>(strJson);

            if (filter != null)
            {
                VehicleSearchCriteria criteria = filter.VehicleSearchCriteria;
                VehicleMasterDataCounter masterDataCounter = filter.VehicleMasterDataCounter;

                if (criteria != null && masterDataCounter != null)
                {
                    HttpContext.Current.Session["Vehicle-Data"] = criteria;
                    List<VehicleData> temp = new List<VehicleData>();
                    List<MasterData> vehicleFeaturesList = new List<MasterData>();
                    int totalRecord = 0;
                    string totalPage = string.Empty;
                    List<string> searchCleanedUpString = VehicleHelper.ReplaceSearchStringWithCorrectData(criteria.Vin, DealerID);
                    List<string> searchFilterType = VehicleHelper.GuessDataFilterType(searchCleanedUpString, masterDataCounter, DealerID);
                    using (ServicePageDbContext databaseContext = new ServicePageDbContext())
                    {
                        // Get Queryable vehicles list
                        var vehicles = VehicleHelper.GetVehicles(DealerID, criteria, searchFilterType, searchCleanedUpString, databaseContext).ToList();

                        //var vehicleDatas = vehicles as VehicleData[] ?? vehicles.ToArray();
                        vehicleFeaturesList = MasterDataHelper.GetVehicleFeaturMasters(vehicles);
                        if (vehicles.Any())
                        {
                            totalRecord = vehicles.Count();

                            if (totalRecord <= criteria.PageSize)
                            {
                                criteria.PageNumber = 1;
                            }

                            temp = vehicles.Skip(criteria.Skip).Take(criteria.PageSize).ToList();

                            if (temp.Any())
                            {
                                var TopPriorityPrograms = databaseContext.TopPriorityPrograms.Where(x => x.Active).ToList();
                                //Getting the Programs after filtering to make the query faster.
                                await temp.ParallelForEachAsync(async vehicle =>
                                {
                                    using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["AgileDealer"].ConnectionString))
                                    {
                                        try
                                        {
                                            if (vehicle.MSRPOverride > 0)
                                                vehicle.DealerSaving = (vehicle.MSRPOverride > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRPOverride > vehicle.OriginalSellingPrice) ? (vehicle.MSRPOverride - vehicle.OriginalSellingPrice) : 0;
                                            else
                                                vehicle.DealerSaving = (vehicle.MSRP > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRP > vehicle.OriginalSellingPrice) ? (vehicle.MSRP - vehicle.OriginalSellingPrice) : 0;

                                            vehicle.IsDisplayMsrp = vehicle.MSRP != 0 && vehicle.MSRP >= vehicle.OriginalSellingPrice;
                                            if (!vehicle.HideIncentives)
                                            {
                                                var offers = (await dbConnection.QueryAsync<ProgramListData>("GetOffersForVehicleTest",
                                                    new { agileVehicleId = vehicle.AgileVehicleId, vehicleVin = vehicle.Vin, modelTranslatedId = vehicle.ModelTranslatedId },
                                                    commandType: System.Data.CommandType.StoredProcedure)).ToList();
                                                vehicle.NormalOffers = offers.Where(offer =>
                                                                                        (offer.DisplaySection.HasValue && offer.DisplaySection.Value == (int)Enum.DisplaySection.Manufacturer && offer.IdBit)
                                                                                        || offer.Conditional.Equals("false", StringComparison.OrdinalIgnoreCase)
                                                                                    ).ToList();

                                                vehicle.ConditionalOffers = offers.Where(offer =>
                                                                                            (offer.DisplaySection.HasValue && offer.DisplaySection.Value == (int)Enum.DisplaySection.Conditional && offer.IdBit)
                                                                                            || (offer.Conditional.Equals("true", StringComparison.OrdinalIgnoreCase) && (!string.IsNullOrEmpty(offer.IsFmcc)) && !string.IsNullOrEmpty(offer.ProgramId))
                                                                                        ).ToList();

                                                vehicle.AprOffers = (await dbConnection.QueryAsync<ProgramListData>("GetAprOffersForVehicle",
                                                    new { agileVehicleId = vehicle.AgileVehicleId, vehicleVin = vehicle.Vin, modelTranslatedId = vehicle.ModelTranslatedId },
                                                    commandType: System.Data.CommandType.StoredProcedure)).ToList();

                                                var translatedIncentive = (await dbConnection.QueryAsync<IncentiveTranslatedModelDapper>("GetTranslatedIncentive",
                                                    new
                                                    {
                                                        vehicleVin = vehicle.Vin,
                                                        modelTranslatedId = vehicle.ModelTranslatedId,
                                                        chromeStyleId = vehicle.ChromeStyleId,
                                                        chromeStyleIdYear = vehicle.ChromeStyleIdYear,
                                                        engineId = vehicle.EngineId.ToString(),
                                                        trimId = vehicle.TrimId.ToString(),
                                                        year = vehicle.Year.ToString(),
                                                        bodyStyleId = vehicle.BodyStyleId.ToString()
                                                    },
                                                    commandType: System.Data.CommandType.StoredProcedure)).ToList();

                                                var ManufactrerCashTradeOffers = translatedIncentive.Where(x => x.DisplaySection == (int)Enum.DisplaySection.Manufacturer).ToList();


                                                //Add the missing programs to conditional offers.

                                                var IncentiveTransLatedDetailwithHideOtherIncentiveList = ManufactrerCashTradeOffers.Where(x => x.HideOtherIncentives && x.IsActive);
                                                bool IsHideOtherIncentives = false;

                                                var IncentiveTransLatedDetailwithHideDiscounts = translatedIncentive.Where(x => x.HideDiscounts && x.IsActive);
                                                if (IncentiveTransLatedDetailwithHideDiscounts != null)
                                                {
                                                    var parallelLoopResult = Parallel.ForEach(IncentiveTransLatedDetailwithHideDiscounts, (item, state) =>
                                                    {
                                                        if (!item.IsExcluded)
                                                        {
                                                            state.Break();
                                                        }
                                                    });
                                                    if (!parallelLoopResult.IsCompleted)
                                                    {
                                                        vehicle.DealerSaving = 0;
                                                        if (vehicle.IsDisplayMsrp)
                                                        {
                                                            vehicle.OriginalSellingPrice = Convert.ToDecimal(vehicle.MSRPOverride) > 0 ? Convert.ToDecimal(vehicle.MSRPOverride) : Convert.ToDecimal(vehicle.MSRP);
                                                        }
                                                    }
                                                }

                                                var IncentiveTransLatedDetailwithHideOtherIncentive = new IncentiveTranslatedModelDapper();

                                                //if Hide Other Incentives is setup then don't add any other programs if that vehicle falls under that category..
                                                if (IncentiveTransLatedDetailwithHideOtherIncentiveList != null)
                                                {
                                                    object lockObject = new object();
                                                    Parallel.ForEach(IncentiveTransLatedDetailwithHideOtherIncentiveList, (HideOtherIncentiveItem, state) =>
                                                    {
                                                        if (!HideOtherIncentiveItem.IsExcluded)
                                                        {
                                                            lock (lockObject)
                                                            {
                                                                IsHideOtherIncentives = true;
                                                                IncentiveTransLatedDetailwithHideOtherIncentive = HideOtherIncentiveItem;
                                                                state.Break();
                                                            }
                                                        }
                                                    });
                                                }
                                                vehicle.HideOtherIncentives = IsHideOtherIncentives;
                                                if (IsHideOtherIncentives && vehicle.IsDisplayMsrp)
                                                {
                                                    var NormalProgramListToAdd = new ProgramListData();

                                                    decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100));
                                                    decimal Amount = IncentiveTransLatedDetailwithHideOtherIncentive.DiscountType.Equals(1) ? Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) : Convert.ToInt32(PercentageAmount);

                                                    NormalProgramListToAdd = new ProgramListData
                                                    {
                                                        ProgramId = IncentiveTransLatedDetailwithHideOtherIncentive.IncentiveTranslatedId.ToString(),
                                                        Name = IncentiveTransLatedDetailwithHideOtherIncentive.ProgramName,
                                                        Amount = Convert.ToString(Amount),
                                                        Type = IncentiveTransLatedDetailwithHideOtherIncentive.Type,
                                                        Disclaimer = IncentiveTransLatedDetailwithHideOtherIncentive.Disclaimer,
                                                        IsCashCoupon = true,
                                                        IsCustomDiscount = false, // IncentiveTransLatedDetailwithHideOtherIncentive.IsCustomDiscount

                                                    };
                                                    if (vehicle.IsNew)
                                                    {
                                                        vehicle.Rebate = 1;
                                                    }
                                                    vehicle.NormalOffers = new List<ProgramListData> { NormalProgramListToAdd };
                                                    vehicle.ConditionalOffers = new List<ProgramListData>();
                                                }
                                                else
                                                {
                                                    var NormalProgramListToAdd = new ConcurrentBag<ProgramListData>();
                                                    Parallel.ForEach(ManufactrerCashTradeOffers, item =>
                                                    {
                                                        if (item.IsCustomDiscount)
                                                        {
                                                            // TODO: this part of code has duplicated elsewhere as well rewrite the logic to have less duplicacy
                                                            if (item.DiscountType == 2)
                                                            {
                                                                decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100));
                                                                decimal Amount = Convert.ToInt32(PercentageAmount);

                                                                if (!item.IsExcluded)
                                                                {
                                                                    NormalProgramListToAdd.Add(new ProgramListData
                                                                    {
                                                                        ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                        Name = item.ProgramName,
                                                                        Amount = Convert.ToString(Amount),
                                                                        Type = item.Type,
                                                                        Disclaimer = item.Disclaimer,
                                                                        IsCashCoupon = true,
                                                                        IsCustomDiscount = item.IsCustomDiscount

                                                                    });
                                                                    if (vehicle.IsNew)
                                                                    {
                                                                        vehicle.Rebate = 1;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (vehicle.DealerSaving > 0)
                                                                {
                                                                    if (Convert.ToDecimal(item.Amount) >= vehicle.DealerSaving)
                                                                    {
                                                                        if (!item.IsExcluded)
                                                                        {
                                                                            NormalProgramListToAdd.Add(new ProgramListData
                                                                            {
                                                                                ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                                Name = item.ProgramName,
                                                                                Amount = vehicle.DealerSaving.ToString(),
                                                                                Type = item.Type,
                                                                                Disclaimer = item.Disclaimer,
                                                                                IsCashCoupon = true,
                                                                                IsCustomDiscount = item.IsCustomDiscount

                                                                            });
                                                                            if (vehicle.IsNew)
                                                                            {
                                                                                vehicle.Rebate = 1;
                                                                            }
                                                                            vehicle.DealerSaving = 0;
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if (!item.IsExcluded)
                                                                        {

                                                                            NormalProgramListToAdd.Add(new ProgramListData
                                                                            {
                                                                                ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                                Name = item.ProgramName,
                                                                                Amount = item.Amount,
                                                                                Type = item.Type,
                                                                                Disclaimer = item.Disclaimer,
                                                                                IsCashCoupon = true,
                                                                                IsCustomDiscount = item.IsCustomDiscount

                                                                            });
                                                                            if (vehicle.IsNew)
                                                                            {
                                                                                vehicle.Rebate = 1;
                                                                            }
                                                                            vehicle.DealerSaving = vehicle.DealerSaving - Convert.ToDecimal(item.Amount);
                                                                        }

                                                                    }
                                                                }
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (item.DiscountType == 2)
                                                            {
                                                                decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100));
                                                                decimal Amount = Convert.ToInt32(PercentageAmount);

                                                                if (!item.IsExcluded)
                                                                {
                                                                    if (vehicle.IsNew)
                                                                    {
                                                                        NormalProgramListToAdd.Add(new ProgramListData
                                                                        {
                                                                            ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                            Name = item.ProgramName,
                                                                            Amount = Convert.ToString(Amount),
                                                                            Type = item.Type,
                                                                            Disclaimer = item.Disclaimer,
                                                                            IsCashCoupon = true,
                                                                            IsCustomDiscount = item.IsCustomDiscount

                                                                        });

                                                                        vehicle.Rebate = 1;
                                                                    }
                                                                }
                                                            }
                                                            else if (!item.IsExcluded)
                                                            {
                                                                if (vehicle.IsNew)
                                                                {
                                                                    NormalProgramListToAdd.Add(new ProgramListData
                                                                    {
                                                                        ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                        Name = item.ProgramName,
                                                                        Amount = item.Amount,
                                                                        Type = item.Type,
                                                                        Disclaimer = item.Disclaimer,
                                                                        IsCashCoupon = true,
                                                                        IsCustomDiscount = item.IsCustomDiscount

                                                                    });

                                                                    vehicle.Rebate = 1;
                                                                }
                                                            }
                                                        }
                                                    });
                                                    vehicle.NormalOffers.AddRange(NormalProgramListToAdd);

                                                    //Get All the Programs of Vehicle
                                                    var ProgramListToAdd = new ConcurrentBag<ProgramListData>();
                                                    var VehicleAllPrograms = (await dbConnection.QueryAsync<ProgramListData>("GetConditionalVehiclePrograms", new { agileVehicleId = vehicle.AgileVehicleId }, commandType: System.Data.CommandType.StoredProcedure)).ToList();
                                                    //Get the missing TopPriorityPrograms which didn't match for ConditionalOffers
                                                    Parallel.ForEach(TopPriorityPrograms, topProrityProgramItem =>
                                                    {
                                                        if (vehicle.ConditionalOffers.Count > 0)
                                                        {
                                                            foreach (var conditionalProgram in vehicle.ConditionalOffers)
                                                            {
                                                                if (!conditionalProgram.Name.Equals(topProrityProgramItem.Name, StringComparison.OrdinalIgnoreCase) && !conditionalProgram.Name.Equals(topProrityProgramItem.Description, StringComparison.OrdinalIgnoreCase) && !ProgramListToAdd.Any(x => x.Name == topProrityProgramItem.Name))
                                                                {
                                                                    var MissingProgram = VehicleAllPrograms.FirstOrDefault(x => x.Name.Equals(topProrityProgramItem.Name));
                                                                    if (MissingProgram != null)
                                                                    {
                                                                        ProgramListToAdd.Add(MissingProgram);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var MissingProgram = VehicleAllPrograms.FirstOrDefault(x => x.Name.Equals(topProrityProgramItem.Name));
                                                            if (MissingProgram != null)
                                                            {
                                                                ProgramListToAdd.Add(MissingProgram);
                                                            }
                                                        }
                                                    });

                                                    VehicleAllPrograms = null;

                                                    //Add the missing programs to conditional offers.
                                                    var ConditionalOffers = vehicle.ConditionalOffers.ToList();

                                                    vehicle.NormalOffers.RemoveAll(a => ConditionalOffers.Exists(w => w.Id == a.Id));

                                                    var ConditionalCashTradeOffers = translatedIncentive.Where(x => x.DisplaySection == (int)Enum.DisplaySection.Conditional).ToList();

                                                    var conditionalCashTradeOffers = ConditionalCashTradeOffers.Where(x => x.HideOtherIncentives && x.IsActive);
                                                    IsHideOtherIncentives = false;

                                                    if (conditionalCashTradeOffers != null)
                                                    {
                                                        object lockObject = new object();
                                                        Parallel.ForEach(conditionalCashTradeOffers, (HideOtherIncentiveItem, state) =>
                                                        {
                                                            if (!HideOtherIncentiveItem.IsExcluded)
                                                            {
                                                                lock (lockObject)
                                                                {
                                                                    IsHideOtherIncentives = true;
                                                                    IncentiveTransLatedDetailwithHideOtherIncentive = HideOtherIncentiveItem;
                                                                    state.Break();
                                                                }
                                                            }
                                                        });
                                                    }
                                                    vehicle.HideOtherIncentives = IsHideOtherIncentives;
                                                    if (IsHideOtherIncentives && vehicle.IsDisplayMsrp)
                                                    {
                                                        ConditionalOffers = new List<ProgramListData>();

                                                        decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100));
                                                        decimal Amount = IncentiveTransLatedDetailwithHideOtherIncentive.DiscountType.Equals(1) ? Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) : Convert.ToInt32(PercentageAmount);
                                                        if (vehicle.IsNew)
                                                        {
                                                            ProgramListToAdd.Add(new ProgramListData
                                                            {
                                                                ProgramId = IncentiveTransLatedDetailwithHideOtherIncentive.IncentiveTranslatedId.ToString(),
                                                                Name = IncentiveTransLatedDetailwithHideOtherIncentive.ProgramName,
                                                                Amount = Convert.ToString(Amount),
                                                                Type = IncentiveTransLatedDetailwithHideOtherIncentive.Type,
                                                                Disclaimer = IncentiveTransLatedDetailwithHideOtherIncentive.Disclaimer,
                                                                IsCashCoupon = true,
                                                                IsCustomDiscount = false, // IncentiveTransLatedDetailwithHideOtherIncentive.IsCustomDiscount
                                                            });

                                                            vehicle.Rebate = 1;
                                                        }

                                                        ConditionalOffers.AddRange(ProgramListToAdd);

                                                        vehicle.ConditionalOffers = ConditionalOffers;
                                                    }
                                                    else
                                                    {
                                                        Parallel.ForEach(ConditionalCashTradeOffers, item =>
                                                        {
                                                            if (!item.IsExcluded)
                                                            {
                                                                if (item.DiscountType == 2)
                                                                {
                                                                    decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vehicle.MSRP) * (Convert.ToDecimal(item.Amount) / 100));
                                                                    decimal Amount = Convert.ToInt32(PercentageAmount);


                                                                    if (vehicle.IsNew)
                                                                    {
                                                                        ProgramListToAdd.Add(new ProgramListData
                                                                        {
                                                                            ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                            Name = item.ProgramName,
                                                                            Amount = Convert.ToString(Amount),
                                                                            Type = item.Type,
                                                                            Disclaimer = item.Disclaimer,
                                                                            IsCashCoupon = true,
                                                                            IsCustomDiscount = item.IsCustomDiscount

                                                                        });

                                                                        vehicle.Rebate = 1;
                                                                    }

                                                                }
                                                                else if (!item.IsExcluded)
                                                                {
                                                                    if (vehicle.IsNew)
                                                                    {
                                                                        ProgramListToAdd.Add(new ProgramListData
                                                                        {
                                                                            ProgramId = item.IncentiveTranslatedId.ToString(),
                                                                            Name = item.ProgramName,
                                                                            Amount = item.Amount,
                                                                            Type = item.Type,
                                                                            Disclaimer = item.Disclaimer,
                                                                            IsCashCoupon = true,
                                                                            IsCustomDiscount = item.IsCustomDiscount

                                                                        });

                                                                        vehicle.Rebate = 1;
                                                                    }
                                                                }
                                                            }
                                                        });

                                                        ConditionalOffers.AddRange(ProgramListToAdd);
                                                        vehicle.ConditionalOffers = ConditionalOffers;

                                                        if (vehicle.EmployeePrice > 0)
                                                        {
                                                            vehicle.OriginalSellingPrice = Convert.ToDecimal(vehicle.EmployeePrice);
                                                            vehicle.DealerSaving = 0;
                                                        }
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                vehicle.NormalOffers = new List<ProgramListData>();
                                                vehicle.AprOffers = new List<ProgramListData>();
                                                vehicle.ConditionalOffers = new List<ProgramListData>();
                                            }

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                    }
                                });
                            }

                            totalPage = ((int)Math.Ceiling((double)totalRecord / criteria.PageSize)).ToString();
                        }

                    }

                    switch (criteria.SortItem)
                    {
                        case (int)SearchControlEnum.PriceLowToHigh:
                            {
                                temp = temp.OrderBy(x => x.NormalSellingPrice).ToList();
                                break;
                            }
                        case (int)SearchControlEnum.PriceHighToLow:
                            {
                                temp = temp.OrderByDescending(x => x.NormalSellingPrice).ToList();
                                break;
                            }

                    }

                    var result = new
                    {
                        vehicleList = temp,
                        isSetPagingVisible = criteria.IsViewAll ? "false" : "true",
                        totalRecord = totalRecord,
                        totalPage = totalPage,
                        pageNumber = criteria.PageNumber,
                        masterDataCounter = masterDataCounter,
                        criteria = criteria,
                        isSuccess = true,
                        vehicleFeatures = vehicleFeaturesList
                    };

                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            }
        }

        private void GetCounterForVehicles(HttpContext context, string strJson)
        {
            VehicleFilter filter = Deserialize<VehicleFilter>(strJson);
            if (filter != null)
            {
                VehicleSearchCriteria criteria = filter.VehicleSearchCriteria;
                VehicleMasterDataCounter masterDataCounter = filter.VehicleMasterDataCounter;

                if (criteria != null && masterDataCounter != null)
                {
                    List<VehicleData> temp = new List<VehicleData>();
                    List<string> searchCleanedUpString = VehicleHelper.ReplaceSearchStringWithCorrectData(criteria.Vin, DealerID);
                    List<string> searchFilterType = VehicleHelper.GuessDataFilterType(searchCleanedUpString,
                        masterDataCounter, DealerID);
                    using (ServicePageDbContext db = new ServicePageDbContext())
                    {
                        IEnumerable<VehicleData> vehicles = VehicleHelper.GetVehicleDataForCounter(DealerID, criteria, searchFilterType, searchCleanedUpString, db);
                        if (vehicles != null)
                        {
                            temp = vehicles.ToList();
                            SetMasterDataCounter(vehicles, masterDataCounter);
                        }
                    }
                    var result = new
                    {
                        vehicleList = temp,
                        pageNumber = criteria.PageNumber,
                        masterDataCounter = masterDataCounter,
                        criteria = criteria,
                        isSuccess = true,
                    };

                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            }
        }

        private void SetMasterDataCounter(IEnumerable<VehicleData> vsData, VehicleMasterDataCounter masterDataCounter)
        {
            if (vsData != null && masterDataCounter != null)
            {
                var vehicleDatas = vsData as VehicleData[] ?? vsData.ToArray();
                masterDataCounter.IsNew = vehicleDatas.Count(x => x.IsNew);
                masterDataCounter.IsUsed = vehicleDatas.Count(x => !x.IsCert && !x.IsNew);
                masterDataCounter.IsCertified = vehicleDatas.Count(x => x.IsCert);
                masterDataCounter.IsLift = vehicleDatas.Count(x => x.IsLift);
                masterDataCounter.IsUpfitted = vehicleDatas.Count(x => x.IsUpfitted);
                for (int i = 0; i < masterDataCounter.Models.Count; i++)
                {
                    masterDataCounter.Models[i].Count = vehicleDatas.Count(x => x.ModelTranslatedId == masterDataCounter.Models[i].Id);
                    if (masterDataCounter.Models[i].SubList != null)
                    {
                        for (int j = 0; j < masterDataCounter.Models[i].SubList.Count; j++)
                        {
                            masterDataCounter.Models[i].SubList[j].Count = vehicleDatas.Count(x => x.ModelTranslatedId == masterDataCounter.Models[i].Id && x.TrimId == masterDataCounter.Models[i].SubList[j].SubListId);
                        }
                    }
                }

                for (int i = 0; i < masterDataCounter.BodyStyles.Count; i++)
                {
                    masterDataCounter.BodyStyles[i].Count = vehicleDatas.Count(x => x.StandardBodyStyleId == masterDataCounter.BodyStyles[i].Id);
                    if (masterDataCounter.BodyStyles[i].SubList != null)
                    {
                        for (int j = 0; j < masterDataCounter.BodyStyles[i].SubList.Count; j++)
                        {
                            masterDataCounter.BodyStyles[i].SubList[j].Count = vehicleDatas.Count(x => x.StandardBodyStyleId == masterDataCounter.BodyStyles[i].Id && x.BodyStyleTypeId == masterDataCounter.BodyStyles[i].SubList[j].SubListId);
                        }
                    }
                }

                for (int i = 0; i < masterDataCounter.Drives.Count; i++)
                {
                    masterDataCounter.Drives[i].Count = vehicleDatas.Count(x => x.DriveId == masterDataCounter.Drives[i].Id);
                }

                for (int i = 0; i < masterDataCounter.Engines.Count; i++)
                {
                    masterDataCounter.Engines[i].Count = vehicleDatas.Count(x => x.EngineTypeTranslatedId == masterDataCounter.Engines[i].Id);
                    if (masterDataCounter.Engines[i].SubList != null)
                    {
                        for (int j = 0; j < masterDataCounter.Engines[i].SubList.Count; j++)
                        {
                            masterDataCounter.Engines[i].SubList[j].Count =
                                vehicleDatas.Count(
                                    x =>
                                        x.EngineTypeTranslatedId == masterDataCounter.Engines[i].Id &&
                                        x.EngineId == masterDataCounter.Engines[i].SubList[j].SubListId);
                        }
                    }
                }

                for (int i = 0; i < masterDataCounter.ExteriorColors.Count; i++)
                {
                    masterDataCounter.ExteriorColors[i].Count = vehicleDatas.Count(x => x.ExteriorColorTranslaredId == masterDataCounter.ExteriorColors[i].Id);
                }
                for (int i = 0; i < masterDataCounter.Transmissions.Count; i++)
                {
                    masterDataCounter.Transmissions[i].Count = vehicleDatas.Count(x => x.Trans == masterDataCounter.Transmissions[i].Name);
                }
                for (int i = 0; i < masterDataCounter.FuelTypes.Count; i++)
                {
                    masterDataCounter.FuelTypes[i].Count = vehicleDatas.Count(x => x.FuelTypeId == masterDataCounter.FuelTypes[i].Id);
                }

                for (int i = 0; i < masterDataCounter.Makes.Count; i++)
                {
                    masterDataCounter.Makes[i].Count = vehicleDatas.Count(x => x.MakeTranslatedId == masterDataCounter.Makes[i].Id);
                }

                for (int i = 0; i < masterDataCounter.Mileages.Count; i++)
                {
                    masterDataCounter.Mileages[i].Count = vehicleDatas.Count(x => (masterDataCounter.Mileages[i].From == 0 ? masterDataCounter.Mileages[i].From <= x.Mileage : masterDataCounter.Mileages[i].From < x.Mileage) && masterDataCounter.Mileages[i].To >= x.Mileage);
                }

                for (int i = 0; i < masterDataCounter.Prices.Count; i++)
                {
                    masterDataCounter.Prices[i].Count = vehicleDatas.Count(x => (masterDataCounter.Prices[i].From == 0 ? masterDataCounter.Prices[i].From <= x.SellingPrice : masterDataCounter.Prices[i].From < x.SellingPrice) && masterDataCounter.Prices[i].To >= x.SellingPrice);
                }

                for (int i = 0; i < masterDataCounter.Years.Count; i++)
                {
                    masterDataCounter.Years[i].Count = vehicleDatas.Count(x => x.Year == masterDataCounter.Years[i].Id);
                }
            }
        }

        public class VehicleFilter
        {
            public VehicleSearchCriteria VehicleSearchCriteria { get; set; }
            public VehicleMasterDataCounter VehicleMasterDataCounter { get; set; }
        }
    }
}