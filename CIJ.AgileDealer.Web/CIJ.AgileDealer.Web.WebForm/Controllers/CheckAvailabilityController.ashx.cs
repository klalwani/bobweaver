﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for CheckAvailabilityController
    /// </summary>
    public class CheckAvailabilityController : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetCheckAvailabilityDetail":
                        GetCheckAvailabilityDetail(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("CheckAvailabilityController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        private void GetCheckAvailabilityDetail(HttpContext context, string strJson)
        {
            int data = Deserialize<int>(strJson);
            VehicleFeedBackModel vehicleFeedBack = CheckAvailabilityHelper.GetVehicleFeedBackById(data,DealerID);
            if (vehicleFeedBack != null)
            {
                var result = new
                {
                    aaData = vehicleFeedBack,
                    isSuccess = true,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
    }
}