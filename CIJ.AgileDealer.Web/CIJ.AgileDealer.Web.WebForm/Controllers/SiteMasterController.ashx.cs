﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Parts;
using CIJ.AgileDealer.Web.Base.Helpers;
using CIJ.AgileDealer.Web.Base.Repositories;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for SiteMasterController
    /// </summary>
    public class SiteMasterController : BaseController, System.Web.SessionState.IRequiresSessionState
    {
        private readonly string _localUploadFolder = ConfigurationManager.AppSettings["LocalUploadFolder"];
        private readonly string _localUploadFolderName = ConfigurationManager.AppSettings["LocalUploadFolderName"];
        private readonly string _blobContainer = ConfigurationManager.AppSettings["BlobContainer"];
        private readonly string _storageConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];

        private VehicleRepository _vehicleRepository;
        private VehicleRepository VehicleRepository
        {
            get
            {
                if (_vehicleRepository == null)
                    _vehicleRepository = new VehicleRepository();
                return _vehicleRepository;
            }
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            string method = string.Empty;
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "UpdateGaClientId":
                        UpdateGaClientId(context, strJson);
                        break;
                    case "SetVcpCurrentTab":
                        SetVcpCurrentTab(context, strJson);
                        break;
                    case "SetCurrentSession":
                        SetCurrentSession(context, strJson);
                        break;
                    case "SubmitContactUs":
                        await SubmitContactUs(context, strJson);
                        break;
                    case "SubmitPartRequest":
                        await SubmitPartRequest(context, strJson);
                        break;
                    case "UploadStaffImage":
                        UploadImage(context, strJson, method);
                        break;
                    case "UploadBlogImage":
                        UploadImage(context, strJson, method);
                        break;
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("SiteMasterController.ProcessRequestAsync() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public void UploadImage(HttpContext context, string strJson, string method)
        {
            string uploadfolder = GetFolderNameForUpload(method);
            string imageFolder = string.Empty;
            string imageNameToReturn = string.Empty;
            string azureLink = string.Empty;
            bool isSuccess = false;

            string imageName = SaveImageIntoLocalUploadFolder(context);
            if (!string.IsNullOrEmpty(imageName))
            {
                isSuccess = true;
                imageNameToReturn = CreateFileNameToReturn(imageName);
                azureLink = Path.Combine(ImageServer, uploadfolder, imageName);
            }
            else
            {
                isSuccess = false;
                imageNameToReturn = string.Empty;
                azureLink = string.Empty;
            }
            var result = new
            {
                isSuccess = isSuccess,
                imageUrl = imageNameToReturn,
                imageName = imageName,
                azureLink = azureLink
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private string CreateFileNameToReturn(string fileName)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(fileName))
            {
                string name = Path.GetFileNameWithoutExtension(fileName);
                result = Path.Combine(_localUploadFolderName, name, fileName);
            }
            return result;
        }

        private void UploadIntoAzure(string uploadFolderName, string dirFullPath)
        {
            VehicleRepository.UploadAgileVehicleImage(_storageConnectionString, uploadFolderName, dirFullPath, _blobContainer);
        }

        private string SaveImageIntoLocalUploadFolder(HttpContext context)
        {
            string imageName = string.Empty;
            string imageNameToReturn = string.Empty;
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                string fileName = file.FileName;
                string fileExtension = file.ContentType;
                try
                {
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileExtension = Path.GetExtension(fileName);
                        string newImageName = Guid.NewGuid().ToString();
                        imageName = newImageName + fileExtension;

                        string pathToSaveThisImage = Path.Combine(GetLocalUploadFolder(newImageName), imageName);
                        file.SaveAs(pathToSaveThisImage);
                    }
                    else
                        imageName = string.Empty;

                }
                catch (Exception ex)
                {
                    imageName = string.Empty;
                }
            }

            return imageName;
        }

        private string GetLocalUploadFolder(string customName = "")
        {
            string dirFullPath = HttpContext.Current.Server.MapPath(_localUploadFolder);
            if (!string.IsNullOrEmpty(customName))
                dirFullPath = Path.Combine(dirFullPath, customName);

            if (!Directory.Exists(dirFullPath))
            {
                Directory.CreateDirectory(dirFullPath);
            }
            return dirFullPath;
        }

        public void SetVcpCurrentTab(HttpContext context, string strJson)
        {
            SessionVariables session = Deserialize<SessionVariables>(strJson);
            if (!string.IsNullOrEmpty(session.SessionName) && !string.IsNullOrEmpty(session.SessionValue))
            {
                Global.VcpCurrentTab = session.SessionValue;
            }
        }

        public async Task SubmitContactUs(HttpContext context, string strJson)
        {
            ContactUsInfo info = Deserialize<ContactUsInfo>(strJson);
            CustomerQuestion customerQuestion;
            string customerQuestionJson = string.Empty;
            using (var db = new ServicePageDbContext())
            {
                int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(context.Session, context.Request);
                customerQuestion = CreateCustomerQuestion(buyerSessionId, info);
                db.CustomerQuestions.Add(customerQuestion);
                db.SaveChanges();
                customerQuestionJson = new JavaScriptSerializer().Serialize(customerQuestion);
            }

            var result = new
            {
                isSuccess = true,
            };

            await Task.Run(() =>
            {
                EmailController.SendEmailForCustomerQuestion(customerQuestionJson, DealerID);
                EmailController.SendEmailConfirmation(customerQuestion.Email);
            }).ContinueWith(t => {
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
            );
        }

        public async Task SubmitPartRequest(HttpContext context, string strJson)
        {
            PartRequest info = Deserialize<PartRequest>(strJson);
            PartRequest partRequest = PartHelper.CreatePartRequest(info,DealerID);
            PartHelper.SavePartRequest(partRequest);

            var result = new
            {
                isSuccess = true,
            };

            await Task.Run(() =>
            {
                EmailController.SendEmailForPartRequest(strJson, DealerID);
                EmailController.SendEmailConfirmation(partRequest.Email);
            }).ContinueWith(t => {
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
            );
        }

        private CustomerQuestion CreateCustomerQuestion(int buyerSessionId, ContactUsInfo info)
        {
            CustomerQuestion customerQuestion = new CustomerQuestion
            {
                FirstName = info.FirstName,
                LastName = info.LastName,
                Email = info.Email,
                PhoneNumber = info.PhoneNumber,
                PostalCode = info.PostalCode,
                Questions = info.Question,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now,
                DealerID = DealerID,
                IsCompleted = true,
                BuyerSessionId = buyerSessionId
            };
            return customerQuestion;
        }

        public void SetCurrentSession(HttpContext context, string strJson)
        {
            SessionVariables session = Deserialize<SessionVariables>(strJson);
            if (!string.IsNullOrEmpty(session.SessionName) && !string.IsNullOrEmpty(session.SessionValue))
            {
                HttpContext.Current.Session[session.SessionName] = session.SessionValue;
                var result = new
                {
                    isSuccess = false,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public void UpdateGaClientId(HttpContext context, string strJson)
        {
            GoogleClient client = Deserialize<GoogleClient>(strJson);
            if (client != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    BuyerSession buyerSession;
                    if (Global.CurrentBuyerSession != null)
                        buyerSession = Global.CurrentBuyerSession;
                    else
                        buyerSession = db.BuyerSessions.FirstOrDefault(x => x.SessionId == Global.CurrentSession);

                    if (buyerSession != null)
                    {
                        buyerSession.GoogleClientId = client.ClientId;
                        db.SaveChanges();
                        var result = new
                        {
                            isSuccess = true,
                        };
                        context.Response.Write(JsonConvert.SerializeObject(result));
                    }
                }
            }
        }


        private string GetFolderNameForUpload(string method)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(method))
            {
                UploadInformation tempUploadInformation = MasterDataHelper.GetUploadInformation(method, DealerID);
                if (tempUploadInformation != null)
                {
                    result = tempUploadInformation.FolderName;
                }
            }
            return result;
        }
    }

    public class GoogleClient
    {
        public string ClientId { get; set; }
    }

    public class SessionVariables
    {
        public string SessionName { get; set; }
        public string SessionValue { get; set; }
    }

    public class ContactUsInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string PostalCode { get; set; }
        public string Question { get; set; }
    }

}