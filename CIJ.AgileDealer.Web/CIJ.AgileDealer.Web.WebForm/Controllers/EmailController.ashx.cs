﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Parts;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Enums;
using Microsoft.Ajax.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for EmailController
    /// </summary>
    public class EmailController : BaseController 
    {
        private static List<NotificationSetting> _notificationSettings;
        public static List<NotificationSetting> NotificationSettings
        {
            get
            {
                _notificationSettings = NotificationHelper.GetNotificationSettings(DealerInfoHelper.DealerID);

                return _notificationSettings;
            }
            set { _notificationSettings = value; }
        }

        private static List<NotificationSetting> _notificationSettingsFullList;
        public static List<NotificationSetting> NotificationSettingsFullList
        {
            get
            {
                _notificationSettingsFullList = NotificationHelper.GetNotificationSettings();

                return _notificationSettingsFullList;
            }
            set { _notificationSettingsFullList = value; }
        }

        private static Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerInfoHelper.DealerID);

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            string method = string.Empty;
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "SendEmailForCustomerQuestion":
                        SendEmailForCustomerQuestion(strJson, DealerInfoHelper.DealerID);
                        break;
                    case "SendEmailForScheduleService":
                        SendEmailForScheduleService(strJson, DealerInfoHelper.DealerID);
                        break;
                    case "SendEmailForForFinance":
                        SendEmailForForFinance(strJson, DealerInfoHelper.DealerID);
                        break;
                    case "SendEmailForCheckAvailability":
                        SendEmailForCheckAvailability(strJson, DealerInfoHelper.DealerID);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("EmailController.ProcessRequest() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public static async void SendEmailForCustomerQuestion(string strJson, int dealerId)
        {
            CustomerQuestion customerQuestion = Deserialize<CustomerQuestion>(strJson);
            if (customerQuestion != null)
            {
                NotificationSetting currentSetting = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.Contact.ToString() && x.DealerID == dealerId);
                NotificationSetting allLeadEmails = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == dealerId);
                string mailSubject = $"Website Contact Request from {customerQuestion.FirstName + " " + customerQuestion.LastName} ";
                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                {
                    string adfMailContent = EmailGenerator.CreateMailContentWithTemplateForCustomerQuestionWithAdfFormat(customerQuestion);
                    if (!string.IsNullOrEmpty(adfMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                        if (adfEmail != null)
                        {
                            EmailHelper.SendEmailAsync(adfEmail, string.Empty, false);
                        }
                    }
                }

                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.PlainTextEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.PlainTextEmail)))
                {
                    string plainTextMailContent = EmailGenerator.CreateMailContentWithTemplateForCustomerQuestion(customerQuestion);
                    if (!string.IsNullOrEmpty(plainTextMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.PlainTextEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.PlainTextEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email plainTextEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, plainTextMailContent);

                        if (plainTextEmail != null)
                        {
                            EmailHelper.SendEmailAsync(plainTextEmail, string.Empty, true);
                        }
                    }
                }
            }
        }

        public static async void SendEmailForScheduleService(string strJson, int dealerId)
        {
            ScheduleService scheduleService = Deserialize<ScheduleService>(strJson);
            if (scheduleService != null)
            {
                NotificationSetting currentSetting = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.Service.ToString() && x.DealerID == dealerId);
                NotificationSetting allLeadEmails = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == dealerId);
                string mailSubject = $"Website Service Request from {scheduleService.FirstName + " " + scheduleService.LastName}";
                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) ||
                    (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                {
                    string adfMailContent =
                        EmailGenerator.CreateMailContentWithTemplateForScheduleServiceWithAdfFormat(scheduleService);
                    if (!string.IsNullOrEmpty(adfMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                        if (adfEmail != null)
                        {
                            EmailHelper.SendEmailAsync(adfEmail, string.Empty, false);
                        }
                    }
                }

                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.PlainTextEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.PlainTextEmail)))
                {
                    string plainTextMailContent = EmailGenerator.CreateMailContentWithTemplateForScheduleService(scheduleService);
                    if (!string.IsNullOrEmpty(plainTextMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.PlainTextEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.PlainTextEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email plainTextEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, plainTextMailContent);

                        if (plainTextEmail != null)
                        {
                            EmailHelper.SendEmailAsync(plainTextEmail, string.Empty, true);
                        }
                    }
                }
            }
        }

        public static async void SendEmailForCheckAvailability(string strJson,int dealerId)
        {
            VehicleFeedBack vehicleFeedBack = Deserialize<VehicleFeedBack>(strJson);
            if (vehicleFeedBack != null)
            {
                NotificationSetting currentSetting = NotificationSettingsFullList.FirstOrDefault(x => x.LeadType == NotificationEnum.Availability.ToString() && x.DealerID == dealerId);
                NotificationSetting allLeadEmails = NotificationSettingsFullList.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == dealerId);
                string mailSubject = "Website Vehicle Lead from " + vehicleFeedBack.Name;

                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) ||
                    (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                {
                    string adfMailContent =
                        EmailGenerator.CreateMailContentWithTemplateForCheckAvailabilityWithAdfFormat(vehicleFeedBack,
                            vehicleFeedBack.HomeNetVehicle, Dealer);
                    if (!string.IsNullOrEmpty(adfMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                        if (adfEmail != null)
                        {
                            EmailHelper.SendEmailAsync(adfEmail, string.Empty, false);
                        }
                    }
                }
                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.PlainTextEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.PlainTextEmail)))
                {
                    string plainTextMailContent = EmailGenerator.CreateMailContentWithTemplateForCheckAvailability(vehicleFeedBack);
                    if (!string.IsNullOrEmpty(plainTextMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.PlainTextEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.PlainTextEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email plainTextEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, plainTextMailContent);

                        if (plainTextEmail != null)
                        {
                            EmailHelper.SendEmailAsync(plainTextEmail, string.Empty, true);
                        }
                    }
                }
            }
        }

        public static async void SendEmailForForFinance(string strJson, int dealerId)
        {
            ContactStructure applicant = Deserialize<ContactStructure>(strJson);
            if (applicant != null)
            {
                NotificationSetting currentSetting = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.Finance.ToString() && x.DealerID == dealerId);
                NotificationSetting allLeadEmails = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == dealerId);
                string mailSubject = $"Website Finance Application from {applicant.ApplicantInfo.FirstName} {applicant.ApplicantInfo.LastName}";
                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) ||
                    (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                {
                    string adfMailContent = EmailGenerator.CreateMailContentWithTemplateForFinanceAdf(applicant);
                    if (!string.IsNullOrEmpty(adfMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                        if (adfEmail != null)
                        {
                            EmailHelper.SendEmailAsync(adfEmail, string.Empty, false);
                        }
                    }
                }

                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.PlainTextEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.PlainTextEmail)))
                {
                    string plainTextMailContent = EmailGenerator.CreateMailContentWithTemplateForFinance(applicant);
                    if (!string.IsNullOrEmpty(plainTextMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.PlainTextEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.PlainTextEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email plainTextEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, plainTextMailContent);

                        if (plainTextEmail != null)
                        {
                            EmailHelper.SendEmailAsync(plainTextEmail, string.Empty, true);
                        }
                    }
                }
            }
        }


        public static async void SendEmailForPartRequest(string strJson, int dealerId)
        {
            PartRequest partRequest = Deserialize<PartRequest>(strJson);
            if (partRequest != null)
            {
                NotificationSetting currentSetting = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.PartRequest.ToString() && x.DealerID == dealerId);
                NotificationSetting allLeadEmails = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == dealerId);
                string mailSubject = $"Website Parts Request from {partRequest.FirstName + " " + partRequest.LastName}";
                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) ||
                    (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                {
                    string adfMailContent =
                        EmailGenerator.CreateMailContentWithTemplateForPartRequestWithAdfFormat(partRequest);
                    if (!string.IsNullOrEmpty(adfMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                        if (adfEmail != null)
                        {
                            EmailHelper.SendEmailAsync(adfEmail, string.Empty, false);
                        }
                    }
                }

                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.PlainTextEmail)) || (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.PlainTextEmail)))
                {
                    string plainTextMailContent = EmailGenerator.CreateMailContentWithTemplateForPart(partRequest);
                    if (!string.IsNullOrEmpty(plainTextMailContent))
                    {
                        string mailTo = currentSetting != null ? currentSetting.PlainTextEmail : string.Empty;
                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.PlainTextEmail : string.Empty;

                        if (!string.IsNullOrEmpty(allLeadEmail))
                        {
                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                            {
                                mailTo = $"{mailTo};{allLeadEmail}";
                            }
                        }

                        Email plainTextEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, plainTextMailContent);

                        if (plainTextEmail != null)
                        {
                            EmailHelper.SendEmailAsync(plainTextEmail, string.Empty, true);
                        }
                    }
                }
            }
        }


        public static async void SendEmailConfirmation(string mailTo)
        {
            if (!string.IsNullOrEmpty(mailTo))
            {
                await Task.Run(() => { EmailHelper.SendConfirmationEmail(mailTo, string.Empty); });
            }
        }

        private new static T Deserialize<T>(string context)
        {
            string jsonData = context;
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }
    }
}