﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for EditDealerInfo
    /// </summary>
    public class EditDealerInfo : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var method = context.Request.QueryString["method"];
            switch (method)
            {
                case "UpdateDealerInfo":
                    UpdateDealerInfo(context);
                    break;
                case "GetDealerInfo":
                    GetDealerInfo(context);
                    break;
            }
        }

        private DealerModel GetDealerInfo(ServicePageDbContext db)
        {
            var dealer = db.Dealers.Include("Locations").Include("Manufacture").FirstOrDefault();
            var location = dealer.Locations.FirstOrDefault();
            var dealerInfoModel = new DealerModel
            {
                DealerName = dealer != null ? dealer.Name : string.Empty,
                DealerCity = location != null ? location.City : string.Empty,
                DealerState = location != null ? location.State : string.Empty,
                ImageServerGeneric = dealer != null ? dealer.ImageServerGeneric : string.Empty,
                VcpImageStockServer = dealer != null ? dealer.VcpImageStockServer : string.Empty,
                ImageServer = dealer != null ? dealer.ImageServer : string.Empty
            };
            return dealerInfoModel;
        }

        private void GetDealerInfo(HttpContext context)
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    var dealerInfoModel = GetDealerInfo(db);
                    context.Response.Write(JsonConvert.SerializeObject(dealerInfoModel));
                }
            }
            catch(Exception ex)
            {
                throw ex.InnerException;
            }
        }

        private void UpdateDealerInfo(HttpContext context)
        {
            try
            {
                string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
                var dealerModel = Deserialize<Dealer>(strJson);
                DealerModel dealerInfoModel = new DealerModel();
                using (var db = new ServicePageDbContext())
                {
                    var dealer = db.Dealers.Include("Locations").Include("Manufacture").FirstOrDefault();
                    if (dealer != null)
                    {
                        var location = dealer.Locations.FirstOrDefault(); ;
                        dealer.Name = dealerModel.Name;
                        dealer.ImageServerGeneric = dealerModel.ImageServerGeneric;
                        dealer.VcpImageStockServer = dealerModel.VcpImageStockServer;
                        dealer.ImageServer = dealerModel.ImageServer;
                        db.Entry(dealer).State = System.Data.Entity.EntityState.Modified;
                        if (location != null)
                        {
                            location.State = dealerModel.Locations.FirstOrDefault().State;
                            location.City = dealerModel.Locations.FirstOrDefault().City;
                            db.Entry(location).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();
                        dealerInfoModel = GetDealerInfo(db);
                        DealerInfoHelper.ResetDealerInstance();
                    }
                    context.Response.Write(JsonConvert.SerializeObject(dealerInfoModel));
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
    }
}