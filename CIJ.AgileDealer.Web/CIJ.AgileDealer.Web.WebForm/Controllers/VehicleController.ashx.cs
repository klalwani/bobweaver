﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for VehicleController
    /// </summary>
    public class VehicleController : BaseController
    {
        private static int OffSetIncentiveTime = int.Parse(ConfigurationManager.AppSettings["OffSetIncentiveTime"]);
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                  //  case "GetVehiclePrograms":
                    //    GetVehiclePrograms(context, strJson);
                      //  break;
                    case "SetProgramState":
                        SetProgramState(context, strJson);
                        break;
                    case "GetProgramList":
                        GetProgramList(context, strJson);
                        break;
                    case "GetRebateForVehicleByAgileVehicleId":
                        GetRebateForVehicleByAgileVehicleId(context, strJson);
                        break;
                    case "GetRebateForVehicleByProgramIdAndAgileVehicleId":
                        GetRebateForVehicleByProgramIdAndAgileVehicleId(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("DashboardController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        private void SetProgramState(HttpContext context, string strJson)
        {
            VehicleProgramData data = Deserialize<VehicleProgramData>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.SetProgramState(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        private void GetRebateForVehicleByAgileVehicleId(HttpContext context, string strJson)
        {
            VehicleProgramData vehicle = Deserialize<VehicleProgramData>(strJson);
            List<VehicleProgramData> cashPrograms = new List<VehicleProgramData>();
            List<VehicleProgramData> conditionalPrograms = new List<VehicleProgramData>();
            List<VehicleProgramData> aprPrograms = new List<VehicleProgramData>();
            List<VehicleProgramData> singleProgramDatas = new List<VehicleProgramData>();
            List<Program> singleProgram = new List<Program>();
            if (vehicle != null)
            {
                if (vehicle.IsUpfitted)
                {
                    AgileVehicle vehicleDetails = VehicleHelper.GetAgileVehicleByVin(vehicle.Vin, DealerID);
                    var ProgramData = new VehicleProgramData()
                    {
                        Id = "0",
                        ProgramId = 0,
                        ProgramName = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Upfitting_Label,
                        Name = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Upfitting_Label,
                        Amount = "$" + Math.Abs(Convert.ToDouble(vehicleDetails.UpfittedCost)).ToString(CultureInfo.InvariantCulture),
                        Type = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Upfitting_Label,
                        Disclaimer = vehicleDetails.UpfittedDescription,
                        IsAprParent = false,
                        StartDate = null,
                        EndDate = null,
                        IsUpfitted = true

                    };
                    singleProgramDatas.Add(ProgramData);
                }
                else
                {

                    List<Program> programs = VehicleHelper.GetProgramOfSpecificVehicle(vehicle.AgileVehicleId, DealerID);
                    List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds().Where(x => !x.IsCashCoupon).ToList();
                    var IncentiveTranslatedsForSpecialProgramsForVin = VehicleHelper.GetIncentiveTranslatedsForSpecialProgramsForVin(vehicle.Vin, vehicle.ModelId);
                    var AgileVehicle = VehicleHelper.GetVehicleByVin(vehicle.Vin);

                    if (!vehicle.IsCashCoupon)
                    {
                        singleProgram = programs.Where(x => x.ProgramId == vehicle.ProgramId).ToList();
                        if (singleProgram != null)
                        {
                            foreach (var programItem in singleProgram)
                            {
                                if (programItem.Name.IndexOf("GM Employee Appreciation Certificate Program") > -1 || programItem.Name.IndexOf("Lease Loyalty") > -1)
                                {
                                    if (programItem.Name.IndexOf("Lease Loyalty") > -1)
                                    {
                                        singleProgram = programs.Where(x => x.Name.IndexOf("Lease Loyalty") > -1 && Convert.ToInt32(x.Amount) <= Convert.ToInt32(programItem.Amount)).OrderByDescending(x => Convert.ToInt32(x.Amount)).ToList();
                                    }
                                    else
                                    {
                                        singleProgram = programs.Where(x => x.Name.Trim() == programItem.Name.Trim() && Convert.ToInt32(x.Amount) <= Convert.ToInt32(programItem.Amount)).OrderByDescending(x => Convert.ToInt32(x.Amount)).ToList();
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        var singProgram = IncentiveTranslatedsForSpecialProgramsForVin.Where(x => x.IncentiveTranslatedId.Equals(vehicle.ProgramId)).FirstOrDefault();

                        decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(AgileVehicle.MSRP) * (Convert.ToDecimal(singProgram.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(AgileVehicle.MSRP) * (Convert.ToDecimal(singProgram.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(AgileVehicle.MSRP) * (Convert.ToDecimal(singProgram.Amount) / 100));
                        decimal Amount = singProgram.DiscountType.Equals(2) ? Convert.ToInt32(PercentageAmount) : Convert.ToDecimal(singProgram.Amount);



                        var ProgramData = new VehicleProgramData()
                        {
                            Id = "0",
                            ProgramId = 0,
                            ProgramName = singProgram.ProgramName.Trim(),
                            Name = singProgram.ProgramName.Trim(),
                            Amount = "$" + Amount.ToString(CultureInfo.InvariantCulture),
                            Type = singProgram.Type,
                            Disclaimer = singProgram.Disclaimer,
                            IsAprParent = false,
                            StartDate = singProgram.StartDate,
                            EndDate = singProgram.EndDate,
                            IsUpfitted = false

                        };
                        singleProgramDatas.Add(ProgramData);
                    }
                    if (programs.Any())
                    {
                        var tempcashPrograms = programs.Where(x => (x.Start.HasValue && x.End.HasValue &&
                                                                x.Start <= DateTime.Now &&
                                                                  DateTime.Now < x.End.Value.AddDays(1)) &&
                                                               x.Type == ProgramTypeEnum.Cash.ToString()
                                                                && x.Conditional == "false"  //Temporady Disabled
                                                               ).ToList();
                        var tempconditionalPrograms = programs.Where(x => (x.Start.HasValue && x.End.HasValue &&
                                                                x.Start <= DateTime.Now &&
                                                                 DateTime.Now < x.End.Value.AddDays(1)) &&
                                                               x.Type != ProgramTypeEnum.APR.ToString() &&
                                                               x.Conditional == "true").ToList();

                        var tempaprPrograms = programs.Where(x => (x.Start.HasValue && x.End.HasValue &&
                                                                x.Start <= DateTime.Now &&
                                                                 DateTime.Now < x.End.Value.AddDays(1)) &&
                                                               x.Type == ProgramTypeEnum.APR.ToString()
                                                                && x.Conditional == "true") //Temporady Disabled
                                                               .OrderBy(x => x.ProgramId).ToList();
                        if (tempcashPrograms.Any())
                        {
                            cashPrograms = FormatPrograms(tempcashPrograms, translateds, vehicle.Vin, vehicle.ModelId);
                        }
                        if (tempconditionalPrograms.Any())
                        {
                            conditionalPrograms = FormatPrograms(tempconditionalPrograms, translateds, vehicle.Vin, vehicle.ModelId);
                        }
                        if (tempaprPrograms.Any())
                        {
                            aprPrograms = FormatPrograms(tempaprPrograms, translateds, vehicle.Vin, vehicle.ModelId);
                        }

                        if (singleProgram.Any())
                        {
                            singleProgramDatas = FormatPrograms(singleProgram, translateds, vehicle.Vin, vehicle.ModelId);
                        }
                    }

                    var ManufactrerCashTradeOffers = IncentiveTranslatedsForSpecialProgramsForVin.Where(x => x.DisplaySection == (int)Enum.DisplaySection.Manufacturer).ToList();
                    foreach (var item in ManufactrerCashTradeOffers)
                    {
                        cashPrograms.Add(new VehicleProgramData()
                        {
                            Id = "0",
                            ProgramId = 0,
                            ProgramName = item.ProgramName.Trim(),
                            Name = item.ProgramName.Trim(),
                            Amount = "$" + Math.Abs(double.Parse(item.Amount)).ToString(CultureInfo.InvariantCulture),
                            Type = item.Type,
                            Disclaimer = item.Disclaimer,
                            IsAprParent = false,
                            StartDate = item.StartDate,
                            EndDate = item.EndDate,
                            IsUpfitted = false

                        });
                    }
                    var ConditionalCashTradeOffers = IncentiveTranslatedsForSpecialProgramsForVin.Where(x => x.DisplaySection == (int)Enum.DisplaySection.Conditional).ToList();
                    foreach (var item in ConditionalCashTradeOffers)
                    {
                        conditionalPrograms.Add(new VehicleProgramData()
                        {
                            Id = "0",
                            ProgramId = 0,
                            ProgramName = item.ProgramName.Trim(),
                            Name = item.ProgramName.Trim(),
                            Amount = "$" + Math.Abs(double.Parse(item.Amount)).ToString(CultureInfo.InvariantCulture),
                            Type = item.Type,
                            Disclaimer = item.Disclaimer,
                            IsAprParent = false,
                            StartDate = item.StartDate,
                            EndDate = item.EndDate,
                            IsUpfitted = false

                        });
                    }
                }

            }

            var result = new
            {
                cashPrograms = cashPrograms,
                conditionalPrograms = conditionalPrograms,
                aprPrograms = aprPrograms,
                singleProgramDatas = singleProgramDatas,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }


        private List<VehicleProgramData> FormatPrograms(List<Program> programs, List<IncentiveTranslated> translateds, string vin, int modelId)
        {
            List<VehicleProgramData> result = new List<VehicleProgramData>();
            foreach (Program program in programs)
            {
                if ((program.Start.HasValue && program.End.HasValue &&
                     program.Start <= DateTime.Now &&
                     DateTime.Now < program.End.Value.AddDays(1)))
                {
                    IncentiveTranslated translatedItem = translateds.Where(x =>
                            (x.ProgramName.Equals(program.Name, StringComparison.OrdinalIgnoreCase)
                         && x.Type.Equals(program.Type, StringComparison.OrdinalIgnoreCase))
                        && ((x.Vin.Equals(vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                            || (x.Vin == "" && x.ModelId == modelId)
                             || (x.Vin == "" && x.ModelId == 0))).OrderByDescending(x => x.Vin != string.Empty).ThenByDescending(x => x.ModelId > 0).FirstOrDefault();


                    if (program.Type.Equals(ProgramTypeEnum.APR.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        program.AprTermList = VehicleHelper.GetAprTermListForSpecificProgram(program, DealerID);
                    }

                    string amountFormated = string.Empty;
                    if (program.AprTermList != null && program.AprTermList.AprTerms != null &&
                        program.AprTermList.AprTerms.Any())
                    {
                        foreach (AprTerm apr in program.AprTermList.AprTerms)
                        {
                            string amount = Math.Round(double.Parse(apr.Apr), 3).ToString();
                            if (amount == "0")
                                amount = "0.0";
                            amountFormated = $"{amountFormated}{amount}% for {apr.Term} mo</br>";
                        }
                    }
                    else
                    {
                        amountFormated = "$" +
                                         Math.Abs(double.Parse(program.Amount))
                                             .ToString(CultureInfo.InvariantCulture);
                    }

                    VehicleProgramData item = new VehicleProgramData
                    {
                        ProgramName = program.Name.Trim(),
                        Name = program.Name.Trim(),
                        Amount = amountFormated,
                        Disclaimer = program.Disclaimer,
                        Type = program.Type,
                        Id = program.Id,
                        IsAprParent = program.Type.Equals(ProgramTypeEnum.APR.ToString(), StringComparison.OrdinalIgnoreCase) && string.IsNullOrEmpty(program.Links),
                        StartDate=program.StartDate,
                        EndDate=program.EndDate
                    };

                    if (translatedItem != null)
                    {
                        item.Name = translatedItem.TranslatedName.Trim();
                    }
                    result.Add(item);
                }
            }
            return result;
        }

        private void GetRebateForVehicleByProgramIdAndAgileVehicleId(HttpContext context, string strJson)
        {
            VehicleProgramData data = Deserialize<VehicleProgramData>(strJson);
            List<VehicleProgramData> programData = new List<VehicleProgramData>();
            if (data != null)
            {
                List<Program> programs = VehicleHelper.GetProgramOfByAgileVehicleIDAndProgramId(data.AgileVehicleId, data.ProgramId, DealerID);

                if (programs.Any())
                {
                    List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds();
                    programData = FormatPrograms(programs, translateds, data.Vin, data.ModelId);
                }
            }
            var result = new
            {
                singleProgram = programData,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetProgramList(HttpContext context, string strJson)
        {
            List<string[]> results = new List<string[]>();
            ProgramListData data = Deserialize<ProgramListData>(strJson);
            if (data != null)
            {
                List<ProgramListData> datas = new List<ProgramListData>();
                if (data.NamePlateTrimId != 0)
                {
                    var temp = VehicleHelper.GetProgramByTrimId(data.NamePlateTrimId, DealerID);
                    if (temp.Any())
                    {
                        datas = temp.Select(x => new ProgramListData
                        {
                            ProgramId = x.ProgramId.ToString(),
                            IsSelected = "false",
                            Name = x.Name.ToString(),
                            Type = x.Type.ToString(),
                            Disclaimer = x.Disclaimer.ToString(),
                            StartDate = x.StartDate.ToString(),
                            EndDate = x.EndDate.ToString()

                        }).ToList();
                    }
                }
                else
                {
                    if (data.NamePlateId != 0)
                    {
                        var temp = VehicleHelper.GetProgramByNamePlateId(data.NamePlateId, DealerID);
                        if (temp.Any())
                        {
                            datas = temp.Select(x => new ProgramListData
                            {
                                ProgramId = x.ProgramId.ToString(),
                                IsSelected = "false",
                                Name = x.Name.ToString(),
                                Type = x.Type.ToString(),
                                Disclaimer = x.Disclaimer.ToString(),
                                StartDate = x.StartDate.ToString(),
                                EndDate = x.EndDate.ToString()

                            }).ToList();
                        }
                    }
                }



                if (datas.Any())
                {
                    results = datas.Select(x => new[]
                    {
                        x.ProgramId.ToString(),
                        x.IsSelected,
                        x.Name,
                        x.Type,
                        x.Disclaimer,
                        x.StartDate,
                        x.EndDate,
                        x.Amount,
                    }).ToList();
                }

                var result = new
                {
                    iTotalRecords = datas.Count,
                    aaData = results,
                    isSuccess = true,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        //private void GetVehiclePrograms(HttpContext context, string strJson)
        //{
        //    AgileVehicle agileVehicle = Deserialize<AgileVehicle>(strJson);

        //    if (agileVehicle != null)
        //    {
        //        agileVehicle = VehicleHelper.GetAgileVehicleByVin(agileVehicle.Vin, DealerID);
        //        List<VehicleProgramData> datas =
        //            VehicleHelper.GetVehicleProgramDatasByVehicelId(agileVehicle.AgileVehicleId, DealerID);
        //        List<string[]> results = new List<string[]>();

        //        if (datas.Any())
        //        {
        //            results = datas.Select(x => new[]
        //            {
        //                x.ProgramId.ToString(),
        //                x.Name,
        //                 x.TranslatedName,
        //                x.Type,
        //                x.Disclaimer,
        //                 $"{x.StartDate} - {x.EndDate}",
        //                x.Id,
        //                x.Amount,
        //                x.IsActive,
        //                //x.IsDeleted
        //            }).ToList();

        //        }
        //        var result = new
        //        {
        //            iTotalRecords = datas.Count,
        //            aaData = results,
        //            isSuccess = true,
        //        };
        //        context.Response.Write(JsonConvert.SerializeObject(result));
        //    }
        //}
    }
}