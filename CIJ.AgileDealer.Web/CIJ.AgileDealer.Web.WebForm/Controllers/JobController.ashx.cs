﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using Microsoft.Azure;
using System.Web.Providers.Entities;
using AgileDealer.Data.Entities.Enums;
using AgileDealer.Data.Entities.Jobs;
using CJI.AgileDealer.Web.Base.Helpers;
using Microsoft.Ajax.Utilities;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for JobController
    /// </summary>
    public class JobController : BaseController
    {
        int TimeDifference = Convert.ToInt32(ConfigurationManager.AppSettings["TimeDifference"]);


        private CloudQueue _reconRequestQueue;
        private CloudQueue _incentivePullerRequestQueue;

        private const string ReconJob = "Inventory";
        private const string IncentiveJob = "Incentives";

        private static JobHelper _jobHelper;
        private static JobHelper JobHelper
        {
            get
            {
                if (_jobHelper == null)
                {
                    _jobHelper = new JobHelper();
                }
                return _jobHelper;
            }
            set { _jobHelper = value; }
        }

        private static CloudStorageAccount _storageAccount;
        private static CloudStorageAccount StorageAccount
        {
            get
            {
                if (_storageAccount == null)
                {
                    _storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
                }
                return _storageAccount;
            }
            set { _storageAccount = value; }
        }

        private static CloudQueueClient _queueClient;
        private static CloudQueueClient QueueClient
        {
            get
            {
                if (_queueClient == null)
                {
                    _queueClient = StorageAccount.CreateCloudQueueClient();
                }
                return _queueClient;
            }
            set { _queueClient = value; }
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetJobTrackingDashboard":
                        GetJobTrackingDashboard(context);
                        break;
                    case "TriggerReconJobManually":
                        TriggerReconJobManually(context, strJson);
                        break;
                    case "TriggerIncentiveManually":
                        TriggerIncentiveManually(context, strJson);
                        break;
                }
            }
            catch (ThreadAbortException ex1)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("JobController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method + " with data: " + strJson);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        private void GetJobTrackingDashboard(HttpContext context)
        {
            List<JobTracking> jobTrackings = JobHelper.GetJobList();
            List<string[]> results = new List<string[]>();
            if (jobTrackings.Any())
            {
                results = jobTrackings.Select(x => new[]
                {
                    x.JobTrackingId.ToString(CultureInfo.InvariantCulture),
                    x.JobName,
                    x.Status == JobStatusEnum.Completed.ToString() ? "Updated" : x.Status,
                    x.TriggerBy,
                     x.TriggeredTime.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),
                    x.StarTime.HasValue ? Convert.ToDateTime(x.StarTime).AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture): string.Empty,
                    x.EndTime.HasValue ? Convert.ToDateTime(x.EndTime).AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture): string.Empty,
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        public async void TriggerReconJobManually(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            string message = string.Empty;
            try
            {
                JobTracking currentRunningJob = JobHelper.GetCurrentRunningOrTriggerdJob(ReconJob);
                if (currentRunningJob == null)
                {
                    _reconRequestQueue = QueueClient.GetQueueReference("triggerrecon");
                    var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject("triggerrecon"));
                    _reconRequestQueue.CreateIfNotExists();
                    _reconRequestQueue.Clear();

                    message = JobStatusEnum.Triggered.ToString();
                    JobHelper.LogTheJob(ReconJob, HttpContext.Current.User.Identity.Name, JobStatusEnum.Triggered.ToString());

                    await _reconRequestQueue.AddMessageAsync(queueMessage, null, TimeSpan.FromSeconds(0), null, null,
                        CancellationToken.None);
                }
                else
                {
                    message = currentRunningJob.Status;
                }
            }

            catch (Exception ex)
            {
                isSuccess = false;
                var el = ExceptionHelper.LogException("JobController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + "TriggerReconJobManually");
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }

            var result = new
            {
                isSuccess = isSuccess,
                message = message
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
        public async void TriggerIncentiveManually(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            string message = string.Empty;
            try
            {
                JobTracking currentRunningJob = JobHelper.GetCurrentRunningOrTriggerdJob(IncentiveJob);
                if (currentRunningJob == null)
                {
                    _incentivePullerRequestQueue = QueueClient.GetQueueReference("triggerincentive");
                    var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject("triggerincentive"));
                    _incentivePullerRequestQueue.CreateIfNotExists();
                    _incentivePullerRequestQueue.Clear(); // Clear all previous message before execute the next message

                    message = JobStatusEnum.Triggered.ToString();
                    JobHelper.LogTheJob(IncentiveJob, HttpContext.Current.User.Identity.Name, JobStatusEnum.Triggered.ToString());

                    await _incentivePullerRequestQueue.AddMessageAsync(queueMessage, null, TimeSpan.FromSeconds(0),
                        null, null, CancellationToken.None);
                }
                else
                {
                    message = currentRunningJob.Status;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var el = ExceptionHelper.LogException("JobController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + "TriggerIncentiveManually");
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }

            var result = new
            {
                isSuccess = isSuccess,
                message = message
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
    }
}
