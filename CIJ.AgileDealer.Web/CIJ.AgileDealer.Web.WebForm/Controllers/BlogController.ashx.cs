﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for BlogController
    /// </summary>
    public class BlogController : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetBlogEntryById":
                        GetBlogEntryById(context, strJson);
                        break;
                    case "UpdateBlogEntry":
                        UpdateBlogEntry(context, strJson);
                        break;
                    case "CreateBlogEntry":
                        CreateBlogEntry(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("BlogController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        private void UpdateBlogEntry(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            string currentUrl = HttpHelper.GetCompleteUrl(context.Request);
            BlogEntryResult tempBlogEntry = Deserialize<BlogEntryResult>(strJson);
            if (tempBlogEntry != null)
            {
                BlogHelper.UpdateBlogEntry(tempBlogEntry, currentUrl,DealerID);
            }
            else
            {
                isSuccess = false;
            }

            var result = new
            {
                isSuccess = isSuccess,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void CreateBlogEntry(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            string currentUrl = HttpHelper.GetCompleteUrl(context.Request);
            BlogEntryResult tempBlogEntry = Deserialize<BlogEntryResult>(strJson);
            if (tempBlogEntry != null)
            {
                BlogHelper.CreateBlogEntry(tempBlogEntry, currentUrl,DealerID);
            }
            else
            {
                isSuccess = false;
            }

            var result = new
            {
                isSuccess = isSuccess,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetBlogEntryById(HttpContext context, string strJson)
        {
            BlogEntryResult blog = Deserialize<BlogEntryResult>(strJson);
            if (blog != null)
            {
                blog = BlogHelper.GetBlogEntryById(blog.BlogEntryId,SiteInfoHelper.GetLanguageId(),DealerID);

            }
            var result = new
            {
                blog = blog,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
    }
}