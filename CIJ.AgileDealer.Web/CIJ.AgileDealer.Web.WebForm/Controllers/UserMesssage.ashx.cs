﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for UserMesssage
    /// </summary>
    public class UserMesssage : BaseController, System.Web.SessionState.IRequiresSessionState
    {
        private string mailFrom = ConfigurationManager.AppSettings["smtpAcct"];
        private string bccEmail = ConfigurationManager.AppSettings["BCCEmail"];

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var method = context.Request.QueryString["method"];
            switch (method)
            {
                case "UserMesssage":
                    await InsertUserMessageToDb(context);
                    break;
            }
        }
        public async Task InsertUserMessageToDb(HttpContext context)
        {
            VehicleFeedBack vehicleFeedBack = new VehicleFeedBack();
            HomeNetVehicle vehicle = new HomeNetVehicle();
            int dealerId = 1;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    string name = context.Request.QueryString["name"];
                    string phone = context.Request.QueryString["phone"];
                    string email = context.Request.QueryString["email"];
                    string message = context.Request.QueryString["message"];
                    string vehicleId = context.Request.QueryString["vehicleId"];
                    string postalcode = context.Request.QueryString["postalcode"];
                    string scheduledate = context.Request.QueryString["scheduledate"];
                    string scheduleweekdaytime = context.Request.QueryString["scheduleweekdaytime"];
                    string schedulesaturdaytime = context.Request.QueryString["schedulesaturdaytime"];

                    int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(context.Session, context.Request);


                    if (!string.IsNullOrEmpty(scheduledate))
                    {
                        message = "Appointment Request: " + scheduledate; 
                    }
                    if (!string.IsNullOrEmpty(scheduleweekdaytime))
                    {
                        message += ", " + scheduleweekdaytime + ".";
                    }
                    else if (!string.IsNullOrEmpty(schedulesaturdaytime))
                    {
                        message += ", " + schedulesaturdaytime + ".";
                    }

                    message = message + " " + context.Request.QueryString["message"];

                    vehicleFeedBack.Name = name;
                    vehicleFeedBack.Phone = phone;
                    vehicleFeedBack.Email = email;
                    vehicleFeedBack.Message = message;
                    vehicleFeedBack.AgileVehicleId = vehicleId;
                    vehicleFeedBack.SubmittedDate = DateTime.Now;
                    vehicleFeedBack.BuyerSessionId = buyerSessionId;
                    vehicleFeedBack.DealerID = DealerID;
                    vehicleFeedBack.PostalCode = postalcode;
                   

                    int agilevehicleId = Convert.ToInt32(vehicleFeedBack.AgileVehicleId);
                    vehicle = VehicleHelper.GetVehicleByVehicleId(agilevehicleId, DealerID);
                    if (vehicle != null)
                    {
                        vehicleFeedBack.HomeNetVehicle = vehicle;
                        dealerId = vehicle.DealerID;
                    }
                    vehicleFeedBack.IsCompleted = true;
                    db.VehicleFeedBacks.Add(vehicleFeedBack);
                    db.SaveChanges();
                }
                var result = new
                {
                    isSuccess = true,
                };

                await Task.Run(() =>
                {
                    EmailController.SendEmailForCheckAvailability(JsonHelper.Serialize(vehicleFeedBack), dealerId);
                    EmailController.SendEmailConfirmation(vehicleFeedBack.Email);
                }).ContinueWith(t =>
                    {
                        context.Response.Write(JsonConvert.SerializeObject(result));
                    }
                );
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false,
                };
                var json = new JavaScriptSerializer().Serialize(vehicleFeedBack);
                var el = ExceptionHelper.LogException("UserMesssage.InsertUserMessageToDb() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when InsertUserMessageToDb: with customer info: " + vehicleFeedBack.ToString());
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
            
        }

        private Email CreateNewEmailContent(ADF adfInformation)
        {
            string content = XmlHelper.Serialize(adfInformation);
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = mailFrom,
                MailTo = EleadTrackAdress,
                MailBCC = bccEmail,
                MailSubject = "Website Vehicle Lead from " + adfInformation.Prospect.Customer.Contact.FirstName,
                MailText = content,
            };
            return email;
        }
    }
}