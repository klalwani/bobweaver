﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using CIJ.AgileDealer.Web.Base.Repositories;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for EmployeeController
    /// </summary>
    public class EmployeeController : BaseController
    {
        private string localUploadFolder = ConfigurationManager.AppSettings["LocalUploadFolder"];
        private string localUploadFolderName = ConfigurationManager.AppSettings["LocalUploadFolderName"];
        private string blobContainer = ConfigurationManager.AppSettings["BlobContainer"];
        private string storageConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];

        private VehicleRepository _vehicleRepository;

        private VehicleRepository VehicleRepository
        {
            get
            {
                if (_vehicleRepository == null)
                    _vehicleRepository = new VehicleRepository();
                return _vehicleRepository;
            }
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetEmployee":
                        GetEmployee(context,strJson);
                        break;
                    case "UpdateEmployee":
                        UpdateEmployee(context, strJson);
                        break;
                    case "AddEmployee":
                        AddEmployee(context, strJson);
                        break;
                    case "DeleteEmployee":
                        DeleteEmployee(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("DashboardController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }


        private void GetEmployee(HttpContext context, string strJson)
        {
            Employee employee = Deserialize<Employee>(strJson);
            List<Employee_Department>  employeeDepartments = new List<Employee_Department>();
            if (employee != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    employee = db.Employees.FirstOrDefault(x=>x.EmployeeId == employee.EmployeeId);
                    if(employee != null)
                    employee.Departments =
                        db.EmployeeDepartments.Where(x => x.EmployeeId == employee.EmployeeId).ToList();
                    var result = new
                    {
                        employee = employee,
                        isSuccess = true,
                    };
                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            }
        }

        private void UpdateEmployee(HttpContext context, string strJson)
        {
            Employee tempEmployee = Deserialize<Employee>(strJson);
            Employee employee;
            if (tempEmployee != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    employee = db.Employees.FirstOrDefault(x => x.EmployeeId == tempEmployee.EmployeeId);
                    if (employee != null)
                    {
                        employee.FirstName = tempEmployee.FirstName;
                        employee.LastName = tempEmployee.LastName;
                        employee.Order = tempEmployee.Order;
                        employee.Email = tempEmployee.Email;
                        employee.OfficePhone = tempEmployee.OfficePhone;
                        employee.CellPhone = tempEmployee.CellPhone;
                        employee.Ext = tempEmployee.Ext;
                        employee.IsNew = tempEmployee.IsNew;
                        employee.IsDeleted = tempEmployee.IsDeleted;
                        employee.ModifiedDt = DateTime.Now;
                        employee.ImageName = tempEmployee.ImageName;

                        AssignEmployeeIntoDepartment(db, tempEmployee);
                        UploadImageOfEmployeeIntoAzure(db, employee);
                        


                        db.SaveChanges();
                        var result = new
                        {
                            isSuccess = true,
                        };
                        context.Response.Write(JsonConvert.SerializeObject(result));
                    }
                }
            }
        }

        private void AssignEmployeeIntoDepartment(ServicePageDbContext db, Employee employee)
        {
            List<Employee_Department> assignedDepartments =
                db.EmployeeDepartments.Where(x => x.EmployeeId == employee.EmployeeId).ToList();

            if (assignedDepartments.Any())
            {
                db.EmployeeDepartments.RemoveRange(assignedDepartments);
                db.SaveChanges();
            }

            if (employee.Departments != null && employee.Departments.Any())
            {
                foreach (Employee_Department department in employee.Departments)
                {
                    db.EmployeeDepartments.Add(new Employee_Department()
                    {
                        EmployeeId = employee.EmployeeId,
                        DepartmentId = department.DepartmentId,
                        CreatedDt = DateTime.Now,
                        ModifiedDt = DateTime.Now,
                        Sequence = employee.EmployeeId.ToString(),
                        Title = department.Title,
                        IsManager = department.Title.ToLower().Contains("manager")
                    });
                }
                db.SaveChanges();
            }

        }

        private void AddEmployee(HttpContext context, string strJson)
        {
            Employee employee = Deserialize<Employee>(strJson);
            bool isSuccess = false;
            if (employee != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    int? maxValue = 0;
                    if (db.Employees.Any())
                        maxValue = db.Employees.Max(x => x.EmployeeId);
                    employee.EmployeeId = maxValue.Value + 1;
                    employee.CreatedDt = DateTime.Now;
                    employee.ModifiedDt = DateTime.Now;
                    employee.DealerID = 1;
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    AssignEmployeeIntoDepartment(db, employee);
                    UploadImageOfEmployeeIntoAzure(db, employee);
                    isSuccess = true;
                }
            }
            var result = new
            {
                isSuccess = isSuccess
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void DeleteEmployee(HttpContext context, string strJson)
        {
            Employee tempEmployee = Deserialize<Employee>(strJson);
            Employee employee;
            if (tempEmployee != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    bool isSuccess = false;
                    employee = db.Employees.FirstOrDefault(x => x.EmployeeId == tempEmployee.EmployeeId);
                    if (employee != null)
                    {
                        employee.IsDeleted = true;
                        employee.ModifiedDt = DateTime.Now;
                        db.Entry(employee).State = EntityState.Modified;
                        db.SaveChanges();
                        isSuccess = true;
                    }
                    var result = new
                    {
                        isSuccess = isSuccess
                    };
                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            }
        }

        private void UploadImageOfEmployeeIntoAzure(ServicePageDbContext db, Employee employee)
        {
            if (!string.IsNullOrEmpty(employee.ImageName))
            {
                string uploadfolder = GetFolderNameForUpload("UploadStaffImage");
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(employee.ImageName);
                string dirFullPath = GetLocalUploadFolder(fileNameWithoutExtension);
                string azureLink = Path.Combine(ImageServer, uploadfolder, employee.ImageName);
                employee.Photo = azureLink;
                db.SaveChanges();
                VehicleRepository.UploadAgileVehicleImage(storageConnectionString, uploadfolder, dirFullPath, blobContainer);
            }
        }

        private string GetFolderNameForUpload(string method)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(method))
            {
                UploadInformation tempUploadInformation = MasterDataHelper.GetUploadInformation(method, DealerID);
                if (tempUploadInformation != null)
                {
                    result = tempUploadInformation.FolderName;
                }
            }
            return result;
        }

        private string GetLocalUploadFolder(string customName = "")
        {
            string dirFullPath = HttpContext.Current.Server.MapPath(localUploadFolder);
            if (!string.IsNullOrEmpty(customName))
                dirFullPath = Path.Combine(dirFullPath, customName);

            if (!Directory.Exists(dirFullPath))
            {
                Directory.CreateDirectory(dirFullPath);
            }
            return dirFullPath;
        }
    }
}