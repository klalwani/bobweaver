﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using System.Threading.Tasks;
namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for NotificationController
    /// </summary>
    public class NotificationController : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            string method = string.Empty;

            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "SaveDataForNotificationSetting":
                        SaveDataForNotificationSetting(context, strJson);
                        break;
                    case "GetDataForNotificationSetting":
                        GetDataForNotificationSetting(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("NotificationController.ProcessRequest() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }


        private void SaveDataForNotificationSetting(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            List<NotificationSetting> settings = Deserialize<List<NotificationSetting>>(strJson);
            if (settings != null && settings.Any())
            {
                try
                {
                    NotificationHelper.SaveNotificationSettings(settings,DealerID);
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                }
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        private void GetDataForNotificationSetting(HttpContext context, string strJson)
        {
            bool isSuccess = true;
            List<NotificationSetting> data = new List<NotificationSetting>();
            try
            {
                data = NotificationHelper.GetNotificationSettings(DealerID);
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            var result = new
            {
                data = data,
                isSuccess = isSuccess,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
    }
}