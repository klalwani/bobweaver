﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Configuration;
using System.Threading.Tasks;
using System.Linq;
using AgileDealer.Data.Entities.Vehicles;
using System.IO;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Collections.Generic;
using AgileDealer.Common.Helper;
using CJI.AgileDealer.Web.Base.Context;
using System.Data.Entity;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for VehicleImageManager
    /// </summary>
    public class VehicleImageManager : HttpTaskAsyncHandler, System.Web.SessionState.IRequiresSessionState
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            if (!HttpContext.Current.User.IsInRole("Admin") && !HttpContext.Current.User.IsInRole("Dealer"))
            {
                var unautheticatedResponse = new ResultModel { IsSuccess = false };
                unautheticatedResponse.ErrorMessages.Add("unauthorized");
                context.Response.Write(JsonConvert.SerializeObject(unautheticatedResponse));
            }

            string method = context.Request.QueryString["method"];
            try
            {
                switch (method)
                {
                    case "GetVinAndStockNumbers":
                        await GetVinAndStockNumbers(context);
                        break;
                    case "GetImageList":
                        await GetImageList(context, context.Request.QueryString["vin"]);
                        break;
                    case "UploadImages":
                        await UploadNewImages(context, context.Request["vin"]);
                        break;
                    case "DeleteImages":
                        await DeleteAllImages(context, context.Request["vin"]);
                        break;
                    case "DeleteImage":
                        await DeleteImage(context, context.Request["imageId"]);
                        break;
                    case "MarkImageAsMain":
                        await MarkImageMain(context, context.Request["imageId"]);
                        break;
                }

            }
            catch (Exception ex)
            {
                var result = new ResultModel { IsSuccess = false };
                result.ErrorMessages.Add(ex.Message);
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = CIJ.AgileDealer.Web.Base.Helpers.ExceptionHelper.LogException("VehicleImageManager.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                Utilities.EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public override bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private async Task GetVinAndStockNumbers(HttpContext context)
        {
            ResultModel<List<string>> response = new ResultModel<List<string>>();
            try
            {
                using (ServicePageDbContext dbConnection = new ServicePageDbContext())
                {
                    var queryResult = dbConnection.AgileVehicles.Where(x => !x.IsSold).Select(x => x.Vin + " - " + x.StockNumber);
                    response.Data = await queryResult?.ToListAsync();
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ErrorMessages.Add(ex.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }

        private async Task GetImageList(HttpContext context, string vin)
        {
            ResultModel<List<AgileVehicleImage>> response = new ResultModel<List<AgileVehicleImage>>();
            try
            {
                if (!string.IsNullOrEmpty(vin))
                {
                    using (ServicePageDbContext dbConnection = new ServicePageDbContext())
                    {
                        var queryResult = from agileImages in dbConnection.AgileVehicleImages
                                          join av in dbConnection.AgileVehicles
                                          on agileImages.AgileVehicleID equals av.AgileVehicleId
                                          where av.Vin == vin
                                          select agileImages;
                        response.Data = await queryResult?.ToListAsync();
                    }
                }
                else
                {
                    response.ErrorMessages.Add("Empty vin number");
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessages.Add(exception.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }

        private async Task UploadNewImages(HttpContext context, string vin)
        {
            ResultModel response = new ResultModel();
            try
            {
                var count = context.Request.Files.Count;
                var uploadFOlder = ConfigurationManager.AppSettings["UploadFolder"];
                if (count > 0)
                {
                    using (ServicePageDbContext dbConnection = new ServicePageDbContext())
                    {
                        // get agile vehicle id for vin
                        int agileVehicleId = dbConnection.AgileVehicles.FirstOrDefault(x => x.Vin == vin)?.AgileVehicleId ?? 0;
                        int imageOrder = dbConnection.AgileVehicleImages.Max(x => x.ImageOrder);
                        foreach (string fileName in context.Request.Files)
                        {
                            // read the data from client stream
                            HttpPostedFile file = context.Request.Files[fileName];
                            byte[] fileData = null;
                            using (var binaryReader = new BinaryReader(file.InputStream))
                            {
                                fileData = binaryReader.ReadBytes(file.ContentLength);
                            }

                            // Store it to DB
                            string relativeUrl = uploadFOlder + file.FileName;
                            dbConnection.AgileVehicleImages.Add(new AgileVehicleImage
                            {
                                ImageName = fileName,
                                AgileVehicleID = agileVehicleId,
                                ImageOrder = imageOrder++,
                                RelativeUrl = relativeUrl,
                                XResolution = 0,
                                YResolution = 0,
                                CreatedDt = DateTime.Now,
                                ModifiedDt = DateTime.Now,
                                DealerID = DealerInfoHelper.DealerID,
                            });

                            // Upload it to Azure
                            await CloudStorageHelper.UploadFileToAzureFromMemory(fileData, relativeUrl);
                        }
                        await dbConnection.SaveChangesAsync();
                    }
                }
            }
            catch (Exception exception)
            {
                response.IsSuccess = false;
                response.ErrorMessages.Add(exception.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }

        private async Task DeleteImage(HttpContext context, string imageId)
        {
            ResultModel response = new ResultModel();
            try
            {
                int agileVehicleImageId;
                if (!string.IsNullOrEmpty(imageId) && int.TryParse(imageId, out agileVehicleImageId))
                {
                    using (ServicePageDbContext dbConnection = new ServicePageDbContext())
                    {
                        var vehicleImage = await dbConnection.AgileVehicleImages.FirstOrDefaultAsync(x => x.AgileVehicleImageID == agileVehicleImageId);
                        dbConnection.AgileVehicleImages.Remove(vehicleImage);
                        await dbConnection.SaveChangesAsync();
                    }
                }
                else
                {
                    response.ErrorMessages.Add("Wrong ImageId");
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessages.Add(exception.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }

        private async Task DeleteAllImages(HttpContext context, string vin)
        {
            ResultModel response = new ResultModel();
            try
            {
                if (!string.IsNullOrEmpty(vin))
                {
                    using (ServicePageDbContext dbConnection = new ServicePageDbContext())
                    {
                        // Write SP to delete all the images for specific vin
                    }
                }
                else
                {
                    response.ErrorMessages.Add("Wrong vin");
                }
            }
            catch (Exception exception)
            {
                response.ErrorMessages.Add(exception.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }

        private async Task MarkImageMain(HttpContext context, string imageId)
        {
            ResultModel response = new ResultModel();
            try
            {

            }
            catch (Exception exception)
            {
                response.ErrorMessages.Add(exception.Message);
            }
            context.Response.Write(JsonConvert.SerializeObject(response));
        }
    }
}