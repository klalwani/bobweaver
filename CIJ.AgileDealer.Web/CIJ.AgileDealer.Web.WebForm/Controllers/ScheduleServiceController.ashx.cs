﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Views.ScheduleService;
using Newtonsoft.Json;
using ScheduleService = AgileDealer.Data.Entities.ScheduleService.ScheduleService;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for ScheduleServiceController
    /// </summary>
    public class ScheduleServiceController : BaseController, System.Web.SessionState.IRequiresSessionState
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "ScheduleService":
                        await SaveDataForScheduleService(context, strJson);
                        break;
                    case "GetScheduleServiceById":
                        GetScheduleServiceById(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                var result = new
                {
                    isSuccess = false,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
                var el = ExceptionHelper.LogException("ScheduleServiceController.ProcessRequest() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: " + method + "with data: " + strJson);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public void GetScheduleServiceById(HttpContext context, string strJson)
        {
            int data = Deserialize<int>(strJson);
            ScheduleService competitiveModel = ScheduleServiceHelper.GetScheduleServiceById(data,DealerID);
            if (competitiveModel != null)
            {
                var result = new
                {
                    aaData = competitiveModel,
                    isSuccess = true,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public async Task SaveDataForScheduleService(HttpContext context, string strJson)
        {
            ScheduleService scheduleService = Deserialize<ScheduleService>(strJson);
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(context.Session, context.Request);
                scheduleService.CreatedDt = DateTime.Now;
                scheduleService.ModifiedDt = DateTime.Now;
                scheduleService.DealerID = 1;
                scheduleService.SaturdayHour = scheduleService.SaturdayHour ?? string.Empty;

                db.ScheduleServices.Add(scheduleService);

                if (scheduleService.ServiceRequestList.Any())
                {
                    foreach (var serviceRequested in scheduleService.ServiceRequestList)
                    {
                        ScheduleServiceRequested requested = new ScheduleServiceRequested
                        {
                            ScheduleServiceId = scheduleService.ScheduleServiceId,
                            ServiceRequestedId = serviceRequested,
                            ScheduleService = scheduleService
                        };
                        db.ScheduleServiceRequesteds.Add(requested);
                    }
                }

                if (scheduleService.CurrentIssueList.Any())
                {
                    foreach (var currentIssue in scheduleService.CurrentIssueList)
                    {
                        ScheduleCurrentIssue issue = new ScheduleCurrentIssue
                        {
                            ScheduleServiceId = scheduleService.ScheduleServiceId,
                            CurrentIssueId = currentIssue,
                            ScheduleService = scheduleService
                        };
                        db.ScheduleCurrentIssues.Add(issue);
                    }
                }

                scheduleService.IsCompleted = true;
                scheduleService.BuyerSessionId = buyerSessionId;
                db.SaveChanges();
            }
            var result = new
            {
                isSuccess = true,
            };

            await Task.Run(() =>
            {
                EmailController.SendEmailForScheduleService(JsonHelper.Serialize(scheduleService),DealerID);
                EmailController.SendEmailConfirmation(scheduleService.Email);
            }).ContinueWith(t =>
                {
                    context.Response.Write(JsonConvert.SerializeObject(result));
                }
            );
        }
    }
}