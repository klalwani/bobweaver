﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Buyer;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for FinanceController
    /// </summary>
    public class FinanceController : BaseController, System.Web.SessionState.IRequiresSessionState
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            string method = string.Empty;
            try
            {
                int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(context.Session, context.Request);
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "InstantlyPreQualify":
                        SaveDataForInstantlyPreQualify(context, strJson, buyerSessionId);
                        break;
                    case "ContactInfo":
                        SaveDataForContactInfo(context, strJson, buyerSessionId);
                        break;
                    case "JoinApplicant":
                        SaveDataForJoinApplicant(context, strJson, buyerSessionId);
                        break;
                    case "GetApplicanttById":
                        GetApplicantByApplicantId(context, strJson);
                        break;
                    case "ArchiveApplicant":
                        ArchiveApplicant(context, strJson);
                        break;
                    case "SendNotificationEmail":
                        await SendNotificationEmail(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("FinanceController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method + ",  Json: " + strJson );
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public void ArchiveApplicant(HttpContext context, string strJson)
        {
            Applicant applicant = Deserialize<Applicant>(strJson);
            if (applicant != null)
            {
                bool isArchived = bool.Parse(applicant.IsArchived.ToString());
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    applicant = db.Applicants.FirstOrDefault(x => x.ApplicantId == applicant.ApplicantId);
                    if (applicant != null)
                    {
                        applicant.IsArchived = isArchived;
                        db.Entry(applicant).State = EntityState.Modified; ;
                        db.SaveChanges();
                    }
                }
                var result = new
                {
                    isSuccess = true,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        public async Task SendNotificationEmail(HttpContext context, string strJson)
        {
            Applicant applicant = Deserialize<Applicant>(strJson);
            if (applicant != null)
            {
                ContactStructure contactStructure = new ContactStructure();
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    contactStructure.ApplicantInfo =
                        db.ApplicantInfos.FirstOrDefault(x => x.ApplicantId == applicant.ApplicantId);

                    contactStructure.ContactInfo =
                        db.ContactInfos.FirstOrDefault(x => x.ApplicantId == applicant.ApplicantId);
                }
                if (contactStructure.ApplicantInfo != null && contactStructure.ContactInfo != null)
                {
                    await SendEmailToDealer(contactStructure,context);
                }
                else if (contactStructure.ApplicantInfo != null && contactStructure.ContactInfo == null)
                {
                    contactStructure.ContactInfo = new ContactInfo();
                    await SendEmailToDealer(contactStructure, context);
                }
            }
        }

        public void GetApplicantByApplicantId(HttpContext context, string strJson)
        {
            ApplicantDetail applicantDetail = new ApplicantDetail();
            int applicantId = Int32.Parse(context.Request.QueryString["ApplicantId"]);
            applicantDetail = ApplicantHelper.GetApplicantByApplicantId(applicantId);

            var result = new
            {
                isSuccess = true,
                applicantDetail = applicantDetail,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        public void SaveDataForInstantlyPreQualify(HttpContext context, string strJson, int buyerSessionId)
        {
            Applicant applicant = Deserialize<Applicant>(strJson);
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                applicant.CreatedDt = DateTime.Now;
                applicant.DealerID = 1;
                applicant.ModifiedDt = DateTime.Now;
                applicant.BuyerSessionId = buyerSessionId;
                db.Applicants.Add(applicant);
                db.SaveChanges();
            }
            var result = new
            {
                isSuccess = true,
                applicantId = applicant.ApplicantId,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        public void SaveDataForContactInfo(HttpContext context, string strJson, int buyerSessionId)
        {
            Applicant applicantItem = new Applicant();
            ContactStructure applicant = Deserialize<ContactStructure>(strJson);
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                applicantItem =
                    db.Applicants.FirstOrDefault(x => x.ApplicantId == applicant.ContactInfo.ApplicantId);
                if (applicantItem != null)
                {
                    applicant.ContactInfo.Applicant = applicantItem;
                    applicant.ContactInfo.CreatedDt = DateTime.Now;
                    applicant.ContactInfo.ModifiedDt = DateTime.Now;
                    applicant.ContactInfo.DealerID = 1;
                    applicant.ContactInfo.BuyerSessionId = buyerSessionId;
                    applicant.ContactInfo.IsCompleted = true;

                    applicant.ApplicantInfo.Applicant = applicantItem;
                    applicant.ApplicantInfo.CreatedDt = DateTime.Now;
                    applicant.ApplicantInfo.DealerID = 1;
                    applicant.ApplicantInfo.ModifiedDt = DateTime.Now;
                    applicant.ApplicantInfo.BuyerSessionId = buyerSessionId;
                    applicant.ApplicantInfo.IsCompleted = true;

                    applicant.EmploymentInfo.Applicant = applicantItem;
                    applicant.EmploymentInfo.CreatedDt = DateTime.Now;
                    applicant.EmploymentInfo.DealerID = 1;
                    applicant.EmploymentInfo.ModifiedDt = DateTime.Now;
                    applicant.EmploymentInfo.BuyerSessionId = buyerSessionId;
                    applicant.EmploymentInfo.IsCompleted = true;

                    db.ContactInfos.Add(applicant.ContactInfo);
                    db.ApplicantInfos.Add(applicant.ApplicantInfo);

                    if (applicant.EmploymentInfo != null)
                    {
                        try
                        {
                            double Amount=Convert.ToDouble(applicant.EmploymentInfo.AdditionalIncomeAmount);
                        }
                        catch {
                            applicant.EmploymentInfo.AdditionalIncomeAmount = 0;
                        }
                    }

                    db.EmploymentInfos.Add(applicant.EmploymentInfo);

                    if (applicant.ContactInfo.NumberOfApplicant == 1 || applicant.ApplicantInfo.NumberOfApplicant == 1 ||
                        applicant.EmploymentInfo.NumberOfApplicant == 1)
                    {
                        applicantItem.IsCompleted = true;
                    }

                    db.SaveChanges();
                }
            }
            var result = new
            {
                isSuccess = true,
                isCompleted = applicantItem != null ? applicantItem.IsCompleted : false,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        public void SaveDataForJoinApplicant(HttpContext context, string strJson, int buyerSessionId)
        {
            ContactStructure applicant = Deserialize<ContactStructure>(strJson);
            Applicant applicantItem;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                applicantItem =
                    db.Applicants.FirstOrDefault(x => x.ApplicantId == applicant.ContactInfo.ApplicantId);
                if (applicantItem != null)
                {
                    applicant.ContactInfo.Applicant = applicantItem;
                    applicant.ContactInfo.CreatedDt = DateTime.Now;
                    applicant.ContactInfo.DealerID = 1;
                    applicant.ContactInfo.ModifiedDt = DateTime.Now;
                    applicant.ContactInfo.BuyerSessionId = buyerSessionId;
                    applicant.ContactInfo.IsCompleted = true;

                    applicant.ApplicantInfo.Applicant = applicantItem;
                    applicant.ApplicantInfo.CreatedDt = DateTime.Now;
                    applicant.ApplicantInfo.DealerID = 1;
                    applicant.ApplicantInfo.ModifiedDt = DateTime.Now;
                    applicant.ApplicantInfo.BuyerSessionId = buyerSessionId;
                    applicant.ApplicantInfo.IsCompleted = true;

                    applicant.EmploymentInfo.Applicant = applicantItem;
                    applicant.EmploymentInfo.CreatedDt = DateTime.Now;
                    applicant.EmploymentInfo.DealerID = 1;
                    applicant.EmploymentInfo.ModifiedDt = DateTime.Now;
                    applicant.EmploymentInfo.BuyerSessionId = buyerSessionId;
                    applicant.EmploymentInfo.IsCompleted = true;

                    db.ContactInfos.Add(applicant.ContactInfo);
                    db.ApplicantInfos.Add(applicant.ApplicantInfo);

                    if (applicant.EmploymentInfo != null)
                    {
                        try
                        {
                            double Amount = Convert.ToDouble(applicant.EmploymentInfo.AdditionalIncomeAmount);
                        }
                        catch
                        {
                            applicant.EmploymentInfo.AdditionalIncomeAmount = 0;
                        }
                    }

                    db.EmploymentInfos.Add(applicant.EmploymentInfo);

                    if (applicant.ContactInfo.NumberOfApplicant == 2 || applicant.ApplicantInfo.NumberOfApplicant == 2 ||
                        applicant.EmploymentInfo.NumberOfApplicant == 2)
                    {
                        applicantItem.IsCompleted = true;
                    }

                    db.SaveChanges();

                }
            }
            var result = new
            {
                isSuccess = true,
                isCompleted = applicantItem != null ? applicantItem.IsCompleted : false,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));

        }

        private async Task SendEmailToDealer(ContactStructure applicant, HttpContext context)
        {
            if (applicant != null)
            {
                await Task.Run(() =>
                {
                    EmailController.SendEmailForForFinance(JsonHelper.Serialize(applicant), DealerID);
                    EmailController.SendEmailConfirmation(applicant.ApplicantInfo.Email);
                }).ContinueWith(t =>
                    {
                        var result = new
                        {
                            isSuccess = true,
                        };
                        context.Response.Write(JsonConvert.SerializeObject(result));
                    }
                );
            }
        }
    }
}

public class ContactStructure
{
    public virtual ContactInfo ContactInfo { get; set; }
    public virtual ApplicantInfo ApplicantInfo { get; set; }
    public virtual EmploymentInfo EmploymentInfo { get; set; }
}