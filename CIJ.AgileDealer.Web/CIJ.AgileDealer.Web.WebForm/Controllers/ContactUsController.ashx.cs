﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.ContactUs;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for ContactUsController
    /// </summary>
    public class ContactUsController : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "GetContactUsDetail":
                        GetContactUsDetail(context, strJson);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("ContactUsController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el); 
            }
        }

        private void GetContactUsDetail(HttpContext context, string strJson)
        {
            int data = Deserialize<int>(strJson);
            CustomerQuestion competitiveModel = ContactUsHelper.GetContactUsById(data, DealerID);
            if (competitiveModel != null)
            {
                var result = new
                {
                    aaData = competitiveModel,
                    isSuccess = true,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
    }
}