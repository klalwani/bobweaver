﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for BaseController
    /// </summary>
    public class BaseController : HttpTaskAsyncHandler, IBaseWebItem
    {
        public int DealerID { get { return DealerInfoHelper.DealerID; } }

        public string DealerCity { get { return DealerInfoHelper.DealerCity; } }

        public string DealerState { get { return DealerInfoHelper.DealerState; } }

        public string DealerName { get { return DealerInfoHelper.DealerName; } }

        public string DealerImageGeneric { get { return DealerInfoHelper.DealerImageGeneric; } }

        public string ImageServer { get { return DealerInfoHelper.ImageServer; } }

        public string ManufacturerName { get { return DealerInfoHelper.ManufactureName; } }

        public string EleadTrackAdress { get { return DealerInfoHelper.EleadTrackAdress; } }

        public int LanguageId { get { return SiteInfoHelper.GetLanguageId(); } }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            
        }

        public override bool IsReusable => true;

        public T Deserialize<T>(string context)
        {
            string jsonData = context;
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }
    }
}