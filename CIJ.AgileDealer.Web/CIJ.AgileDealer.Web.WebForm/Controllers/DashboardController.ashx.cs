﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Dashboards;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
using System.Configuration;
namespace CJI.AgileDealer.Web.WebForm.Controllers
{
    /// <summary>
    /// Summary description for DashboardController
    /// </summary>
    public class DashboardController : BaseController
    {
        int TimeDifference = Convert.ToInt32(ConfigurationManager.AppSettings["TimeDifference"]);


        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string method = string.Empty;
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            try
            {
                method = context.Request.QueryString["method"];
                switch (method)
                {
                    case "ApplicantDashboard":
                        GetDataForApplicantDashboard(context, strJson);
                        break;
                    case "UserAccountDashboard":
                        GetDataForUserAccountDashboard(context);
                        break;
                    case "EmployeeDashboard":
                        GetDataForEmployeeDashboard(context);
                        break;
                    case "GetCompetitiveModels":
                        GetCompetitiveModels(context, strJson);
                        break;
                    case "UpdateCompetitiveModel":
                        UpdateCompetitiveModel(context, strJson);
                        break;
                    case "LoadContactUsDashboard":
                        GetDataForContactUsDashboard(context);
                        break;
                    case "LoadScheduleServiceDashboard":
                        LoadScheduleServiceDashboard(context);
                        break;
                    case "LoadCheckAvailabilityDashboard":
                        LoadCheckAvailabilityDashboard(context);
                        break;
                    case "LoadBlogManagementDashboard":
                        LoadBlogManagementDashboard(context, strJson);
                        break;
                    case "LoadIncentiveManagementDashboard":
                        LoadIncentiveManagementDashboard(context, strJson);
                        break;
                    case "UpdateIncentiveTranslated":
                        UpdateIncentiveTranslated(context, strJson);
                        break;
                    case "ResetIncentiveTranslated":
                        ResetIncentiveTranslated(context, strJson);
                        break;
                    case "LoadIncentiveManagementDashboardSpecialPrograms":
                        LoadIncentiveManagementDashboardSpecialPrograms(context, strJson);
                        break;
                    case "UpdateIncentiveTranslatedSpecialProgram":
                        UpdateIncentiveTranslatedSpecialProgram(context, strJson);
                        break;
                    case "ResetIncentiveTranslatedSpecialProgram":
                        ResetIncentiveTranslatedSpecialProgram(context, strJson);
                        break;
                    case "LoadIncentiveManagementCustomDiscounts":
                        LoadIncentiveManagementCustomDiscounts(context, strJson);
                        break;
                    case "UpdateIncentiveTranslatedCustomDiscounts":
                        UpdateIncentiveTranslatedCustomDiscounts(context, strJson);
                        break;
                    case "ResetIncentiveTranslatedCustomDiscounts":
                        ResetIncentiveTranslatedCustomDiscounts(context, strJson);
                        break;
                    case "HideIncentiveProgram":
                        HideIncentiveProgram(context, strJson);
                        break;
                    case "GetModelSpecificData":
                        GetModelSpecificData(context, strJson);
                        break;
                    case "FilterChromeStyleIDBasedData":
                        FilterChromeStyleIDBasedData(context, strJson);
                        break;
                    case "FilterYearBasedChromeStyleIds":
                        FilterYearBasedChromeStyleIds(context, context.Request.QueryString["chromeStyleIdYear"], context.Request.QueryString["updateForModelId"]);
                        break;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("false");
                var el = ExceptionHelper.LogException("DashboardController.ProcessRequest() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: " + method);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        private void UpdateCompetitiveModel(HttpContext context, string strJson)
        {
            CompetitiveModel data = Deserialize<CompetitiveModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.UpdateCompetitiveModel(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        private void GetModelSpecificData(HttpContext context, string strJson)
        {
            List<string> YearList = new List<string>();
            ModelSpecificData FilterModelSpecicData = VehicleHelper.FilterModelSpecicData(Deserialize<int>(strJson));
            var result = new
            {
                isSuccess = true,
                filterModelSpecicData= FilterModelSpecicData
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
        private void FilterChromeStyleIDBasedData(HttpContext context, string strJson)
        {
            List<MasterData> FilterChromeStyleIDBasedData = VehicleHelper.FilterChromeStyleIDBasedData(Deserialize<string>(strJson));
            var result = new
            {
                isSuccess = true,
                filterChromeStyleIDBasedData = FilterChromeStyleIDBasedData
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void FilterYearBasedChromeStyleIds(HttpContext context, string ChromeStyleIdYear,string UpdateForModelId)
        {
            List<MasterData> FilterYearBasedChromeStyleIdsData = VehicleHelper.FilterYearBasedChromeStyleIdsData(Convert.ToInt32(ChromeStyleIdYear),Convert.ToInt32(UpdateForModelId));
            var result = new
            {
                isSuccess = true,
                filterYearBasedChromeStyleIds = FilterYearBasedChromeStyleIdsData
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void LoadBlogManagementDashboard(HttpContext context, string strJson)
        {
            List<string[]> results = new List<string[]>();
            string cultureName = Deserialize<string>(strJson);
            int languageId = SiteInfoHelper.GetLanguageIdByCultureName(cultureName);
            List<BlogEntryResult> blogs = BlogHelper.GetBlogEntries(languageId, DealerID);
            if (blogs.Any())
            {
                results = blogs.Select(x => new[]
                {
                     x.BlogEntryId.ToString(),
                     x.PostTitle,
                     x.BlogCategory,
                     x.BlogTag,
                     x.DateEnter.AddHours(-TimeDifference).ToShortDateString()
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void LoadCheckAvailabilityDashboard(HttpContext context)
        {
            List<string[]> results = new List<string[]>();
            List<VehicleFeedBackModel> vehicleFeedBacks = CheckAvailabilityHelper.GetVehicleFeedBacks(DealerID);
            if (vehicleFeedBacks.Any())
            {
                results = vehicleFeedBacks.Select(x => new[]
                {
                     x.VehicleFeedBackId.ToString(),
                     x.Name,
                     x.Email,
                     x.Phone,
                     x.Message,
                     x.Vin,
                     x.DealerName,
                     x.SubmittedDate.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void LoadScheduleServiceDashboard(HttpContext context)
        {
            List<string[]> results = new List<string[]>();
            List<ScheduleService> scheduleServices = ScheduleServiceHelper.GetScheduleServices(DealerID);
            if (scheduleServices.Any())
            {
                results = scheduleServices.Select(x => new[]
                {
                     x.ScheduleServiceId.ToString(),
                     x.FirstName,
                     x.LastName,
                     x.Email,
                     x.PhoneNumber,
                     x.ScheduleDate.ToString(CultureInfo.InvariantCulture),
                     x.WeeklyHour,
                     x.SaturdayHour,
                     x.CreatedDt.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture)
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetDataForContactUsDashboard(HttpContext context)
        {
            List<string[]> results = new List<string[]>();
            List<CustomerQuestion> contactUss = ContactUsHelper.GetContactUss(DealerID);
            if (contactUss.Any())
            {
                results = contactUss.Select(x => new[]
                {
                     x.CustomerQuestionId.ToString(),
                     x.FirstName,
                     x.LastName,
                     x.Email,
                     x.PhoneNumber,
                     x.CreatedDt.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture)
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }


        private void GetDataForUserAccountDashboard(HttpContext context)
        {
            List<UserAccountDashboard> userAccounts = UserAccountHelper.GetAllUsers();
            List<string[]> results = new List<string[]>();
            if (userAccounts.Any())
            {
                results = userAccounts.Select(x => new[]
                {
                    x.UserId,
                     x.UserName,
                     x.Email,
                     x.EmailConfirmed,
                     x.PhoneNumber,
                     x.CurrentRole,
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = userAccounts.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetDataForEmployeeDashboard(HttpContext context)
        {
            List<Employee> employees = EmployeeHelper.GetAllEmployee(DealerID);
            List<string[]> results = new List<string[]>();
            if (employees.Any())
            {
                results = employees.Select(x => new[]
                {
                    x.EmployeeId.ToString(),
                     x.FirstName.ToString(),
                     x.LastName.ToString(),
                     x.Email,
                     x.IsNew.HasValue && x.IsNew.Value ? "Yes" : "No",
                     x.Order.HasValue ? x.Order.Value.ToString() : "0",
                     x.IsDeleted ? "Yes" : "No",
                     x.CreatedDt.AddHours(-TimeDifference).ToString(DateTimeFormatInfo.CurrentInfo)
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = employees.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetCompetitiveModels(HttpContext context, string strJson)
        {
            MasterData data = Deserialize<MasterData>(strJson);
            List<string[]> results = new List<string[]>();
            List<CompetitiveModel> competitiveModels = VehicleHelper.GetCompetitiveModels(data, 1);
            if (competitiveModels.Any())
            {
                results = competitiveModels.Select(x => new[]
                {
                    x.CompetitiveModelId.ToString(),
                     x.MakeName.ToString(),
                     x.Name.ToString(),
                     string.IsNullOrEmpty(x.Segment) ? "-None-" : x.Segment,
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = competitiveModels.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void GetDataForApplicantDashboard(HttpContext context, string strJson)
        {
            bool isIncludeArchive = bool.Parse(Deserialize<string>(strJson));
            List<string[]> results = new List<string[]>();
            List<ApplicantDashboard> applicants = ApplicantHelper.GetApplicants(isIncludeArchive, 1);
            if (applicants.Any())
            {
                results = applicants.Select(x => new[]
                {
                     x.ApplicantId.ToString(),
                     x.CustomerName,
                     x.PhoneNumber,
                     x.City,
                     x.State,
                     x.NumberOfApplicants == (int)ApplicantEnums.JoinedApplicant ? "Join (Co-Applicant)" : "Individual",
                     x.IsArchived == true ? "Archived" : "Current",
                     x.DateCreated.AddHours(-TimeDifference).ToString(CultureInfo.InvariantCulture),
                     x.IsCompleted == true ? "Yes" : "No",
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = applicants.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void UpdateIncentiveTranslated(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.UpdateIncentiveTranslated(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        private void ResetIncentiveTranslated(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.ResetIncentiveTranslated(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }

        private void LoadIncentiveManagementDashboard(HttpContext context, string strJson)
        {
            List<string[]> results = new List<string[]>();
            string cultureName = Deserialize<string>(strJson);
            int languageId = SiteInfoHelper.GetLanguageIdByCultureName(cultureName);
            List<IncentiveTranslatedModel> incentives = VehicleHelper.GetIncentiveTranslatedData(false, false);
            if (incentives.Any())
            {
                results = incentives.Select(x => new[]
                {
                     x.DisplaySectionGroupName.ToString(CultureInfo.InvariantCulture),
                    x.TranslatedName ?? "",
                    x.Name ?? string.Empty,
                    x.Type ?? string.Empty,
                    x.Id ?? string.Empty,
                    x.Amount.ToString(CultureInfo.InvariantCulture),
                    x.DisplaySection.HasValue ? x.DisplaySection.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.Position.HasValue ? x.Position.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.ChromeStyleId,
                    x.ChromeStyleIdYear.ToString()
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
        private void UpdateIncentiveTranslatedSpecialProgram(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.UpdateIncentiveTranslatedSpecialProgram(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
        private void ResetIncentiveTranslatedSpecialProgram(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.ResetIncentiveTranslatedSpecialProgram(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
        private void HideIncentiveProgram(HttpContext context, string strJson)
        {
            HideIncentiveProgramModel data = Deserialize<HideIncentiveProgramModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.SaveIncentiveDataToHide(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
        private void LoadIncentiveManagementDashboardSpecialPrograms(HttpContext context, string strJson)
        {
            List<string[]> results = new List<string[]>();
            string cultureName = Deserialize<string>(strJson);
            int languageId = SiteInfoHelper.GetLanguageIdByCultureName(cultureName);
            List<IncentiveTranslatedModel> incentives = VehicleHelper.GetIncentiveTranslatedData(true, false);
            if (incentives.Any())
            {
                results = incentives.Select(x => new[]
                {
                     x.DisplaySectionGroupName.ToString(CultureInfo.InvariantCulture),
                    x.TranslatedName ?? "",
                    x.Name ?? "",
                    x.Type ?? string.Empty,
                    x.Amount.ToString(CultureInfo.InvariantCulture),
                    x.DiscountTypeFriendlyName.ToString(CultureInfo.InvariantCulture),
                    x.ExcludeEngingCodeUserFriendlyName,
                    x.ExcludeVINs,
                    x.StartDate,
                    x.EndDate,
                    x.excludeenginecodesData,
                    x.Disclaimer,
                    x.DisplaySection.HasValue ? x.DisplaySection.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.Position.HasValue ? x.Position.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.Id,
                    x.HasSetDateRange.ToString(),
                    x.HideOtherIncentives.ToString(),
                    x.excludeTrimId,
                    x.ExcludeYear,
                    x.DiscountType.ToString(),
                    x.HideDiscounts.ToString(),
                    x.excludeBodyStyleId,
                    x.IsMasterIncentive.ToString(),
                    x.ChromeStyleId,
                    x.ChromeStyleIdYear.ToString()

                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }

        private void LoadIncentiveManagementCustomDiscounts(HttpContext context, string strJson)
        {
            List<string[]> results = new List<string[]>();
            string cultureName = Deserialize<string>(strJson);
            int languageId = SiteInfoHelper.GetLanguageIdByCultureName(cultureName);
            List<IncentiveTranslatedModel> incentives = VehicleHelper.GetIncentiveTranslatedData(true, true);
            if (incentives.Any())
            {
                results = incentives.Select(x => new[]
                {
                    x.DisplaySectionGroupName.ToString(CultureInfo.InvariantCulture),
                    x.TranslatedName ?? "",
                    x.Name ?? "",
                    x.Type ?? string.Empty,
                    x.Amount.ToString(CultureInfo.InvariantCulture),
                    x.DiscountTypeFriendlyName.ToString(CultureInfo.InvariantCulture),
                    x.ExcludeEngingCodeUserFriendlyName,
                    x.ExcludeVINs,
                    x.StartDate,
                    x.EndDate,
                    x.excludeenginecodesData,
                    x.Disclaimer,
                    x.DisplaySection.HasValue ? x.DisplaySection.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.Position.HasValue ? x.Position.Value.ToString(CultureInfo.InvariantCulture) : "",
                    x.Id,
                    x.HasSetDateRange.ToString(),
                    x.HideOtherIncentives.ToString(),
                    x.excludeTrimId,
                    x.ExcludeYear,
                    x.DiscountType.ToString(),
                    x.HideDiscounts.ToString(),
                    x.excludeBodyStyleId,
                    x.IsMasterIncentive.ToString(),
                    x.ChromeStyleId,
                    x.ChromeStyleIdYear.ToString()
                }).ToList();
            }
            var result = new
            {
                iTotalRecords = results.Count,
                aaData = results,
                isSuccess = true,
            };
            context.Response.Write(JsonConvert.SerializeObject(result));
        }
        private void ResetIncentiveTranslatedCustomDiscounts(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.ResetIncentiveTranslatedCustomDiscount(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
        private void UpdateIncentiveTranslatedCustomDiscounts(HttpContext context, string strJson)
        {
            IncentiveTranslatedModel data = Deserialize<IncentiveTranslatedModel>(strJson);
            if (data != null)
            {
                bool isSuccess = VehicleHelper.UpdateIncentiveTranslatedCustomDiscount(data, DealerID);
                var result = new
                {
                    isSuccess = isSuccess,
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
        //private void FilterModelSpecicData(HttpContext context, string strModelNumber)
        //{

        //    if (!string.IsNullOrEmpty(strModelNumber))
        //    {
        //        using (var context = new AppDbContext())
        //        {
        //            var SiteMapXML = context.Database.SqlQuery<string>("exec Usp_GetFatFooters 0,1");
        //            foreach (var item in SiteMapXML)
        //            {
        //                siteMapLocations.Add(new SiteMapData.SiteMapLocation
        //                {
        //                    Url = _baseUrl + item,
        //                    Priority = 1,
        //                    LastModified = DateTime.Now.ToString("yyyy-MM-dd"),
        //                    ChangeFrequency = SiteMapData.SiteMapLocation.eChangeFrequency.daily
        //                });
        //            }
        //        }
        //        var result = new
        //        {
        //            isSuccess = isSuccess,
        //        };
        //        context.Response.Write(JsonConvert.SerializeObject(result));
        //    }
        //}
    }
}