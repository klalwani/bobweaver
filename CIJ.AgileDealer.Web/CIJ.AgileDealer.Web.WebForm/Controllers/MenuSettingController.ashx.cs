﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Controllers
{

    public class MenuSettingController : BaseController
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            if (context != null)
            {
                string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
                List<TreeviewModel> treeViews = Deserialize<List<TreeviewModel>>(strJson);
                var result = new
                {
                    isSuccess = CMSHelper.UpdateMenu(treeViews,DealerID)
                };
                context.Response.Write(JsonConvert.SerializeObject(result));
            }
        }
    }
}