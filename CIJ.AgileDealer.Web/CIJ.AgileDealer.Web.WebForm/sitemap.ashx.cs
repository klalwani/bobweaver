﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;

namespace CJI.AgileDealer.Web.WebForm
{
    /// <summary>
    /// Summary description for HandlerSiteMap
    /// </summary>
    public class HandlerSiteMap : IHttpHandler
    {
        private XmlWriter _xmlWriter;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/xml";

            var settings = new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true };
            _xmlWriter = XmlWriter.Create(context.Response.OutputStream, settings);
            _xmlWriter.WriteStartDocument();
            _xmlWriter.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");
            // Add root node 
            AddUrl(SiteMap.RootNode);
            // Add all other nodes 
            SiteMapNodeCollection nodes = SiteMap.RootNode.GetAllNodes();
            foreach (SiteMapNode node in nodes)
            {
                AddUrl(node);
            }
            _xmlWriter.WriteEndElement();
            _xmlWriter.WriteEndDocument();
            _xmlWriter.Flush();
        }

        private void AddUrl(SiteMapNode siteMapNode)
        {
            // ignore any nodes that are empty 
            if (string.IsNullOrEmpty(siteMapNode.Url))
            {
                return;
            }
            // ignore any nodes that are not part of the site
            if (siteMapNode.Url.StartsWith("http", true, null))
            {
                return;
            }
            // Open url tag 
            _xmlWriter.WriteStartElement("url");
            _xmlWriter.WriteStartElement("loc");
            _xmlWriter.WriteString(GetFullUrl(siteMapNode.Url));
            _xmlWriter.WriteEndElement();

            // Write last modified
            _xmlWriter.WriteStartElement("lastmod");
            _xmlWriter.WriteString(GetLastModifiedDate(siteMapNode.Url));
            _xmlWriter.WriteEndElement();

            // Close url tag
            _xmlWriter.WriteEndElement();
        }

        private string GetFullUrl(string url)
        {
            HttpContext currentContext = HttpContext.Current;
            string server = currentContext.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.UriEscaped);
            return CombineURL(server, url);
        }

        private string CombineURL(string urlBase, string url)
        {
            urlBase = urlBase.TrimEnd(new[] { '/' });
            url = url.TrimStart(new[] { '/' });
            return urlBase + "/" + url;
        }

        private string GetLastModifiedDate(string url)
        {
            HttpContext currentContext = HttpContext.Current;
            string path = currentContext.Server.MapPath(url);
            return File.GetLastWriteTimeUtc(path).ToString("s");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}