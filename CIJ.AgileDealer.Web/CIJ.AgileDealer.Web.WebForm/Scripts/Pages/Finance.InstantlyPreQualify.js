﻿var FinanceInstantlyPreQualify = {
    getInstantlyPreQualifyInformation: function () {
        var postData = {
            applicantTypeId: '',
            firstName: '',
            lastName: '',
            phoneNumber: '',
            zipCode: ''
        };

        postData.applicantTypeId = $('input[name="creditAppType"]:checked').val();
        postData.firstName = $('#instantFirstName').val();
        postData.lastName = $('#instantLastName').val();
        postData.phoneNumber = $('#instantPhone').val();
        postData.zipCode = $('#instantZip').val();

        return postData;
    }
};