﻿//Forms
var creditForms = [
    {
        currentForm: 'instantlyCtr', previousForm: '', nextForm: 'contactInfoCtr', lastForm: 'contactInfoCtr'
    },
    {
        currentForm: 'contactInfoCtr', previousForm: 'instantlyCtr', nextForm: 'joinApplicantCtr', lastForm: 'completeCtr'
    },
    {
        currentForm: 'joinApplicantCtr', previousForm: 'contactInfoCtr', nextForm: 'completeCtr', lastForm: 'completeCtr'
    }
];

$(document).ready(function () {
    Finance.initialize();
    AgileDealerValidation.InitialValidationEvent();
});


var Finance = {
    localizations : {},
    initialize: function () {
        Finance.registerEvents();
        Finance.localizations = common.getLocalizations();
    },
    saveDataOfCurrentForm: function (currentForm) {
        var result = true;
        if (typeof currentForm != "undefined" && currentForm != null) {
            var data = {};
            var method = 'InstantlyPreQualify';
            switch (currentForm) {
                case 'instantlyCtr':
                    {
                        method = 'InstantlyPreQualify';
                        data = FinanceInstantlyPreQualify.getInstantlyPreQualifyInformation();
                    }
                    break;
                case 'contactInfoCtr':
                    {
                        method = 'ContactInfo';
                        data = FinanceContactInfo.getContactInfoInformation();
                    }
                    break;
                case 'joinApplicantCtr':
                    {
                        method = 'JoinApplicant';
                        data = FinanceJoinApplicant.getJoinApplicantInformation();
                    }
                    break;
                default:
                    {
                        result = false;
                    }
            }

            $.ajax({
                url: '../../Controllers/FinanceController.ashx?method=' + method,
                type: 'POST',
                async: false,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 60 * 30 * 1000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        if (dataResult.applicantId) {
                            Finance.updateApplicantId(dataResult.applicantId);
                        }
                        if (dataResult.isCompleted) {
                            Finance.sendNotificationEmailForElead(dataResult.applicantId);
                        }
                        result = true;
                    }
                },
                error: function () {
                    result = false;
                    alert(Finance.localizations["ErrorMessage"]);
                }
            });

        }

        return result;
    },
    registerEvents: function () {
        $('[id$="btnSubmit"]').on('click', function () {
            var currentForm = Finance.getCurrentForm();
            Finance.enableRequiredFields(currentForm);
            var isValid = Finance.checkValidationOfCurrentForm(currentForm);
            if (isValid) {
                var isSaveSucceed = Finance.saveDataOfCurrentForm(currentForm);
                if (isSaveSucceed) {
                    if (typeof currentForm != "undefined" && currentForm != null) {
                        var movementInfo = Finance.getMovementInfo(currentForm);
                        if (typeof movementInfo != "undefined" && movementInfo != null) {
                            if (movementInfo.nextForm !== '') {
                                if (Finance.getNumberOfApplicant() == NUMBEROFAPPLICANT.JOIN_CO_APPLICANT.toString()) {
                                    Finance.updateCurrentForm(movementInfo.nextForm);
                                    Finance.switchForms(currentForm, movementInfo.nextForm);
                                } else {
                                    Finance.updateCurrentForm(movementInfo.lastForm);
                                    Finance.switchForms(currentForm, movementInfo.lastForm);
                                }
                                Finance.scrollBackToTop();
                            }
                        }
                    }
                }
            } else {
                alert(Finance.localizations["ValidationError"]);
            }
        });

        $("#contactInfoCtr .multiButtonDefault :input").on("change", function () {
            var self = this;
            var currentForm = Finance.getCurrentForm();
            if (self.value == NUMBEROFAPPLICANT.INDIVIDUAL.toString()) {
                Finance.updateNumberOfApplicant(NUMBEROFAPPLICANT.INDIVIDUAL.toString());
                Finance.changeSubmitText(currentForm, 'Submit');
            } else {
                Finance.updateNumberOfApplicant(NUMBEROFAPPLICANT.JOIN_CO_APPLICANT.toString());
                Finance.changeSubmitText(currentForm, 'Next');
            }
        });
    },
    sendNotificationEmailForElead: function (applicantId) {
        var data = {
            applicantId: $('#applicantId').val()
        };
        var method = 'SendNotificationEmail';
        $.ajax({
            url: '../../Controllers/FinanceController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(data),
            success: function (dataResult) {
               
            },
            error: function () {
            }
        });
    },
    switchForms: function (currentForm, nextForm) {
        $('#' + currentForm).addClass('hidden').removeClass('active');
        $('#' + nextForm).removeClass('hidden').addClass('active');
    },
    updateCurrentForm: function (nextForm) {
        $('#currentForm').val(nextForm);
    },
    checkValidationOfCurrentForm: function (currentForm) {
        return $('#' + currentForm).find("[data-val=true]:visible:not([type=hidden])").All("valid");
    },
    enableRequiredFields: function (currentForm) {
        $('#' + currentForm).find("[data-val=true]:visible:not([type=hidden])").blur();
    },
    updateApplicantId: function (id) {
        $('#applicantId').val(id);
    },
    updateNumberOfApplicant: function (numberOfApplicant) {
        $('#numberOfApplicant').val(numberOfApplicant);
    },
    getCurrentForm: function () {
        return $('#currentForm').val();
    },
    getNumberOfApplicant: function () {
        return $('#numberOfApplicant').val();
    },
    getMovementInfo: function (currentForm) {
        var result = null;
        if (typeof currentForm != "undefined" && currentForm != null) {
            for (var i = 0; i < creditForms.length; i++) {
                if (creditForms[i].currentForm === currentForm) {
                    result = creditForms[i];
                }
            }
        }
        return result;
    },
    scrollBackToTop: function () {
        $(document).scrollTop(0);
    },
    changeSubmitText: function (currentForm, text) {
        $('#' + currentForm).find('[id$="btnSubmit"]').val(text);
    }
}