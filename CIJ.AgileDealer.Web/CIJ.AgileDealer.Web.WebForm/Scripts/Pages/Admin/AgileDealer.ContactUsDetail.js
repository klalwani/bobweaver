﻿$(document).ready(function () {
    contactUsDetail.initialize();
});

var contactUsDetail = {
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/ContactUsController.ashx?method=' + 'GetContactUsDetail';
    },
    initialize: function () {
        contactUsDetail.loadContactUsData();
        contactUsDetail.registerEvent();
    },
    registerEvent: function () {
        $('#btnBack').click(function () {
            parent.history.back();
            return false;
        });
    },
    mapDataForContactUsDetail: function(data) {
        if (typeof data != "undefined" && data != null) {
            $('#firstName').val(data.FirstName).prop('readonly',true);
            $('#lastName').val(data.LastName).prop('readonly', true);
            $('#email').val(data.Email).prop('readonly', true);
            $('#phoneNumber').val(data.PhoneNumber).prop('readonly', true);
            $('#userMessage').val(data.Questions).prop('readonly', true);
        }
    },
    loadContactUsData: function () {
        var data = $('[id$="contactusId"]').val();
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: contactUsDetail.url(),
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        contactUsDetail.mapDataForContactUsDetail(dataResult.aaData);
                    } else {
                        alert("Something went wrong when getting this customer info.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    }
};