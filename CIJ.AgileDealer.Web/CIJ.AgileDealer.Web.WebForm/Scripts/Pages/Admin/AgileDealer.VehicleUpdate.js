﻿function CommaFormatted(nStr) {
    
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
   
        
    return nStr;
}



$(document).ready(function () {
    

    agilevehicle.initialize();
    AgileDealerValidation.InitialValidationEvent();
});
var vehicleTemp;

var agilevehicle = {
    self: 'vehicle-update',
    currentVehicle: function() {
        return vehicleTemp;
    },
    initialize: function () {
        
        agilevehicle.enableDatePickerControl();
        agilevehicle.registerEvent();
        agilevehicle.getVehicleData();

    },
    enableDatePickerControl: function () {
        $('#priceDateStart,#priceDateEnd').datepicker(
        {
            dateFormat: 'mm/dd/yy',
            minDate: 0,
            beforeShowDay: function (date) {
                var day = date.getDay();
                return [(day != 0), ''];
            },
            onSelect: function (dateText, inst) {
                var that = $(this);
                that.blur();
            }
        });
    },
    getVehicleData: function () {
        
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'GetVehicle',
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: $('[id$="VinHdf"]').val(),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    vehicleTemp = dataResult.vehicle;
                    agilevehicle.mapDataForVehicle(dataResult.vehicle);
                    
                    $.unblockUI(siteMaster.loadingWheel());
                }
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert("Error when contacting to server, please try again later!");
            }
        });
    },
    getModifiedData: function () {
        
        var MSRP = $('#msrp').val().replace(/,/gi, "").replace("$", "");

        var SellingPrice = $('#sellingPrice').val().replace(/,/gi, "").replace("$", "");
        var UpfittedCost = $('#upfittedCost').val().replace(/,/gi, "").replace("$", "");

        var MSRPOverride = $('#msrpoverride').val().replace(/,/gi, "").replace("$", "");
        var EmployeePrice = $('#employeeprice').val().replace(/,/gi, "").replace("$", "");

        var DiscountOverride = $('#discountoverride').val().replace(/,/gi, "").replace("$", "");

        var price = $('#overridePrice').val().replace(/,/gi, "").replace("$", "");
        if (typeof DiscountOverride != "undefined" && DiscountOverride !== '' && DiscountOverride !== 0) {

            if (typeof MSRPOverride != "undefined" && MSRPOverride !== '' && MSRPOverride !== 0) {

                price = MSRPOverride - DiscountOverride;
                if (price < 0)
                    price = 0;
            }
            else if (typeof MSRP != "undefined" && MSRP !== '' && MSRP !== 0) {

                price = MSRP - DiscountOverride;
                if (price < 0)
                    price = 0;
            }
            if (price == 0)
            {
                if (SellingPrice > 0)
                {
                    price = SellingPrice - DiscountOverride;
                }
            }
        }
        //else {
        //    if (typeof MSRPOverride != "undefined" && MSRPOverride !== '' && MSRPOverride !== 0) {

        //        price = MSRPOverride;

        //    }
        //    else if (typeof MSRP != "undefined" && MSRP !== '' && MSRP !== 0) {

        //        price = MSRP;
        //    }

        //}
        var vehicle = {
            Vin: $('#vin').val(),
            IsSpecial: $('[name="isSpecial"]').is(':checked'),
            IsRentalSpecial: $('[name="isRentalSpecial"]').is(':checked'),
            HideIncentives: $('[name="hideIncentives"]').is(':checked'),
            IsLift: $('[name="isLift"]').is(':checked'),
            IsUpfitted: $('[name="isUpfitted"]').is(':checked'),
            IsHidden: $('[name="isHidden"]').is(':checked'),
            OverrideDescription: $('#overrideDescription').val(),
            AutoWriterDescription: $('#autoWriterDescription').val(),
              UpfittedDescription: $('#upfittedDescription').val()
        };

        if (typeof price != "undefined" && price !== '' && price !== 0) {
            vehicle.OverridePrice = price;
            vehicle.OverrideStartDate = $('#priceDateStart').val();
            vehicle.OverrideEndDate = $('#priceDateEnd').val();

            var isSelectedMode = $('#priceDate').find('label.active');
            if ($(isSelectedMode).data('toggle') == 'in') {
                vehicle.HasSetDateRange = true;
            } else {
                vehicle.HasSetDateRange = false;
                vehicle.OverrideStartDate = null;
                vehicle.OverrideEndDate = null;
            }
        } else {
            vehicle.OverridePrice = 0;
            vehicle.OverrideStartDate = null;
            vehicle.OverrideEndDate = null;
            vehicle.HasSetDateRange = false;
        }
        if (typeof MSRPOverride != "undefined" && MSRPOverride !== '' && MSRPOverride !== 0) {

            vehicle.MSRPOverride = MSRPOverride;
        }
        if (typeof EmployeePrice != "undefined" && EmployeePrice !== '' && EmployeePrice !== 0) {

            vehicle.EmployeePrice = EmployeePrice;
        }
        vehicle.UpfittedCost = UpfittedCost;
        return vehicle;
    },
    submitModifiedData: function () {
        var data = agilevehicle.getModifiedData();
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'UpdateVehicle',
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        alert("You have updated this vehicle successfully!");
                        $('#discountoverride').val("");
                        agilevehicle.getVehicleData();
                    } else {
                        alert("Something went wrong when updating this vehicle.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    mapDataForVehicle: function (vehicle) {
        if (typeof vehicle != "undefined" && vehicle != null) {
            $('#vehicleTitle').text(vehicle.Year + " " + vehicle.Model + " " + vehicle.Trim);
            $('#vin').val(vehicle.Vin);
            $('#isSpecial').attr('checked', vehicle.IsSpecial);
            $('#isRentalSpecial').attr('checked', vehicle.IsRentalSpecial);
            $('#vehicleImage').attr('src', vehicle.ImageFile);
            $('#hideIncentives').attr('checked', vehicle.HideIncentives);
            $('#isLift').attr('checked', vehicle.IsLift);
            $('#isUpfitted').attr('checked', vehicle.IsUpfitted);
            $('#isHidden').attr('checked', vehicle.IsHidden);
            $('#msrp').val(CommaFormatted(vehicle.MSRP));
            if (vehicle.MSRPOverride == 0)
            {
                $('#msrpoverride').val("");
            }
            else
                $('#msrpoverride').val(CommaFormatted(vehicle.MSRPOverride));

            if (vehicle.EmployeePrice == 0) {
                $('#employeeprice').val("");
            }
            else
                $('#employeeprice').val(CommaFormatted(vehicle.EmployeePrice));


            $('#sellingPrice').val(CommaFormatted(vehicle.NonEditedSellingPrice));
            $('#discount').val(CommaFormatted(vehicle.DealerSaving));
            var price = vehicle.OverridePrice;
            if (typeof price != "undefined" && price != null && price !== 0) {
                $('#overridePrice').val(CommaFormatted(price));
            }


            $('#overrideDescription').val(vehicle.OverrideDescription);
            $('#autoWriterDescription').val(vehicle.AutowriterDescription);
            $('#upfittedDescription').val(vehicle.UpfittedDescription);
            var upfittedCost = vehicle.UpfittedCost;
            if (typeof upfittedCost != "undefined" && upfittedCost != null && upfittedCost !== 0) {
                $('#upfittedCost').val(CommaFormatted(upfittedCost));
            }
            if (vehicle.HasSetDateRange) {
                agilevehicle.setOverrideExperationButton('setDate');
                $('#priceDateStart').datepicker('setDate', new Date(vehicle.OverrideStartDate));
                $('#priceDateEnd').datepicker('setDate', new Date(vehicle.OverrideEndDate));
            }
        }
    },
    setOverrideExperationButton: function(name) {
        $('#priceDate').find('label[name="' + name + '"]').click();
    },
    registerEvent: function () {
        //$('#overridePrice').on('change', function() {
        //    var that = $(this);
        //    var currentText = that.val();
        //    var positiveValue = Math.abs(currentText);
        //    if (typeof positiveValue == "undefined" || isNaN(positiveValue) || positiveValue == 0) {
        //        that.val('');
        //    } else {
        //        that.val(Math.abs(currentText));
        //    }
        //});

        $('#btnUpdateVehicle').on('click', null, null, function (e) {
            siteMaster.touchUpRequireFields(agilevehicle.self);
            if (siteMaster.checkValidationOfCurrentForm(agilevehicle.self))
                agilevehicle.submitModifiedData(agilevehicle.self);
            else {
                alert('Please input all required information!');
            }
        });

        $('#priceDate').on('click', 'label', null, function (e) {
            var that = $(this);
            var toggleState = that.data('toggle');
            var toggleControl = that.attr('href');
            if (toggleState == 'in') {
                $(toggleControl).removeClass('collapse');
                agilevehicle.setRequireFieldForDateRange(true);
            } else {
                $(toggleControl).addClass('collapse');
                agilevehicle.setRequireFieldForDateRange(false);
            }
        });

        $('#btnBack').on('click', null, null, function (e) {
            parent.history.back();
            return false;
        });
    },
    setRequireFieldForDateRange: function (isRequired) {
        if (isRequired) {
            $('#priceDateStart').attr('data-val', true).attr('data-val-required', true);
            $('#priceDateEnd').attr('data-val', true).attr('data-val-required', true);
        } else {
            $('#priceDateStart').removeAttr('data-val').removeAttr('data-val-required').removeClass('input-validation-error');
            $('#priceDateEnd').removeAttr('data-val').removeAttr('data-val-required').removeClass('input-validation-error');
        }
    }
};

