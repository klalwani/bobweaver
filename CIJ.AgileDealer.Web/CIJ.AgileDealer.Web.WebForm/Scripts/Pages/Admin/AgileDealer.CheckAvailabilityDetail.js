﻿$(document).ready(function () {
    checkAvailabilityDetail.initialize();
});

var checkAvailabilityDetail = {
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/CheckAvailabilityController.ashx?method=' + 'GetCheckAvailabilityDetail';
    },
    initialize: function () {
        checkAvailabilityDetail.loadAvailabilityDetailData();
        checkAvailabilityDetail.registerEvent();
    },
    registerEvent: function () {
        $('#btnBack').click(function () {
            parent.history.back();
            return false;
        });
    },
    mapDataForcheckAvailabilityDetail: function(data) {
        if (typeof data != "undefined" && data != null) {
            $('#custName').val(data.Name).prop('readonly',true);
            $('#email').val(data.Email).prop('readonly', true);
            $('#phoneNumber').val(data.Phone).prop('readonly', true);
            $('#vin').val(data.Vin).prop('readonly', true);
            $('#dealerName').val(data.DealerName).prop('readonly', true);
            $('#userMessage').val(data.Message).prop('readonly', true);
        }
    },
    loadAvailabilityDetailData: function () {
        var data = $('[id$="vehicleFeedbackId"]').val();
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: checkAvailabilityDetail.url(),
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        checkAvailabilityDetail.mapDataForcheckAvailabilityDetail(dataResult.aaData);
                    } else {
                        alert("Something went wrong when getting this customer info.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    }
};