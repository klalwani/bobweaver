﻿$(document).ready(function () {
    notificationSetting.initialize();
    AgileDealerValidation.InitialValidationEvent();
});
var emailRegex = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
var notificationSetting = {
    data: [],
    self: 'notificationSetting',
    initialize: function () {
        notificationSetting.registerEvents();
        notificationSetting.getNotificationSettings();
    },
    registerEvents: function () {
        $('#btnSubmit').click(function (e) {
            siteMaster.touchUpRequireFields(notificationSetting.self);
            if (siteMaster.checkValidationOfCurrentForm(notificationSetting.self)) {
                notificationSetting.saveNotificationSettings();
            } else {
                alert('Please input all required information!');
            }
        });

        $('#' + notificationSetting.self).on('click', 'input[type="checkbox"]', function () {
            var that = $(this);
            var parent = that.closest('.rowElem');
            var value = that.attr('value');
            that.prop('checked', true);
            parent.find('input[type="checkbox"]:not([value=' + value + '])').prop('checked', false);
        });
    },
    getDataForSubmitting: function () {
        for (var i = 0; i < notificationSetting.data.length; i++) {
            var currentItem = notificationSetting.data[i];
            var currentSection = $('.' + currentItem.LeadType);
            currentItem.AdfEmail = currentSection.find('#txt' + currentItem.LeadType + 'Adf').val();
            currentItem.PlainTextEmail = currentSection.find('#txt' + currentItem.LeadType + 'Plain').val();
            currentItem.IsAdf = currentSection.find('input[type="checkbox"]:checked').attr('value');
        }

        return notificationSetting.data;
    },
    mappingDataForNotificationSettings: function (data) {
        if (typeof data != "undefined" && data != null) {
            for (var i = 0; i < NOTIFICATIONSETTING.length; i++) {
                var currentLeadType = NOTIFICATIONSETTING[i];
                var currentSection = $('.' + currentLeadType);
                var item = notificationSetting.getCurrentSettingData(data, currentLeadType);
                if (typeof item != "undefined" && item != null) {
                    currentSection.find('#txt' + currentLeadType + 'Adf').val(item.AdfEmail);
                    currentSection.find('#txt' + currentLeadType + 'Plain').val(item.PlainTextEmail);
                    currentSection.find('input[type="checkbox"][value=' + item.IsAdf + ']').prop('checked',true);
                }
            }
        }
    },
    getCurrentSettingData: function(data,leadType) {
      if (typeof data != "undefined" && data != null) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].LeadType == leadType) {
                    return data[i];
                }
            }
        }
    },
    getNotificationSettings: function () {
        var method = 'GetDataForNotificationSetting';
        notificationSetting.submitData(method);
    },
    saveNotificationSettings: function () {
        var method = 'SaveDataForNotificationSetting';
        notificationSetting.submitData(method);
    },
    submitData: function (currentMethod) {
        var result = true;
        var data = {};
        switch (currentMethod) {
            case 'GetDataForNotificationSetting':
                {
                    data = {};
                }
                break;
            case 'SaveDataForNotificationSetting':
                {
                    data = notificationSetting.getDataForSubmitting();
                }
                break;
        }
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/NotificationController.ashx?method=' + currentMethod,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 180000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    $.unblockUI(siteMaster.loadingWheel());
                    if (currentMethod == 'GetDataForNotificationSetting') {
                        notificationSetting.data = dataResult.data;
                        notificationSetting.mappingDataForNotificationSettings(dataResult.data);
                    }
                    else if (currentMethod == 'SaveDataForNotificationSetting') {
                        alert("You have updated the notification settings successfully!");
                    }

                }
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                result = false;
                alert("Error when contacting to server, please try again later!");
            }
        });
    }
}