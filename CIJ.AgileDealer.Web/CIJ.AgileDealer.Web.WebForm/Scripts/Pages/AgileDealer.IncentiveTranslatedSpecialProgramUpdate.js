﻿function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

$(document).ready(function () {
    incentiveTranslatedUpdateSpecialProgram.initialize();
});


var incentiveTranslatedUpdateSpecialProgram = {
    initialize: function () {
        incentiveTranslatedUpdateSpecialProgram.registerEvents();
    },
    isResetAll: false,
    currentTable: {},
    registerEvents: function () {
        $(document).on('click', '#btnSaveChangeSpecialProgram', function () {
            incentiveTranslatedUpdateSpecialProgram.prepareDataForSubmitting();
            incentiveTranslatedUpdateSpecialProgram.submitData();
        });

        $(document).on('click', '#btnResetSpecialProgram', function () {
            $('#modal-confirm-resetSpecialProgram').modal('show');
        });

        $(document).on('click', '#btnConfirmResetSpecialProgram', function () {
            incentiveTranslatedUpdateSpecialProgram.prepareDataForSubmitting();
            incentiveTranslatedUpdateSpecialProgram.resetIncentiveTranslated();
            $('#modal-confirm-resetSpecialProgram').modal('toggle');
        });

        $(document).on('click', '#btnCancelResetSpecialProgram', function () {
            $('#modal-confirm-resetSpecialProgram').modal('toggle');
        });

        $(document).on('click', '#updateForSpecialProgram label', function () {
            var that = $(this);
            var input = that.find('input');
            var value = input.val();
            var target = input.data('target');
            if (target == 'all') {
                $('#vinSectionSpecialProgram').removeClass('in').addClass('collapse');
                $('#modelSectionSpecialProgram').removeClass('in').addClass('collapse');
            }
            else if (target == 'vinSection') {
                $('#vinSectionSpecialProgram').removeClass('collapse').addClass('in');
                $('#modelSectionSpecialProgram').removeClass('in').addClass('collapse');
            }
            else if (target == 'modelSection') {
                $('#vinSectionSpecialProgram').removeClass('in').addClass('collapse');
                $('#modelSectionSpecialProgram').removeClass('collapse').addClass('in');
            }
        });

        $(document).on('shown.bs.modal', '#incentiveEditSpecialProgramControl', function () {
            if (typeof incentiveTranslatedUpdateSpecialProgram.programData != "undefined" && incentiveTranslatedUpdateSpecialProgram.programData != null) {
                $('#lblProgramNameSpecialProgram').text(incentiveTranslatedUpdateSpecialProgram.programData.name);
                if (incentiveTranslatedUpdateSpecialProgram.programData.displaySection != '0' && incentiveTranslatedUpdateSpecialProgram.programData.displaySection != '')
                    $('#ddlDisplaySectionSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.displaySection);
                $('input[name="chkStatusSpecialProgram"][value="' + incentiveTranslatedUpdateSpecialProgram.programData.status + '"]').click();

                $('#CashCouponAmountSpecialProgram').removeClass('collapse').addClass('in');
                $('#DisclaimerSpecialProgram').removeClass('collapse').addClass('in');
                $("#txtCashCouponAmountSpecialProgram").val(CommaFormatted(incentiveTranslatedUpdateSpecialProgram.programData.amount).replace("$", ""));
                $("#txtCashCouponPercentageSpecialProgram").val(CommaFormatted(incentiveTranslatedUpdateSpecialProgram.programData.amount).replace("$", ""));
                $("#txtDisclaimerSpecialProgram").val(incentiveTranslatedUpdateSpecialProgram.programData.disclaimer);
                $('#txtDateStart').val(incentiveTranslatedUpdateSpecialProgram.programData.startdate);
                $('#txtDateEnd').val(incentiveTranslatedUpdateSpecialProgram.programData.enddate);
                // $('#modelSection').removeClass('in').addClass('collapse');




                $('#txtPositionSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.position);
                $('#txtTranslatedNameSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.translatedName);

                if (incentiveTranslatedUpdateSpecialProgram.programData.updateFor == UPDATEFOR.VIN) {
                    $('#txtVinSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.updateForVin);
                    $('#updateForSpecialProgram label input[value="2"]').click();
                }
                else if (incentiveTranslatedUpdateSpecialProgram.programData.updateFor == UPDATEFOR.MODEL) {
                    $("#ddlModelSpecialProgram").unbind("change");
                    $('#ddlModelSpecialProgramYear').unbind("change");
                    $('#ddlModelSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId);
                    $('#updateForSpecialProgram label input[value="3"]').click();

                    var method = "GetModelSpecificData";
                    $.ajax({
                        url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                        type: 'POST',
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        cache: false,
                        timeout: 30000,
                        data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId),
                        success: function (dataResult) {


                            $('#ddlEnginesSpecialProgram').empty();
                            $('#ddlExcludeTrimsSpecialProgram').empty();
                            $('#ddlExcludeBodyStyleSpecialProgram').empty();
                            $('#ddlExcludeYearsSpecialProgram').empty();
                            $('#ddlVinSpecialProgram').empty();
                            $('#ddlModelSpecialProgramYear').empty();
                            $('#ddlModelSpecialProgramStyles').empty();

                            var option = '<option value="0">Select Year(s)</option>';
                            $('#ddlExcludeYearsSpecialProgram').append(option);

                            option = '<option value="">StyleID</option>';
                            $('#ddlModelSpecialProgramStyles').append(option);


                            option = '<option value="0">Years</option>';
                            $('#ddlModelSpecialProgramYear').append(option);


                            option = '<option value="-1">Select Engine(s)</option>';
                            $('#ddlEnginesSpecialProgram').append(option);

                            option = '<option value="0">Select Trim(s)</option>';
                            $('#ddlExcludeTrimsSpecialProgram').append(option);

                            option = '<option value="0">Select BodyStyle(s)</option>';
                            $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                            option = '<option value="0">Select Vin(s)</option>';
                            $('#ddlVinSpecialProgram').append(option);

                            var engines = dataResult.filterModelSpecicData.EnginesList;

                            for (var i = 0; i < engines.length; i++) {
                                var engine = engines[i];
                                option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';

                                $('#ddlEnginesSpecialProgram').append(option);
                            }

                            $("#ddlEnginesSpecialProgram").multiselect('reload');

                            var trims = dataResult.filterModelSpecicData.TrimList;

                            for (var i = 0; i < trims.length; i++) {
                                var trimDetail = trims[i];
                                option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
                                $('#ddlExcludeTrimsSpecialProgram').append(option);

                            }
                            $("#ddlExcludeTrimsSpecialProgram").multiselect('reload');
                            var bodystyles = dataResult.filterModelSpecicData.BodyStyleList;

                            for (var i = 0; i < bodystyles.length; i++) {
                                var bodyStyleDetail = bodystyles[i];
                                option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
                                $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                            }
                            $("#ddlExcludeBodyStyleSpecialProgram").multiselect('reload');

                            var years = dataResult.filterModelSpecicData.YearList;

                            for (var i = 0; i < years.length; i++) {
                                var yearDetail = years[i];
                                option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                                $('#ddlExcludeYearsSpecialProgram').append(option);
                                $('#ddlModelSpecialProgramYear').append(option);
                            }
                            $("#ddlExcludeYearsSpecialProgram").multiselect('reload');
                            var vins = dataResult.filterModelSpecicData.VinList;
                            for (var i = 0; i < vins.length; i++) {
                                var vin = vins[i];
                                option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
                                $('#ddlVinSpecialProgram').append(option);
                            }
                            $("#ddlVinSpecialProgram").multiselect('reload');

                            var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                            for (var i = 0; i < chromeStyles.length; i++) {
                                var chromeStyleDetail = chromeStyles[i];
                                if (incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleId == chromeStyleDetail.IdString) {
                                    option = '<option value="' + chromeStyleDetail.IdString + '" selected="selected">' + chromeStyleDetail.Name + '</option>';
                                }
                                else {
                                    option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';

                                }
                                $('#ddlModelSpecialProgramStyles').append(option);

                            }


                            $('#ddlModelSpecialProgramYear').val(incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleIdYear);

                            method = "FilterYearBasedChromeStyleIds";
                            $.ajax({
                                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleIdYear + "&updateForModelId=" + incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId,
                                type: 'POST',
                                async: true,
                                dataType: "json",
                                contentType: "application/json",
                                cache: false,
                                timeout: 30000,
                                data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData),
                                success: function (dataResultYear) {


                                    $('#ddlModelSpecialProgramStyles').empty();


                                    var option = '<option value="">StyleID</option>';
                                    $('#ddlModelSpecialProgramStyles').append(option);

                                    var chromeStyles = dataResultYear.filterYearBasedChromeStyleIds;

                                    for (var i = 0; i < years.length; i++) {
                                        var chromeStyleDetail = chromeStyles[i];
                                        if (incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleId == chromeStyleDetail.IdString) {
                                            option = '<option value="' + chromeStyleDetail.IdString + '" selected="selected">' + chromeStyleDetail.Name + '</option>';
                                        }
                                        else {
                                            option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';

                                        }
                                        $('#ddlModelSpecialProgramStyles').append(option);
                                    }

                                    $('#ddlModelSpecialProgramStyles').val(incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleId);

                                }
                            });


                          
                            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text("Select Vin(s)");
                            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Vin(s)");
                            $("#engineSectionSpecialProgram .ms-options-wrap button").text("Select Engine(s)");
                            $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Engine(s)");

                            $("#excludeTrimsSpecialProgram .ms-options-wrap button").text("Select Trim(s)");
                            $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", "Select Trim(s)");


                            $("#excludeYearsSpecialProgram .ms-options-wrap button").text("Select Year(s)");
                            $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", "Select Year(s)");
                            $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").text("Select BodyStyle(s)");
                            $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").attr("title", "Select BodyStyle(s)");



                            $(".ms-options ul li").removeClass("selected");
                            $(".ms-options ul li input").addClass("focused");
                            $(".ms-options ul li input").attr('checked', false);
                            $(".ms-options ul li input").prop('checked', false);


                        }
                    });
                }
                else if (incentiveTranslatedUpdateSpecialProgram.programData.updateFor == UPDATEFOR.ALL) {
                    $('#updateForSpecialProgram label input[value="1"]').click();
                }

               



                if (incentiveTranslatedUpdateSpecialProgram.programData.hideOtherIncentives == "True" || incentiveTranslatedUpdateSpecialProgram.programData.hideOtherIncentives == "1") {
                    $("#chkHideAllOtherIncentivesSpecialProgram").prop("checked", true);
                }
                else
                    $("#chkHideAllOtherIncentivesSpecialProgram").prop("checked", false);


                if (incentiveTranslatedUpdateSpecialProgram.programData.isMasterIncentive == "True" || incentiveTranslatedUpdateSpecialProgram.programData.isMasterIncentive == "1") {
                    $("#chkIsMasterIncentiveSpecialProgram").prop("checked", true);
                }
                else
                    $("#chkIsMasterIncentiveSpecialProgram").prop("checked", false);

                if ($("#chkIsMasterIncentiveSpecialProgram").is(":checked")) {
                    $(".isMasterSpecialPrograms").addClass("hide");
                    $(".isMasterSpecialProgramsTab").attr("disabled", "disabled");
                    $(".updateForMasterIncentiveSpecialPrograms").removeAttr("disabled");
                    $('#updateForSpecialProgram label input[value="4"]').click();
                }
                else {
                    $(".isMasterSpecialPrograms").removeClass("hide");
                    $(".isMasterSpecialProgramsTab").removeAttr("disabled");
                    $(".updateForMasterIncentiveSpecialPrograms").attr("disabled", "disabled");
                }


                if (incentiveTranslatedUpdateSpecialProgram.programData.hideDiscounts == "True" || incentiveTranslatedUpdateSpecialProgram.programData.hideDiscounts == "1") {
                    $("#chkHideDealerDiscountsSpecialProgram").prop("checked", true);
                }
                else
                    $("#chkHideDealerDiscountsSpecialProgram").prop("checked", false);

                $("#ddlDiscountTypeSpecialProgram").val(incentiveTranslatedUpdateSpecialProgram.programData.discountType);

                if (incentiveTranslatedUpdateSpecialProgram.programData.discountType == "1") {
                    $("#divCashCouponPercentageSpecialProgram").addClass("hide");
                    $("#divCashCouponAmountSpecialProgram").removeClass("hide");
                }
                else {
                    $("#divCashCouponPercentageSpecialProgram").removeClass("hide");
                    $("#divCashCouponAmountSpecialProgram").addClass("hide");
                }


                var valEngineCodesArr = "0".split(",");
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes != null) {
                    valEngineCodesArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes.split(",");
                }



                var i = 0;
                var size = valEngineCodesArr.length;
                var selectedEngines = "";
                for (i = 0; i < size; i++) {
                    $("#engineSectionSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valEngineCodesArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valEngineCodesArr - 1) {
                                selectedEngines = selectedEngines + $(this).find("input").prop('title');
                            }
                            else
                                selectedEngines = selectedEngines + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedEngines.toString().length > 70) {
                        $("#engineSectionSpecialProgram .ms-options-wrap button").text(selectedEngines.substr(1, 69) + "...");
                        $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", selectedEngines);
                    }
                    if (selectedEngines.toString().length > 1) {

                        $("#engineSectionSpecialProgram .ms-options-wrap button").text(selectedEngines);
                        $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", selectedEngines);
                    }


                }
                $("#ddlEnginesSpecialProgram").val(valEngineCodesArr);

                var valVinArr = "0";
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeVINs != null) {
                    valVinArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeVINs.split(",");
                }



                i = 0;
                var sizeVinArr = valVinArr.length;
                var selectedVins = "";

                for (i = 0; i < sizeVinArr; i++) {
                    valVinArr[i] = $.trim(valVinArr[i]);
                    $("#excludeVinSectionSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valVinArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == sizeVinArr - 1) {
                                selectedVins = selectedVins + $(this).find("input").prop('title');
                            }
                            else
                                selectedVins = selectedVins + $(this).find("input").prop('title') + ",";

                        }
                    });
                    if (selectedVins.toString().length > 70) {
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text(selectedVins.substr(1, 69) + "...");
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", selectedVins);
                    }
                    if (selectedVins.toString().length > 1) {

                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text(selectedVins);
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", selectedVins);
                    }


                }
                $("#ddlVinSpecialProgram").val(valVinArr);



                var valTrimsArr = "0".split(",");
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeTrims != null) {
                    valTrimsArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeTrims.split(",");
                }

                i = 0;
                size = valTrimsArr.length;
                var selectedTrims = "";


                for (i = 0; i < size; i++) {
                    $("#excludeTrimsSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valTrimsArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valTrimsArr - 1) {
                                selectedTrims = selectedTrims + $(this).find("input").prop('title');
                            }
                            else
                                selectedTrims = selectedTrims + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedTrims.toString().length > 70) {
                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").text(selectedTrims.substr(1, 69) + "...");
                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", selectedTrims);
                    }
                    if (selectedTrims.toString().length > 1) {

                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").text(selectedTrims);
                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", selectedTrims);
                    }


                }
                $("#ddlExcludeTrimsSpecialProgram").val(valTrimsArr);


                var valYearsArr = "0".split(",");
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeYear != null) {
                    valYearsArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeYear.split(",");
                }
                i = 0;
                size = valYearsArr.length;
                var selectedYears = "";


                for (i = 0; i < size; i++) {
                    $("#excludeYearsSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valYearsArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valYearsArr - 1) {
                                selectedYears = selectedYears + $(this).find("input").prop('title');
                            }
                            else
                                selectedYears = selectedYears + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedYears.toString().length > 70) {
                        $("#excludeYearsSpecialProgram .ms-options-wrap button").text(selectedYears.substr(1, 69) + "...");
                        $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", selectedYears);
                    }
                    if (selectedYears.toString().length > 1) {

                        $("#excludeYearsSpecialProgram .ms-options-wrap button").text(selectedYears);
                        $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", selectedYears);
                    }


                }
                $("#ddlExcludeYearsSpecialProgram").val(valYearsArr);


                var valBodyStylesArr = "0".split(",");

                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeBodyStyles != null) {
                    valBodyStylesArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeBodyStyles.split(",");
                }

                i = 0;
                size = valBodyStylesArr.length;
                var selectedBodyStyles = "";

                for (i = 0; i < size; i++) {
                    $("#excludeBodyStyleSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valBodyStylesArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valBodyStylesArr - 1) {
                                selectedBodyStyles = selectedBodyStyles + $(this).find("input").prop('title');
                            }
                            else
                                selectedBodyStyles = selectedBodyStyles + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedBodyStyles.toString().length > 70) {
                        $("#excludeBodyStyleSpecialProgram .ms-options-wrap button").text(selectedBodyStyles.substr(1, 69) + "...");
                        $("#excludeBodyStyleSpecialProgram .ms-options-wrap button").attr("title", selectedBodyStyles);
                    }
                    if (selectedBodyStyles.toString().length > 1) {

                        $("#excludeBodyStyleSpecialProgram .ms-options-wrap button").text(selectedBodyStyles);
                        $("#excludeBodyStyleSpecialProgram .ms-options-wrap button").attr("title", selectedBodyStyles);
                    }


                }
                $("#ddlExcludeBodyStyleSpecialProgram").val(valBodyStylesArr);


                if (incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange == 'True') {
                    incentiveManagementSpecialProgram.setOverrideExperationButtonSpecialPrograms('setDate');

                }
                $("#ddlModelSpecialProgram").bind("change", function () {
                    if ($(this).val() == "0") {

                        $('#ddlEnginesSpecialProgram').empty();
                        $('#ddlExcludeTrimsSpecialProgram').empty();
                        $('#ddlExcludeBodyStyleSpecialProgram').empty();
                        $('#ddlExcludeYearsSpecialProgram').empty();
                        $('#ddlVinSpecialProgram').empty();
                        $('#ddlModelSpecialProgramYear').empty();
                        $("#ddlModelSpecialProgramStyles").empty();

                        var option = '<option value="0">Select Year(s)</option>';
                        $('#ddlExcludeYearsSpecialProgram').append(option);

                        option = '<option value="0">Years</option>';
                        $('#ddlModelSpecialProgramYear').append(option);


                        option = '<option value="-1">Select Engine(s)</option>';
                        $('#ddlEnginesSpecialProgram').append(option);

                        option = '<option value="0">Select Trim(s)</option>';
                        $('#ddlExcludeTrimsSpecialProgram').append(option);

                        option = '<option value="0">Select BodyStyle(s)</option>';
                        $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                        option = '<option value="0">Select Vin(s)</option>';
                        $('#ddlVinSpecialProgram').append(option);

                        option = '<option value="">StyleID</option>';
                        $('#ddlModelSpecialProgramStyles').append(option);


                        incentiveManagementSpecialProgram.loadMasterData();

                        $('#ddlEnginesSpecialProgram').multiselect('reload');
                        $('#ddlExcludeTrimsSpecialProgram').multiselect('reload');
                        $('#ddlExcludeBodyStyleSpecialProgram').multiselect('reload');
                        $('#ddlExcludeYearsSpecialProgram').multiselect('reload');
                        $('#ddlVinSpecialProgram').multiselect('reload');


                    }
                    else {
                        var method = "GetModelSpecificData";
                        $.ajax({
                            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            contentType: "application/json",
                            cache: false,
                            timeout: 30000,
                            data: JSON.stringify($(this).val()),
                            success: function (dataResult) {


                                $('#ddlEnginesSpecialProgram').empty();
                                $('#ddlExcludeTrimsSpecialProgram').empty();
                                $('#ddlExcludeBodyStyleSpecialProgram').empty();
                                $('#ddlExcludeYearsSpecialProgram').empty();
                                $('#ddlVinSpecialProgram').empty();
                                $('#ddlModelSpecialProgramYear').empty();
                                $('#ddlModelSpecialProgramStyles').empty();

                                var option = '<option value="0">Select Year(s)</option>';
                                $('#ddlExcludeYearsSpecialProgram').append(option);

                                option = '<option value="">StyleID</option>';
                                $('#ddlModelSpecialProgramStyles').append(option);


                                option = '<option value="0">Years</option>';
                                $('#ddlModelSpecialProgramYear').append(option);


                                option = '<option value="-1">Select Engine(s)</option>';
                                $('#ddlEnginesSpecialProgram').append(option);

                                option = '<option value="0">Select Trim(s)</option>';
                                $('#ddlExcludeTrimsSpecialProgram').append(option);

                                option = '<option value="0">Select BodyStyle(s)</option>';
                                $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                                option = '<option value="0">Select Vin(s)</option>';
                                $('#ddlVinSpecialProgram').append(option);

                                var engines = dataResult.filterModelSpecicData.EnginesList;

                                for (var i = 0; i < engines.length; i++) {
                                    var engine = engines[i];
                                    option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';

                                    $('#ddlEnginesSpecialProgram').append(option);
                                }

                                $("#ddlEnginesSpecialProgram").multiselect('reload');

                                var trims = dataResult.filterModelSpecicData.TrimList;

                                for (var i = 0; i < trims.length; i++) {
                                    var trimDetail = trims[i];
                                    option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
                                    $('#ddlExcludeTrimsSpecialProgram').append(option);

                                }
                                $("#ddlExcludeTrimsSpecialProgram").multiselect('reload');
                                var bodystyles = dataResult.filterModelSpecicData.BodyStyleList;

                                for (var i = 0; i < bodystyles.length; i++) {
                                    var bodyStyleDetail = bodystyles[i];
                                    option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
                                    $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                                }
                                $("#ddlExcludeBodyStyleSpecialProgram").multiselect('reload');

                                var years = dataResult.filterModelSpecicData.YearList;

                                for (var i = 0; i < years.length; i++) {
                                    var yearDetail = years[i];
                                    option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                                    $('#ddlExcludeYearsSpecialProgram').append(option);
                                    $('#ddlModelSpecialProgramYear').append(option);
                                }
                                $("#ddlExcludeYearsSpecialProgram").multiselect('reload');
                                var vins = dataResult.filterModelSpecicData.VinList;
                                for (var i = 0; i < vins.length; i++) {
                                    var vin = vins[i];
                                    option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
                                    $('#ddlVinSpecialProgram').append(option);
                                }
                                $("#ddlVinSpecialProgram").multiselect('reload');

                                var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                                for (var i = 0; i < chromeStyles.length; i++) {
                                    var chromeStyleDetail = chromeStyles[i];
                                    option = '<option value="' + chromeStyleDetail.IdString + '">' + chromeStyleDetail.Name + '</option>';
                                    $('#ddlModelSpecialProgramStyles').append(option);

                                }



                                $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text("Select Vin(s)");
                                $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Vin(s)");
                                $("#engineSectionSpecialProgram .ms-options-wrap button").text("Select Engine(s)");
                                $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Engine(s)");

                                $("#excludeTrimsSpecialProgram .ms-options-wrap button").text("Select Trim(s)");
                                $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", "Select Trim(s)");


                                $("#excludeYearsSpecialProgram .ms-options-wrap button").text("Select Year(s)");
                                $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", "Select Year(s)");
                                $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").text("Select BodyStyle(s)");
                                $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").attr("title", "Select BodyStyle(s)");



                                $(".ms-options ul li").removeClass("selected");
                                $(".ms-options ul li input").addClass("focused");
                                $(".ms-options ul li input").attr('checked', false);
                                $(".ms-options ul li input").prop('checked', false);


                            }
                        });
                    }
                });

                $("#ddlModelSpecialProgramYear").bind("change", function () {
                    if ($(this).val() == "0") {

                        $('#ddlModelSpecialProgramStyles').empty();

                        var option = '<option value="">StyleID</option>';
                        $('#ddlModelSpecialProgramStyles').append(option);
                    }
                    else {
                        method = "FilterYearBasedChromeStyleIds";
                        $.ajax({
                            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + $(this).val() + "&updateForModelId=" + $("#ddlModelSpecialProgram").val(),
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            contentType: "application/json",
                            cache: false,
                            timeout: 30000,
                            data: JSON.stringify($(this).val()),
                            success: function (dataResultYear) {


                                $('#ddlModelSpecialProgramStyles').empty();


                                var option = '<option value="">StyleID</option>';
                                $('#ddlModelSpecialProgramStyles').append(option);

                                var chromeStyles = dataResultYear.filterYearBasedChromeStyleIds;

                                for (var i = 0; i < chromeStyles.length; i++) {
                                    var chromeStyleDetail = chromeStyles[i];
                                    option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';
                                    $('#ddlModelSpecialProgramStyles').append(option);
                                }
                            }
                        });
                    }
                });

            }
        });
    },
    prepareDataForSubmitting: function () {
        incentiveTranslatedUpdateSpecialProgram.programData.id = incentiveTranslatedUpdateSpecialProgram.programData.id;
        incentiveTranslatedUpdateSpecialProgram.programData.displaySection = $('#ddlDisplaySectionSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.position = $('#txtPositionSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.translatedName = $('#txtTranslatedNameSpecialProgram').val();
        if ($('input[name="chkStatusSpecialProgram"]:checked')[0].value == "Hidden") {
            incentiveTranslatedUpdateSpecialProgram.programData.isActive = false;
        } else {
            incentiveTranslatedUpdateSpecialProgram.programData.isActive = true;
        }

        incentiveTranslatedUpdateSpecialProgram.programData.updateFor = $('#updateForSpecialProgram label.active input').val();
        incentiveTranslatedUpdateSpecialProgram.programData.updateForVin = $('#txtVinSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId = $('#ddlModelSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleId = $('#ddlModelSpecialProgramStyles').val();
        incentiveTranslatedUpdateSpecialProgram.programData.chromeStyleIdYear = $('#ddlModelSpecialProgramYear').val();

        incentiveTranslatedUpdateSpecialProgram.programData.isResetAll = incentiveTranslatedUpdateSpecialProgram.isResetAll;

        incentiveTranslatedUpdateSpecialProgram.programData.isCashCoupon = true;


        if ($("#ddlDiscountTypeSpecialProgram").val() == "1") {
            incentiveTranslatedUpdateSpecialProgram.programData.amount = $("#txtCashCouponAmountSpecialProgram").val().replace(/,/gi, "").replace("$", "");

        }
        else {
            incentiveTranslatedUpdateSpecialProgram.programData.amount = $("#txtCashCouponPercentageSpecialProgram").val().replace(/,/gi, "");
        }

        incentiveTranslatedUpdateSpecialProgram.programData.discountType = $("#ddlDiscountTypeSpecialProgram").val();

        if ($("#chkHideAllOtherIncentivesSpecialProgram").prop("checked") == "true" || $("#chkHideAllOtherIncentivesSpecialProgram").prop("checked") == "True" || $("#chkHideAllOtherIncentivesSpecialProgram").is(":checked")) {
            incentiveTranslatedUpdateSpecialProgram.programData.hideOtherIncentives = "true";
        }
        else {
            incentiveTranslatedUpdateSpecialProgram.programData.hideOtherIncentives = "false";
        }

        if ($("#chkIsMasterIncentiveSpecialProgram").prop("checked") == "true" || $("#chkIsMasterIncentiveSpecialProgram").prop("checked") == "True" || $("#chkIsMasterIncentiveSpecialProgram").is(":checked")) {
            incentiveTranslatedUpdateSpecialProgram.programData.isMasterIncentive = "true";
        }
        else {
            incentiveTranslatedUpdateSpecialProgram.programData.isMasterIncentive = "false";
        }

        if ($("#chkHideDealerDiscountsSpecialProgram").prop("checked") == "true" || $("#chkHideDealerDiscountsSpecialProgram").prop("checked") == "True" || $("#chkHideDealerDiscountsSpecialProgram").is(":checked")) {
            incentiveTranslatedUpdateSpecialProgram.programData.hideDiscounts = "true";
        }
        else {
            incentiveTranslatedUpdateSpecialProgram.programData.hideDiscounts = "false";
        }



    //    incentiveTranslatedUpdateSpecialProgram.programData.amount = $("#txtCashCouponAmountSpecialProgram").val().replace(/,/gi, "").replace("$", "");
        incentiveTranslatedUpdateSpecialProgram.programData.disclaimer = $("#txtDisclaimerSpecialProgram").val();
        incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes = $('#ddlEnginesSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.excludeVINArrayData = $('#ddlVinSpecialProgram').val();


        incentiveTranslatedUpdateSpecialProgram.programData.excludeYearsArrayData = $('#ddlExcludeYearsSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.excludeTrims = $('#ddlExcludeTrimsSpecialProgram').val();

      
        incentiveTranslatedUpdateSpecialProgram.programData.excludeBodyStyles = $('#ddlExcludeBodyStyleSpecialProgram').val();



        var isSelectedMode = $('#priceDateSpecialPrograms').find('label.active');
        if ($(isSelectedMode).data('toggle') == 'in') {
            incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange = true;
            incentiveTranslatedUpdateSpecialProgram.programData.startdate = $("#txtDateStart").val();
            incentiveTranslatedUpdateSpecialProgram.programData.enddate = $("#txtDateEnd").val();

        } else {
            incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange = false;
            incentiveTranslatedUpdateSpecialProgram.programData.startdate = null;
            incentiveTranslatedUpdateSpecialProgram.programData.enddate = null;
        }

    },
    submitData: function () {
        var method = 'UpdateIncentiveTranslatedSpecialProgram';
        var isValid = true;
        //if (incentiveTranslatedUpdateSpecialProgram.programData.amount == "") {
        //    $("#txtCashCouponAmountSpecialProgram").addClass("input-validation-error");
        //    isValid = false;
        //}
        if (incentiveTranslatedUpdateSpecialProgram.programData.displaySection == "") {
            $("#ddlDisplaySectionSpecialProgram").addClass("input-validation-error");
            isValid = false;
        }
        if (incentiveTranslatedUpdateSpecialProgram.programData.translatedName == "") {
            $("#txtTranslatedNameSpecialProgram").addClass("input-validation-error");
            isValid = false;
        }

        if (isValid) {

            var result = true;
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 30000,
                data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        result = true;
                        if (typeof incentiveTranslatedUpdateSpecialProgram.currentTable != 'undefined' && incentiveTranslatedUpdateSpecialProgram.currentTable != null)
                            $('.modal').modal('hide');
                        incentiveTranslatedUpdateSpecialProgram.currentTable.ajax.reload();
                    }
                }
            });
        }
        else {
            // $('.modal').modal('hide');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            alert("Please input all required information!");
            document.getElementById('incentiveEditSpecialProgramControl').style.display = "block";
            return false;
        }
    },
    resetIncentiveTranslated: function () {
        var method = 'ResetIncentiveTranslatedSpecialProgram';
        var isResetAll = false;

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdateSpecialProgram.currentTable != 'undefined' && incentiveTranslatedUpdateSpecialProgram.currentTable != null)
                        $('.modal').modal('hide');
                    incentiveTranslatedUpdateSpecialProgram.currentTable.ajax.reload();
                }
            }
        });
    }
};