﻿var ContactUs = {
    currentForm: ".contact-us",
    dealerName: function () { return $('#dealerName').val(); },
    localizations: {},
    initialize: function () {
        ContactUs.registerEvent();
        AgileDealerValidation.InitialValidationEvent();
        ContactUs.clearTextBox();
        ContactUs.localizations = common.getLocalizations();
    },
    registerEvent: function () {
        //$('#MainContent_UCContactUs_btnSubmit').click(function (e) {
        //    ContactUs.blurAllControl(ContactUs.currentForm);
        //    if (!ContactUs.checkValidationOfCurrentForm(ContactUs.currentForm)) {
        //        e.preventDefault();
        //    }
        //});

        $('[id$="btnSubmit"]').click(function (e) {
            e.preventDefault();
            ContactUs.blurAllControl(ContactUs.currentForm);
            if (ContactUs.checkValidationOfCurrentForm(ContactUs.currentForm)) {
                ContactUs.submitDataForContactUs();
                $('#ucContact').dialog('close');
            }
        });

        $('#btnCloseContact').click(function (e) {
            $('#ucContact').dialog('close');
        });
    },
    submitDataForContactUs: function () {
        var method = 'SubmitContactUs';
        var data = {
            firstName: $('[id$="txtFirstName"]').val(),
            lastName: $('[id$="txtLastName"]').val(),
            email: $('[id$="txtEmail"]').val(),
            phoneNumber: $('[id$="txtPhoneNumber"]').val(),
            postalCode: $('[id$="txtPostalCode"]').val(),
            question: $('[id$="txtQuestionComment"]').val()
        };

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SiteMasterController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    ContactUs.clearTextBox();
                    var successMessage = ContactUs.localizations["SuccessMessage"];
                    successMessage = successMessage.format(ContactUs.dealerName());
                    alert(successMessage);
                }
            }
        });
    },
    checkValidationOfCurrentForm: function (currentForm) {
        return $(currentForm).find("[data-val=true]:visible:not([type=hidden])").All("valid");
    },
    blurAllControl: function (currentForm) {
        $(currentForm).find("[data-val=true]:visible:not([type=hidden])").blur();
    },
    clearTextBox: function () {
        $('[id$="txtFirstName"]').val('');
        $('[id$="txtLastName"]').val('');
        $('[id$="txtPhoneNumber"]').val('');
        $('[id$="txtEmail"]').val('');
        $('[id$="txtPostalCode"]').val('');
        $('[id$="txtQuestionComment"]').val('');
    },
    removeRedundantAttribute: function (currentForm) {
        $(currentForm).find("[data-val=true]:visible:not([type=hidden])").removeAttr('required');
    },
    initializeMap: function () {
        var mapProp = {
            center: new google.maps.LatLng(40.675877, -76.224874),
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var marker = new google.maps.Marker({
            position: mapProp.center,
            animation: google.maps.Animation.BOUNCE
        });

        marker.setMap(map);
    },
    loadScript: function () {
        var script = document.createElement("script");
        script.id = 'googleMapScrip';
        script.type = "text/javascript";
        script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBdir-3DWdNDER_bzEROToCr1DQr7jDMuA&sensor=false&callback=ContactUs.initializeMap";
        document.body.appendChild(script);
    }
};

$(document).ready(function () {
    ContactUs.initialize();
    ContactUs.removeRedundantAttribute(ContactUs.currentForm);
});

window.onload = function () {
    ContactUs.loadScript();
    $('.clear-20').remove();
    $(".contact-us").lazyload({
        event: "sporty",
        effect: "fadeIn"
    });
    //var imgSource = "https://agiledealer.blob.core.windows.net/gatorford/content/background/ContactUsBackGround.jpg";
    //setTimeout(function () {
    //    $('.contact-us').css({ 'background-image': 'url(' + imgSource + ')', 'background-repeat': 'no-repeat' });
    //    $('.contact-us').attr('data-original', imgSource);
    //    $(".contact-us").trigger("sporty");
    //}, 3000);

};

