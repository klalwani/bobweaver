﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};
function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

$(document).ready(function () {
    incentiveManagementSpecialProgram.initialize();
});

var incentiveManagementTableSpecialProgram;

var incentiveManagementSpecialProgram = {
    self: 'incentiveManagementDashboardSpecialProgram',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadIncentiveManagementDashboardSpecialPrograms';
    },
    programData: {},
    initialize: function () {
        incentiveManagementSpecialProgram.registerEvents();
        incentiveManagementSpecialProgram.loadMasterData();
        incentiveManagementSpecialProgram.loadData();
        
    },
    registerEvents: function () {

        $('#priceDateSpecialPrograms').on('click', 'label', null, function (e) {
            var that = $(this);
            var toggleState = that.data('toggle');
            var toggleControl = that.attr('href');

            if (toggleState == 'in') {
                $('#dateRangeFieldSpecialPrograms').removeClass('collapse');
                // agilevehicle.setRequireFieldForDateRange(true);
            } else {
                $('#dateRangeFieldSpecialPrograms').addClass('collapse');
                //agilevehicle.setRequireFieldForDateRange(false);
            }
        });

        $('#incentiveManagementDashboardSpecialProgram').on('click', '.programEdit,.itemActive,.itemHidden', function () {

            var that = $(this);
            var thisRow = that.closest('tr');
            $('#incentiveEditSpecialProgramControl').modal('show');
            $("#txtVinSpecialProgram").val('');
//            $("#ddlModelSpecialProgram").val('0');
            var rowData = incentiveManagementTableSpecialProgram.row(thisRow).data();
            incentiveManagementSpecialProgram.setProgramData(rowData, that);
        });

        $("#ddlModelSpecialProgram").on("change", function () {
            if ($(this).val() == "0") {

                $('#ddlEnginesSpecialProgram').empty();
                $('#ddlExcludeTrimsSpecialProgram').empty();
                $('#ddlExcludeBodyStyleSpecialProgram').empty();
                $('#ddlExcludeYearsSpecialProgram').empty();
                $('#ddlVinSpecialProgram').empty();
                $('#ddlModelSpecialProgramYear').empty();
                $("#ddlModelSpecialProgramStyles").empty();

                var option = '<option value="0">Select Year(s)</option>';
                $('#ddlExcludeYearsSpecialProgram').append(option);

                option = '<option value="0">Years</option>';
                $('#ddlModelSpecialProgramYear').append(option);


                option = '<option value="-1">Select Engine(s)</option>';
                $('#ddlEnginesSpecialProgram').append(option);

                option = '<option value="0">Select Trim(s)</option>';
                $('#ddlExcludeTrimsSpecialProgram').append(option);

                option = '<option value="0">Select BodyStyle(s)</option>';
                $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                option = '<option value="0">Select Vin(s)</option>';
                $('#ddlVinSpecialProgram').append(option);

                option = '<option value="">StyleID</option>';
                $('#ddlModelSpecialProgramStyles').append(option);


                incentiveManagementSpecialProgram.loadMasterData();

                $('#ddlEnginesSpecialProgram').multiselect('reload');
                $('#ddlExcludeTrimsSpecialProgram').multiselect('reload');
                $('#ddlExcludeBodyStyleSpecialProgram').multiselect('reload');
                $('#ddlExcludeYearsSpecialProgram').multiselect('reload');
                $('#ddlVinSpecialProgram').multiselect('reload');


            }
            else {
                var method = "GetModelSpecificData";
                $.ajax({
                    url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    timeout: 30000,
                    data: JSON.stringify($(this).val()),
                    success: function (dataResult) {


                        $('#ddlEnginesSpecialProgram').empty();
                        $('#ddlExcludeTrimsSpecialProgram').empty();
                        $('#ddlExcludeBodyStyleSpecialProgram').empty();
                        $('#ddlExcludeYearsSpecialProgram').empty();
                        $('#ddlVinSpecialProgram').empty();
                        $('#ddlModelSpecialProgramYear').empty();
                        $('#ddlModelSpecialProgramStyles').empty();

                        var option = '<option value="0">Select Year(s)</option>';
                        $('#ddlExcludeYearsSpecialProgram').append(option);

                        option = '<option value="">StyleID</option>';
                        $('#ddlModelSpecialProgramStyles').append(option);


                        option = '<option value="0">Years</option>';
                        $('#ddlModelSpecialProgramYear').append(option);


                        option = '<option value="-1">Select Engine(s)</option>';
                        $('#ddlEnginesSpecialProgram').append(option);

                        option = '<option value="0">Select Trim(s)</option>';
                        $('#ddlExcludeTrimsSpecialProgram').append(option);

                        option = '<option value="0">Select BodyStyle(s)</option>';
                        $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                        option = '<option value="0">Select Vin(s)</option>';
                        $('#ddlVinSpecialProgram').append(option);

                        var engines = dataResult.filterModelSpecicData.EnginesList;
                        
                        for (var i = 0; i < engines.length; i++) {
                            var engine = engines[i];
                            option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';
                             
                            $('#ddlEnginesSpecialProgram').append(option);
                        }

                        $("#ddlEnginesSpecialProgram").multiselect('reload'); 

                        var trims = dataResult.filterModelSpecicData.TrimList;

                        for (var i = 0; i < trims.length; i++) {
                            var trimDetail = trims[i];
                            option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
                            $('#ddlExcludeTrimsSpecialProgram').append(option);

                        }
                        $("#ddlExcludeTrimsSpecialProgram").multiselect('reload'); 
                        var bodystyles = dataResult.filterModelSpecicData.BodyStyleList;

                        for (var i = 0; i < bodystyles.length; i++) {
                            var bodyStyleDetail = bodystyles[i];
                            option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
                            $('#ddlExcludeBodyStyleSpecialProgram').append(option);

                        }
                        $("#ddlExcludeBodyStyleSpecialProgram").multiselect('reload'); 

                        var years = dataResult.filterModelSpecicData.YearList;

                        for (var i = 0; i < years.length; i++) {
                            var yearDetail = years[i];
                            option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                            $('#ddlExcludeYearsSpecialProgram').append(option);
                            $('#ddlModelSpecialProgramYear').append(option);
                        }
                        $("#ddlExcludeYearsSpecialProgram").multiselect('reload'); 
                        var vins = dataResult.filterModelSpecicData.VinList;
                        for (var i = 0; i < vins.length; i++) {
                            var vin = vins[i];
                            option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
                            $('#ddlVinSpecialProgram').append(option);
                        }
                        $("#ddlVinSpecialProgram").multiselect('reload'); 

                        var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                        for (var i = 0; i < chromeStyles.length; i++) {
                            var chromeStyleDetail = chromeStyles[i];
                            option = '<option value="' + chromeStyleDetail.IdString + '">' + chromeStyleDetail.Name + '</option>';
                            $('#ddlModelSpecialProgramStyles').append(option);

                        }
                      


                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text("Select Vin(s)");
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Vin(s)");
                        $("#engineSectionSpecialProgram .ms-options-wrap button").text("Select Engine(s)");
                        $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Engine(s)");

                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").text("Select Trim(s)");
                        $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", "Select Trim(s)");


                        $("#excludeYearsSpecialProgram .ms-options-wrap button").text("Select Year(s)");
                        $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", "Select Year(s)");
                        $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").text("Select BodyStyle(s)");
                        $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").attr("title", "Select BodyStyle(s)");



                        $(".ms-options ul li").removeClass("selected");
                        $(".ms-options ul li input").addClass("focused");
                        $(".ms-options ul li input").attr('checked', false);
                        $(".ms-options ul li input").prop('checked', false);


                    }
                });
            }
        });

        $("#ddlModelSpecialProgramYear").bind("change", function () {
            if ($(this).val() == "0") {

                $('#ddlModelSpecialProgramStyles').empty();

                var option = '<option value="">StyleID</option>';
                $('#ddlModelSpecialProgramStyles').append(option);
            }
            else {
                method = "FilterYearBasedChromeStyleIds";
                $.ajax({
                    url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + $(this).val() + "&updateForModelId=" + $("#ddlModelSpecialProgram").val(),
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    timeout: 30000,
                    data: JSON.stringify($(this).val()),
                    success: function (dataResultYear) {


                        $('#ddlModelSpecialProgramStyles').empty();


                        var option = '<option value="">StyleID</option>';
                        $('#ddlModelSpecialProgramStyles').append(option);

                        var chromeStyles = dataResultYear.filterYearBasedChromeStyleIds;

                        for (var i = 0; i < chromeStyles.length; i++) {
                            var chromeStyleDetail = chromeStyles[i];
                            option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';
                            $('#ddlModelSpecialProgramStyles').append(option);
                        }
                    }
                });
            }
        });

        $('#btnAddNewSpecialProgramIncentive').on('click', function () {
            
          
           
            $('#incentiveEditSpecialProgramControl').modal('show');

            $('#txtDateStart').datepicker({
                minDate: 0
            });

            $('#txtDateEnd').datepicker({
                minDate: 0,
                defaultDate: "+1M"
            });
           
            $("#txtVinSpecialProgram").val('');
            $("#ddlModelSpecialProgram").val('0');
            $("#ddlEnginesSpecialProgram").val('0');
            $("#ddlExcludeYearsSpecialProgram").val('0');
            $("#ddlExcludeTrimsSpecialProgram").val('0');
            $("#ddlExcludeBodyStyleSpecialProgram").val('0');
            $("#ddlVinSpecialProgram").val('0');
            $('#txtDateStart').val('');
            $('#txtDateEnd').val('');
            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text("Select Vin(s)");
            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Vin(s)");
            $("#engineSectionSpecialProgram .ms-options-wrap button").text("Select Engine(s)");
            $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Engine(s)");

            $("#excludeTrimsSpecialProgram .ms-options-wrap button").text("Select Trim(s)");
            $("#excludeTrimsSpecialProgram .ms-options-wrap button").attr("title", "Select Trim(s)");


            $("#excludeYearsSpecialProgram .ms-options-wrap button").text("Select Year(s)");
            $("#excludeYearsSpecialProgram .ms-options-wrap button").attr("title", "Select Year(s)");
            $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").text("Select BodyStyle(s)");
            $("#excludeBodyStylesSpecialProgram .ms-options-wrap button").attr("title", "Select BodyStyle(s)");



            $(".ms-options ul li").removeClass("selected");
            $(".ms-options ul li input").addClass("focused");
            $(".ms-options ul li input").attr('checked', false);
            $(".ms-options ul li input").prop('checked', false);

            var d = new Date();
            var StartDate = getFormattedDate(d);
            var EndDate = getFormattedDate(new Date(d.getFullYear(), d.getMonth() + 1, d.getDate()));
            $('#txtDateStart').val(EndDate);
            $('#txtDateEnd').val(StartDate);

            var data = {
                name: "",
                type: "Cash",
                isActive: false,
                amount: "",
                disclaimer: "",
                updateFor: 1, // All Vehicles
                updateForVin: '',
                updateForModelId: 0,
                id: 0,
                excludeenginecodes: "",
                excludeVINs: "",
                excludeVINArrayData: "",
                excludeTrimId: "",
                excludeTrims: "",
                excludeYear: "",
                excludeYearsArrayData: "",
                startdate: StartDate,
                enddate: EndDate,
                hassetDateRange: false,
                discountType: 1,
                excludeBodyStyleId: "",
                excludeBodyStyles: "",
            };
            data.status = 'Active';
            data.displaySection = "";
            data.translatedName = "";
            data.position = "";
            data.updateFor = UPDATEFOR.ALL;
            excludeenginecodes = "";
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
            incentiveTranslatedUpdateSpecialProgram.programData = data;

        });

    },
    loadMasterData: function () {
        var modelListSource = $('[id$="ModelList"]').val();
        var models = $.parseJSON(modelListSource);
        for (var i = 0; i < models.length; i++) {
            var model = models[i];
            var option = '<option value="' + model.Id + '">' + model.Name + '</option>';
            $('#ddlModelSpecialProgram').append(option);
        }

        var engineListSource = $('[id$="EngineList"]').val();
        var engines = $.parseJSON(engineListSource);
        for (var i = 0; i < engines.length; i++) {
            var engine = engines[i];
            var option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';
            $('#ddlEnginesSpecialProgram').append(option);
        }

     

        var trimsListSource = $('[id$="TrimList"]').val();
        var trims = $.parseJSON(trimsListSource);

        for (var i = 0; i < trims.length; i++) {
            var trimDetail = trims[i];
            var option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
            $('#ddlExcludeTrimsSpecialProgram').append(option);

        }

        var bodyStyleListSource = $('[id$="BodyStyleList"]').val();
        var bodystyles = $.parseJSON(bodyStyleListSource);

        for (var i = 0; i < bodystyles.length; i++) {
            var bodyStyleDetail = bodystyles[i];
            var option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
            $('#ddlExcludeBodyStyleSpecialProgram').append(option);

        }


        var yearListSource = $('[id$="YearList"]').val();
        var years = $.parseJSON(yearListSource);

        for (var i = 0; i < years.length; i++) {
            var yearDetail = years[i];
            var option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
            $('#ddlExcludeYearsSpecialProgram').append(option);

        }

        var vinListSource = $('[id$="ExcludeVinList"]').val();
        var vins = $.parseJSON(vinListSource);
        for (var i = 0; i < vins.length; i++) {
            var vin = vins[i];
            var option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
            $('#ddlVinSpecialProgram').append(option);
        }
    },

    setProgramData: function (rowData, row) {
        var isActive = 'Active';
        var vin = '';
        var modelid = '';

        var data = {
            //displaySection: rowData[7],
            //status: rowData[1],
            //position: rowData[8],
            //translatedName: rowData[2],
            name: rowData[2],
            type: rowData[3],
            isActive: false,
            amount: rowData[4],
            disclaimer: rowData[11],
            updateFor: 1, // All Vehicles
            updateForVin: '',
            updateForModelId: 0,
            id: rowData[14],
            excludeVINs: rowData[7],
            excludeenginecodes: rowData[10],
            excludeVINArrayData: rowData[7],
            startdate: rowData[8],
            enddate: rowData[9],
            hassetDateRange: rowData[15],
            hideOtherIncentives: rowData[16],
            excludeTrims: rowData[17],
            excludeYear: rowData[18],
            discountType: rowData[19],
            hideDiscounts: rowData[20],
            excludeBodyStyles: rowData[21],
            isMasterIncentive: rowData[22],
            chromeStyleId: rowData[23],
            chromeStyleIdYear: rowData[24]

        };
        data.displaySection = row.attr('displaySection');
        data.translatedName = row.attr('translatedName');
        data.position = row.attr('position');

        if (row.is('.itemActive')) {
            data.status = 'Active';
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
        }
        else if (row.is('.itemHidden')) {
            data.status = 'Hidden';
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
        } else {
            incentiveTranslatedUpdateSpecialProgram.isResetAll = true;
        }

        if (row.attr('isUpdateFor') == 'all') {
            data.updateFor = UPDATEFOR.ALL;
        }
        else if (row.attr('isUpdateFor') == 'vinSection') {
            data.updateFor = UPDATEFOR.VIN;
            data.updateForVin = row.attr('vin');;
        }
        else if (row.attr('isUpdateFor') == 'modelSection') {
            data.updateFor = UPDATEFOR.MODEL;
            data.updateForModelId = row.attr('modelid');
        }


        incentiveTranslatedUpdateSpecialProgram.programData = data;
    },
    setOverrideExperationButtonSpecialPrograms: function (name) {
        $('#priceDateSpecialPrograms').find('label[name="' + name + '"]').click();
    },
    loadData: function () {
        var data = $('#ddlLanguageSpecialProgram').val();
        incentiveManagementTableSpecialProgram = $('#tableIncentiveManagementSpecialProgram').DataTable({
            ajax: {
                url: incentiveManagementSpecialProgram.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function () {
                    return JSON.stringify(data);
                }
            },
            columns: [
                { sName: 'DisplaySection', width: "10%" },
                { sName: 'TranslatedName', width: "10%", sClass: "textLeft" },
                { sName: 'ProgramName', width: "10%", sClass: "textLeft" },
                { sName: 'ProgramType', width: "5%" },
                { sName: 'Amount', sClass: 'breakText textLeft', width: "10%" },
                { sName: 'DiscountTypeFriendlyName', sClass: 'breakText textLeft', width: "10%" },
                { sName: 'ExcludeEngingCodeUserFriendlyName', width: "10%" },
                { sName: 'ExcludeVINs', width: "15%" },
                { sName: 'StartDate', width: "10%" },
                { sName: 'EndDate', width: "10%" },
                { sName: 'excludeenginecodes', "visible": false },
                { sName: 'Disclaimer', "visible": false },
                { sName: 'DisplaySectionId', "visible": false },
                { sName: 'Position', "visible": false },
                { sName: 'Id', "visible": false },
                { sName: 'HasSetDateRange', "visible": false },
                { sName: 'HideOtherIncentives', "visible": false },
                { sName: 'ExcludeTrimID', "visible": false },
                { sName: 'ExcludeYear', "visible": false },
                { sName: 'DiscountType', "visible": false },
                { sName: 'HideDiscounts', "visible": false },
                { sName: 'ExcludeBodyStylesID', "visible": false },
                { sName: 'IsMasterIncentive', "visible": false },
                { sName: 'ChromeStyleId', "visible": false },
                { sName: 'ChromeStyleIdYear', "visible": false },
            ],
            "initComplete": function (settings, json) {
                incentiveTranslatedUpdateSpecialProgram.currentTable = incentiveManagementTableSpecialProgram;
            }
        });

    }
};

function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}
