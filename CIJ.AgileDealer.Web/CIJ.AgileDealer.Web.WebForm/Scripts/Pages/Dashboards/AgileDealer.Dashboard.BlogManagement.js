﻿$(document).ready(function () {
    blogManagement.initialize();
});

var blogManagementTable;

var blogManagement = {
    self: 'blogManagementDashboard',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadBlogManagementDashboard';
    },
    initialize: function () {
        blogManagement.registerEvents();
        blogManagement.loadData();
    },
    registerEvents: function () {
        $('#tableBlogManagement').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = blogManagementTable.row(row).data()[0];
            $('[id$="BlogEntryId"]').val(id);
            $("#blog-update").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Edit Blog',
                show: { effect: "fade", duration: 100 },
                position: { my: "center", at: "center", collison: 'fit' },
                width: "1170px",
            });
        });

        $('#btnAddNewBlog').click(function (e) {
            $('[id$="BlogEntryId"]').val(0);
            siteMaster.clearAllInput('blog-update');
            siteMaster.removeValidationError('blog-update');
            $("#blog-update").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Add New Blog',
                show: { effect: "fade", duration: 100 },
                position: { my: "center", at: "center", collison: 'fit' },
                width: "1170px",
            });
        });
    },
    loadData: function () {
        var data = $('#ddlLanguage').val();
        blogManagementTable = $('#tableBlogManagement').DataTable({
            ajax: {
                url: blogManagement.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function() {
                    return JSON.stringify(data);
                }
            },
            //stateSave: true,
            //stateDuration: 60 * 60,
            columns: [
                { sName: 'BlogEntryId', "visible": false },
                { sName: 'BlogTitile' },
                { sName: 'BlogCategory' },
                { sName: 'BlogTag' },
                { sName: 'DateCreated' },
                {
                    sName: 'BlogEntryId',
                    defaultContent: '<a href="" class="editor_edit">Detail</a>'
                }
            ],
            order: [[4, "desc"]]
        });
    }
};