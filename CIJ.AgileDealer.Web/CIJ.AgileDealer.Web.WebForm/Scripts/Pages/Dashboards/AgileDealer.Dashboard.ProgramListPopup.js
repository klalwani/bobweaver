﻿var METHOD = {
    Active: true,
    InActive: false
};

$(document).ready(function () {
    programList.initialize();
});

var programListTable;

var programList = {
    isRunningAsPopup: function () {
        return true;
    },
    populateMasterData: function () {

    },
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/VehicleController.ashx?method=' + 'GetProgramList';
    },
    initialize: function () {
        programList.registerEvent();
    },
    registerEvent: function () {
        //$('#tableProgramList').on('click', 'a.editor_active, a.editor_inactive', function (e) {
        //    e.preventDefault();
        //    var that = $(this);
        //    var row = $(this).closest('tr');
        //    var id = programListTable.row(row).data()[0];
        //    var method = 'active';
        //    if (that.hasClass('editor_active')) {
        //        method = METHOD.Active;
        //    }
        //    else if (that.hasClass('editor_inactive')) {
        //        method = METHOD.InActive;
        //    }

        //    var data = programList.getDataForSubmit(row);
        //    data.isActive = method;
        //    data.agileVehicleId = $('[id$="AgileVehicleId"]').val();
        //    programList.submitData(data, 'SetProgramState');

        //});

        $('#btnSelectPrograms').click(function () {

        });
    },
    submitData: function (data, method) {
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/VehicleController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {

                    } else {
                        alert("Something went wrong when updating this program.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                    programListTable.ajax.reload();
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    getDataForSubmit: function (row) {
        var data = {};
        if (typeof row != "undefined" && row != null) {
            data = {
                programId: programListTable.row(row).data()[0],
                name: programListTable.row(row).data()[1],
                type: programListTable.row(row).data()[2],
                id: programListTable.row(row).data()[3],
                disclaimer: programListTable.row(row).data()[4]
            };
        }
        return data;
    },
    loadPrograms: function () {
        var data = {
            nameplateId: $('#nameplateList').val(),
            nameplateTrimId: $('#nameplateTrimList').val()
        };
        if (typeof programListTable != "undefined" && programListTable != null) {
            programListTable.ajax.reload();
        } else {
            programListTable = $('#tableProgramList')
                .DataTable({
                    ajax: {
                        url: programList.url(),
                        type: 'post',
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        cache: false,
                        data: function () {
                            return JSON.stringify(data);
                        }
                    },
                    columns: [
                        { sName: 'ProgramId', "visible": false },
                        {
                            sName: 'Select',
                            defaultContent: '<input type="checkbox" name="selectProgram" title="Select this program!"/>'
                        },
                        { sName: 'Name' },
                        { sName: 'Type' },
                        { sName: 'Disclaimer' },
                        { sName: 'StartDate' },
                        { sName: 'EndDate' },
                        { sName: 'Amount' },
                    ],

                });
        }
    }

};