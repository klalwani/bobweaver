﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};


$(document).ready(function () {
    incentiveManagement.initialize();
});

var incentiveManagementTable;

var incentiveManagement = {
    self: 'incentiveManagementDashboard',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadIncentiveManagementDashboard';
    },
    programData: {},
    initialize: function () {
        incentiveManagement.registerEvents();
        incentiveManagement.loadMasterData();
        incentiveManagement.loadData();
    },
    registerEvents: function () {
        $('#incentiveManagementDashboard').on('click', '.programEdit,.itemActive,.itemHidden', function () {

            var that = $(this);
            var thisRow = that.closest('tr');
            $('#incentiveEditControl').modal('show');
            $("#txtVin").val('');
            $("#ddlModel").val('0');
            var rowData = incentiveManagementTable.row(thisRow).data();
            incentiveManagement.setProgramData(rowData, that);
        });


        $("#ddlModel").on("change", function () {
            if ($(this).val() == "0") {

                option = '<option value="0">Years</option>';
                $('#ddlModelYear').append(option);

                option = '<option value="">StyleID</option>';
                $('#ddlModelStyles').append(option);
            }
            else {
                var method = "GetModelSpecificData";
                $.ajax({
                    url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    timeout: 30000,
                    data: JSON.stringify($(this).val()),
                    success: function (dataResult) {

                        $('#ddlModelYear').empty();
                        $('#ddlModelStyles').empty();

                        var option = '<option value="">StyleID</option>';
                        $('#ddlModelStyles').append(option);


                        option = '<option value="0">Years</option>';
                        $('#ddlModelYear').append(option);


                        var years = dataResult.filterModelSpecicData.YearList;

                        for (var i = 0; i < years.length; i++) {
                            var yearDetail = years[i];
                            option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                            $('#ddlModelYear').append(option);
                        }

                        var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                        for (var i = 0; i < chromeStyles.length; i++) {
                            var chromeStyleDetail = chromeStyles[i];
                            option = '<option value="' + chromeStyleDetail.IdString + '">' + chromeStyleDetail.Name + '</option>';
                            $('#ddlModelStyles').append(option);

                        }
                    }
                });
            }
        });

        $("#ddlModelYear").on("change", function () {
            if ($(this).val() == "0") {

                $('#ddlModelStyles').empty();

                var option = '<option value="">StyleID</option>';
                $('#ddlModelStyles').append(option);
            }
            else {
                method = "FilterYearBasedChromeStyleIds";
                $.ajax({
                    url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + $(this).val() + "&updateForModelId=" + $("#ddlModel").val(),
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    timeout: 30000,
                    data: JSON.stringify($(this).val()),
                    success: function (dataResultYear) {


                        $('#ddlModelStyles').empty();


                        var option = '<option value="">StyleID</option>';
                        $('#ddlModelStyles').append(option);

                        var chromeStyles = dataResultYear.filterYearBasedChromeStyleIds;

                        for (var i = 0; i < chromeStyles.length; i++) {
                            var chromeStyleDetail = chromeStyles[i];
                            option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';
                            $('#ddlModelStyles').append(option);
                        }
                    }
                });
            }
        });


    },
    loadMasterData: function () {
        var modelListSource = $('[id$="ModelList"]').val();
        var models = $.parseJSON(modelListSource);
        for (var i = 0; i < models.length; i++) {
            var model = models[i];
            var option = '<option value="' + model.Id + '">' + model.Name + '</option>';
            $('#ddlModel').append(option);
        }
        
    },



    setProgramData: function (rowData, row) {
        var isActive = 'Active';
        var vin = '';
        var modelid = '';

        var data = {
            //displaySection: rowData[7],
            //status: rowData[1],
            //position: rowData[8],
            //translatedName: rowData[2],
            name: rowData[2],
            type: rowData[3],
            id: rowData[4],
            isActive: false,
            updateFor: 1, // All Vehicles
            updateForVin: '',
            updateForModelId: 0,
            chromeStyleId: rowData[8],
            chromeStyleIdYear: rowData[9]
        };

        data.displaySection = row.attr('displaySection');
        data.translatedName = row.attr('translatedName');
        data.position = row.attr('position');

        if (row.is('.itemActive')) {
            data.status = 'Active';
            incentiveTranslatedUpdate.isResetAll = false;
        }
        else if (row.is('.itemHidden')) {
            data.status = 'Hidden';
            incentiveTranslatedUpdate.isResetAll = false;
        } else {
            incentiveTranslatedUpdate.isResetAll = true;
        }

        if (row.attr('isUpdateFor') == 'all') {
            data.updateFor = UPDATEFOR.ALL;
        }
        else if (row.attr('isUpdateFor') == 'vinSection') {
            data.updateFor = UPDATEFOR.VIN;
            data.updateForVin = row.attr('vin');;
        }
        else if (row.attr('isUpdateFor') == 'modelSection') {
            data.updateFor = UPDATEFOR.MODEL;
            data.updateForModelId = row.attr('modelid');
        }

        incentiveTranslatedUpdate.programData = data;
    },
    loadData: function () {
        var data = $('#ddlLanguage').val();
        incentiveManagementTable = $('#tableIncentiveManagement').DataTable({
            ajax: {
                url: incentiveManagement.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function () {
                    return JSON.stringify(data);
                }
            },
            columns: [
                { sName: 'DisplaySection', width: "10%" },
                { sName: 'TranslatedName', width: "40%", sClass: "textLeft" },
                { sName: 'ProgramName', width: "20%", sClass: "textLeft" },
                { sName: 'ProgramType', width: "5%" },
                { sName: 'ProgramNumber', width: "5%" },
                { sName: 'Amount', sClass: 'breakText textLeft', width: "10%" },
                { sName: 'DisplaySectionId', "visible": false },
                { sName: 'Position', "visible": false },
                { sName: 'ChromeStyleId', "visible": false },
                { sName: 'ChromeStyleIdYear', "visible": false },
                {
                    sName: 'DisplaySectionId',
                    defaultContent: '<span  class="programEdit">Add</a>'
                    , width: "5%"
                }
            ],
            order: [[3, "desc"]],
            "initComplete": function (settings, json) {
                incentiveTranslatedUpdate.currentTable = incentiveManagementTable;
            }
        });
        
    }
};