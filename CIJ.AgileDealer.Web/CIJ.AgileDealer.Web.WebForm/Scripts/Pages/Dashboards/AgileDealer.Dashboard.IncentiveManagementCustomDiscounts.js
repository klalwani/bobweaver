﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};
function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

$(document).ready(function () {
    incentiveManagementCustomDiscounts.initialize();
});

var incentiveManagementTableCustomDiscounts;

var incentiveManagementCustomDiscounts = {
    self: 'incentiveManagementDashboardCustomDiscounts',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadIncentiveManagementCustomDiscounts';
    },
    programData: {},
    initialize: function () {
        incentiveManagementCustomDiscounts.registerEvents();
        incentiveManagementCustomDiscounts.loadMasterData();
        incentiveManagementCustomDiscounts.loadData();
    },
    registerEvents: function () {

        $('#priceDate').on('click', 'label', null, function (e) {
            var that = $(this);
            var toggleState = that.data('toggle');
            var toggleControl = that.attr('href');
            
            if (toggleState == 'in') {
                $('#dateRangeField').removeClass('collapse');
                // agilevehicle.setRequireFieldForDateRange(true);
            } else {
                $('#dateRangeField').addClass('collapse');
                //agilevehicle.setRequireFieldForDateRange(false);
            }
        });

        $('#incentiveManagementDashboardCustomDiscounts').on('click', '.programEdit,.itemActive,.itemHidden', function () {

            var that = $(this);
            var thisRow = that.closest('tr');
            $('#incentiveEditCustomDiscountsControl').modal('show');
            $("#txtVinCustomDiscounts").val('');
            $("#ddlModelCustomDiscounts").val('0');
            var rowData = incentiveManagementTableCustomDiscounts.row(thisRow).data();
            incentiveManagementCustomDiscounts.setProgramData(rowData, that);
        });


        $("#ddlModelCustomDiscounts").on("change", function () {
            if ($(this).val() == "0") {

                $('#ddlEnginesCustomDiscounts').empty();
                $('#ddlExcludeTrimsCustomDiscounts').empty();
                $('#ddlExcludeBodyStyleCustomDiscounts').empty();
                $('#ddlExcludeYearsCustomDiscounts').empty();
                $('#ddlVinCustomDiscounts').empty();


                var option = '<option value="0">Select Year(s)</option>';
                $('#ddlExcludeYearsCustomDiscounts').append(option);

                option = '<option value="-1">Select Engine(s)</option>';
                $('#ddlEnginesCustomDiscounts').append(option);

                option = '<option value="0">Select Trim(s)</option>';
                $('#ddlExcludeTrimsCustomDiscounts').append(option);

                option = '<option value="0">Select BodyStyle(s)</option>';
                $('#ddlExcludeBodyStyleCustomDiscounts').append(option);

                option = '<option value="0">Select Vin(s)</option>';
                $('#ddlVinCustomDiscounts').append(option);

                incentiveManagementCustomDiscounts.loadMasterData();

                $('#ddlEnginesCustomDiscounts').multiselect('reload')
                $('#ddlExcludeTrimsCustomDiscounts').multiselect('reload')
                $('#ddlExcludeBodyStyleCustomDiscounts').multiselect('reload')
                $('#ddlExcludeYearsCustomDiscounts').multiselect('reload')
                $('#ddlVinCustomDiscounts').multiselect('reload')

            }
            else {
                var method = "GetModelSpecificData";
                $.ajax({
                    url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    timeout: 30000,
                    data: JSON.stringify($(this).val()),
                    success: function (dataResult) {


                        $('#ddlEnginesCustomDiscounts').empty();
                        $('#ddlExcludeTrimsCustomDiscounts').empty();
                        $('#ddlExcludeBodyStyleCustomDiscounts').empty();
                        $('#ddlExcludeYearsCustomDiscounts').empty();
                        $('#ddlVinCustomDiscounts').empty();


                        var option = '<option value="0">Select Year(s)</option>';
                        $('#ddlExcludeYearsCustomDiscounts').append(option);

                        option = '<option value="-1">Select Engine(s)</option>';
                        $('#ddlEnginesCustomDiscounts').append(option);

                        option = '<option value="0">Select Trim(s)</option>';
                        $('#ddlExcludeTrimsCustomDiscounts').append(option);

                        option = '<option value="0">Select BodyStyle(s)</option>';
                        $('#ddlExcludeBodyStyleCustomDiscounts').append(option);

                        option = '<option value="0">Select Vin(s)</option>';
                        $('#ddlVinCustomDiscounts').append(option);

                        var engines = dataResult.filterModelSpecicData.EnginesList;

                        for (var i = 0; i < engines.length; i++) {
                            var engine = engines[i];
                            var option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';

                            $('#ddlEnginesCustomDiscounts').append(option);
                        }

                        $("#ddlEnginesCustomDiscounts").multiselect('reload');

                        var trims = dataResult.filterModelSpecicData.TrimList;

                        for (var i = 0; i < trims.length; i++) {
                            var trimDetail = trims[i];
                            var option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
                            $('#ddlExcludeTrimsCustomDiscounts').append(option);

                        }
                        $("#ddlExcludeTrimsCustomDiscounts").multiselect('reload');
                        var bodystyles = dataResult.filterModelSpecicData.BodyStyleList;

                        for (var i = 0; i < bodystyles.length; i++) {
                            var bodyStyleDetail = bodystyles[i];
                            var option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
                            $('#ddlExcludeBodyStyleCustomDiscounts').append(option);

                        }
                        $("#ddlExcludeBodyStyleCustomDiscounts").multiselect('reload');

                        var years = dataResult.filterModelSpecicData.YearList;

                        for (var i = 0; i < years.length; i++) {
                            var yearDetail = years[i];
                            var option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                            $('#ddlExcludeYearsCustomDiscounts').append(option);

                        }
                        $("#ddlExcludeYearsCustomDiscounts").multiselect('reload');
                        var vins = dataResult.filterModelSpecicData.VinList;
                        for (var i = 0; i < vins.length; i++) {
                            var vin = vins[i];
                            var option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
                            $('#ddlVinCustomDiscounts').append(option);
                        }
                        $("#ddlVinCustomDiscounts").multiselect('reload');

                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").text("Select Vin(s)");
                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").attr("title", "Select Vin(s)");
                        $("#engineSectionCustomDiscounts .ms-options-wrap button").text("Select Engine(s)");
                        $("#engineSectionCustomDiscounts .ms-options-wrap button").attr("title", "Select Engine(s)");

                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").text("Select Trim(s)");
                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").attr("title", "Select Trim(s)");


                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").text("Select Year(s)");
                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").attr("title", "Select Year(s)");
                        $("#excludeBodyStylesCustomDiscounts .ms-options-wrap button").text("Select BodyStyle(s)");
                        $("#excludeBodyStylesCustomDiscounts .ms-options-wrap button").attr("title", "Select BodyStyle(s)");



                        $(".ms-options ul li").removeClass("selected");
                        $(".ms-options ul li input").addClass("focused");
                        $(".ms-options ul li input").attr('checked', false);
                        $(".ms-options ul li input").prop('checked', false);


                    }
                });
            }
        });

        $('#btnAddNewCustomDiscountsIncentive').on('click', function () {

            $('#incentiveEditCustomDiscountsControl').modal('show');
           
            $('#txtDateStartCustomDiscounts').datepicker({
                minDate: 0 
            });
            
            $('#txtDateEndCustomDiscounts').datepicker({
                minDate: 0,
                defaultDate: "+1M" 
            });
            
            $("#txtVinCustomDiscounts").val('');
            $("#ddlModelCustomDiscounts").val('0');
            $("#ddlEnginesCustomDiscounts").val('0');
            $("#ddlExcludeYearsCustomDiscounts").val('0');
            $("#ddlExcludeTrimsCustomDiscounts").val('0');
            $("#ddlExcludeBodyStylesCustomDiscounts").val('0');
            $("#ddlVinCustomDiscounts").val('0');
            $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").text("Select Vin(s)");
            $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").attr("title", "Select Vin(s)");
            $("#engineSectionCustomDiscounts .ms-options-wrap button").text("Select Engine(s)");
            $("#engineSectionCustomDiscounts .ms-options-wrap button").attr("title", "Select Engine(s)");

            $("#excludeTrimsCustomDiscounts .ms-options-wrap button").text("Select Trim(s)");
            $("#excludeTrimsCustomDiscounts .ms-options-wrap button").attr("title", "Select Trim(s)");

            $("#excludeYearsCustomDiscounts .ms-options-wrap button").text("Select Year(s)");
            $("#excludeYearsCustomDiscounts .ms-options-wrap button").attr("title", "Select Year(s)");

            $("#excludeBodyStylesCustomDiscounts .ms-options-wrap button").text("Select BodyStyle(s)");
            $("#excludeBodyStylesCustomDiscounts .ms-options-wrap button").attr("title", "Select BodyStyle(s)");

            $(".ms-options ul li").removeClass("selected");
            $(".ms-options ul li input").addClass("focused");
            $(".ms-options ul li input").attr('checked', false);
            $(".ms-options ul li input").prop('checked', false);
         
            var d = new Date();
            var StartDate = getFormattedDate(d);
            var EndDate = getFormattedDate(new Date(d.getFullYear(),d.getMonth() + 1,d.getDate()));
            $('#txtDateEndCustomDiscounts').val(EndDate);
            $('#txtDateStartCustomDiscounts').val(StartDate);
            var data = {
                name: "",
                type: "Cash",
                isActive: false,
                amount: "",
                disclaimer: "",
                updateFor: 1, // All Vehicles
                updateForVin: '',
                updateForModelId: 0,
                id: 0,
                excludeenginecodes: "",
                excludeVINs: "",
                excludeVINArrayData: "",
                excludeTrimId: "",
                excludeTrims: "",
                excludeYear: "",
                excludeYearsArrayData: "",
                startdate: StartDate,
                enddate: EndDate,
                hassetDateRange: false,
                discountType: 1,
                excludeBodyStyleId: "",
                excludeBodyStyles: "",
            };
            data.status = 'Active';
            data.displaySection = "";
            data.translatedName = "";
            data.position = "";
            data.updateFor = UPDATEFOR.ALL;
            excludeenginecodes = "";
            incentiveTranslatedUpdateCustomDiscounts.isResetAll = false;
            incentiveTranslatedUpdateCustomDiscounts.programData = data;
            
        });
        
    },
    loadMasterData: function () {
        var modelListSource = $('[id$="ModelListCustomDiscounts"]').val();
        var models = $.parseJSON(modelListSource);
        for (var i = 0; i < models.length; i++) {
            var model = models[i];
            var option = '<option value="' + model.Id + '">' + model.Name + '</option>';
            $('#ddlModelCustomDiscounts').append(option);
        }

        var engineListSource = $('[id$="EngineListCustomDiscounts"]').val();
        var engines = $.parseJSON(engineListSource);
        
        for (var i = 0; i < engines.length; i++) {
            var engine = engines[i];
            var option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';
            $('#ddlEnginesCustomDiscounts').append(option);
           
        }

        var trimsListSource = $('[id$="TrimListCustomDiscounts"]').val();
        var trims = $.parseJSON(trimsListSource);

        for (var i = 0; i < trims.length; i++) {
            var trimDetail = trims[i];
            var option = '<option value="' + trimDetail.Id + '">' + trimDetail.Name + '</option>';
            $('#ddlExcludeTrimsCustomDiscounts').append(option);

        }

        var bodyStyleListSource = $('[id$="BodyStyleListCustomDiscounts"]').val();
        var bodystyles = $.parseJSON(bodyStyleListSource);

        for (var i = 0; i < bodystyles.length; i++) {
            var bodyStyleDetail = bodystyles[i];
            var option = '<option value="' + bodyStyleDetail.Id + '">' + bodyStyleDetail.Name + '</option>';
            $('#ddlExcludeBodyStylesCustomDiscounts').append(option);

        }


        var yearListSource = $('[id$="YearListCustomDiscounts"]').val();
        var years = $.parseJSON(yearListSource);

        for (var i = 0; i < years.length; i++) {
            var yearDetail = years[i];
            var option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
            $('#ddlExcludeYearsCustomDiscounts').append(option);

        }

        var vinListSource = $('[id$="ExcludeVinListCustomDiscounts"]').val();
        var vins = $.parseJSON(vinListSource);
        for (var i = 0; i < vins.length; i++) {
            var vin = vins[i];
            var option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
            $('#ddlVinCustomDiscounts').append(option);
        }

     
    },

    setProgramData: function (rowData, row) {
        var isActive = 'Active';
        var vin = '';
        var modelid = '';

        var data = {
            //displaySection: rowData[7],
            //status: rowData[1],
            //position: rowData[8],
            //translatedName: rowData[2],
            name: rowData[2],
            type: rowData[3],
            isActive: false,
            amount: rowData[4],
            disclaimer: rowData[11],
            updateFor: 1, // All Vehicles
            updateForVin: '',
            updateForModelId: 0,
            id: rowData[14],
            excludeVINs: rowData[7],
            excludeenginecodes: rowData[10],
            excludeVINArrayData: rowData[7],
            startdate: rowData[8],
            enddate: rowData[9],
            hassetDateRange: rowData[15],
            hideOtherIncentives: rowData[16],
            excludeTrims: rowData[17],
            excludeYear: rowData[18],
            discountType: rowData[19],
            hideDiscounts: rowData[20],
            excludeBodyStyles: rowData[21],
            isMasterIncentive: rowData[22],
        };
        data.displaySection = row.attr('displaySection');
        data.translatedName = row.attr('translatedName');
        data.position = row.attr('position');

        if (row.is('.itemActive')) {
            data.status = 'Active';
            incentiveTranslatedUpdateCustomDiscounts.isResetAll = false;
        }
        else if (row.is('.itemHidden')) {
            data.status = 'Hidden';
            incentiveTranslatedUpdateCustomDiscounts.isResetAll = false;
        } else {
            incentiveTranslatedUpdateCustomDiscounts.isResetAll = true;
        }

        if (row.attr('isUpdateFor') == 'all') {
            data.updateFor = UPDATEFOR.ALL;
        }
        else if (row.attr('isUpdateFor') == 'vinSection') {
            data.updateFor = UPDATEFOR.VIN;
            data.updateForVin = row.attr('vin');;
        }
        else if (row.attr('isUpdateFor') == 'modelSection') {
            data.updateFor = UPDATEFOR.MODEL;
            data.updateForModelId = row.attr('modelid');
        }

        
        incentiveTranslatedUpdateCustomDiscounts.programData = data;
    },
    setOverrideExperationButton: function(name) {
            $('#priceDate').find('label[name="' + name + '"]').click();
        },
    loadData: function () {
        var data = $('#ddlLanguageCustomDiscounts').val();
        incentiveManagementTableCustomDiscounts = $('#tableIncentiveManagementCustomDiscounts').DataTable({
            ajax: {
                url: incentiveManagementCustomDiscounts.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function () {
                    return JSON.stringify(data);
                }
            },
            columns: [
               { sName: 'DisplaySection', width: "10%" },
                { sName: 'TranslatedName', width: "10%", sClass: "textLeft" },
                 { sName: 'ProgramName', width: "10%", sClass: "textLeft" },
                { sName: 'ProgramType', width: "5%" },
                { sName: 'Amount', sClass: 'breakText textLeft', width: "10%" },
                { sName: 'DiscountTypeFriendlyName', sClass: 'breakText textLeft', width: "10%" },
                 { sName: 'ExcludeEngingCodeUserFriendlyName', width: "10%" },
                  { sName: 'ExcludeVINs', width: "15%" },
                   { sName: 'StartDate', width: "10%" },
                    { sName: 'EndDate', width: "10%" },
                   { sName: 'excludeenginecodes', "visible": false },
                 { sName: 'Disclaimer', "visible": false },
                { sName: 'DisplaySectionId', "visible": false },
                { sName: 'Position', "visible": false },
                { sName: 'Id', "visible": false },
                { sName: 'HasSetDateRange', "visible": false },
                { sName: 'HideOtherIncentives', "visible": false },
                { sName: 'ExcludeTrimID', "visible": false },
                { sName: 'ExcludeYear', "visible": false },
                { sName: 'DiscountType', "visible": false },
                { sName: 'HideDiscounts', "visible": false },
                { sName: 'ExcludeBodyStylesID', "visible": false },
                { sName: 'IsMasterIncentive', "visible": false },
            ],
            "initComplete": function (settings, json) {
                incentiveTranslatedUpdateCustomDiscounts.currentTable = incentiveManagementTableCustomDiscounts;
            }
        });
        
    }
};

function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
  
    return month + '/' + day + '/' + year;
}
 
