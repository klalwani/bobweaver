﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

var METHOD = {
    Active: true,
    InActive: false
};

$(document).ready(function () {
    vehicleProgram.initialize();
});

var oTable;

var vehicleProgram = {
    isRunningAsPopup: function () {
        return true;
    },
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/VehicleController.ashx?method=' + 'GetVehiclePrograms';
    },
    programData: {},
    initialize: function () {
        vehicleProgram.loadPrograms();
        vehicleProgram.registerEvent();

        $('.updateForAllVehicle').remove();
        $('.updateForModel').remove();
        $('.updateForVin').click();
        $('#vinSection').removeClass('collapse').addClass('in');
    },
    registerEvent: function () {
        $('#tableVehicleProgram').on('click', 'a.editor_active, a.editor_inactive', function (e) {
            e.preventDefault();
            var that = $(this);
            var row = $(this).closest('tr');
            var id = oTable.row(row).data()[0];
            var method = 'active';
            if (that.hasClass('editor_active')) {
                method = METHOD.Active;
            }
            else if (that.hasClass('editor_inactive')) {
                method = METHOD.InActive;
            }

            var data = vehicleProgram.getDataForSubmit(row);
            data.isActive = method;
            data.agileVehicleId = $('[id$="AgileVehicleId"]').val();
            vehicleProgram.submitData(data, 'SetProgramState');

        });
        $('#tableVehicleProgram').on('click', '.editor_edit,.programEdit,.itemActive,.itemHidden', function () {
            var that = $(this);
            var thisRow = that.closest('tr');
            $('#incentiveEditControl').modal('show');
            var rowData = oTable.row(thisRow).data();
            vehicleProgram.setProgramData(rowData, that);
        });
        $('#btnAddNewProgram').click(function () {
            $("#program-list").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Add New Program',
                show: { effect: "fade", duration: 300 },
                position: { my: "center", at: "center", of: window },
                width: "auto",
                height: "auto",
                open: function () {
                    if (typeof programList != "undefined" && programList != null) {
                        programList.loadPrograms();
                    }
                },
                close: function () {
                    $("#program-list").dialog('destroy');
                }
            });
        });
    },
    setProgramData: function (rowData, row) {
        var isActive = 'Active';
        var vin = '';
        var modelid = '';
        var data = {
            //displaySection: rowData[7],
            //status: rowData[1],
            //position: rowData[8],
            //translatedName: rowData[2],
            name: rowData[1],
            type: rowData[3],
            id: rowData[6],
            isActive: false,
            updateFor: 1, // All Vehicles
            updateForVin: '',
            updateForModelId: 0
        };
        data.displaySection = row.attr('displaySection');
        data.translatedName = row.attr('translatedName');
        data.position = row.attr('position');
        if (row.is('.itemActive')) {
            data.status = 'Active';
        }
        else if (row.is('.itemHidden')) {
            data.status = 'Hidden';
        }
        data.updateFor = UPDATEFOR.VIN;
        data.updateForVin = $('#vin').val();
        incentiveTranslatedUpdate.isResetAll = false;
        incentiveTranslatedUpdate.programData = data;
    },

    submitData: function (data, method) {
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/VehicleController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {

                    } else {
                        alert("Something went wrong when updating this program.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                    oTable.ajax.reload();
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    getDataForSubmit: function (row) {
        var data = {};
        if (typeof row != "undefined" && row != null) {
            data = {
                programId: oTable.row(row).data()[0],
                name: oTable.row(row).data()[1],
                type: oTable.row(row).data()[2],
                id: oTable.row(row).data()[4],
                disclaimer: oTable.row(row).data()[5]
            };
        }
        return data;
    },
    loadPrograms: function () {
        var data = {
            vin: $('[id$="VinHdf"]').val()
        };
        oTable = $('#tableVehicleProgram')
            .DataTable({
                ajax: {
                    url: vehicleProgram.url(),
                    type: 'post',
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    cache: false,
                    data: function () {
                        return JSON.stringify(data);
                    }
                },
                language: {
                    emptyTable: "No record found!"
                },
                columns: [
                    { sName: 'ProgramId', "visible": false },
                    { sName: 'Name' },
                    { sName: 'TranslatedName' },
                    { sName: 'Type' },
                    { sName: 'Disclaimer' },
                    { sName: 'StartEnd' },
                    { sName: 'Id' },
                    { sName: 'Amount' },
                    { sName: 'IsActive' },
                    //{ sName: 'IsDeleted' },
                    {
                        sName: 'Active',
                        defaultContent: '<a href="" class="editor_active">Active</a>'
                    },
                    {
                        sName: 'In-Active',
                        defaultContent: '<a href="" class="editor_inactive">In-Active</a>'
                    },
                    {
                        sName: 'Edit',
                        defaultContent: '<span href="" class="programEdit">Edit</span>'
                    }
                ],
                "initComplete": function (settings, json) {
                    incentiveTranslatedUpdate.currentTable = oTable;
                }
            });
    }

};