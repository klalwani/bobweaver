﻿$(document).ready(function () {
    //applicantDashboard.initialize();
});

var applicantColumns =
[
    { "sName": "CustomerName", sWidth: "200px", sClass: "dateduetovenue", 'bSortable': true },
	{ "sName": "PhoneNumber", sWidth: "200px", sClass: "tprbDueDate", 'bSortable': true },
	{ "sName": "ApplicantType", sWidth: "200px", sClass: "title", 'bSortable': true },
	{ "sName": "IsJoinedApplicant", sWidth: "200px", sClass: "documenttype", 'bSortable': true },
];

var oTable;

var applicantDashboard = {
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'ApplicantDashboard';
    },
    initialize: function () {
        applicantDashboard.loadApplicantDashboard();
        applicantDashboard.registerEvent();
    },
    registerEvent: function() {
        $('#tableApplicant').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = oTable.row(row).data()[0];
            location.href = siteMaster.currentUrl() + '/Admin/FinanceApplicant/Detail/' + id;
        });

        $('#applicantTypeddl').change(function() {
            oTable.ajax.reload();
        });
    },
    loadApplicantDashboard: function () {
        oTable = $('#tableApplicant').DataTable({
            ajax: {
                url: applicantDashboard.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function () {
                    return JSON.stringify($('#applicantTypeddl').val());
                }
            },
            language: {
                emptyTable: "No record found!"
            },
            stateSave: true,
            stateDuration: 60 * 60,
            columns: [
                { sName: 'ApplicantId', "visible": false },
                { sName: 'CustomerName' },
                { sName: 'PhoneNumber' },
                { sName: 'City' },
                { sName: 'State' },
                { sName: 'NumberOfApplicant' },
                { sName: 'IsArchived' },
                { sName: 'DateCreated' },
                 { sName: 'IsCompleted' },
                {
                    sName: 'ApplicantId',
                    defaultContent: '<a href="" class="editor_edit">Detail</a>'
                }
            ],
            order: [[7, "desc"]]
        });
    }
}