﻿$(document).ready(function () {
});

var checkAvailabilityTable;

var checkAvailabilityDashboard = {
    self: 'checkAvailabilityDashboard',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadCheckAvailabilityDashboard';
    },
    initialize: function () {
        checkAvailabilityDashboard.registerEvents();
        checkAvailabilityDashboard.loadData();
    },
    registerEvents: function () {
        $('#tableCheckAvailability').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = checkAvailabilityTable.row(row).data()[0];
            location.href = siteMaster.currentUrl() + '/Admin/CheckAvailabilityDetail/' + id;
        });
    },
    loadData: function () {
        checkAvailabilityTable = $('#tableCheckAvailability').DataTable({
            ajax: checkAvailabilityDashboard.url(),
            //stateSave: true,
            //stateDuration: 60 * 60,
            language: {
                emptyTable: "No record found!"
            },
            columns: [
                { sName: 'VehicleFeedBackId', "visible": false },
                { sName: 'Name' },
                { sName: 'Email' },
                { sName: 'Phone' },
                { sName: 'Message' },
                { sName: 'Vin' },
                { sName: 'DealerName' },
                { sName: 'SubmittedDate' },
                {
                    sName: 'VehicleFeedBackId',
                    defaultContent: '<a href="" class="editor_edit">Detail</a>'
                }
            ],
            order: [[7, "desc"]]
        });
    }
};