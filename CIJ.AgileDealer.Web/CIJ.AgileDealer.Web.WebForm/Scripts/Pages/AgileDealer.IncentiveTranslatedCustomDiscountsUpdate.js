﻿function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

$(document).ready(function () {
    incentiveTranslatedUpdateCustomDiscounts.initialize();
});


var incentiveTranslatedUpdateCustomDiscounts = {
    initialize: function () {
        incentiveTranslatedUpdateCustomDiscounts.registerEvents();
    },
    isResetAll: false,
    currentTable: {},
    registerEvents: function () {
        $(document).on('click', '#btnSaveChangeCustomDiscounts', function () {
            incentiveTranslatedUpdateCustomDiscounts.prepareDataForSubmitting();
            incentiveTranslatedUpdateCustomDiscounts.submitData();
        });

        $(document).on('click', '#btnResetCustomDiscounts', function () {
            $('#modal-confirm-resetCustomDiscounts').modal('show');
        });

        $(document).on('click', '#btnConfirmResetCustomDiscounts', function () {
            incentiveTranslatedUpdateCustomDiscounts.prepareDataForSubmitting();
            incentiveTranslatedUpdateCustomDiscounts.resetIncentiveTranslated();
            $('#modal-confirm-resetCustomDiscounts').modal('toggle');
        });

        $(document).on('click', '#btnCancelResetCustomDiscounts', function () {
            $('#modal-confirm-resetCustomDiscounts').modal('toggle');
        });

        $(document).on('click', '#updateForCustomDiscounts label', function () {
            var that = $(this);
            var input = that.find('input');
            var value = input.val();
            var target = input.data('target');
            if (target == 'all') {
                $('#vinSectionCustomDiscounts').removeClass('in').addClass('collapse');
                $('#modelSectionCustomDiscounts').removeClass('in').addClass('collapse');
            }
            else if (target == 'vinSection') {
                $('#vinSectionCustomDiscounts').removeClass('collapse').addClass('in');
                $('#modelSectionCustomDiscounts').removeClass('in').addClass('collapse');
            }
            else if (target == 'modelSection') {
                $('#vinSectionCustomDiscounts').removeClass('in').addClass('collapse');
                $('#modelSectionCustomDiscounts').removeClass('collapse').addClass('in');
            }
        });

        $(document).on('shown.bs.modal', '#incentiveEditCustomDiscountsControl', function () {
            if (typeof incentiveTranslatedUpdateCustomDiscounts.programData != "undefined" && incentiveTranslatedUpdateCustomDiscounts.programData != null) {
                $('#lblProgramNameCustomDiscounts').text(incentiveTranslatedUpdateCustomDiscounts.programData.name);
                if (incentiveTranslatedUpdateCustomDiscounts.programData.displaySection != '0' && incentiveTranslatedUpdateCustomDiscounts.programData.displaySection != '')
                    $('#ddlDisplaySectionCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.displaySection);
                $('input[name="chkStatusCustomDiscounts"][value="' + incentiveTranslatedUpdateCustomDiscounts.programData.status + '"]').click();

                $('#CashCouponAmountCustomDiscounts').removeClass('collapse').addClass('in');
                $('#DisclaimerCustomDiscounts').removeClass('collapse').addClass('in');
                $("#txtCashCouponAmountCustomDiscounts").val(CommaFormatted(incentiveTranslatedUpdateCustomDiscounts.programData.amount).replace("$", ""));
                $("#txtCashCouponPercentageCustomDiscounts").val(CommaFormatted(incentiveTranslatedUpdateCustomDiscounts.programData.amount).replace("$", ""));
                $("#txtDisclaimerCustomDiscounts").val(incentiveTranslatedUpdateCustomDiscounts.programData.disclaimer);
                // $('#modelSection').removeClass('in').addClass('collapse');
                $('#txtDateStartCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.startdate);
                $('#txtDateEndCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.enddate);


                $('#txtPositionCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.position);
                $('#txtTranslatedNameCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.translatedName);

                if (incentiveTranslatedUpdateCustomDiscounts.programData.updateFor == UPDATEFOR.VIN) {
                    $('#txtVinCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.updateForVin);
                    $('#updateForCustomDiscounts label input[value="' + incentiveTranslatedUpdateCustomDiscounts.programData.updateForVin + '"]').click();

                }
                else if (incentiveTranslatedUpdateCustomDiscounts.programData.updateFor == UPDATEFOR.MODEL) {
                    $('#ddlModelCustomDiscounts').val(incentiveTranslatedUpdateCustomDiscounts.programData.updateForModelId);
                    $('#updateForCustomDiscounts label input[value="' + incentiveTranslatedUpdateCustomDiscounts.programData.updateForModelId + '"]').click();

                }
                else if (incentiveTranslatedUpdateCustomDiscounts.programData.updateFor == UPDATEFOR.ALL) {
                    $('#updateForCustomDiscounts label input[value="1"]').click();
                }


                if (incentiveTranslatedUpdateCustomDiscounts.programData.hideOtherIncentives == "True" || incentiveTranslatedUpdateCustomDiscounts.programData.hideOtherIncentives == "1") {
                    $("#chkHideAllOtherIncentivesCustomDiscounts").prop("checked", true);
                }
                else
                    $("#chkHideAllOtherIncentivesCustomDiscounts").prop("checked", false);

                if (incentiveTranslatedUpdateCustomDiscounts.programData.isMasterIncentive == "True" || incentiveTranslatedUpdateCustomDiscounts.programData.isMasterIncentive == "1") {
                    $("#chkIsMasterIncentiveCustomDiscounts").prop("checked", true);
                }
                else
                    $("#chkIsMasterIncentiveCustomDiscounts").prop("checked", false);

                if ($("#chkIsMasterIncentiveCustomDiscounts").is(":checked")) {
                    $(".isMasterCustomDiscounts").addClass("hide");
                    $(".isMasterCustomDiscountsTab").attr("disabled", "disabled");
                    $(".updateForMasterIncentiveCustomDiscounts").removeAttr("disabled");
                    $('#updateForCustomDiscounts label input[value="4"]').click();
                }
                else {
                    $(".isMasterCustomDiscounts").removeClass("hide");
                    $(".isMasterCustomDiscountsTab").removeAttr("disabled");
                    $(".updateForMasterIncentiveCustomDiscounts").attr("disabled", "disabled");
                }

               if (incentiveTranslatedUpdateCustomDiscounts.programData.hideDiscounts == "True" || incentiveTranslatedUpdateCustomDiscounts.programData.hideDiscounts == "1") {
                    $("#chkHideDealerDiscountsCustomDiscounts").prop("checked", true);
                }
                else
                    $("#chkHideDealerDiscountsCustomDiscounts").prop("checked", false);

                 $("#ddlDiscountTypeCustomDiscounts").val(incentiveTranslatedUpdateCustomDiscounts.programData.discountType);

                 if (incentiveTranslatedUpdateCustomDiscounts.programData.discountType == "1") {
                     $("#divCashCouponPercentageCustomDiscounts").addClass("hide");
                     $("#divCashCouponAmountCustomDiscounts").removeClass("hide");
                 }
                 else {
                     $("#divCashCouponPercentageCustomDiscounts").removeClass("hide");
                     $("#divCashCouponAmountCustomDiscounts").addClass("hide");
                 }

                var valEngineCodesArr = "0".split(",");
                if (incentiveTranslatedUpdateCustomDiscounts.programData.excludeenginecodes != null) {
                    valEngineCodesArr = incentiveTranslatedUpdateCustomDiscounts.programData.excludeenginecodes.split(",");
                }

                var i = 0;
                var size = valEngineCodesArr.length;
                var selectedEngines = "";
                for (i = 0; i < size; i++) {
                    $("#engineSectionCustomDiscounts .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valEngineCodesArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valEngineCodesArr - 1) {
                                selectedEngines = selectedEngines + $(this).find("input").prop('title');
                            }
                            else
                                selectedEngines = selectedEngines + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedEngines.toString().length > 70) {
                        $("#engineSectionCustomDiscounts .ms-options-wrap button").text(selectedEngines.substr(1, 69) + "...");
                        $("#engineSectionCustomDiscounts .ms-options-wrap button").attr("title", selectedEngines);
                    }
                    if (selectedEngines.toString().length > 1) {

                        $("#engineSectionCustomDiscounts .ms-options-wrap button").text(selectedEngines);
                        $("#engineSectionCustomDiscounts .ms-options-wrap button").attr("title", selectedEngines);
                    }


                }
                $("#ddlEnginesCustomDiscounts").val(valEngineCodesArr);

                var valVinArr = "0";
                if (incentiveTranslatedUpdateCustomDiscounts.programData.excludeVINs != null) {
                    valVinArr = incentiveTranslatedUpdateCustomDiscounts.programData.excludeVINs.split(",");
                }



                i = 0;
                var sizeVinArr = valVinArr.length;
                var selectedVins = "";

                for (i = 0; i < sizeVinArr; i++) {
                    valVinArr[i] = $.trim(valVinArr[i]);
                    $("#excludeVinSectionCustomDiscounts .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valVinArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == sizeVinArr - 1) {
                                selectedVins = selectedVins + $(this).find("input").prop('title');
                            }
                            else
                                selectedVins = selectedVins + $(this).find("input").prop('title') + ",";

                        }
                    });
                    if (selectedVins.toString().length > 70) {
                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").text(selectedVins.substr(1, 69) + "...");
                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").attr("title", selectedVins);
                    }
                    if (selectedVins.toString().length > 1) {

                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").text(selectedVins);
                        $("#excludeVinSectionCustomDiscounts .ms-options-wrap button").attr("title", selectedVins);
                    }


                }
                $("#ddlVinCustomDiscounts").val(valVinArr);


                var valTrimsArr = "0".split(",");
                if (incentiveTranslatedUpdateCustomDiscounts.programData.excludeTrims != null) {
                    valTrimsArr = incentiveTranslatedUpdateCustomDiscounts.programData.excludeTrims.split(",");
                }

                i = 0;
                size = valTrimsArr.length;
                var selectedTrims = "";


                for (i = 0; i < size; i++) {
                    $("#excludeTrimsCustomDiscounts .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valTrimsArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valTrimsArr - 1) {
                                selectedTrims = selectedTrims + $(this).find("input").prop('title');
                            }
                            else
                                selectedTrims = selectedTrims + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedTrims.toString().length > 70) {
                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").text(selectedTrims.substr(1, 69) + "...");
                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").attr("title", selectedTrims);
                    }
                    if (selectedTrims.toString().length > 1) {

                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").text(selectedTrims);
                        $("#excludeTrimsCustomDiscounts .ms-options-wrap button").attr("title", selectedTrims);
                    }


                }
                 $("#ddlExcludeTrimsCustomDiscounts").val(valTrimsArr);


                var valYearsArr = "0".split(",");
                if (incentiveTranslatedUpdateCustomDiscounts.programData.excludeYear != null) {
                    valYearsArr = incentiveTranslatedUpdateCustomDiscounts.programData.excludeYear.split(",");
                }
                i = 0;
                size = valYearsArr.length;
                var selectedYears = "";


                for (i = 0; i < size; i++) {
                    $("#excludeYearsCustomDiscounts .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valYearsArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valYearsArr - 1) {
                                selectedYears = selectedYears + $(this).find("input").prop('title');
                            }
                            else
                                selectedYears = selectedYears + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedYears.toString().length > 70) {
                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").text(selectedYears.substr(1, 69) + "...");
                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").attr("title", selectedYears);
                    }
                    if (selectedYears.toString().length > 1) {

                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").text(selectedYears);
                        $("#excludeYearsCustomDiscounts .ms-options-wrap button").attr("title", selectedYears);
                    }


                }
                 $("#ddlExcludeYearsCustomDiscounts").val(valYearsArr);


                var valBodyStylesArr = "0".split(",");
              
                if (incentiveTranslatedUpdateCustomDiscounts.programData.excludeBodyStyles != null) {
                    valBodyStylesArr = incentiveTranslatedUpdateCustomDiscounts.programData.excludeBodyStyles.split(",");
                }

                i = 0;
                size = valBodyStylesArr.length;
                var selectedBodyStyles = "";

                for (i = 0; i < size; i++) {
                    $("#excludeBodyStyleCustomDiscounts .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valBodyStylesArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valBodyStylesArr - 1) {
                                selectedBodyStyles = selectedBodyStyles + $(this).find("input").prop('title');
                            }
                            else
                                selectedBodyStyles = selectedBodyStyles + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedBodyStyles.toString().length > 70) {
                        $("#excludeBodyStyleCustomDiscounts .ms-options-wrap button").text(selectedBodyStyles.substr(1, 69) + "...");
                        $("#excludeBodyStyleCustomDiscounts .ms-options-wrap button").attr("title", selectedBodyStyles);
                    }
                    if (selectedBodyStyles.toString().length > 1) {

                        $("#excludeBodyStyleCustomDiscounts .ms-options-wrap button").text(selectedBodyStyles);
                        $("#excludeBodyStyleCustomDiscounts .ms-options-wrap button").attr("title", selectedBodyStyles);
                    }


                }
                $("#ddlexcludeBodyStyleCustomDiscounts").val(valBodyStylesArr);



                if (incentiveTranslatedUpdateCustomDiscounts.programData.hassetDateRange == 'True') {
                    incentiveManagementCustomDiscounts.setOverrideExperationButton('setDate');

                }

            }
        });
    },
    prepareDataForSubmitting: function () {
        incentiveTranslatedUpdateCustomDiscounts.programData.id = incentiveTranslatedUpdateCustomDiscounts.programData.id;
        incentiveTranslatedUpdateCustomDiscounts.programData.displaySection = $('#ddlDisplaySectionCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.position = $('#txtPositionCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.translatedName = $('#txtTranslatedNameCustomDiscounts').val();
        if ($('input[name="chkStatusCustomDiscounts"]:checked')[0].value == "Hidden") {
            incentiveTranslatedUpdateCustomDiscounts.programData.isActive = false;
        } else {
            incentiveTranslatedUpdateCustomDiscounts.programData.isActive = true;
        }

        incentiveTranslatedUpdateCustomDiscounts.programData.updateFor = $('#updateForCustomDiscounts label.active input').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.updateForVin = $('#txtVinCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.updateForModelId = $('#ddlModelCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.isResetAll = incentiveTranslatedUpdateCustomDiscounts.isResetAll;

        incentiveTranslatedUpdateCustomDiscounts.programData.isCashCoupon = true;


        if ($("#ddlDiscountTypeCustomDiscounts").val() == "1") {
            incentiveTranslatedUpdateCustomDiscounts.programData.amount = $("#txtCashCouponAmountCustomDiscounts").val().replace(/,/gi, "").replace("$", "");
            
        }
        else {
            incentiveTranslatedUpdateCustomDiscounts.programData.amount = $("#txtCashCouponPercentageCustomDiscounts").val().replace(/,/gi, "");
        }

        incentiveTranslatedUpdateCustomDiscounts.programData.discountType = $("#ddlDiscountTypeCustomDiscounts").val();

        if ($("#chkHideAllOtherIncentivesCustomDiscounts").prop("checked") == "true" || $("#chkHideAllOtherIncentivesCustomDiscounts").prop("checked") == "True" || $("#chkHideAllOtherIncentivesCustomDiscounts").is(":checked"))
        {
            incentiveTranslatedUpdateCustomDiscounts.programData.hideOtherIncentives = "true";
        }
        else {
            incentiveTranslatedUpdateCustomDiscounts.programData.hideOtherIncentives = "false";
        }


        if ($("#chkIsMasterIncentiveCustomDiscounts").prop("checked") == "true" || $("#chkIsMasterIncentiveCustomDiscounts").prop("checked") == "True" || $("#chkIsMasterIncentiveCustomDiscounts").is(":checked")) {
            incentiveTranslatedUpdateCustomDiscounts.programData.isMasterIncentive = "true";
        }
        else {
            incentiveTranslatedUpdateCustomDiscounts.programData.isMasterIncentive = "false";
        }
       

        if ($("#chkHideDealerDiscountsCustomDiscounts").prop("checked") == "true" || $("#chkHideDealerDiscountsCustomDiscounts").prop("checked") == "True" || $("#chkHideDealerDiscountsCustomDiscounts").is(":checked")) {
            incentiveTranslatedUpdateCustomDiscounts.programData.hideDiscounts = "true";
        }
        else {
            incentiveTranslatedUpdateCustomDiscounts.programData.hideDiscounts = "false";
        }

        incentiveTranslatedUpdateCustomDiscounts.programData.disclaimer = $("#txtDisclaimerCustomDiscounts").val();
        incentiveTranslatedUpdateCustomDiscounts.programData.excludeenginecodes = $('#ddlEnginesCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.excludeVINArrayData = $('#ddlVinCustomDiscounts').val();

     
        incentiveTranslatedUpdateCustomDiscounts.programData.excludeYearsArrayData = $('#ddlExcludeYearsCustomDiscounts').val();
        incentiveTranslatedUpdateCustomDiscounts.programData.excludeTrims = $('#ddlExcludeTrimsCustomDiscounts').val();

        incentiveTranslatedUpdateCustomDiscounts.programData.excludeBodyStyles = $('#ddlexcludeBodyStyleCustomDiscounts').val();

        var isSelectedMode = $('#priceDate').find('label.active');
        if ($(isSelectedMode).data('toggle') == 'in') {
            incentiveTranslatedUpdateCustomDiscounts.programData.hassetDateRange = true;
            incentiveTranslatedUpdateCustomDiscounts.programData.startdate = $("#txtDateStartCustomDiscounts").val();
            incentiveTranslatedUpdateCustomDiscounts.programData.enddate = $("#txtDateEndCustomDiscounts").val();
        
        } else {
            incentiveTranslatedUpdateCustomDiscounts.programData.hassetDateRange = false;
            incentiveTranslatedUpdateCustomDiscounts.programData.startdate = null;
            incentiveTranslatedUpdateCustomDiscounts.programData.enddate = null;
        }

       


    },
    submitData: function () {
        var method = 'UpdateIncentiveTranslatedCustomDiscounts';
        var isValid = true;
        //if (incentiveTranslatedUpdateCustomDiscounts.programData.amount == "") {
        //    $("#txtCashCouponAmountCustomDiscounts").addClass("input-validation-error");
        //    isValid = false;
        //}
        if (incentiveTranslatedUpdateCustomDiscounts.programData.displaySection == "") {
            $("#ddlDisplaySectionCustomDiscounts").addClass("input-validation-error");
            isValid = false;
        }
        if (incentiveTranslatedUpdateCustomDiscounts.programData.translatedName == "") {
            $("#txtTranslatedNameCustomDiscounts").addClass("input-validation-error");
            isValid = false;
        }

        if (isValid) {


            var result = true;
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 30000,
                data: JSON.stringify(incentiveTranslatedUpdateCustomDiscounts.programData),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        result = true;
                        if (typeof incentiveTranslatedUpdateCustomDiscounts.currentTable != 'undefined' && incentiveTranslatedUpdateCustomDiscounts.currentTable != null)
                            $('.modal').modal('hide');
                        incentiveTranslatedUpdateCustomDiscounts.currentTable.ajax.reload();
                    }
                }
            });
        }
        else {
            // $('.modal').modal('hide');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            alert("Please input all required information!");
            document.getElementById('incentiveEditCustomDiscountsControl').style.display = "block";
            return false;
        }

    },
    resetIncentiveTranslated: function () {
        var method = 'ResetIncentiveTranslatedCustomDiscounts';
        var isResetAll = false;

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdateCustomDiscounts.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdateCustomDiscounts.currentTable != 'undefined' && incentiveTranslatedUpdateCustomDiscounts.currentTable != null)
                        $('.modal').modal('hide');
                        incentiveTranslatedUpdateCustomDiscounts.currentTable.ajax.reload();
                }
            }
        });
    }
};