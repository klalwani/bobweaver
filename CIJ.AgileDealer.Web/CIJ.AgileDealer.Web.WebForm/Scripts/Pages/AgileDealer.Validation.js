﻿var AgileDealerValidation = {
    //register event for validating
    InitialValidationEvent: function () {
        $(document).on("change blur", "input[type=text][data-val=true],textarea[data-val=true]", function (e) {
            //validate control
            AgileDealerValidation.ValidateTextInput(this);
        });
        $(document).on("change blur", "input[type=date][data-val=true]", function (e) {
            //validate control
            AgileDealerValidation.ValidateTextInput(this);
        });
        $(document).on("change blur", "select[data-val=true]", function (e) {
            //validate control
            AgileDealerValidation.ValidateDropdownList(this);
        });
        $(document).on("change blur", "input[type=radio]", function (e) {
            //validate control
            AgileDealerValidation.ValidateRadioButton(this);
        });
    },
    //change state of selected element valid or invalid
    ChangeState: function (isValid, element) {
        if (isValid) {
            $(element).removeClass("input-validation-error");
            $(element).addClass("valid");
        } else {
            $(element).removeClass("valid");
            $(element).addClass("input-validation-error");
        }
    },
    ValidateRow: function (childControl) {
        //get parent group
        var parentGroup = $(childControl).closest(".rowElem");
        //if all controls in parent group have class valid
        var isValid = $(parentGroup).find("[data-val=true]:not([type=hidden])").All("valid");
        //change state of parent group
        AgileDealerValidation.ChangeState(isValid, parentGroup);
    },
    ValidateTextInput: function (control) {
        $(control).valid();
    },
    ValidateDropdownList: function (control) {
        $(control).valid();
    },
    ValidateRadioButton: function (control) {
        var radioBag = $(control).closest(".formUnder");
        var radios = radioBag.find("input[type=radio]");
        var isValidated = false;
        var result = true;
        radios.each(function () {
            var attr = $(this).attr("data-val");
            if (typeof attr != 'undefined') {
                isValidated = true;
                return;
            }
        });
        //if validate
        if (isValidated) {

            var checkedRadio = radioBag.find("label.checked input[type=radio]");
            var txtCheckedRadio = checkedRadio.attr("text");
            txtCheckedRadio = String(txtCheckedRadio).toLowerCase();
            var valCheckedRadio = checkedRadio.val();
            valCheckedRadio = String(valCheckedRadio).toLowerCase();
            var markValidateControl = radioBag.find("input[type=radio][data-val=true]");

            if (txtCheckedRadio == "yes" || txtCheckedRadio == "no" || valCheckedRadio == "true" || valCheckedRadio == "false") {
                result = true;
            } else {
                result = false;
            }
            AgileDealerValidation.ChangeState(result, markValidateControl);
        }

        return result;
    },
    StringIsEmty: function (locationField) {
        var source = $(locationField).val();
        source = $.trim(source);

        if (source == "") {
            return true;
        }
        return false;
    },
    IsMatchRegex: function (control) {
        var target = $(control);
        var isControlValid = true;
        var regex = target.attr("data-val-regex-pattern");
        if (typeof regex !== "undefined") {
            var value = target.val();
            if (value != "") {
                if (regex == "website") {
                    isControlValid = RegExp("^(http(s)?://)?(([\w\-]+\\.)*)?[\\w\-]+(\\:\\d{0,5})?(/[\\w\- ;,./?%&=#]*)$").test(value);
                } else {
                    isControlValid = RegExp(regex).test(value);
                }
            }
        }
        return isControlValid;
    },
    IsNotContainHtmlCode: function (control) {
        var isValid = true;
        var target = $(control);
        var value = target.val();
        if (value != "" && value != "undefined" && value != null)
            isValid = value.indexOf("<") < 0;
        if (isValid) {
            control.removeClass("htmlcode");
        } else {
            control.addClass("htmlcode");
        }
        return isValid;
    }
};

$.fn.applyNumeric = function () {
    $(this).numeric();
};

$.fn.All = function (className) {
    var result = true;

    jQuery(this).each(function () {
        //if element has not class name
        if (!$(this).hasClass(className)) {

            var attr = $(this).attr("data-val-required");
            //if element has not attr
            if (typeof attr === 'undefined') {
                if ($(this).hasClass("input-validation-error")) {
                    result = false;
                    return;
                }
            } else {
                result = false;
                return;
            }
        }
    });

    return result;
};

$.fn.valid = function () {
    var target = $(this);
    var isControlValid = true;
    var dataVal = target.attr("data-val");

    if (typeof dataVal !== "undefined") {
        var type = target.attr("type");
        //if is input
        if (typeof type !== "undefined") {
            //if text
            if (type === "text") {
                var dataRequired = target.attr("data-val-required");
                if (typeof dataRequired !== "undefined") {

                    var isEmty = AgileDealerValidation.StringIsEmty(target);
                    isControlValid = !isEmty;

                    if (isControlValid) {
                        isControlValid = AgileDealerValidation.IsMatchRegex(target);
                    }
                } else {
                    isControlValid = AgileDealerValidation.IsMatchRegex(target);
                }
                isControlValid = AgileDealerValidation.IsNotContainHtmlCode(target) && isControlValid;
            }
            //if other

        }
            //for select and other
        else {
            var required = target.attr("data-val-required");
            if (typeof required !== "undefined") {

                if ($(target).prop("tagName").toLowerCase() == "select") {
                    var val = $(target).find(":selected").val();
                    if (val == "" || typeof val === "undefined") {
                        isControlValid = false;
                    }
                } else if ($(target).prop("tagName").toLowerCase() == "textarea") {
                    var dataRequired = target.attr("data-val-required");
                    if (typeof dataRequired !== "undefined") {

                        var isEmty = AgileDealerValidation.StringIsEmty(target);
                        isControlValid = !isEmty;

                        if (isControlValid) {
                            isControlValid = AgileDealerValidation.IsMatchRegex(target);
                        }
                    } else {
                        isControlValid = AgileDealerValidation.IsMatchRegex(target);
                    }
                    isControlValid = AgileDealerValidation.IsNotContainHtmlCode(target) && isControlValid;
                } else {
                    jMessageBox({
                        message: "this control does not validate.",
                        title: "Warning",
                        type: "warning"
                    });
                }
            }
        }

    }

    AgileDealerValidation.ChangeState(isControlValid, target);
    return isControlValid;
};