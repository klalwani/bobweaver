﻿$(document).ready(function () {
    partDepartment.initialize();
});

var partDepartment = {
    currentForm: "#partDepartmentForm",
    initialize: function () {
        partDepartment.registerEvent();
        AgileDealerValidation.InitialValidationEvent();
        partDepartment.clearTextBox();
    },
    registerEvent: function () {
        $('[id$="btnSubmit"]').click(function (e) {
            e.preventDefault();
            partDepartment.blurAllControl(partDepartment.currentForm);
            if (partDepartment.checkValidationOfCurrentForm(partDepartment.currentForm)) {
                partDepartment.submitDataForPartRequest();
            } else {
                alert('Please input all required informations!');
            }
        });
    },
    getDealerName: function () {
        return $('#dealerName').val();
    },
    submitDataForPartRequest: function () {
        var method = 'SubmitPartRequest';
        var data = {
            firstName: $('[id$="firstName"]').val(),
            lastName: $('[id$="lastName"]').val(),
            email: $('[id$="email"]').val(),
            phoneNumber: $('[id$="phone"]').val(),
            postalCode: $('[id$="postalCode"]').val(),
            year: $('[id$="year"]').val(),
            make: $('[id$="make"]').val(),
            model: $('[id$="model"]').val(),
            vin: $('[id$="vin"]').val(),
            partNumber: $('[id$="partNumber"]').val(),
            contactBy: $('[id$="contactBy"] option:selected').text()
        };

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SiteMasterController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    partDepartment.clearTextBox();
                    alert('Thank you for contacting ' + partDepartment.getDealerName() + '!');
                }
            }
        });
    },
    clearTextBox: function () {
        $('[id$="firstName"]').val('');
        $('[id$="lastName"]').val('');
        $('[id$="email"]').val('');
        $('[id$="phone"]').val('');
        $('[id$="postalCode"]').val('');
        $('[id$="year"]').val('');
        $('[id$="make"]').val('');
        $('[id$="model"]').val('');
        $('[id$="vin"]').val('');
        $('[id$="partNumber"]').val('');
        $('[id$="contactBy"]').val('email');
    },
    blurAllControl: function (currentForm) {
        $(currentForm).find("[data-val=true]:visible:not([type=hidden])").blur();
    },
    checkValidationOfCurrentForm: function (currentForm) {
        return $(currentForm).find("[data-val=true]:visible:not([type=hidden])").All("valid");
    },
};