﻿$(document).ready(function () {
    vehicleInfo.initialize();
});

var vehicleInfo = {
    initialize: function () {
        vehicleInfo.registerEvents();
        vehicleInfo.prepairEditLink();
        vehicleInfo.setDefaultMasked();
        vehicleInfo.modifyCurrencyValue();
        vehicleInfo.hidePriceFieldsIfOverridePriceIsTrue();
        vehicleInfo.loadRebate();
    },
    prepairEditLink: function() {
        var isVisible = $('[id$="IsVisible"]').val() == "True";
        if (isVisible) {
            var vehicleVin = $('[id$="VehicleVin"]').val();
            $('#btnEditVehicle').attr('href', '/admin/vehicleEdit/' + vehicleVin);
            $('#btnEditVehicle').show();
        } else {
            $('#btnEditVehicle').hide();
        }
    },
    hidePriceFieldsIfOverridePriceIsTrue: function() {
        var isOverridePrice = $('[id$="IsOverridePrice"]').val() == "True";
        var overrideStartDate = $('[id$="OverrideStartDate"]').val();
        var overrideEndDate = $('[id$="OverrideEndDate"]').val();
        var hasSetDateRange = $('[id$="HasSetDateRange"]').val() == "True";
        var today = new Date();
        //if (isOverridePrice && (!hasSetDateRange || (new Date(overrideStartDate) <= today  && today <= new Date(overrideEndDate)))) {
        //    //$('#msrpSection').hide();
        //    $('#rebateSection').hide();
        //    //$('#totalSavingSection').hide();
        //}
    },
    loadRebate: function() {
        if (typeof rebateControl != "undefined" && rebateControl != null) {
            var agileVehicleId = $('[id$="hdfAgileVehicleId"]').val();
            rebateControl.loadRebateForVehicle(agileVehicleId);
        }
    },
    unmaskedInput: function () {
        $(".price-main,.rebate,.msrp-price,.dealersaving").unmask();
    },
    setDefaultMasked: function () {
        $(".price-main,.rebate,.msrp-price,.dealersaving").each(function () {
            var that = $(this);
            var text = that.text();
            text = text.replace('.00', '');
            that.text(text);
        });
    },
    currencyFormat: function (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    },
    tryParseValue: function (value) {
        if (typeof value != "undefined" && value != NaN && value != null) {
            var temp = value;
            temp = temp.replace('$', '').replace('.', '.');
            return parseFloat(temp);
        }
        return '';
    },
    modifyCurrencyValue: function() {
        if ($('[id$="lblMPGCityTwo"]').text() == 0 || $('[id$="lblMPGHwyTwo"]').text() == 0) {
            $('#fuelRating').hide();
            $('#hwyMPG').hide();
            $('#cityMPG').hide();
        } else {
            $('#fuelRating').show();
            $('#hwyMPG').show();
            $('#cityMPG').show();
        }

        //if ($('[id$="lblMSRP"]').text() == '$0' || $('[id$="lblMSRP"]').text() == '- $0' || $('[id$="lblMSRP"]').text() == $('[id$="lblPrice"]').text()) {
        //    $('#msrpSection').hide();
        //} else {
        //    $('#msrpSection').show();
        //}

        if ($('[id$="lblRebate"]').text() == '$0' || $('[id$="lblRebate"]').text() == '- $0') {
            $('#rebateSection').hide();
        } else {
            $('#rebateSection').show();
        }

        if ($('[id$="lblDealerSave"]').text() == '$0' || $('[id$="lblDealerSave"]').text() == '- $0' || $('[id$="lblPrice"]').text().startsWith('$0')) {
            $('#totalSavingSection').hide();
        } else {
            $('#totalSavingSection').show();
        }

        if ($('[id$="lblPrice"]').text() == '$0') {
            $('[id$="lblPrice"]').text('Call');
        }

        //TODO:This need to be changed into another way, should be in code behind.
        if (vehicleInfo.tryParseValue($('[id$="lblMSRP"]').text()) <= vehicleInfo.tryParseValue($('[id$="lblPrice"]').text())) {
            $('#msrpSection').hide();
            $('#totalSavingSection').hide();
        }

        if ($('[id$="VehicleType"]').val() != 'True' && $('[id$="VehicleType"]').val() != 'true') {
            $('.autocheck').show();
        } else {
            $('.autocheck').hide();
        }
    },

    AddParameter: function (form, name, value) {
        var $input = $("<input />").attr("type", "hidden")
            .attr("name", name)
            .attr("value", value);
        form.append($input);
    },

    registerEvents: function () {
        $("#btnCheckAvailability").click(function () {
            $('#check-availability-control').modal('show');
           
        });

        $("#btnPayment").click(function () {
            $('#ucPaymentCalculator').dialog({
                dialogClass: "no-close no-title",
                modal: true,
                closeOnEscape: true,
                position: { my: "center", at: "center", of: window },
                width: "auto",
                height: "auto",
                show: { effect: "fade", duration: 300 },
                bgiframe: true,
                open: function () {
                    $("#ui-dialog-title-dialog").hide();
                    $(".ui-dialog-titlebar").removeClass('ui-widget-header');
                    $(".ui-dialog-titlebar").hide();
                    $(".ui-dialog-titlebar-close").remove();
                },
                close: function () {
                }
            });
        });

        //$('.autocheck').click(function (event) {
        //    event.preventDefault();
        //    var $form = $("<form/>")
        //        .attr("id", "data_form")
        //        .attr("action", "/partners/autocheck")
        //        .attr("method", "post")
        //        .attr('target', '_blank');

        //    $("body").append($form);
        //    var vin = $('[id$="VehicleVin"]').val();
        //    //Append the values to be send
        //    vehicleInfo.AddParameter($form, "vin", vin);
        //    $form[0].submit();
        //});

        $('.openRebate').click(function () {
            var that = $(this);
            var incentiveDisplayTypeId = $('[id$="HdfIncentiveDisplayTypeId"]').val();
            var agileVehicleId = that.attr('agileVehicleId');
            var programId = that.attr('programId');
            var vin = that.attr('vin');
            var modelId = that.attr('modelId');
            var isCashCoupon = that.attr('isCashCoupon');
            var isUpfitted = that.attr('isUpfitted');
            var method = '';
            var data = {
                agileVehicleId: agileVehicleId,
                vin: vin,
                modelId: modelId,
                programId: programId,
                isCashCoupon: isCashCoupon,
                isUpfitted: isUpfitted
            };

            if (incentiveDisplayTypeId == INCENTIVETYPEID.NORMAl) {
                method = 'GetRebateForVehicleByAgileVehicleId';
            } else {
                method = 'GetRebateForVehicleByAgileVehicleId';
            }
            

            if (typeof rebateControl != "undefined" && rebateControl != null) {
                rebateControl.loadRebateForVehicle(data, method);
            }

            $('#rebateControl').modal('show');
        });


        $('.ui-front').click(function () {
            $('#ucCheckAvailability').dialog('close');
        });

        $('#ucRebate').on('click', '#btnCloseRebate', function () {
            $('#ucRebate').dialog('close');
        });

        $('#rebateControl').on('shown.bs.modal', function () {
            //if (typeof rebateControl != "undefined" && rebateControl != null) {
            //    rebateControl.loadRebateForVehicle(data, method);
            //}
        });

        $('#MainContent_btnPrint,#MainContent_btnWindow').click(function (e) {
            alert('This page is under construction!');
            e.preventDefault();
        });

        $(document).on('click', '#btnPrint', null, function () {
            $('#printFeature').removeClass('hidden');

            $('#vehicle-display').print({
                globalStyles: true,
                mediaPrint: false,
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: false,
                timeout: 1000,
                title: null,
                doctype: '<!doctype html>'
            });
            setTimeout(function () {
                $('#printFeature').addClass('hidden');
            }, 3000);
        });

        
    }
};