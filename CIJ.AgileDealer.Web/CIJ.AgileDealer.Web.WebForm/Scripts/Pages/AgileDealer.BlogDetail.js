﻿$(document).ready(function () {
    blogDetail.initialize();
});

var blogDetail = {
    initialize: function () {
        blogDetail.registerEvent();
    },
    registerEvent: function () {
        $('[id$="btnText2"]').click(function (e) {
            e.preventDefault();
            $('#ucContact').dialog({
                dialogClass: "no-close no-title",
                modal: true,
                closeOnEscape: true,
                position: { my: "center", at: "center", of: window },
                width: "auto",
                height: "auto",
                bgiframe: true,
                open: function () {
                    ContactUs.clearTextBox();
                    ContactUs.removeRedundantAttribute(ContactUs.currentForm);
                    ContactUs.loadScript();
                    $("#ui-dialog-title-dialog").hide();
                    $(".ui-dialog-titlebar").removeClass('ui-widget-header');
                    $(".ui-dialog-titlebar").hide();
                    $('.ui-dialog').removeClass('ui-widget');
                },
                close: function () {
                }
            });
        });
    }
};