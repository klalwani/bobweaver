﻿//Forms
var serviceForms = [
    {
        currentForm: 'scheduleServiceCtr', previousForm: '', nextForm: 'completeCtr', lastForm: 'completeCtr'
    }
];
var weekday = new Array(7);
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";

$(document).ready(function () {
    ScheduleService.initialize();
    AgileDealerValidation.InitialValidationEvent();
});
var ScheduleService = {
    localizations:{},
    initialize: function () {
        ScheduleService.registerEvents();
        ScheduleService.enableControl();
        ScheduleService.localizations = common.getLocalizations();
    },
    enableControl: function () {
        $('#serviceDate').datepicker(
        {
            minDate: +1,
            beforeShowDay: function (date) {
                var day = date.getDay();
                return [(day != 0), ''];
            },
            onSelect: function (dateText, inst) {
                var that = $(this);
                ScheduleService.setRequireFieldForDaysWithinWeek(that);
                that.blur();
            }
        });
    },
    registerEvents: function () {
        $('#btnSubmit').click(function () {
            var isValid = ScheduleService.checkValidationOfCurrentForm('scheduleServiceCtr');
            isValid = isValid && ScheduleService.validateForm('serviceRequested');
            isValid = isValid && ScheduleService.validateForm('currentIssue');
            if (isValid) {
                var isSaveSucceed = ScheduleService.saveDataOfCurrentForm();
                if (isSaveSucceed) {
                    var currentForm = 'scheduleServiceCtr';
                    if (typeof currentForm != "undefined" && currentForm != null) {
                        var movementInfo = ScheduleService.getMovementInfo(currentForm);
                        if (typeof movementInfo != "undefined" && movementInfo != null) {
                            if (movementInfo.nextForm !== '') {
                                ScheduleService.switchForms(currentForm, movementInfo.nextForm);
                                ScheduleService.scrollBackToTop();
                            }
                        }
                    }
                }
            } else {
                alert(ScheduleService.localizations["ValidationError"]);
            }
        });
    },
    setRequireFieldForDaysWithinWeek: function(datePicker) {
        if (typeof datePicker != "undefined" && datePicker != null) {
            var saturdayTimeSelect = $('#saturday-hours');
            var date = datePicker.datepicker('getDate');
            var dayOfWeek = weekday[date.getDay()];
            if (dayOfWeek == "Saturday") {
                saturdayTimeSelect.attr('data-val', true).attr('data-val-required', true);
            } else {
                saturdayTimeSelect.removeAttr('data-val').removeAttr('data-val-required');
                saturdayTimeSelect.removeClass('input-validation-error');
            }
        }
    },
    validateForm: function (currentForm) {
        var selecteds = $('#' + currentForm).find('input[type=checkbox]:checked');
        if (typeof selecteds !== 'undefined' && selecteds !== null && selecteds.length > 0) {
            $('#' + currentForm).removeClass('input-validation-error');
        } else {
            $('#' + currentForm).addClass('input-validation-error');
            return false;
        }
        return true;
    },
    checkValidationOfCurrentForm: function (currentForm) {
        return $('#' + currentForm).find("[data-val=true]:visible:not([type=hidden])").All("valid");
    },
    saveDataOfCurrentForm: function () {
        var result = true;
        var data = ScheduleService.getDataForScheduleService();
        var method = 'ScheduleService';

        $.ajax({
            url: '../../Controllers/ScheduleServiceController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                }
            }
        });

        return result;
    },
    switchForms: function (currentForm, nextForm) {
        $('#' + currentForm).addClass('hidden').removeClass('active');
        $('#' + nextForm).removeClass('hidden').addClass('active');
    },
    getDataForScheduleService: function () {
        var postData = {
            ScheduleDate: '',
            WeeklyHour: '',
            SaturdayHour: '',
            FirstName: '',
            LastName: '',
            PhoneNumber: '',
            Email: '',
            Year: '',
            Make: '',
            Model: '',
            Mileage: '',
            ServiceRequested: '',
            CurrentIssue: '',
            WarrantyStatus: '',
            AdditionalInformation: ''
        };

        postData.ScheduleDate = $('#serviceDate').val();
        postData.WeeklyHour = $('#weekday-hours').val();
        postData.SaturdayHour = $('#saturday-hours').val();
        postData.FirstName = $('#contactFirstName').val();
        postData.LastName = $('#contactLastName').val();
        postData.PhoneNumber = $('#contactPhone').val();
        postData.Email = $('#contactEmail').val();
        postData.Year = $('#vehicleYear').val();
        postData.Make = $('#vehicleMake').val();
        postData.Model = $('#vehicleModel').val();
        postData.Mileage = $('#vehicleMileage').val();
        postData.ServiceRequestList = ScheduleService.getCheckboxesData($('#serviceRequested').find('input[type=checkbox]:checked'));
        postData.CurrentIssueList = ScheduleService.getCheckboxesData($('#currentIssue').find('input[type=checkbox]:checked'));
        postData.WarrantyStatus = $('input[name="serviceWarranty"]:checked').val();
        postData.AdditionalInformation = $('#contactMessage').val();

        return postData;
    },
    getCheckboxesData: function (checkbox) {
        var result = [];
        if (typeof checkbox !== "undefined" && checkbox !== null) {
            for (var i = 0; i < checkbox.length; i++) {
                result.push(checkbox[i].value);
            }
        }
        return result;
    },
    getMovementInfo: function (currentForm) {
        var result = null;
        if (typeof currentForm != "undefined" && currentForm != null) {
            for (var i = 0; i < serviceForms.length; i++) {
                if (serviceForms[i].currentForm === currentForm) {
                    result = serviceForms[i];
                }
            }
        }
        return result;
    },
    scrollBackToTop: function () {
        $(document).scrollTop(0);
    }
};