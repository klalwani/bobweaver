﻿var JobStatusEnum = {
    Triggered: 'Triggered',
    Running: 'Running',
    Failed: 'Failed',
    Completed: 'Completed'
};