﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

$(document).ready(function () {
    incentiveTranslatedUpdate.initialize();
});


var incentiveTranslatedUpdate = {
    initialize: function () {
        incentiveTranslatedUpdate.registerEvents();
    },
    isResetAll: false,
    currentTable: {},
    registerEvents: function () {
        $(document).on('click', '#btnSaveChange', function () {
            incentiveTranslatedUpdate.prepareDataForSubmitting();
            incentiveTranslatedUpdate.submitData();
        });

        $(document).on('click', '#btnReset', function () {
            $('#modal-confirm-reset').modal('show');
        });

        $(document).on('click', '#btnConfirmReset', function () {
            incentiveTranslatedUpdate.prepareDataForSubmitting();
            incentiveTranslatedUpdate.resetIncentiveTranslated();
            $('#modal-confirm-reset').modal('toggle');
        });

        $(document).on('click', '#btnCancelReset', function () {
            $('#modal-confirm-reset').modal('toggle');
        });

        $(document).on('click', '#updateFor label', function () {
            var that = $(this);
            var input = that.find('input');
            var value = input.val();
            var target = input.data('target');
            if (target == 'all') {
                $('#vinSection').removeClass('in').addClass('collapse');
                $('#modelSection').removeClass('in').addClass('collapse');
            }
            else if (target == 'vinSection') {
                $('#vinSection').removeClass('collapse').addClass('in');
                $('#modelSection').removeClass('in').addClass('collapse');
            }
            else if (target == 'modelSection') {
                $('#vinSection').removeClass('in').addClass('collapse');
                $('#modelSection').removeClass('collapse').addClass('in');
            }
        });

        $(document).on('shown.bs.modal', '#incentiveEditControl', function () {
            if (typeof incentiveTranslatedUpdate.programData != "undefined" && incentiveTranslatedUpdate.programData != null) {
                $('#lblProgramName').text(incentiveTranslatedUpdate.programData.name);
                if (incentiveTranslatedUpdate.programData.displaySection != '0' && incentiveTranslatedUpdate.programData.displaySection != '')
                    $('#ddlDisplaySection').val(incentiveTranslatedUpdate.programData.displaySection);
                $('input[name="chkStatus"][value="' + incentiveTranslatedUpdate.programData.status + '"]').click();
                $('#txtPosition').val(incentiveTranslatedUpdate.programData.position);
                $('#txtTranslatedName').val(incentiveTranslatedUpdate.programData.translatedName);

                $('#updateFor label input[value="' + incentiveTranslatedUpdate.programData.updateFor + '"]').click();
                if (incentiveTranslatedUpdate.programData.updateFor == UPDATEFOR.VIN) {
                    $('#txtVin').val(incentiveTranslatedUpdate.programData.updateForVin);
                }
                else if (incentiveTranslatedUpdate.programData.updateFor == UPDATEFOR.MODEL) {
                    $("#ddlModel").unbind("change");
                    $('#ddlModelYear').unbind("change");
                    $('#ddlModel').val(incentiveTranslatedUpdate.programData.updateForModelId);
                    var method = "GetModelSpecificData";
                    $.ajax({
                        url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                        type: 'POST',
                        async: true,
                        dataType: "json",
                        contentType: "application/json",
                        cache: false,
                        timeout: 30000,
                        data: JSON.stringify(incentiveTranslatedUpdate.programData.updateForModelId),
                        success: function (dataResult) {


                           
                            $('#ddlModelYear').empty();
                            $('#ddlModelStyles').empty();

                            
                            var option = '<option value="">StyleID</option>';
                            $('#ddlModelStyles').append(option);


                            option = '<option value="0">Years</option>';
                            $('#ddlModelYear').append(option);

                            var years = dataResult.filterModelSpecicData.YearList;

                            for (var i = 0; i < years.length; i++) {
                                var yearDetail = years[i];
                                option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                                $('#ddlModelYear').append(option);
                            }

                            var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                            for (var i = 0; i < chromeStyles.length; i++) {
                                var chromeStyleDetail = chromeStyles[i];
                                if (incentiveTranslatedUpdate.programData.chromeStyleId == chromeStyleDetail.IdString) {
                                    option = '<option value="' + chromeStyleDetail.IdString + '" selected="selected">' + chromeStyleDetail.Name + '</option>';
                                }
                                else {
                                    option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';

                                }
                                $('#ddlModelStyles').append(option);

                            }


                            $('#ddlModelYear').val(incentiveTranslatedUpdate.programData.chromeStyleIdYear);

                            method = "FilterYearBasedChromeStyleIds";
                            $.ajax({
                                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + incentiveTranslatedUpdate.programData.chromeStyleIdYear + "&updateForModelId=" + incentiveTranslatedUpdate.programData.updateForModelId,
                                type: 'POST',
                                async: true,
                                dataType: "json",
                                contentType: "application/json",
                                cache: false,
                                timeout: 30000,
                                data: JSON.stringify(incentiveTranslatedUpdate.programData),
                                success: function (dataResultYear) {


                                    $('#ddlModelStyles').empty();


                                    var option = '<option value="">StyleID</option>';
                                    $('#ddlModelStyles').append(option);

                                    var chromeStylesBasedYears = dataResultYear.filterYearBasedChromeStyleIds;

                                    for (var i = 0; i < chromeStylesBasedYears.length; i++) {
                                        var chromeStyleDetail = chromeStylesBasedYears[i];
                                        if (incentiveTranslatedUpdate.programData.chromeStyleId == chromeStyleDetail.IdString) {
                                            option = '<option value="' + chromeStyleDetail.IdString + '" selected="selected">' + chromeStyleDetail.Name + '</option>';
                                        }
                                        else {
                                            option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';

                                        }
                                        $('#ddlModelStyles').append(option);
                                    }

                                    $('#ddlModelStyles').val(incentiveTranslatedUpdate.programData.chromeStyleId);

                                }
                            });

                        }
                    });


                    $("#ddlModel").bind("change", function () {
                        if ($(this).val() == "0") {

                            option = '<option value="0">Years</option>';
                            $('#ddlModelYear').append(option);

                            option = '<option value="">StyleID</option>';
                            $('#ddlModelStyles').append(option);
                        }
                        else {
                            var method = "GetModelSpecificData";
                            $.ajax({
                                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                                type: 'POST',
                                async: true,
                                dataType: "json",
                                contentType: "application/json",
                                cache: false,
                                timeout: 30000,
                                data: JSON.stringify($(this).val()),
                                success: function (dataResult) {

                                    $('#ddlModelYear').empty();
                                    $('#ddlModelStyles').empty();

                                    var option = '<option value="">StyleID</option>';
                                    $('#ddlModelStyles').append(option);


                                    option = '<option value="0">Years</option>';
                                    $('#ddlModelYear').append(option);


                                    var years = dataResult.filterModelSpecicData.YearList;

                                    for (var i = 0; i < years.length; i++) {
                                        var yearDetail = years[i];
                                        option = '<option value="' + yearDetail.Id + '">' + yearDetail.Name + '</option>';
                                        $('#ddlModelYear').append(option);
                                    }

                                    var chromeStyles = dataResult.filterModelSpecicData.ChromeStyles;

                                    for (var i = 0; i < chromeStyles.length; i++) {
                                        var chromeStyleDetail = chromeStyles[i];
                                        option = '<option value="' + chromeStyleDetail.IdString + '">' + chromeStyleDetail.Name + '</option>';
                                        $('#ddlModelStyles').append(option);

                                    }
                                }
                            });
                        }
                    });

                    $("#ddlModelYear").bind("change", function () {
                        if ($(this).val() == "0") {

                            $('#ddlModelStyles').empty();

                            var option = '<option value="">StyleID</option>';
                            $('#ddlModelStyles').append(option);
                        }
                        else {
                            method = "FilterYearBasedChromeStyleIds";
                            $.ajax({
                                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method + "&chromeStyleIdYear=" + $(this).val() + "&updateForModelId=" + $("#ddlModel").val(),
                                type: 'POST',
                                async: true,
                                dataType: "json",
                                contentType: "application/json",
                                cache: false,
                                timeout: 30000,
                                data: JSON.stringify($(this).val()),
                                success: function (dataResultYear) {


                                    $('#ddlModelStyles').empty();


                                    var option = '<option value="">StyleID</option>';
                                    $('#ddlModelStyles').append(option);

                                    var chromeStyles = dataResultYear.filterYearBasedChromeStyleIds;

                                    for (var i = 0; i < chromeStyles.length; i++) {
                                        var chromeStyleDetail = chromeStyles[i];
                                        option = '<option value="' + chromeStyleDetail.IdString + '" >' + chromeStyleDetail.Name + '</option>';
                                        $('#ddlModelStyles').append(option);
                                    }
                                }
                            });
                        }
                    });


                }
            }
        });
    },
    prepareDataForSubmitting: function () {
        incentiveTranslatedUpdate.programData.displaySection = $('#ddlDisplaySection').val();
        incentiveTranslatedUpdate.programData.position = $('#txtPosition').val();
        incentiveTranslatedUpdate.programData.translatedName = $('#txtTranslatedName').val();
        if ($('input[name="chkStatus"]:checked')[0].value == "Hidden") {
            incentiveTranslatedUpdate.programData.isActive = false;
        } else {
            incentiveTranslatedUpdate.programData.isActive = true;
        }

        incentiveTranslatedUpdate.programData.updateFor = $('#updateFor label.active input').val();
        incentiveTranslatedUpdate.programData.updateForVin = $('#txtVin').val();
        incentiveTranslatedUpdate.programData.updateForModelId = $('#ddlModel').val();
        incentiveTranslatedUpdate.programData.isResetAll = incentiveTranslatedUpdate.isResetAll;
        incentiveTranslatedUpdate.programData.chromeStyleId = $('#ddlModelStyles').val();
        incentiveTranslatedUpdate.programData.chromeStyleIdYear = $('#ddlModelYear').val();
    },
    submitData: function () {
        var method = 'UpdateIncentiveTranslated';

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdate.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdate.currentTable != 'undefined' && incentiveTranslatedUpdate.currentTable != null)
                        $('.modal').modal('hide');
                        incentiveTranslatedUpdate.currentTable.ajax.reload();
                }
            }
        });
    },
    resetIncentiveTranslated: function () {
        var method = 'ResetIncentiveTranslated';
        var isResetAll = false;

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdate.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdate.currentTable != 'undefined' && incentiveTranslatedUpdate.currentTable != null)
                        $('.modal').modal('hide');
                        incentiveTranslatedUpdate.currentTable.ajax.reload();
                }
            }
        });
    }
};