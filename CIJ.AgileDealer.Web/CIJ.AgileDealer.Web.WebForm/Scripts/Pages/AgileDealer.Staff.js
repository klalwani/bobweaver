﻿$(document).ready(function () {
    staff.initialize();
});
var employeeTemp;

var staff = {
    self: 'staff',
    localizations:{},
    initialize: function () {
        staff.registerEvent();
        staff.setModifyButtonVisible();
        staff.localizations = common.getLocalizations();
    },
    isEditable: function () {
        return $('[id$="CanBeEdit"]').val() == "True";
    },
    currentEmployee: function () {
        return employeeTemp;
    },
    registerEvent: function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var that = $(this);
            var employeeId = that.attr('id');
            var parentDiv = $(that).closest('.staff-card');
            var buttonYes = staff.localizations["ButtonYes"];
            var buttonNo = staff.localizations["ButtonNo"];
            if (typeof employeeId != "undefined" && employeeId != null && employeeId > 0) {
                var employee = {
                    employeeId: employeeId
                };
                $("#dialog-confirm").dialog({
                    resizable: false,
                    modal: true,
                    closeOnEscape: true,
                    show: { effect: "fade", duration: 300 },
                    position: { my: "center", at: "center", of: window },
                    width: "auto",
                    height: "auto",
                    buttons: 
                        [{
                            text: buttonYes, click: function ()
                            {
                                staff.submitData(employee, 'DeleteEmployee', staff.removeStaffAfterDeleted, parentDiv);
                                $(this).dialog("close");
                            }
                        }, 
                        {
                            text: buttonNo, click: function ()
                            {
                                $(this).dialog("close");
                            }
                        }]
                        
                });
            }
        });

        $('.editEmployee').click(function (e) {
            e.preventDefault();
            var that = $(this);
            var id = that.attr('id');
            $('[id$="EmployeeId"]').val(id);
            $("#employee-update").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: staff.localizations["EmployeeUpdateTitle"],
                show: { effect: "fade", duration: 300 },
                position: { my: "center", at: "center", of: window },
                width: "768px",
                height: "auto"
            });
        });
    },
    submitData: function (employee, method, callback, parentDiv) {
        var data = employee;
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/EmployeeController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        if (typeof callback == "function" && typeof callback != "undefined" && callback != null) {
                            callback(parentDiv);
                        }
                    } else {
                        alert(staff.localizations["UpdateFailed"]);
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert(staff.localizations["ErrorMessage"]);
                }
            });
        }
    },
    removeStaffAfterDeleted: function (parentDiv) {
        $(parentDiv).delay(200).fadeOut(1000);
        $(parentDiv).animate({
            "opacity": "0"
        }, {
            "complete": function () {
                $(parentDiv).remove();
            }
        });
    },
    setModifyButtonVisible: function () {
        if (!staff.isEditable()) {
            $('.adminSection').remove();
        }
    }
};