﻿var isPostingBack = false;
var thisUl = 'ul:not([id="tabs"])';
var currentPagingTop;
var currentPagingBottom;
var masked = false;
var allPageSize = 50;
var defaultPageSize = 15;
var reloadTriggerScrollPosition = 0;
var totalResultSet = 0;
var shouldGetMoreDataOnScroll = true;
var hasShownTheSearchSection = false;
var inventoryPosition = 0;
var searchFilterHistory = [];
var currentHistoryIndex = 0;
var isSearchingByHistoryFilter = false;
var isRealTouching = false;
var defaultVehicleSearchCriteria = {
    Vin: '',
    IsNew: '',
    IsUsed: '',
    IsCertified: '',
    IsLift: '',
    IsUpfitted: '',
    SortItem: '',
    Years: [],
    ModelTrims: [],
    Makes: [],
    Prices: [],
    Models: [],
    EngineTranslatedEngines: [],
    BodyStyles: [],
    VehicleBodyStandardBodyStyles: [],
    ExteriorColors: [],
    FuelTypes: [],
    Engines: [],
    Drives: [],
    Mileages: [],
    VehicleFeatures: [],
    RemainingYears: [],
    RemainingMakes: [],
    RemainingPrices: [],
    RemainingModelTrims: [],
    RemainingBodyStyles: [],
    RemainingExteriorColors: [],
    RemainingFuelTypes: [],
    RemainingEngines: [],
    RemainingDrives: [],
    RemainingMileages: [],
    RemainingVehicleFeatures: [],
    PageSize: '',
    PageNumber: '',
    IsExpandType: false,
    IsExpandModel: false,
    IsExpandBodyStyle: false,
    IsViewAll: false,
    IsSelectingPrice: false,
    IsSelectingYear: false,
    IsSelectingMake: false,
    IsSelectingModel: false,
    IsSelectingBody: false,
    IsSelectingStandardBody: false,
    IsSelectingFuel: false,
    IsSelectingColor: false,
    IsSelectingTransmission: false,
    IsSelectingEngine: false,
    IsSelectingDrive: false,
    IsSelectingVehiclefeatures: false,
    IsSelectingTrim: false,
    IsSelectingMileage: false,
    IsModelChange: false,
    InLitmitedMode: false

};
var vehicleSearchCriteria = {
    Vin: '',
    IsNew: '',
    IsUsed: '',
    IsCertified: '',
    IsLift: '',
    IsUpfitted: '',
    SortItem: '',
    Years: [],
    ModelTrims: [],
    Makes: [],
    Prices: [],
    Models: [],
    EngineTranslatedEngines: [],
    BodyStyles: [],
    VehicleBodyStandardBodyStyles: [],
    ExteriorColors: [],
    FuelTypes: [],
    Engines: [],
    Drives: [],
    Mileages: [],
    RemainingYears: [],
    RemainingMakes: [],
    RemainingPrices: [],
    RemainingModelTrims: [],
    VehicleFeatures: [],
    RemainingBodyStyles: [],
    RemainingExteriorColors: [],
    RemainingFuelTypes: [],
    RemainingEngines: [],
    RemainingDrives: [],
    RemainingMileages: [],
    RemainingVehicleFeatures: [],
    PageSize: '',
    PageNumber: '',
    IsExpandType: false,
    IsExpandModel: false,
    IsExpandBodyStyle: false,
    IsViewAll: false,
    IsSelectingPrice: false,
    IsSelectingYear: false,
    IsSelectingMake: false,
    IsSelectingModel: false,
    IsSelectingBody: false,
    IsSelectingStandardBody: false,
    IsSelectingFuel: false,
    IsSelectingColor: false,
    IsSelectingTransmission: false,
    IsSelectingEngine: false,
    IsSelectingDrive: false,
    IsSelectingTrim: false,
    IsSelectingMileage: false,
    IsModelChange: false,
    InLitmitedMode: false,
    IsSelectingVehiclefeatures: false,
};


var vehicleMasterDataCounter;

$(window).resize(function () {
    searchControl.resizeWindows();
});


$(document).ready(function () {
    searchControl.resizeWindows();
    searchControl.preInitialize();
});


window.onload = function () {
    setTimeout(function () {
        if (typeof history.pushState === "function") {
            history.pushState('original', null, location.href);
            window.onpopstate = function (e) {
                isRealTouching = false;
                //if has history
                if (typeof searchFilterHistory != "undefined" &&
					searchFilterHistory != null &&
					searchFilterHistory.length > 0) {
                    var currentIndex = searchFilterHistory.length - 1;
                    currentHistoryIndex = currentIndex;
                    if (currentHistoryIndex > 0) {
                        var currentSearchFilterItem = searchFilterHistory[currentIndex - 1].data;
                        if (typeof currentSearchFilterItem != "undefined" &&
							currentSearchFilterItem != null) {

                            isSearchingByHistoryFilter = true;
                            searchControl
								.pressFilterBySearchCriteria(currentSearchFilterItem.VehicleSearchCriteria);
                            searchControl
								.resetToSpecificPage(currentSearchFilterItem.VehicleSearchCriteria.PageNumber);
                            if (typeof currentSearchFilterItem.VehicleSearchCriteria.Vin != "undefined" &&
								currentSearchFilterItem.VehicleSearchCriteria.Vin != null) {
                                $('#txtSearch').val(currentSearchFilterItem.VehicleSearchCriteria.Vin);
                            }
                            $('#txtSearch').blur();
                            searchControl.reloadData();
                            searchControl
								.pushStateForSearch(currentSearchFilterItem,
									currentSearchFilterItem.VehicleSearchCriteria.PageNumber);
                            isSearchingByHistoryFilter = false;
                            searchFilterHistory.splice(currentIndex, 1);
                        }
                    } else {
                        window.history.back();
                    }
                } else {
                    window.history.back();
                }
            };
        }
    },
		500);
}
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}
var searchControl = {
    preInitialize: function () {
        try {
            var rememberHistory = $.urlParam('rememberHistory');
            if (typeof rememberHistory != "undefined" && rememberHistory != null && rememberHistory != '') {
                rememberHistory = 'true';
            } else {
                rememberHistory = 'false';
            }
        }
        catch (ex) {
            rememberHistory = 'false';
        }
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'GetVehicleHistoryIntoSession',
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(rememberHistory),
            success: function (dataResult) {
                if (dataResult.isSuccess) {
                    $('[id$="previousVehicleData"]').val(dataResult.vehicle);
                    searchControl.initialize();
                }
            },
            error: function () {
            }
        });
    },
    resizeWindows: function () {
        $('#MainMenu').removeClass('modal').removeClass('left').removeClass('fade');
        $('#MainMenu #row').removeClass('modal-dialog');
        if ($(window).width() <= 628) {
            $('#MainMenu').addClass('modal').addClass('left').addClass('fade');
            $('#MainMenu #row').addClass('modal-dialog');
        } else {
            $('#MainMenu').removeClass('modal').removeClass('left').removeClass('fade');
            $('#MainMenu #row').removeClass('modal-dialog');
            $('#MainMenu').show();
        }
    },
    localizations: {},
    initialize: function () {
        searchControl.scrollWindowToTopAsDefault();
        searchControl.getInventoryLocation();
        searchControl.localizations = common.getLocalizations();
        searchControl.registerEvents();
        searchControl.setDefaultFilter();
        searchControl.setDefaultMasterDataCounter();
        searchControl.reloadData();
        searchControl.hookUpPagination();
        searchControl.preventPagingToPostBack();

    },
    scrollWindowToTopAsDefault: function () {
        $(window).scrollTop(0);
    },
    getInventoryLocation: function () {
        inventoryPosition = $('.inventory-content').offset().top;
    },
    unmaskedInput: function () {
        $(".price,.rebate,.msrp,.dealersaving").unmask();
    },
    setDefaultMasked: function () {
        $(".price,.rebate,.msrp,.dealersaving").each(function () {
            var that = $(this);
            var result = searchControl.currencyFormat(that.text());
            that.text(result);
        });
    },
    setNormalThousand: function (num) {
        return searchControl.formatThousand(num);
    },
    currencyFormat: function (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    },
    formatThousand: function (num) {
        return num.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
    },
    setDefaultFilter: function () {
        $('#txtSearch').val('');
        var defaultTextSearch = $('[id$="HdfSearchText"]').val();
        if (typeof defaultTextSearch != "undefined" && defaultTextSearch != null && defaultTextSearch != '') {
            $('#txtSearch').val(defaultTextSearch);

            $('[id$="HdfSearchText"]').val(''); // Just set for only 1 first time
        }
        var defaultFilter = '';
        var textSearch = '';
        defaultFilter = JSON.parse($('[id$="defaultFilter"]').val());
        $.extend(true, defaultVehicleSearchCriteria, defaultFilter);
        if (typeof $('[id$="previousVehicleData"]').val() != 'undefined' && $('[id$="previousVehicleData"]').val() != null && $('[id$="previousVehicleData"]').val() != '') {
            var previousVehicleData = $.parseJSON($('[id$="previousVehicleData"]').val());
            defaultFilter = previousVehicleData;
            $('#txtSearch').val(previousVehicleData.Vin);
            //searchControl.resetToSpecificPage(previousVehicleData.PageNumber);
        }


        $('#txtSearch').blur();

        vehicleSearchCriteria = defaultFilter;

        if (vehicleSearchCriteria.SortItem != '')
            $('#ddlSort').val(vehicleSearchCriteria.SortItem);

        $(thisUl).find('li.active:not(.list-group-custom-default)').removeClass('active');
        $(thisUl).find('li.list-group-custom-default').addClass('active');
        if (typeof vehicleSearchCriteria != "undefined" && vehicleSearchCriteria != null) {
            searchControl.pressFilterBySearchCriteria(vehicleSearchCriteria);
        }
    },
    pressFilterBySearchCriteria: function (tempVehicleSearchCriteria) {
        if (tempVehicleSearchCriteria
			.IsNew ||
			tempVehicleSearchCriteria.IsUsed ||
			tempVehicleSearchCriteria.IsCertified) {
            searchControl.setFilterBackToNormal('type');
            $('ul[data-type="type"]').find('li.list-group-custom-default').removeClass('active');
            if (tempVehicleSearchCriteria.IsNew) {
                $('ul[data-type="type"]').find('li[value="new"]').addClass('active');
            }
            if (tempVehicleSearchCriteria.IsUsed) {
                $('ul[data-type="type"]').find('li[value="used"]').addClass('active');
            }
            if (tempVehicleSearchCriteria.IsCertified) {
                $('ul[data-type="type"]').find('li[value="certified"]').addClass('active');
            }
        } else {
            searchControl.setFilterBackToNormal('type');
        }

        if (tempVehicleSearchCriteria.IsLift) {
            searchControl.setFilterBackToNormal('lift');
            $('ul[data-type="lift"]').find('li.list-group-custom-default').removeClass('active');
            if (tempVehicleSearchCriteria.IsLift) {
                $('ul[data-type="lift"]').find('li[value="isLift"]').addClass('active');
            }
        } else {
            searchControl.setFilterBackToNormal('lift');
        }


        if (tempVehicleSearchCriteria.IsUpfitted) {
            searchControl.setFilterBackToNormal('upfitted');
            $('ul[data-type="upfitted"]').find('li.list-group-custom-default').removeClass('active');
            if (tempVehicleSearchCriteria.IsLift) {
                $('ul[data-type="upfitted"]').find('li[value="isLift"]').addClass('active');
            }
        } else {
            searchControl.setFilterBackToNormal('upfitted');
        }

        if (tempVehicleSearchCriteria.Makes != null && tempVehicleSearchCriteria.Makes.length > 0) {
            var currentMake;
            searchControl.setFilterBackToNormal('make');
            $('ul[data-type="make"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Makes.length; i++) {
                currentMake = tempVehicleSearchCriteria.Makes[i];
                $('ul[data-type="make"]').find('li[value="' + currentMake + '"]').addClass('active');
            }
        } else {
            searchControl.setFilterBackToNormal('make');
        }

        //if (tempVehicleSearchCriteria.Models != null && tempVehicleSearchCriteria.Models.length > 0) {
        //    var currentModel;
        //    var parent = $('ul[data-type="model"]');
        //    parent.find('li.list-group-custom-default').removeClass('active');
        //    for (var i = 0; i < tempVehicleSearchCriteria.Models.length; i++) {
        //        currentModel = tempVehicleSearchCriteria.Models[i];
        //        var currentLi = $('ul[data-type="model"]').find('li:not(.sublistItem)[value="' + currentModel + '"]');
        //        currentLi.addClass('active');
        //        currentLi.find('span:not(.badge)').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        //        if (currentLi.attr('hassublist') == "True") {
        //            var currentId = currentLi.attr('parentid');
        //            searchControl.expandChildrenItem(parent, currentId);
        //        }
        //    }
        //}

        if (tempVehicleSearchCriteria.ModelTrims != null && tempVehicleSearchCriteria.ModelTrims.length > 0 && (tempVehicleSearchCriteria.IsSelectingTrim || tempVehicleSearchCriteria.IsSelectingModel)) {
            var currentModelTrim;
            var parentModelTrim = $('ul[data-type="model"]');
            searchControl.setFilterBackToNormal('model');
            parentModelTrim.find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.ModelTrims.length; i++) {
                currentModelTrim = tempVehicleSearchCriteria.ModelTrims[i];
                var currentModelTrimLi = $('ul[data-type="model"]').find('li:not(.sublistItem)[value="' + currentModelTrim.modelId + '"]');
                currentModelTrimLi.addClass('active');
                currentModelTrimLi.find('span:not(.badge)')
					.addClass('glyphicon-minus')
					.removeClass('glyphicon-plus');
                if ($(currentModelTrimLi[0]).attr('hassublist') == "True" || $(currentModelTrimLi[1]).attr('hassublist') == "True") {//trick on IE
                    var currentModelTrimId = $(currentModelTrimLi[0]).attr('parentid');
                    if (typeof currentModelTrimId == "undefined" || currentModelTrimId == null) {
                        currentModelTrimId = $(currentModelTrimLi[1]).attr('parentid');;
                    }
                    searchControl.expandChildrenItem(parentModelTrim, currentModelTrimId);
                    searchControl.expandSubChildItem(currentModelTrimId, currentModelTrim.trimId);
                }
            }
            tempVehicleSearchCriteria.IsSelectingTrim = false;
        } else {
            searchControl.setFilterBackToNormal('model');
        }

        if (tempVehicleSearchCriteria.VehicleBodyStandardBodyStyles != null && tempVehicleSearchCriteria.VehicleBodyStandardBodyStyles.length > 0 && (tempVehicleSearchCriteria.IsSelectingStandardBody || tempVehicleSearchCriteria.IsSelectingBody)) {
            var currentStyle;
            searchControl.setFilterBackToNormal('style');
            var parentBodyStyleTrim = $('ul[data-type="style"]');
            parentBodyStyleTrim.find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.VehicleBodyStandardBodyStyles.length; i++) {
                currentStyle = tempVehicleSearchCriteria.VehicleBodyStandardBodyStyles[i];
                var currentBodyStyleLi = $('ul[data-type="style"]').find('li:not(.sublistItem)[value="' + currentStyle.standardBodyId + '"]');
                currentBodyStyleLi.addClass('active');
                currentBodyStyleLi.find('span:not(.badge)')
					.addClass('glyphicon-minus')
					.removeClass('glyphicon-plus');
                if ($(currentBodyStyleLi[0]).attr('hassublist') == "True" || $(currentBodyStyleLi[1]).attr('hassublist') == "True") {//trick on IE
                    var currentBodyStyleId = $(currentBodyStyleLi[0]).attr('parentid');
                    if (typeof currentBodyStyleId == "undefined" || currentBodyStyleId == null) {
                        currentBodyStyleId = $(currentBodyStyleLi[1]).attr('parentid');;
                    }
                    searchControl.expandChildrenItem(parentBodyStyleTrim, currentBodyStyleId);
                    searchControl.expandSubChildItem(currentBodyStyleId, currentStyle.bodyStyleTypeId);
                }
            }
            tempVehicleSearchCriteria.IsSelectingStandardBody = false;
        } else {
            searchControl.setFilterBackToNormal('style');
        }

        if (tempVehicleSearchCriteria.Prices != null && tempVehicleSearchCriteria.Prices.length > 0) {
            var currentPrice;
            searchControl.setFilterBackToNormal('price');
            $('ul[data-type="price"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Prices.length; i++) {
                currentPrice = tempVehicleSearchCriteria.Prices[i];
                $('ul[data-type="price"]').find('li[value="' + currentPrice + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingPrice = false;
        } else {
            searchControl.setFilterBackToNormal('price');
        }

        if (tempVehicleSearchCriteria.Years != null && tempVehicleSearchCriteria.Years.length > 0) {
            var currentYear;
            searchControl.setFilterBackToNormal('year');
            $('ul[data-type="year"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Years.length; i++) {
                currentYear = tempVehicleSearchCriteria.Years[i];
                $('ul[data-type="year"]').find('li[value="' + currentYear + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingYear = false;
        } else {
            searchControl.setFilterBackToNormal('year');
        }

        if (tempVehicleSearchCriteria.Mileages != null && tempVehicleSearchCriteria.Mileages.length > 0) {
            var currentMilage;
            searchControl.setFilterBackToNormal('mile');
            $('ul[data-type="mileage"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Mileages.length; i++) {
                currentMilage = tempVehicleSearchCriteria.Mileages[i];
                $('ul[data-type="mileage"]').find('li[value="' + currentMilage + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingMileage = false;
        } else {
            searchControl.setFilterBackToNormal('mile');
        }

        if (tempVehicleSearchCriteria.EngineTranslatedEngines != null &&
			tempVehicleSearchCriteria.EngineTranslatedEngines.length > 0) {
            var currentEngine;
            var parentEngineTranslated = $('ul[data-type="engine"]');
            searchControl.setFilterBackToNormal('engine');
            parentEngineTranslated.find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.EngineTranslatedEngines.length; i++) {
                currentEngine = tempVehicleSearchCriteria.EngineTranslatedEngines[i];
                var parentEngineTranslatedLi = $('ul[data-type="engine"]')
					.find('li:not(.sublistItem)[value="' + currentEngine.engineTranslatedId + '"]');
                parentEngineTranslatedLi.addClass('active');
                parentEngineTranslatedLi.find('span:not(.badge)')
					.addClass('glyphicon-minus')
					.removeClass('glyphicon-plus');
                if ($(parentEngineTranslatedLi[0]).attr('hassublist') == "True" || $(parentEngineTranslatedLi[1]).attr('hassublist') == "True") {//trick on IE
                    var currentEngineId = $(parentEngineTranslatedLi[0]).attr('parentid');
                    if (typeof currentEngineId == "undefined" || currentEngineId == null) {
                        currentEngineId = $(parentEngineTranslatedLi[1]).attr('parentid');
                    }
                    searchControl.expandChildrenItem(parentEngineTranslated, currentEngineId);
                    searchControl.expandSubChildItem(currentEngineId, currentEngine.engineId);
                }
            }
            tempVehicleSearchCriteria.IsSelectingEngine = false;
        } else {
            searchControl.setFilterBackToNormal('engine');
        }

        if (tempVehicleSearchCriteria.FuelTypes != null && tempVehicleSearchCriteria.FuelTypes.length > 0) {
            var currentFuel;
            searchControl.setFilterBackToNormal('fuel');
            $('ul[data-type="fuel"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.FuelTypes.length; i++) {
                currentFuel = tempVehicleSearchCriteria.FuelTypes[i];
                $('ul[data-type="fuel"]').find('li[value="' + currentFuel + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingFuel = false;
        } else {
            searchControl.setFilterBackToNormal('fuel');
        }

        if (tempVehicleSearchCriteria.Drives != null && tempVehicleSearchCriteria.Drives.length > 0) {
            var currentDrive;
            searchControl.setFilterBackToNormal('drive');
            $('ul[data-type="drive"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Drives.length; i++) {
                currentDrive = tempVehicleSearchCriteria.Drives[i];
                $('ul[data-type="drive"]').find('li[value="' + currentDrive + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingDrive = false;
        } else {
            searchControl.setFilterBackToNormal('drive');
        }

        if (tempVehicleSearchCriteria.VehicleFeatures != null && tempVehicleSearchCriteria.VehicleFeatures.length > 0) {

            var currentvehicleFeature;
            searchControl.setFilterBackToNormal('vehiclefeatures');
            $('ul[data-type="vehiclefeatures"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.VehicleFeatures.length; i++) {
                currentvehicleFeature = tempVehicleSearchCriteria.VehicleFeatures[i];
                $('ul[data-type="vehiclefeatures"]').find('li[value="' + currentvehicleFeature + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingVehiclefeatures = false;
        } else {
            searchControl.setFilterBackToNormal('vehiclefeatures');
        }

        if (tempVehicleSearchCriteria.ExteriorColors != null && tempVehicleSearchCriteria.ExteriorColors.length > 0) {
            var currentColor;
            searchControl.setFilterBackToNormal('color');
            $('ul[data-type="color"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.ExteriorColors.length; i++) {
                currentColor = tempVehicleSearchCriteria.ExteriorColors[i];
                $('ul[data-type="color"]').find('li[value="' + currentColor + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingColor = false;
        } else {
            searchControl.setFilterBackToNormal('color');
        }

        if (tempVehicleSearchCriteria.Transmissions != null && tempVehicleSearchCriteria.Transmissions.length > 0) {
            var currentTransmission;
            searchControl.setFilterBackToNormal('transmission');
            $('ul[data-type="transmission"]').find('li.list-group-custom-default').removeClass('active');
            for (var i = 0; i < tempVehicleSearchCriteria.Transmissions.length; i++) {
                currentTransmission = tempVehicleSearchCriteria.Transmissions[i];
                $('ul[data-type="transmission"]').find('li[value="' + currentTransmission + '"]').addClass('active');
            }
            tempVehicleSearchCriteria.IsSelectingTransmission = false;
        } else {
            searchControl.setFilterBackToNormal('transmission');
        }

        if (!siteMaster.isMobile()) {
            if (tempVehicleSearchCriteria.IsExpandType ||
				tempVehicleSearchCriteria.IsExpandModel ||
				tempVehicleSearchCriteria.IsExpandBodyStyle) {
                if (tempVehicleSearchCriteria.IsExpandModel) {
                    if (!$('div[href="#collapseModel"] span').hasClass('glyphicon-minus'))
                        $('div[href="#collapseModel"]').click();
                }
                if (tempVehicleSearchCriteria.IsExpandBodyStyle) {
                    if (!$('div[href="#collapseStyle"] span').hasClass('glyphicon-minus'))
                        $('div[href="#collapseStyle"] span').click();
                }
            }
            if (tempVehicleSearchCriteria.Makes != null && tempVehicleSearchCriteria.Makes.length > 0) {
                if (!$('div[href="#collapseMake"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseMake"] span').click();
            }
            if (tempVehicleSearchCriteria.Prices != null && tempVehicleSearchCriteria.Prices.length > 0) {
                if (!$('div[href="#collapsePrice"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapsePrice"] span').click();
            }
            if (tempVehicleSearchCriteria.Years != null && tempVehicleSearchCriteria.Years.length > 0) {
                if (!$('div[href="#collapseYear"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseYear"] span').click();
            }
            if (tempVehicleSearchCriteria.Mileages != null && tempVehicleSearchCriteria.Mileages.length > 0) {
                if (!$('div[href="#collapseMile"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseMile"] span').click();
            }
            if (tempVehicleSearchCriteria.EngineTranslatedEngines != null &&
				tempVehicleSearchCriteria.EngineTranslatedEngines.length > 0) {
                if (!$('div[href="#collapseEngine"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseEngine"] span').click();
            }
            if (tempVehicleSearchCriteria.FuelTypes != null && tempVehicleSearchCriteria.FuelTypes.length > 0) {
                if (!$('div[href="#collapseFuel"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseFuel"] span').click();
            }
            if (tempVehicleSearchCriteria.Drives != null && tempVehicleSearchCriteria.Drives.length > 0) {
                if (!$('div[href="#collapseDrive"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseDrive"] span').click();
            }

            if (tempVehicleSearchCriteria.VehicleFeatures != null && tempVehicleSearchCriteria.VehicleFeatures.length > 0) {

                if (!$('div[href="#collapsevehiclefeatures"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapsevehiclefeatures"] span').click();
            }

            if (tempVehicleSearchCriteria.ExteriorColors != null && tempVehicleSearchCriteria.ExteriorColors.length > 0) {
                if (!$('div[href="#collapseColor"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseColor"] span').click();
            }
            if (tempVehicleSearchCriteria.Transmissions != null && tempVehicleSearchCriteria.Transmissions.length > 0) {
                if (!$('div[href="#collapseTransmission"] span').hasClass('glyphicon-minus'))
                    $('div[href="#collapseTransmission"] span').click();
            }
        }
    },
    expandSubChildItem: function (parentId, childId) {
        $('div.list-group-submenu[id="' + parentId + '"] li[value="' + childId + '"]').addClass('active');
    },
    expandChildrenItem: function (parent, parentId) {
        if ($(parent).find('li[parentId="' + parentId + '"]').hasClass('active')) {
            $(parent).find('.list-group-submenu[id="' + parentId + '"]').addClass('in');
        }
    },
    collapseAllChildItem: function (parent) {
        $('ul[data-type="' + parent + '"]').find('.list-group-item').removeClass('active');
    },
    collapseAllSubChildrenItem: function (parent) {
        $('ul[data-type="' + parent + '"]').find('.list-group-submenu').removeClass('in');
    },
    setDefaultMasterDataCounter: function () {
        var defaultMasterDataCounter = $('[id$="defaultMasterDataCounter"]').val();
        vehicleMasterDataCounter = $.parseJSON(defaultMasterDataCounter);
    },
    setDefaultSelectedItems: function () {
    },
    generateCurrentUrl: function () {
        var currentPage = $('[id$="currentPage"]').val();
        var currentOriginalUrl = location.pathname;
        var linkQuote = '?';
        var keyworkString = searchControl.modifyUrlByQuerySearchString();
        if (typeof keyworkString != "undefined" && keyworkString != null & keyworkString != '') {
            currentOriginalUrl = keyworkString;
            linkQuote = '&';
        }
        currentOriginalUrl = currentOriginalUrl + linkQuote + 'page=' + currentPage;
        return currentOriginalUrl;
    },
    preventPagingToPostBack: function () {
        $(document).on('click', 'ul.pagination.bootpag li a', function (e) {
            var that = $(this);
            var currentPage = $('[id$="currentPage"]').val();
            var currentUrl = searchControl.generateCurrentUrl();
            siteMaster.updatePageTitleWhenPagingOccur(currentPage);
            e.preventDefault();
            window.history.pushState(null, null, currentUrl);
        });
    },
    checkIsLoadingNewPage: function () {
        //if (window.location.search.indexOf('page') == -1) {
        //    localStorage.setItem('SearchData', '');
        //    localStorage.setItem('SearchDataText', '');
        //}
    },
    registerEvents: function () {
        $('#ddlSort')
			.change(function (index, item) {
			    var that = $(this);
			    searchControl.reloadData();
			});

        $('.checkboxes.model-list-categories').each(function (index, item) {
            var that = $(this);
            var height = that.height();
            if (height > 100) {
                var defaultHeight = height / 3;
                var actualHeight = defaultHeight + (((defaultHeight / 100) * 30));
                that.height(actualHeight);
            }
        });

        if ($('[id$="IsSetPagingVisible"]').val() === 'false') {
            $('.vsr-pagenation').hide();
        }

        searchControl.registerButtonsClick();
        searchControl.registerScrollEvent();
        searchControl.registerEventForPriceAndRebate();
    },
    modifyUrlByQuerySearchString: function () {
        var result = '';
        var defaultTextSearch = $('[id$="HdfSearchText"]').val();
        if (typeof defaultTextSearch != "undefined" && defaultTextSearch != null && defaultTextSearch != '') {
            var temp = defaultTextSearch.replace(' ', '+');
            result = '?keywords=' + temp;
        }
        return result;
    },
    modifyUrlForPageOne: function () {
        var result = '';
        var currentQueryString = location.href;
        if (typeof currentQueryString != "undefined" && currentQueryString != null && currentQueryString != '') {
            if (currentQueryString.indexOf('page') == -1) {
                result = 'page=1';
            }
        }
        return result;
    },
    modifyPagingRel: function () {
        $('ul.pagination.bootpag li a').each(function (index, item) {
            var that = $(this);
            var currentLi = that.closest('li');
            if (currentLi.hasClass('next')) {
                currentLi.attr('rel', 'next');
            }
            if (currentLi.hasClass('prev')) {
                currentLi.attr('rel', 'prev');
            }
        });
    },
    pushStateForSearch: function (data, page, rememberHistory) {
        var originalUrl = siteMaster.currentUrl() + location.pathname;

        var params = {};

        var defaultTextSearch = $('[id$="HdfSearchText"]').val();
        if (typeof defaultTextSearch != "undefined" && defaultTextSearch != null && defaultTextSearch != '') {
            var temp = defaultTextSearch.replace(' ', '+');
            params.keywords = temp;
        }

        params.page = page;

        var currentHref = originalUrl + "?" + decodeURIComponent($.param(params));
        if (typeof rememberHistory != "undefined" && rememberHistory != null && rememberHistory != '') {
            currentHref = currentHref + rememberHistory;
        }
        window.history.pushState(data, null, currentHref);
    },
    resetToSpecificPage: function (page) {
        if (typeof page != "undefined" && page != null && page != 0) {
            $('[id$="currentPage"]').val(page);
            $(currentPagingTop).find('li.active').removeClass('active');
            $(currentPagingBottom).find('li.active').removeClass('active');
            $(currentPagingTop).find('li[data-lp="' + page + '"]:not(.prev)').addClass('active');
            $(currentPagingBottom).find('li[data-lp="' + page + '"]:not(.prev)').addClass('active');
            $('#pageNumber').attr('currentPage', page);

        }
    },
    resetToPageOne: function () {
        $(currentPagingTop).find('li.active').removeClass('active');
        $(currentPagingBottom).find('li.active').removeClass('active');
        $(currentPagingTop).find('li[data-lp="1"]:not(.prev)').addClass('active');
        $(currentPagingBottom).find('li[data-lp="1"]:not(.prev)').addClass('active');
        $('[id$="currentPage"]').val("1");
        $('#pageNumber').attr('currentPage', '1');
        var originalUrl = siteMaster.currentUrl() + location.pathname;
        var params = {};

        var defaultTextSearch = $('[id$="HdfSearchText"]').val();
        if (typeof defaultTextSearch != "undefined" && defaultTextSearch != null && defaultTextSearch != '') {
            var temp = defaultTextSearch.replace(' ', '+');
            params.keywords = temp;
        }

        params.page = 1;

        var currentHref = originalUrl + "?" + decodeURIComponent($.param(params));
        window.history.replaceState(null, null, currentHref);
    },
    disableAndSetDefaultForPaging: function () {
        searchControl.resetToPageOne();
        $('ul.pagination li:not(.prev)').toggleClass('disabled');
    },
    performSearch: function (searchText) {
        if (typeof searchText != 'undefined' && searchText != null && searchText != '') {
            var temp = searchText.toLowerCase();
            var formatedText = searchText.replace(' ', '+');
            var pageURL = "inventory";
            if (window.location.toString().toLowerCase().indexOf("/new") > -1)
                pageURL = "new";
            else if (window.location.toString().toLowerCase().indexOf("/used") > -1)
                pageURL = "used";
            else if (window.location.toString().toLowerCase().indexOf("/certified") > -1)
                pageURL = "certified";
            var url = '/' + pageURL + '?keywords=' + formatedText;
            location.href = url;
        }
    },
    toogleMainMenu: function () {
        $('#MainMenu').css('opacity', 0.1, 0);
        $('#MainMenu').toggle("slide", { direction: "left" }, 400, function () {
            $(this).css('opacity', 1, 0);
            ///$(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                //$(this).focus();
            }
            $('body').block({ message: null });

        });
    },
    registerButtonsClick: function () {

        $('#new-vehicle').click(function () {
            //if ($('#MainMenu').hasClass('in')) {
            //    $('#MainMenu').removeClass('in');
            //}
        });

        $('#btnEnbleFilter').click(function () {
            $('#MainMenu').addClass('left').addClass('fade');
            //searchControl.toogleMainMenu();
        });

        $('#MainMenu').on('focusout', function () {


        });


        $(document).on('touchstart mousedown', '.btnInfo,.vehicleCardTitle,.lazy,#btnEdit', function () {
            var rememberHistory = '&rememberHistory=true';
            $('[id$="isFirstLoad"]').val('false');

            searchControl.pushStateForSearch(vehicleSearchCriteria, vehicleSearchCriteria.PageNumber, rememberHistory);

        });
        $('#txtSearch').on('keydown', function (e) {
            var searchText = $('#txtSearch').val();
            searchText = $.trim(searchText);

            if (e.keyCode == 13) {
                e.preventDefault();
                e.stopPropagation();
                $('#txtSearch').val(searchText);
                searchControl.performSearch(searchText);
            }

        });
        $('#btnSearchVin').click(function () {
            var that = $(this);
            var searchText = $('#txtSearch').val();
            searchText = $.trim(searchText);
            $('#txtSearch').val(searchText);
            if (typeof searchText != "undefined" && searchText != '') {
                vehicleSearchCriteria.Vin = $.trim(searchText);
            }
            reloadTriggerScrollPosition = 0;
            searchControl.performSearch(searchText);
        });
        $('#newSearch').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (window.location.toString().toLowerCase().indexOf("/new") > -1)
                location.href = "/new";
            else if (window.location.toString().toLowerCase().indexOf("/used") > -1)
                location.href = "/used";
            else if (window.location.toString().toLowerCase().indexOf("/certified") > -1)
                location.href = "/certified";
            else
                location.href = '/inventory';
            //vehicleSearchCriteria = defaultVehicleSearchCriteria;
            //searchControl.pressFilterBySearchCriteria(vehicleSearchCriteria);
            //searchControl.reloadData();
        });

        $('#btnRemoveSearchText').click(function () {
            $('#txtSearch').val('');
            searchControl.reloadData();
        });

        $('.has-clear input[type="text"]').on('input propertychange blur', function () {
            var $this = $(this);
            var visible = Boolean($this.val());
            $this.siblings('.form-control-clear').toggleClass('hidden', !visible);
        }).trigger('propertychange');

        $('.form-control-clear').click(function () {
            $(this).siblings('input[type="text"]').val('')
			  .trigger('propertychange').focus();
        });

        $("[name='my-checkbox']").bootstrapSwitch();

        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
            vehicleSearchCriteria.IsViewAll = state;
            //searchControl.resetToPageOne();
            reloadTriggerScrollPosition = 0;


            if (vehicleSearchCriteria.IsViewAll) {
                $('[id$="pageSize"]').val(allPageSize);
            } else {
                $('[id$="pageSize"]').val(defaultPageSize);
            }


            searchControl.reloadData();
            $("[name='my-checkbox']").bootstrapSwitch(state);
            searchControl.disableAndSetDefaultForPaging();
        });

        $('span.parentItem').on('click', function (e) {
            var that = $(this);
            that.closest('li').click();
        });

        $('.panel-heading').on('click', function (e) {
            var that = $(this);

            var child = that.find('span');
            if (!child.hasClass('glyphicon-minus')) {
                child.addClass('glyphicon-minus').removeClass('glyphicon-plus');
            } else {
                child.removeClass('glyphicon-minus').addClass('glyphicon-plus');
            }
        });


        $('.list-group-item').on('click', function (e) {
           
            var stopLoadingOnCurrentPage = false;
            var that = $(this);
            var hasSubList = that.attr('hassublist') == "True";
            var isSubList = that.hasClass('sublistItem');
            var parentId = that.attr('parentid');
            var ul = that.closest('ul');
            var type = ul.data('type');
            var divParentId = that.closest('div.list-group-submenu').attr('id');

            if ($(e.target).is('span.glyphicon')) {
                return;
            }

            var parent = that.closest(".list-group");
            var parentOfSubList = parent.find('li[parentId="' + divParentId + '"]');

            if ($(e.target).hasClass('active')) {
                $(e.target).removeClass('active');
                if (hasSubList) {
                    parent.find('div[id="' + parentId + '"] li').removeClass('active');
                }
            } else {
                if (isSubList)
                    parent.find('div[id="' + divParentId + '"] li').removeClass('active');
                $(e.target).addClass('active');
            }

            var child = that.children('span.glyphicon');
            if (!that.hasClass('active')) {
                if (!child.hasClass('glyphicon-minus')) {
                    child.addClass('glyphicon-minus').removeClass('glyphicon-plus');
                } else {
                    child.removeClass('glyphicon-minus').addClass('glyphicon-plus');
                }
            } else {
                child.addClass('glyphicon-minus').removeClass('glyphicon-plus');
            }

            if (!that.hasClass('list-group-custom-default')) {
                parent.find('.list-group-custom-default').removeClass("active");
            } else {
                parent.find('.list-group-item').removeClass("active");
                parent.find('span.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
                parent.find('.list-group-submenu').removeClass('in');
            }

            //Check if parent of sublist has not checked then check it
            if (isSubList) {
                if (!$(parentOfSubList).hasClass('active')) {
                    $(parentOfSubList).addClass('active');
                }
            }

            if (!isSubList && hasSubList) {
                if ($(parent).find('li[parentId="' + parentId + '"]').hasClass('active'))
                    $(parent).find('.list-group-submenu[id="' + parentId + '"]').addClass('in');
                else
                    $(parent).find('.list-group-submenu[id="' + parentId + '"]').removeClass('in');
            }

            //No Selected
            if (parent.find('.list-group-item.active').length == 0) {
                parent.find('.list-group-custom-default').addClass("active");
            }
            searchControl.setSelectingFlagToDefault();
            if (!that.hasClass('list-group-custom-default')) {
                searchControl.setSelectingFlag(type);
                isRealTouching = true;
            }
            //else {
            //    if (type == 'model') {
            //        location.href = '/inventory';
            //        stopLoadingOnCurrentPage = true;
            //    }
            //}
            if (!stopLoadingOnCurrentPage) {
                searchControl.resetToPageOne();
                searchControl.reloadData();
            }
        });

        $('span.badge').click(function () {
         
            var that = $(this);
            that.closest('li').click();
        });

        $('.non-pagenation').click(function () {
            var that = $(this);
            //vehicleSearchCriteria.IsViewAll = true;
            //searchControl.reloadData();
        });

        $(document).on('click', '#btnCloseRebate', function () {
            $('#ucRebate').dialog('close');
        });

        $('#new-vehicle').on('click', '.openRebate', function () {
            var that = $(this);
            var incentiveDisplayTypeId = that.attr('IncentiveDisplayTypeId');
            var agileVehicleId = that.attr('agileVehicleId');
            var programId = that.attr('programId');
            var vin = that.attr('vin');
            var modelId = that.attr('modelId');
            var isCashCoupon = that.attr('isCashCoupon');
            var isUpfitted = that.attr('isUpfitted');
            var method = '';
            var data = {
                agileVehicleId: agileVehicleId,
                vin: vin,
                modelId: modelId,
                programId: programId,
                isCashCoupon: isCashCoupon,
                isUpfitted: isUpfitted
            };

            if (incentiveDisplayTypeId == INCENTIVETYPEID.NORMAl) {
                method = 'GetRebateForVehicleByAgileVehicleId';
            } else {
                method = 'GetRebateForVehicleByAgileVehicleId';
            }

            if (typeof rebateControl != "undefined" && rebateControl != null) {
                rebateControl.loadRebateForVehicle(data, method);
            }

            $('#rebateControl').modal('show');
        });

        $('#rebateControl').on('shown.bs.modal', function () {
            //if (typeof rebateControl != "undefined" && rebateControl != null) {
            //    rebateControl.loadRebateForVehicle(data, method);
            //}
        });

        $('#new-vehicle').on('click', '.autocheck', function (event) {
            event.preventDefault();
            var that = $(this);
            var $form = $("<form/>")
				.attr("id", "data_form")
				.attr("action", "/partners/autocheck")
				.attr("method", "post")
				.attr('target', '_blank');

            $("body").append($form);
            var vin = that.attr('vin');
            //Append the values to be send
            searchControl.AddParameter($form, "vin", vin);
            $form[0].submit();
        });
    },
    AddParameter: function (form, name, value) {
        var $input = $("<input />").attr("type", "hidden")
								.attr("name", name)
								.attr("value", value);
        form.append($input);
    },
    setSelectingFlagToDefault: function () {
        vehicleSearchCriteria.IsSelectingPrice = false;
        vehicleSearchCriteria.IsSelectingYear = false;
        vehicleSearchCriteria.IsSelectingMake = false;
        vehicleSearchCriteria.IsSelectingModel = false;
        vehicleSearchCriteria.IsSelectingBody = false;
        vehicleSearchCriteria.IsSelectingStandardBody = false;
        vehicleSearchCriteria.IsSelectingFuel = false;
        vehicleSearchCriteria.IsSelectingColor = false;
        vehicleSearchCriteria.IsSelectingTransmission = false;
        vehicleSearchCriteria.IsSelectingEngine = false;
        vehicleSearchCriteria.IsSelectingDrive = false;
        vehicleSearchCriteria.IsSelectingVehiclefeatures = false;
        vehicleSearchCriteria.IsSelectingTrim = false;
        vehicleSearchCriteria.IsSelectingMileage = false;
    },
    setSelectingFlag: function (currentSelectingCategory) {
        if (typeof currentSelectingCategory != "undefined" && currentSelectingCategory != null) {
            switch (currentSelectingCategory) {
                case 'style':
                    vehicleSearchCriteria.IsSelectingBody = true;
                    break;
                case 'price':
                    vehicleSearchCriteria.IsSelectingPrice = true;
                    break;
                case 'make':
                    vehicleSearchCriteria.IsSelectingMake = true;
                    break;
                case 'year':
                    vehicleSearchCriteria.IsSelectingYear = true;
                    break;
                case 'fuel':
                    vehicleSearchCriteria.IsSelectingFuel = true;
                    break;
                case 'color':
                    vehicleSearchCriteria.IsSelectingColor = true;
                    break;
                case 'transmission':
                    vehicleSearchCriteria.IsSelectingTransmission = true;
                    break;
                case 'engine':
                    vehicleSearchCriteria.IsSelectingEngine = true;
                    break;
                case 'drive':
                    vehicleSearchCriteria.IsSelectingDrive = true;
                    break;
                case 'mileage':
                    vehicleSearchCriteria.IsSelectingMileage = true;
                    break;
                case 'model':
                    vehicleSearchCriteria.IsSelectingModel = true;
                    break;
                case 'vehiclefeatures':
                    vehicleSearchCriteria.IsSelectingVehiclefeatures = true;
                    break;
                default:
                    break;
            }
        }
    },
    setFilterBackToNormal: function (type) {
        var ul = $('ul[data-type="' + type + '"]');
        $(ul).find('li.list-group-item:not(.list-group-custom-default)').removeClass('active');
        $(ul).find('li.list-group-item').find('span:not(.badge)').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        $(ul).find('div.list-group-submenu').removeClass('in');
        $(ul).find('li.list-group-custom-default').addClass('active');
    },
    setAllBackToNormal: function (type) {
        var allListGroupUl = $('ul.list-group:not([data-type="type"]):not([data-type="model"])');
        $.each(allListGroupUl, function () {
            var that = $(this);
            $(that).find('li.list-group-item:not(.list-group-custom-default)').removeClass('active');
            $(that).find('li.list-group-item').find('span:not(.badge)').removeClass('glyphicon-minus').addClass('glyphicon-plus');
            $(that).find('div.list-group-submenu').removeClass('in');
            $(that).find('li.list-group-custom-default').addClass('active');
        });
    },
    setReloadTriggerScrollPosition: function () {

        if (vehicleSearchCriteria.IsViewAll) {
            $(window).scrollTop(reloadTriggerScrollPosition);
            var vehicleDisplays = $('#vehicleList .panel');
            var numVehiclesOnDisplay = vehicleDisplays.length;
            shouldGetMoreDataOnScroll = totalResultSet > numVehiclesOnDisplay;
            var thirdLastEntry = numVehiclesOnDisplay > 1 ? vehicleDisplays[numVehiclesOnDisplay - 1] : 0;
            reloadTriggerScrollPosition = $(thirdLastEntry).position().top;
        }

    },
    debounce: function (func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    },
    scrollMoved: function () {
        var currentScrollPosition = $(window).scrollTop();
        if (reloadTriggerScrollPosition <= currentScrollPosition && vehicleSearchCriteria.IsViewAll && shouldGetMoreDataOnScroll) {//Force Reload
            var hidPageNumber = $('#pageNumber');
            var currentPage = searchControl.tryParseValue(hidPageNumber.attr('currentPage'));
            currentPage = currentPage + 1;
            hidPageNumber.attr('currentPage', currentPage);
            $('[id$="currentPage"]').val(currentPage);
            searchControl.reloadData();
        }
    },
    scrollToDisplaySearchSection: function () {
        if ($(window).width() <= 628) {
            var currentScrollPosition = $(window).scrollTop();
            if (!hasShownTheSearchSection && currentScrollPosition >= inventoryPosition) {
                $('#btnEnbleFilter').click();
                hasShownTheSearchSection = true; // Just show for once
            }
        }
    },
    registerScrollEvent: function () {
        $(window).on('scroll', searchControl.debounce(searchControl.scrollMoved, 250));
       // $(window).on('scroll', searchControl.debounce(searchControl.scrollToDisplaySearchSection, 250));
    },
    registerEventForPriceAndRebate: function () {
        $('.price').each(function (index, item) {
            var price = searchControl.tryParseValue($(item).attr('value'));
            if (price == 0) {
                $(item).text("Call");
            }
        });

        $('.rebate').each(function (index, item) {
            var text = $(item).text();
            var replacedText = text.replace('(', '').replace(')', '');
            var tryText = replacedText.replace('$', '').replace('-', '');
            if (searchControl.tryParseValue(tryText) != 0) {
                $(item).text(replacedText);
            } else {
                $(item).closest('.rebateDiv').hide();
            }
        });

    },
    reloadData: function () {
        searchControl.addSelectedValueToCriteria();
        searchControl.getVehicleData();
    },
    getVehicleSearchCriteria: function () {
        var vehicleFilter = {
            VehicleSearchCriteria: vehicleSearchCriteria,
            VehicleMasterDataCounter: vehicleMasterDataCounter
        };
        vehicleFilter.VehicleSearchCriteria.CanBeEdit = $('[id$="CanBeEdit"]').val() == "True";
        return vehicleFilter;
    },
    getVehicleData: function () {
        var data = searchControl.getVehicleSearchCriteria();
        if (!isSearchingByHistoryFilter) {
            var newData = {};
            $.extend(true, newData, data);
            //put history here
            var newHistory = {
                data: newData,
                url: location.href
            };
            searchFilterHistory.push(newHistory);
        }
        searchControl.unmaskedInput();

        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'GetVehicles',
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    var canbeEdit = $('[id$="CanBeEdit"]').val() == "True";
                    if (!dataResult.criteria.IsViewAll) {
                        $('#vehicleList').empty();
                    } else {
                        if (dataResult.criteria.PageNumber == 1)
                            $('#vehicleList').empty();
                    }
                    $('[id$="total"]').val(dataResult.totalPage);
                    if (dataResult.vehicleList != null) {
                        var total = dataResult.vehicleList.length;
                        for (var i = 0; i < total; i++) {
                            var currentItem = dataResult.vehicleList[i];
                            currentItem.Rebate = currentItem.Rebate.toString().replace('-', '');
                            currentItem.IsDisplayRebate = currentItem.Rebate == 0 ? "hidden" : "";

                            currentItem.IsEmployeePrice = currentItem.EmployeePrice == 0 ? "hidden" : "";

                            if (currentItem.EmployeePrice != 0) {
                                currentItem.EmployeePrice = '$' + currentItem.EmployeePrice.toString().replace('-', '');
                               // currentItem.DealerSaving = 0;
                            }



                            currentItem.IsDisplayDealerSaving = currentItem.DealerSaving == 0 || currentItem.OriginalSellingPrice == 0 ? "hidden" : "";
                            currentItem.SellingPrice = currentItem.SellingPrice <= 0 ? "Call" : currentItem.SellingPrice;
                            currentItem.ConditionalSellingPrice = currentItem.ConditionalSellingPrice <= 0 ? "Call" : '$' + currentItem.ConditionalSellingPrice;
                            currentItem.IsDisplayConditionalPrice = (typeof currentItem.ConditionalOffers != undefined && currentItem.ConditionalOffers != null && currentItem.ConditionalOffers.length > 0) ? '' : 'hidden';

                            currentItem.IsDisplayMRSP = currentItem.MSRP == 0 || (currentItem.MSRP < currentItem.OriginalSellingPrice && currentItem.IsDisplayConditionalPrice == "hidden") ? true : false;

                            if ((currentItem.SellingPrice != 'Call' && currentItem.MSRP != 0) || (currentItem.SellingPrice != 'Call' && currentItem.MSRP != 0 && currentItem.ConditionalSellingPrice != 'Call' && currentItem.NormalSellingPrice != 'Call')) {
                                if (((currentItem.SellingPrice == currentItem.MSRP && (currentItem.IsDisplayRebate == "hidden" || currentItem.IsDisplayDealerSaving == "hidden")) || (currentItem.SellingPrice == currentItem.OriginalSellingPrice && (currentItem.IsDisplayDealerSaving == "hidden") && !currentItem.IsUpfitted && currentItem.IsDisplayRebate == "hidden"))) {
                                    currentItem.IsDisplayMRSP = currentItem.NormalOffers.length == 0 ? "hidden" : "";
                                }
                                else {
                                    currentItem.IsDisplayMRSP = "";
                                }
                            }

                            currentItem.NormalSellingPrice = currentItem.NormalSellingPrice <= 0 ? "Call" : currentItem.NormalSellingPrice = '$' + currentItem.NormalSellingPrice;
                            if (currentItem.DealerSaving != 0)
                                currentItem.DealerSaving = '-$' + currentItem.DealerSaving.toString().replace('-', '');



                            if (currentItem.ModelFormated == "")
                                currentItem.ModelFormated = "0";

                            var url = '/vehicle-info/' + (currentItem.IsNew ? "new" : (currentItem.IsCert ? "used-certified" : "used")) +  '-' + currentItem.Year + '-' + currentItem.Make.toLowerCase() + '-' + currentItem.ModelFormated.toLowerCase() + '-' + currentItem.Vin.toLowerCase();
                            currentItem.VehicleInfoUrl = url;
                            currentItem.EditUrl = '/admin/vehicleEdit/' + currentItem.Vin;
                            currentItem.Trim = currentItem.Trim == "Other" ? '' : currentItem.Trim;
                            currentItem.IsDisplayMPG = (currentItem.MPGCity == 0 || currentItem.MPGHighway == 0) ? "hidden" : "";

                            currentItem.IsDisplayExtColor = currentItem.IsNew ? "" : "hidden";
                            currentItem.IsDisplayMileage = !currentItem.IsNew ? "" : "hidden";
                            currentItem.IdDisplayAutoCheck = !currentItem.IsNew ? "" : "hidden";
                            currentItem.Mileage = searchControl.formatThousand(currentItem.Mileage);
                            currentItem.IsDisplayEditButton = !canbeEdit ? "hidden" : "";
                            currentItem.DisplayCertified = currentItem.IsCert ? "show" : "hidden";
                            currentItem.IsCertified = currentItem.IsCert;
                            if (currentItem.IsHidden) {
                                currentItem.IsHiddenCard = 'IsHiddenCard';
                            }
                            currentItem.IsUsed = !currentItem.IsNew;
                            var today = new Date();
                            //if (currentItem.IsOverridePrice) {
                            //    currentItem.SellingPrice = currentItem.SellingPrice;
                            //	currentItem.NormalSellingPrice = currentItem.SellingPrice;
                            //	//currentItem.IsDisplayMRSP = "hidden";
                            //	//currentItem.IsDisplayDealerSaving = "hidden";
                            //	currentItem.IsDisplayRebate = "hidden";
                            //	currentItem.IsDisplayConditionalPrice = "hidden";
                            //}


                            $.tmpl($('#vehicleListTemplate'), currentItem).appendTo('#vehicleList');
                        }
                        if (dataResult.vehicleFeatures.length > 0) {
                            if (dataResult.vehicleFeatures != null) {
                                $("#collapsevehiclefeatures ul").empty();
                                var total = dataResult.vehicleFeatures.length;

                                var vehicleFeaturesCriteria = dataResult.criteria.VehicleFeatures;
                                var IsFeatureInSearchExists = false;
                                $("#collapsevehiclefeatures ul").append("<li class='list-group-item active list-group-custom-default'>All Features</li>");
                                for (var i = 0; i < total; i++) {
                                    IsFeatureInSearchExists = false;
                                    var currentFeatureItem = dataResult.vehicleFeatures[i];
                                    if (vehicleFeaturesCriteria.length > 0) {
                                        for (var j = 0; j < vehicleFeaturesCriteria.length; j++) {
                                            if (vehicleFeaturesCriteria[j] == currentFeatureItem.Id) {
                                                IsFeatureInSearchExists = true;

                                            }
                                        }
                                        if (IsFeatureInSearchExists) {
                                            $("#collapsevehiclefeatures ul").append("<li class='list-group-item active' value='" + currentFeatureItem.Id + "'><span class='badge'>" + currentFeatureItem.Count + "</span>" + currentFeatureItem.Name + "</li>");
                                        }
                                        else { $("#collapsevehiclefeatures ul").append("<li class='list-group-item' value='" + currentFeatureItem.Id + "'><span class='badge'>" + currentFeatureItem.Count + "</span>" + currentFeatureItem.Name + "</li>"); }
                                    }
                                    else {
                                        $("#collapsevehiclefeatures ul").append("<li class='list-group-item' value='" + currentFeatureItem.Id + "'><span class='badge'>" + currentFeatureItem.Count + "</span>" + currentFeatureItem.Name + "</li>");
                                    }

                                }
                                $('#collapsevehiclefeatures .list-group-item').on('click', function (e) {
                                    var stopLoadingOnCurrentPage = false;
                                    var that = $(this);
                                    var hasSubList = that.attr('hassublist') == "True";
                                    var isSubList = that.hasClass('sublistItem');
                                    var parentId = that.attr('parentid');
                                    var ul = that.closest('ul');
                                    var type = ul.data('type');
                                    var divParentId = that.closest('div.list-group-submenu').attr('id');

                                    if ($(e.target).is('span.glyphicon')) {
                                        return;
                                    }

                                    var parent = that.closest(".list-group");
                                    var parentOfSubList = parent.find('li[parentId="' + divParentId + '"]');

                                    if ($(e.target).hasClass('active')) {
                                        $(e.target).removeClass('active');
                                        if (hasSubList) {
                                            parent.find('div[id="' + parentId + '"] li').removeClass('active');
                                        }
                                    } else {
                                        if (isSubList)
                                            parent.find('div[id="' + divParentId + '"] li').removeClass('active');
                                        $(e.target).addClass('active');
                                    }

                                    var child = that.children('span.glyphicon');
                                    if (!that.hasClass('active')) {
                                        if (!child.hasClass('glyphicon-minus')) {
                                            child.addClass('glyphicon-minus').removeClass('glyphicon-plus');
                                        } else {
                                            child.removeClass('glyphicon-minus').addClass('glyphicon-plus');
                                        }
                                    } else {
                                        child.addClass('glyphicon-minus').removeClass('glyphicon-plus');
                                    }

                                    if (!that.hasClass('list-group-custom-default')) {
                                        parent.find('.list-group-custom-default').removeClass("active");
                                    } else {
                                        parent.find('.list-group-item').removeClass("active");
                                        parent.find('span.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
                                        parent.find('.list-group-submenu').removeClass('in');
                                    }

                                    //Check if parent of sublist has not checked then check it
                                    if (isSubList) {
                                        if (!$(parentOfSubList).hasClass('active')) {
                                            $(parentOfSubList).addClass('active');
                                        }
                                    }

                                    if (!isSubList && hasSubList) {
                                        if ($(parent).find('li[parentId="' + parentId + '"]').hasClass('active'))
                                            $(parent).find('.list-group-submenu[id="' + parentId + '"]').addClass('in');
                                        else
                                            $(parent).find('.list-group-submenu[id="' + parentId + '"]').removeClass('in');
                                    }

                                    //No Selected
                                    if (parent.find('.list-group-item.active').length == 0) {
                                        parent.find('.list-group-custom-default').addClass("active");
                                    }
                                    if (!that.hasClass('list-group-custom-default')) {
                                        searchControl.setSelectingFlag(type);
                                        isRealTouching = true;
                                    }
                                    //else {
                                    //    if (type == 'model') {
                                    //        location.href = '/inventory';
                                    //        stopLoadingOnCurrentPage = true;
                                    //    }
                                    //}
                                    if (!stopLoadingOnCurrentPage) {
                                        searchControl.resetToPageOne();
                                        searchControl.reloadData();
                                    }
                                });

                                $('#collapsevehiclefeatures ul li span.badge').click(function () {
                                    var that = $(this);
                                    that.closest('li').click();
                                });

                                //$.tmpl($('#vehicleFeaturesTemplate'), dataResult).appendTo('#collapsevehiclefeatures');
                                //vehicleSearchCriteria.VehicleFeatures = [];
                                //var vehiclefeatures = searchControl.getSelectedItemsByType('vehiclefeatures');
                                //if (vehiclefeatures.length !== 0) {
                                //    alert(11);
                                //    vehicleSearchCriteria.IsSelectingVehiclefeatures = true;
                                //    for (var i = 0; i < vehiclefeatures.length; i++) {
                                //        vehicleSearchCriteria.VehicleFeatures.push($(vehiclefeatures[i]).attr('value'));
                                //    }
                                //}
                            }
                        }
                    }

                    searchControl.setTotalCount(dataResult.totalRecord);
                    $('[data-toggle="tooltip"]').tooltip();

                    if (dataResult.totalRecord == 0) {
                        if (window.location.toString().toLowerCase().indexOf("/new") > -1) {
                            window.location.href = "/inventory?keywords=" + $("#txtSearch").val();
                        }
                        else if (window.location.toString().toLowerCase().indexOf("/used") > -1) {
                            window.location.href = "/inventory?keywords=" + $("#txtSearch").val();
                        }
                        else if (window.location.toString().toLowerCase().indexOf("/certified") > -1) {
                            window.location.href = "/inventory?keywords=" + $("#txtSearch").val();
                        }
                        //$(".divNoSearchData").removeClass("hide");
                        //if (window.location.toString().toLowerCase().indexOf("/new") > -1) {
                        //    $(".divNoSearchData").html("<span>There were no matches in new inventory.  Would you like to search all inventory? <button id='lnkYes' style='cursor:pointer' class='btn btn-primary'>Yes</button>&nbsp;<button id='lnkNo' style='cursor:pointer' class='btn btn-primary'>No</button></span>");
                        //}
                        //else if (window.location.toString().toLowerCase().indexOf("/used") > -1) {
                        //    $(".divNoSearchData").html("<span>There were no matches in used inventory.  Would you like to search all inventory? <button id='lnkYes' style='cursor:pointer' class='btn btn-primary'>Yes</button>&nbsp;<button id='lnkNo' style='cursor:pointer' class='btn btn-primary'>No</button></span>");
                        //}
                        //else if (window.location.toString().toLowerCase().indexOf("/certified") > -1) {
                        //    $(".divNoSearchData").html("<span>There were no matches in certified inventory.  Would you like to search all inventory? <button id='lnkYes' style='cursor:pointer' class='btn btn-primary'>Yes</button>&nbsp;<button id='lnkNo' style='cursor:pointer' class='btn btn-primary'>No</button></span>");
                        //}
                        //else {
                        //    $(".divNoSearchData").html("Unfortunately there were no matches found in all inventory.");
                        //}
                        //$("#lnkYes").on("click", function () {
                        //    $(".divNoSearchData").removeClass("hide");
                        //    $(".divNoSearchData").html("<img width='32' height='32'  src='" + siteMaster.currentUrl() + "/Content/Images/spinner.gif' />&nbsp;Searching inventory....");
                        //    //var timeDelay = 5000;           // MILLISECONDS (5 SECONDS).
                        //    searchControl.setSelectingFlagToDefault();
                        //    searchControl.resetToPageOne();
                        //    searchControl.addSelectedValueToCriteria();
                        //    vehicleSearchCriteria.IsNew = false;
                        //    vehicleSearchCriteria.IsUsed = false;
                        //    vehicleSearchCriteria.IsCertified = false;
                        //    var data = searchControl.getVehicleSearchCriteria();
                        //    setTimeout(function () {
                        //        $.ajax({
                        //            url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'GetCounter',
                        //            type: 'POST',
                        //            async: true,
                        //            dataType: "json",
                        //            contentType: "application/json",
                        //            cache: false,
                        //            timeout: 300000,
                        //            data: JSON.stringify(data),
                        //            success: function (dataResult) {
                        //                if (dataResult.isSuccess === true) {
                        //                    if (dataResult.vehicleList.length > 0) {
                        //                        window.location.href = "/inventory?keywords=" + $("#txtSearch").val();
                        //                    }
                        //                    else {
                        //                        $(".divNoSearchData").html("Unfortunately there were no matches found in all inventory as well.");
                        //                        setTimeout(function () {
                        //                            setTimeout(function () {
                        //                                $("#txtSearch").val('');
                        //                                searchControl.reloadData();
                        //                                return false;
                        //                            }, 5000);
                        //                        }, 5000);
                        //                    }
                        //                }
                        //            },
                        //            error: function () {
                        //                $.unblockUI(siteMaster.loadingWheel());
                        //                alert(searchControl.localizations["ErrorMessage"]);
                        //            }
                        //        });
                        //    }, 5000);

                        //    return false;
                        //});
                        //$("#lnkNo").on("click", function () {
                        //    $("#txtSearch").val('');
                        //    searchControl.reloadData();
                        //    return false;
                        //});
                    }
                    else {
                        $(".divNoSearchData").addClass("hide");
                        $(".divNoSearchData").html("");
                    }


                    var isShow = (dataResult.totalRecord >= dataResult.criteria.PageSize);
                    if (dataResult.criteria.IsViewAll) {
                        isShow = false;
                    }
                    searchControl.displayPagination(isShow);
                    vehicleMasterDataCounter = dataResult.masterDataCounter;
                    searchControl.setDefaultMasked();
                    searchControl.setPaginationTotal();
                    searchControl.hideSearchMessageIfNoResultFound(dataResult.totalRecord);
                    searchControl.getCounterForVehicle(data);
                    searchControl.setReloadTriggerScrollPosition();
                    searchControl.modifyPagingRel();

                    if ($(window).scrollTop() > 1000) {
                        $(window).scrollTop(300);
                    }
                }
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert(searchControl.localizations["ErrorMessage"]);
            }
        });
    },
    getCounterForVehicle: function (data) {
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SearchController.ashx?method=' + 'GetCounter',
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    searchControl.setCounterForFilter(dataResult.masterDataCounter, dataResult.criteria);
                    searchControl.hideTypeIfNowResultFound();
                    searchControl.hideModelIsNoResultFound();
                    searchControl.hideFilterIfNoResultFound();
                    $.unblockUI(siteMaster.loadingWheel());
                }
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert(searchControl.localizations["ErrorMessage"]);
            }
        });
    },
    hideSearchMessageIfNoResultFound: function (count) {
        var total = searchControl.tryParseValue(count);
        if (total > 0) {
            $('#search-res-msg').addClass('hidden');
        } else {
            var searchText = $('#txtSearch').val();
            if (searchText == '' || searchText == null) {
                $('#search-res-msg').addClass('hidden');
            } else {
                $('#search-res-msg').removeClass('hidden');
            }
        }
    },
    hideTypeIfNowResultFound: function () {
        var newCount = $('ul[data-type="type"]').find('li[value="new"]');
        var usedCount = $('ul[data-type="type"]').find('li[value="used"]');
        var certitiedCount = $('ul[data-type="type"]').find('li[value="certified"]');
        var liftCount = $('ul[data-type="lift"]').find('li[value="isLift"]');
        var upfittedCount = $('ul[data-type="upfitted"]').find('li[value="isUpfitted"]');

        if (newCount.find('span.badge').text() == 0) {
            newCount.hide();
        } else {
            newCount.show();
        }

        if (usedCount.find('span.badge').text() == 0) {
            usedCount.hide();
        } else {
            usedCount.show();
        }

        if (certitiedCount.find('span.badge').text() == 0) {
            certitiedCount.hide();
        } else {
            certitiedCount.show();
        }

        if (liftCount.find('span.badge').text() == 0) {
            liftCount.hide();
        } else {
            liftCount.show();
        }

        if (upfittedCount.find('span.badge').text() == 0) {
            upfittedCount.hide();
        } else {
            upfittedCount.show();
        }
    },
    hideFilterIfNoResultFound: function () {
        $('span.badge:not([isChildCount="true"]):not([isParentModelCount="true"])').each(function (index, item) {
            var that = $(this);
            var parent = that.closest('li.list-group-item');
            //var topParent = parent.closest('ul.list-group');
            //if (topParent)
            if (that.text() == 0) {
                parent.hide();
            } else {
                parent.show();
            }
        });
    },
    hideModelIsNoResultFound: function () {
        $('span[isParentModelCount="true"]').each(function (index, item) {
            var that = $(this);
            var parent = that.closest('li.list-group-item');
            var child = $('div[id="' + parent.attr('parentid') + '"]');
            if (that.text() == 0) {
                parent.hide();
                $(child).removeClass('in');
            } else {
                parent.show();
            }
        });

        $('span[isChildCount="true"]').each(function (index, item) {
            var that = $(this);
            var parent = that.closest('li.sublistItem');
            if (that.text() == 0) {
                parent.hide();
            } else {
                parent.show();
            }
        });
    },
    setTotalCount: function (total) {
        totalResultSet = total;
        var msg;
        if (total > 1) {
            msg = total + " " + searchControl.localizations["VehiclesFound"];
        } else {
            msg = total + " " + searchControl.localizations["VehiclesFound"];
        }
        $('#lblCount').text(msg);
    },
    setCounterForFilter: function (masterDataCounter, criteria) {
        if (typeof masterDataCounter != "undefined" && masterDataCounter != null) {
            $('ul[data-type="type"]').find('li[value="new"] span.badge').text(masterDataCounter.IsNew);
            $('ul[data-type="type"]').find('li[value="used"] span.badge').text(masterDataCounter.IsUsed);
            $('ul[data-type="type"]').find('li[value="certified"] span.badge').text(masterDataCounter.IsCertified);
            $('ul[data-type="type"]').find('li.list-group-custom-default span.badge').text(masterDataCounter.IsNew + masterDataCounter.IsCertified + masterDataCounter.IsUsed);

            $('ul[data-type="lift"]').find('li.list-group-custom-default span.badge').text(masterDataCounter.IsLift);
            $('ul[data-type="lift"]').find('li[value="isLift"] span.badge').text(masterDataCounter.IsLift);
            $('ul[data-type="upfitted"]').find('li[value="isUpfitted"] span.badge').text(masterDataCounter.IsUpfitted);

            if (!criteria.IsSelectingYear || !isRealTouching) {
                if (masterDataCounter.Years.length !== 0) {
                    var count = masterDataCounter.Years.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Years[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('year', masterDataCounter.Years[i].Id, masterDataCounter.Years[i].Count);
                    }
                    $('ul[data-type="year"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }
            if (!criteria.IsSelectingMake || !isRealTouching) {
                if (masterDataCounter.Makes.length !== 0) {
                    var count = masterDataCounter.Makes.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Makes[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('make', masterDataCounter.Makes[i].Id, masterDataCounter.Makes[i].Count);
                    }
                    $('ul[data-type="make"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }

            if (!criteria.IsSelectingPrice || !isRealTouching) {
                if (masterDataCounter.Prices.length !== 0) {
                    var count = masterDataCounter.Prices.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Prices[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('price', masterDataCounter.Prices[i].Id, masterDataCounter.Prices[i].Count);
                    }
                    $('ul[data-type="price"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }


            if ((!criteria.IsSelectingTrim || !criteria.IsSelectingModel || !isRealTouching) && masterDataCounter.Models.length !== 0) {
                var count = masterDataCounter.Models.length;
                var sum = 0;
                for (var i = 0; i < count; i++) {
                    var parent = searchControl.getParentIdByTypeAndId('model', masterDataCounter.Models[i].Id);
                    sum += masterDataCounter.Models[i].Count;
                    searchControl.setValueForSelectedItemsByTypeAndId('model', masterDataCounter.Models[i].Id, masterDataCounter.Models[i].Count);
                    var subList = masterDataCounter.Models[i].SubList;
                    if (typeof subList != "undefined" && subList != null) {
                        for (var j = 0; j < subList.length; j++) {
                            searchControl
                                .setValueForSelectedItemsOfSubMenuByTypeAndId('model',
                                    $(parent).attr('parentid'),
                                    masterDataCounter.Models[i].SubList[j].SubListId,
                                    masterDataCounter.Models[i].SubList[j].Count);
                        }
                    }
                }
                $('ul[data-type="model"]').find('li.list-group-custom-default span.badge').text(sum);
            }

            if ((!criteria.IsSelectingEngine || !isRealTouching) && masterDataCounter.Engines.length !== 0) {
                var count = masterDataCounter.Engines.length;
                var sum = 0;
                //for (var i = 0; i < count; i++) {
                //    sum += masterDataCounter.Engines[i].Count;
                //    searchControl.setValueForSelectedItemsByTypeAndId('engine', masterDataCounter.Engines[i].Id, masterDataCounter.Engines[i].Count);
                //}
                //$('ul[data-type="engine"]').find('li.list-group-custom-default span.badge').text(sum);


                for (var i = 0; i < count; i++) {
                    var parent = searchControl.getParentIdByTypeAndId('engine', masterDataCounter.Engines[i].Id);
                    sum += masterDataCounter.Engines[i].Count;
                    searchControl.setValueForSelectedItemsByTypeAndId('engine', masterDataCounter.Engines[i].Id, masterDataCounter.Engines[i].Count);
                    var subList = masterDataCounter.Engines[i].SubList;
                    if (typeof subList != "undefined" && subList != null) {
                        for (var j = 0; j < subList.length; j++) {
                            searchControl.setValueForSelectedItemsOfSubMenuByTypeAndId('engine', $(parent).attr('parentid'), masterDataCounter.Engines[i].SubList[j].SubListId, masterDataCounter.Engines[i].SubList[j].Count);
                        }
                    }
                }
                $('ul[data-type="engine"]').find('li.list-group-custom-default span.badge').text(sum);
            }

            if ((!criteria.IsSelectingStandardBody || !criteria.IsSelectingBody || !isRealTouching) && masterDataCounter.BodyStyles.length !== 0) {
                if (masterDataCounter.BodyStyles.length !== 0) {
                    var count = masterDataCounter.BodyStyles.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        var parent = searchControl.getParentIdByTypeAndId('style', masterDataCounter.BodyStyles[i].Id);
                        sum += masterDataCounter.BodyStyles[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('style', masterDataCounter.BodyStyles[i].Id, masterDataCounter.BodyStyles[i].Count);
                        var subList = masterDataCounter.BodyStyles[i].SubList;
                        if (typeof subList != "undefined" && subList != null) {
                            for (var j = 0; j < subList.length; j++) {
                                searchControl.setValueForSelectedItemsOfSubMenuByTypeAndId('style', $(parent).attr('parentid'), masterDataCounter.BodyStyles[i].SubList[j].SubListId, masterDataCounter.BodyStyles[i].SubList[j].Count);
                            }
                        }
                    }
                    $('ul[data-type="style"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }

            if (!criteria.IsSelectingColor || !isRealTouching) {
                if (masterDataCounter.ExteriorColors.length !== 0) {
                    var count = masterDataCounter.ExteriorColors.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.ExteriorColors[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('color', masterDataCounter.ExteriorColors[i].Id, masterDataCounter.ExteriorColors[i].Count);
                    }
                    $('ul[data-type="color"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }

            if (!criteria.IsSelectingTransmission || !isRealTouching) {
                if (masterDataCounter.Transmissions.length !== 0) {
                    var count = masterDataCounter.Transmissions.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Transmissions[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('transmission', masterDataCounter.Transmissions[i].Id, masterDataCounter.Transmissions[i].Count);
                    }
                    $('ul[data-type="transmission"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }


            if (!criteria.IsSelectingFuel || !isRealTouching) {
                if (masterDataCounter.FuelTypes.length !== 0) {
                    var count = masterDataCounter.FuelTypes.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.FuelTypes[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('fuel', masterDataCounter.FuelTypes[i].Id, masterDataCounter.FuelTypes[i].Count);
                    }
                    $('ul[data-type="fuel"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }


            if (!criteria.IsSelectingDrive || !isRealTouching) {
                if (masterDataCounter.Drives.length !== 0) {
                    var count = masterDataCounter.Drives.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Drives[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('drive', masterDataCounter.Drives[i].Id, masterDataCounter.Drives[i].Count);
                    }
                    $('ul[data-type="drive"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }

            if (!criteria.IsSelectingMileage || !isRealTouching) {
                if (masterDataCounter.Mileages.length !== 0) {
                    var count = masterDataCounter.Mileages.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += masterDataCounter.Mileages[i].Count;
                        searchControl.setValueForSelectedItemsByTypeAndId('mile', masterDataCounter.Mileages[i].Id, masterDataCounter.Mileages[i].Count);
                    }
                    $('ul[data-type="mile"]').find('li.list-group-custom-default span.badge').text(sum);
                }
            }
        }
    },
    addSelectedValueToCriteria: function () {
        vehicleSearchCriteria.Vin = $('#txtSearch').val();

        if (window.location.toString().toLowerCase().indexOf("/new") > -1) {
            vehicleSearchCriteria.IsNew = true;
            vehicleSearchCriteria.IsUsed = false;
            vehicleSearchCriteria.IsCertified = false;
        }
        else if (window.location.toString().toLowerCase().indexOf("/used") > -1) {
            vehicleSearchCriteria.IsNew = false;
            vehicleSearchCriteria.IsUsed = true;
            vehicleSearchCriteria.IsCertified = true;
        }
        else if (window.location.toString().toLowerCase().indexOf("/certified") > -1) {
            vehicleSearchCriteria.IsNew = false;
            vehicleSearchCriteria.IsUsed = true;
            vehicleSearchCriteria.IsCertified = true;
        }
        else {
            vehicleSearchCriteria.IsNew = typeof $('ul[data-type="type"]').find('li.active[value="new"]').attr('value') != "undefined" ? true : false;
            vehicleSearchCriteria.IsUsed = typeof $('ul[data-type="type"]').find('li.active[value="used"]').attr('value') != "undefined" ? true : false;
            vehicleSearchCriteria.IsCertified = typeof $('ul[data-type="type"]').find('li.active[value="certified"]').attr('value') != "undefined" ? true : false;
        }

        vehicleSearchCriteria.IsLift = typeof $('ul[data-type="lift"]').find('li.active[value="isLift"]').attr('value') != "undefined" ? true : false;
        vehicleSearchCriteria.IsUpfitted = typeof $('ul[data-type="upfitted"]').find('li.active[value="isUpfitted"]').attr('value') != "undefined" ? true : false;

        vehicleSearchCriteria.SortItem = $('#ddlSort').val();

        vehicleSearchCriteria.Years = [];
        var years = searchControl.getSelectedItemsByType('year');
        if (years.length !== 0) {
            vehicleSearchCriteria.IsSelectingYear = true;
            for (var i = 0; i < years.length; i++) {
                vehicleSearchCriteria.Years.push($(years[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingYears = [];
        //var remainingYears = searchControl.getRemainingSelectedItemsByType('year');
        //if (remainingYears.length !== 0) {
        //    for (var i = 0; i < remainingYears.length; i++) {
        //        vehicleSearchCriteria.RemainingYears.push($(remainingYears[i]).attr('value'));
        //    }
        //}


        vehicleSearchCriteria.Makes = [];
        var makes = searchControl.getSelectedItemsByType('make');
        if (makes.length !== 0) {
            vehicleSearchCriteria.IsSelectingMake = true;
            for (var i = 0; i < makes.length; i++) {
                vehicleSearchCriteria.Makes.push($(makes[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingMakes = [];
        //var remainingMakes = searchControl.getRemainingSelectedItemsByType('make');
        //if (remainingMakes.length !== 0) {
        //    for (var i = 0; i < remainingMakes.length; i++) {
        //        vehicleSearchCriteria.RemainingMakes.push($(remainingMakes[i]).attr('value'));
        //    }
        //}


        vehicleSearchCriteria.Prices = [];
        var prices = searchControl.getSelectedItemsByType('price');
        if (prices.length !== 0) {
            vehicleSearchCriteria.IsSelectingPrice = true;
            for (var i = 0; i < prices.length; i++) {
                vehicleSearchCriteria.Prices.push($(prices[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingPrices = [];
        //var remainingPrices = searchControl.getRemainingSelectedItemsByType('price');
        //if (remainingPrices.length !== 0) {
        //    for (var i = 0; i < remainingPrices.length; i++) {
        //        vehicleSearchCriteria.RemainingPrices.push($(remainingPrices[i]).attr('value'));
        //    }
        //}

        vehicleSearchCriteria.ModelTrims = [];
        var models = searchControl.getSelectedItemsByType('model');
        if (models.length !== 0) {
            vehicleSearchCriteria.IsSelectingModel = true;
            for (var i = 0; i < models.length; i++) {
                var parentId = $(models[i]).attr('parentid');
                var hasSubList = $(models[i]).attr('hassublist') == "True";
                var modelId = $(models[i]).attr('value');
                var model = {
                    modelId: modelId,
                    trimId: 0
                };

                if (hasSubList) {
                    var sublist = searchControl.getSelectedSubItemByTypeAndId('model', parentId);
                    if (sublist.length != 0) {
                        for (var j = 0; j < sublist.length; j++) {
                            var newModel = {
                                modelId: modelId,
                                trimId: $(sublist[j]).attr('value')
                            }
                            vehicleSearchCriteria.IsSelectingTrim = true;
                            vehicleSearchCriteria.ModelTrims.push(newModel);
                        }
                    } else {
                        vehicleSearchCriteria.ModelTrims.push(model);
                    }
                } else {
                    vehicleSearchCriteria.ModelTrims.push(model);
                }
            }
        } else {
            if (vehicleSearchCriteria.InLitmitedMode) {
                vehicleSearchCriteria.ModelTrims = [];
                var remainingModels = searchControl.getRemainingSelectedItemsByType('model');
                if (remainingModels.length !== 0) {
                    for (var i = 0; i < remainingModels.length; i++) {
                        var model = {
                            modelId: $(remainingModels[i]).attr('value'),
                            trimId: 0
                        };
                        vehicleSearchCriteria.ModelTrims.push(model);
                    }
                }
            }
        }


        vehicleSearchCriteria.EngineTranslatedEngines = [];
        var engines = searchControl.getSelectedItemsByType('engine');
        if (engines.length !== 0) {
            vehicleSearchCriteria.IsSelectingEngine = true;
            for (var i = 0; i < engines.length; i++) {
                var parentId = $(engines[i]).attr('parentid');
                var hasSubList = $(engines[i]).attr('hassublist') == "True";
                var engineId = $(engines[i]).attr('value');
                var engine = {
                    engineTranslatedId: engineId,
                    engineId: 0
                };

                if (hasSubList) {
                    var sublist = searchControl.getSelectedSubItemByTypeAndId('engine', parentId);
                    if (sublist.length != 0) {
                        for (var j = 0; j < sublist.length; j++) {
                            var newEngine = {
                                engineTranslatedId: engineId,
                                engineId: $(sublist[j]).attr('value')
                            }
                            vehicleSearchCriteria.EngineTranslatedEngines.push(newEngine);
                        }
                    } else {
                        vehicleSearchCriteria.EngineTranslatedEngines.push(engine);
                    }
                } else {
                    vehicleSearchCriteria.EngineTranslatedEngines.push(engine);
                }
            }
        }



        //vehicleSearchCriteria.RemainingModelTrims = [];
        //var remainingModels = searchControl.getRemainingSelectedItemsByTypeForModel('model');
        //if (remainingModels.length !== 0) {
        //    for (var i = 0; i < remainingModels.length; i++) {
        //        var remainingParentId = $(remainingModels[i]).attr('parentid');
        //        var remainingHasSubList = $(remainingModels[i]).attr('hassublist') == "True";
        //        var remainingModelId = $(remainingModels[i]).attr('value');
        //        var remainingModel = {
        //            modelId: remainingModelId,
        //            trimId: 0
        //        };
        //        if (remainingHasSubList) {
        //            var remainingSublist = searchControl.getRemainingSelectedSubItemByTypeAndIdForModel('model', remainingParentId);
        //            if (remainingSublist.length != 0) {
        //                for (var j = 0; j < remainingSublist.length; j++) {
        //                    var remainingNewModel = {
        //                        modelId: remainingModelId,
        //                        trimId: $(remainingSublist[j]).attr('value')
        //                    }
        //                    vehicleSearchCriteria.RemainingModelTrims.push(remainingNewModel);
        //                }
        //            } else {
        //                vehicleSearchCriteria.RemainingModelTrims.push(remainingModel);
        //            }
        //        } else {
        //            vehicleSearchCriteria.RemainingModelTrims.push(remainingModel);
        //        }
        //    }
        //}


        vehicleSearchCriteria.VehicleBodyStandardBodyStyles = [];
        var styles = searchControl.getSelectedItemsByType('style');
        if (styles.length !== 0) {
            vehicleSearchCriteria.IsSelectingBody = true;
            for (var i = 0; i < styles.length; i++) {
                var parentId = $(styles[i]).attr('parentid');
                var hasSubList = $(styles[i]).attr('hassublist') == "True";
                var standardBodyId = $(styles[i]).attr('value');
                var bodyStyle = {
                    standardBodyId: standardBodyId,
                    bodyStyleTypeId: 0
                };

                if (hasSubList) {
                    var sublist = searchControl.getSelectedSubItemByTypeAndId('style', parentId);
                    if (sublist.length != 0) {
                        for (var j = 0; j < sublist.length; j++) {
                            var newbodyStyle = {
                                standardBodyId: standardBodyId,
                                bodyStyleTypeId: $(sublist[j]).attr('value')
                            }
                            vehicleSearchCriteria.IsSelectingStandardBody = true;
                            vehicleSearchCriteria.VehicleBodyStandardBodyStyles.push(newbodyStyle);
                        }
                    } else {
                        vehicleSearchCriteria.VehicleBodyStandardBodyStyles.push(bodyStyle);
                    }
                } else {
                    vehicleSearchCriteria.VehicleBodyStandardBodyStyles.push(bodyStyle);
                }
            }
        } else {
            if (vehicleSearchCriteria.InLitmitedMode) {
                vehicleSearchCriteria.VehicleBodyStandardBodyStyles = [];
                var remainingBodyStyles = searchControl.getRemainingSelectedItemsByType('style');
                if (remainingBodyStyles.length !== 0) {
                    for (var i = 0; i < remainingBodyStyles.length; i++) {
                        var bodyStyle = {
                            standardBodyId: $(remainingBodyStyles[i]).attr('value'),
                            bodyStyleTypeId: 0
                        };
                        vehicleSearchCriteria.VehicleBodyStandardBodyStyles.push(bodyStyle);
                    }
                }
            }
        }





        vehicleSearchCriteria.ExteriorColors = [];
        var colors = searchControl.getSelectedItemsByType('color');
        if (colors.length !== 0) {
            vehicleSearchCriteria.IsSelectingColor = true;
            for (var i = 0; i < colors.length; i++) {
                vehicleSearchCriteria.ExteriorColors.push($(colors[i]).attr('value'));
            }
        }
        vehicleSearchCriteria.Transmissions = [];
        var transmissions = searchControl.getSelectedItemsByType('transmission');
        if (transmissions.length !== 0) {
            vehicleSearchCriteria.IsSelectingTransmission = true;
            for (var i = 0; i < transmissions.length; i++) {
                if ($(transmissions[i]).attr('value') == "1")
                    vehicleSearchCriteria.Transmissions.push("Automatic");
                else if ($(transmissions[i]).attr('value') == "0")
                    vehicleSearchCriteria.Transmissions.push("Manual");
            }
        }
        //vehicleSearchCriteria.RemainingExteriorColors = [];
        //var remainingExteriorColors = searchControl.getRemainingSelectedItemsByType('color');
        //if (remainingExteriorColors.length !== 0) {
        //    for (var i = 0; i < remainingExteriorColors.length; i++) {
        //        vehicleSearchCriteria.RemainingExteriorColors.push($(remainingExteriorColors[i]).attr('value'));
        //    }
        //}

        vehicleSearchCriteria.FuelTypes = [];
        var fuels = searchControl.getSelectedItemsByType('fuel');
        if (fuels.length !== 0) {
            vehicleSearchCriteria.IsSelectingFuel = true;
            for (var i = 0; i < fuels.length; i++) {
                vehicleSearchCriteria.FuelTypes.push($(fuels[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingFuelTypes = [];
        //var remainingFuelTypes = searchControl.getRemainingSelectedItemsByType('fuel');
        //if (remainingFuelTypes.length !== 0) {
        //    for (var i = 0; i < remainingFuelTypes.length; i++) {
        //        vehicleSearchCriteria.RemainingFuelTypes.push($(remainingFuelTypes[i]).attr('value'));
        //    }
        //}



        //vehicleSearchCriteria.RemainingEngines = [];
        //var remainingEngines = searchControl.getRemainingSelectedItemsByType('engine');
        //if (remainingEngines.length !== 0) {
        //    for (var i = 0; i < remainingEngines.length; i++) {
        //        vehicleSearchCriteria.RemainingEngines.push($(remainingEngines[i]).attr('value'));
        //    }
        //}

        vehicleSearchCriteria.Drives = [];
        var drives = searchControl.getSelectedItemsByType('drive');
        if (drives.length !== 0) {
            vehicleSearchCriteria.IsSelectingDrive = true;
            for (var i = 0; i < drives.length; i++) {
                vehicleSearchCriteria.Drives.push($(drives[i]).attr('value'));
            }
        }

        vehicleSearchCriteria.VehicleFeatures = [];
        var vehiclefeatures = searchControl.getSelectedItemsByType('vehiclefeatures');
        if (vehiclefeatures.length !== 0) {
            vehicleSearchCriteria.IsSelectingVehiclefeatures = true;
            for (var i = 0; i < vehiclefeatures.length; i++) {
                vehicleSearchCriteria.VehicleFeatures.push($(vehiclefeatures[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingDrives = [];
        //var remainingDrives = searchControl.getRemainingSelectedItemsByType('drive');
        //if (remainingDrives.length !== 0) {
        //    for (var i = 0; i < remainingDrives.length; i++) {
        //        vehicleSearchCriteria.RemainingDrives.push($(remainingDrives[i]).attr('value'));
        //    }
        //}

        vehicleSearchCriteria.Mileages = [];
        var miles = searchControl.getSelectedItemsByType('mile');
        if (miles.length !== 0) {
            vehicleSearchCriteria.IsSelectingMileage = true;
            for (var i = 0; i < miles.length; i++) {
                vehicleSearchCriteria.Mileages.push($(miles[i]).attr('value'));
            }
        }

        //vehicleSearchCriteria.RemainingMileages = [];
        //var remainingMileages = searchControl.getRemainingSelectedItemsByType('mile');
        //if (remainingMileages.length !== 0) {
        //    for (var i = 0; i < remainingMileages.length; i++) {
        //        vehicleSearchCriteria.RemainingMileages.push($(remainingMileages[i]).attr('value'));
        //    }
        //}

        vehicleSearchCriteria.PageSize = $('[id$="pageSize"]').val();
        vehicleSearchCriteria.PageNumber = $('[id$="currentPage"]').val();

    },
    applyLazyLoadForImages: function () {
        $("img.lazy").lazyload({
            effect: "fadeIn"
        });
    },
    getParentIdByTypeAndId: function (type, id) {
        return $('ul[data-type="' + type + '"]').find('li:not(.list-group-custom-default):not(.sublistItem)[value="' + id + '"]');
    },
    getSelectedItemsByType: function (type) {
        return $('ul[data-type="' + type + '"]').find('li.active:not(.list-group-custom-default):not(.sublistItem)');
    },
    getRemainingSelectedItemsByType: function (type) {
        return $('ul[data-type="' + type + '"]').find('li:not(.list-group-custom-default):not(.sublistItem)');
    },
    getRemainingSelectedItemsByTypeForModel: function (type) {
        return $('ul[data-type="' + type + '"]').find('li:not(.list-group-custom-default):not(.sublistItem)');
    },
    getRemainingSelectedSubItemByTypeAndId: function (type, id) {
        var result = null;
        var subList = $('ul[data-type="' + type + '"]').find('div.list-group-submenu[id="' + id + '"]');
        if (typeof subList != "undefined" && subList != null) {
            result = $(subList).find('.sublistItem:not([style="display: none;"])');
        }
        return result;
    },
    getRemainingSelectedSubItemByTypeAndIdForModel: function (type, id) {
        var result = null;
        var subList = $('ul[data-type="' + type + '"]').find('div.list-group-submenu[id="' + id + '"]');
        if (typeof subList != "undefined" && subList != null) {
            result = $(subList).find('.sublistItem');
        }
        return result;
    },
    getSelectedSubItemByTypeAndId: function (type, id) {
        var result = null;
        var subList = $('ul[data-type="' + type + '"]').find('div.list-group-submenu[id="' + id + '"]');
        if (typeof subList != "undefined" && subList != null) {
            result = $(subList).find('.sublistItem.active');
        }
        return result;
    },
    setValueForSelectedItemsByTypeAndId: function (type, id, count) {
        $('ul[data-type="' + type + '"]').find('li:not(.list-group-custom-default):not(.sublistItem)[value="' + id + '"] span.badge').text(count);
    },
    setValueForSelectedItemsOfSubMenuByTypeAndId: function (type, parentId, id, count) {
        var subList = $('ul[data-type="' + type + '"]').find('div.list-group-submenu[id="' + parentId + '"]');
        $(subList).find('li.sublistItem:not(.list-group-custom-default)[value="' + id + '"] span.badge').text(count);
    },
    displayPagination: function (isShow) {
        if (isShow) {
            $('.vsr-pagenation').show();
        } else {
            $('.vsr-pagenation').hide();
        }

    },
    hookUpPagination: function () {
        currentPagingTop = $('#vsr-pagenationTop').bootpag({
            total: $('[id$="total"]').val(),
            href: searchControl.generateCurrentUrl(),
            maxVisible: 10,
            leaps: false,
            disabledClass: 'disabled',
            page: $('[id$="currentPage"]').val(),
            next: 'Next',
            prev: 'Prev',
            first: '&larr;',
            last: '&rarr;',
            firstLastUse: true,
        }).on("page", function (event, /* page number here */ num) {
            vehicleSearchCriteria.IsViewAll = false;
            $('[id$="currentPage"]').val(num);
            searchControl.reloadData();
            $(this).bootpag({ total: $('[id$="total"]').val() });
            $(currentPagingBottom).bootpag({ page: $('[id$="currentPage"]').val(), total: $('[id$="total"]').val() });
        });

        currentPagingBottom = $('#vsr-pagenationBottom').bootpag({
            total: $('[id$="total"]').val(),
            href: searchControl.generateCurrentUrl(),
            maxVisible: 10,
            leaps: false,
            disabledClass: 'disabled',
            page: $('[id$="currentPage"]').val(),
            next: 'Next',
            prev: 'Prev',
            first: '&larr;',
            last: '&rarr;',
            firstLastUse: true,
        }).on("page", function (event, /* page number here */ num) {
            vehicleSearchCriteria.IsViewAll = false;
            $('[id$="currentPage"]').val(num);
            searchControl.reloadData();
            $(this).bootpag({ total: $('[id$="total"]').val() });
            $(currentPagingTop).bootpag({ page: $('[id$="currentPage"]').val(), total: $('[id$="total"]').val() });
        });
    },
    setPaginationTotal: function () {
        $(currentPagingTop).bootpag({ page: $('[id$="currentPage"]').val(), total: $('[id$="total"]').val() });
        $(currentPagingBottom).bootpag({ page: $('[id$="currentPage"]').val(), total: $('[id$="total"]').val() });
    },
    tryParseValue: function (value) {
        return parseFloat(value);
    }
};



