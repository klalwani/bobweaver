﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CIJ.AgileDealer.Web.WebForm.Common
{
    public class Enum
    {
        public enum Department
        {
            Commercial = 1,
            Sale = 2,
            Service = 3,
            FinanceMarketing = 4,
            General = 5,
            All = 0,
        }

        public enum ButtonType
        {
            FourButton = 1,
            TwoButton = 2
        }

        public enum ServiceHourType
        {
            Service = 1,
            Sales = 2,
            Parts = 3,
            ServiceShort = 4,
            SalesShort = 5,
            PartsShort =6
        }

        public enum DisplaySection
        {
            Manufacturer = 1,
            Conditional = 2,
            Finance = 3,
            Lease = 4
        }

        public enum UpdateFor
        {
            All = 1,
            Vin = 2,
            Model = 3,
            Make = 4
        }
    }
}