﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Common
{
    public class ServicePageUI : BasedPage, IDisposable
    {
        public void BindingData(FormView formView,List<Service> services,
            List<Special> specials,DisplayPage displayPage)
        {
            formView.DataSource = new List<DisplayPage> { displayPage };
            formView.DataBind();

            var rpterService = formView.FindControl("rpterService") as Repeater;
            if (rpterService != null)
            {
                rpterService.DataSource = services.ToList();
                rpterService.DataBind();
            }
            var rpterSpecial = formView.FindControl("rpterSpecial") as Repeater;
            if (rpterSpecial != null)
            {
                rpterSpecial.DataSource = specials.ToList();
                rpterSpecial.DataBind();
            }
        }
        protected string ConvertDecimalToString(object value)
        {
            return String.Format("{0:C}", value);
        }
    }
}