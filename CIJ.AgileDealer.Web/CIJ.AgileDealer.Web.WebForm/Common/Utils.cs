﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CIJ.AgileDealer.Web.WebForm.Common
{
    public class Utils
    {
        public static bool IsValidImage(string fileName)
        {
            Regex regex = new Regex(@"(.*?)\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF|bmp|BMP)$");
            return regex.IsMatch(fileName);
        }

        public static bool SuportFile(string ext)
        {
            bool flag = false;
            switch (ext)
            {
                case ".doc":
                case ".docx":
                case ".xls":
                case ".xlsx":
                case ".ppt":
                case ".pptx":
                case ".vsd":
                    flag = true;
                    break;
                case ".gif":
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".png":
                    flag = true;
                    break;
                case ".wav":
                case ".mp3":
                    flag = true;
                    break;
                case ".pdf":
                    flag = true;
                    break;
            }
            return flag;
        }
    }
}