﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CJI.AgileDealer.Web.WebForm.Startup))]
namespace CJI.AgileDealer.Web.WebForm
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {

            var baseItem = new CJI.AgileDealer.Web.Base.Startup();
            baseItem.ConfigureAuth(app);
            app.MapSignalR();
            //ConfigureAuth(app);
        }
    }
}
