﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Sitemap.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Sitemap" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" id="sitemap">
        <asp:Repeater runat="server" ID="parent" OnItemDataBound="parent_OnItemDataBound">
            <ItemTemplate>
                <ul class="col-md-4" runat="server">
                    <li>
                        <h2> <a title='<%#Eval("Title") %>' href='<%#Eval("Url") %>' runat="server"><%# Eval("Title") %></a></h2>

                        <ul class="std-list">
                            <asp:Repeater runat="server" ID="child">
                                <ItemTemplate>
                                    <li>
                                        <a title='<%#Eval("Title") %>' href='<%#Eval("Url") %>' runat="server"><%# Eval("Title") %></a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </li>
                </ul>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
