﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using CJI.AgileDealer.Web.Base.Models;
using CJI.AgileDealer.Web.Base;

namespace CJI.AgileDealer.Web.WebForm.Account
{
    public partial class Confirm : Page
    {
        protected string StatusMessage
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string code = IdentityHelper_WebApp.GetCodeFromRequest(Request);
            string userId = IdentityHelper_WebApp.GetUserIdFromRequest(Request);
            if (code != null && userId != null)
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var result = manager.ConfirmEmail(userId, code);
                if (result.Succeeded)
                {
                    StatusMessage = "Thank you for confirming your account.";
                    return;
                }
            }

            StatusMessage = "An error has occurred";
        }
    }
}