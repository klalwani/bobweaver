﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using AgileDealer.Data.Entities.Buyer;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System.Globalization;
using System.Threading;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using System.Threading.Tasks;
using System.Net;

namespace CJI.AgileDealer.Web.WebForm
{
    public class Global : HttpApplication
    {
        public HttpApplication CurrentHttpApplication => this;
       
       public static string CurrentSession { get; set; }
       public static BuyerSession CurrentBuyerSession  { get; set; }
       public static string VcpCurrentTab { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 |
                        SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        protected void Application_Error(object sender, EventArgs e)
		{
			// Code that runs when an unhandled error occurs

			// Get the exception object.
			Exception exc = Server.GetLastError();

			var el = ExceptionHelper.LogException("Unhandled Error: ", exc, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", string.Empty);
			EmailHelper.SendEmailForNotificationWhenFailing(el);

			// Clear the error from the server
			Server.ClearError();
		}

		protected void Session_Start(object sender, EventArgs e)
        {
            var that = this;
            CurrentSession = that.Session.SessionID;
            BuyerSession buyerSession = BuyerSessionHelper.CreateNewBuyerSession(that.Request, that.Session.SessionID);
            CurrentBuyerSession = buyerSession;
            
            // async call to save Session
            BuyerSessionHelper.SaveBuyerSession(buyerSession,that.Session);
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture(ci.Name);
        }
    }
}