﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System.Configuration;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;

namespace CJI.AgileDealer.Web.WebForm.CMS
{
	public partial class CMSPage : BasedPage
	{

        private Model _model;
        public Model Model
        {
            get
            {
                if (_model != null) return _model;
                string modelString = Convert.ToString(Request.QueryString["Model"]);

                _model = MasterDataHelper.GetModelByModelTranslatedName(modelString, DealerID);
                if (_model == null && _modelTranslated != null)
                    _model = MasterDataHelper.GetModelById(ModelTranslated.ModelTranslatedId, DealerID);

                return _model;
            }
            set { _model = value; }
        }

        private ModelTranslated _modelTranslated;
        public ModelTranslated ModelTranslated
        {
            get
            {
                if (_modelTranslated != null) return _modelTranslated;
                string modelString = Convert.ToString(Request.QueryString["Model"]);

                _modelTranslated = MasterDataHelper.GetModelByTranslatedName(modelString, DealerID);
                return _modelTranslated;
            }
            set { _modelTranslated = value; }
        }

        private Make _make;
        public Make Make
        {
            get
            {
                if (_make != null) return _make;
                string makeString = Convert.ToString(Request.QueryString["Make"]);
                _make = MasterDataHelper.GetMakeByName(makeString, DealerID);
                return _make;
            }
            set { _make = value; }
        }

        private string _type;
        public string Type
        {
            get
            {
                if (!string.IsNullOrEmpty(_type)) return _type;
                string typeString = Convert.ToString(Request.QueryString["Type"]);
                _type = typeString;
                return _type;
            }
            set { _type = value; }
        }

        public bool HasBlogBaseModel { get; set; }

        public DefaultFilter DefaultFilter
        {
            get
            {
               // if (ViewState["DefaultFilter"] != null) return (DefaultFilter)ViewState["DefaultFilter"];
                DefaultFilter filters = new DefaultFilter
                {
                    DefaultTypes = new List<string> { Type },
                    DefaultModels = new List<int>() { ModelTranslated.ModelTranslatedId },
                    DefaultMakes = new List<int>() { Make.MakeId }
                };
                ViewState["DefaultFilter"] = filters;
                return (DefaultFilter)ViewState["DefaultFilter"];
            }
            set
            {
                ViewState["DefaultFilter"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
		{
			var pageContent = CMSHelper.GetContentPage(Page, LanguageId,DealerID);
			ContentPlaceHolder content = (ContentPlaceHolder)this.Master.FindControl("MainContent");
            

            if (pageContent != null && content != null)
			{
				LiteralControl litCon = new LiteralControl();
                LiteralControl litConBottom = new LiteralControl();
                pnlBodyTopContent.Controls.Add(litCon);
                pnlBodyBottomContent.Controls.Add(litConBottom);
                if (!IsPostBack)
				{

					#region display content

					#region test iframe
					//var myFrame = new HtmlGenericControl("iframe");
					//myFrame.ID = "Special1";
					//myFrame.Attributes.Add("src", "www.google.com");
					//myFrame.Attributes.Add("height", "400px");
					//myFrame.Attributes.Add("width", "100%");
					//myContent.Controls.Add(myFrame);
					#endregion

					#region loading content
					StringBuilder bodyContent = new StringBuilder();
					bodyContent.AppendLine(pageContent.BodyContent);
					bodyContent.Replace("{DealerName}", DealerName);
					bodyContent.Replace("{DealerCity}", DealerCity);
					bodyContent.Replace("{DealerState}", DealerState);
					bodyContent.Replace("{DealerImageGeneric}", DealerImageGeneric);
					bodyContent.Replace("{ManufacturerName}", ManufacturerName);

					litCon.Text = bodyContent.ToString();

                    StringBuilder bodyContentBottom = new StringBuilder();
                    bodyContentBottom.AppendLine(pageContent.BodyContentBottom);
                    bodyContentBottom.Replace("{DealerName}", DealerName);
                    bodyContentBottom.Replace("{DealerCity}", DealerCity);
                    bodyContentBottom.Replace("{DealerState}", DealerState);
                    bodyContentBottom.Replace("{DealerImageGeneric}", DealerImageGeneric);
                    bodyContentBottom.Replace("{ManufacturerName}", ManufacturerName);

                    litConBottom.Text = bodyContentBottom.ToString();
                    //litCon.Text = "<iframe name='testiFrame' id='runtimeIFrame'  frameborder='no' scrolling='auto' height='400px' width='100%'  src='http://www.google.com' style='left:0; background-color: beige;'></iframe>";

                    #endregion

                  
                    SearchControl.Visible = pageContent.ShowInventory;
                    if (pageContent.ShowInventory)
                    {
                        try
                        {
                            var InventoryFilters = pageContent.InventoryFilters.Split(new char[] { '&' });
                            for (int iCounter = 0; iCounter < InventoryFilters.Length; iCounter++)
                            {
                                if (InventoryFilters[iCounter].IndexOf("Type") > -1)
                                {
                                    Type = InventoryFilters[iCounter].Replace("Type=", "");
                                }
                                if (InventoryFilters[iCounter].IndexOf("Make") > -1)
                                {
                                    Make = MasterDataHelper.GetMakeByName(InventoryFilters[iCounter].Replace("Make=", ""), DealerID);
                                }
                                if (InventoryFilters[iCounter].IndexOf("Model") > -1)
                                {
                                    ModelTranslated = MasterDataHelper.GetModelByTranslatedName(InventoryFilters[iCounter].Replace("Model=", ""), DealerID);
                                }
                                if (InventoryFilters[iCounter].IndexOf("Keywords") > -1)
                                {
                                    ((HiddenField)SearchControl.FindControl("HdfSearchText")).Value = InventoryFilters[iCounter].Replace("Keywords=", "");
                                }

                                if (InventoryFilters[iCounter].IndexOf("Years") > -1)
                                {
                                    var yearData = InventoryFilters[iCounter].Split(new char[] { ',' });
                                    for (int yearCounter = 0; yearCounter < yearData.Length; yearCounter++)
                                        SearchControl.VehicleSearchCriteria.Years.Add(Convert.ToInt16(yearData[yearCounter].Replace("Years=", "")));
                                }
                            }
                            DefaultFilter filters = new DefaultFilter
                            {
                                DefaultTypes = new List<string> { Type },
                                DefaultModels = new List<int> { ModelTranslated != null ? ModelTranslated.ModelTranslatedId : 0 },
                                DefaultMakes = new List<int>() { Make!=null ? Make.MakeId : 0},
                                DefaultModelTrims =
                                       new List<VehicleModelTrim>()
                                       {
                                    new VehicleModelTrim()
                                    {
                                        modelId = ModelTranslated != null ? ModelTranslated.ModelTranslatedId : 0,
                                        trimId = 0
                                    }
                                       },
                                // IsExpandModel = true,
                                //IsExpandType = true
                            };
                           
                            SearchControl.DefaultFilter = filters;
                        }
                        catch {
                            SearchControl.Visible = false;
                        }
                     
                        
                    }

                    #endregion

                }
				else
				{
					var postData = pageContent.PostData;
					if (postData != null)
					{
						#region process form post data

						var formKeys = Request.Form.Keys;
						var sb = new StringBuilder();

						string userEmail = null;
						string userName = null;
						

						userEmail = Request.Form.Get(postData.PostEmailFieldName);
						userName = Request.Form.Get(postData.PostUserFieldName);

				        var responseText = new StringBuilder();
                        responseText.Append(postData.PostResponseText);
                        ReplaceContent(responseText, postData.PostEmailFieldName, userEmail);
                        ReplaceContent(responseText, postData.PostUserFieldName, userName);

                        var postDataFormart = new StringBuilder();

                        // if post data format isnt null then do this
                        if (!string.IsNullOrEmpty(postData.PostDataFormat))
                        {
                            postDataFormart.Append(postData.PostDataFormat);

                            ReplaceContent(postDataFormart, postData.PostEmailFieldName, userEmail);
                            ReplaceContent(postDataFormart, postData.PostUserFieldName, userName);
                        }
                        else
                        {
                            postDataFormart.Append(responseText);
                        }

                        var adminMail = ConfigurationManager.AppSettings["smtpAcct"];
                        
                        
						foreach (var key in formKeys)
						{
							var keyStr = key.ToString();
							if (keyStr.StartsWith("frm"))
							{
								sb.AppendLine(string.Format("{0}: {1}</br>", key, Request.Form[key.ToString()]));
							}
						}


                        litCon.Text = responseText.ToString();

                        // post to system
                        EmailHelper.SendEmailAsync(new Models.Email
                        {
                            MailSubject = postData.PostTitle,
                            MailTo = postData.PosToEmailValue,
                            MailCC = postData.PosToEmailCCValue, 
                            MailText = sb.ToString(),
                            MailFrom = adminMail,
                        }, adminMail, true, postData.PostToEmailBCCValue);

                        // post to user here
                        EmailHelper.SendEmailAsync(new Models.Email
                        {
                            MailSubject = postData.PostTitle,
                            MailTo = userEmail,
                            MailText = postDataFormart.ToString(),
                            MailFrom = adminMail,
                        }, adminMail, true, postData.PostToEmailBCCValue);
                        
                        
					}
					#endregion

				}
			}
		}

		private void ReplaceContent(StringBuilder content,string oldData, string newData)
		{
			content.Replace("{" + oldData + "}", newData);
		}
	}
}