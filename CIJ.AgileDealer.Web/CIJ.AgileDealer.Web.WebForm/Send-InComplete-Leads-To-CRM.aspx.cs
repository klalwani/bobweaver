﻿using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Controllers;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm
{
    public partial class Send_InComplete_Leads_To_CRM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Task.Run(() => ProcessVehicleFeedbacks());
            ProcessData();
        }

        private async Task ProcessVehicleFeedbacks()
        {
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var vehicleFeedbacks = db.VehicleFeedBacks.ToList().Where(x => x.VehicleFeedBackId == 692).ToList();
                    HomeNetVehicle vehicle = new HomeNetVehicle();
                    foreach (var vehicleFeedBack in vehicleFeedbacks)
                    {
                        int agilevehicleId = Convert.ToInt32(vehicleFeedBack.AgileVehicleId);
                        vehicle = VehicleHelper.GetVehicleByVehicleId(agilevehicleId, vehicleFeedBack.DealerID);
                        vehicleFeedBack.HomeNetVehicle = vehicle;
                        await Task.Run(() =>
                        {
                            EmailController.SendEmailForCheckAvailability(JsonHelper.Serialize(vehicleFeedBack), vehicleFeedBack.DealerID);
                           // EmailController.SendEmailConfirmationCheckAvailability(vehicleFeedBack.Email);
                        }).ContinueWith(t =>
                        {
                        }
                );
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        private static List<NotificationSetting> _notificationSettings;
        public static List<NotificationSetting> NotificationSettings
        {
            get
            {
                _notificationSettings = NotificationHelper.GetNotificationSettings(DealerInfoHelper.DealerID);

                return _notificationSettings;
            }
            set { _notificationSettings = value; }
        }

        private static List<NotificationSetting> _notificationSettingsFullList;
        public static List<NotificationSetting> NotificationSettingsFullList
        {
            get
            {
                _notificationSettingsFullList = NotificationHelper.GetNotificationSettings();

                return _notificationSettingsFullList;
            }
            set { _notificationSettingsFullList = value; }
        }

        private static Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerInfoHelper.DealerID);

        private void ProcessData()
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<Applicant> lstApplicants = db.Applicants.Where(x => x.ApplicantId == 912).ToList();

                foreach (Applicant applicant in lstApplicants)
                {
                    try
                    {
                        ContactStructure contactStructure = new ContactStructure();

                        contactStructure.ApplicantInfo =
                            db.ApplicantInfos.FirstOrDefault(x => x.ApplicantId == applicant.ApplicantId);

                        if (contactStructure.ApplicantInfo == null)
                        {
                            contactStructure.ApplicantInfo = new ApplicantInfo()
                            {
                                FirstName = applicant.FirstName,
                                LastName = applicant.LastName,
                                Email = "",
                                PhoneNumber = applicant.PhoneNumber
                            };
                        }

                        contactStructure.ContactInfo =
                            db.ContactInfos.FirstOrDefault(x => x.ApplicantId == applicant.ApplicantId);

                        if (contactStructure.ContactInfo == null)
                        {
                            contactStructure.ContactInfo = new ContactInfo()
                            {
                                City = "",
                                State = "",
                                CurrentAddress = "",
                                ZipCode = applicant.ZipCode
                            };
                        }
                        if (contactStructure.ApplicantInfo != null && contactStructure.ContactInfo != null)
                        {
                            if (contactStructure != null)
                            {
                                NotificationSetting currentSetting = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.Finance.ToString() && x.DealerID == 1);
                                NotificationSetting allLeadEmails = NotificationSettings.FirstOrDefault(x => x.LeadType == NotificationEnum.AllLead.ToString() && x.DealerID == 1);
                                string mailSubject = $"Website Finance Application from {contactStructure.ApplicantInfo.FirstName} {contactStructure.ApplicantInfo.LastName}";
                                if ((currentSetting != null && !string.IsNullOrEmpty(currentSetting.AdfEmail)) ||
                                    (allLeadEmails != null && !string.IsNullOrEmpty(allLeadEmails.AdfEmail)))
                                {
                                    string adfMailContent = EmailGenerator.CreateMailContentWithTemplateForFinanceAdf(contactStructure);
                                    if (!string.IsNullOrEmpty(adfMailContent))
                                    {
                                        string mailTo = currentSetting != null ? currentSetting.AdfEmail : string.Empty;
                                        string allLeadEmail = allLeadEmails != null ? allLeadEmails.AdfEmail : string.Empty;

                                        if (!string.IsNullOrEmpty(allLeadEmail))
                                        {
                                            if (!mailTo.ToLower().Contains(allLeadEmail.ToLower()))
                                            {
                                                mailTo = $"{mailTo};{allLeadEmail}";
                                            }
                                        }

                                        Email adfEmail = EmailGenerator.CreateNewEmail(mailTo, mailSubject, adfMailContent);

                                        if (adfEmail != null)
                                        {
                                            EmailHelper.SendEmail(adfEmail, string.Empty, false);
                                        }
                                    }
                                }


                            }
                        }

                    }
                    catch
                    {

                        Response.Write("Exception :" + applicant.ApplicantId);
                    }
                }
            }


        }

    }
}