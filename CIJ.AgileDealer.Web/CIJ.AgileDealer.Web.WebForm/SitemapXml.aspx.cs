﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System.Security.Policy;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm
{
    public partial class SitemapXml : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";

            var baseUrl = string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Authority);
            XmlDocument xmlResult = SiteMapHelper.GetSiteMapLocationData(ns, baseUrl,DealerID);
            Response.ContentType = "application/xml";
            Response.Write(xmlResult.OuterXml);
            Response.End();
        }

        private void GetSiteMapXml()
        {
        }
    }
}