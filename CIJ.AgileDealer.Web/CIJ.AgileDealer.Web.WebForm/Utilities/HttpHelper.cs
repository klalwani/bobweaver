﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Buyer;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class HttpHelper
    {
        #region Http Helper Methods
        public static string GetCurrentIpAddressOfClient(HttpRequest request)
        {
            return (request.ServerVariables["HTTP_X_FORWARDED_FOR"] ??
                  request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
        }

        public static string GetBrowserType(HttpRequest request)
        {
            return request.Browser.Type;
        }

        public static string GetPlatform(HttpRequest request)
        {
            return request.Browser.Platform;
        }
        public static bool IsMobile(HttpRequest request)
        {
            return request.Browser.IsMobileDevice;
        }

        public static string GetMobileDeviceManufacturer(HttpRequest request)
        {
            return request.Browser.MobileDeviceManufacturer;
        }

        public static string GetMobileDeviceModel(HttpRequest request)
        {
            return request.Browser.MobileDeviceModel;
        }

        public static Uri GetUrlDeferer(HttpRequest request)
        {
            return request.UrlReferrer;
        }

        public static string GetCompleteUrl(HttpRequest request)
        {
            return request.Url.GetLeftPart(UriPartial.Authority);
        }

        public static Uri GetUrl(HttpRequest request)
        {
            return request.Url;
        }

        public static bool IsUrlExist(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Timeout = 2000; // set timeout to 2 seconds
                request.Method = "HEAD";

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    response.Close();
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        #endregion

        #region Handle Request Method

        public async static Task SetPageVisited(HttpRequest request, int BuyerSessionId)
        {
            bool iscrawler = true;
            if (request != null && !string.IsNullOrEmpty(request.UserAgent))
                iscrawler = Regex.IsMatch(request.UserAgent, @"bot|crawler|baiduspider|80legs|ia_archiver|voyager|curl|wget|yahoo! slurp|mediapartners-google", RegexOptions.IgnoreCase);
            if (!iscrawler)
            {
                BuyerPageVisited pageVisited = new BuyerPageVisited();
                try
                {
                    using (ServicePageDbContext db = new ServicePageDbContext())
                    {

                        pageVisited.BuyerSessionId = BuyerSessionId;
                        pageVisited.CreatedDt = DateTime.Now;
                        pageVisited.ModifiedDt = DateTime.Now;
                        pageVisited.Url = request?.Url.ToString();
                        pageVisited.UrlReferer = request?.UrlReferrer?.ToString() ?? string.Empty;
                        pageVisited.DealerID = 1;
                        db.BuyerPageVisiteds.Add(pageVisited);
                        await db.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    var el = ExceptionHelper.LogException("HttpHelper.SetPageVisited() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: SetPageVisited with buyerSessionId: " + pageVisited.BuyerSessionId);
                    EmailHelper.SendEmailForNotificationWhenFailing(el);
                }
            }
        }

        public async static Task SetVehicleDisplayPage(VehicleData vehicle, int buyerSessionId)
        {
            BuyerVehicleDisplayPage pageVisited = new BuyerVehicleDisplayPage();
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    pageVisited.BuyerSessionId = buyerSessionId;
                    pageVisited.CreatedDt = DateTime.Now;
                    pageVisited.ModifiedDt = DateTime.Now;
                    pageVisited.DealerID = 1;
                    pageVisited.AgileVehicleId = vehicle.AgileVehicleId;
                    db.BuyerVehicleDisplayPages.Add(pageVisited);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var el = ExceptionHelper.LogException("HttpHelper.SetVehicleDisplayPage() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: SetVehicleDisplayPage with buyerSessionId: " + buyerSessionId);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public static Control GetPostBackControl(this Page page)
        {
            if (!page.IsPostBack)
                return null;

            Control control = null;
            // first we will check the "__EVENTTARGET" because if post back made by the controls
            // which used "_doPostBack" function also available in Request.Form collection.
            string controlName = page.Request.Params["__EVENTTARGET"];
            if (!String.IsNullOrEmpty(controlName))
            {
                control = page.FindControl(controlName);
            }
            else
            {
                // if __EVENTTARGET is null, the control is a button type and we need to
                // iterate over the form collection to find it

                // ReSharper disable TooWideLocalVariableScope
                string controlId;
                Control foundControl;
                // ReSharper restore TooWideLocalVariableScope

                foreach (string ctl in page.Request.Form)
                {
                    // handle ImageButton they having an additional "quasi-property" 
                    // in their Id which identifies mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        controlId = ctl.Substring(0, ctl.Length - 2);
                        foundControl = page.FindControl(controlId);
                    }
                    else
                    {
                        foundControl = page.FindControl(ctl);
                    }

                    if (!(foundControl is Button || foundControl is ImageButton)) continue;

                    control = foundControl;
                    break;
                }
            }

            return control;
        }


        #endregion

    }
}