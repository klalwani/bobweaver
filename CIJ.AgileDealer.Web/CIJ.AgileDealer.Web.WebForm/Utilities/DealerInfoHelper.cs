﻿using System.Collections.Generic;
using System.Linq;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    /// <summary>
    /// Dealer info cache 
    /// - Contains everything we need at runtime for dealer 
    /// </summary>
    public static class DealerInfoHelper
    {
        public static int DealerID { get; set; }
        public static string DealerName { get; private set; }
        public static string DealerCity { get; private set; }
        public static string DealerState { get; private set; }
        public static string ManufactureName { get; set; }
        public static string EleadTrackAdress { get; set; }

        public static string ImageServer { get; private set; }
        public static string DealerImageStorage { get; private set; } // this points to azure blob - https://gatorford.blob.core.windows.net/images/
        public static string DealerImageStock { get; private set; } // this points to cdn googleapis etc
        public static string DealerImageGeneric { get; private set; } // this points to azure blob - https://agiledealer.blob.core.windows.net/generic
                                                         // https://agiledealer.blob.core.windows.net/generic/content/slider/Ford-F-150-Lariat.jpg  

        public static List<HeaderModel> DealerHeaders { get; set; }

        /// <summary>
        /// Reset dealer info cache instace
        /// </summary>
        public static void ResetDealerInstance()
        {
            PopulateDealerInfo();
        }

        static DealerInfoHelper()
        {
            PopulateDealerInfo();
        }

        private static void PopulateDealerInfo()
        {
            int languageId = 1; // default to english

            using (var db = new ServicePageDbContext())
            {
                var dealer = db.Dealers.Include("Locations").Include("Manufacture").FirstOrDefault();

                if (dealer != null)
                {
                    DealerName = dealer.Name;
                    DealerID = dealer.DealerID;
                    DealerImageGeneric = dealer.ImageServerGeneric;
                    DealerImageStock = dealer.VcpImageStockServer;
                    DealerImageStorage = dealer.ImageServer;
                    ManufactureName = dealer.Manufacture.Name;
                    ImageServer = dealer.ImageServer;
                    EleadTrackAdress = dealer.EleadTrackAdress;
                    if (dealer.Locations != null && dealer.Locations.Count > 0)
                    {
                        DealerCity = dealer.Locations[0].City;
                        DealerState = dealer.Locations[0].State;
                    }
                }

                var query = from dealers in db.Dealers
                            join header in db.Headers on dealers.DealerID equals header.DealerID
                            join header_Trans in db.Header_Trans on header.HeaderID equals header_Trans.HeaderId into tblTrans
                            from tblTran in tblTrans.DefaultIfEmpty()
                            join location in db.Locations on new { LocationTypeID = 1, DealerID = dealers.DealerID } equals new { location.LocationTypeID, location.DealerID }

                            select new HeaderModel()
                            {
                                HeaderID = header.HeaderID,
                                PeronsalizationAltText = tblTran.PeronsalizationAltText
                                                      ?? header.PeronsalizationAltText,
                                PersonalizationText = tblTran.PersonalizationText ?? header.PersonalizationText,
                                PersonalizationLogo = tblTran.PersonalizationLogo ?? header.PersonalizationLogo,
                                PeronsalizationUrl = tblTran.PeronsalizationUrl ?? header.PeronsalizationUrl,
                                SiteLink = header.SiteLink,
                                SiteLogoAltText = header.SiteLogoAltText,
                                SiteLogo = header.SiteLogo,
                                CTAImage = tblTran.CTAImage ?? header.CTAImage,
                                CTALink = tblTran.CTALink ?? header.CTALink,
                                CTAImageAltText = tblTran.CTAImageAltText ?? header.CTAImageAltText,
                                SalesPhone = location.SalesPhone,
                                Street = location.Street1,
                                City = location.City,
                                Zip = location.Zip,
                                State = location.State,
                                AlternatePhone = location.AlternatePhone,
                                ExitSignButton = header.ExitSignButton,
                                LanguageId = (tblTran != null) ? tblTran.LanguageId : languageId
                            };


                DealerHeaders = query.ToList();
            }
        }
    }
}