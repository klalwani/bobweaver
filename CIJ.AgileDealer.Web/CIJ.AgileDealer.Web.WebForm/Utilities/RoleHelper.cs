﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;
using CIJ.AgileDealer.Web.Base.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public class RoleHelper
    {
        public void CreateRoles(ServicePageDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            List<string> roleList = new List<string>() { "Dealer", "Admin", "User" };
            foreach (string role in roleList)
            {
                if (!roleManager.RoleExists(role))
                {
                    roleManager.Create(new IdentityRole(role));
                }
            }
        }

        public static IdentityRole GetRoleByUserId(string userId)
        {
            IdentityRole role;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                string roleId = userManager.FindById(userId).Roles.Select(x => x.RoleId).FirstOrDefault() ?? string.Empty;
                role = roleManager.FindById(roleId);
            }
            return role;
        }

        public static List<MasterData> GetRoles()
        {
            List<MasterData> roles = new List<MasterData>();

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                var roleList = roleManager.Roles;
                if (roleList.Any())
                {
                    foreach (var role in roleList)
                    {
                        roles.Add(new MasterData() { IdString = role.Id, Name = role.Name });
                    }
                }
            }
            return roles;
        }

        public static bool AssignUserToRole(string userId, string roleName)
        {
            bool isSuccess = true;
            try
            {
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ServicePageDbContext()));
                userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                };

                var oldRole = GetRoleByUserId(userId);
                if (oldRole != null)
                {
                    userManager.RemoveFromRole(userId, oldRole.Name);
                }
                var idResult = userManager.AddToRole(userId, roleName);
                isSuccess = idResult.Succeeded;
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var el = ExceptionHelper.LogException("RoleHelper.AssignUserToRole() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: SetVehicleDisplayPage with buyerSessionId: ");
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }

            return isSuccess;
        }

        //public static bool CheckAdminPermissionOfCurrentUser(bool isAuthenticated, IPrincipal currentUser)
        //{
        //    bool isAllow = false;
        //    if (!isAuthenticated || (!currentUser.IsInRole("Admin") && !currentUser.IsInRole("Dealer")))
        //    {
        //        httpResponse.Redirect("~/Account/Login");
        //    }

        //    return isAllow;
        //}
    }
}