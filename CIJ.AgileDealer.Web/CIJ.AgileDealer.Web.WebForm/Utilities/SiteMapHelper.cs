﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using SiteMapData = AgileDealer.Data.Entities.SiteMap;
using HomeData = AgileDealer.Data.Entities.Home;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class SiteMapHelper
    {
        private static string _baseUrl = String.Empty;
        public static string GetSiteMapData(int dealerId)
        {
            string result = string.Empty;
            List<SiteMapData.Sitemap> siteMaps = GetSiteMaps(dealerId);
            List<SiteMapData.SiteMapLocation> siteMapLocations = new List<SiteMapData.SiteMapLocation>();
            int count = siteMaps.Count;
            for (int i = 0; i < count; i++)
            {
                siteMaps[i].Locations = GetSiteMapLocations(siteMaps[i].SitemapId);
            }

            result = XmlHelper.Serialize(siteMaps);
            return result;
        }

        public static XmlDocument GetSiteMapLocationData(XNamespace ns, string baseUrl,int dealerId)
        {
            string temp = string.Empty;
            _baseUrl = baseUrl;
            XmlDocument document = new XmlDocument();
            SiteMapData.SiteMapUrlSet urlSet = new SiteMapData.SiteMapUrlSet()
            {
                SiteMapLocations = new List<SiteMapData.SiteMapLocation>()
            };
            List<SiteMapData.SiteMapLocation> siteMapLocations = GetSiteMapLocationList(dealerId);
            List<SiteMapData.SiteMapLocation> blogEntries = GetBlogEntries(dealerId);
            List<SiteMapData.SiteMapLocation> homePageEntries = GetHomePageEntries(dealerId);
            List<SiteMapData.SiteMapLocation> carPageEntries = GetAgileVehicles(dealerId);

            urlSet.SiteMapLocations.AddRange(siteMapLocations);
            urlSet.SiteMapLocations.AddRange(blogEntries);
            urlSet.SiteMapLocations.AddRange(homePageEntries);
            urlSet.SiteMapLocations.AddRange(carPageEntries);
            temp = XmlHelper.Serialize(urlSet);
            document = XmlHelper.GetXmlDocumentByString(temp);
            XmlNodeList nodes = document.SelectNodes("//CreatedDt");
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
            nodes = document.SelectNodes("//ModifiedDt");
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }
            nodes = document.SelectNodes("//DealerID");
            foreach (XmlNode node in nodes)
            {
                node.ParentNode.RemoveChild(node);
            }

            if (document != null && document.DocumentElement != null)
            {
                document.DocumentElement.SetAttribute("xmlns", ns.NamespaceName);
                document.DocumentElement.SetAttribute("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
                document.DocumentElement.RemoveAttribute("xmlns:xsd");
            }
            return document;
        }

        public static List<SiteMapData.SiteMapLocation> GetSiteMapLocationList(int dealerId)
        {
            List<SiteMapData.SiteMapLocation> siteMapLocations;
            List< SiteMapData.SiteMapLocation> siteMapWithAbsUrl= new List<SiteMapData.SiteMapLocation>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                siteMapLocations = db.SiteMapLocations.SqlQuery("Usp_GetSitemapXMLData").ToList();
                siteMapLocations.ForEach(x=> siteMapWithAbsUrl.Add(new SiteMapData.SiteMapLocation
                {
                    Priority = x.Priority,
                    LastModified = DateTime.Now.ToString("yyyy-MM-dd"),
                    ChangeFrequency = x.ChangeFrequency,
                    Url = _baseUrl + x.Url
                }));
            }
            return siteMapWithAbsUrl;
        }

        private static List<HomeData.HomePageVehicle> AddUsedAndCertifiedEntries(List<HomeData.HomePageVehicle> homePageVehicles,int dealerId)
        {
            List<HomeData.HomePageVehicle> expandedList = new List<HomeData.HomePageVehicle>();
            foreach (var vehicle in homePageVehicles.Where(x=>x.DealerID == dealerId))
            {
                var urlFrags = vehicle.VehicleRelativeUrl.Split('/');
                var tailFrags = String.Empty;
                for (int pos = 3; pos < urlFrags.Length; pos++)
                {
                    tailFrags += '/' + urlFrags[pos];
                }
                vehicle.VehicleRelativeUrl = _baseUrl + vehicle.VehicleRelativeUrl;
                expandedList.Add(vehicle);
                if (urlFrags.Length > 1)
                {
                    var certRelUrl = _baseUrl + urlFrags[0] + '/' + urlFrags[1] + '/' + "certified" + tailFrags;
                    var usedRelUrl = _baseUrl +  urlFrags[0] + '/' + urlFrags[1] + '/' + "used" + tailFrags;
                    expandedList.Add(new HomeData.HomePageVehicle()
                    {
                        VehicleRelativeUrl = certRelUrl
                    });
                    expandedList.Add(new HomeData.HomePageVehicle()
                    {
                        VehicleRelativeUrl = usedRelUrl
                    });
                }
            }
            return expandedList;
        }
        public static List<SiteMapData.SiteMapLocation> GetHomePageEntries(int dealerId)
        {
            List<SiteMapData.SiteMapLocation> siteMapLocations = new List<SiteMapData.SiteMapLocation>();

            using (var context = new AppDbContext())
            {
                var SiteMapXML = context.Database.SqlQuery<string>("exec Usp_GetFatFooters 0,1");
                foreach (var item in SiteMapXML)
                {
                    siteMapLocations.Add(new SiteMapData.SiteMapLocation
                    {
                        Url = _baseUrl + item,
                        Priority = 1,
                        LastModified = DateTime.Now.ToString("yyyy-MM-dd"),
                        ChangeFrequency = SiteMapData.SiteMapLocation.eChangeFrequency.daily
                    });
                }
            }
            //using (ServicePageDbContext db = new ServicePageDbContext())
            //{

            //    var homePageVehicles = db.HomePageVehicles.Where(x=>x.DealerID == dealerId).ToList();
            //    homePageVehicles = AddUsedAndCertifiedEntries(homePageVehicles,dealerId);
            //    homePageVehicles.ForEach(x => siteMapLocations.Add(new SiteMapData.SiteMapLocation
            //    {
            //        Url = x.VehicleRelativeUrl,
            //        Priority = 1,
            //        LastModified = DateTime.Now.ToString("yyyy-MM-dd"),
            //        ChangeFrequency = SiteMapData.SiteMapLocation.eChangeFrequency.daily
            //    }));
            //}
            return siteMapLocations;
        }
        public static List<SiteMapData.SiteMapLocation> GetBlogEntries(int dealerId)
        {
            List<SiteMapData.SiteMapLocation> siteMapLocations = new List<SiteMapData.SiteMapLocation>();

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var blogEntryIds = db.BlogEntries_BlogTags.Where(x => x.BlogTagId == 1 && x.DealerID == dealerId).Select(x => x.BlogEntryId);
                var blogEntries = db.BlogEntrys.Where(x=> !blogEntryIds.Contains(x.BlogEntryId) && x.DealerID == dealerId).ToList();
                blogEntries.ForEach(x=>siteMapLocations.Add(new SiteMapData.SiteMapLocation
                {
                    Url = _baseUrl + "/research/detail/" + x.PostUrl,
                    Priority = 1,
                    LastModified = x.ModifiedDt.ToString("yyyy-MM-dd"),
                    ChangeFrequency = SiteMapData.SiteMapLocation.eChangeFrequency.daily
                } ));
            }
            return siteMapLocations;
        }

        public static List<SiteMapData.SiteMapLocation> GetAgileVehicles(int dealerId)
        {
            List<SiteMapData.SiteMapLocation> siteMapLocations = new List<SiteMapData.SiteMapLocation>();

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var cars = db.AgileVehicles.Where(w => w.IsSold == false && w.DealerID == dealerId).ToList();
                cars.ForEach(x => siteMapLocations.Add(new SiteMapData.SiteMapLocation
                {
                    Url = _baseUrl + "/vehicle-info/" + (x.IsNew.ToString()=="True" ? "new" : x.IsCertified ? "used-certified" : "used") + '-' + x.Year.ToString() + '-' + db.Makes.First(f=>f.MakeId == x.MakeId).Name + '-' + db.Models.First(f => f.ModelId == x.ModelId).Name.Replace(' ','-') + '-' + x.Vin,
                    Priority = 1,
                    LastModified = DateTime.Now.ToString("yyyy-MM-dd"),
                    ChangeFrequency = SiteMapData.SiteMapLocation.eChangeFrequency.daily
                }));
            }
            return siteMapLocations;
        }
        public static List<SiteMapData.Sitemap> GetSiteMaps(int dealerId)
        {
            List<SiteMapData.Sitemap> sitemaps;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                sitemaps = db.SiteMaps.Where(x=>x.DealerID == dealerId).ToList();
            }
            return sitemaps;
        }

        public static SiteMapData.SiteMapLocation[] GetSiteMapLocations(int dealerId,int siteMapId = 0)
        {
            SiteMapData.SiteMapLocation[] siteMapLocations;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                siteMapLocations = db.SiteMapLocations.Where(x=> (siteMapId == 0 || x.SitemapId == siteMapId) && x.DealerID == dealerId).ToArray();
            }
            return siteMapLocations;
        }
    }
}