﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CIJ.AgileDealer.Web.Base.Extensions;
using Microsoft.Ajax.Utilities;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class VehicleHelper
    {
        private static char[] charSeparators = { ' ' };

        private static string BreakNewLine = "</br>";

        private static StringComparison StringComparisonOrdinalIgnoreCase = StringComparison.OrdinalIgnoreCase;

        private static List<VehicleCriteriaMapping> _vehicleCriteriaMappings;

        public static List<VehicleCriteriaMapping> VehicleCriteriaMappings
        {
            get
            {
                if (_vehicleCriteriaMappings == null || !_vehicleCriteriaMappings.Any())
                    _vehicleCriteriaMappings = MasterDataHelper.GetVehicleCriteriaMappings(DealerInfoHelper.DealerID);
                return _vehicleCriteriaMappings;
            }
            set { _vehicleCriteriaMappings = value; }
        }

        private static List<Price> _prices;
        private static List<Price> PriceList
        {
            get { return _prices ?? (_prices = MasterDataHelper.GetPriceList()); }
            set { _prices = value; }
        }

        private static List<Drive> _drives;
        private static List<Drive> DriveList
        {
            get { return _drives ?? (_drives = MasterDataHelper.GetDriveList()); }
            set { _drives = value; }
        }

        private static List<Mileage> _mileages;
        private static List<Mileage> MileageList
        {
            get { return _mileages ?? (_mileages = MasterDataHelper.GetMileageList()); }
            set { _mileages = value; }
        }
        private static List<VehicleFeatureMaster> _vehiclefeatures;
        private static List<VehicleFeatureMaster> VehicleFeaturesList
        {
            get { return _vehiclefeatures ?? (_vehiclefeatures = MasterDataHelper.GetVehicleFeatureMasterList()); }
            set { _vehiclefeatures = value; }
        }

        public static Model GetModelByModelId(int modelId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.Models.FirstOrDefault(x => x.ModelId == modelId);
            }
        }


        public static List<ModelTranslated> GetModelTranslatedForVehicleTypePage(string modelName, string makeName,
            List<string> segments, int dealerId)
        {
            List<ModelTranslated> modelTranslateds = new List<ModelTranslated>();
            if (!string.IsNullOrEmpty(modelName) || !string.IsNullOrEmpty(makeName) || (segments != null && segments.Any()))
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    List<CompetitiveModel> competitiveModels = GetCompetitiveModels(modelName, makeName, segments, dealerId);
                    List<Model> models;
                    if (competitiveModels != null && competitiveModels.Any())
                    {
                        List<string> competitiveModelNameList = competitiveModels.Select(x => x.Name).ToList();
                        if (competitiveModelNameList.Any())
                        {
                            models = MasterDataHelper.GetModelByListName(competitiveModelNameList, dealerId);
                            if (models.Any())
                            {
                                List<int> modelListId =
                                    models.Where(x => x.ModelTranslatedId.HasValue)
                                        .Select(a => a.ModelTranslatedId.Value)
                                        .ToList();
                                modelTranslateds = MasterDataHelper.GetModelTranslatedsByModels(modelListId, dealerId);
                            }
                        }
                    }
                }
            }
            return modelTranslateds;
        }

        public static List<BodyStyleType> GetBodyStylesByModeltranslated(List<ModelTranslated> modelTranslateds, int dealerId)
        {
            List<BodyStyleType> result = new List<BodyStyleType>();
            if (modelTranslateds.Any())
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    List<int> modeltranslatedIds = modelTranslateds.Select(x => x.ModelTranslatedId).ToList();
                    List<int> bodyStyleIds = (from av in db.AgileVehicles
                                              join model in db.Models on av.ModelId equals model.ModelId
                                              where model.ModelTranslatedId.HasValue &&
                                                    modeltranslatedIds.Contains(model.ModelTranslatedId.Value)
                                              select av.BodyStyleId).ToList();
                    if (bodyStyleIds.Any())
                    {
                        result = db.BodyStyles.Include(a => a.BodyStyleType).Where(x => bodyStyleIds.Contains(x.BodyStyleId)).Select(b => b.BodyStyleType).ToList();
                    }
                }
            }

            return result;
        }

        public static List<VehicleKeyPairValueData> GetNamePlates(int dealerId)
        {
            List<VehicleKeyPairValueData> result = new List<VehicleKeyPairValueData>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<NamePlate> nameplates = db.NamePlates.Where(x => x.DealerID == dealerId).ToList();
                if (nameplates.Any())
                {
                    foreach (NamePlate nameplate in nameplates)
                    {
                        VehicleKeyPairValueData newItem = new VehicleKeyPairValueData();
                        newItem.Id = nameplate.NamePlateId;
                        newItem.Name = $"{nameplate.Make}-{nameplate.Model}-{nameplate.Year}";
                        List<NamePlateTrim> tempNamePlateTrims = GetNamePlateTrimsByNamePlateId(nameplate.NamePlateId, dealerId);
                        if (tempNamePlateTrims.Any())
                        {
                            newItem.SubList = tempNamePlateTrims.Select(x => new MasterData
                            {
                                Id = x.NamePlateTrimId,
                                Name = x.Name
                            }).ToList();
                        }
                        result.Add(newItem);

                    }
                }
            }
            return result;
        }

        public static List<Segment> GetSegments(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.Segments.ToList();
            }
        }

        public static List<MasterData> GetCompetitiveModelAsMasterData(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> tempMatched = new List<int>();
                List<string> models = MasterDataHelper.GetModels(dealerId).Select(x => x.Name).ToList();
                List<MasterData> tempModel = db.CompetitiveModels.Select(x => new MasterData { Id = x.CompetitiveModelId, Name = x.Name }).ToList();
                if (tempModel != null && tempModel.Any())
                {
                    foreach (string model in models)
                    {
                        foreach (MasterData competitiveModel in tempModel)
                        {
                            if (CheckModelMatchedWithCompetitiveModel(model, competitiveModel.Name, dealerId))
                            {
                                tempMatched.Add(competitiveModel.Id);
                            }
                        }
                    }
                }
                return db.CompetitiveModels.Where(x => tempMatched.Contains(x.CompetitiveModelId)).Select(x => new MasterData
                {
                    IdString = x.MakeName,
                    Name = x.MakeName
                }).OrderBy(x => x.Name).Distinct().ToList();
            }
        }

        public static List<NamePlateTrim> GetNamePlateTrimsByNamePlateId(int namePlateId, int dealerId)
        {
            List<NamePlateTrim> namePlateTrims = new List<NamePlateTrim>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                namePlateTrims = (from np in db.NamePlates
                                  join t in db.TrimLists on np.NamePlateId equals t.NamePlateId
                                  join npt in db.NamePlateTrims on t.TrimListId equals npt.TrimListId
                                  where np.NamePlateId == namePlateId && np.DealerID == dealerId
                                  select npt).ToList();
            }
            return namePlateTrims;
        }

        public static List<BodyStyleType> GetBodyStyleTypesForVehicleTypePage(List<string> vehicleTypeNames, int dealerId)
        {
            List<BodyStyleType> bodyStyleTypes = new List<BodyStyleType>();
            if (vehicleTypeNames != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    List<string> standardBodies =
                        db.VehicleStandardBodies.Where(x => vehicleTypeNames.Contains(x.TranslatedBodyStyleName))
                            .Select(a => a.Name)
                            .ToList();
                    if (standardBodies.Any())
                    {
                        List<string> bodyStyles =
                            db.Vehicles.Where(x => standardBodies.Contains(x.standardbody)).Select(a => a.body).ToList();
                        bodyStyleTypes = (from b in db.BodyStyles
                                          join bt in db.BodyStyleTypes on b.BodyStyleTypeId equals bt.BodyStyleTypeId
                                          where bodyStyles.Contains(b.Name)
                                          select bt).ToList();
                    }
                }
            }
            return bodyStyleTypes;
        }

        public static List<VehicleStandardBody> GetBodyStyleTypesOfStandardBodyForVehicleTypePage(List<string> vehicleTypeNames, int dealerId)
        {
            List<VehicleStandardBody> bodyStyleTypes = new List<VehicleStandardBody>();
            if (vehicleTypeNames != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    bodyStyleTypes = (from vsb in db.VehicleStandardBodies
                                      join bt in db.BodyStyleTypes on vsb.VehicleStandardBodyId equals bt.VehicleStandardBodyId
                                      where vehicleTypeNames.Contains(vsb.Name.ToLower())
                                      select vsb).ToList();
                }
            }
            return bodyStyleTypes;
        }

        public static List<CompetitiveModel> GetCompetitiveModels(MasterData data, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> tempMatched = new List<int>();
                List<string> models = MasterDataHelper.GetModels(dealerId).Select(x => x.Name).ToList();
                List<MasterData> tempModel = db.CompetitiveModels.Select(x => new MasterData { Id = x.CompetitiveModelId, Name = x.Name }).ToList();
                if (tempModel != null && tempModel.Any())
                {
                    foreach (string model in models)
                    {
                        foreach (MasterData competitiveModel in tempModel)
                        {
                            if (CheckModelMatchedWithCompetitiveModel(model, competitiveModel.Name, dealerId))
                            {
                                tempMatched.Add(competitiveModel.Id);
                            }
                        }
                    }
                }

                return
                    db.CompetitiveModels.Where(
                            x => (tempMatched.Contains(x.CompetitiveModelId)) &&
                            ((!string.IsNullOrEmpty(x.Segment)) && (string.IsNullOrEmpty(data.Name) || x.MakeName.ToLower() == data.Name.ToLower())))
                        .OrderBy(a => a.MakeName).ThenByDescending(a => a.Segment ?? string.Empty)
                        .ToList();
            }
        }

        public static AgileVehicle GetAgileVehicleByAgileVehicleId(int agileVehicleId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.AgileVehicles.FirstOrDefault(x => x.AgileVehicleId == agileVehicleId);
            }
        }

        public static HomeNetVehicle GetVehicleByVehicleId(int vehicleId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.Vehicles.FirstOrDefault(x => x.Id == vehicleId);
            }
        }

        public static bool UpdateAgileVehicle(AgileVehicle vehicle, int dealerId)
        {
            bool isUpdated;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                try
                {
                    //vehicle.DealerID = dealerId;
                    db.Entry(vehicle).State = EntityState.Modified;
                    db.SaveChanges();
                    isUpdated = true;
                }
                catch (Exception ex)
                {
                    var el = ExceptionHelper.LogException("VehicleHelper.UpdateAgileVehicle() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when update vehicle with Id: " + vehicle.AgileVehicleId);
                    EmailHelper.SendEmailForNotificationWhenFailing(el);
                    isUpdated = false;
                }
            }
            return isUpdated;
        }
        public static List<IncentiveTranslated> GetIncentiveTranslateds()
        {
            List<IncentiveTranslated> translateds = new List<IncentiveTranslated>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                translateds = db.IncentiveTranslateds.ToList();
            }
            return translateds;
        }
        public static List<Program> GetProgramOfSpecificVehicle(int agileVehicleId, int dealerId)
        {
            List<Program> programs = new List<Program>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> programIds =
                    db.AgileVehiclesPrograms.Where(x => x.AgileVehicleId == agileVehicleId).Select(a => a.ProgramId).ToList();
                if (programIds.Any())
                {
                    programs = db.Programs.Where(x => programIds.Contains(x.ProgramId)).OrderBy(o => o.Conditional).ToList();
                }
            }

            return programs;
        }
        public static List<Program> GetProgramOfByAgileVehicleIDAndProgramId(int agileVehicleId, int programId, int dealerId)
        {
            List<Program> programs = new List<Program>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<int> programIds =
                    db.AgileVehiclesPrograms.Where(x => x.AgileVehicleId == agileVehicleId && x.ProgramId == programId && x.IsActive).Select(a => a.ProgramId).ToList();
                if (programIds.Any())
                {
                    programs = db.Programs.Where(x => programIds.Contains(x.ProgramId)).OrderByDescending(o => o.Type).ToList();
                }
            }

            return programs;
        }

        public static AprTermList GetAprTermListForSpecificProgram(Program program, int dealerId)
        {
            AprTermList result = new AprTermList();
            if (program != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    result = db.AprTermList.FirstOrDefault(x => x.ProgramId == program.ProgramId);
                    if (result != null)
                    {
                        result.AprTerms = db.AprTerms.Where(x => x.AprTermListId == result.AprTermListId).OrderBy(a => a.Term).ToList();
                    }
                }
            }
            return result;
        }

        public static LeaseTermList GetLeaseTermListForSpecificProgram(Program program, int dealerId)
        {
            LeaseTermList result = new LeaseTermList();
            if (program != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    result = db.LeaseTermList.FirstOrDefault(x => x.ProgramId == program.ProgramId);
                    if (result != null)
                    {
                        result.LeaseTerms = db.LeaseTerms.Where(x => x.LeaseTermListId == result.LeaseTermListId).ToList();
                    }
                }
            }
            return result;
        }

        public static List<Program> GetProgramByTrimId(int trimId, int dealerId)
        {
            List<Program> programs;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                programs = (from trim in db.NamePlateTrims
                            join groupListJoin in db.GroupLists on trim.NamePlateTrimId equals groupListJoin.NamePlateTrimId
                                into groupLeftJoin
                            from groupList in groupLeftJoin.DefaultIfEmpty()
                            join groupJoin in db.Groups on groupList.GroupListId equals groupJoin.GroupListId into
                                groupChildLeftJoin
                            from groupChild in groupChildLeftJoin.DefaultIfEmpty()
                            join program in db.Programs on groupChild.GroupId equals program.GroupId into programLeftjoin
                            from programChild in programLeftjoin.DefaultIfEmpty()
                            where trimId == trim.NamePlateTrimId && trim.DealerID == dealerId
                            && (programChild.Start <= DateTime.Now && programChild.End >= DateTime.Now)
                            select programChild
                    ).ToList();
            }
            return programs;
        }

        public static List<Program> GetProgramByNamePlateId(int namePlateId, int dealerId)
        {
            List<Program> programs;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                programs = (from namePlate in db.NamePlates
                            join groupList in db.GroupLists on namePlate.NamePlateId equals groupList.NamePlateId
                            join groups in db.Groups on groupList.GroupListId equals groups.GroupListId
                            join program in db.Programs on groups.GroupId equals program.GroupId
                            where namePlate.NamePlateId == namePlateId && namePlate.DealerID == dealerId
                            && (program.Start <= DateTime.Now && program.End >= DateTime.Now)
                            select program
                    ).ToList();
            }
            return programs;
        }

        public static bool SetProgramState(VehicleProgramData data, int dealerId)
        {
            bool isSuccess = false;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<AgileVehiclesProgram> agileVehiclesPrograms = (from avp in db.AgileVehiclesPrograms
                                                                    join p in db.Programs on avp.ProgramId equals p.ProgramId
                                                                    where avp.AgileVehicleId == data.AgileVehicleId && p.Name == data.Name
                                                                    select avp).ToList();
                if (agileVehiclesPrograms.Any())
                {
                    agileVehiclesPrograms.ForEach(x => x.IsActive = data.IsActive == "True");
                    db.SaveChanges();
                    MapRebateForSpecificVehicle(data.AgileVehicleId, dealerId);
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        public static bool UpdateCompetitiveModel(CompetitiveModel tempModel, int dealerId)
        {
            bool isSuccess = false;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                CompetitiveModel model =
                    db.CompetitiveModels.FirstOrDefault(x => x.CompetitiveModelId == tempModel.CompetitiveModelId);
                if (model != null)
                {
                    model.Segment = tempModel.Segment;
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            return isSuccess;
        }


        public static bool ResetIncentiveTranslated(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    if (incentive.IsResetAll)
                    {
                        List<IncentiveTranslated> translateds = db.IncentiveTranslateds.Where(x =>
                            x.ProgramName.Equals(incentive.Name, StringComparisonOrdinalIgnoreCase)).ToList();
                        if (translateds.Any())
                        {
                            foreach (var translated in translateds)
                            {
                                db.Entry(translated).State = EntityState.Deleted;
                            }
                        }
                    }
                    else
                    {
                        IncentiveTranslated translated = db.IncentiveTranslateds.FirstOrDefault(x =>
                            x.ProgramName.Equals(incentive.Name, StringComparisonOrdinalIgnoreCase)
                            && x.Type.Equals(incentive.Type, StringComparisonOrdinalIgnoreCase)
                            && (incentive.UpdateFor == (int)Enum.UpdateFor.Vin &&
                                incentive.UpdateForVin.Equals(x.Vin, StringComparisonOrdinalIgnoreCase)
                                || incentive.UpdateFor == (int)Enum.UpdateFor.Model &&
                                incentive.UpdateForModelId == x.ModelId
                                || incentive.UpdateFor == (int)Enum.UpdateFor.All && x.Vin == string.Empty &&
                                x.ModelId == 0));
                        if (translated != null)
                        {
                            db.Entry(translated).State = EntityState.Deleted;
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }


        public static bool UpdateIncentiveTranslated(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    IncentiveTranslated translated = db.IncentiveTranslateds.FirstOrDefault(x =>
                        x.ProgramName.Equals(incentive.Name, StringComparisonOrdinalIgnoreCase)
                        && x.Type.Equals(incentive.Type, StringComparisonOrdinalIgnoreCase)
                        && x.Id.Equals(incentive.Id, StringComparison.OrdinalIgnoreCase)
                        && (incentive.UpdateFor == (int)Enum.UpdateFor.Vin &&
                            incentive.UpdateForVin.Equals(x.Vin, StringComparisonOrdinalIgnoreCase)
                            || incentive.UpdateFor == (int)Enum.UpdateFor.Model &&
                            incentive.UpdateForModelId == x.ModelId
                            || incentive.UpdateFor == (int)Enum.UpdateFor.All && x.Vin == string.Empty &&
                            x.ModelId == 0));
                    if (translated != null)
                    {
                        translated.DisplaySection = incentive.DisplaySection ?? 0;
                        translated.IsActive = incentive.IsActive ?? false;
                        translated.Position = incentive.Position ?? 0;
                        translated.TranslatedName = incentive.TranslatedName;
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            translated.Vin = incentive.UpdateForVin.Trim();
                            translated.ModelId = 0;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;

                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            translated.ModelId = incentive.UpdateForModelId;
                            translated.Vin = string.Empty;
                            translated.ChromeStyleId = incentive.ChromeStyleId;
                            translated.ChromeStyleIdYear = incentive.ChromeStyleIdYear;

                        }
                        else
                        {
                            translated.ModelId = 0;
                            translated.Vin = string.Empty;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;
                        }
                        translated.ModifiedDt = DateTime.Now;
                        translated.ExcludeVINs = "0";
                        db.Entry(translated).State = EntityState.Modified;
                    }
                    else
                    {
                        IncentiveTranslated newItem = new IncentiveTranslated();
                        newItem.DisplaySection = incentive.DisplaySection ?? 0;
                        newItem.IsActive = incentive.IsActive ?? false;
                        newItem.Position = incentive.Position ?? 0;
                        newItem.TranslatedName = incentive.TranslatedName;
                        newItem.ProgramName = incentive.Name;
                        newItem.Type = incentive.Type;
                        newItem.Id = incentive.Id;
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            newItem.Vin = incentive.UpdateForVin.Trim();
                            newItem.ModelId = 0;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;
                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            newItem.ModelId = incentive.UpdateForModelId;
                            newItem.Vin = string.Empty;
                            newItem.ChromeStyleId = incentive.ChromeStyleId;
                            newItem.ChromeStyleIdYear = incentive.ChromeStyleIdYear;
                        }
                        else
                        {
                            newItem.ModelId = 0;
                            newItem.Vin = string.Empty;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;
                        }
                        newItem.CreatedDt = DateTime.Now;
                        newItem.ModifiedDt = DateTime.Now;
                        newItem.ExcludeVINs = "0";
                        db.IncentiveTranslateds.Add(newItem);
                    }

                    db.SaveChanges();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
            }

            return isSuccess;
        }
        public static void MapRebateForSpecificVehicle(int agileVehicleId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<Program> programs = GetProgramOfSpecificVehicle(agileVehicleId, dealerId);
                if (programs.Any())
                {
                    AgileVehicle vehicle = db.AgileVehicles.First(x => x.AgileVehicleId == agileVehicleId);
                    if (vehicle != null)
                    {
                        decimal total = 0;
                        if (vehicle.IsNew)
                        {
                            List<string> programNames =
                                programs.Where(x => x.Type == "Cash").Select(x => x.Name).Distinct().ToList();
                            foreach (string name in programNames)
                            {
                                string price =
                                    programs.Where(x => x.Type == "Cash" && x.Name == name).Max(m => m.Amount);
                                total += decimal.Parse(price);

                            }
                            vehicle.Rebate = total;
                        }
                        else
                        {
                            vehicle.Rebate = 0;
                        }
                    }
                    db.SaveChanges();
                }
            }
        }

        //public static List<VehicleProgramData> GetVehicleProgramDatasByVehicelId(int agileVehicleId, int dealerId)
        //{
        //    List<VehicleProgramData> programDatas = new List<VehicleProgramData>();
        //    List<Program> programs = new List<Program>();

        //    using (ServicePageDbContext db = new ServicePageDbContext())
        //    {
        //        List<AgileVehiclesProgram> agileVehiclesPrograms =
        //            db.AgileVehiclesPrograms.Where(x => x.AgileVehicleId == agileVehicleId).ToList();

        //        List<int> programIds = agileVehiclesPrograms.Select(a => a.ProgramId).ToList();

        //        if (programIds.Any())
        //        {
        //            programs = db.Programs.Where(x => programIds.Contains(x.ProgramId) && (x.Start <= DateTime.Now && DateTime.Now <= x.End.Value.EndOfDay())).OrderBy(o => o.Amount).ToList();
        //            if (programs.Any())
        //            {
        //                foreach (Program program in programs)
        //                {
        //                    VehicleProgramData item = programs.Where(x => x.ProgramId == program.ProgramId).OrderByDescending(o => o.Amount).Select(a =>
        //                            new VehicleProgramData
        //                            {
        //                                ProgramId = a.ProgramId,
        //                                Name = a.Name,
        //                                Type = a.Type,
        //                                Disclaimer = a.Disclaimer,
        //                                StartDate = a.StartDate,
        //                                EndDate = a.EndDate,
        //                                Amount = a.Amount
        //                            }).FirstOrDefault();
        //                    if (item != null)
        //                    {
        //                        AgileVehiclesProgram agileVehiclesProgram =
        //                            agileVehiclesPrograms.First(x => x.AgileVehicleId == agileVehicleId && x.ProgramId == item.ProgramId);
        //                        item.IsActive = agileVehiclesProgram.IsActive ? "Yes" : "No";
        //                        item.IsDeleted = agileVehiclesProgram.IsDeleted ? "Yes" : "No";
        //                        programDatas.Add(item);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return programDatas;
        //}

        public static List<CompetitiveModel> GetCompetitiveModels(string modelName, string makeName, List<string> segments, int dealerId)
        {
            List<CompetitiveModel> competitiveModels = new List<CompetitiveModel>();
            if (segments != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var tempCompetitiveModels = db.CompetitiveModels.Where(x =>
                        (string.IsNullOrEmpty(makeName) || x.MakeName == makeName)
                         && (!segments.Any() || segments.Any(a => x.Segment.Contains(a)))).ToList();
                    if (tempCompetitiveModels != null && tempCompetitiveModels.Any())
                    {
                        if (!string.IsNullOrEmpty(modelName))
                        {
                            foreach (var item in tempCompetitiveModels)
                            {
                                if (CheckModelMatchedWithCompetitiveModel(modelName, item.Name, dealerId))
                                {
                                    competitiveModels.Add(item);
                                }
                            }
                        }
                        else
                        {
                            competitiveModels.AddRange(tempCompetitiveModels);
                        }
                    }
                }
            }
            return competitiveModels;
        }

        public static bool IsVehicleExisted(string vin, int dealerId)
        {
            bool isExist = true;
            if (!string.IsNullOrEmpty(vin))
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    isExist = db.AgileVehicles.Any(x => x.Vin.ToLower() == vin.ToLower());
                }
            }
            return isExist;
        }

        public static AgileVehicle GetAgileVehicle(string type, int makeId, int modelId, int dealerId)
        {
            AgileVehicle result = new AgileVehicle();
            using (var db = new ServicePageDbContext())
            {
                result = db.AgileVehicles.Include(y => y.Model).FirstOrDefault(
                    x =>
                    (
                        (type == "New" && x.IsNew)
                        || (type == "Certified" && x.IsCertified)
                        || (type == "Used" && (!x.IsNew && !x.IsCertified))
                    )
                    && x.Model.ModelTranslatedId == modelId
                    && x.MakeId == makeId
                );
            }
            return result;
        }

        public static HomeNetVehicle GetHomeNetVehicleByVin(string vin, int dealerId)
        {
            HomeNetVehicle result = new HomeNetVehicle();
            using (var db = new ServicePageDbContext())
            {
                result = db.Vehicles.FirstOrDefault(x => x.vin.ToLower() == vin.ToLower());
            }
            return result;
        }

        public static List<BlogEntryResult> GetBlogEntryIdsByTag(List<int> tagIds, int languageId, int dealerId, int baseModelId = 0)
        {
            List<BlogEntryResult> result = new List<BlogEntryResult>();
            if (tagIds.Any())
            {
                using (var db = new ServicePageDbContext())
                {
                    var temp = (db.BlogEntries_BlogTags.Where(bet => bet.DealerID == dealerId && tagIds.Contains(bet.BlogTagId) && (baseModelId == 0 || bet.BlogEntryId != (int)baseModelId))
                        .GroupBy(bet => bet.BlogEntryId)
                        .Select(grp => new { grp, count = grp.Count() })
                        .Where(@t => @t.count == tagIds.Count)
                        .Select(@t => new
                        {
                            @t.grp.Key
                        })).ToList();
                    List<int> listInt = temp.Select(x => x.Key).ToList();
                    if (temp.Any())
                    {
                        result = (from blog in db.BlogEntrys
                                  join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                                  on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                                  from blog_Tran in blogTranJoin.DefaultIfEmpty()
                                  where listInt.Any(y => y == blog.BlogEntryId) && blog.DealerID == dealerId
                                  select new BlogEntryResult
                                  {
                                      BlogEntryId = blog.BlogEntryId,
                                      PostTitle = blog_Tran.PostTitle ?? blog.PostTitle,
                                      PostUrl = blog_Tran.PostUrl ?? blog.PostUrl,
                                      PostSummary = blog_Tran.PostSummary ?? blog.PostSummary,
                                      PostFeatureImage = blog.PostFeatureImage,
                                      DateEnter = blog.CreatedDt
                                  }).ToList();
                    }
                }
            }
            return result;
        }

        public static List<BlogEntry> GetBlogCustomBaseModel(List<int> tagIds, int dealerId)
        {
            List<BlogEntry> result = new List<BlogEntry>();
            if (tagIds.Any())
            {
                using (var db = new ServicePageDbContext())
                {
                    result = (from b in db.BlogEntrys
                              join bt in db.BlogEntries_BlogTags on b.BlogEntryId equals bt.BlogEntryId
                              join tag in db.BlogTags on bt.BlogTagId equals tag.BlogTagId
                              where tagIds.Contains(tag.BlogTagId) && b.DealerID == dealerId
                              orderby b.DateEnter
                              select b).ToList();
                }
            }
            return result;
        }

        public static List<string> ReplaceSearchStringWithCorrectData(string searchString, int dealerId)
        {
            List<string> result = new List<string>();
            List<string> temp = new List<string>();
            string lowerSearchString = searchString.ToLower();
            List<string> listSearchString = !string.IsNullOrEmpty(lowerSearchString)
                ? lowerSearchString.Split(charSeparators,
                    StringSplitOptions.RemoveEmptyEntries).ToList<string>()
                : new List<string>();
            List<VehicleCriteriaMapping> tempList = new List<VehicleCriteriaMapping>();
            int searchStringLength = listSearchString.Count;

            if (searchStringLength > 0)
            {
                for (int i = 0; i < searchStringLength; i++)
                {
                    string tempName = listSearchString[i];
                    List<VehicleCriteriaMapping> matchedItems = VehicleCriteriaMappings.Where(
                        x => x.BadName.Equals(tempName, StringComparisonOrdinalIgnoreCase)).ToList();
                    if (matchedItems != null && matchedItems.Any())
                    {
                        for (int j = 0; j < matchedItems.Count; j++)
                        {
                            temp = matchedItems[j].GoodName.ToLower()
                                .Split(charSeparators, StringSplitOptions.RemoveEmptyEntries)
                                .ToList<string>();
                            result.AddRange(temp);
                        }

                    }
                    else
                    {
                        result.Add(listSearchString[i]);
                    }
                }
            }

            return result;
        }

        public static List<string> GuessDataFilterType(List<string> searchString, VehicleMasterDataCounter masterDataCounter, int dealerId)
        {
            List<string> result = new List<string>();
            if (searchString.Any())
            {
                if (searchString.Contains("new") || searchString.Contains("used") || searchString.Contains("certified"))
                {
                    result.Add(SearchFilterType.Type.ToString());
                }
                if (masterDataCounter.Makes.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.Make.ToString());
                }
                if (masterDataCounter.Models.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.Model.ToString());
                }
                if (masterDataCounter.Models.Any(x => x.SubList.Any() && searchString.Any(a => x.SubList.Any(s => s.SubListName.Contains(a, StringComparisonOrdinalIgnoreCase) && s.SubListName.StartsWith(a, StringComparisonOrdinalIgnoreCase) && s.SubListName.EndsWith(a, StringComparisonOrdinalIgnoreCase)))))
                {
                    result.Add(SearchFilterType.Trim.ToString());
                }
                if (masterDataCounter.Years.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.Year.ToString());
                }
                if (masterDataCounter.ExteriorColors.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.ExtColor.ToString());
                }
                if (masterDataCounter.Transmissions.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.Trans.ToString());
                }
                if (masterDataCounter.BodyStyles.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.BodyStyle.ToString());
                }
                if (masterDataCounter.BodyStyles.Any(x => x.SubList.Any() && searchString.Any(a => x.SubList.Any(s => s.SubListName.Contains(a, StringComparisonOrdinalIgnoreCase) || s.SubListName.StartsWith(a, StringComparisonOrdinalIgnoreCase) || s.SubListName.EndsWith(a, StringComparisonOrdinalIgnoreCase)))))
                {
                    result.Add(SearchFilterType.BodyStyle.ToString());
                }
                if (masterDataCounter.Engines.Any(x =>
                        searchString.Any(
                        a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase)
                        || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase)
                        || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase)
                        || (x.SubList.Any() && x.SubList.Any(s => s.SubListName.Contains(a, StringComparisonOrdinalIgnoreCase) || s.SubListName.StartsWith(a, StringComparisonOrdinalIgnoreCase) || s.SubListName.EndsWith(a, StringComparisonOrdinalIgnoreCase)))
                        )))
                {
                    result.Add(SearchFilterType.Engine.ToString());
                }
                if (masterDataCounter.FuelTypes.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.FuelType.ToString());
                }
                if (masterDataCounter.Prices.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.Price.ToString());
                }

                if ((result.Contains(SearchFilterType.Model.ToString()) || result.Contains(SearchFilterType.Make.ToString())) &&
                      result.Contains(SearchFilterType.BodyStyle.ToString()))
                {
                    result.Remove(SearchFilterType.Model.ToString());
                    result.Remove(SearchFilterType.Make.ToString());
                }
                if (masterDataCounter.VehicleFeatures.Any(x => searchString.Any(a => x.Name.Contains(a, StringComparisonOrdinalIgnoreCase) || x.Name.StartsWith(a, StringComparisonOrdinalIgnoreCase) || x.Name.EndsWith(a, StringComparisonOrdinalIgnoreCase))))
                {
                    result.Add(SearchFilterType.VehicleFeature.ToString());
                }
            }
            return result;
        }

        public static IEnumerable<VehicleData> GetVehicleDataForCounter(int _dealerID, VehicleSearchCriteria criteria, List<string> searchFilterType, List<string> listSearchString, ServicePageDbContext db)
        {
            var priceList = PriceList.Where(x => criteria.Prices.Contains(x.PriceId)).ToList();
            var milList = MileageList.Where(x => criteria.Mileages.Contains(x.MileageId)).ToList();

            List<VehicleData> SearchResultVehicleData = new List<VehicleData>();
            List<VehicleData> vsResult = new List<VehicleData>();
            List<VehicleData> vsResultVehicleFeatures = new List<VehicleData>();
            IEnumerable<VehicleData> vs = (from av in db.AgileVehicles.Where(x => x.IsSold == false && !string.IsNullOrEmpty(x.MainImageUrl) && !x.IsHidden || (criteria.CanBeEdit && x.IsHidden && !x.IsSold))
                                           join d in db.Dealers on av.DealerID equals d.DealerID
                                           join hv in db.Vehicles on av.AgileVehicleId equals hv.Id

                                           join m in db.Makes on av.MakeId equals m.MakeId into lMake
                                           from leftJoinMake in lMake.DefaultIfEmpty()

                                           join md in db.Models on av.ModelId equals md.ModelId into lModel
                                           from leftJoinModel in lModel.DefaultIfEmpty()

                                           join en in db.Engines on av.EngineId equals en.EngineId into lEngine
                                           from leftJoinEngine in lEngine.DefaultIfEmpty()

                                           join t in db.Trims on av.TrimId equals t.TrimId into lTrim
                                           from leftJoinTrim in lTrim.DefaultIfEmpty()

                                           join ec in db.ExteriorColors on av.ExteriorColorId equals ec.ExteriorColorId into lExteriorColor
                                           from leftJoinExteriorColor in lExteriorColor.DefaultIfEmpty()

                                           join s in db.BodyStyles on av.BodyStyleId equals s.BodyStyleId into lBody
                                           from leftJoinBody in lBody.DefaultIfEmpty()

                                           join bodyStyleType in db.BodyStyleTypes on leftJoinBody.BodyStyleTypeId equals bodyStyleType.BodyStyleTypeId into lBodyStyleType
                                           from leftJoinlBodyStyleType in lBodyStyleType.DefaultIfEmpty()

                                           join standardBody in db.VehicleStandardBodies on leftJoinlBodyStyleType.VehicleStandardBodyId equals standardBody.VehicleStandardBodyId into lStandardBody
                                           from leftJoinStandardBody in lStandardBody.DefaultIfEmpty()

                                           join f in db.FuelTypes on av.FuelTypeId equals f.FuelTypeId into lFuel
                                           from leftJoinFuel in lFuel.DefaultIfEmpty()

                                               //  join vf in db.VehicleFeatures on av.AgileVehicleId equals vf.AgileVehicleId into lVehicleFeatures
                                               //from leftJoinVehicleFeatures in lVehicleFeatures.DefaultIfEmpty()

                                           select new VehicleData
                                           {
                                               AgileVehicleId = av.AgileVehicleId,
                                               Vin = av.Vin,
                                               Year = av.Year,
                                               BodyStyleString = leftJoinBody.Name,
                                               BodyStyleTypeString = leftJoinlBodyStyleType.Name,
                                               StandardBodyString = leftJoinStandardBody.Name,
                                               EngineString = leftJoinEngine.Name,
                                               EngineTypeString = leftJoinEngine.EngineTypeTranslated.Name,
                                               FuelTypeString = leftJoinFuel.Name,
                                               Model = leftJoinModel.Name,
                                               ModelTranslated = leftJoinModel.ModelTranslated.Name,
                                               ModelFormated = leftJoinModel.Name.Replace(" ", "-"),
                                               ModelId = leftJoinModel.ModelId,
                                               ModelTranslatedId = leftJoinModel.ModelTranslatedId ?? 0,
                                               ExtColor = leftJoinExteriorColor.Name,
                                               ExtColorTranslatedName = leftJoinExteriorColor.ExteriorColorTranslated.Name,
                                               ExteriorColorTranslaredId = leftJoinExteriorColor.ExteriorColorTranslatedId ?? 0,
                                               EngineId = leftJoinEngine.EngineId,
                                               FuelTypeId = av.FuelTypeId ?? 0,
                                               EngineTypeTranslatedId = leftJoinEngine.EngineTypeTranslatedId ?? 0,
                                               DriveId = av.DriveId ?? 0,
                                               Make = leftJoinMake.Name,
                                               MakeTranslatedName = leftJoinMake.MakeTranslated.Name,
                                               MakeTranslatedId = leftJoinMake.MakeTranslatedId ?? 0,
                                               BodyStyleTypeId = leftJoinlBodyStyleType.BodyStyleTypeId,
                                               StandardBodyStyleId = leftJoinStandardBody.VehicleStandardBodyId,
                                               IsNew = av.IsNew,
                                               IsCert = av.IsCertified,
                                               IsLift = av.IsLift,
                                               IsUpfitted = av.IsUpfitted,
                                               IsSpecial = av.IsSpecial,
                                               IsRentalSpecial = av.IsRentalSpecial,
                                               IsHidden = av.IsHidden,
                                               TypeString = hv.certified == "True" ? "certified" : (hv.type == "New" ? "new" : "used"),
                                               Mileage = av.Miles,
                                               TrimId = av.TrimId,
                                               Trim = leftJoinTrim.Name,
                                               MSRP = av.MSRP ?? 0,
                                               Rebate = av.Rebate,
                                               Trans = hv.trans == "Manual" ? "Manual" : "Automatic",

                                               OriginalSellingPrice = (av.OverridePrice > 0
                                                                          ? (!av.HasSetDateRange ? (av.OverridePrice)
                                                                              : ((av.OverrideStartDate <= DateTime.Now && DateTime.Now <= av.OverrideEndDate)
                                                                                  ? av.OverridePrice
                                                                                  : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice) : 0))
                                                                          )
                                                                          : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice) : 0)) ?? 0,
                                               //SellingPrice = av.OverridePrice > 0
                                               //       ? (!av.HasSetDateRange ? (av.OverridePrice)
                                               //             : ((av.OverrideStartDate <= DateTime.Now && DateTime.Now <= av.OverrideEndDate)
                                               //                     ? av.OverridePrice
                                               //                     : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice + Math.Abs(av.Rebate)) : (Math.Abs(av.Rebate))))
                                               //         )
                                               //       : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice + Math.Abs(av.Rebate)) : (Math.Abs(av.Rebate))),
                                               StockNum = av.StockNumber
                                           }).AsEnumerable();
            vsResult = vs.ToList();
            vsResultVehicleFeatures = vs.ToList();
            if (listSearchString.Any())
            {
                foreach (var SearchTerm in listSearchString)
                {
                    SearchResultVehicleData.Clear();
                    if (!searchFilterType.Any())
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.Vin, StringComparisonOrdinalIgnoreCase) || SearchTerm.Contains(x.StockNum, StringComparisonOrdinalIgnoreCase) || x.Vin.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.StockNum.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Type.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.TypeString, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                        if (SearchTerm.ToLower().Equals("used"))
                        {
                            vs = vsResult.Where(x => "certified".Contains(x.TypeString, StringComparisonOrdinalIgnoreCase));
                            SearchResultVehicleData.AddRange(vs);
                        }
                    }
                    if (searchFilterType.Contains(SearchFilterType.Make.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.MakeTranslatedName, StringComparisonOrdinalIgnoreCase) || SearchTerm.Contains(x.Make, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Model.ToString()))
                    {
                        vs = vsResult.Where(x => x.Model.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ModelTranslated.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ModelFormated.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Trim.ToString()))
                    {
                        vs = vsResult.Where(x => x.Trim.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Year.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.Year.ToString(), StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.ExtColor.ToString()))
                    {
                        vs = vsResult.Where(x => x.ExtColor.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ExtColorTranslatedName.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Trans.ToString()))
                    {
                        vs = vsResult.Where(x => x.Trans.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.BodyStyle.ToString()))
                    {
                        vs = vsResult.Where(x => x.BodyStyleString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.BodyStyleTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.StandardBodyString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Engine.ToString()))
                    {
                        vs = vsResult.Where(x => x.EngineString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.EngineTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.FuelType.ToString()))
                    {
                        vs = vsResult.Where(x => x.FuelTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Price.ToString()))
                    {
                        if (priceList.Any(x => SearchTerm.Contains(x.Name, StringComparisonOrdinalIgnoreCase)))
                        {
                            vs = vsResult.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                            SearchResultVehicleData.AddRange(vs);
                        }
                    }
                    vsResult = new List<VehicleData>();
                    vsResult.AddRange(SearchResultVehicleData);

                }

                if (SearchResultVehicleData.Count > 0)
                    vs = SearchResultVehicleData.Distinct();


                if (criteria.IsNew || criteria.IsCertified || criteria.IsUsed)
                {
                    vs = vs.Where(x => (criteria.IsNew && x.IsNew) || (criteria.IsCertified && x.IsCert) || (criteria.IsUsed && (!x.IsNew && !x.IsCert)));
                }

                if (criteria.IsLift)
                {
                    vs = vs.Where(x => x.IsLift);
                }

                if (criteria.IsUpfitted)
                {
                    vs = vs.Where(x => x.IsUpfitted);
                }


                if (criteria.IsSpecial)
                {
                    vs = vs.Where(x => x.IsSpecial).AsEnumerable();
                }
                if (criteria.IsRentalSpecial)
                {
                    vs = vs.Where(x => x.IsRentalSpecial);
                }


                if (criteria.ModelTrims != null && criteria.ModelTrims.Any())
                {
                    var modelWithTrims = criteria.ModelTrims.Count(x => x.trimId != 0);
                    vs = vs.Where(x => criteria.ModelTrims.Any(a => a.modelId == x.ModelTranslatedId));
                    if (modelWithTrims > 0) //
                    {
                        vs =
                            vs.Where(
                                x =>
                                    criteria.ModelTrims.Any(
                                        a => a.modelId == x.ModelTranslatedId && a.trimId == x.TrimId));
                    }
                }

                if (criteria.VehicleBodyStandardBodyStyles != null && criteria.VehicleBodyStandardBodyStyles.Any())
                {
                    vs =
                        vs.Where(
                                x => x.StandardBodyStyleId.HasValue &&
                                criteria.VehicleBodyStandardBodyStyles.Any(a => a.standardBodyId == x.StandardBodyStyleId && (a.bodyStyleTypeId == 0 || a.bodyStyleTypeId == x.BodyStyleTypeId)));
                }


                if (criteria.Prices != null && criteria.Prices.Any())
                {
                    if (priceList.Any())
                    {
                        vs = vs.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                    }
                }

                if (criteria.Years != null && criteria.Years.Any())
                {
                    vs = vs.Where(v => criteria.Years.Contains(v.Year));
                }

                if (criteria.Mileages != null && criteria.Mileages.Any())
                {
                    if (milList.Any())
                    {
                        vs = vs.Where(x => milList.Any(y => ((y.From == 0 ? y.From <= x.Mileage : y.From < x.Mileage) && y.To >= x.Mileage)));
                    }
                }

                if (criteria.Makes != null && criteria.Makes.Any())
                {
                    vs = vs.Where(v => v.MakeTranslatedId.HasValue && criteria.Makes.Contains(v.MakeTranslatedId.Value));
                }

                if (criteria.EngineTranslatedEngines != null && criteria.EngineTranslatedEngines.Any())
                {
                    vs = vs.Where(x => x.EngineTypeTranslatedId.HasValue && criteria.EngineTranslatedEngines.Any(
                                         a => a.engineTranslatedId == x.EngineTypeTranslatedId.Value && (a.engineId == 0 || a.engineId == x.EngineId)));
                }

                if (criteria.FuelTypes != null && criteria.FuelTypes.Any())
                {
                    vs = vs.Where(x => criteria.FuelTypes.Contains(x.FuelTypeId));
                }

                if (criteria.ExteriorColors != null && criteria.ExteriorColors.Any())
                {
                    vs = vs.Where(v => criteria.ExteriorColors.Contains(v.ExteriorColorTranslaredId));
                }
                if (criteria.Transmissions != null && criteria.Transmissions.Any())
                {
                    vs = vs.Where(v => criteria.Transmissions.Contains(v.Trans));
                }


                if (criteria.Drives != null && criteria.Drives.Any())
                {
                    vs = vs.Where(x => criteria.Drives.Contains(x.DriveId));
                }

                if ((criteria.VehicleFeatures != null && criteria.VehicleFeatures.Any())
                     || (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString())))
                {
                    if (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString()))
                    {
                        if (listSearchString.Any())
                        {
                            List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                            string FinalSearchTerm = "";
                            foreach (var SearchTerm in listSearchString)
                            {
                                FinalSearchTerm += SearchTerm + " ";

                            }
                            lstVehicleFeatures = db.VehicleFeatures.ToList().Where(x => x.FeatureName.Contains(FinalSearchTerm.Trim(), StringComparison.OrdinalIgnoreCase) && x.IsSearchable).ToList();

                            if (lstVehicleFeatures != null)
                            {
                                if (vs.Count() == 0)
                                {
                                    vs = vsResultVehicleFeatures;
                                }
                                vs = vs.Where(x => lstVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                            }

                        }
                    }
                    else
                    {
                        List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                        List<VehicleFeatures> lstFinalVehicleFeatures = new List<VehicleFeatures>();
                        foreach (var item in criteria.VehicleFeatures)
                        {
                            lstVehicleFeatures.AddRange(db.VehicleFeatures.Where(x => x.MasterFeatureId.Equals(item) && x.IsSearchable));
                        }
                        if (criteria.VehicleFeatures.Count > 0)
                        {
                            foreach (var vehicleFeatureItem in lstVehicleFeatures.Select(x => x.AgileVehicleId).Distinct())
                            {
                                var AgileVehicleID = vehicleFeatureItem;
                                int FeatureCount = 0;
                                int FeatureCounter = 0;
                                foreach (var item in criteria.VehicleFeatures)
                                {
                                    FeatureCount = lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID && x.MasterFeatureId.Equals(item) && x.IsSearchable).Count();
                                    if (FeatureCount > 0)
                                    {
                                        FeatureCounter++;
                                    }
                                }
                                if (FeatureCounter == criteria.VehicleFeatures.Count())
                                {
                                    lstFinalVehicleFeatures.AddRange(lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID));
                                }
                            }
                        }
                        else
                        {
                            lstFinalVehicleFeatures.AddRange(lstVehicleFeatures);
                        }
                        if (lstFinalVehicleFeatures != null)
                        {
                            if (vs.Count() == 0)
                            {
                                vs = vsResultVehicleFeatures;
                            }
                            vs = vs.Where(x => lstFinalVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                        }
                    }

                }
            }
            else
            {
                if (criteria.IsNew || criteria.IsCertified || criteria.IsUsed)
                {
                    vs = vs.Where(x => (criteria.IsNew && x.IsNew)
                                       || (criteria.IsCertified && x.IsCert)
                                       || (criteria.IsUsed && (!x.IsNew && !x.IsCert)));
                }

                if (criteria.IsLift)
                {
                    vs = vs.Where(x => x.IsLift);
                }

                if (criteria.IsUpfitted)
                {
                    vs = vs.Where(x => x.IsUpfitted);
                }

                if (criteria.IsSpecial)
                {
                    vs = vs.Where(x => x.IsSpecial).AsEnumerable();
                }
                if (criteria.IsRentalSpecial)
                {
                    vs = vs.Where(x => x.IsRentalSpecial);
                }

                if (criteria.ModelTrims != null && criteria.ModelTrims.Any())
                {
                    var modelWithTrims = criteria.ModelTrims.Count(x => x.trimId != 0);
                    vs = vs.Where(x => criteria.ModelTrims.Any(a => a.modelId == x.ModelTranslatedId));
                    if (modelWithTrims > 0) //
                    {
                        vs =
                            vs.Where(
                                x =>
                                    criteria.ModelTrims.Any(
                                        a => a.modelId == x.ModelTranslatedId && a.trimId == x.TrimId));
                    }
                }

                if (criteria.VehicleBodyStandardBodyStyles != null && criteria.VehicleBodyStandardBodyStyles.Any())
                {
                    vs = vs.Where(x => x.StandardBodyStyleId.HasValue &&
                                       criteria.VehicleBodyStandardBodyStyles.Any(
                                           a =>
                                               a.standardBodyId == x.StandardBodyStyleId &&
                                               (a.bodyStyleTypeId == 0 || a.bodyStyleTypeId == x.BodyStyleTypeId)));
                }


                if (criteria.Prices != null && criteria.Prices.Any())
                {
                    if (priceList.Any())
                    {
                        vs = vs.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                    }
                }

                if (criteria.Years != null && criteria.Years.Any())
                {
                    vs = vs.Where(v => criteria.Years.Contains(v.Year));
                }

                if (criteria.Mileages != null && criteria.Mileages.Any())
                {
                    if (milList.Any())
                    {
                        vs = vs.Where(x => milList.Any(y => ((y.From == 0 ? y.From <= x.Mileage : y.From < x.Mileage) && y.To >= x.Mileage)));
                    }
                }

                if (criteria.Makes != null && criteria.Makes.Any())
                {
                    vs = vs.Where(v => v.MakeTranslatedId.HasValue && criteria.Makes.Contains(v.MakeTranslatedId.Value));
                }

                if (criteria.EngineTranslatedEngines != null && criteria.EngineTranslatedEngines.Any())
                {
                    vs =
                         vs.Where(
                                 x => x.EngineTypeTranslatedId.HasValue &&
                                     criteria.EngineTranslatedEngines.Any(
                                         a => a.engineTranslatedId == x.EngineTypeTranslatedId.Value && (a.engineId == 0 || a.engineId == x.EngineId)));
                }

                if (criteria.FuelTypes != null && criteria.FuelTypes.Any())
                {
                    vs = vs.Where(x => criteria.FuelTypes.Contains(x.FuelTypeId));
                }

                if (criteria.ExteriorColors != null && criteria.ExteriorColors.Any())
                {
                    vs = vs.Where(v => criteria.ExteriorColors.Contains(v.ExteriorColorTranslaredId));
                }

                if (criteria.Transmissions != null && criteria.Transmissions.Any())
                {
                    vs = vs.Where(v => criteria.Transmissions.Contains(v.Trans));
                }


                if (criteria.Drives != null && criteria.Drives.Any())
                {
                    vs = vs.Where(x => criteria.Drives.Contains(x.DriveId));
                }
                if ((criteria.VehicleFeatures != null && criteria.VehicleFeatures.Any())
                    || (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString())))
                {
                    if (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString()))
                    {
                        if (listSearchString.Any())
                        {
                            List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                            string FinalSearchTerm = "";
                            foreach (var SearchTerm in listSearchString)
                            {
                                FinalSearchTerm += SearchTerm + " ";

                            }
                            lstVehicleFeatures = db.VehicleFeatures.ToList().Where(x => x.FeatureName.Contains(FinalSearchTerm.Trim(), StringComparison.OrdinalIgnoreCase) && x.IsSearchable).ToList();

                            if (lstVehicleFeatures != null)
                            {
                                if (vs.Count() == 0)
                                {
                                    vs = vsResultVehicleFeatures;
                                }

                                vs = vs.Where(x => lstVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                            }

                        }
                    }
                    else
                    {
                        List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                        List<VehicleFeatures> lstFinalVehicleFeatures = new List<VehicleFeatures>();
                        foreach (var item in criteria.VehicleFeatures)
                        {
                            lstVehicleFeatures.AddRange(db.VehicleFeatures.Where(x => x.MasterFeatureId.Equals(item) && x.IsSearchable));
                        }
                        if (criteria.VehicleFeatures.Count > 0)
                        {
                            foreach (var vehicleFeatureItem in lstVehicleFeatures.Select(x => x.AgileVehicleId).Distinct())
                            {
                                var AgileVehicleID = vehicleFeatureItem;
                                int FeatureCount = 0;
                                int FeatureCounter = 0;
                                foreach (var item in criteria.VehicleFeatures)
                                {
                                    FeatureCount = lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID && x.MasterFeatureId.Equals(item) && x.IsSearchable).Count();
                                    if (FeatureCount > 0)
                                    {
                                        FeatureCounter++;
                                    }
                                }
                                if (FeatureCounter == criteria.VehicleFeatures.Count())
                                {
                                    lstFinalVehicleFeatures.AddRange(lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID));
                                }
                            }
                        }
                        else
                        {
                            lstFinalVehicleFeatures.AddRange(lstVehicleFeatures);
                        }
                        if (lstFinalVehicleFeatures != null)
                        {
                            if (vs.Count() == 0)
                            {
                                vs = vsResultVehicleFeatures;
                            }

                            vs = vs.Where(x => lstFinalVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                        }

                    }

                }
            }

            return vs;
        }

        public static int GetCounterForHomePage(int dealerID, string keyword, bool? isNew)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var counter = 0;
                var query = from av in db.AgileVehicles
                            join make in db.Makes on av.MakeId equals make.MakeId
                            join d in db.Dealers on av.DealerID equals d.DealerID
                            join hv in db.Vehicles on av.Vin equals hv.vin
                            join md in db.Models on av.ModelId equals md.ModelId
                            where av.IsSold == false
                                  && !string.IsNullOrEmpty(av.MainImageUrl)
                            select new
                            {
                                DealerID = av.DealerID,
                                MakeName = make.Name,
                                IsNew = av.IsNew
                            };

                counter = query.Count(c => (isNew == null || (isNew != null && c.IsNew == isNew))
                                            &&
                                            (string.IsNullOrEmpty(keyword) || (!string.IsNullOrEmpty(keyword) && c.MakeName.ToLower() == keyword.ToLower())));
                return counter;
            }
        }

        public static IEnumerable<VehicleData> GetVehicleDataForHomePage(int _dealerID)
        {
            IEnumerable<VehicleData> data = new List<VehicleData>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                data = (from av in db.AgileVehicles
                        join hv in db.Vehicles on av.AgileVehicleId equals hv.Id
                        join md in db.Models on av.ModelId equals md.ModelId
                        where av.IsSold == false
                              && !string.IsNullOrEmpty(av.MainImageUrl)
                        select new VehicleData
                        {
                            AgileVehicleId = av.AgileVehicleId,
                            Vin = av.Vin,
                            IsNew = av.IsNew,
                            IsCert = av.IsCertified,
                            ModelTranslatedId = md.ModelTranslatedId ?? 0
                        }).ToList();
            }
            return data;
        }

        public static IQueryable<VehicleData> GetVehicles(int _dealerID, VehicleSearchCriteria criteria, List<string> searchFilterType, List<string> listSearchString, ServicePageDbContext db)
        {
            var priceList = PriceList.Where(x => criteria.Prices.Contains(x.PriceId)).ToList();
            var milList = MileageList.Where(x => criteria.Mileages.Contains(x.MileageId)).ToList();

            List<VehicleData> SearchResultVehicleData = new List<VehicleData>();
            List<VehicleData> vsResult = new List<VehicleData>();
            List<VehicleData> vsResultVehicleFeatures = new List<VehicleData>();

            var vs = (from av in db.AgileVehicles.Where(x =>
                     x.IsSold == false && !string.IsNullOrEmpty(x.MainImageUrl) && !x.IsHidden ||
                     (criteria.CanBeEdit && x.IsHidden && x.IsSold == false))
                      join d in db.Dealers on av.DealerID equals d.DealerID
                      join hv in db.Vehicles on av.AgileVehicleId equals hv.Id

                      join m in db.Makes on av.MakeId equals m.MakeId into lMake
                      from leftJoinMake in lMake.DefaultIfEmpty()

                      join md in db.Models on av.ModelId equals md.ModelId into lModel
                      from leftJoinModel in lModel.DefaultIfEmpty()

                      join en in db.Engines on av.EngineId equals en.EngineId into lEngine
                      from leftJoinEngine in lEngine.DefaultIfEmpty()

                      join t in db.Trims on av.TrimId equals t.TrimId into lTrim
                      from leftJoinTrim in lTrim.DefaultIfEmpty()

                      join ec in db.ExteriorColors on av.ExteriorColorId equals ec.ExteriorColorId into lExteriorColor
                      from leftJoinExteriorColor in lExteriorColor.DefaultIfEmpty()

                      join s in db.BodyStyles on av.BodyStyleId equals s.BodyStyleId into lBody
                      from leftJoinBody in lBody.DefaultIfEmpty()

                      join bodyStyleType in db.BodyStyleTypes on leftJoinBody.BodyStyleTypeId equals bodyStyleType
                         .BodyStyleTypeId into lBodyStyleType
                      from leftJoinlBodyStyleType in lBodyStyleType.DefaultIfEmpty()

                      join standardBody in db.VehicleStandardBodies on leftJoinlBodyStyleType.VehicleStandardBodyId equals
                          standardBody.VehicleStandardBodyId into lStandardBody
                      from leftJoinStandardBody in lStandardBody.DefaultIfEmpty()

                      join f in db.FuelTypes on av.FuelTypeId equals f.FuelTypeId into lFuel
                      from leftJoinFuel in lFuel.DefaultIfEmpty()

                          //  join vf in db.VehicleFeatures on av.AgileVehicleId equals vf.AgileVehicleId into lVehicleFeatures
                          // from leftJoinVehicleFeatures in lVehicleFeatures.DefaultIfEmpty()

                      select new VehicleData
                      {
                          AgileVehicleId = av.AgileVehicleId,
                          ImageFile = !string.IsNullOrEmpty(av.MainImageUrl) ? d.ImageServer + av.MainImageUrl : hv.imagelist,
                          Vin = av.Vin,
                          Year = av.Year,
                          BodyStyleString = leftJoinBody.Name,
                          BodyStyleTypeString = leftJoinlBodyStyleType.Name,
                          StandardBodyString = leftJoinStandardBody.Name,
                          EngineString = leftJoinEngine.Name,
                          EngineTypeString = leftJoinEngine.EngineTypeTranslated.Name,
                          FuelTypeString = leftJoinFuel.Name,
                          Model = leftJoinModel.Name,
                          ModelFormated = leftJoinModel.Name.Replace(" ", "-"),
                          ModelTranslated = leftJoinModel.ModelTranslated.Name,
                          ModelId = leftJoinModel.ModelId,
                          ModelTranslatedId = leftJoinModel.ModelTranslatedId ?? 0,
                          ExteriorColorTranslaredId = leftJoinExteriorColor.ExteriorColorTranslatedId ?? 0,
                          FuelTypeId = av.FuelTypeId ?? 0,
                          EngineTypeTranslatedId = leftJoinEngine.EngineTypeTranslatedId ?? 0,
                          EngineId = av.EngineId ?? 0,
                          DriveId = av.DriveId ?? 0,
                          MakeId = av.MakeId,
                          Make = leftJoinMake.Name,
                          MakeTranslatedName = leftJoinMake.MakeTranslated.Name,
                          MakeTranslatedId = leftJoinMake.MakeTranslatedId ?? 0,
                          Trim = leftJoinTrim.Name,
                          MPGCity = av.MPG_City,
                          MPGHighway = av.MPG_Highway,
                          OverridePrice = av.OverridePrice,
                          MSRP = av.MSRPOverride ?? av.MSRP ?? 0,
                          EmployeePrice = av.EmployeePrice ?? 0,
                          HideIncentives = av.HideIncentives,
                          //Check when we should apply the OverridePrice for Vehicle
                          OriginalSellingPrice = (av.OverridePrice > 0
                                                     ? (!av.HasSetDateRange
                                                         ? (av.OverridePrice)
                                                         : ((av.OverrideStartDate <= DateTime.Now &&
                                                             DateTime.Now <= av.OverrideEndDate)
                                                             ? av.OverridePrice
                                                             : av.MSRP - (av.SellingPrice > 0
                                                                   ? (av.MSRP - av.SellingPrice)
                                                                   : 0))
                                                     )
                                                     : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice) : 0)) ??
                                                 0,
                          ExtColor = leftJoinExteriorColor.Name,
                          ExtColorTranslatedName = leftJoinExteriorColor.ExteriorColorTranslated.Name,
                          EngDescription = hv.engdescription,
                          DriveTrain = hv.drivetrain,
                          Trans = hv.trans == "Manual" ? "Manual" : "Automatic",
                          IsNew = av.IsNew,
                          IsCert = av.IsCertified,
                          IsLift = av.IsLift,
                          IsSpecial = av.IsSpecial,
                          IsRentalSpecial = av.IsRentalSpecial,
                          IsHidden = av.IsHidden,
                          TypeString = hv.certified == "True" ? "certified" : (hv.type == "New" ? "new" : "used"),
                          BodyStyleTypeId = leftJoinlBodyStyleType.BodyStyleTypeId,
                          StandardBodyStyleId = leftJoinStandardBody.VehicleStandardBodyId,
                          Mileage = av.Miles,
                          IsOverridePrice = av.OverridePrice > 0,
                          Rebate = av.Rebate,
                          TrimId = av.TrimId,
                          StockNum = av.StockNumber,
                          HasSetDateRange = av.HasSetDateRange,
                          OverrideStartDate = av.OverrideStartDate,
                          OverrideEndDate = av.OverrideEndDate,
                          IncentiveDisplayTypeId = d.IncentiveDisplayTypeId ?? (int)DealerIncentiveTypeEnum.Normal,
                          BodyStyleId = av.BodyStyleId,
                          ChromeStyleId = hv.chromestyleid,
                          ChromeStyleIdYear = av.Year,
                          IsUpfitted = av.IsUpfitted,
                          UpfittedDescription = av.UpfittedDescription,
                          UpfittedCost = av.UpfittedCost,
                          DateInStock=hv.dateinstock
                      }).AsEnumerable();

            vsResult = vs.ToList();
            vsResultVehicleFeatures = vs.ToList();
            if (listSearchString.Any())
            {
                foreach (var SearchTerm in listSearchString)
                {
                    SearchResultVehicleData.Clear();
                    if (!searchFilterType.Any())
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.Vin, StringComparisonOrdinalIgnoreCase) || SearchTerm.Contains(x.StockNum, StringComparisonOrdinalIgnoreCase) || x.Vin.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.StockNum.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Type.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.TypeString, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                        if (SearchTerm.ToLower().Equals("used"))
                        {
                            vs = vsResult.Where(x => "certified".Contains(x.TypeString, StringComparisonOrdinalIgnoreCase));
                            SearchResultVehicleData.AddRange(vs);
                        }
                    }
                    if (searchFilterType.Contains(SearchFilterType.Make.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.MakeTranslatedName, StringComparisonOrdinalIgnoreCase) || SearchTerm.Contains(x.Make, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Model.ToString()))
                    {
                        vs = vsResult.Where(x => x.Model.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ModelTranslated.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ModelFormated.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Trim.ToString()))
                    {
                        vs = vsResult.Where(x => x.Trim.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Year.ToString()))
                    {
                        vs = vsResult.Where(x => SearchTerm.Contains(x.Year.ToString(), StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.ExtColor.ToString()))
                    {
                        vs = vsResult.Where(x => x.ExtColor.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.ExtColorTranslatedName.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Trans.ToString()))
                    {
                        vs = vsResult.Where(x => x.Trans.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.BodyStyle.ToString()))
                    {
                        vs = vsResult.Where(x => x.BodyStyleString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.BodyStyleTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.StandardBodyString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Engine.ToString()))
                    {
                        vs = vsResult.Where(x => x.EngineString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase) || x.EngineTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.FuelType.ToString()))
                    {
                        vs = vsResult.Where(x => x.FuelTypeString.Contains(SearchTerm, StringComparisonOrdinalIgnoreCase));
                        SearchResultVehicleData.AddRange(vs);
                    }
                    if (searchFilterType.Contains(SearchFilterType.Price.ToString()))
                    {
                        if (priceList.Any(x => SearchTerm.Contains(x.Name, StringComparisonOrdinalIgnoreCase)))
                        {
                            vs = vsResult.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                            SearchResultVehicleData.AddRange(vs);
                        }
                    }
                    vsResult = new List<VehicleData>();
                    vsResult.AddRange(SearchResultVehicleData);

                }
                if (SearchResultVehicleData.Count > 0)
                    vs = SearchResultVehicleData.Distinct();

                if (criteria.IsNew || criteria.IsCertified || criteria.IsUsed)
                {
                    vs = vs.Where(x => (criteria.IsNew && x.IsNew) || (criteria.IsCertified && x.IsCert) || (criteria.IsUsed && (!x.IsNew && !x.IsCert)));
                }

                if (criteria.IsLift)
                {
                    vs = vs.Where(x => x.IsLift);
                }

                if (criteria.IsUpfitted)
                {
                    vs = vs.Where(x => x.IsUpfitted);
                }
                if (criteria.IsSpecial)
                {
                    vs = vs.Where(x => x.IsSpecial);
                }
                if (criteria.IsRentalSpecial)
                {
                    vs = vs.Where(x => x.IsRentalSpecial);
                }


                if (criteria.ModelTrims != null && criteria.ModelTrims.Any())
                {
                    var modelWithTrims = criteria.ModelTrims.Count(x => x.trimId != 0);
                    vs = vs.Where(x => criteria.ModelTrims.Any(a => a.modelId == x.ModelTranslatedId));
                    if (modelWithTrims > 0) //
                    {
                        vs =
                            vs.Where(
                                x =>
                                    criteria.ModelTrims.Any(
                                        a => a.modelId == x.ModelTranslatedId && a.trimId == x.TrimId));
                    }
                }

                if (criteria.VehicleBodyStandardBodyStyles != null && criteria.VehicleBodyStandardBodyStyles.Any())
                {
                    vs =
                        vs.Where(
                                x => x.StandardBodyStyleId.HasValue &&
                                criteria.VehicleBodyStandardBodyStyles.Any(a => a.standardBodyId == x.StandardBodyStyleId && (a.bodyStyleTypeId == 0 || a.bodyStyleTypeId == x.BodyStyleTypeId)));
                }


                if (criteria.Prices != null && criteria.Prices.Any())
                {
                    if (priceList.Any())
                    {
                        vs = vs.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                    }
                }

                if (criteria.Years != null && criteria.Years.Any())
                {
                    vs = vs.Where(v => criteria.Years.Contains(v.Year));
                }

                if (criteria.Mileages != null && criteria.Mileages.Any())
                {
                    if (milList.Any())
                    {
                        vs = vs.Where(x => milList.Any(y => ((y.From == 0 ? y.From <= x.Mileage : y.From < x.Mileage) && y.To >= x.Mileage)));
                    }
                }

                if (criteria.Makes != null && criteria.Makes.Any())
                {
                    vs = vs.Where(v => v.MakeTranslatedId.HasValue && criteria.Makes.Contains(v.MakeTranslatedId.Value));
                }

                if (criteria.EngineTranslatedEngines != null && criteria.EngineTranslatedEngines.Any())
                {
                    vs = vs.Where(x => x.EngineTypeTranslatedId.HasValue && criteria.EngineTranslatedEngines.Any(
                                         a => a.engineTranslatedId == x.EngineTypeTranslatedId.Value && (a.engineId == 0 || a.engineId == x.EngineId)));
                }

                if (criteria.FuelTypes != null && criteria.FuelTypes.Any())
                {
                    vs = vs.Where(x => criteria.FuelTypes.Contains(x.FuelTypeId));
                }

                if (criteria.ExteriorColors != null && criteria.ExteriorColors.Any())
                {
                    vs = vs.Where(v => criteria.ExteriorColors.Contains(v.ExteriorColorTranslaredId));
                }

                if (criteria.Transmissions != null && criteria.Transmissions.Any())
                {
                    vs = vs.Where(v => criteria.Transmissions.Contains(v.Trans));
                }

                if (criteria.Drives != null && criteria.Drives.Any())
                {
                    vs = vs.Where(x => criteria.Drives.Contains(x.DriveId));
                }
                if ((criteria.VehicleFeatures != null && criteria.VehicleFeatures.Any())
        || (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString())))
                {

                    if (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString()))
                    {
                        if (listSearchString.Any())
                        {
                            List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                            string FinalSearchTerm = "";
                            foreach (var SearchTerm in listSearchString)
                            {
                                FinalSearchTerm += SearchTerm + " ";

                            }
                            lstVehicleFeatures = db.VehicleFeatures.ToList().Where(x => x.FeatureName.Contains(FinalSearchTerm.Trim(), StringComparison.OrdinalIgnoreCase) && x.IsSearchable).ToList();

                            if (lstVehicleFeatures != null)
                            {
                                if (vs.Count() == 0)
                                {
                                    vs = vsResultVehicleFeatures;
                                }
                                vs = vs.Where(x => lstVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));

                            }

                        }
                    }
                    else
                    {
                        List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                        List<VehicleFeatures> lstFinalVehicleFeatures = new List<VehicleFeatures>();
                        foreach (var item in criteria.VehicleFeatures)
                        {
                            lstVehicleFeatures.AddRange(db.VehicleFeatures.Where(x => x.MasterFeatureId.Equals(item) && x.IsSearchable));
                        }
                        if (criteria.VehicleFeatures.Count > 0)
                        {
                            foreach (var vehicleFeatureItem in lstVehicleFeatures.Select(x => x.AgileVehicleId).Distinct())
                            {
                                var AgileVehicleID = vehicleFeatureItem;
                                int FeatureCount = 0;
                                int FeatureCounter = 0;
                                foreach (var item in criteria.VehicleFeatures)
                                {
                                    FeatureCount = lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID && x.MasterFeatureId.Equals(item) && x.IsSearchable).Count();
                                    if (FeatureCount > 0)
                                    {
                                        FeatureCounter++;
                                    }
                                }
                                if (FeatureCounter == criteria.VehicleFeatures.Count())
                                {
                                    lstFinalVehicleFeatures.AddRange(lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID));
                                }
                            }
                        }
                        else
                        {
                            lstFinalVehicleFeatures.AddRange(lstVehicleFeatures);
                        }
                        if (lstFinalVehicleFeatures != null)
                        {
                            if (vs.Count() == 0)
                            {
                                vs = vsResultVehicleFeatures;
                            }
                            vs = vs.Where(x => lstFinalVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                        }

                    }

                }

            }
            else
            {
                if (criteria.IsNew || criteria.IsCertified || criteria.IsUsed)
                {
                    vs = vs.Where(x => (criteria.IsNew && x.IsNew)
                                       || (criteria.IsCertified && x.IsCert)
                                       || (criteria.IsUsed && (!x.IsNew && !x.IsCert)));
                }

                if (criteria.IsLift)
                {
                    vs = vs.Where(x => x.IsLift);
                }
                if (criteria.IsUpfitted)
                {
                    vs = vs.Where(x => x.IsUpfitted);
                }

                if (criteria.IsSpecial)
                {
                    vs = vs.Where(x => x.IsSpecial);
                }
                if (criteria.IsRentalSpecial)
                {
                    vs = vs.Where(x => x.IsRentalSpecial);
                }

                if (criteria.ModelTrims != null && criteria.ModelTrims.Any())
                {
                    var modelWithTrims = criteria.ModelTrims.Count(x => x.trimId != 0);
                    vs = vs.Where(x => criteria.ModelTrims.Any(a => a.modelId == x.ModelTranslatedId));
                    if (modelWithTrims > 0) //
                    {
                        vs =
                            vs.Where(
                                x =>
                                    criteria.ModelTrims.Any(
                                        a => a.modelId == x.ModelTranslatedId && a.trimId == x.TrimId));
                    }
                }

                if (criteria.VehicleBodyStandardBodyStyles != null && criteria.VehicleBodyStandardBodyStyles.Any())
                {
                    vs = vs.Where(x => x.StandardBodyStyleId.HasValue &&
                                       criteria.VehicleBodyStandardBodyStyles.Any(
                                           a =>
                                               a.standardBodyId == x.StandardBodyStyleId &&
                                               (a.bodyStyleTypeId == 0 || a.bodyStyleTypeId == x.BodyStyleTypeId)));
                }


                if (criteria.Prices != null && criteria.Prices.Any())
                {
                    if (priceList.Any())
                    {
                        vs = vs.Where(x => priceList.Any(y => ((y.From == 0 ? y.From <= x.SellingPrice : y.From < x.SellingPrice) && y.To >= x.SellingPrice)));
                    }
                }

                if (criteria.Years != null && criteria.Years.Any())
                {
                    vs = vs.Where(v => criteria.Years.Contains(v.Year));
                }

                if (criteria.Mileages != null && criteria.Mileages.Any())
                {
                    if (milList.Any())
                    {
                        vs = vs.Where(x => milList.Any(y => ((y.From == 0 ? y.From <= x.Mileage : y.From < x.Mileage) && y.To >= x.Mileage)));
                    }
                }

                if (criteria.Makes != null && criteria.Makes.Any())
                {
                    vs = vs.Where(v => v.MakeTranslatedId.HasValue && criteria.Makes.Contains(v.MakeTranslatedId.Value));
                }

                if (criteria.EngineTranslatedEngines != null && criteria.EngineTranslatedEngines.Any())
                {
                    vs =
                         vs.Where(
                                 x => x.EngineTypeTranslatedId.HasValue &&
                                     criteria.EngineTranslatedEngines.Any(
                                         a => a.engineTranslatedId == x.EngineTypeTranslatedId.Value && (a.engineId == 0 || a.engineId == x.EngineId)));
                }

                if (criteria.FuelTypes != null && criteria.FuelTypes.Any())
                {
                    vs = vs.Where(x => criteria.FuelTypes.Contains(x.FuelTypeId));
                }

                if (criteria.ExteriorColors != null && criteria.ExteriorColors.Any())
                {
                    vs = vs.Where(v => criteria.ExteriorColors.Contains(v.ExteriorColorTranslaredId));
                }

                if (criteria.Transmissions != null && criteria.Transmissions.Any())
                {
                    vs = vs.Where(v => criteria.Transmissions.Contains(v.Trans));
                }

                if (criteria.Drives != null && criteria.Drives.Any())
                {
                    vs = vs.Where(x => criteria.Drives.Contains(x.DriveId));
                }
                if ((criteria.VehicleFeatures != null && criteria.VehicleFeatures.Any())
     || (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString())))
                {
                    if (searchFilterType.Contains(SearchFilterType.VehicleFeature.ToString()))
                    {
                        if (listSearchString.Any())
                        {
                            List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                            string FinalSearchTerm = "";
                            foreach (var SearchTerm in listSearchString)
                            {
                                FinalSearchTerm += SearchTerm + " ";

                            }
                            lstVehicleFeatures = db.VehicleFeatures.ToList().Where(x => x.FeatureName.Contains(FinalSearchTerm.Trim(), StringComparison.OrdinalIgnoreCase) && x.IsSearchable).ToList();

                            if (lstVehicleFeatures != null)
                            {
                                if (vs.Count() == 0)
                                {
                                    vs = vsResultVehicleFeatures;
                                }
                                vs = vs.Where(x => lstVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                            }

                        }
                    }
                    else
                    {
                        List<VehicleFeatures> lstVehicleFeatures = new List<VehicleFeatures>();
                        List<VehicleFeatures> lstFinalVehicleFeatures = new List<VehicleFeatures>();
                        foreach (var item in criteria.VehicleFeatures)
                        {
                            lstVehicleFeatures.AddRange(db.VehicleFeatures.Where(x => x.MasterFeatureId.Equals(item) && x.IsSearchable));
                        }
                        if (criteria.VehicleFeatures.Count > 0)
                        {
                            foreach (var vehicleFeatureItem in lstVehicleFeatures.Select(x => x.AgileVehicleId).Distinct())
                            {
                                var AgileVehicleID = vehicleFeatureItem;
                                int FeatureCount = 0;
                                int FeatureCounter = 0;
                                foreach (var item in criteria.VehicleFeatures)
                                {
                                    FeatureCount = lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID && x.MasterFeatureId.Equals(item) && x.IsSearchable).Count();
                                    if (FeatureCount > 0)
                                    {
                                        FeatureCounter++;
                                    }
                                }
                                if (FeatureCounter == criteria.VehicleFeatures.Count())
                                {
                                    lstFinalVehicleFeatures.AddRange(lstVehicleFeatures.Where(x => x.AgileVehicleId == AgileVehicleID));
                                }
                            }
                        }
                        else
                        {
                            lstFinalVehicleFeatures.AddRange(lstVehicleFeatures);
                        }
                        if (lstFinalVehicleFeatures != null)
                        {
                            if (vs.Count() == 0)
                            {
                                vs = vsResultVehicleFeatures;
                            }
                            vs = vs.Where(x => lstFinalVehicleFeatures.Any(y => y.AgileVehicleId == x.AgileVehicleId));
                        }
                    }

                }

            }

            switch (criteria.SortItem)
            {
                case (int)SearchControlEnum.PriceLowToHigh:
                    {

                        List<VehicleData> vsDataWithSellingPrice = vs.OrderBy(x => (x.SellingPrice - x.Rebate)).Where(x => x.SellingPrice > 0).ToList();
                        List<VehicleData> vsDataWithoutSellingPrice = vs.Where(x => x.SellingPrice == 0).ToList();

                        vsDataWithSellingPrice.AddRange(vsDataWithoutSellingPrice);

                        vs = vsDataWithSellingPrice.AsQueryable();

                    }

                    break;
                case (int)SearchControlEnum.PriceHighToLow:
                    vs = vs.OrderByDescending(x => x.SellingPrice);
                    break;
                case (int)SearchControlEnum.YearLowToHigh:
                    vs = vs.OrderBy(x => x.Year);
                    break;
                case (int)SearchControlEnum.YearHighToLow:
                    vs = vs.OrderByDescending(x => x.Year);
                    break;
                case (int)SearchControlEnum.MpgLowToHigh:
                    vs = vs.OrderBy(x => x.MPGCity).ThenBy(a => a.MPGHighway);
                    break;
                case (int)SearchControlEnum.MpgHighToLow:
                    vs = vs.OrderByDescending(x => x.MPGCity).ThenByDescending(a => a.MPGHighway);
                    break;
                case (int)SearchControlEnum.MilegeLowToHigh:
                    vs = vs.OrderBy(x => x.Mileage).ThenBy(a => a.Mileage);
                    break;
                case (int)SearchControlEnum.MilegeHighToLow:
                    vs = vs.OrderByDescending(x => x.Mileage).ThenByDescending(a => a.Mileage);
                    break;
                case (int)SearchControlEnum.StockDate:
                    vs = vs.OrderBy(x => x.DateInStock);
                    break;
            }

            return vs.AsQueryable();
        }

        public static VehicleData GetVehicle(int _dealerID, string type, int makeId, int modelId, ServicePageDbContext db)
        {
            var vs = (from av in db.AgileVehicles.OrderByDescending(x => x.Year)
                      join avi in db.AgileVehicleImages
                        on new { a = av.AgileVehicleId, b = 1 } equals new { a = avi.AgileVehicleID, b = avi.ImageOrder }
                      join m in db.Makes on av.MakeId equals m.MakeId
                      join md in db.Models on av.ModelId equals md.ModelId
                      join t in db.Trims on av.TrimId equals t.TrimId
                      join ec in db.ExteriorColors on av.ExteriorColorId equals ec.ExteriorColorId
                      join d in db.Dealers on av.DealerID equals d.DealerID
                      join hv in db.Vehicles on av.Vin equals hv.vin
                      join s in db.BodyStyles on av.BodyStyleId equals s.BodyStyleId
                      where ((type == "New" && av.IsNew)
                        || (type == "Certified" && av.IsCertified)
                        || (type == "Used" && (!av.IsNew && !av.IsCertified)))
                      && m.MakeTranslatedId == modelId
                      && av.MakeId == makeId
                      && av.IsSold == false
                      select new VehicleData
                      {
                          ImageFile = !string.IsNullOrEmpty(avi.RelativeUrl) ? d.ImageServer + avi.RelativeUrl : hv.imagelist,
                          Vin = av.Vin,
                          Year = av.Year,
                          Model = md.Name,
                          ModelId = md.ModelId,
                          ModelTranslatedId = md.ModelTranslatedId ?? 0,
                          ExteriorColorId = ec.ExteriorColorId,
                          ExteriorColorTranslaredId = ec.ExteriorColorTranslatedId ?? 0,
                          FuelTypeId = av.FuelTypeId ?? 0,
                          EngineId = av.EngineId ?? 0,
                          EngineTypeId = av.Engine.EngineTypeId ?? 0,
                          EngineTypeTranslatedId = av.Engine.EngineTypeTranslatedId ?? 0,
                          DriveId = av.DriveId ?? 0,
                          Make = m.Name,
                          Trim = t.Name,
                          MPGCity = av.MPG_City,
                          MPGHighway = av.MPG_Highway,
                          MSRP = av.MSRP,
                          //SellingPrice = av.SellingPrice,
                          ExtColor = ec.Name,
                          EngDescription = hv.engdescription,
                          DriveTrain = hv.drivetrain,
                          MakeId = m.MakeId,
                          IsNew = av.IsNew,
                          IsCert = av.IsCertified,
                          BodyStyleId = s.BodyStyleId,
                          BodyStyleTypeId = s.BodyStyleTypeId ?? 0,
                          Mileage = av.Miles,
                          IsDisplayMsrp = (av.SellingPrice < av.MSRP) ? true : false
                      }).Distinct().OrderByDescending(v => v.MSRP).FirstOrDefault();
            return vs;
        }

        public static List<VehicleData> GetAllImagesOfVehicle(string vin, int dealerId)
        {
            List<VehicleData> result = new List<VehicleData>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                result = (from av in db.AgileVehicles
                          join avi in db.AgileVehicleImages on av.AgileVehicleId equals avi.AgileVehicleID
                          join d in db.Dealers on av.DealerID equals d.DealerID
                          join hv in db.Vehicles on av.Vin equals hv.vin
                          where av.Vin == vin
                          && av.IsSold == false
                          orderby avi.ImageOrder descending
                          select new VehicleData
                          {
                              ImageFile = !string.IsNullOrEmpty(avi.RelativeUrl) ? d.ImageServer + avi.RelativeUrl : hv.imagelist,
                              ImageOrder = avi.ImageOrder
                          }).Distinct().ToList();
            }

            return result;
        }

        public static AgileVehicle GetAgileVehicleByVin(string vin, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.AgileVehicles.FirstOrDefault(x => x.Vin.ToLower() == vin.ToLower());
            }
        }

        public static VehicleData GetVehicleByVin(string vin)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var vs = (from av in db.AgileVehicles
                          join m in db.Makes on av.MakeId equals m.MakeId
                          join md in db.Models on av.ModelId equals md.ModelId
                          join t in db.Trims on av.TrimId equals t.TrimId
                          join ec in db.ExteriorColors on av.ExteriorColorId equals ec.ExteriorColorId
                          join ic in db.InteriorColors on av.InteriorColorId equals ic.InteriorColorId
                          join d in db.Dealers on av.DealerID equals d.DealerID
                          join hv in db.Vehicles on av.Vin equals hv.vin
                          join s in db.BodyStyles on av.BodyStyleId equals s.BodyStyleId
                          where av.Vin.ToLower() == vin.ToLower() && av.IsSold == false
                          select new VehicleData
                          {
                              ImageFile = !string.IsNullOrEmpty(av.MainImageUrl)
                                  ? d.ImageServer + av.MainImageUrl
                                  : hv.imagelist,
                              Vin = av.Vin,
                              AgileVehicleId = av.AgileVehicleId,
                              Year = av.Year,
                              Model = md.Name,
                              ModelTranslatedId = md.ModelTranslatedId ?? 0,
                              Make = m.Name,
                              Trim = t.Name,
                              MPGCity = av.MPG_City,
                              MPGHighway = av.MPG_Highway,
                              MSRP = av.MSRP,
                              HideIncentives = av.HideIncentives,
                              NonEditedSellingPrice = av.SellingPrice,
                              OriginalSellingPrice = (av.OverridePrice > 0
                                                         ? (!av.HasSetDateRange
                                                             ? (av.OverridePrice)
                                                             : ((av.OverrideStartDate <= DateTime.Now &&
                                                                 DateTime.Now <= av.OverrideEndDate)
                                                                 ? av.OverridePrice
                                                                 : av.MSRP - (av.SellingPrice > 0
                                                                       ? (av.MSRP - av.SellingPrice)
                                                                       : 0))
                                                         )
                                                         : av.MSRP - (av.SellingPrice > 0 ? (av.MSRP - av.SellingPrice) : 0)
                                                     ) ??
                                                     0,

                              NormalOffers =
                              (from normal in db.AgileVehiclesPrograms.Where(x => x.IsActive && x.AgileVehicleId == av.AgileVehicleId)
                               join program in db.Programs on normal.ProgramId equals program.ProgramId into leftProgram
                               from leftJoinProgram in leftProgram.DefaultIfEmpty()
                               join pTranslated in db.IncentiveTranslateds.Where(x => x.IsCashCoupon == false && x.IsActive
                                                                                      && ((x.Vin.Equals(av.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                                          || (x.Vin == "" && x.ModelId == md.ModelTranslatedId)
                                                                                          || (x.Vin == "" && x.ModelId == 0) || (x.IncludeMasterIncentiveVins.IndexOf(vin) > -1))
                                                                                      && x.DisplaySection == (int)Enum.DisplaySection.Manufacturer
                                                                                      && x.ExcludeVINs.IndexOf(vin) == -1)
                                       .OrderByDescending(x => x.Vin != string.Empty).ThenByDescending(x => x.ModelId > 0)
                                   on new
                                   {
                                       Name = leftJoinProgram.Name,
                                       Type = leftJoinProgram.Type,
                                   } equals new
                                   {
                                       Name = pTranslated.ProgramName,
                                       Type = pTranslated.Type,
                                   } into leftTranslated
                               from leftJoinTranslated in leftTranslated.DefaultIfEmpty()
                               where (leftJoinProgram != null
                                && leftJoinProgram.Conditional == "false"
                                && normal.IsActive
                                && leftJoinProgram.Type.Equals(ProgramTypeEnum.Cash.ToString(),
                                    StringComparison.OrdinalIgnoreCase))
                               || (leftJoinTranslated.DisplaySection == (int)Enum.DisplaySection.Manufacturer && leftJoinTranslated.Id == leftJoinProgram.Id)
                               select new ProgramListData()
                               {
                                   Id = leftJoinProgram.Id.ToString(),
                                   ProgramId = leftJoinProgram.ProgramId.ToString(),
                                   Name = leftJoinTranslated.TranslatedName ?? leftJoinProgram.Name,
                                   ProgramName = leftJoinProgram.Name,
                                   Amount = leftJoinProgram.Amount,
                                   Type = leftJoinProgram.Type,
                                   Disclaimer = leftJoinProgram.Disclaimer,
                                   IsCashCoupon = false
                               }).ToList(),

                              //Temporary Disabled
                              ConditionalOffers =
                              (from conditional in db.AgileVehiclesPrograms.Where(x => x.IsActive && x.AgileVehicleId == av.AgileVehicleId)
                               join program in db.Programs on conditional.ProgramId equals program.ProgramId into
                             leftProgram
                               from leftJoinProgram in leftProgram.DefaultIfEmpty()
                               join pTranslated in db.IncentiveTranslateds.Where(x => x.IsCashCoupon == false && x.IsActive
                                                                                      && ((x.Vin.Equals(av.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                                      || (x.Vin == "" && x.ModelId == md.ModelTranslatedId)
                                                                                          || (x.Vin == "" && x.ModelId == 0) || (x.IncludeMasterIncentiveVins.IndexOf(vin) > -1))
                                                                                      && x.DisplaySection == (int)Enum.DisplaySection.Conditional
                                                                                      && x.ExcludeVINs.IndexOf(vin) == -1)
                                       .OrderByDescending(x => x.Vin != string.Empty).ThenByDescending(x => x.ModelId > 0)
                                   on new
                                   {
                                       Name = leftJoinProgram.Name,
                                       Type = leftJoinProgram.Type,
                                   } equals new
                                   {
                                       Name = pTranslated.ProgramName,
                                       Type = pTranslated.Type,
                                   } into leftTranslated
                               from leftJoinTranslated in leftTranslated.DefaultIfEmpty()

                               where (leftJoinProgram != null
                                && leftJoinProgram.Conditional == "true"
                                && conditional.IsActive
                                && (leftJoinProgram.Type.Equals(ProgramTypeEnum.Cash.ToString(), StringComparison.OrdinalIgnoreCase) || leftJoinProgram.Type.Equals(ProgramTypeEnum.Certificate.ToString(), StringComparison.OrdinalIgnoreCase))
                                && (!string.IsNullOrEmpty(leftJoinProgram.IsFmcc)))
                                                                               || (leftJoinTranslated.DisplaySection ==
                                                                               (int)Enum.DisplaySection.Conditional && leftJoinTranslated.Id == leftJoinProgram.Id)
                               select new ProgramListData()
                               {
                                   Id = leftJoinProgram.Id.ToString(),
                                   ProgramId = leftJoinProgram.ProgramId.ToString(),
                                   Name = leftJoinTranslated.TranslatedName ?? leftJoinProgram.Name,
                                   ProgramName = leftJoinProgram.Name,
                                   Amount = leftJoinProgram.Amount,
                                   Type = leftJoinProgram.Type,
                                   Disclaimer = leftJoinProgram.Disclaimer,
                                   IsCashCoupon = false
                               }).OrderBy(x => x.Name.Length).ToList(),

                              AprOffers =
                                    (from apr in db.AgileVehiclesPrograms.Where(x =>
                                            x.IsActive && x.AgileVehicleId == av.AgileVehicleId)
                                     join program in db.Programs on apr.ProgramId equals program.ProgramId
                                         into leftProgram
                                     from leftJoinProgram in leftProgram.DefaultIfEmpty()
                                     join pTranslated in db.IncentiveTranslateds.Where(x => x.IsActive && ((x.Vin.Equals(av.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                                               || (x.Vin == "" && x.ModelId == md.ModelTranslatedId)
                                                                                                || (x.Vin == "" && x.ModelId == 0))
                                                                                                /*&& x.DisplaySection == (int)Enum.DisplaySection.Finance*/)
                                                                                                //.OrderByDescending(x => x.Vin != string.Empty).ThenByDescending(x => x.ModelId > 0)

                                                                                                on new
                                                                                                {
                                                                                                    Name = leftJoinProgram.Name,
                                                                                                    Type = leftJoinProgram.Type,
                                                                                                } equals new
                                                                                                {
                                                                                                    Name = pTranslated.ProgramName,
                                                                                                    Type = pTranslated.Type,
                                                                                                } into leftTranslated
                                     from leftJoinTranslated in leftTranslated.DefaultIfEmpty()
                                     where (leftJoinProgram != null
                                             && apr.IsActive
                                            && leftJoinProgram.Type.Equals(ProgramTypeEnum.APR.ToString(), StringComparison.OrdinalIgnoreCase))
                                     //|| leftJoinTranslated.DisplaySection == (int)Enum.DisplaySection.Finance
                                     select new ProgramListData()
                                     {
                                         Id = leftJoinProgram.Id.ToString(),
                                         ProgramId = leftJoinProgram.ProgramId.ToString(),
                                         Name = leftJoinTranslated.TranslatedName ?? leftJoinProgram.Name,
                                         ProgramName = leftJoinProgram.Name,
                                         Amount = leftJoinProgram.Amount,
                                         Type = leftJoinProgram.Type,
                                         Disclaimer = leftJoinProgram.Disclaimer,
                                         IsCashCoupon = false
                                     }).ToList(),


                              //DealerSaving = (av.MSRP > 0 && av.SellingPrice > 0 && av.MSRP > av.SellingPrice) ? (av.MSRP - av.SellingPrice) : 0,
                              ExtColor = ec.Name,
                              IntColor = ic.Name,
                              EngDescription = hv.engdescription,
                              DriveTrain = hv.drivetrain,
                              Trans = hv.transdescription,
                              Options = hv.options,
                              StockNum = hv.stock,
                              Convenience = hv.interiorequipment,
                              ExtEquipmt = hv.exteriorequipment,
                              Safety = hv.safetyequipment,
                              Technical = hv.mechanicalequipment,
                              DealerSave = 0,
                              Available = av.IsSold == false ? "Yes" : "No",
                              Rebate = av.Rebate,
                              Mileage = av.Miles,
                              AutowriterDescription = av.AutowriterDescription,
                              IsOverridePrice = av.OverridePrice > 0,
                              IsLift = av.IsLift,
                              IsUpfitted = av.IsUpfitted,
                              UpfittedCost = av.UpfittedCost,
                              IsHidden = av.IsHidden,
                              IsNew = av.IsNew,
                              IsCert = av.IsCertified,
                              EngineId = av.EngineId ?? 0,
                              IsSpecial = av.IsSpecial,
                              OverridePrice = av.OverridePrice,
                              OverrideDescription = av.OverrideDescription,
                              HasSetDateRange = av.HasSetDateRange,
                              OverrideStartDate = av.OverrideStartDate,
                              OverrideEndDate = av.OverrideEndDate,
                              DealerId = av.DealerID,
                              IncentiveDisplayTypeId = d.IncentiveDisplayTypeId ?? (int)DealerIncentiveTypeEnum.Normal,
                              MSRPOverride = av.MSRPOverride == null ? 0 : av.MSRPOverride,
                              EmployeePrice = av.EmployeePrice ?? 0,
                              TrimId = av.TrimId,
                              BodyStyleId = av.BodyStyleId,
                              ChromeStyleId = hv.chromestyleid,
                              ChromeStyleIdYear = av.Year,
                              UpfittedDescription = av.UpfittedDescription,
                              IsRentalSpecial = av.IsRentalSpecial

                          }).FirstOrDefault();

                var TopPriorityPrograms = db.TopPriorityPrograms.Where(x => x.Active).ToList();
                if (vs != null)
                {

                    bool IsExcludeEngineCode = false;
                    bool IsExcludeVIN = false;
                    bool IsExcludeYear = false;
                    bool IsExcludeTrim = false;
                    bool IsExcludeBodyStyle = false;


                    vs.IsDisplayMsrp =
                                          (vs.MSRP != 0) && ((vs.MSRP >= vs.OriginalSellingPrice) ? true : false);

                    if (vs.MSRPOverride > 0)
                        vs.DealerSaving = (vs.MSRPOverride > 0 && vs.OriginalSellingPrice > 0 && vs.MSRPOverride > vs.OriginalSellingPrice) ? (vs.MSRPOverride - vs.OriginalSellingPrice) : 0;
                    else
                        vs.DealerSaving = (vs.MSRP > 0 && vs.OriginalSellingPrice > 0 && vs.MSRP > vs.OriginalSellingPrice) ? (vs.MSRP - vs.OriginalSellingPrice) : 0;

                    //   vs.DealerSaving = vs.EmployeePrice > 0 ? 0 : vs.DealerSaving;

                    if (!vs.HideIncentives)
                    {
                        var ManufactrerCashTradeOffers = db.IncentiveTranslateds.Where(x => x.IsCashCoupon == true && x.IsActive
                                                                   && ((x.Vin.Equals(vs.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                   || (x.Vin == "" && x.ModelId == vs.ModelTranslatedId && (vs.ChromeStyleId == (!string.IsNullOrEmpty(x.ChromeStyleId) ? (x.ChromeStyleId) : vs.ChromeStyleId)) && (vs.Year == (x.ChromeStyleIdYear != 0 ? (x.ChromeStyleIdYear) : vs.Year)))
                                                                   || (x.Vin == "" && x.ModelId == 0) || (x.IncludeMasterIncentiveVins.IndexOf(vin) > -1))
                                                                   && x.DisplaySection == (int)Enum.DisplaySection.Manufacturer)
                                                                  .OrderByDescending(x => x.IsCashCoupon)
                                                                 .ThenByDescending(x => x.IsCustomDiscount)
                                                                 .OrderBy(x => x.Position).ToList();


                        //Add the missing programs to conditional offers.

                        var HideDealerDiscountOffers = db.IncentiveTranslateds.Where(x => x.IsCashCoupon == true && x.IsActive
                                                                                                       && ((x.Vin.Equals(vs.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                                                       || (x.Vin == "" && x.ModelId == vs.ModelTranslatedId && (vs.ChromeStyleId == (!string.IsNullOrEmpty(x.ChromeStyleId) ? (x.ChromeStyleId) : vs.ChromeStyleId)) && (vs.Year == (x.ChromeStyleIdYear != 0 ? (x.ChromeStyleIdYear) : vs.Year)))
                                                                                                       || (x.Vin == "" && x.ModelId == 0) || (x.IncludeMasterIncentiveVins.IndexOf(vin) > -1))
                                                                                                       ).OrderByDescending(x => x.IsCashCoupon)
                                                                                                      .ThenByDescending(x => x.IsCustomDiscount)
                                                                                                      .OrderBy(x => x.Position).ToList();




                        var IncentiveTransLatedDetailwithHideOtherIncentiveList = ManufactrerCashTradeOffers.Where(x => x.HideOtherIncentives && x.IsActive);
                        bool IsHideOtherIncentives = false;

                        var IncentiveTransLatedDetailwithHideOtherIncentive = new IncentiveTranslated();

                        var IncentiveTransLatedDetailwithHideDiscounts = HideDealerDiscountOffers.Where(x => x.HideDiscounts && x.IsActive);
                        //bool IsHideDiscounts = false;

                        if (IncentiveTransLatedDetailwithHideDiscounts != null)
                        {

                            foreach (var item in IncentiveTransLatedDetailwithHideDiscounts)
                            {
                                IsExcludeEngineCode = false;
                                IsExcludeVIN = false;
                                IsExcludeYear = false;
                                IsExcludeTrim = false;
                                IsExcludeBodyStyle = false;
                                if (!string.IsNullOrEmpty(item.ExcludeEngineCodes))
                                {
                                    string[] strExcludeEngineCodes = item.ExcludeEngineCodes.Split(new char[] { ',' });
                                    foreach (var engineCode in strExcludeEngineCodes)
                                    {
                                        if (engineCode.Equals(vs.EngineId.ToString()))
                                        {
                                            IsExcludeEngineCode = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.ExcludeVINs))
                                {
                                    string[] strExcludeVINs = item.ExcludeVINs.Split(new char[] { ',' });
                                    foreach (var vinItem in strExcludeVINs)
                                    {
                                        if (vinItem.Trim().Equals(vs.Vin.ToString()))
                                        {
                                            IsExcludeVIN = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.excludeTrimId))
                                {
                                    string[] strExcludeTrims = item.excludeTrimId.Split(new char[] { ',' });
                                    foreach (var trimid in strExcludeTrims)
                                    {
                                        if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                        {
                                            IsExcludeTrim = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.ExcludeYear) && Convert.ToString(item.ExcludeYear) != "0")
                                {
                                    string[] strExcludeYears = item.ExcludeYear.Split(new char[] { ',' });
                                    foreach (var vehicleYear in strExcludeYears)
                                    {
                                        if (vehicleYear.Trim().Equals(vs.Year.ToString()))
                                        {
                                            if (!string.IsNullOrEmpty(item.excludeBodyStyleId) && Convert.ToString(item.excludeBodyStyleId) != "0")
                                            {
                                                string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                                foreach (var bodystyleId in strExcludeBodyStyles)
                                                {
                                                    if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                    {
                                                        IsExcludeYear = true;
                                                        IsExcludeBodyStyle = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                                IsExcludeYear = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(item.excludeBodyStyleId))
                                    {
                                        string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                        foreach (var bodystyleId in strExcludeBodyStyles)
                                        {
                                            if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                            {
                                                IsExcludeBodyStyle = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                {

                                    vs.DealerSaving = 0;
                                    if (vs.IsDisplayMsrp)
                                    {
                                        vs.OriginalSellingPrice = Convert.ToDecimal(vs.MSRPOverride) > 0 ? Convert.ToDecimal(vs.MSRPOverride) : Convert.ToDecimal(vs.MSRP);
                                    }
                                    break;
                                }
                            }

                        }
                        //if Hide Other Incentives is setup then don't add any other programs if that vehicle falls under that category..
                        if (IncentiveTransLatedDetailwithHideOtherIncentiveList != null)
                        {
                            foreach (var HideOtherIncentiveItem in IncentiveTransLatedDetailwithHideOtherIncentiveList)
                            {
                                IsExcludeEngineCode = false;
                                IsExcludeVIN = false;
                                IsExcludeYear = false;
                                IsExcludeTrim = false;
                                IsExcludeBodyStyle = false;
                                if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeEngineCodes))
                                {
                                    string[] strExcludeEngineCodes = HideOtherIncentiveItem.ExcludeEngineCodes.Split(new char[] { ',' });
                                    foreach (var engineCode in strExcludeEngineCodes)
                                    {
                                        if (engineCode.Equals(vs.EngineId.ToString()))
                                        {
                                            IsExcludeEngineCode = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeVINs))
                                {
                                    string[] strExcludeVINs = HideOtherIncentiveItem.ExcludeVINs.Split(new char[] { ',' });
                                    foreach (var vinItem in strExcludeVINs)
                                    {
                                        if (vinItem.Trim().Equals(vs.Vin.ToString()))
                                        {
                                            IsExcludeVIN = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeTrimId))
                                {
                                    string[] strExcludeTrims = HideOtherIncentiveItem.excludeTrimId.Split(new char[] { ',' });
                                    foreach (var trimid in strExcludeTrims)
                                    {
                                        if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                        {
                                            IsExcludeTrim = true;
                                            break;
                                        }
                                    }
                                }


                                if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeYear) && Convert.ToString(HideOtherIncentiveItem.ExcludeYear) != "0")
                                {
                                    string[] strExcludeYears = HideOtherIncentiveItem.ExcludeYear.Split(new char[] { ',' });
                                    foreach (var vehicleYear in strExcludeYears)
                                    {
                                        if (vehicleYear.Trim().Equals(vs.Year.ToString()))
                                        {
                                            if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeBodyStyleId) && Convert.ToString(HideOtherIncentiveItem.excludeBodyStyleId) != "0")
                                            {
                                                string[] strExcludeBodyStyles = HideOtherIncentiveItem.excludeBodyStyleId.Split(new char[] { ',' });
                                                foreach (var bodystyleId in strExcludeBodyStyles)
                                                {
                                                    if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                    {
                                                        IsExcludeYear = true;
                                                        IsExcludeBodyStyle = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                                IsExcludeYear = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeBodyStyleId))
                                    {
                                        string[] strExcludeBodyStyles = HideOtherIncentiveItem.excludeBodyStyleId.Split(new char[] { ',' });
                                        foreach (var bodystyleId in strExcludeBodyStyles)
                                        {
                                            if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                            {
                                                IsExcludeBodyStyle = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                {
                                    IsHideOtherIncentives = true;
                                    IncentiveTransLatedDetailwithHideOtherIncentive = HideOtherIncentiveItem;
                                    break;
                                }
                            }


                        }
                        vs.HideOtherIncentives = IsHideOtherIncentives;
                        if (IsHideOtherIncentives && vs.IsDisplayMsrp)
                        {
                            var NormalProgramListToAdd = new List<ProgramListData>();

                            decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100));
                            decimal Amount = IncentiveTransLatedDetailwithHideOtherIncentive.DiscountType.Equals(1) ? Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) : Convert.ToInt32(PercentageAmount);

                            NormalProgramListToAdd.Add(new ProgramListData()
                            {
                                ProgramId = IncentiveTransLatedDetailwithHideOtherIncentive.IncentiveTranslatedId.ToString(),
                                Name = IncentiveTransLatedDetailwithHideOtherIncentive.ProgramName,
                                Amount = Convert.ToString(Amount),
                                Type = IncentiveTransLatedDetailwithHideOtherIncentive.Type,
                                Disclaimer = IncentiveTransLatedDetailwithHideOtherIncentive.Disclaimer,
                                IsCashCoupon = true,
                                IsCustomDiscount = false, // IncentiveTransLatedDetailwithHideOtherIncentive.IsCustomDiscount

                            });
                            if (vs.IsNew)
                            {
                                vs.Rebate = 1;
                            }

                            var NormalCashTradeOffers = new List<ProgramListData>();

                            NormalCashTradeOffers.AddRange(NormalProgramListToAdd);
                            vs.NormalOffers = NormalCashTradeOffers;
                            vs.ConditionalOffers = new List<ProgramListData>();
                            //if (vs.DealerSaving > 0)
                            //{
                            //    if (Convert.ToDecimal(Amount) >= vs.DealerSaving)
                            //    {
                            //        vs.DealerSaving = 0;
                            //    }
                            //    else
                            //    {
                            //        vs.DealerSaving = vs.DealerSaving - Convert.ToDecimal(Amount);
                            //    }

                            //}
                        }
                        else
                        {
                            var NormalProgramListToAdd = new List<ProgramListData>();
                            foreach (var item in ManufactrerCashTradeOffers)
                            {
                                IsExcludeEngineCode = false;
                                IsExcludeVIN = false;
                                IsExcludeYear = false;
                                IsExcludeTrim = false;
                                IsExcludeBodyStyle = false;
                                if (!string.IsNullOrEmpty(item.ExcludeEngineCodes))
                                {
                                    string[] strExcludeEngineCodes = item.ExcludeEngineCodes.Split(new char[] { ',' });
                                    foreach (var engineCode in strExcludeEngineCodes)
                                    {
                                        if (engineCode.Equals(vs.EngineId.ToString()))
                                        {
                                            IsExcludeEngineCode = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.ExcludeVINs))
                                {
                                    string[] strExcludeVINs = item.ExcludeVINs.Split(new char[] { ',' });
                                    foreach (var vinItem in strExcludeVINs)
                                    {
                                        if (vinItem.Trim().Equals(vs.Vin.ToString()))
                                        {
                                            IsExcludeVIN = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.excludeTrimId))
                                {
                                    string[] strExcludeTrims = item.excludeTrimId.Split(new char[] { ',' });
                                    foreach (var trimid in strExcludeTrims)
                                    {
                                        if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                        {
                                            IsExcludeTrim = true;
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.ExcludeYear) && Convert.ToString(item.ExcludeYear) != "0")
                                {
                                    string[] strExcludeYears = item.ExcludeYear.Split(new char[] { ',' });
                                    foreach (var vehicleYear in strExcludeYears)
                                    {
                                        if (vehicleYear.Trim().Equals(vs.Year.ToString()))
                                        {

                                            if (!string.IsNullOrEmpty(item.excludeBodyStyleId) && Convert.ToString(item.excludeBodyStyleId) != "0")
                                            {
                                                string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                                foreach (var bodystyleId in strExcludeBodyStyles)
                                                {
                                                    if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                    {
                                                        IsExcludeYear = true;
                                                        IsExcludeBodyStyle = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                                IsExcludeYear = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (item.excludeBodyStyleId != null)
                                    {
                                        string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                        foreach (var bodystyleId in strExcludeBodyStyles)
                                        {
                                            if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                            {
                                                IsExcludeBodyStyle = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (item.IsCustomDiscount)
                                {
                                    if (item.DiscountType == 2)
                                    {
                                        decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100));
                                        decimal Amount = Convert.ToInt32(PercentageAmount);

                                        if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                        {
                                            NormalProgramListToAdd.Add(new ProgramListData()
                                            {
                                                ProgramId = item.IncentiveTranslatedId.ToString(),
                                                Name = item.ProgramName,
                                                Amount = Convert.ToString(Amount),
                                                Type = item.Type,
                                                Disclaimer = item.Disclaimer,
                                                IsCashCoupon = true,
                                                IsCustomDiscount = item.IsCustomDiscount

                                            });
                                            if (vs.IsNew)
                                            {
                                                vs.Rebate = 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vs.DealerSaving > 0)
                                        {
                                            if (Convert.ToDecimal(item.Amount) >= vs.DealerSaving)
                                            {
                                                if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                                {
                                                    NormalProgramListToAdd.Add(new ProgramListData()
                                                    {
                                                        ProgramId = item.IncentiveTranslatedId.ToString(),
                                                        Name = item.ProgramName,
                                                        Amount = vs.DealerSaving.ToString(),
                                                        Type = item.Type,
                                                        Disclaimer = item.Disclaimer,
                                                        IsCashCoupon = true,
                                                        IsCustomDiscount = item.IsCustomDiscount

                                                    });
                                                    if (vs.IsNew)
                                                    {
                                                        vs.Rebate = 1;
                                                    }
                                                    vs.DealerSaving = 0;
                                                }

                                            }
                                            else
                                            {
                                                if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                                {
                                                    NormalProgramListToAdd.Add(new ProgramListData()
                                                    {
                                                        ProgramId = item.IncentiveTranslatedId.ToString(),
                                                        Name = item.ProgramName,
                                                        Amount = item.Amount,
                                                        Type = item.Type,
                                                        Disclaimer = item.Disclaimer,
                                                        IsCashCoupon = true,
                                                        IsCustomDiscount = item.IsCustomDiscount

                                                    });
                                                    if (vs.IsNew)
                                                    {
                                                        vs.Rebate = 1;
                                                    }
                                                    vs.DealerSaving = vs.DealerSaving - Convert.ToDecimal(item.Amount);
                                                }

                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                    {
                                        if (vs.IsNew)
                                        {
                                            NormalProgramListToAdd.Add(new ProgramListData()
                                            {
                                                ProgramId = item.IncentiveTranslatedId.ToString(),
                                                Name = item.ProgramName,
                                                Amount = item.Amount,
                                                Type = item.Type,
                                                Disclaimer = item.Disclaimer,
                                                IsCashCoupon = true,
                                                IsCustomDiscount = item.IsCustomDiscount

                                            });

                                            vs.Rebate = 1;
                                        }
                                    }
                                }
                            }

                            var NormalCashTradeOffers = new List<ProgramListData>();
                            NormalCashTradeOffers = vs.NormalOffers.ToList();
                            NormalCashTradeOffers.AddRange(NormalProgramListToAdd);
                            vs.NormalOffers = NormalCashTradeOffers;

                            //Get All the Programs of Vehicle
                            var VehicleAllPrograms = (from conditional in db.AgileVehiclesPrograms.Where(x => x.IsActive && x.AgileVehicleId == vs.AgileVehicleId)
                                                      join program in db.Programs on conditional.ProgramId equals program.ProgramId into leftProgram
                                                      from leftJoinProgram in leftProgram.DefaultIfEmpty()
                                                      where leftJoinProgram != null
                                                         && leftJoinProgram.Conditional == "true"
                                                         && conditional.IsActive
                                                      select new ProgramListData()
                                                      {
                                                          Id = leftJoinProgram.Id.ToString(),
                                                          ProgramId = leftJoinProgram.ProgramId.ToString(),
                                                          Name = leftJoinProgram.Name,
                                                          Amount = leftJoinProgram.Amount,
                                                          Type = leftJoinProgram.Type,
                                                          Disclaimer = leftJoinProgram.Disclaimer
                                                      }).OrderBy(x => x.Name.Length).ToList();

                            var ProgramListToAdd = new List<ProgramListData>();
                            //Get the missing TopPriorityPrograms which didn't match for ConditionalOffers
                            foreach (var topProrityProgramItem in TopPriorityPrograms)
                            {
                                if (vs.ConditionalOffers.Count > 0)
                                {
                                    foreach (var conditionalProgram in vs.ConditionalOffers)
                                    {
                                        if (!conditionalProgram.Name.Equals(topProrityProgramItem.Name, StringComparison.OrdinalIgnoreCase) && !conditionalProgram.Name.Equals(topProrityProgramItem.Description, StringComparison.OrdinalIgnoreCase) && !ProgramListToAdd.Any(x => x.Name == topProrityProgramItem.Name))
                                        {
                                            var MissingProgram = VehicleAllPrograms.Where(x => x.Name.Equals(topProrityProgramItem.Name)).FirstOrDefault();
                                            if (MissingProgram != null)
                                            {
                                                ProgramListToAdd.Add(MissingProgram);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var MissingProgram = VehicleAllPrograms.Where(x => x.Name.Equals(topProrityProgramItem.Name)).FirstOrDefault();
                                    if (MissingProgram != null)
                                    {
                                        ProgramListToAdd.Add(MissingProgram);
                                    }
                                }
                            }
                            VehicleAllPrograms = null;

                            //Add the missing programs to conditional offers.
                            var ConditionalOffers = new List<ProgramListData>();
                            ConditionalOffers = vs.ConditionalOffers.ToList();

                            vs.NormalOffers.RemoveAll(a => ConditionalOffers.Exists(w => w.Id == a.Id));

                            var ConditionalCashTradeOffers = db.IncentiveTranslateds.Where(x => x.IsCashCoupon == true && x.IsActive
                                                                          && ((x.Vin.Equals(vs.Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                                          || (x.Vin == "" && x.ModelId == vs.ModelTranslatedId && (vs.ChromeStyleId == (!string.IsNullOrEmpty(x.ChromeStyleId) ? (x.ChromeStyleId) : vs.ChromeStyleId)) && (vs.Year == (x.ChromeStyleIdYear != 0 ? (x.ChromeStyleIdYear) : vs.Year)))
                                                                          || (x.Vin == "" && x.ModelId == 0) || (x.IncludeMasterIncentiveVins.IndexOf(vin) > -1))
                                                                          && x.DisplaySection == (int)Enum.DisplaySection.Conditional)
                                                                         .OrderByDescending(x => x.IsCashCoupon)
                                                                        .ThenByDescending(x => x.IsCustomDiscount)
                                                                        .OrderBy(x => x.Position).ToList();


                            IncentiveTransLatedDetailwithHideOtherIncentiveList = ConditionalCashTradeOffers.Where(x => x.HideOtherIncentives && x.IsActive);
                            IsHideOtherIncentives = false;

                            if (IncentiveTransLatedDetailwithHideOtherIncentiveList != null)
                            {
                                foreach (var HideOtherIncentiveItem in IncentiveTransLatedDetailwithHideOtherIncentiveList)
                                {
                                    IsExcludeEngineCode = false;
                                    IsExcludeVIN = false;
                                    IsExcludeYear = false;
                                    IsExcludeTrim = false;
                                    IsExcludeBodyStyle = false;
                                    if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeEngineCodes))
                                    {
                                        string[] strExcludeEngineCodes = HideOtherIncentiveItem.ExcludeEngineCodes.Split(new char[] { ',' });
                                        foreach (var engineCode in strExcludeEngineCodes)
                                        {
                                            if (engineCode.Equals(vs.EngineId.ToString()))
                                            {
                                                IsExcludeEngineCode = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeVINs))
                                    {
                                        string[] strExcludeVINs = HideOtherIncentiveItem.ExcludeVINs.Split(new char[] { ',' });
                                        foreach (var vinItem in strExcludeVINs)
                                        {
                                            if (vinItem.Trim().Equals(vs.Vin.ToString()))
                                            {
                                                IsExcludeVIN = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeTrimId))
                                    {
                                        string[] strExcludeTrims = HideOtherIncentiveItem.excludeTrimId.Split(new char[] { ',' });
                                        foreach (var trimid in strExcludeTrims)
                                        {
                                            if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                            {
                                                IsExcludeTrim = true;
                                                break;
                                            }
                                        }
                                    }


                                    if (!string.IsNullOrEmpty(HideOtherIncentiveItem.ExcludeYear))
                                    {
                                        string[] strExcludeYears = HideOtherIncentiveItem.ExcludeYear.Split(new char[] { ',' });
                                        foreach (var vehicleYear in strExcludeYears)
                                        {
                                            if (vehicleYear.Trim().Equals(vs.Year.ToString()))
                                            {
                                                if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeBodyStyleId))
                                                {
                                                    string[] strExcludeBodyStyles = HideOtherIncentiveItem.excludeBodyStyleId.Split(new char[] { ',' });
                                                    foreach (var bodystyleId in strExcludeBodyStyles)
                                                    {
                                                        if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                        {
                                                            IsExcludeYear = true;
                                                            IsExcludeBodyStyle = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                    IsExcludeYear = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(HideOtherIncentiveItem.excludeBodyStyleId))
                                        {
                                            string[] strExcludeBodyStyles = HideOtherIncentiveItem.excludeBodyStyleId.Split(new char[] { ',' });
                                            foreach (var bodystyleId in strExcludeBodyStyles)
                                            {
                                                if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                {
                                                    IsExcludeBodyStyle = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                    {
                                        IsHideOtherIncentives = true;
                                        IncentiveTransLatedDetailwithHideOtherIncentive = HideOtherIncentiveItem;
                                        break;
                                    }
                                }
                            }
                            vs.HideOtherIncentives = IsHideOtherIncentives;
                            if (IsHideOtherIncentives && vs.IsDisplayMsrp)
                            {
                                ConditionalOffers = new List<ProgramListData>();

                                decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) / 100));
                                decimal Amount = IncentiveTransLatedDetailwithHideOtherIncentive.DiscountType.Equals(1) ? Convert.ToDecimal(IncentiveTransLatedDetailwithHideOtherIncentive.Amount) : Convert.ToInt32(PercentageAmount);
                                if (vs.IsNew)
                                {
                                    ProgramListToAdd.Add(new ProgramListData()
                                    {
                                        ProgramId = IncentiveTransLatedDetailwithHideOtherIncentive.IncentiveTranslatedId.ToString(),
                                        Name = IncentiveTransLatedDetailwithHideOtherIncentive.ProgramName,
                                        Amount = Convert.ToString(Amount),
                                        Type = IncentiveTransLatedDetailwithHideOtherIncentive.Type,
                                        Disclaimer = IncentiveTransLatedDetailwithHideOtherIncentive.Disclaimer,
                                        IsCashCoupon = true,
                                        IsCustomDiscount = false, // IncentiveTransLatedDetailwithHideOtherIncentive.IsCustomDiscount

                                    });

                                    vs.Rebate = 1;
                                }

                                ConditionalOffers.AddRange(ProgramListToAdd);

                                vs.ConditionalOffers = ConditionalOffers;
                                //if (vs.DealerSaving > 0)
                                //{
                                //    if (Convert.ToDecimal(Amount) >= vs.DealerSaving)
                                //    {
                                //        vs.DealerSaving = 0;
                                //    }
                                //    else
                                //    {
                                //        vs.DealerSaving = vs.DealerSaving - Convert.ToDecimal(Amount);
                                //    }

                                //}
                            }
                            else
                            {
                                foreach (var item in ConditionalCashTradeOffers)
                                {
                                    IsExcludeEngineCode = false;
                                    IsExcludeVIN = false;
                                    IsExcludeTrim = false;
                                    IsExcludeYear = false;
                                    IsExcludeBodyStyle = false;
                                    if (item.ExcludeEngineCodes != null)
                                    {
                                        string[] strExcludeEngineCodes = item.ExcludeEngineCodes.Split(new char[] { ',' });
                                        foreach (var engineCode in strExcludeEngineCodes)
                                        {
                                            if (engineCode.Equals(vs.EngineId.ToString()))
                                            {
                                                IsExcludeEngineCode = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (item.ExcludeVINs != null)
                                    {
                                        string[] strExcludeVINs = item.ExcludeVINs.Split(new char[] { ',' });
                                        foreach (var vinItem in strExcludeVINs)
                                        {

                                            if (vinItem.Trim().Equals(vs.Vin.ToString()))
                                            {
                                                IsExcludeVIN = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (item.excludeTrimId != null)
                                    {
                                        string[] strExcludeTrims = item.excludeTrimId.Split(new char[] { ',' });
                                        foreach (var trimid in strExcludeTrims)
                                        {
                                            if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                            {
                                                IsExcludeTrim = true;
                                                break;
                                            }
                                        }
                                    }



                                    if (item.excludeTrimId != null)
                                    {
                                        string[] strExcludeTrims = item.excludeTrimId.Split(new char[] { ',' });
                                        foreach (var trimid in strExcludeTrims)
                                        {
                                            if (trimid.Trim().Equals(vs.TrimId.ToString()))
                                            {
                                                IsExcludeTrim = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(item.ExcludeYear) && Convert.ToString(item.ExcludeYear) != "0")
                                    {
                                        string[] strExcludeYears = item.ExcludeYear.Split(new char[] { ',' });
                                        foreach (var vehicleYear in strExcludeYears)
                                        {
                                            if (vehicleYear.Trim().Equals(vs.Year.ToString()))
                                            {
                                                if (!string.IsNullOrEmpty(item.excludeBodyStyleId) && Convert.ToString(item.excludeBodyStyleId) != "0")
                                                {
                                                    string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                                    foreach (var bodystyleId in strExcludeBodyStyles)
                                                    {
                                                        if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                        {
                                                            IsExcludeYear = true;
                                                            IsExcludeBodyStyle = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                    IsExcludeYear = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.excludeBodyStyleId))
                                        {
                                            string[] strExcludeBodyStyles = item.excludeBodyStyleId.Split(new char[] { ',' });
                                            foreach (var bodystyleId in strExcludeBodyStyles)
                                            {
                                                if (bodystyleId.Trim().Equals(vs.BodyStyleId.ToString()))
                                                {
                                                    IsExcludeBodyStyle = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeTrim && !IsExcludeYear && !IsExcludeBodyStyle)
                                    {
                                        if (item.DiscountType == 2)
                                        {
                                            decimal PercentageAmount = Convert.ToString((Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100))).IndexOf(".50") > -1 ? (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100) + Convert.ToDecimal(0.05)) : (Convert.ToDecimal(vs.MSRP) * (Convert.ToDecimal(item.Amount) / 100));
                                            decimal Amount = Convert.ToInt32(PercentageAmount);


                                            if (vs.IsNew)
                                            {
                                                ProgramListToAdd.Add(new ProgramListData()
                                                {
                                                    ProgramId = item.IncentiveTranslatedId.ToString(),
                                                    Name = item.ProgramName,
                                                    Amount = Convert.ToString(Amount),
                                                    Type = item.Type,
                                                    Disclaimer = item.Disclaimer,
                                                    IsCashCoupon = true,
                                                    IsCustomDiscount = item.IsCustomDiscount

                                                });

                                                vs.Rebate = 1;
                                            }

                                        }
                                        else if (!IsExcludeEngineCode && !IsExcludeVIN && !IsExcludeYear && !IsExcludeTrim && !IsExcludeBodyStyle)
                                        {
                                            if (vs.IsNew)
                                            {
                                                ProgramListToAdd.Add(new ProgramListData()
                                                {
                                                    ProgramId = item.IncentiveTranslatedId.ToString(),
                                                    Name = item.ProgramName,
                                                    Amount = item.Amount,
                                                    Type = item.Type,
                                                    Disclaimer = item.Disclaimer,
                                                    IsCashCoupon = true,
                                                    IsCustomDiscount = item.IsCustomDiscount

                                                });

                                                vs.Rebate = 1;
                                            }
                                        }


                                    }
                                }

                                ConditionalOffers.AddRange(ProgramListToAdd);
                                vs.ConditionalOffers = ConditionalOffers;

                                if (vs.EmployeePrice > 0)
                                {
                                    vs.OriginalSellingPrice = Convert.ToDecimal(vs.EmployeePrice);
                                    vs.DealerSaving = 0;
                                }
                            }
                        }

                    }
                    else
                    {
                        vs.NormalOffers = new List<ProgramListData>();
                        vs.AprOffers = new List<ProgramListData>();
                        vs.ConditionalOffers = new List<ProgramListData>();
                    }
                }


                return vs;
            }
        }

        public static List<FordVehicleContent> GetFordVehicleContents(int dealerId)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                return db.FordVehicleContents.ToList();
            }
        }

        public static string GenerateVcpLinkForSoldVehicle(string vin, int dealerId)
        {
            string url = string.Empty;
            if (!string.IsNullOrEmpty(vin))
            {
                HomeNetVehicle tempVehicle = GetHomeNetVehicleByVin(vin, dealerId);
                if (tempVehicle != null)
                {
                    List<FordVehicleContent> fordVehicleContents = GetFordVehicleContents(dealerId);
                    foreach (FordVehicleContent item in fordVehicleContents)
                    {
                        if (item.Type.ToLower() == tempVehicle.type.ToLower() &&
                            CheckModelMatchedForVehicle(item.Model, tempVehicle, dealerId))
                        {
                            url = $"/vcp/{item.Type.ToLower()}/{tempVehicle.make.ToLower()}/{item.Model.ToLower()}";
                            return url;
                        }
                    }
                }
            }

            return url;
        }

        private static bool CheckModelMatchedWithCompetitiveModel(string modelName, string competitiveModelName, int dealerId)
        {
            bool result = false;
            char[] charSeparators = { ' ' };

            List<string> nameList = modelName.ToLowerInvariant().Split(charSeparators).ToList();

            List<string> modelListString = !string.IsNullOrEmpty(competitiveModelName) ?
                competitiveModelName.ToLowerInvariant().Split(charSeparators,
                    StringSplitOptions.RemoveEmptyEntries).ToList<string>() : new List<string>();

            List<string> matchedStringOfModel = CheckMatchedByIntersectForModel(nameList, modelListString, dealerId);
            bool isMatchedModel = matchedStringOfModel.Any();

            result = isMatchedModel;
            return result;
        }

        private static bool CheckModelMatchedForVehicle(string modelName, HomeNetVehicle homenetVehicle, int dealerId)
        {
            bool result = false;
            char[] charSeparators = { ' ' };

            List<string> nameList = modelName.ToLowerInvariant().Split(charSeparators).ToList();

            List<string> modelListString = !string.IsNullOrEmpty(homenetVehicle.model) ?
                homenetVehicle.model.ToLowerInvariant().Split(charSeparators,
                    StringSplitOptions.RemoveEmptyEntries).ToList<string>() : new List<string>();

            List<string> matchedStringOfModel = CheckMatchedByIntersectForModel(nameList, modelListString, dealerId);
            bool isMatchedModel = matchedStringOfModel.Any();

            result = isMatchedModel;
            return result;
        }

        private static List<string> CheckMatchedByIntersectForModel(List<string> nameList, List<string> modelListString, int dealerId)
        {
            List<string> result = new List<string>();
            var temp = nameList.Intersect(modelListString);
            var enumerable = temp as string[] ?? temp.ToArray();
            if (enumerable.Any() && enumerable.Length >= nameList.Count)
            {
                result.AddRange(enumerable);
            }

            return result;
        }

        public static int GetDealerIncentiveType(int dealerId)
        {
            int incentiveType = (int)DealerIncentiveTypeEnum.Normal;
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var dealer = db.Dealers.FirstOrDefault(x => x.DealerID == dealerId);
                if (dealer != null)
                {
                    if (dealer.IncentiveDisplayTypeId.HasValue && dealer.IncentiveDisplayTypeId.Value > 0)
                    {
                        incentiveType = dealer.IncentiveDisplayTypeId.Value;
                    }
                }
            }

            return incentiveType;
        }

        public static List<IncentiveTranslatedModel> GetIncentiveTranslatedData(bool IsSpecialProgram)
        {
            List<IncentiveTranslatedModel> result = new List<IncentiveTranslatedModel>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var models = (from model in db.ModelTranslateds
                              select new { ModelTranslatedId = model.ModelTranslatedId, Name = model.Name })
                    .ToList();
                string tempName = string.Empty;
                string formatedTranslatedName = string.Empty;
                string shownData = string.Empty;
                string type = string.Empty;
                string currentName = string.Empty;


                if (!IsSpecialProgram)
                {
                    var translatedSource = db.IncentiveTranslateds.ToList().Where(x => !x.IsCashCoupon);
                    var sourcePrograms = (from program in db.Programs
                                          select new
                                          {
                                              Name = program.Name,
                                              Id = program.Id,
                                              Type = program.Type,
                                              IsTargeted = program.IsTargeted,
                                              Conditional = program.Conditional,
                                          }).Distinct();

                    foreach (var program in sourcePrograms)
                    {

                        var newItem = new IncentiveTranslatedModel()
                        {
                            Name = program.Name,
                            Type = program.Type,
                            Id = program.Id,
                            //Position = program.Position,
                            //DisplaySection = program.DisplaySection,
                            TranslatedName = string.Empty,
                            //IsActive = program.IsActive,
                        };

                        string defaultSection = string.Empty;

                        if (program.Conditional == "false" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Manufacturer.ToString();
                        }
                        else if (program.Conditional == "true" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Conditional.ToString();
                        }
                        else if (program.Type.Equals(ProgramTypeEnum.APR.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Finance.ToString();
                        }

                        List<IncentiveTranslated> translateds = translatedSource.Where(x =>
                            x.ProgramName.Equals(program.Name, StringComparisonOrdinalIgnoreCase)
                            && x.Type.Equals(program.Type, StringComparisonOrdinalIgnoreCase)).OrderByDescending(x => x.Vin != string.Empty).ThenBy(x => x.ModelId > 0).ToList();

                        tempName = string.Empty;
                        formatedTranslatedName = string.Empty;
                        shownData = string.Empty;
                        type = string.Empty;
                        currentName = string.Empty;

                        if (translateds.Any())
                        {
                            newItem.DisplaySection = 0;


                            foreach (var translated in translateds)
                            {
                                string currentType = string.Empty;
                                string status = string.Empty;
                                string className = string.Empty;
                                string name = string.Empty;
                                if (translated.DisplaySection > 0)
                                {
                                    Enum.DisplaySection section = (Enum.DisplaySection)translated.DisplaySection;
                                    name = EnumHelper.GetDescriptionFromEnumValue(section);
                                }

                                if (!string.IsNullOrEmpty(name) && !tempName.Contains(name, StringComparisonOrdinalIgnoreCase))
                                {
                                    tempName += name + BreakNewLine;
                                }

                                if (translated.IsActive)
                                {
                                    className = "itemActive";
                                    status = "active";
                                }
                                else
                                {
                                    className = "itemHidden";
                                    status = "hidden";
                                }

                                if (!string.IsNullOrEmpty(translated.TranslatedName))
                                {
                                    if (!string.IsNullOrEmpty(translated.Vin))
                                    {
                                        currentType = Enum.UpdateFor.Vin.ToString();
                                        shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='vinSection' vin='{translated.Vin}'>({translated.Vin})</span>";
                                    }
                                    else if (translated.ModelId > 0)
                                    {
                                        currentType = Enum.UpdateFor.Model.ToString();
                                        var model = models.FirstOrDefault(x => x.ModelTranslatedId == translated.ModelId);
                                        shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='modelSection' modelid='{translated.ModelId}' >({(model != null ? model.Name : string.Empty)})</span>";
                                    }
                                    else
                                    {
                                        currentType = Enum.UpdateFor.All.ToString();
                                        shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >(All Vehicles)</span>";
                                    }

                                }

                                if (!currentType.Equals(type, StringComparisonOrdinalIgnoreCase))
                                {
                                    if (type == string.Empty)// First item
                                    {
                                        formatedTranslatedName +=
                                            $@"{translated.TranslatedName} - {shownData}";
                                    }
                                    else
                                    {
                                        formatedTranslatedName +=
                                            $@"{BreakNewLine}{BreakNewLine}{translated.TranslatedName} - {shownData}";
                                    }

                                    shownData = string.Empty;
                                }
                                else
                                {
                                    if (currentName.Equals(translated.TranslatedName, StringComparisonOrdinalIgnoreCase))
                                    {
                                        formatedTranslatedName += $@" - {shownData}";
                                    }
                                    else
                                    {
                                        formatedTranslatedName += $@"{BreakNewLine}{BreakNewLine}{translated.TranslatedName} - {shownData}";
                                    }
                                    shownData = string.Empty;
                                }

                                type = currentType;
                                currentName = translated.TranslatedName;
                            }

                            if (!tempName.Contains(defaultSection, StringComparisonOrdinalIgnoreCase))
                            {
                                tempName += defaultSection + BreakNewLine;
                            }
                            newItem.DisplaySectionGroupName = tempName;
                            newItem.TranslatedName = formatedTranslatedName;

                        }
                        else
                        {
                            if (program.Conditional == "false" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Manufacturer;
                            }
                            else if (program.Conditional == "true" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Conditional;
                            }
                            else if (program.Type.Equals(ProgramTypeEnum.APR.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Finance;
                            }
                            newItem.TranslatedName = string.Empty;
                            newItem.IsActive = false;
                            newItem.IsActiveString = "Hidden";
                            newItem.DisplaySectionGroupName = defaultSection;



                        }


                        newItem.Amount = string.Join(",", (from p in db.Programs
                                                           where program.Name == p.Name && program.Type == p.Type
                                                           select p.Amount).Distinct());

                        newItem.IsCashCoupon = false;
                        newItem.IsMasterIncentive = false;
                        result.Add(newItem);
                    }
                }
                else
                {
                    //Get all the Cash or Trade items
                    var translatedSource = db.IncentiveTranslateds.ToList().Where(x => x.IsCashCoupon);

                    foreach (var translated in translatedSource)
                    {
                        StringBuilder sbExcludeEngingCodeUserFriendlyName = new StringBuilder();
                        if (translated.ExcludeEngineCodes != null)
                        {
                            foreach (var item in translated.ExcludeEngineCodes.Split(new char[] { ',' }))
                            {
                                int EngineId = Convert.ToInt32(item);
                                var EngineData = db.Engines.Where(x => x.EngineId == EngineId).FirstOrDefault();
                                if (EngineData != null)
                                    sbExcludeEngingCodeUserFriendlyName.Append(EngineData.Name + ",");
                            }
                        }

                        var newItem = new IncentiveTranslatedModel()
                        {
                            DisplaySection = translated.DisplaySection,
                            Name = translated.ProgramName,
                            Type = translated.Type,
                            //Position = program.Position,
                            Disclaimer = translated.Disclaimer,
                            TranslatedName = translated.TranslatedName,
                            IsCashCoupon = translated.IsCashCoupon,
                            Amount = translated.Amount,
                            ExcludeEngingCodeUserFriendlyName = sbExcludeEngingCodeUserFriendlyName.ToString().TrimEnd(','),
                            Id = Convert.ToString(translated.IncentiveTranslatedId),
                            excludeenginecodesData = translated.ExcludeEngineCodes == null ? null : translated.ExcludeEngineCodes,
                            ExcludeVINs = translated.ExcludeVINs == "0" ? null : translated.ExcludeVINs


                            //IsActive = program.IsActive,
                        };

                        string currentType = string.Empty;
                        string status = string.Empty;
                        string className = string.Empty;
                        string name = string.Empty;
                        tempName = string.Empty;
                        shownData = string.Empty;
                        formatedTranslatedName = string.Empty;
                        if (translated.DisplaySection > 0)
                        {
                            Enum.DisplaySection section = (Enum.DisplaySection)translated.DisplaySection;
                            name = EnumHelper.GetDescriptionFromEnumValue(section);
                        }

                        if (!string.IsNullOrEmpty(name) && !tempName.Contains(name, StringComparisonOrdinalIgnoreCase))
                        {
                            tempName += name + BreakNewLine;
                        }

                        if (translated.IsActive)
                        {
                            className = "itemActive";
                            status = "active";
                        }
                        else
                        {
                            className = "itemHidden";
                            status = "hidden";
                        }

                        if (!string.IsNullOrEmpty(translated.TranslatedName))
                        {
                            if (!string.IsNullOrEmpty(translated.Vin))
                            {
                                currentType = Enum.UpdateFor.Vin.ToString();
                                shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='vinSection' vin='{translated.Vin}'>({translated.Vin})</span>";
                            }
                            else if (translated.ModelId > 0)
                            {
                                currentType = Enum.UpdateFor.Model.ToString();
                                var model = models.FirstOrDefault(x => x.ModelTranslatedId == translated.ModelId);
                                shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='modelSection' modelid='{translated.ModelId}' >({(model != null ? model.Name : string.Empty)})</span>";
                            }
                            else
                            {
                                currentType = Enum.UpdateFor.All.ToString();
                                shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >(All Vehicles)</span>";
                            }

                        }

                        if (!currentType.Equals(type, StringComparisonOrdinalIgnoreCase))
                        {
                            if (type == string.Empty)// First item
                            {
                                formatedTranslatedName +=
                                    $@"{translated.TranslatedName} - {shownData}";
                            }
                            else
                            {
                                formatedTranslatedName +=
                                    $@"{translated.TranslatedName} - {shownData}";
                            }

                            shownData = string.Empty;
                        }
                        else
                        {
                            if (currentName.Equals(translated.TranslatedName, StringComparisonOrdinalIgnoreCase))
                            {
                                formatedTranslatedName += $@" - {shownData}";
                            }
                            else
                            {
                                formatedTranslatedName += $@"{translated.TranslatedName} - {shownData}";
                            }
                            shownData = string.Empty;
                        }

                        type = currentType;
                        currentName = translated.TranslatedName;

                        newItem.DisplaySectionGroupName = tempName;
                        if (formatedTranslatedName.Trim() == "-" && IsSpecialProgram)
                        {
                            shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >Edit</span>";

                            formatedTranslatedName = $@"{shownData}";
                            newItem.TranslatedName = formatedTranslatedName;
                        }
                        else
                            newItem.TranslatedName = formatedTranslatedName;

                        result.Add(newItem);

                    }
                }

            }
            return result.OrderByDescending(x => x.Type).ToList();
        }

        public static void SortOutPrograms(List<VehicleProgramData> cashPrograms, List<VehicleProgramData> conditionalPrograms, List<VehicleProgramData> aprPrograms, List<IncentiveTranslated> translateds)
        {
            List<VehicleProgramData> tempCashPrograms = GetProgramsNeedToMove(cashPrograms, translateds);
            List<VehicleProgramData> tempConditionalPrograms = GetProgramsNeedToMove(conditionalPrograms, translateds);
            List<VehicleProgramData> tempAprPrograms = GetProgramsNeedToMove(aprPrograms, translateds);
            foreach (var cash in tempCashPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)cash.DisplaySection;
                if (section == Enum.DisplaySection.Conditional)
                {
                    MovingPrograms(cash, cashPrograms, conditionalPrograms);
                }
                if (section == Enum.DisplaySection.Finance)
                {
                    MovingPrograms(cash, cashPrograms, aprPrograms);
                }
            }
            foreach (var conditional in tempConditionalPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)conditional.DisplaySection;
                if (section == Enum.DisplaySection.Manufacturer)
                {
                    MovingPrograms(conditional, conditionalPrograms, cashPrograms);
                }
                if (section == Enum.DisplaySection.Finance)
                {
                    MovingPrograms(conditional, conditionalPrograms, aprPrograms);
                }
            }
            foreach (var apr in tempAprPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)apr.DisplaySection;
                if (section == Enum.DisplaySection.Manufacturer)
                {
                    MovingPrograms(apr, aprPrograms, cashPrograms);
                }
                if (section == Enum.DisplaySection.Conditional)
                {
                    MovingPrograms(apr, aprPrograms, conditionalPrograms);
                }
            }
            cashPrograms.OrderBy(x => x.Position.HasValue);
            conditionalPrograms.OrderBy(x => x.Position.HasValue);
            aprPrograms.OrderBy(x => x.Position.HasValue);
        }
        public static void MovingPrograms(VehicleProgramData movingProgram, List<VehicleProgramData> source, List<VehicleProgramData> destination)
        {
            if (movingProgram != null && source.Any() && destination.Any())
            {
                destination.Add(movingProgram);
                source.Remove(movingProgram);
            }
        }
        public static List<VehicleProgramData> GetProgramsNeedToMove(List<VehicleProgramData> programs, List<IncentiveTranslated> translateds)
        {
            //Cash
            List<VehicleProgramData> result = new List<VehicleProgramData>();
            foreach (var program in programs)
            {
                var matched = translateds.FirstOrDefault(y =>
                    y.DisplaySection > 0 &&
                    program.ProgramName.Equals(y.TranslatedName, StringComparison.OrdinalIgnoreCase) &&
                    program.Type.Equals(y.Type, StringComparison.OrdinalIgnoreCase));
                if (matched != null)
                {
                    program.DisplaySection = matched.DisplaySection;
                    program.Position = matched.Position;
                    result.Add(program);
                }
            }
            return result;
        }
        public static void SortOutProgramsForSearch(List<ProgramListData> cashPrograms, List<ProgramListData> conditionalPrograms, List<ProgramListData> aprPrograms, List<IncentiveTranslated> translateds)
        {
            List<ProgramListData> tempCashPrograms = GetProgramsNeedToMoveForSearch(cashPrograms, translateds);
            List<ProgramListData> tempConditionalPrograms = GetProgramsNeedToMoveForSearch(conditionalPrograms, translateds);
            List<ProgramListData> tempAprPrograms = GetProgramsNeedToMoveForSearch(aprPrograms, translateds);
            foreach (var cash in tempCashPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)cash.DisplaySection;
                if (section == Enum.DisplaySection.Conditional)
                {
                    MovingProgramsForSearch(cash, cashPrograms, conditionalPrograms);
                }
                if (section == Enum.DisplaySection.Finance)
                {
                    MovingProgramsForSearch(cash, cashPrograms, aprPrograms);
                }
            }
            foreach (var conditional in tempConditionalPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)conditional.DisplaySection;
                if (section == Enum.DisplaySection.Manufacturer)
                {
                    MovingProgramsForSearch(conditional, conditionalPrograms, cashPrograms);
                }
                if (section == Enum.DisplaySection.Finance)
                {
                    MovingProgramsForSearch(conditional, conditionalPrograms, aprPrograms);
                }
            }
            foreach (var apr in tempAprPrograms)
            {
                Enum.DisplaySection section = (Enum.DisplaySection)apr.DisplaySection;
                if (section == Enum.DisplaySection.Manufacturer)
                {
                    MovingProgramsForSearch(apr, aprPrograms, cashPrograms);
                }
                if (section == Enum.DisplaySection.Conditional)
                {
                    MovingProgramsForSearch(apr, aprPrograms, conditionalPrograms);
                }
            }
            cashPrograms.OrderBy(x => x.Position.HasValue);
            conditionalPrograms.OrderBy(x => x.Position.HasValue);
            aprPrograms.OrderBy(x => x.Position.HasValue);
        }
        public static void MovingProgramsForSearch(ProgramListData movingProgram, List<ProgramListData> source, List<ProgramListData> destination)
        {
            if (movingProgram != null && source.Any() && destination.Any())
            {
                if (!destination.Any(x => x.ProgramId.Equals(movingProgram.ProgramId)))
                {

                    destination.Add(movingProgram);
                    source.Remove(movingProgram);
                }
            }
        }
        public static List<ProgramListData> GetProgramsNeedToMoveForSearch(List<ProgramListData> programs, List<IncentiveTranslated> translateds)
        {
            //Cash
            List<ProgramListData> result = new List<ProgramListData>();
            foreach (var program in programs)
            {
                var matched = translateds.FirstOrDefault(y =>
                    y.DisplaySection > 0 &&
                    program.Name.Equals(y.TranslatedName, StringComparison.OrdinalIgnoreCase) &&
                    program.Type.Equals(y.Type, StringComparison.OrdinalIgnoreCase));
                if (matched != null)
                {
                    program.DisplaySection = matched.DisplaySection;
                    program.Position = matched.Position;
                    result.Add(program);
                }
            }
            return result;
        }

        public static List<IncentiveTranslated> GetIncentiveTranslatedsForSpecialProgramsForVin(string Vin, int ModelId)
        {
            List<IncentiveTranslated> translateds = new List<IncentiveTranslated>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                translateds = db.IncentiveTranslateds.Where(x => x.IsCashCoupon == true
                                                              && ((x.Vin.Equals(Vin, StringComparison.OrdinalIgnoreCase) && x.ModelId == 0)
                                                              || (x.Vin == "" && x.ModelId == 0) || (x.Vin == "" && x.ModelId == ModelId))).ToList();
            }
            return translateds;
        }
        public static bool ResetIncentiveTranslatedSpecialProgram(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {

                    int IncentiveTranslatedId = Convert.ToInt32(incentive.Id);
                    List<IncentiveTranslated> translateds = db.IncentiveTranslateds.Where(x =>
                       x.IncentiveTranslatedId.Equals(IncentiveTranslatedId)).ToList();
                    if (translateds.Any())
                    {
                        foreach (var translated in translateds)
                        {
                            db.Entry(translated).State = EntityState.Deleted;
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }
        public static bool UpdateIncentiveTranslatedSpecialProgram(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    StringBuilder sbEngineCodes = new StringBuilder();
                    StringBuilder sbExcludeVINs = new StringBuilder();
                    StringBuilder sbExcludeTrims = new StringBuilder();
                    StringBuilder sbExcludeBodyStyles = new StringBuilder();
                    StringBuilder sbExcludeYears = new StringBuilder();

                    int IncentiveTranslatedId = Convert.ToInt32(incentive.Id);
                    IncentiveTranslated translated = db.IncentiveTranslateds.FirstOrDefault(x =>
                        x.IncentiveTranslatedId == IncentiveTranslatedId);
                    if (translated != null)
                    {
                        translated.DisplaySection = incentive.DisplaySection ?? 0;
                        translated.IsActive = incentive.IsActive ?? false;
                        translated.Position = incentive.Position ?? 0;
                        translated.TranslatedName = incentive.TranslatedName;
                        translated.ProgramName = incentive.TranslatedName;
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            translated.Vin = incentive.UpdateForVin.Trim();
                            translated.ModelId = 0;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;
                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            translated.ModelId = incentive.UpdateForModelId;
                            translated.ChromeStyleId = incentive.ChromeStyleId;
                            translated.ChromeStyleIdYear = incentive.ChromeStyleIdYear;
                            translated.Vin = string.Empty;
                        }
                        else
                        {
                            translated.ModelId = 0;
                            translated.Vin = string.Empty;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;
                        }

                        translated.ModifiedDt = DateTime.Now;
                        translated.IsCashCoupon = true;
                        translated.IsCustomDiscount = false;
                        translated.Disclaimer = incentive.Disclaimer;
                        translated.Amount = incentive.Amount.Replace(",", "").Replace("$", "");
                        translated.ExcludeEngineCodes = "";
                        translated.ExcludeVINs = "";
                        translated.excludeTrimId = "";
                        translated.ExcludeYear = "";

                        if (incentive.excludeenginecodes != null)
                        {
                            foreach (var item in incentive.excludeenginecodes)
                            {
                                sbEngineCodes.Append(item + ",");
                            }
                        }
                        if (sbEngineCodes.Length > 0)
                        {

                            translated.ExcludeEngineCodes = sbEngineCodes.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeVINArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeVINArrayData)
                            {
                                sbExcludeVINs.Append(item + ", ");
                            }
                        }
                        if (sbExcludeVINs.Length > 0)
                        {
                            translated.ExcludeVINs = sbExcludeVINs.ToString().TrimEnd(',');
                        }
                        else
                        {
                            translated.ExcludeVINs = "0";
                        }
                        if (incentive.excludeTrims != null)
                        {
                            foreach (var item in incentive.excludeTrims)
                            {
                                sbExcludeTrims.Append(item + ",");
                            }
                        }
                        if (sbExcludeTrims.Length > 0)
                        {
                            translated.excludeTrimId = sbExcludeTrims.ToString().TrimEnd(',');
                        }
                        if (incentive.excludeBodyStyles != null)
                        {
                            foreach (var item in incentive.excludeBodyStyles)
                            {
                                sbExcludeBodyStyles.Append(item + ",");
                            }
                        }
                        if (sbExcludeBodyStyles.Length > 0)
                        {
                            translated.excludeBodyStyleId = sbExcludeBodyStyles.ToString().TrimEnd(',');
                        }

                        if (incentive.ExcludeYearsArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeYearsArrayData)
                            {
                                sbExcludeYears.Append(item + ",");
                            }
                        }
                        if (sbExcludeYears.Length > 0)
                        {
                            translated.ExcludeYear = sbExcludeYears.ToString().TrimEnd(',');
                        }

                        translated.StartDate = incentive.StartDate;
                        translated.EndDate = incentive.EndDate;
                        translated.HasSetDateRange = incentive.HasSetDateRange;
                        translated.HideOtherIncentives = incentive.HideOtherIncentives;
                        translated.HideDiscounts = incentive.HideDiscounts;
                        translated.DiscountType = incentive.DiscountType;
                        translated.IsMasterIncentive = incentive.IsMasterIncentive;
                        if (incentive.IsMasterIncentive)
                        {
                            translated.Vin = "0";
                            translated.ExcludeEngineCodes = "";
                            //  translated.ExcludeVINs = "";
                            translated.excludeTrimId = "";
                            translated.ExcludeYear = "";
                            translated.excludeBodyStyleId = "";
                            translated.ModelId = 0;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;

                        }
                        db.Entry(translated).State = EntityState.Modified;
                    }
                    else
                    {
                        IncentiveTranslated newItem = new IncentiveTranslated();
                        newItem.DisplaySection = incentive.DisplaySection ?? 0;
                        newItem.IsActive = incentive.IsActive ?? false;
                        newItem.Position = incentive.Position ?? 0;
                        newItem.TranslatedName = incentive.TranslatedName;
                        newItem.ProgramName = incentive.TranslatedName;
                        newItem.Type = incentive.Type;
                        newItem.Id = incentive.Id;
                        newItem.ExcludeEngineCodes = "";
                        newItem.ExcludeVINs = "";
                        newItem.excludeTrimId = "";
                        newItem.ExcludeYear = "";
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            newItem.Vin = incentive.UpdateForVin.Trim();
                            newItem.ModelId = 0;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;

                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            newItem.ModelId = incentive.UpdateForModelId;
                            newItem.ChromeStyleId = incentive.ChromeStyleId;
                            newItem.ChromeStyleIdYear = incentive.ChromeStyleIdYear;
                            newItem.Vin = string.Empty;
                        }
                        else
                        {
                            newItem.ModelId = 0;
                            newItem.Vin = string.Empty;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;
                        }
                        newItem.IsCashCoupon = true;
                        newItem.IsCustomDiscount = false;
                        newItem.Disclaimer = incentive.Disclaimer;
                        newItem.Amount = incentive.Amount.Replace(",", "").Replace("$", "");
                        newItem.CreatedDt = DateTime.Now;
                        newItem.ModifiedDt = DateTime.Now;

                        if (incentive.excludeenginecodes != null)
                        {
                            foreach (var item in incentive.excludeenginecodes)
                            {
                                sbEngineCodes.Append(item + ",");
                            }
                        }
                        if (sbEngineCodes.Length > 0)
                        {
                            newItem.ExcludeEngineCodes = sbEngineCodes.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeVINArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeVINArrayData)
                            {
                                sbExcludeVINs.Append(item + ", ");
                            }
                        }
                        if (sbExcludeVINs.Length > 0)
                        {
                            newItem.ExcludeVINs = sbExcludeVINs.ToString().TrimEnd(',');
                        }
                        else
                        {
                            newItem.ExcludeVINs = "0";
                        }
                        if (incentive.excludeTrims != null)
                        {
                            foreach (var item in incentive.excludeTrims)
                            {
                                sbExcludeTrims.Append(item + ",");
                            }
                        }
                        if (sbExcludeTrims.Length > 0)
                        {
                            newItem.excludeTrimId = sbExcludeTrims.ToString().TrimEnd(',');
                        }
                        if (incentive.excludeBodyStyles != null)
                        {
                            foreach (var item in incentive.excludeBodyStyles)
                            {
                                sbExcludeBodyStyles.Append(item + ",");
                            }
                        }
                        if (sbExcludeBodyStyles.Length > 0)
                        {
                            newItem.excludeBodyStyleId = sbExcludeBodyStyles.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeYearsArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeYearsArrayData)
                            {
                                sbExcludeYears.Append(item + ",");
                            }
                        }
                        if (sbExcludeYears.Length > 0)
                        {
                            newItem.ExcludeYear = sbExcludeYears.ToString().TrimEnd(',');
                        }

                        newItem.StartDate = incentive.StartDate;
                        newItem.EndDate = incentive.EndDate;
                        newItem.HasSetDateRange = incentive.HasSetDateRange;
                        newItem.HideOtherIncentives = incentive.HideOtherIncentives;
                        newItem.HideDiscounts = incentive.HideDiscounts;
                        newItem.DiscountType = incentive.DiscountType;
                        newItem.IsMasterIncentive = incentive.IsMasterIncentive;
                        if (incentive.IsMasterIncentive)
                        {
                            newItem.Vin = "0";
                            newItem.ExcludeEngineCodes = "";
                            newItem.ExcludeVINs = "";
                            newItem.excludeTrimId = "";
                            newItem.ExcludeYear = "";
                            newItem.excludeBodyStyleId = "";
                            newItem.ModelId = 0;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;

                        }
                        db.IncentiveTranslateds.Add(newItem);
                    }
                    db.SaveChanges();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
            }
            return isSuccess;
        }
        public static bool UpdateVehicleFactoryCodes(AgileVehicleFactoryCodeModel agileVehicleFactoryCodeModel)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var item = db.AgileVehicleFactoryCodes.Where(x => x.AgileVehicleId.Equals(agileVehicleFactoryCodeModel.AgileVehicleId)).FirstOrDefault();
                    if (item != null)
                    {
                        item.FactoryCodes = agileVehicleFactoryCodeModel.FactoryCodes;
                        db.Entry(item).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public static List<AgileVehicleFactoryCodeModel> GetVehicleFactoryCodesOfAgileVehicles()
        {
            List<AgileVehicleFactoryCodeModel> result = new List<AgileVehicleFactoryCodeModel>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var query = from av in db.AgileVehicles.Where(c => c.IsSold == false)
                            join vf in db.AgileVehicleFactoryCodes on av.AgileVehicleId equals vf.AgileVehicleId
                            into tbl_finalTable
                            from tbl in tbl_finalTable.DefaultIfEmpty()
                            select new AgileVehicleFactoryCodeModel()
                            {
                                AgileVehicleId = av.AgileVehicleId,
                                FactoryCodes = tbl.FactoryCodes,
                                Vin = av.Vin
                            };
                result = query.ToList();
            }
            return result;
        }

        public static List<HideIncentiveProgram> GetHiddenIncentivePrograms()
        {
            List<HideIncentiveProgram> result = new List<HideIncentiveProgram>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                result = db.HideIncentivePrograms.ToList();
            }
            return result;
        }
        public static async System.Threading.Tasks.Task<bool> DeleteHiddenIncentiveProgram(int Id)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter ProgramNumberParam = new SqlParameter("@Id", Id);
                await db.Database.ExecuteSqlCommandAsync("Usp_DeleteIncentiveProgram @Id", ProgramNumberParam);
                db.SaveChanges();
            }
            return true;
        }

        public static bool SaveIncentiveDataToHide(HideIncentiveProgramModel incentive, int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    HideIncentiveProgram newItem = new HideIncentiveProgram();
                    newItem.ProgramNumber = incentive.ProgramNumber;
                    newItem.CreatedDt = DateTime.Now;
                    newItem.ModifiedDt = DateTime.Now;
                    db.HideIncentivePrograms.Add(newItem);
                    db.SaveChanges();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
            }

            return isSuccess;
        }
        public static async System.Threading.Tasks.Task<bool> HideIncentiveProgramAsync(string ProgramNumber, string VIN, int ModelId, string ModelName)
        {
            int Flag = 0;
            if (string.IsNullOrEmpty(VIN) && ModelId == 0)
                Flag = 0;
            else if (!string.IsNullOrEmpty(VIN))
                Flag = 1;
            else if (ModelId != 0)
                Flag = 2;


            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter FlagParam = new SqlParameter("@Flag", Flag);
                SqlParameter ProgramNumberParam = new SqlParameter("@ProgramNumber", ProgramNumber);
                SqlParameter VINParam = new SqlParameter("@VIN", VIN);
                SqlParameter ModelIdParam = new SqlParameter("@ModelId", ModelId);
                SqlParameter ModelNameParam = new SqlParameter("@ModelName", ModelName);
                await db.Database.ExecuteSqlCommandAsync("Usp_HideIncentiveProgram @Flag,@ProgramNumber,@VIN,@ModelId,@ModelName", FlagParam, ProgramNumberParam, VINParam, ModelIdParam, ModelNameParam);
                db.SaveChanges();
            }
            return true;
        }
        public static List<VehicleFeatureMaster> GetVehicleFeatureMasterData()
        {
            List<VehicleFeatureMaster> result = new List<VehicleFeatureMaster>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                result = db.VehicleFeatureMaster.OrderBy(x => x.FeatureName).ToList();
            }
            return result;
        }
        public static async System.Threading.Tasks.Task<bool> UpdateVehicleFeatureMasterData(VehicleFeatureMaster vehicleFeatureMaster)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var item = db.VehicleFeatureMaster.Where(x => x.FeatureId.Equals(vehicleFeatureMaster.FeatureId)).FirstOrDefault();
                    if (item != null)
                    {
                        item.LabelOverride = vehicleFeatureMaster.LabelOverride;
                        item.IsSearchable = vehicleFeatureMaster.IsSearchable;
                        db.Entry(item).State = EntityState.Modified;

                    }
                    SqlParameter MasterFeatureId = new SqlParameter("@MasterFeatureId", vehicleFeatureMaster.FeatureId);
                    SqlParameter IsSearchable = new SqlParameter("@IsSearchable", vehicleFeatureMaster.IsSearchable);
                    await db.Database.ExecuteSqlCommandAsync("Usp_HideIncentiveProgram @MasterFeatureId,@IsSearchable", MasterFeatureId, IsSearchable);
                    db.SaveChanges();
                    //var VehicleFeaturesList = db.VehicleFeatures.Where(x => x.MasterFeatureId.Equals(vehicleFeatureMaster.FeatureId)).ToList();
                    //if (VehicleFeaturesList != null)
                    //{
                    //    foreach (var VehicleFeaturesItem in VehicleFeaturesList)
                    //    {
                    //        VehicleFeaturesItem.IsSearchable = vehicleFeatureMaster.IsSearchable;
                    //        db.Entry(VehicleFeaturesItem).State = EntityState.Modified;
                    //    }
                    //}

                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public static List<VehicleFeatures> GetVehicleFeaturesOfAgileVehicleId(int AgileVehicleId)
        {
            List<VehicleFeatures> result = new List<VehicleFeatures>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                result = db.VehicleFeatures.Where(x => x.AgileVehicleId.Equals(AgileVehicleId)).OrderBy(x => x.FeatureName).ToList();
            }
            return result;
        }
        public static List<VehicleData> GetBulkUpdateVehicleData(string Year = "", string Make = "", string Model = "", string Orderby = "Year", string OrderType = "asc")
        {
            List<VehicleData> result = new List<VehicleData>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter ParamYear = new SqlParameter("@Year", Year);
                SqlParameter ParamMake = new SqlParameter("@Make", Make);
                SqlParameter ParamModel = new SqlParameter("@Model", Model);
                SqlParameter ParamOrderby = new SqlParameter("@Orderby", Orderby);
                SqlParameter ParamOrderType = new SqlParameter("@OrderType", OrderType);
                result = db.Database.SqlQuery<VehicleData>("Usp_GetBulkUpdateVehicles @Year,@Make,@Model,@Orderby,@OrderType", ParamYear, ParamMake, ParamModel, ParamOrderby, ParamOrderType).ToList();
            }
            return result;
        }
        public static async System.Threading.Tasks.Task<bool> UpdateBulkVehicleData(AgileVehicle agilevehicle)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    SqlParameter AgileVehicleId = new SqlParameter("@AgileVehicleId", agilevehicle.AgileVehicleId);
                    SqlParameter OverridePrice = new SqlParameter("@OverridePrice", agilevehicle.OverridePrice);
                    SqlParameter MsrpOverride = new SqlParameter("@Msrpoverride", agilevehicle.MSRPOverride);
                    SqlParameter EmployeePrice = new SqlParameter("@EmployeePrice", agilevehicle.EmployeePrice);
                    await db.Database.ExecuteSqlCommandAsync("Usp_UpdateBulkVehicles @AgileVehicleId,@OverridePrice,@Msrpoverride,@EmployeePrice", AgileVehicleId, OverridePrice, MsrpOverride, EmployeePrice);
                    db.SaveChanges();


                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public static async System.Threading.Tasks.Task<bool> ResetPriceOverride(int AgileVehicleId, string Username)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    SqlParameter ParamAgileVehicleId = new SqlParameter("@AgileVehicleId", AgileVehicleId);
                    SqlParameter ParamModifiedBy = new SqlParameter("@ModifiedBy", Username);
                    await db.Database.ExecuteSqlCommandAsync("Usp_ResetPriceOverride @AgileVehicleId,@ModifiedBy", ParamAgileVehicleId, ParamModifiedBy);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }


        public static bool ResetIncentiveTranslatedCustomDiscount(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = true;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {

                    int IncentiveTranslatedId = Convert.ToInt32(incentive.Id);
                    List<IncentiveTranslated> translateds = db.IncentiveTranslateds.Where(x =>
                       x.IncentiveTranslatedId.Equals(IncentiveTranslatedId)).ToList();
                    if (translateds.Any())
                    {
                        foreach (var translated in translateds)
                        {
                            db.Entry(translated).State = EntityState.Deleted;
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }



        public static bool UpdateIncentiveTranslatedCustomDiscount(IncentiveTranslatedModel incentive, int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    StringBuilder sbEngineCodes = new StringBuilder();
                    StringBuilder sbExcludeVINs = new StringBuilder();
                    StringBuilder sbExcludeTrims = new StringBuilder();
                    StringBuilder sbExcludeBodyStyles = new StringBuilder();
                    StringBuilder sbExcludeYears = new StringBuilder();
                    int IncentiveTranslatedId = Convert.ToInt32(incentive.Id);
                    IncentiveTranslated translated = db.IncentiveTranslateds.FirstOrDefault(x =>
                        x.IncentiveTranslatedId == IncentiveTranslatedId);
                    if (translated != null)
                    {
                        translated.DisplaySection = incentive.DisplaySection ?? 0;
                        translated.IsActive = incentive.IsActive ?? false;
                        translated.Position = incentive.Position ?? 0;
                        translated.TranslatedName = incentive.TranslatedName;
                        translated.ProgramName = incentive.TranslatedName;
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            translated.Vin = incentive.UpdateForVin.Trim();
                            translated.ModelId = 0;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;
                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            translated.ModelId = incentive.UpdateForModelId;
                            translated.Vin = string.Empty;
                            translated.ChromeStyleId = incentive.ChromeStyleId;
                            translated.ChromeStyleIdYear = incentive.ChromeStyleIdYear;
                        }
                        else
                        {
                            translated.ModelId = 0;
                            translated.Vin = string.Empty;
                            translated.ChromeStyleId = string.Empty;
                            translated.ChromeStyleIdYear = 0;
                        }
                        translated.ModifiedDt = DateTime.Now;
                        translated.IsCashCoupon = true;
                        translated.IsCustomDiscount = true;
                        translated.Disclaimer = incentive.Disclaimer;
                        translated.Amount = incentive.Amount.Replace(",", "").Replace("$", "");
                        translated.ExcludeEngineCodes = "";
                        translated.ExcludeVINs = "";
                        translated.excludeTrimId = "";
                        translated.ExcludeYear = "";
                        if (incentive.excludeenginecodes != null)
                        {
                            foreach (var item in incentive.excludeenginecodes)
                            {
                                sbEngineCodes.Append(item + ",");
                            }
                        }
                        if (sbEngineCodes.Length > 0)
                        {
                            translated.ExcludeEngineCodes = sbEngineCodes.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeVINArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeVINArrayData)
                            {
                                sbExcludeVINs.Append(item + ", ");
                            }
                        }
                        if (sbExcludeVINs.Length > 0)
                        {
                            translated.ExcludeVINs = sbExcludeVINs.ToString().TrimEnd(',');
                        }
                        else
                        {
                            translated.ExcludeVINs = "0";
                        }
                        if (incentive.excludeTrims != null)
                        {
                            foreach (var item in incentive.excludeTrims)
                            {
                                sbExcludeTrims.Append(item + ",");
                            }
                        }
                        if (sbExcludeTrims.Length > 0)
                        {
                            translated.excludeTrimId = sbExcludeTrims.ToString().TrimEnd(',');
                        }
                        if (incentive.excludeBodyStyles != null)
                        {
                            foreach (var item in incentive.excludeBodyStyles)
                            {
                                sbExcludeBodyStyles.Append(item + ",");
                            }
                        }
                        if (sbExcludeBodyStyles.Length > 0)
                        {
                            translated.excludeBodyStyleId = sbExcludeBodyStyles.ToString().TrimEnd(',');
                        }


                        foreach (var item in incentive.ExcludeYearsArrayData)
                        {
                            sbExcludeYears.Append(item + ",");
                        }
                        if (sbExcludeYears.Length > 0)
                        {
                            translated.ExcludeYear = sbExcludeYears.ToString().TrimEnd(',');
                        }

                        translated.StartDate = incentive.StartDate;
                        translated.EndDate = incentive.EndDate;
                        translated.HasSetDateRange = incentive.HasSetDateRange;
                        translated.HideOtherIncentives = incentive.HideOtherIncentives;
                        translated.HideDiscounts = incentive.HideDiscounts;
                        translated.DiscountType = incentive.DiscountType;
                        translated.IsMasterIncentive = incentive.IsMasterIncentive;
                        if (incentive.IsMasterIncentive)
                        {
                            translated.Vin = "0";
                            translated.ExcludeEngineCodes = "";
                            //  translated.ExcludeVINs = "";
                            translated.excludeTrimId = "";
                            translated.ExcludeYear = "";
                            translated.excludeBodyStyleId = "";
                            translated.ModelId = 0;
                        }
                        db.Entry(translated).State = EntityState.Modified;
                    }
                    else
                    {
                        IncentiveTranslated newItem = new IncentiveTranslated();
                        newItem.DisplaySection = incentive.DisplaySection ?? 0;
                        newItem.IsActive = incentive.IsActive ?? false;
                        newItem.Position = incentive.Position ?? 0;
                        newItem.TranslatedName = incentive.TranslatedName;
                        newItem.ProgramName = incentive.TranslatedName;
                        newItem.Type = incentive.Type;
                        newItem.Id = incentive.Id;
                        newItem.ExcludeEngineCodes = "";
                        newItem.ExcludeVINs = "";
                        newItem.excludeTrimId = "";
                        newItem.ExcludeYear = "";
                        if (incentive.UpdateFor == (int)Enum.UpdateFor.Vin)
                        {
                            newItem.Vin = incentive.UpdateForVin.Trim();
                            newItem.ModelId = 0;
                        }
                        else if (incentive.UpdateFor == (int)Enum.UpdateFor.Model)
                        {
                            newItem.ModelId = incentive.UpdateForModelId;
                            newItem.Vin = string.Empty;
                        }
                        else
                        {
                            newItem.ModelId = 0;
                            newItem.Vin = string.Empty;
                        }
                        newItem.IsCashCoupon = true;
                        newItem.IsCustomDiscount = true;
                        newItem.Disclaimer = incentive.Disclaimer;
                        newItem.Amount = incentive.Amount.Replace(",", "").Replace("$", "");
                        newItem.CreatedDt = DateTime.Now;
                        newItem.ModifiedDt = DateTime.Now;
                        if (incentive.excludeenginecodes != null)
                        {
                            foreach (var item in incentive.excludeenginecodes)
                            {
                                sbEngineCodes.Append(item + ",");
                            }
                        }
                        if (sbEngineCodes.Length > 0)
                        {
                            newItem.ExcludeEngineCodes = sbEngineCodes.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeVINArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeVINArrayData)
                            {
                                sbExcludeVINs.Append(item + ", ");
                            }
                        }
                        if (sbExcludeVINs.Length > 0)
                        {
                            newItem.ExcludeVINs = sbExcludeVINs.ToString().TrimEnd(',');
                        }
                        else
                        {
                            newItem.ExcludeVINs = "0";
                        }
                        if (incentive.excludeTrims != null)
                        {
                            foreach (var item in incentive.excludeTrims)
                            {
                                sbExcludeTrims.Append(item + ",");
                            }
                        }
                        if (sbExcludeTrims.Length > 0)
                        {
                            newItem.excludeTrimId = sbExcludeTrims.ToString().TrimEnd(',');
                        }
                        if (incentive.excludeBodyStyles != null)
                        {
                            foreach (var item in incentive.excludeBodyStyles)
                            {
                                sbExcludeBodyStyles.Append(item + ",");
                            }
                        }
                        if (sbExcludeBodyStyles.Length > 0)
                        {
                            newItem.excludeBodyStyleId = sbExcludeBodyStyles.ToString().TrimEnd(',');
                        }
                        if (incentive.ExcludeYearsArrayData != null)
                        {
                            foreach (var item in incentive.ExcludeYearsArrayData)
                            {
                                sbExcludeYears.Append(item + ",");
                            }
                        }
                        if (sbExcludeYears.Length > 0)
                        {
                            newItem.ExcludeYear = sbExcludeYears.ToString().TrimEnd(',');
                        }

                        newItem.StartDate = incentive.StartDate;
                        newItem.EndDate = incentive.EndDate;
                        newItem.HasSetDateRange = incentive.HasSetDateRange;
                        newItem.HideOtherIncentives = incentive.HideOtherIncentives;
                        newItem.HideDiscounts = incentive.HideDiscounts;
                        newItem.DiscountType = incentive.DiscountType;
                        newItem.IsMasterIncentive = incentive.IsMasterIncentive;
                        if (incentive.IsMasterIncentive)
                        {
                            newItem.Vin = "0";
                            newItem.ExcludeEngineCodes = "";
                            newItem.ExcludeVINs = "";
                            newItem.excludeTrimId = "";
                            newItem.ExcludeYear = "";
                            newItem.excludeBodyStyleId = "";
                            newItem.ModelId = 0;
                        }
                        db.IncentiveTranslateds.Add(newItem);
                    }
                    db.SaveChanges();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
            }
            return isSuccess;
        }



        public static List<IncentiveTranslatedModel> GetIncentiveTranslatedData(bool IsSpecialProgram, bool IsCustomDiscount = false)
        {
            List<IncentiveTranslatedModel> result = new List<IncentiveTranslatedModel>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var models = (from model in db.ModelTranslateds
                              select new { ModelTranslatedId = model.ModelTranslatedId, Name = model.Name })
                    .ToList();
                string tempName = string.Empty;
                string formatedTranslatedName = string.Empty;
                string shownData = string.Empty;
                string type = string.Empty;
                string currentName = string.Empty;


                if (!IsSpecialProgram)
                {
                    var translatedSource = db.IncentiveTranslateds.ToList().Where(x => !x.IsCashCoupon && x.IsActive);
                    var sourcePrograms = (from program in db.Programs
                                          select new
                                          {
                                              Name = program.Name,
                                              Id = program.Id,
                                              Type = program.Type,
                                              IsTargeted = program.IsTargeted,
                                              Conditional = program.Conditional,
                                          }).Distinct();

                    foreach (var program in sourcePrograms)
                    {

                        var newItem = new IncentiveTranslatedModel()
                        {
                            Name = program.Name,
                            Type = program.Type,
                            Id = program.Id,
                            //Position = program.Position,
                            //DisplaySection = program.DisplaySection,
                            TranslatedName = string.Empty,

                            //IsActive = program.IsActive,
                        };

                        string defaultSection = string.Empty;

                        if (program.Conditional == "false" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Manufacturer.ToString();
                        }
                        else if (program.Conditional == "true" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Conditional.ToString();
                        }
                        else if (program.Type.Equals(ProgramTypeEnum.APR.ToString()))
                        {
                            defaultSection = Enum.DisplaySection.Finance.ToString();
                        }

                        List<IncentiveTranslated> translateds = translatedSource.Where(x =>
                            x.ProgramName.Equals(program.Name, StringComparisonOrdinalIgnoreCase)
                            && x.Type.Equals(program.Type, StringComparisonOrdinalIgnoreCase) && x.Id == program.Id).OrderByDescending(x => x.Vin != string.Empty).ThenBy(x => x.ModelId > 0).ToList();

                        tempName = string.Empty;
                        formatedTranslatedName = string.Empty;
                        shownData = string.Empty;
                        type = string.Empty;
                        currentName = string.Empty;

                        if (translateds.Any())
                        {
                            newItem.DisplaySection = 0;


                            foreach (var translated in translateds)
                            {
                                string currentType = string.Empty;
                                string status = string.Empty;
                                string className = string.Empty;
                                string name = string.Empty;
                                if (translated.DisplaySection > 0)
                                {
                                    Enum.DisplaySection section = (Enum.DisplaySection)translated.DisplaySection;
                                    name = EnumHelper.GetDescriptionFromEnumValue(section);
                                }

                                if (!string.IsNullOrEmpty(name) && !tempName.Contains(name, StringComparisonOrdinalIgnoreCase))
                                {
                                    tempName += name + BreakNewLine;
                                }

                                if (translated.IsActive)
                                {
                                    className = "itemActive";
                                    status = "active";
                                }
                                else
                                {
                                    className = "itemHidden";
                                    status = "hidden";
                                }

                                if (!string.IsNullOrEmpty(translated.TranslatedName))
                                {
                                    if (!string.IsNullOrEmpty(translated.Vin))
                                    {
                                        currentType = Enum.UpdateFor.Vin.ToString();
                                        shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='vinSection' vin='{translated.Vin}'>({translated.Vin})</span>";
                                    }
                                    else if (translated.ModelId > 0)
                                    {
                                        currentType = Enum.UpdateFor.Model.ToString();
                                        var model = models.FirstOrDefault(x => x.ModelTranslatedId == translated.ModelId);
                                        shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='modelSection' modelid='{translated.ModelId}' >({(model != null ? model.Name : string.Empty)})</span>";
                                    }
                                    else
                                    {
                                        currentType = Enum.UpdateFor.All.ToString();
                                        shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >(All Vehicles)</span>";
                                    }

                                }

                                if (!currentType.Equals(type, StringComparisonOrdinalIgnoreCase))
                                {
                                    if (type == string.Empty)// First item
                                    {
                                        formatedTranslatedName +=
                                            $@"{translated.TranslatedName} - {shownData}";
                                    }
                                    else
                                    {
                                        formatedTranslatedName +=
                                            $@"{BreakNewLine}{BreakNewLine}{translated.TranslatedName} - {shownData}";
                                    }

                                    shownData = string.Empty;
                                }
                                else
                                {
                                    if (currentName.Equals(translated.TranslatedName, StringComparisonOrdinalIgnoreCase))
                                    {
                                        formatedTranslatedName += $@" - {shownData}";
                                    }
                                    else
                                    {
                                        formatedTranslatedName += $@"{BreakNewLine}{BreakNewLine}{translated.TranslatedName} - {shownData}";
                                    }
                                    shownData = string.Empty;
                                }

                                type = currentType;
                                currentName = translated.TranslatedName;
                                newItem.StartDate = translated.StartDate;
                                newItem.EndDate = translated.EndDate;
                                newItem.ChromeStyleId = translated.ChromeStyleId;
                                newItem.ChromeStyleIdYear = translated.ChromeStyleIdYear;
                            }

                            if (!tempName.Contains(defaultSection, StringComparisonOrdinalIgnoreCase))
                            {
                                tempName += defaultSection + BreakNewLine;
                            }
                            newItem.DisplaySectionGroupName = tempName;
                            newItem.TranslatedName = formatedTranslatedName;


                        }
                        else
                        {
                            if (program.Conditional == "false" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Manufacturer;
                            }
                            else if (program.Conditional == "true" && program.Type.Equals(ProgramTypeEnum.Cash.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Conditional;
                            }
                            else if (program.Type.Equals(ProgramTypeEnum.APR.ToString()))
                            {
                                newItem.DisplaySection = (int)Enum.DisplaySection.Finance;
                            }
                            newItem.TranslatedName = string.Empty;
                            newItem.IsActive = false;
                            newItem.IsActiveString = "Hidden";
                            newItem.DisplaySectionGroupName = defaultSection;
                            newItem.ChromeStyleId = string.Empty;
                            newItem.ChromeStyleIdYear = 0;


                        }


                        newItem.Amount = string.Join(",", (from p in db.Programs
                                                           where program.Name == p.Name && program.Type == p.Type
                                                           select p.Amount).Distinct());

                        newItem.IsCashCoupon = false;
                        result.Add(newItem);
                    }
                }
                else
                {
                    //Get all the Cash or Trade items
                    var translatedSource = db.IncentiveTranslateds.ToList().Where(x => x.IsCashCoupon && x.IsCustomDiscount == IsCustomDiscount && x.IsActive);

                    foreach (var translated in translatedSource)
                    {
                        StringBuilder sbExcludeEngingCodeUserFriendlyName = new StringBuilder();
                        if (!string.IsNullOrEmpty(translated.ExcludeEngineCodes))
                        {
                            foreach (var item in translated.ExcludeEngineCodes.Split(new char[] { ',' }))
                            {
                                int EngineId = Convert.ToInt32(item);
                                var EngineData = db.Engines.Where(x => x.EngineId == EngineId).FirstOrDefault();
                                if (EngineData != null)
                                    sbExcludeEngingCodeUserFriendlyName.Append(EngineData.Name + ",");
                            }
                        }
                        var newItem = new IncentiveTranslatedModel()
                        {
                            DisplaySection = translated.DisplaySection,
                            Name = translated.ProgramName,
                            Type = translated.Type,
                            //Position = program.Position,
                            Disclaimer = translated.Disclaimer,
                            TranslatedName = translated.TranslatedName,
                            IsCashCoupon = translated.IsCashCoupon,
                            Amount = translated.Amount,
                            ExcludeEngingCodeUserFriendlyName = sbExcludeEngingCodeUserFriendlyName.ToString().TrimEnd(','),
                            Id = Convert.ToString(translated.IncentiveTranslatedId),
                            excludeenginecodesData = translated.ExcludeEngineCodes == null ? null : translated.ExcludeEngineCodes,
                            ExcludeVINs = translated.ExcludeVINs == "0" ? null : translated.ExcludeVINs,
                            StartDate = translated.StartDate,
                            EndDate = translated.EndDate,
                            DiscountTypeFriendlyName = translated.DiscountType == 2 ? "% Of MSRP" : "Flat Amount",
                            excludeTrimId = translated.excludeTrimId,
                            ExcludeYear = translated.ExcludeYear,
                            HideOtherIncentives = translated.HideOtherIncentives,
                            HideDiscounts = translated.HideDiscounts,
                            DiscountType = translated.DiscountType,
                            excludeBodyStyleId = translated.excludeBodyStyleId,
                            IsMasterIncentive = translated.IsMasterIncentive,
                            ChromeStyleId = translated.ChromeStyleId,
                            ChromeStyleIdYear = translated.ChromeStyleIdYear
                            //IsActive = program.IsActive,
                        };


                        string currentType = string.Empty;
                        string status = string.Empty;
                        string className = string.Empty;
                        string name = string.Empty;
                        tempName = string.Empty;
                        shownData = string.Empty;
                        formatedTranslatedName = string.Empty;
                        if (translated.DisplaySection > 0)
                        {
                            Enum.DisplaySection section = (Enum.DisplaySection)translated.DisplaySection;
                            name = EnumHelper.GetDescriptionFromEnumValue(section);
                        }

                        if (!string.IsNullOrEmpty(name) && !tempName.Contains(name, StringComparisonOrdinalIgnoreCase))
                        {
                            tempName += name + BreakNewLine;
                        }

                        if (translated.IsActive)
                        {
                            className = "itemActive";
                            status = "active";
                        }
                        else
                        {
                            className = "itemHidden";
                            status = "hidden";
                        }

                        if (!string.IsNullOrEmpty(translated.TranslatedName))
                        {
                            if (!string.IsNullOrEmpty(translated.Vin))
                            {
                                currentType = Enum.UpdateFor.Vin.ToString();
                                shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='vinSection' vin='{translated.Vin}'>({translated.Vin})</span>";
                            }
                            else if (translated.ModelId > 0)
                            {
                                currentType = Enum.UpdateFor.Model.ToString();
                                var model = models.FirstOrDefault(x => x.ModelTranslatedId == translated.ModelId);
                                shownData += $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='modelSection' modelid='{translated.ModelId}' >({(model != null ? model.Name : string.Empty)})</span>";
                            }
                            else
                            {
                                currentType = Enum.UpdateFor.All.ToString();
                                shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >(All Vehicles)</span>";
                            }

                        }

                        if (!currentType.Equals(type, StringComparisonOrdinalIgnoreCase))
                        {
                            if (type == string.Empty)// First item
                            {
                                formatedTranslatedName +=
                                    $@"{translated.TranslatedName} - {shownData}";
                            }
                            else
                            {
                                formatedTranslatedName +=
                                    $@"{translated.TranslatedName} - {shownData}";
                            }

                            shownData = string.Empty;
                        }
                        else
                        {
                            if (currentName.Equals(translated.TranslatedName, StringComparisonOrdinalIgnoreCase))
                            {
                                formatedTranslatedName += $@" - {shownData}";
                            }
                            else
                            {
                                formatedTranslatedName += $@"{translated.TranslatedName} - {shownData}";
                            }
                            shownData = string.Empty;
                        }

                        type = currentType;
                        currentName = translated.TranslatedName;

                        newItem.DisplaySectionGroupName = tempName;
                        if (formatedTranslatedName.Trim() == "-" && IsSpecialProgram)
                        {
                            shownData = $@"<span class='{className}' displaySection='{translated.DisplaySection}' status='{status}' position='{translated.Position}' translatedName='{translated.TranslatedName}' isUpdateFor='all' >Edit</span>";

                            formatedTranslatedName = $@"{shownData}";
                            newItem.TranslatedName = formatedTranslatedName;
                        }
                        else
                            newItem.TranslatedName = formatedTranslatedName;

                        result.Add(newItem);

                    }
                }

            }
            return result.OrderByDescending(x => x.Type).ToList();
        }
        public static ModelSpecificData FilterModelSpecicData(int ModelNumber)
        {
            ModelSpecificData lstModelSpecificData = new ModelSpecificData();

            try
            {
                using (var db = new ServicePageDbContext())
                {
                    lstModelSpecificData.YearList = db.AgileVehicles.Where(x => x.ModelId.Equals(ModelNumber) && !x.IsSold).Select(item => new MasterData { Id = item.Year, Name = item.Year.ToString() }).Distinct().ToList();
                    lstModelSpecificData.VinList = db.AgileVehicles.Where(x => x.ModelId.Equals(ModelNumber) && !x.IsSold).Select(item => new MasterData { Name = item.Vin.ToString() }).Distinct().ToList();
                    lstModelSpecificData.EnginesList = (from av in db.AgileVehicles
                                                        join e in db.Engines on av.EngineId equals e.EngineId
                                                        where !av.IsSold && av.ModelId == ModelNumber
                                                        select e).Distinct().Select(item => new MasterData { Id = item.EngineId, Name = item.Name.ToString() }).ToList();
                    lstModelSpecificData.BodyStyleList = (from av in db.AgileVehicles
                                                          join b in db.BodyStyles on av.BodyStyleId equals b.BodyStyleId
                                                          where !av.IsSold && av.ModelId == ModelNumber
                                                          select b).Distinct().Select(item => new MasterData { Id = item.BodyStyleId, Name = item.Name.ToString() }).ToList();
                    lstModelSpecificData.TrimList = (from av in db.AgileVehicles
                                                     join t in db.Trims on av.TrimId equals t.TrimId
                                                     where !av.IsSold && av.ModelId == ModelNumber
                                                     select t).Distinct().Select(item => new MasterData { Id = item.TrimId, Name = item.Name.ToString() }).ToList();

                    lstModelSpecificData.ChromeStyles = (from av in db.AgileVehicles
                                                         join hv in db.Vehicles on av.AgileVehicleId equals hv.Id
                                                         join m in db.Models on av.ModelId equals m.ModelId
                                                         where !av.IsSold && av.ModelId == ModelNumber
                                                         && !string.IsNullOrEmpty(hv.chromestyleid)
                                                         select hv).Select(item => new MasterData { IdString = item.chromestyleid, Name = item.model + " - " + item.styledescription }).DistinctBy(p => new { p.IdString, p.Name }).ToList();

                }

            }
            catch (Exception e)
            {
                lstModelSpecificData = new ModelSpecificData();
            }
            return lstModelSpecificData;
        }
        public static List<MasterData> FilterChromeStyleIDBasedData(string ChromeStyleId)
        {
            List<MasterData> lstChromeStyleSpecificData = new List<MasterData>();

            try
            {
                using (var db = new ServicePageDbContext())
                {
                    lstChromeStyleSpecificData = (from av in db.AgileVehicles
                                                  join hv in db.Vehicles on av.AgileVehicleId equals hv.Id
                                                  where !av.IsSold && hv.chromestyleid.Equals(ChromeStyleId)
                                                  && !string.IsNullOrEmpty(hv.chromestyleid)
                                                  select hv).Select(item => new MasterData { Id = item.year, Name = item.year.ToString() }).DistinctBy(p => new { p.Name }).ToList();

                }

            }
            catch (Exception e)
            {
                lstChromeStyleSpecificData = new List<MasterData>();
            }
            return lstChromeStyleSpecificData;
        }

        public static List<MasterData> FilterYearBasedChromeStyleIdsData(int year, int modelNumber)
        {
            List<MasterData> lstChromeStyleSpecificData = new List<MasterData>();

            try
            {
                using (var db = new ServicePageDbContext())
                {
                    lstChromeStyleSpecificData = (from av in db.AgileVehicles
                                                  join hv in db.Vehicles on av.AgileVehicleId equals hv.Id
                                                  join m in db.Models on av.ModelId equals m.ModelId
                                                  where !av.IsSold && av.ModelId == modelNumber && av.Year == year
                                                  && !string.IsNullOrEmpty(hv.chromestyleid)
                                                  select hv).Select(item => new MasterData { IdString = item.chromestyleid, Name = item.model + " - " + item.styledescription }).DistinctBy(p => new { p.IdString, p.Name }).ToList();

                }

            }
            catch (Exception e)
            {
                lstChromeStyleSpecificData = new List<MasterData>();
            }
            return lstChromeStyleSpecificData;
        }

        public static bool UpdateBulkVehicleIncentives(int IncentiveTranslatedId, string ExcludeVins, string IncludeMasterIncentiveVins)
        {
            List<VehicleData> result = new List<VehicleData>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter ParamIncentiveTranslatedId = new SqlParameter("@IncentiveTranslatedId", IncentiveTranslatedId);
                SqlParameter ParamExcludeVins = new SqlParameter("@ExcludeVins", ExcludeVins);
                SqlParameter ParamIncludeMasterIncentiveVins = new SqlParameter("@IncludeMasterIncentiveVins", IncludeMasterIncentiveVins);
                db.Database.ExecuteSqlCommand("exec UpdateBulkVehicleIncentives @IncentiveTranslatedId,@ExcludeVins,@IncludeMasterIncentiveVins", ParamIncentiveTranslatedId, ParamExcludeVins, ParamIncludeMasterIncentiveVins);
                db.SaveChanges();

            }
            return true;
        }
        public static List<VehicleIncentivesModel> GetMasterCustomIncentives(int dealerId, bool IsUsed)
        {
            using (var context = new AppDbContext())
            {
                return context.Database.SqlQuery<VehicleIncentivesModel>("exec GetMasterCustomIncentives " + (IsUsed ? 0 : 1)).ToList();
            }
        }

        public static List<VehicleData> GetVehiclesForBulkIncentiveUpdate(string Year = "", string Make = "", string Model = "", string Trim = "", bool IncludeUsed = false, string ChromeStyleId = "", string Vin = "", int IncentiveTranslatedId = 0, string Orderby = "Year", string OrderType = "asc")
        {
            List<VehicleData> result = new List<VehicleData>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter ParamYear = new SqlParameter("@Year", Year);
                SqlParameter ParamMake = new SqlParameter("@Make", Make);
                SqlParameter ParamModel = new SqlParameter("@Model", Model);
                SqlParameter ParamTrim = new SqlParameter("@Trim", Trim);
                SqlParameter ParamIncludeUsed = new SqlParameter("@IncludeUsed", IncludeUsed);
                SqlParameter ParamChromeStyleId = new SqlParameter("@ChromeStyleId", ChromeStyleId);
                SqlParameter ParamVin = new SqlParameter("@Vin", Vin);
                SqlParameter ParamIncentiveTranslatedId = new SqlParameter("@IncentiveTranslatedId", IncentiveTranslatedId);
                SqlParameter ParamOrderby = new SqlParameter("@Orderby", Orderby);
                SqlParameter ParamOrderType = new SqlParameter("@OrderType", OrderType);
                result = db.Database.SqlQuery<VehicleData>("GetVehicles @Year,@Make,@Model,@Trim,@IncludeUsed,@ChromeStyleId,@Vin,@IncentiveTranslatedId,@Orderby,@OrderType", ParamYear, ParamMake, ParamModel, ParamTrim, ParamIncludeUsed, ParamChromeStyleId, ParamVin, ParamIncentiveTranslatedId, ParamOrderby, ParamOrderType).ToList();
            }
            return result;
        }
        public static List<VehicleIncentivesModel> GetVehicleProgramDatasByVehicelId(int agileVehicleId, int dealerId)
        {
            using (var context = new AppDbContext())
            {
                return context.Database.SqlQuery<VehicleIncentivesModel>("exec GetVehicleIncentives " + agileVehicleId).ToList();
            }
        }
        public static void UpdateVehicleIncentive(VehicleIncentivesModel vehicleIncentivesModel)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter PARAMINCENTIVETRANSLATEDID = new SqlParameter("@IncentiveTranslatedId", vehicleIncentivesModel.IncentivetranslatedId);
                SqlParameter PARAMID = new SqlParameter("@Id", vehicleIncentivesModel.Id);
                SqlParameter PARAMISACTIVE = new SqlParameter("@IsActive", vehicleIncentivesModel.Active);
                SqlParameter PARAMAGILEVEHICLEID = new SqlParameter("@AgileVehicleId", vehicleIncentivesModel.AgileVehicleId);
                SqlParameter PARAMVIN = new SqlParameter("@Vin", vehicleIncentivesModel.Vin);
                SqlParameter PARAMDEALERID = new SqlParameter("@DealerID", vehicleIncentivesModel.DealerId);
                SqlParameter PARAMPROGRAMID = new SqlParameter("@ProgramId", vehicleIncentivesModel.ProgramID);

                db.Database.ExecuteSqlCommand("exec UpdateVehicleIncentive @IncentivetranslatedId,@Id,@IsActive,@AgileVehicleId,@Vin,@DealerID,@ProgramId",
                    PARAMINCENTIVETRANSLATEDID, PARAMID, PARAMISACTIVE, PARAMAGILEVEHICLEID, PARAMVIN, PARAMDEALERID, PARAMPROGRAMID);
                db.SaveChanges();
            }

        }

        public static IncentiveTranslated GetExcludeVinsOfIncentive(int IncentiveTranslatedId)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                return db.IncentiveTranslateds.Where(x => x.IncentiveTranslatedId.Equals(IncentiveTranslatedId)).FirstOrDefault();
            }
        }

        /// <summary>
        ///  Get Missing Incentive Program.
        /// </summary>
        /// <returns>
        /// List of Programms.
        /// </returns>
        public static List<MissingIncentiveProgram> GetMissingIncentivePrograms()
        {
            List<MissingIncentiveProgram> result = new List<MissingIncentiveProgram>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                result = db.MissingIncentivePrograms.Where(p => p.IsActive == true).ToList();
            }
            return result;
        }

        /// <summary>
        /// Delete the Missing Incentive Program.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static async Task<bool> DeleteMissingIncentiveProgram(int Id)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter idParam = new SqlParameter("@Id", Id);
                await db.Database.ExecuteSqlCommandAsync("exec [usp_MissingIncentivePrograms_Delete] @Id", idParam);
                db.SaveChanges();
            }
            return true;
        }

        /// <summary>
        /// Add Missing Incentive Programms in the Database.
        /// </summary>
        /// <param name="ProgramNumber"></param>
        /// <param name="createdID"></param>
        /// <returns></returns>
        public static async Task<bool> AddMissingIncentiveProgramAsync(string programNumber, string createdID)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                SqlParameter programNumberParam = new SqlParameter("@ProgramNumber", programNumber);
                SqlParameter createdIdParam = new SqlParameter("@CreatedId", createdID);
                await db.Database.ExecuteSqlCommandAsync("exec [usp_MissingIncentivePrograms_Insert] @ProgramNumber, @CreatedId",
                    programNumberParam, createdIdParam);
                db.SaveChanges();
            }
            return true;
        }
    }
}