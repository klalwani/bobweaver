﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Dashboards;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class UserAccountHelper
    {
        public static List<UserAccountDashboard> GetAllUsers()
        {
            List<UserAccountDashboard> vs;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {

                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                vs = (from user in userManager.Users
                      select new UserAccountDashboard
                      {
                          UserId = user.Id,
                          UserName = user.UserName,
                          Email = user.Email,
                          EmailConfirmed = user.EmailConfirmed ? "Yes" : "Not Yet",
                          PhoneNumber = user.PhoneNumber,
                      }).ToList();
                if (vs.Any())
                {
                    foreach (UserAccountDashboard user in vs)
                    {
                        string roleId = userManager.FindByName(user.UserName).Roles.Select(x => x.RoleId).FirstOrDefault() ?? string.Empty;

                        IdentityRole role = roleManager.FindById(roleId);
                        user.CurrentRole = role != null ? role.Name : "Not Assigned";
                    }
                }
            }
            return vs;
        }

        public static ApplicationUser GetUserById(string userId)
        {
            ApplicationUser user;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                user = userManager.FindById(userId);
            }
            return user;
        }
    }
}