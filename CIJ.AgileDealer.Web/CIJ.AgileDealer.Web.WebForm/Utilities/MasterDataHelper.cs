﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using AjaxControlToolkit;
using CIJ.AgileDealer.Core;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Configuration;
using CIJ.AgileDealer.Web.Base.Helpers;
using AgileDealer.Data.Entities.Localization;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public class MasterDataHelper
    {
        #region static member
        static string defaultVpcImageUrl = null;
        #endregion

        #region static constructor
        static MasterDataHelper()
        {
            // "/content/images/black-white-auto-mobile.jpg";
            defaultVpcImageUrl = ConfigurationManager.AppSettings["DefaultVcpImageUrl"];//
        }
        #endregion

        public static Header GetHeader(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Headers.FirstOrDefault(x => x.DealerID == dealerId);
            }
        }
        public static Footer GetFooter(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Footers.FirstOrDefault(x => x.DealerID == dealerId);
            }
        }

        public static List<HoursModel> GetHours(int dealerId, int languageId, int hourtypeId)
        {
            List<HoursModel> result = new List<HoursModel>();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                var query = from hour in db.Hours
                            join hour_Trans in db.Hour_Translations.Where(c => c.LanguageId == languageId)
                                on new { hrsid = hour.HoursID, hrsTypeId = hour.HourTypeID }
                                    equals new { hrsid = hour_Trans.HoursId, hrsTypeId = hour_Trans.HourTypeID } into tbl_Trans

                            from tbl in tbl_Trans.DefaultIfEmpty()
                            orderby hour.Order
                            where hour.HourTypeID == hourtypeId && hour.DealerID == dealerId
                            select new HoursModel
                            {
                                ShortDayName = tbl.ShortName ?? hour.ShortDayName,
                                Time = (tbl.OperationStatus ?? hour.OperationStatus) ?? hour.StartTime + " - " + hour.EndTime,
                                HourTypeID = hour.HourTypeID,
                                StartTime = hour.StartTime,
                                EndTime = hour.EndTime,
                            };

                result = query.ToList();

                return result;
            }
        }

        public static List<PeriodTypeModel> GetPeridTypes(int languageId, int dealerId)
        {
            List<PeriodTypeModel> periodTypes = new List<PeriodTypeModel>();

            using (var db = new ServicePageDbContext())
            {
                var query = from periodType in db.PeriodTypes
                            join periodType_Tran in db.PeriodType_Trans.Where(c => c.LanguageId == languageId)
                            on periodType.PeriodTypeId equals periodType_Tran.PeriodTypeId
                            into tbl_Trans
                            where periodType.DealerID == dealerId
                            from tbl in tbl_Trans.DefaultIfEmpty()
                            select new PeriodTypeModel
                            {
                                PeriodTypeName = tbl.PeriodTypeName ?? periodType.PeriodTypeName,
                                TermInMonths = periodType.TermInMonths
                            };
                periodTypes = query.ToList();
            }
            return periodTypes;
        }
        public static List<MasterData> GetYearList()
        {
            List<MasterData> vs = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                vs = (from av in db.AgileVehicles
                      select new MasterData { Id = av.Year, Name = av.Year.ToString() }).Distinct().OrderByDescending(v => v.Id).ToList();
            }
            return vs;
        }
        public static List<MasterData> GetPrices()
        {
            List<MasterData> prices = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                prices.AddRange(
                    db.Prices.Where(x => x.Name != string.Empty)
                        .Select(
                            item => new MasterData { Id = item.PriceId, Name = item.Name, From = item.From, To = item.To }));
            }
            return prices;
        }

        public static List<MasterData> GetModels(int dealerId)
        {
            List<MasterData> models = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                models.AddRange(db.Models.Where(x => x.Name != string.Empty).Select(item => new MasterData { Id = item.ModelId, Name = item.Name }));
            }
            return models;
        }

        public static Price GetPricesByRange(decimal minValue, decimal maxValue)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Prices.FirstOrDefault(x => x.Name != string.Empty && x.From == minValue && x.To == maxValue);
            }
        }


        public static List<MasterData> GetModelTranslated(List<int> modelIds)
        {
            List<MasterData> models = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                models = db.ModelTranslateds.Where(
                            x => x.Name != string.Empty &&
                                (!modelIds.Any() || modelIds.Contains(x.ModelTranslatedId)))
                        .Select(item => new MasterData { Id = item.ModelTranslatedId, Name = item.Name }).ToList();
            }
            return models;
        }

        public static List<MasterData> GetModelTranslatedWithSubList(List<int> modelIds)
        {
            List<MasterData> result = new List<MasterData>();

            List<MasterData> modelTranslateds = GetModelTranslated(modelIds);

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<Model> models = (from model in db.Models
                                      join av in db.AgileVehicles on model.ModelId equals av.ModelId
                                      where av.IsSold == false
                                      select model).ToList();

                List<VehicleModelTrim> modelTrims = db.VehicleModelTrims.ToList();

                List<Trim> trims = (from t in db.Trims
                                    join av in db.AgileVehicles on t.TrimId equals av.TrimId
                                    where av.IsSold == false
                                    select t).Distinct().ToList();

                foreach (Model model in models)
                {
                    if (model.ModelTranslatedId.HasValue)
                    {
                        MasterData modelTranslated = modelTranslateds.FirstOrDefault(x => x.Id == model.ModelTranslatedId);
                        if (modelTranslated != null)
                        {
                            List<int> trimIds =
                                modelTrims.Where(x => x.modelId == model.ModelId).Select(a => a.trimId).ToList();
                            MasterData newItem = new MasterData()
                            {
                                Id = modelTranslated.Id,
                                Name = modelTranslated.Name,
                                SubList = (from trim in trims
                                           where trimIds.Contains(trim.TrimId)
                                           orderby trim.Order ascending
                                           select new MasterData()
                                           {
                                               SubListId = trim.TrimId,
                                               SubListName = trim.Name

                                           }).ToList()
                            };
                            newItem.HasSubList = newItem.SubList.Any();
                            if (!result.Any(x => x.Id == newItem.Id))
                                result.Add(newItem);
                        }
                    }
                }

                result.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
            }
            return result;
        }

        public static List<MasterData> GetEngineListWithSubList()
        {
            List<MasterData> engineTranslateds = GetEngineType();
            List<Engine> engines = GetEngines();
            int engineTranslatedCount = engineTranslateds.Count;
            for (int i = 0; i < engineTranslatedCount; i++)
            {
                MasterData engineTypeTranslated = engineTranslateds[i];
                engineTypeTranslated.SubList = (from engine in engines
                                                where engine.EngineTypeTranslatedId == engineTypeTranslated.Id
                                                select new MasterData
                                                {
                                                    SubListId = engine.EngineId,
                                                    SubListName = engine.Name
                                                }).ToList();
                engineTypeTranslated.HasSubList = engineTypeTranslated.SubList.Any();
            }

            return engineTranslateds;
        }

        public static List<ModelTranslated> GetModelTranslatedsByModels(List<int> modelTranslatedIds, int dealerId)
        {
            List<ModelTranslated> modeltranslateds;
            using (var db = new ServicePageDbContext())
            {
                modeltranslateds = (from mt in db.ModelTranslateds where modelTranslatedIds.Contains(mt.ModelTranslatedId) select mt).ToList();
            }
            return modeltranslateds;
        }

        public static Model GetModelById(int id, int dealerId)
        {
            Model model = new Model();
            using (var db = new ServicePageDbContext())
            {
                model = db.Models.FirstOrDefault(x => x.Name != string.Empty && x.ModelId == id);
            }
            return model;
        }

        public static Model GetModelByName(string name, int dealerId)
        {
            Model model = new Model();
            using (var db = new ServicePageDbContext())
            {
                model = db.Models.FirstOrDefault(x => x.Name != string.Empty && x.Name.ToLower() == name.ToLower());
            }
            return model;
        }

        public static Model GetModelByModelTranslatedName(string name, int dealerId)
        {
            Model model = new Model();
            using (var db = new ServicePageDbContext())
            {
                model = db.Models.Include(a => a.ModelTranslated).FirstOrDefault(x => x.Name != string.Empty && ((x.ModelTranslated.Name.ToLower() == name.ToLower()) || (x.ModelTranslated.TranslatedLinkName.ToLower() == name.ToLower())));
            }
            return model;
        }

        public static List<string> GetModelNames(int dealerId)
        {
            List<string> modelNames = new List<string>();
            using (var db = new ServicePageDbContext())
            {
                modelNames = db.Models.Where(a => !string.IsNullOrEmpty(a.Name)).Select(x => x.Name).ToList();
            }
            return modelNames;
        }

        public static List<string> GetModelTranslatedNames(int dealerId)
        {
            List<string> modelTranslatedNames = new List<string>();
            using (var db = new ServicePageDbContext())
            {
                modelTranslatedNames = db.ModelTranslateds.Where(a => !string.IsNullOrEmpty(a.Name)).Select(x => x.Name).ToList();
            }
            return modelTranslatedNames;
        }

        public static Manufacture GetManufactureById(int id, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Manufactures.FirstOrDefault(x => x.ManufactureID == id);
            }
        }

        public static List<Model> GetModelByListName(List<string> names, int dealerId)
        {
            List<Model> model = new List<Model>();
            using (var db = new ServicePageDbContext())
            {
                model = db.Models.Where(x => names.Contains(x.Name)).ToList();
            }
            return model;
        }

        public static ModelTranslated GetModelByTranslatedName(string name, int dealerId)
        {
            ModelTranslated model = new ModelTranslated();
            using (var db = new ServicePageDbContext())
            {
                model = db.ModelTranslateds.FirstOrDefault(x => x.Name != string.Empty && ((x.Name.ToLower() == name.ToLower()) || (x.TranslatedLinkName.ToLower() == name.ToLower())));
            }
            return model;
        }

        public static List<MasterData> GetBodyStyleType(List<int> bodyStyleLists)
        {
            List<MasterData> bodyTypeStyles = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                bodyTypeStyles =
                    db.BodyStyleTypes.Where(
                            x =>
                                x.Name != string.Empty &&
                                (!bodyStyleLists.Any() || bodyStyleLists.Contains(x.BodyStyleTypeId)))
                        .Select(item => new MasterData { Id = item.BodyStyleTypeId, Name = item.Name })
                        .ToList();
            }
            return bodyTypeStyles;
        }

        public static List<MasterData> GetVehicleStandardBodyStyles(List<int> vehicleStandardBodyList)
        {
            List<MasterData> vehicleStandardBodies = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                vehicleStandardBodies =
                    db.VehicleStandardBodies.Where(
                            x =>
                                x.Name != string.Empty &&
                                (!vehicleStandardBodyList.Any() || vehicleStandardBodyList.Contains(x.VehicleStandardBodyId)))
                        .Select(item => new MasterData { Id = item.VehicleStandardBodyId, Name = item.Name })
                        .ToList();
            }
            return vehicleStandardBodies;
        }

        public static List<MasterData> GetVehicleStandardBodyStyle(List<int> vehicleStandardBodyList)
        {
            List<MasterData> result = new List<MasterData>();

            List<MasterData> vehicleStandardBodyStyles = GetVehicleStandardBodyStyles(vehicleStandardBodyList);

            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                foreach (MasterData standardBody in vehicleStandardBodyStyles)
                {
                    if (standardBody != null)
                    {
                        standardBody.SubList = (from bodystyleType in db.BodyStyleTypes
                            join vehicleStandardBody in db.VehicleStandardBodies on bodystyleType.VehicleStandardBodyId
                            equals vehicleStandardBody.VehicleStandardBodyId
                            where vehicleStandardBody.VehicleStandardBodyId == standardBody.Id
                            select new MasterData()
                            {
                                SubListId = bodystyleType.BodyStyleTypeId,
                                SubListName = bodystyleType.Name
                            }).ToList();
                        standardBody.HasSubList = standardBody.SubList.Any();
                        if (standardBody.Name.Equals("Other", StringComparison.OrdinalIgnoreCase))
                            standardBody.HasSubList = false;
                        if (!result.Any(x => x.Id == standardBody.Id))
                            result.Add(standardBody);
                    }
                }

                result.OrderBy(x=>x.Id);
            }
            return result;
        }

        public static List<MasterData> GetExteriorColor()
        {
            List<MasterData> exteriorColors = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                List<string> translatedParentStrings = db.ExteriorColorTranslateds.Where(x => x.Name != string.Empty).Select(y => y.Description).Distinct().ToList();

                exteriorColors.AddRange(db.ExteriorColorTranslateds.Where(x => translatedParentStrings.Contains(x.Name) && x.Name != "Other").Select(item => new MasterData { Id = item.ExteriorColorTranslatedId, Name = item.Name }).OrderByDescending(x => x.Id));
                exteriorColors.AddRange(db.ExteriorColorTranslateds.Where(x => translatedParentStrings.Contains(x.Name) && x.Name == "Other").Select(item => new MasterData { Id = item.ExteriorColorTranslatedId, Name = item.Name }));
            }
            return exteriorColors;
        }
        public static List<MasterData> GetTransmission()
        {
            List<MasterData> transmissions = new List<MasterData>();
            transmissions.Add(new MasterData { Id = 0, Name = "Manual" });
            transmissions.Add(new MasterData { Id = 1, Name = "Automatic" });
            return transmissions;
        }
        public static List<MasterData> GetFuelType()
        {
            List<MasterData> fuelTypes = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                fuelTypes.AddRange(db.FuelTypes.Where(x => x.Name != string.Empty).Select(item => new MasterData { Id = item.FuelTypeId, Name = item.Name }));
            }
            return fuelTypes;
        }

        public static List<MasterData> GetEngineType()
        {
            List<MasterData> engines = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                engines.AddRange(db.EngineTypeTranslateds.Where(x => x.Name != string.Empty).Select(item => new MasterData { Id = item.EngineTypeTranslatedId, Name = item.Name }));
            }
            return engines;
        }

        public static List<Engine> GetEngines()
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Engines.ToList();
            }
        }

        public static List<MasterData> GetDrive()
        {
            List<MasterData> drives = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                drives.AddRange(db.Drives.Where(x => x.Name != string.Empty).Select(item => new MasterData { Id = item.DriveId, Name = item.Name }));
            }
            return drives;
        }

        public static List<MasterData> Getmileage()
        {
            List<MasterData> mileages = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                mileages.AddRange(db.Mileages.Where(x => x.Name != string.Empty).Select(item => new MasterData { Id = item.MileageId, Name = item.Name, From = item.From, To = item.To }));
            }
            return mileages;
        }

        public static List<Drive> GetDriveList()
        {
            List<Drive> drives = new List<Drive>();
            using (var db = new ServicePageDbContext())
            {
               drives = db.Drives.Where(x => x.Name != string.Empty).ToList();
            }
            return drives;
        }

        public static List<VehicleFeatureMaster> GetVehicleFeatureMasterList()
        {
            List<VehicleFeatureMaster> vehicleFeatures = new List<VehicleFeatureMaster>();
            using (var db = new ServicePageDbContext())
            {
                vehicleFeatures = db.VehicleFeatureMaster.Where(x => x.LabelOverride != string.Empty).ToList();
            }
            return vehicleFeatures;
        }


        public static List<MasterData> GetVehicleFeatur()
        {
            List<MasterData> vehiclefeatures = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                vehiclefeatures.AddRange(db.VehicleFeatureMaster.Where(x => x.LabelOverride != string.Empty).Select(item => new MasterData { Id = item.FeatureId, Name = item.LabelOverride }));
            }
            return vehiclefeatures;

        }

        public static List<MasterData> GetVehicleFeaturMasters(List<VehicleData> lstSearchedVehicles)
        {
            List<MasterData> vs = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                var vehicleFeaturMasters = db.VehicleFeatureMaster.Where(x => x.IsSearchable).ToList();
                List<VehicleData> vehicleDataList = new List<VehicleData>();

                foreach (var item in vehicleFeaturMasters)
                {
                    vehicleDataList = new List<VehicleData>();
                    vehicleDataList = (from av in lstSearchedVehicles
                                       join vf in db.VehicleFeatures
                                      on av.AgileVehicleId equals vf.AgileVehicleId
                                       where vf.MasterFeatureId.Equals(item.FeatureId)
                                       && vf.IsSearchable
                                       select new VehicleData
                                       {
                                           AgileVehicleId = av.AgileVehicleId
                                       }).ToList();

                    vs.Add(new MasterData { Id = item.FeatureId, Name = item.LabelOverride, Count = vehicleDataList.Count() });

                }
                return vs;
            }
        }

        public static List<MasterData> GetVehicleFeaturMasters(VehicleSearchCriteria vehicleSearchCriteria)
        {
            List<MasterData> vs = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                var vehicleFeaturMasters = db.VehicleFeatureMaster.Where(x => x.IsSearchable).ToList();
                List<VehicleData> vehicleDataList = new List<VehicleData>();

                foreach (var item in vehicleFeaturMasters)
                {
                    vehicleDataList = new List<VehicleData>();
                    if (vehicleSearchCriteria.IsNew)
                    {
                        vehicleDataList = (from av in db.AgileVehicles.Where(x =>
                                              x.IsSold == false && !string.IsNullOrEmpty(x.MainImageUrl) && !x.IsHidden)
                                           join vf in db.VehicleFeatures
                                           on av.AgileVehicleId equals vf.AgileVehicleId
                                           where vf.MasterFeatureId.Equals(item.FeatureId) && av.IsNew == true && !av.IsSold
                                           && vf.IsSearchable
                                           select new VehicleData
                                           {
                                               AgileVehicleId = av.AgileVehicleId
                                           }).ToList();

                    }
                    else if (vehicleSearchCriteria.IsCertified)
                    {
                        vehicleDataList = (from av in db.AgileVehicles.Where(x =>
                                              x.IsSold == false && !string.IsNullOrEmpty(x.MainImageUrl) && !x.IsHidden)
                                           join vf in db.VehicleFeatures
                                           on av.AgileVehicleId equals vf.AgileVehicleId
                                           where vf.MasterFeatureId.Equals(item.FeatureId) && av.IsCertified == true && !av.IsSold
                                           && vf.IsSearchable
                                           select new VehicleData
                                           {
                                               AgileVehicleId = av.AgileVehicleId
                                           }).ToList();

                    }
                    else if (vehicleSearchCriteria.IsUsed)
                    {
                        vehicleDataList = (from av in db.AgileVehicles.Where(x =>
                                              x.IsSold == false && !string.IsNullOrEmpty(x.MainImageUrl) && !x.IsHidden)
                                           join vf in db.VehicleFeatures
                                           on av.AgileVehicleId equals vf.AgileVehicleId
                                           where vf.MasterFeatureId.Equals(item.FeatureId) && !av.IsNew && !av.IsSold
                                           && vf.IsSearchable
                                           select new VehicleData
                                           {
                                               AgileVehicleId = av.AgileVehicleId
                                           }).ToList();

                    }
                    vs.Add(new MasterData { Id = item.FeatureId, Name = item.LabelOverride, Count = vehicleDataList.Count() });

                }
                return vs;
            }
        }

        public static List<Mileage> GetMileageList()
        {
            List<Mileage> mileages = new List<Mileage>();
            using (var db = new ServicePageDbContext())
            {
                mileages = db.Mileages.Where(x => x.Name != string.Empty).ToList();
            }
            return mileages;
        }

        public static List<Price> GetPriceList()
        {
            List<Price> prices = new List<Price>();
            using (var db = new ServicePageDbContext())
            {
                prices = db.Prices.Where(x => x.Name != string.Empty).ToList();
            }
            return prices;
        }

        public static List<MasterData> GetMakes()
        {
            List<MasterData> vs = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                var makes = db.Makes.Select(x => x.MakeTranslatedId).Distinct().ToList();
                vs.AddRange(db.MakeTranslateds.Where(x => x.Name != string.Empty && makes.Contains(x.MakeTranslatedId)).Select(item => new MasterData { Id = item.MakeTranslatedId, Name = item.Name }));
            }

            return vs;
        }

        public static Make GetMakeByName(string name, int dealerId)
        {
            Make make = new Make();
            using (var db = new ServicePageDbContext())
            {
                make = db.Makes.FirstOrDefault(x => x.Name != string.Empty && x.Name.ToLower() == name.ToLower());
            }
            return make;
        }

        public static Make GetMakeTranslatedByName(string name, int dealerId)
        {
            Make make = new Make();
            using (var db = new ServicePageDbContext())
            {
                make = db.Makes.FirstOrDefault(x => x.Name != string.Empty && x.Name.ToLower() == name.ToLower());
            }
            return make;
        }

        public static List<NotificationModel> GetNotifications(int pageIndex, int languageId, int dealerId)
        {
            List<NotificationModel> vs = new List<NotificationModel>();
            using (var db = new ServicePageDbContext())
            {
                var query = from notification in db.Notifications.Where(x => x.PageIndex == pageIndex && x.DealerID == dealerId)
                            join notification_Tran in db.Notification_Trans.Where(c => c.LanguageId == languageId && c.DealerID == dealerId)
                            on notification.NotificationId equals notification_Tran.NotificationId
                            into tblTrans
                            from tbl in tblTrans.DefaultIfEmpty()
                            orderby notification.NotificationId
                            select new NotificationModel
                            {
                                Class = notification.Class,
                                Glyphicon = notification.Glyphicon,
                                Content = tbl.Content ?? notification.Content,
                                Title = tbl.Title ?? notification.Title,
                            };
                vs = query.ToList();
            }
            return vs;
        }

        public static List<State> GetStates(int dealerId)
        {
            List<State> vs = new List<State>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.States.Where(x => x.DealerID == dealerId).OrderBy(y => y.StateId).ToList());
            }

            return vs;
        }

        public static FordVehicleContent GetFordVehicleContentByTypeAndModel(string type, string model, int dealerId)
        {
            FordVehicleContent vs;
            using (var db = new ServicePageDbContext())
            {
                vs = db.FordVehicleContents.FirstOrDefault(x => x.Type == type && x.Model == model);
            }
            return vs;
        }

        public static List<ResidenceStatus> GetResidenceStatus(int dealerId)
        {
            List<ResidenceStatus> vs = new List<ResidenceStatus>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.ResidenceStatuses.Where(x => x.DealerID == dealerId).OrderBy(y => y.ResidenceStatusId).ToList());
            }

            return vs;
        }

        public static ResidenceStatus GetResidenceStatusById(int resId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.ResidenceStatuses.FirstOrDefault(x => x.ResidenceStatusId == resId);
            }
        }

        public static List<Year> GetYears(int dealerId)
        {
            List<Year> vs = new List<Year>();

            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.Years.OrderByDescending(x => x.YearId).ToList());
            }
            return vs;
        }

        public static List<Date> GetDates(int dealerId)
        {
            List<Date> vs = new List<Date>();

            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.Dates.ToList());
            }
            return vs;
        }

        public static Dealer GetDefaultDealer(int _dealerId)
        {
            Dealer dealer = new Dealer();
            using (var db = new ServicePageDbContext())
            {
                dealer = db.Dealers.FirstOrDefault(x => x.DealerID == _dealerId);
            }
            return dealer;
        }

        public static List<MonthModel> GetMonths(int languageId, int dealerId)
        {
            List<MonthModel> vs = new List<MonthModel>();
            using (var db = new ServicePageDbContext())
            {
                var query = from month in db.Months
                            join month_Tran in db.Month_Trans.Where(c => c.LanguageId == languageId)
                            on month.MonthId equals month_Tran.MonthId into tblTrans
                            from tbl in tblTrans.DefaultIfEmpty()
                            select new MonthModel()
                            {
                                IsDisabled = month.IsDisabled,
                                MonthId = month.MonthId,
                                Name = tbl.Name ?? month.Name
                            };
                vs = query.ToList();
            }
            return vs;
        }

        public static List<ScheduleTime> GetScheduleTime(bool isSaturday, int dealerId)
        {
            List<ScheduleTime> vs = new List<ScheduleTime>();

            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.ScheduleTimes.Where(x => x.IsSaturday == isSaturday).ToList());
            }
            return vs;
        }
        public static List<ServiceRequestedModel> GetServiceRequested(int languageId, int dealerId)
        {
            List<ServiceRequestedModel> vs = new List<ServiceRequestedModel>();

            using (var db = new ServicePageDbContext())
            {
                var query = from serviceRequest in db.ServiceRequesteds
                            join serviceRequest_Tran in db.ServiceRequested_Trans.Where(c => c.LanguageId == languageId)
                            on serviceRequest.ServiceRequestedId equals serviceRequest_Tran.ServiceRequestedId into tbl_Trans
                            from tbl in tbl_Trans.DefaultIfEmpty()
                            select new ServiceRequestedModel
                            {
                                ServiceRequestedId = serviceRequest.ServiceRequestedId,
                                Name = tbl.Name ?? serviceRequest.Name
                            };
                vs = query.ToList();
            }
            return vs;
        }
        public static List<CurrentIssueModel> GetCurrentIssue(int languageId, int dealerId)
        {
            List<CurrentIssueModel> vs = new List<CurrentIssueModel>();

            using (var db = new ServicePageDbContext())
            {
                var query = from currentIssues in db.CurrentIssues
                            join currentIssue_Tran in db.CurrentIssue_Trans.Where(c => c.LanguageId == languageId)
                            on currentIssues.CurrentIssueId equals currentIssue_Tran.CurrentIssueId into tbl_Tran
                            from tbl in tbl_Tran.DefaultIfEmpty()
                            select new CurrentIssueModel
                            {
                                CurrentIssueId = currentIssues.CurrentIssueId,
                                Name = tbl.Name ?? currentIssues.Name
                            };
                vs = query.ToList();
            }
            return vs;
        }

        public static List<BlogTag> GetBlogTags(int dealerId, bool isTakeAll = false)
        {
            List<BlogTag> vs = new List<BlogTag>();
            using (var db = new ServicePageDbContext())
            {
                if (isTakeAll)
                {
                    vs.AddRange(db.BlogTags.Where(x => x.DealerID == dealerId && !string.IsNullOrEmpty(x.TagText)).ToList());
                }
                else
                {
                    var selectBasictags = (from blog in db.BlogTags
                                           join entry_tag in db.BlogEntries_BlogTags on blog.BlogTagId equals entry_tag.BlogTagId
                                           where blog.IsBasicTag
                                           select blog.BlogTagId
                    ).Distinct().AsQueryable();

                    var tempBasic = (from tag in db.BlogTags
                                     where tag.DealerID == dealerId
                                           && !string.IsNullOrEmpty(tag.TagText)
                                           && (selectBasictags.Any() && selectBasictags.Contains(tag.BlogTagId)
                                           )
                                     orderby tag.Order
                                     select tag
                    ).AsQueryable();

                    foreach (BlogTag tag in tempBasic)
                    {
                        if (vs.All(x => x.BlogTagId != tag.BlogTagId))
                        {
                            vs.Add(tag);
                        }
                    }


                    var makes = (from make in db.Makes
                                 join tag in db.BlogTags on make.Name equals tag.TagText
                                 select tag).AsQueryable();

                    var models = (from model in db.Models
                                  join tag in db.BlogTags on model.Name equals tag.TagText
                                  select tag).AsQueryable();

                    //var selectedBlogTags = db.BlogEntries_BlogTags.Select(x => x.BlogTagId).Distinct();

                    var selectedMakes = (from tag in db.BlogTags
                                         join entry_tag in db.BlogEntries_BlogTags on tag.BlogTagId equals entry_tag.BlogTagId
                                         where makes.Any(x => x.BlogTagId == tag.BlogTagId)
                                         orderby tag.TagText
                                         select tag.BlogTagId).Distinct().AsQueryable();

                    var tempMakes = from tag in db.BlogTags
                                    where tag.DealerID == dealerId
                                          && !string.IsNullOrEmpty(tag.TagText)
                                          && (selectedMakes.Any() && selectedMakes.Contains(tag.BlogTagId))
                                    select tag;
                    foreach (BlogTag tag in tempMakes)
                    {
                        if (vs.All(x => x.BlogTagId != tag.BlogTagId))
                        {
                            vs.Add(tag);
                        }
                    }

                    var selectedModels = (from tag in db.BlogTags
                                          join entry_tag in db.BlogEntries_BlogTags on tag.BlogTagId equals entry_tag.BlogTagId
                                          where models.Any(x => x.BlogTagId == tag.BlogTagId)
                                          orderby tag.TagText
                                          select tag.BlogTagId).Distinct().AsQueryable();

                    var tempModels = from tag in db.BlogTags
                                     where tag.DealerID == dealerId
                                           && !string.IsNullOrEmpty(tag.TagText)
                                           && (selectedModels.Any() && selectedModels.Contains(tag.BlogTagId))
                                     select tag;
                    foreach (BlogTag tag in tempModels)
                    {
                        if (vs.All(x => x.BlogTagId != tag.BlogTagId))
                        {
                            vs.Add(tag);
                        }
                    }

                }
            }
            return vs.ToList();
        }

        public static List<BlogTag> GetBlogTagByName(string name, int dealerId)
        {
            List<BlogTag> vs = new List<BlogTag>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogTags.Where(x => name.Contains(x.TagText) && x.DealerID == dealerId).ToList());
            }
            BlogTag ExactMatchingBlogTag = new BlogTag();
            if (vs != null)
            {
                ExactMatchingBlogTag = vs.Where(x => x.TagText.Equals(name,StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (ExactMatchingBlogTag != null)
                {
                    vs.Clear();
                    vs.Add(ExactMatchingBlogTag);
                }
                    
            }
            return vs;
        }

        public static List<BlogTag> GetBlogTagsByTagIds(List<int> tagids, int dealerId)
        {
            List<BlogTag> vs = new List<BlogTag>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogTags.Where(x => tagids.Contains(x.BlogTagId) && x.DealerID == dealerId).ToList());
            }
            return vs;
        }

        public static List<BlogTag> GetBlogTagById(int id, int dealerId)
        {
            List<BlogTag> vs = new List<BlogTag>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogTags.Where(x => x.BlogTagId == id && x.DealerID == dealerId).ToList());
            }
            return vs;
        }

        public static BlogEntry GetBlogEntryById(int blogId, int languageId, int dealerId)
        {
            if (blogId > 0) // safeguard
            {
                using (var db = new ServicePageDbContext())
                {
                    return (from blog in db.BlogEntrys
                            join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId && x.DealerID == dealerId)
                            on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                            from blog_Tran in blogTranJoin.DefaultIfEmpty()
                            where blog.BlogEntryId == blogId
                            select new BlogEntry()
                            {
                                PostTitle = blog_Tran.PostTitle ?? blog.PostTitle,
                                PostText = blog_Tran.PostText ?? blog.PostText,
                                PostFeatureImage = blog.PostFeatureImage,
                                Button1Text = blog_Tran.Button1Text ?? blog.Button1Text,
                                Button2Text = blog_Tran.Button2Text ?? blog.Button2Text,
                            }).FirstOrDefault();
                }
            }

            return null;
        }

        public static string GetVcpImageUrl(int blogEntryId, string type, string make, string model, string dealerVcpImageStockServer, int dealerId)
        {
            string imgUrl = null;

            if (blogEntryId > 0)// safeguard - lets check if we have the image from the blog
            {
                using (var db = new ServicePageDbContext())
                {
                    var blog = db.BlogEntrys.FirstOrDefault(b => b.BlogEntryId == blogEntryId && b.DealerID == dealerId);
                    imgUrl = (blog != null) ? !string.IsNullOrEmpty(blog.OverrideImage) ? blog.OverrideImage : blog.PostFeatureImage : null;
                }
            }

            // if we dont have image for the blog lets try pull the image from server
            if (string.IsNullOrEmpty(imgUrl) && (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(make) && !string.IsNullOrEmpty(model)))
            {
                string makeModelImageUrl = (string.Format("{0}/{1}/{2}/{3}/{3}.jpg", dealerVcpImageStockServer, type, make, model)).ToLower();
                imgUrl = HttpHelper.IsUrlExist(makeModelImageUrl) ? makeModelImageUrl : null;
            }

            return !string.IsNullOrEmpty(imgUrl) ? imgUrl : defaultVpcImageUrl;
        }

        public static List<BlogCategory> GetBlogCategories(int dealerId)
        {
            List<BlogCategory> vs = new List<BlogCategory>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogCategoryies.Where(x => x.DealerID == dealerId).ToList());
            }
            return vs;
        }

        public static List<BlogCategory> GetBlogCategoriesByName(string name, int dealerId)
        {
            List<BlogCategory> vs = new List<BlogCategory>();
            if (!string.IsNullOrEmpty(name))
                name = name.Replace("-", " ");
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogCategoryies.Where(x => x.CategoryText.Contains(name) && x.DealerID == dealerId).ToList());
            }
            return vs;
        }

        public static List<BlogCategory> GetBlogCategoriesById(int id, int dealerId)
        {
            List<BlogCategory> vs = new List<BlogCategory>();
            using (var db = new ServicePageDbContext())
            {
                vs.AddRange(db.BlogCategoryies.Where(x => x.BlogCategoryId == id && x.DealerID == dealerId).ToList());
            }
            return vs;
        }

        public static UploadInformation GetUploadInformation(string method, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.UploadInformations.FirstOrDefault(x => x.MethodName.ToLower() == method.ToLower());
            }
        }

        public static List<Department> GetDepartments(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.Departments.Where(x => x.DealerID == dealerId).ToList();
            }
        }

        public static List<Program> GetProgramsByAgileVehicleId(int agileVehicleId, int dealerId)
        {
            List<Program> programs = new List<Program>();
            if (agileVehicleId > 0)
            {
                using (var db = new ServicePageDbContext())
                {
                    programs = (from ap in db.AgileVehiclesPrograms
                                join p in db.Programs on ap.ProgramId equals p.ProgramId
                                where ap.AgileVehicleId == agileVehicleId && ap.DealerID == dealerId
                                select p).ToList();
                }
            }
            return programs;
        }

        public static List<VehicleCriteriaMapping> GetVehicleCriteriaMappings(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                return db.VehicleCriteriaMappings.ToList();
            }
        }

        public static List<MasterData> GetVinList()
        {
            List<MasterData> vs = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                vs = (from av in db.AgileVehicles.Where(x => x.IsSold == false)
                      select new MasterData { Name = av.Vin }).ToList();
            }
            return vs;
        }

        public static List<MasterData> GetBodyStyleList()
        {
            List<MasterData> vs = new List<MasterData>();
            using (var db = new ServicePageDbContext())
            {
                vs = (from av in db.BodyStyles
                      select new MasterData { Id = av.BodyStyleId, Name = av.Name}).OrderBy(x=>x.Name).ToList();
            }
            return vs;
        }

        public static List<MasterData> GetTrimList()
        {
            List<MasterData> models = new List<MasterData>();

            using (var db = new ServicePageDbContext())
            {
                models = db.Trims.Where(
                            x => x.Name != string.Empty)
                        .OrderBy(x=>x.Name).Select(item => new MasterData { Id = item.TrimId, Name = item.Name }).ToList();
            }
            return models;
        }

        public static List<MasterData> GetEnginesForExclude()
        {
            List<MasterData> engineData = new List<MasterData>();
            List<Engine> engines = GetEngines();
            using (var db = new ServicePageDbContext())
            {
                engineData = (from engine in engines
                              select new MasterData
                              {
                                  Id = engine.EngineId,
                                  Name = engine.Name
                              }).ToList();

            }
            return engineData;
        }

    }
}