﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Parts;
using AgileDealer.Data.Entities.ScheduleService;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class EmailGenerator
    {
        public static Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerInfoHelper.DealerID);

        private static List<NotificationSetting> _notificationSettings;
        public static List<NotificationSetting> NotificationSettings
        {
            get
            {
                _notificationSettings = NotificationHelper.GetNotificationSettings(Dealer.DealerID);

                return _notificationSettings;
            }
            set { _notificationSettings = value; }
        }


        private static string _smtpAcct = ConfigurationManager.AppSettings["smtpAcct"];
        private static readonly string MailFrom = ConfigurationManager.AppSettings["smtpAcct"];
        private static readonly string BccEmail = ConfigurationManager.AppSettings["BCCEmail"];
        public static Email CreateNewEmail(string mailto, string mailSubject, string mailContent, string allLeadEmail = "")
        {
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = MailFrom,
                MailTo = mailto,
                MailBCC = BccEmail,
                MailSubject = mailSubject,
                MailText = mailContent,
            };
            return email;
        }



        #region Content Creator
        public static string CreateMailContentWithTemplateForCustomerQuestion(CustomerQuestion customerQuestion)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + customerQuestion.FirstName + " " + customerQuestion.LastName + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + customerQuestion.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + customerQuestion.PhoneNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Additional Information</td>");
            sb.AppendLine(@"<td>" + customerQuestion.Questions + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }

        public static string CreateMailContentWithTemplateForCheckAvailability(VehicleFeedBack vehicleFeedBack)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + vehicleFeedBack.Name + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + vehicleFeedBack.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + vehicleFeedBack.Phone + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Message</td>");
            sb.AppendLine(@"<td>" + vehicleFeedBack.Message + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Vehicle Vin</td>");
            sb.AppendLine(@"<td>" + vehicleFeedBack.HomeNetVehicle.vin + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }

        public static string CreateMailContentWithTemplateForScheduleService(ScheduleService scheduleService)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + scheduleService.FirstName + " " + scheduleService.LastName + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Appointment Date</td>");
            sb.AppendLine(@"<td>" +
                          $"{scheduleService.ScheduleDate:D} {(string.IsNullOrEmpty(scheduleService.SaturdayHour) ? scheduleService.WeeklyHour : scheduleService.SaturdayHour)}" + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + scheduleService.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + scheduleService.PhoneNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Additional Information</td>");
            sb.AppendLine(@"<td>" + scheduleService.AdditionalInformation + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Vehicle Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Year</td>");
            sb.AppendLine(@"<td>" + scheduleService.Year + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Make</td>");
            sb.AppendLine(@"<td>" + scheduleService.Make + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Model</td>");
            sb.AppendLine(@"<td>" + scheduleService.Model + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }

        public static string CreateMailContentWithTemplateForFinanceAdf(ContactStructure applicant)
        {
            ADF adf = AdfHelper.CreateAdfInformationContact(applicant.ContactInfo, applicant.ApplicantInfo, Dealer);
            string mailContent = XmlHelper.Serialize(adf);

            return mailContent;
        }
        public static string CreateMailContentWithTemplateForFinance(ContactStructure applicant)
        {
            StringBuilder sb = new StringBuilder();
            if (applicant.ApplicantInfo != null)
            {
                sb.Append(@"<table class=""body-wrap"">");
                sb.AppendLine(@"<tbody>");
                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Customer Name</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.FirstName + " " + applicant.ApplicantInfo.LastName + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Email</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.Email + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Phone Number</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.PhoneNumber + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td><strong>Address Info</strong></td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Address</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.CurrentAddress + "</td>");
                sb.AppendLine(@"</tr>");


                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>City</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.City + "</td>");
                sb.AppendLine(@"</tr>");


                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>State</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.State + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"</tbody>");
                sb.AppendLine(@"</table>");
            }
            return sb.ToString();
        }

        public static string CreateMailContentWithTemplateForPart(PartRequest partRequest)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + partRequest.FirstName + " " + partRequest.LastName + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Contact me by</td>");
            sb.AppendLine(@"<td>" + partRequest.ContactBy + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + partRequest.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + partRequest.PhoneNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Postal Code</td>");
            sb.AppendLine(@"<td>" + partRequest.PostalCode + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Vehicle Information</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Year</td>");
            sb.AppendLine(@"<td>" + partRequest.Year + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Make</td>");
            sb.AppendLine(@"<td>" + partRequest.Make + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Model</td>");
            sb.AppendLine(@"<td>" + partRequest.Model + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Vin</td>");
            sb.AppendLine(@"<td>" + partRequest.Vin + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Part Number</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Vin</td>");
            sb.AppendLine(@"<td>" + partRequest.PartNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }

        public static string CreateConfirmationMailContent(string mailTo)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"<h2><strong><span style=""color: #000000;"">Thank you for contacting " + Dealer.Name + "&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">Someone will be contacting your shortly.&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">Sincerely,&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">The " + Dealer.Name + " Team</span></strong></h2>");

            return sb.ToString();
        }



        public static string CreateMailContentWithTemplateForCustomerQuestionWithAdfFormat(
            CustomerQuestion customerQuestion)
        {

            ADF adf = AdfHelper.CreateAdfInformation(customerQuestion, Dealer);
            string mailContent = XmlHelper.Serialize(adf);

            return mailContent;
        }
        public static string CreateMailContentWithTemplateForScheduleServiceWithAdfFormat(
            ScheduleService scheduleService)
        {
            ADF adf = AdfHelper.CreateAdfInformation(scheduleService, Dealer);
            string mailContent = XmlHelper.Serialize(adf);

            return mailContent;
        }
        public static string CreateMailContentWithTemplateForCheckAvailabilityWithAdfFormat(
            VehicleFeedBack vehicleFeedBack, HomeNetVehicle vehicle, Dealer dealer)
        {
            ADF adf = AdfHelper.CreateAdfInformation(vehicleFeedBack, vehicle, Dealer);
            string mailContent = XmlHelper.Serialize(adf);

            return mailContent;
        }

        public static string CreateMailContentWithTemplateForPartRequestWithAdfFormat(
            PartRequest partRequest)
        {
            ADF adf = AdfHelper.CreateAdfInformation(partRequest, Dealer);
            string mailContent = XmlHelper.Serialize(adf);

            return mailContent;
        }

        #endregion

        #region Receiver

        #endregion
    }
}