﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Context;


namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class EmployeeHelper
    {
        public static List<Employee> GetAllEmployee(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.Employees.Where(x=>x.DealerID == dealerId).OrderBy(x=>x.IsNew.HasValue).ToList();
            }
        }
    }
}