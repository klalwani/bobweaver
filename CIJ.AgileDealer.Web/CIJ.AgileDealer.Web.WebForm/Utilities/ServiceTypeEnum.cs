﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public enum ServiceTypeEnum
    {
        [Description("Oil-change")]
        OilChange = 1,
        [Description("Brake")]
        Brake = 2,
        [Description("Tire")]
        Tire = 3
    }
}