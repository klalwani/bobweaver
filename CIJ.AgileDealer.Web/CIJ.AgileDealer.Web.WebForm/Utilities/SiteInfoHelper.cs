﻿using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Localization;
using CJI.AgileDealer.Web.Base.Context;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System;
using System.Text;
using CJI.AgileDealer.Web.WebForm.Models;
using CIJ.AgileDealer.Web.Base.Helpers;
using AgileDealer.Data.Entities.Content;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Data;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{

    /// <summary>
    /// Site info cache - contains everything for site.... sitemap, menu, routings, etc
    /// </summary>
    public static class SiteInfoHelper
    {
        public static List<CustomRedirectUrl> RedirectUlrList { get; private set; }

        public static List<Language> Languages { get; private set; }

        public static Location Location { get; private set; }

        public static Footer Footer { get; private set; }

        // Move this to DealerInfoHelper?
        public static Dealer Dealer { get; private set; }

        public static Dictionary<string, PageMetaDataModel> PageMetaData { get; private set; }

        private static readonly Dictionary<int, string> MenuDictionaryDefaultPage = new Dictionary<int, string>();

        private static readonly Dictionary<int, string> MenuDictionary = new Dictionary<int, string>();

        /// <summary>
        /// constructor for singleton
        /// </summary>
        static SiteInfoHelper()
        {
            PopulateSiteMetaData();
        }

        internal static void ResetSiteInfoInstance()
        {
            // TODO: Rebuild SitInfo here
            PopulateSiteMetaData();
        }

        internal static void SetLanguage(Page page, HttpContext context, string langParam, object sessionCurrentUI)
        {
            try
            {
                if (page != null && context != null)
                {
                    if (page.Request != null && page.Request.UserLanguages.Count() > 0)
                    {
                        string defaultLanguage = page.Request.UserLanguages[0];

                        // if there isnt session culture then set it 
                        var selectedLanguage = (!string.IsNullOrEmpty(langParam)) ? langParam : (sessionCurrentUI ?? defaultLanguage);

                        string selectedLanguageStr = selectedLanguage.ToString();// "es-MX";

                        if (Languages != null && Languages.Count > 0 && !Languages.Any(a => a.Name.Equals(selectedLanguageStr, StringComparison.OrdinalIgnoreCase)))
                            selectedLanguageStr = defaultLanguage;
                        else
                            selectedLanguageStr = "en-US";

                        if (Thread.CurrentThread.CurrentUICulture == null
                          || !string.Equals(Thread.CurrentThread.CurrentUICulture.Name, selectedLanguageStr, StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                page.UICulture = selectedLanguageStr;
                                page.Culture = selectedLanguageStr;

                                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguageStr);

                                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguageStr);

                                if ((context.Session["CurrentUI"] ?? string.Empty).ToString() != selectedLanguageStr)
                                    context.Session["CurrentUI"] = selectedLanguageStr;
                            }
                            catch (Exception ex)
                            {
                                //ignore because of invalid language
                                var el = ExceptionHelper.LogException("SetLanguage Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error,
                                    "CJI.AgileDealer.Web.WebForm", "Error when set language");
                                EmailHelper.SendEmailForNotificationWhenFailing(el);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ignore because of invalid language
                var el = ExceptionHelper.LogException("SetLanguage Outer Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error,
                    "CJI.AgileDealer.Web.WebForm", "Error when set language outside");
            }

        }

        internal static int GetLanguageId()
        {
            var name = Thread.CurrentThread.CurrentCulture.Name;

            var language = Languages?.FirstOrDefault(c => string.Compare(c.Name, name, true) == 0 && c.DealerID == DealerInfoHelper.DealerID);

            return language?.LanguageId ?? 1;
        }

        internal static string MenuString { get { return MenuDictionary[GetLanguageId()]; } }

        internal static string MenuStringDefaultPage
        {
            get
            {
                return MenuDictionaryDefaultPage[GetLanguageId()];
            }
        }

        internal static int GetLanguageIdByCultureName(string cultureName)
        {
            var language = Languages.Where(c => string.Compare(c.Name, cultureName, true) == 0 && c.DealerID == DealerInfoHelper.DealerID).FirstOrDefault();

            return language != null ? language.LanguageId : 0;
        }

        internal static List<FatFooterModel> GetFatFooter()
        {
            using (var context = new AppDbContext())
            {
                return context.Database.SqlQuery<FatFooterModel>("exec Usp_GetFatFooters 1,0").ToList();
            }
        }

        internal static List<InventoryExcelDataModel> GetInventoryDataInExcel()
        {
            using (var context = new AppDbContext())
            {
                return context.Database.SqlQuery<InventoryExcelDataModel>("exec Usp_ExportInventoryData").ToList();
            }
        }

        #region Private Methods
        private static void PopulateMenu(List<MenuModel> menu)
        {
            var sb = new StringBuilder();
            const string caret = "<span class='caret'></span>";
            const string class_toggle = @"class=""dropdown-toggle""";
            const string class_dropdown = @"class=""dropdown""";
            const string class_dropdown_menu = @"class=""dropdown-menu""";

            // build menu for all languages
            foreach (var lan in Languages)
            {

                var toplevel = menu.Where(a => a.LanguageId == lan.LanguageId && a.ParentId == null && a.ShowOnMenu == true && a.DealerID == DealerInfoHelper.DealerID).OrderBy(a => a.DisplayOrderId);
                foreach (var parent in toplevel)
                {
                    var hasChild = menu.Any(a => a.ParentId == parent.MenuId && a.ShowOnMenu == true);
                    if (hasChild)
                    {
                        sb.AppendFormat(@"<li {0}><a {1}>{2}{3}</a>", class_dropdown, class_toggle, parent.Title, caret);
                        var children = menu.Where(a => a.ParentId == parent.MenuId && a.ShowOnMenu == true && a.LanguageId == lan.LanguageId && a.DealerID == DealerInfoHelper.DealerID).OrderBy(a => a.DisplayOrderId);

                        sb.AppendFormat("<ul {0}>", class_dropdown_menu);
                        foreach (var c in children)
                        {
                            sb.AppendFormat(@"<li><a href=""{0}"" name=""{1}"">{2}</a></li>", c.Url, c.Name, c.Title);
                            // we can do grand child here too
                        }
                        sb.Append("</ul>");
                    }
                    else
                    {
                        sb.Append(string.Format(@"<li {0}><a href=""{1}"">{2}</a>", class_dropdown, parent.Url, parent.Title));
                    }

                    sb.Append(@"</li>");
                }

                MenuDictionary.Add(lan.LanguageId, sb.ToString());

                sb.Clear();
            }
        }

        private static void PopulateMenuDefaultPage(List<MenuModel> menu)
        {
            var sb = new StringBuilder();
            const string caret = "<i></i>";
            const string class_toggle = @"class=""""";
            const string class_dropdown = @"class=""parent""";
            const string class_dropdown_menu = @"class=""submenu""";

            // build menu for all languages
            foreach (var lan in Languages)
            {

                var toplevel = menu.Where(a => a.LanguageId == lan.LanguageId && a.ParentId == null && a.ShowOnMenu == true && a.DealerID == DealerInfoHelper.DealerID).OrderBy(a => a.DisplayOrderId);
                foreach (var parent in toplevel)
                {
                    var hasChild = menu.Any(a => a.ParentId == parent.MenuId && a.ShowOnMenu == true);
                    if (hasChild)
                    {
                        sb.AppendFormat(@"<li {0}><a {1}>{2}{3}</a>", class_dropdown, class_toggle, parent.Title, caret);
                        var children = menu.Where(a => a.ParentId == parent.MenuId && a.ShowOnMenu == true && a.LanguageId == lan.LanguageId && a.DealerID == DealerInfoHelper.DealerID).OrderBy(a => a.DisplayOrderId);

                        sb.AppendFormat("<ul {0}>", class_dropdown_menu);
                        foreach (var c in children)
                        {
                            sb.AppendFormat(@"<li><a href=""{0}"" name=""{1}"">{2}</a></li>", c.Url, c.Name, c.Title);
                            // we can do grand child here too
                        }
                        sb.Append("</ul><span class='arrowbtn'><img src='/images/arrow-dwn.png' alt=''></span>");
                    }
                    else
                    {
                        sb.Append(string.Format(@"<li {0}><a href=""{1}"">{2}</a>", class_dropdown, parent.Url, parent.Title));
                    }

                    sb.Append(@"</li>");
                }

                MenuDictionaryDefaultPage.Add(lan.LanguageId, sb.ToString());

                sb.Clear();
            }
        }

        private static void PopulateSiteMetaData()
        {
            List<MenuModel> menu = new List<MenuModel>();

            using (var database = new SqlConnection(ConfigurationManager.ConnectionStrings["AgileDealer"]?.ConnectionString))
            using (var pageMetaData = database.QueryMultiple("GetPageMetaData",
                new { languageId = GetLanguageId(), dealerId = DealerInfoHelper.DealerID },
                commandType: CommandType.StoredProcedure))
            {
                // LoopThrough this and get dictionary
                PageMetaData = pageMetaData.Read<PageMetaDataModel>()?.ToDictionary(x => x.PageName);
                Languages = pageMetaData.Read<Language>()?.ToList();
                RedirectUlrList = pageMetaData.Read<CustomRedirectUrl>()?.ToList();
                Location = pageMetaData.Read<Location>()?.FirstOrDefault();
                Footer = pageMetaData.Read<Footer>()?.FirstOrDefault();
                menu = pageMetaData.Read<MenuModel>()?.ToList();
                Dealer = pageMetaData.Read<Dealer>()?.FirstOrDefault();
            }

            PopulateMenu(menu);
            PopulateMenuDefaultPage(menu);
        }
        #endregion
    }
}