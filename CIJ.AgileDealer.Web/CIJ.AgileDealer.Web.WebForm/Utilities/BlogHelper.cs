﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Localization;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Repositories;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{

    public static class BlogHelper
    {
        private static string ResearchDefaultLink => "research/detail/";
        private static string Dash => "/";
        public static Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerInfoHelper.DealerID);

        private static string localUploadFolder = ConfigurationManager.AppSettings["LocalUploadFolder"];
        private static string localUploadFolderName = ConfigurationManager.AppSettings["LocalUploadFolderName"];
        private static string blobContainer = ConfigurationManager.AppSettings["BlobContainer"];
        private static string storageConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
        public static string GetBlogContentById(int blogId, int languageId, int dealerId)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                var firstOrDefault = (from blog in db.BlogEntrys
                                      join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                                      on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                                      from blog_Tran in blogTranJoin.DefaultIfEmpty()
                                      where blog.BlogEntryId == blogId && blog.DealerID == dealerId
                                      select new
                                      {
                                          PostSummary = blog_Tran.PostSummary ?? blog.PostSummary
                                      }).FirstOrDefault();

                if (firstOrDefault != null)
                    return firstOrDefault.PostSummary ?? firstOrDefault.PostSummary;
                else
                {
                    return string.Empty;
                }
            }
        }

        public static string CreateParentLink(List<int> tagids, string postTitle, string currentUrl, int dealerId,string Button1Path="")
        {

            string parentLink = string.Empty;
            List<BlogTag> blogTags = MasterDataHelper.GetBlogTagsByTagIds(tagids, dealerId);
            List<string> models = MasterDataHelper.GetModelNames(dealerId);
            List<string> modelTranslateds = MasterDataHelper.GetModelTranslatedNames(dealerId);
            if (blogTags.Any(x => x.TagText.ToLower().Contains("basemodel")))
            {
                string type = string.Empty;
                string model = string.Empty;
                string make = MasterDataHelper.GetManufactureById(Dealer.ManufactureID, dealerId).Name;
                if (blogTags.Any(x => x.TagText.ToLower() == "new" || x.TagText.ToLower() == "used" || x.TagText.ToLower() == "certified"))
                {
                    var typeDefault = blogTags.FirstOrDefault(
                        x => x.TagText.ToLower() == "new" || x.TagText.ToLower() == "used" || x.TagText.ToLower() == "certified");
                    if (typeDefault != null)
                        type = typeDefault.TagText;

                    var modelDefault = blogTags.FirstOrDefault(
                        x => models.Any(a => (x.TagText.ToLower().Contains(a.ToLower()) || x.TagText.ToLower().StartsWith(a.ToLower()) || x.TagText.ToLower().EndsWith(a.ToLower()))
                        && !x.TagText.ToLower().Contains("basemodel")));
                    //Check in Model Translated if cannot find in Model
                    if (modelDefault == null)
                    {
                        modelDefault = blogTags.FirstOrDefault(
                        x => modelTranslateds.Any(a => (x.TagText.ToLower().Contains(a.ToLower()) || x.TagText.ToLower().StartsWith(a.ToLower()) || x.TagText.ToLower().EndsWith(a.ToLower()))
                        && !x.TagText.ToLower().Contains("basemodel")));
                    }

                    if (modelDefault != null)
                        model = modelDefault.TagText.Replace(" ", "-");

                    if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(model) && !string.IsNullOrEmpty(make))
                    {
                        parentLink = $"{currentUrl}{Button1Path}";
                    }
                    else
                    {
                        parentLink = $"{currentUrl}{Dash}{ResearchDefaultLink}{postTitle}";
                    }
                }
            }
            else
            {
                parentLink = $"{currentUrl}{Dash}{ResearchDefaultLink}{postTitle}";
            }

            return parentLink;
        }

        public static void CreatePageMetaData(BlogEntry entry, string parentLink, ServicePageDbContext db, int dealerId)
        {
            if (!db.PageMetaDatas.Any(x => x.Title.Equals(entry.PostTitle, StringComparison.OrdinalIgnoreCase) && x.DealerID == dealerId))
            {
                PageMetaData pmd = new PageMetaData()
                {
                    DealerID = Dealer.DealerID,
                    PageName = "~" + Dash + ResearchDefaultLink + entry.PostUrl,
                    Title = entry.PostTitle,
                    ParentLink = parentLink.ToLower(),
                    AmpLink = string.Empty,
                    Description = entry.PostSummary,
                    CreatedDt = DateTime.Now,
                    ModifiedDt = DateTime.Now,
                };
                db.PageMetaDatas.Add(pmd);
            }
            else
            {
                PageMetaData pmd = db.PageMetaDatas.FirstOrDefault(x => x.Title.Equals(entry.PostTitle, StringComparison.OrdinalIgnoreCase) && x.DealerID == dealerId);
                if (pmd != null)
                {
                    pmd.PageName = "~" + Dash + ResearchDefaultLink + entry.PostUrl;
                    pmd.Title = entry.PostTitle;
                    pmd.ParentLink = parentLink.ToLower();
                    pmd.Description = entry.PostSummary;
                    pmd.ModifiedDt = DateTime.Now;
                    pmd.DealerID = dealerId;
                    db.Entry(pmd).State = EntityState.Modified;
                }
            }
        }

        public static void CreateBlogEntry(BlogEntryResult blog, string currentUrl, int dealerId)
        {
            if (blog != null)
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    BlogEntry entry = db.BlogEntrys.FirstOrDefault(x => x.BlogEntryId == blog.BlogEntryId && x.DealerID == dealerId);
                    if (entry == null)
                    {
                        int maxId = 0;

                        if (db.BlogEntrys.Any())
                        {
                            maxId = db.BlogEntrys.Max(x => x.BlogEntryId) + 1;
                        }
                        else
                            maxId = maxId + 1;

                        entry = new BlogEntry
                        {
                            BlogEntryId = maxId,
                            PostTitle = blog.PostTitle,
                            PostText = blog.PostText,
                            DateEnter = DateTime.Now,
                            PostSummary = blog.PostSummary,
                            Button1Text = blog.Button1Text,
                            Button1Path = blog.Button1Path,
                            Button2Text = blog.Button2Text,
                            Button2Path = blog.Button2Path,
                            CreatedDt = DateTime.Now,
                            ModifiedDt = DateTime.Now,
                            LanguageId = blog.LanguageId,
                            DealerID = Dealer.DealerID,
                            OverrideImage = blog.OverrideImage,
                        };

                        if (blog.IsUpdateNewImage)
                        {
                            entry.PostFeatureImage = blog.PostFeatureImage;
                            entry.ImageName = blog.ImageName;
                        }
                        entry.PostUrl = !string.IsNullOrEmpty(blog.PostTitle) ? blog.PostTitle.ToLower().Replace(" ", "-").Trim() : string.Empty;

                        string parentLink = CreateParentLink(blog.BlogTagIds, entry.PostUrl, currentUrl, dealerId);

                        CreatePageMetaData(entry, parentLink, db, dealerId);

                        UploadImageOfBlogIntoAzure(db, entry, dealerId);
                        db.BlogEntrys.Add(entry);
                        db.SaveChanges();
                        LinkBlogEntryWithBlogTags(db, entry, blog, dealerId);
                        LinkBlogEntryWithBlogCategories(db, entry, blog, dealerId);

                        if (blog.LanguageId.HasValue)
                        {
                            LinkBlogEntryWithTranslator(db, entry, dealerId);
                        }
                    }
                }
            }
        }

        public static void UpdateBlogEntry(BlogEntryResult blog, string currentUrl, int dealerId)
        {
            if (blog != null)
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    BlogEntry entry = db.BlogEntrys.FirstOrDefault(x => x.BlogEntryId == blog.BlogEntryId && x.DealerID == dealerId);
                    if (entry != null)
                    {
                        entry.PostTitle = blog.PostTitle;

                        //entry.DateEnter = blog.DateEnter;
                        entry.PostSummary = blog.PostSummary;
                        entry.PostText = blog.PostText;
                        entry.Button1Text = blog.Button1Text;
                        entry.Button1Path = blog.Button1Path;
                        entry.Button2Text = blog.Button2Text;
                        entry.Button2Path = blog.Button2Path;
                        entry.PostUrl = !string.IsNullOrEmpty(blog.PostTitle) ? blog.PostTitle.ToLower().Replace(" ", "-").Trim() : string.Empty;
                        entry.ModifiedDt = DateTime.Now;
                        entry.LanguageId = blog.LanguageId;
                        entry.OverrideImage = blog.OverrideImage;
                        if (blog.IsUpdateNewImage)
                        {
                            entry.PostFeatureImage = blog.PostFeatureImage;
                            entry.ImageName = blog.ImageName;
                            UploadImageOfBlogIntoAzure(db, entry, dealerId);
                        }

                        string parentLink = CreateParentLink(blog.BlogTagIds, entry.PostUrl, currentUrl, dealerId, blog.Button1Path);
                        CreatePageMetaData(entry, parentLink, db, dealerId);
                        db.Entry(entry).State = EntityState.Modified;
                        db.SaveChanges();
                        LinkBlogEntryWithBlogTags(db, entry, blog, dealerId);
                        LinkBlogEntryWithBlogCategories(db, entry, blog, dealerId);

                        if (blog.LanguageId.HasValue)
                        {
                            LinkBlogEntryWithTranslator(db, entry, dealerId);
                        }
                    }
                }
            }
        }

        private static void LinkBlogEntryWithTranslator(ServicePageDbContext db, BlogEntry blogEntry, int dealerId)
        {
            if (blogEntry != null && blogEntry.LanguageId.HasValue)
            {

                if (
                    !db.BlogEntry_Trans.Any(
                        x => x.BlogEntryId == blogEntry.BlogEntryId && x.LanguageId == blogEntry.LanguageId.Value && x.DealerID == dealerId))
                {
                    BlogEntry_Trans blogEntryTrans = new BlogEntry_Trans();
                    blogEntryTrans.PostTitle = blogEntry.PostTitle;
                    blogEntryTrans.PostSummary = blogEntry.PostSummary;
                    blogEntryTrans.PostText = blogEntry.PostText;
                    blogEntryTrans.PostUrl = blogEntry.PostUrl;
                    blogEntryTrans.Button1Text = blogEntry.Button1Text;
                    blogEntryTrans.Button1Path = blogEntry.Button1Path;
                    blogEntryTrans.Button2Text = blogEntry.Button2Text;
                    blogEntryTrans.Button2Path = blogEntry.Button2Path;
                    blogEntryTrans.BlogEntryId = blogEntry.BlogEntryId;
                    blogEntryTrans.LanguageId = blogEntry.LanguageId ?? 0;
                    blogEntryTrans.DealerID = dealerId;
                    db.BlogEntry_Trans.Add(blogEntryTrans);
                }
                else
                {
                    BlogEntry_Trans blogEntryTrans = db.BlogEntry_Trans.FirstOrDefault(x => x.BlogEntryId == blogEntry.BlogEntryId && x.LanguageId == blogEntry.LanguageId.Value && x.DealerID == dealerId);
                    if (blogEntryTrans != null)
                    {
                        blogEntryTrans.PostTitle = blogEntry.PostTitle;
                        blogEntryTrans.PostSummary = blogEntry.PostSummary;
                        blogEntryTrans.PostText = blogEntry.PostText;
                        blogEntryTrans.PostUrl = blogEntry.PostUrl;
                        blogEntryTrans.Button1Text = blogEntry.Button1Text;
                        blogEntryTrans.Button1Path = blogEntry.Button1Path;
                        blogEntryTrans.Button2Text = blogEntry.Button2Text;
                        blogEntryTrans.Button2Path = blogEntry.Button2Path;
                        blogEntryTrans.DealerID = dealerId;
                        db.Entry(blogEntryTrans).State = EntityState.Modified;
                    }
                }

                db.SaveChanges();
            }
        }

        private static string GetFolderNameForUpload(string method, int dealerId)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(method))
            {
                UploadInformation tempUploadInformation = MasterDataHelper.GetUploadInformation(method, dealerId);
                if (tempUploadInformation != null)
                {
                    result = tempUploadInformation.FolderName;
                }
            }
            return result;
        }

        private static string GetLocalUploadFolder(string customName = "")
        {
            string dirFullPath = HttpContext.Current.Server.MapPath(localUploadFolder);
            if (!string.IsNullOrEmpty(customName))
                dirFullPath = Path.Combine(dirFullPath, customName);

            if (!Directory.Exists(dirFullPath))
            {
                Directory.CreateDirectory(dirFullPath);
            }
            return dirFullPath;
        }

        private static void UploadImageOfBlogIntoAzure(ServicePageDbContext db, BlogEntry blog, int dealerId)
        {
            if (!string.IsNullOrEmpty(blog.ImageName))
            {
                string uploadfolder = GetFolderNameForUpload("UploadBlogImage", dealerId);
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(blog.ImageName);
                string dirFullPath = GetLocalUploadFolder(fileNameWithoutExtension);
                string azureLink = Path.Combine(Dealer.ImageServer, uploadfolder, blog.ImageName);
                blog.PostFeatureImage = azureLink;
                new VehicleRepository().UploadAgileVehicleImage(storageConnectionString, uploadfolder, dirFullPath, blobContainer);
            }
        }

        private static void LinkBlogEntryWithBlogCategories(ServicePageDbContext db, BlogEntry currentBlogEntry, BlogEntryResult tempBlogEntry, int dealerId)
        {
            List<BlogEntry_BlogCategory> blogEntryBlogCategories =
                db.BlogEntries_BlogCategories.Where(x => x.BlogEntryId == currentBlogEntry.BlogEntryId && x.DealerID == dealerId).ToList();

            if (blogEntryBlogCategories.Any())
            {
                db.BlogEntries_BlogCategories.RemoveRange(blogEntryBlogCategories);
                db.SaveChanges();
            }

            if (tempBlogEntry.BlogCategoryIds != null && tempBlogEntry.BlogCategoryIds.Any())
            {
                foreach (int categoryId in tempBlogEntry.BlogCategoryIds)
                {
                    db.BlogEntries_BlogCategories.Add(new BlogEntry_BlogCategory
                    {
                        BlogEntryId = currentBlogEntry.BlogEntryId,
                        BlogCategoryId = categoryId,
                        CreatedDt = DateTime.Now,
                        ModifiedDt = DateTime.Now,
                        DealerID = dealerId
                    });
                }
                db.SaveChanges();
            }
        }

        private static void LinkBlogEntryWithBlogTags(ServicePageDbContext db, BlogEntry currentBlogEntry, BlogEntryResult tempBlogEntry, int dealerId)
        {
            List<BlogEntry_BlogTag> blogEntryBlogCategories =
                db.BlogEntries_BlogTags.Where(x => x.BlogEntryId == currentBlogEntry.BlogEntryId && x.DealerID == dealerId).ToList();

            if (blogEntryBlogCategories.Any())
            {
                db.BlogEntries_BlogTags.RemoveRange(blogEntryBlogCategories);
                db.SaveChanges();
            }

            if (tempBlogEntry.BlogTagIds != null && tempBlogEntry.BlogTagIds.Any())
            {
                foreach (int tagId in tempBlogEntry.BlogTagIds)
                {
                    db.BlogEntries_BlogTags.Add(new BlogEntry_BlogTag()
                    {
                        BlogEntryId = currentBlogEntry.BlogEntryId,
                        BlogTagId = tagId,
                        CreatedDt = DateTime.Now,
                        ModifiedDt = DateTime.Now,
                        DealerID = dealerId
                    });
                }
                db.SaveChanges();
            }
        }

        public static List<BlogEntryResult> GetBlogEntries(int languageId, int dealerId)
        {
            List<BlogEntryResult> results = new List<BlogEntryResult>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                results = (from blog in db.BlogEntrys
                           join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                           on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                           from blog_Tran in blogTranJoin.DefaultIfEmpty()
                           where blog.DealerID == dealerId
                           select new BlogEntryResult
                           {
                               BlogEntryId = blog.BlogEntryId,
                               PostTitle = blog_Tran.PostTitle ?? blog.PostTitle,
                               DateEnter = blog.DateEnter
                           }).ToList();

                if (results.Any())
                {
                    int count = results.Count;

                    for (int i = 0; i < count; i++)
                    {
                        results[i].BlogTag = GetBlogTagJoinedStringByBlogEntryId(results[i].BlogEntryId, dealerId);
                        results[i].BlogCategory = GetBlogCategoryJoinedStringByBlogEntryId(results[i].BlogEntryId, dealerId);
                    }
                }
            }
            return results;
        }

        public static BlogEntryResult GetBlogEntryById(int blogEntryId, int languageId, int dealerId)
        {
            BlogEntryResult blogEntry = new BlogEntryResult();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                blogEntry = (from blog in db.BlogEntrys
                             join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                                on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                             from blog_Tran in blogTranJoin.DefaultIfEmpty()
                             where blog.BlogEntryId == blogEntryId && blog.DealerID == dealerId
                             select new BlogEntryResult
                             {
                                 BlogEntryId = blog.BlogEntryId,
                                 PostTitle = blog_Tran.PostTitle ?? blog.PostTitle,
                                 PostFeatureImage = blog.PostFeatureImage,
                                 PostSummary = blog_Tran.PostSummary ?? blog.PostSummary,
                                 PostText = blog_Tran.PostText ?? blog.PostText,
                                 Button1Text = blog_Tran.Button1Text ?? blog.Button1Text,
                                 Button1Path = blog_Tran.Button1Path ?? blog.Button1Path,
                                 Button2Text = blog_Tran.Button2Text ?? blog.Button2Text,
                                 Button2Path = blog_Tran.Button2Path ?? blog.Button2Path,
                                 DateEnter = blog.DateEnter,
                                 IsPublish = blog.IsPublish,
                                 IsDeleted = blog.IsDeleted,
                                 IsUpdateNewImage = false,
                                 LanguageId = blog.LanguageId,
                                 OverrideImage = blog.OverrideImage,
                             }).FirstOrDefault();

                if (blogEntry != null)
                {
                    blogEntry.ImageName = Path.GetFileName(blogEntry.PostFeatureImage);
                    blogEntry.BlogTagIds = GetBlogTagsByBlogEntryId(blogEntry.BlogEntryId, dealerId);
                    blogEntry.BlogCategoryIds = GetBlogCategoriesByBlogEntryId(blogEntry.BlogEntryId, dealerId);
                }
            }
            return blogEntry;
        }

        public static string GetBlogTagJoinedStringByBlogEntryId(int blogEntryId, int dealerId)
        {
            string result = string.Empty;
            if (blogEntryId > 0)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var temp = (from tag in db.BlogTags
                                join blog_tag in db.BlogEntries_BlogTags on tag.BlogTagId equals blog_tag.BlogTagId
                                where blog_tag.BlogEntryId == blogEntryId && tag.DealerID == dealerId
                                select tag.TagText).ToList();
                    if (temp.Any())
                    {
                        result = string.Join("; ", temp);
                    }
                }
            }
            return result;
        }

        private static List<int> GetBlogCategoriesByBlogEntryId(int blogEntryId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return
                    db.BlogEntries_BlogCategories.Where(x => x.BlogEntryId == blogEntryId && x.DealerID == dealerId)
                        .Select(a => a.BlogCategoryId)
                        .ToList();
            }
        }

        private static List<int> GetBlogTagsByBlogEntryId(int blogEntryId, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return
                    db.BlogEntries_BlogTags.Where(x => x.BlogEntryId == blogEntryId && x.DealerID == dealerId)
                        .Select(a => a.BlogTagId)
                        .ToList();
            }
        }

        public static string GetBlogCategoryJoinedStringByBlogEntryId(int blogEntryId, int dealerId)
        {
            string result = string.Empty;
            if (blogEntryId > 0)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    var temp = (from cat in db.BlogCategoryies
                                join blog_cat in db.BlogEntries_BlogCategories on cat.BlogCategoryId equals blog_cat.BlogCategoryId
                                where blog_cat.BlogEntryId == blogEntryId && cat.DealerID == dealerId
                                select cat.CategoryText).ToList();
                    if (temp.Any())
                    {
                        result = string.Join(";", temp);
                    }
                }
            }
            return result;
        }
    }
}