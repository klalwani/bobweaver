﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Dashboards;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class ApplicantHelper
    {
        public static List<ApplicantDashboard> GetApplicants(bool isIncludeArchive, int dealerId)
        {
            List<ApplicantDashboard> vs;

            using (var db = new ServicePageDbContext())
            {
                vs = (from app in db.Applicants
                      join appType in db.ApplicantTypes on app.ApplicantTypeId equals appType.ApplicantTypeId into appTypeJoin
                      from appTypeLeftJoin in appTypeJoin.DefaultIfEmpty()
                      join contact in db.ContactInfos on app.ApplicantId equals contact.ApplicantId into contactJoin
                      from contactLeftJoin in contactJoin.DefaultIfEmpty()
                      join appInfo in db.ApplicantInfos on app.ApplicantId equals appInfo.ApplicantId into appInfoJoin
                      from appInfoLeftJoin in appInfoJoin.DefaultIfEmpty()
                      join empInfo in db.EmploymentInfos on app.ApplicantId equals empInfo.ApplicantId into empInfoJoin
                      from empInfoLeftJoin in empInfoJoin.DefaultIfEmpty()
                      where app.DealerID == dealerId
                     // && contactLeftJoin.IsCorrespondingContactInfo == "True"
                     // && appInfoLeftJoin.IsCorrespondingApplicantInfo == "True"
                     // && empInfoLeftJoin.IsCorrespondingEmploymentInfo == "True"
                      && !app.IsArchived
                      || (isIncludeArchive && app.IsArchived)
                      select new ApplicantDashboard
                      {
                          ApplicantId = app.ApplicantId,
                          CustomerName = app.FirstName + " " + app.LastName,
                          PhoneNumber = app.PhoneNumber,
                          City = contactLeftJoin.City,
                          State = contactLeftJoin.State,
                          NumberOfApplicants = appInfoLeftJoin.NumberOfApplicant,
                          DateCreated = app.CreatedDt,
                          IsArchived = app.IsArchived,
                          IsCompleted=app.IsCompleted                          
                      }).Distinct().ToList();
            }
            return vs;
        }

        public static ApplicantType GetApplicantTypeById(int id)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.ApplicantTypes.FirstOrDefault(x => x.ApplicantTypeId == id);
            }
        }

        public static ApplicantDetail GetApplicantByApplicantId(int applicantId)
        {
            ApplicantDetail applicantDetail = new ApplicantDetail();
            if (applicantId > 0)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    applicantDetail = (from app in db.Applicants
                                       join applicantType in db.ApplicantTypes on app.ApplicantTypeId equals applicantType.ApplicantTypeId into applicantTypeJoin
                                       from applicantTypeLeftJoin in applicantTypeJoin.DefaultIfEmpty()
                                       join contact in db.ContactInfos on app.ApplicantId equals contact.ApplicantId into contactJoin
                                       from contactLeftJoin in contactJoin.DefaultIfEmpty()
                                       join appInfo in db.ApplicantInfos on app.ApplicantId equals appInfo.ApplicantId into appInfoJoin
                                       from appInfoLeftJoin in appInfoJoin.DefaultIfEmpty()
                                       join empInfo in db.EmploymentInfos on app.ApplicantId equals empInfo.ApplicantId into empInfoJoin
                                       from empInfoLeftJoin in empInfoJoin.DefaultIfEmpty()
                                       where app.ApplicantId == applicantId
                                       //&& (contactLeftJoin.IsCorrespondingContactInfo == "True")
                                       //&& (appInfoLeftJoin.IsCorrespondingApplicantInfo == "True")
                                       //&& (empInfoLeftJoin.IsCorrespondingEmploymentInfo == "True")
                                       select new ApplicantDetail
                                       {
                                           Applicant = app,
                                           ContactInfo = contactLeftJoin,
                                           ApplicantInfo = appInfoLeftJoin,
                                           EmploymentInfo = empInfoLeftJoin,
                                       }).FirstOrDefault();
                    if (applicantDetail != null)
                    {
                        applicantDetail.Applicant.ApplicantType =
                            GetApplicantTypeById(applicantDetail.Applicant.ApplicantTypeId);
                        if (applicantDetail.ContactInfo != null)
                        {
                            applicantDetail.ContactInfo.ResidenceStatusName =
                                MasterDataHelper.GetResidenceStatusById(applicantDetail.ContactInfo.ResidenceStatus).Name;
                        }
                    }
                    if (applicantDetail != null && applicantDetail.ContactInfo != null)
                    {
                        if (applicantDetail != null && applicantDetail.ContactInfo.NumberOfApplicant != (int)ApplicantEnums.Individual)
                        {
                            applicantDetail.JoinedContactInfo =
                                db.ContactInfos.FirstOrDefault(
                                    x => x.ApplicantId == applicantId && x.IsCorrespondingContactInfo != "True");
                            if (applicantDetail.JoinedContactInfo != null)
                            {
                                applicantDetail.JoinedContactInfo.ResidenceStatusName =
                                MasterDataHelper.GetResidenceStatusById(applicantDetail.JoinedContactInfo.ResidenceStatus).Name;
                            }
                            applicantDetail.JoinedEmploymentInfo =
                                db.EmploymentInfos.FirstOrDefault(
                                    x => x.ApplicantId == applicantId && x.IsCorrespondingEmploymentInfo != "True");
                            applicantDetail.JoinedApplicantInfo =
                                db.ApplicantInfos.FirstOrDefault(
                                    x => x.ApplicantId == applicantId && x.IsCorrespondingApplicantInfo != "True");
                        }
                    }
                }
            }
            return applicantDetail;
        }
    }
}