﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using AgileDealer.Data.Entities.CMS;
using AgileDealer.Data.Entities.Localization;
using AgileDealer.Data.Entities.Nagivation;
using AgileDealer.Data.Entities.SiteMap;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using SiteMapData = AgileDealer.Data.Entities.SiteMap;
namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public class CMSHelper
    {
        static readonly string TAGS = "tags";
        public static bool IsCMS(Page page)
        {
            return page.RouteData.Values[TAGS] != null;
        }

        public static ContentPage GetContentPage(Page page, int languageId, int dealerId)
        {
            ContentPage content = null;
            var routing = page.Request.Url.LocalPath;
            if (routing != string.Empty)
            {
                using (var db = new ServicePageDbContext())
                {
                    // retrieve menu id by routing
                    var menu = db.Menus.SingleOrDefault(a => a.Url.ToLower() == routing && a.DealerID == dealerId);
                    // retrieve content by menu
                    if (menu != null)
                    {
                        content = db.ContentPages.Include("PostData").SingleOrDefault(a => a.MenuId == menu.MenuId && a.IsActive == true);
                    }
                }
            }

            return content;
        }
        public static ContentPage GetContentPageMetaData(Page page, int languageId, int dealerId)
        {
            ContentPage content = null;
            var routing = page.Request.Url.LocalPath;
            if (routing != string.Empty)
            {
                using (var db = new ServicePageDbContext())
                {
                    // retrieve menu id by routing
                    var menu = db.Menus.SingleOrDefault(a => a.Url.ToLower() == routing && a.DealerID == dealerId);
                    // retrieve content by menu
                    if (menu != null)
                    {
                        var ContentPageModelData = GetContentPage(menu.MenuId, languageId, dealerId);
                        if (ContentPageModelData != null)
                        {
                            content = new ContentPage()
                            {
                                Canonical = ContentPageModelData.Canonical,
                                Description = ContentPageModelData.Description,
                                Title = ContentPageModelData.Title,
                                AmpLink = ContentPageModelData.AmpLink
                            };
                        }
                    }
                }
            }

            return content;
        }
        public static bool ValidateUrl(string url, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                url = url.Trim().ToLower();
                var item = db.Menus.FirstOrDefault(c => c.Url.ToLower() == url && c.DealerID == dealerId);
                return item == null;
            }
        }

        public static List<TreeviewModel> GetMenus(int dealerId, bool IsAdminPage)
        {
            List<TreeviewModel> result = new List<TreeviewModel>();
            using (var db = new ServicePageDbContext())
            {
                List<Menu> menus = new List<Menu>();
                if (IsAdminPage)
                    menus = db.Menus.Where(x => x.DealerID == dealerId).ToList();
                else
                    menus = db.Menus.Where(x => x.DealerID == dealerId && x.ShowOnSitemap).ToList();
                var roots = menus.Where(c => c.ParentId == null).OrderBy(c => c.DisplayOrderId);
                foreach (var root in roots)
                {
                    var menuModel = CreateTreviewMenuNode(root);
                    menuModel.Nodes = new List<TreeviewModel>();
                    CreateSubMenu(menus, menuModel, dealerId);
                    result.Add(menuModel);
                }
            }
            return result;
        }

        public static ContentPagePostDataModel GetContentPagePostData(int menuId, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                ContentPagePostDataModel result = null;
                var contentPage = db.ContentPages.FirstOrDefault(c => c.MenuId == menuId && c.DealerID == dealerId);
                var postData = contentPage != null ? contentPage.PostData : null;
                if (postData != null)
                {
                    result = new ContentPagePostDataModel()
                    {
                        ContentPagePostDataId = postData.ContentPagePostDataId,
                        UserEmailField = postData.PostUserFieldName,
                        EmailField = postData.PostEmailFieldName,
                        PostTitle = postData.PostTitle,
                        EmailTo = postData.PosToEmailValue,
                        EmailCC = postData.PosToEmailCCValue,
                        EmailBCC = postData.PostToEmailBCCValue,
                        DataFormat = postData.PostDataFormat,
                        ResponseText = postData.PostResponseText
                    };
                }
                return result;
            }
        }

        public static ContentPageModel GetContentPage(int menuId, int languageId, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                var query = from contentPage in db.ContentPages.Where(c => c.MenuId == menuId && c.DealerID == dealerId)
                            join contentPage_Trans in db.ContentPage_Trans.Where(c => c.LanguageId == languageId) on contentPage.ContentPageId equals contentPage_Trans.ContentPageId
                            into tbl_Trans
                            from tbl in tbl_Trans.DefaultIfEmpty()
                            select new ContentPageModel()
                            {
                                Description = contentPage.Description,
                                AmpLink = contentPage.AmpLink,
                                BodyContent = tbl.BodyContent ?? contentPage.BodyContent,
                                BodyContentBottom = contentPage.BodyContentBottom,
                                ShowInventory = contentPage.ShowInventory,
                                InventoryFilters = contentPage.InventoryFilters,
                                Canonical = contentPage.Canonical,
                                IsActive = contentPage.IsActive,
                                Title = tbl.Title ?? contentPage.Title,
                                ContentPageId = contentPage.ContentPageId
                            };
                return query.FirstOrDefault();
            }
        }

        public static MenuModel GetMenuById(int menuId, int languageId, int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                var query = from menu in db.Menus.Where(c => c.MenuId == menuId && c.DealerID == dealerId)
                            join menu_Trans in db.Menu_Trans.Where(c => c.LanguageId == languageId && c.DealerID == dealerId) on menu.MenuId equals menu_Trans.MenuId
                            into tbl_Trans
                            from tbl in tbl_Trans.DefaultIfEmpty()
                            select new MenuModel()
                            {
                                Description = menu.Description,
                                Name = menu.Name,
                                Title = tbl.Title ?? menu.Title,
                                Url = menu.Url,
                                ShowOnMenu = menu.ShowOnMenu,
                                ShowOnSiteMap = menu.ShowOnSitemap,
                                ShowOnSiteMapXML = menu.ShowOnSitemapXML,
                                DealerID = menu.DealerID ?? 0,
                                DisplayOrderId = menu.DisplayOrderId,
                                IsActive = menu.IsActive,
                                IsAdminOnly = true,
                                ParentId = menu.ParentId
                            };
                return query.FirstOrDefault();
            }
        }

        public static bool UpdateMenu(List<TreeviewModel> treeViews, int dealerId)
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    foreach (var tree in treeViews)
                    {
                        var childs = tree.Nodes;
                        if (childs != null)
                        {
                            var childIds = childs.Select(c => c.NodeId).ToList();
                            var menus = db.Menus.Where(c => childIds.Contains(c.MenuId) && c.DealerID == dealerId);
                            var index = 0;
                            foreach (var menu in menus)
                            {
                                var child = childs.FirstOrDefault(c => c.NodeId == menu.MenuId);
                                menu.ParentId = tree.NodeId;
                                menu.DealerID = dealerId;
                                menu.DisplayOrderId = child != null ? child.Index : 0;
                                db.Entry(menu).State = EntityState.Modified;
                                index++;
                            }
                        }
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void SaveOrUpdateContentPagePostData(ContentPagePostDataModel contentPagePostDataModel, int dealerId)
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    var contentPagePostData = new ContentPagePostData();
                    if (contentPagePostDataModel.ContentPagePostDataId != 0)
                    {
                        contentPagePostData = db.ContentPagePostData.FirstOrDefault(c => c.ContentPagePostDataId == contentPagePostDataModel.ContentPagePostDataId && c.DealerID == dealerId);
                        if (contentPagePostData != null)
                        {
                            contentPagePostData.PostEmailFieldName = contentPagePostDataModel.EmailField;
                            contentPagePostData.PostUserFieldName = contentPagePostDataModel.UserEmailField;
                            contentPagePostData.PostTitle = contentPagePostDataModel.PostTitle;
                            contentPagePostData.PostToMethod = "Email";
                            contentPagePostData.PosToEmailValue = contentPagePostDataModel.EmailTo;
                            contentPagePostData.PostToEmailBCCValue = contentPagePostDataModel.EmailBCC;
                            contentPagePostData.PosToEmailCCValue = contentPagePostDataModel.EmailCC;
                            contentPagePostData.PostDataFormat = contentPagePostDataModel.DataFormat;
                            contentPagePostData.PostResponseText = contentPagePostDataModel.ResponseText;
                            contentPagePostData.DealerID = dealerId;
                        }
                    }
                    else
                    {
                        contentPagePostData = new ContentPagePostData
                        {
                            PostEmailFieldName = contentPagePostDataModel.EmailField,
                            PostUserFieldName = contentPagePostDataModel.UserEmailField,
                            PostTitle = contentPagePostDataModel.PostTitle,
                            PostToMethod = "Email",
                            PosToEmailValue = contentPagePostDataModel.EmailTo,
                            PostToEmailBCCValue = contentPagePostDataModel.EmailBCC,
                            PosToEmailCCValue = contentPagePostDataModel.EmailCC,
                            PostDataFormat = contentPagePostDataModel.DataFormat,
                            PostResponseText = contentPagePostDataModel.ResponseText,
                            DealerID = dealerId,
                            CreatedDt = DateTime.Now,
                            ModifiedDt = DateTime.Now
                        };
                        db.ContentPagePostData.Add(contentPagePostData);
                    }
                    var contentPage = db.ContentPages.FirstOrDefault(c => c.ContentPageId == contentPagePostDataModel.ContentPageId && c.DealerID == dealerId);
                    if (contentPage != null)
                    {
                        contentPage.PostData = contentPagePostData;
                        db.Entry(contentPage).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static int SaveOrUpdateContentPage(ContentPageModel contentPageModel, int languageId, int dealerId)
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    var contentPage = new ContentPage();
                    if (contentPageModel.ContentPageId != 0)
                    {
                        contentPage = db.ContentPages.FirstOrDefault(c => c.ContentPageId == contentPageModel.ContentPageId && c.DealerID == dealerId);
                        if (contentPage != null)
                        {
                            contentPage.Description = contentPageModel.Description;
                            contentPage.Canonical = contentPageModel.Canonical;
                            contentPage.AmpLink = contentPageModel.AmpLink;
                            contentPage.IsActive = contentPageModel.IsActive;
                            contentPage.BodyContent = contentPageModel.BodyContent;
                            contentPage.BodyContentBottom = contentPageModel.BodyContentBottom;
                            contentPage.ShowInventory = contentPageModel.ShowInventory;
                            contentPage.InventoryFilters = contentPageModel.InventoryFilters;
                            contentPage.DealerID = dealerId;
                            db.ContentPages.Attach(contentPage);
                            db.Entry(contentPage).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        contentPage = new ContentPage()
                        {
                            Title = contentPageModel.Title,
                            Description = contentPageModel.Description,
                            Canonical = contentPageModel.Canonical,
                            AmpLink = contentPageModel.AmpLink,
                            MenuId = contentPageModel.MenuId,
                            IsActive = contentPageModel.IsActive,
                            IsAdminOnly = true,
                            BodyContent = contentPageModel.BodyContent,
                            BodyContentBottom = contentPageModel.BodyContentBottom,
                            ShowInventory = contentPageModel.ShowInventory,
                            InventoryFilters = contentPageModel.InventoryFilters,
                            DealerID = dealerId,
                            CreatedDt = DateTime.Now,
                            ModifiedDt = DateTime.Now
                        };
                        db.ContentPages.Add(contentPage);
                    }
                    var contentPageTran = db.ContentPage_Trans.FirstOrDefault(c => c.LanguageId == languageId && c.ContentPageId == contentPageModel.ContentPageId && c.DealerID == dealerId);
                    if (contentPageTran != null)
                    {
                        contentPageTran.Title = contentPageModel.Title;
                        contentPageTran.BodyContent = contentPage.BodyContent;
                        db.ContentPage_Trans.Attach(contentPageTran);
                        db.Entry(contentPageTran).State = EntityState.Modified;
                    }
                    else
                    {
                        contentPageTran = new ContentPage_Trans()
                        {
                            LanguageId = languageId,
                            ContentPageId = contentPageModel.MenuId,
                            Title = contentPageModel.Title,
                            BodyContent = contentPageModel.BodyContent,
                            ContentPage = contentPage,
                            DealerID = dealerId
                        };
                        db.ContentPage_Trans.Add(contentPageTran);
                    }
                    db.SaveChanges();
                    return contentPage.ContentPageId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int SaveOrUpdateMenu(MenuModel menuModel, int languageId, int dealerId)
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    var menu = new Menu();
                    if (menuModel.MenuId != 0)
                    {
                        menu = db.Menus.FirstOrDefault(c => c.MenuId == menuModel.MenuId && c.DealerID == dealerId);
                        SiteMapLocation siteMapLocation = null;
                        if (menu != null && menu.ParentId != null && menuModel.ShowOnSiteMapXML)
                        {
                            siteMapLocation = db.SiteMapLocations.FirstOrDefault(c => c.DealerID == dealerId && (c.Url.ToLower().Trim() == menu.Url.ToLower().Trim() || c.SiteMapLocationId == menu.SiteMapLocationId));
                            if (siteMapLocation == null)
                            {
                                siteMapLocation = AddSiteMapLocation(db, menuModel, dealerId);
                            }
                            else
                            {
                                siteMapLocation.Url = menuModel.Url;
                                var parent = db.Menus.FirstOrDefault(c => c.MenuId == menuModel.ParentId && menu.ParentId != menuModel.ParentId && c.DealerID == dealerId);
                                var title = parent != null ? parent.Title : string.Empty;
                                var siteMap = db.SiteMaps.FirstOrDefault(c => c.Title.ToLower().Trim() == title.ToLower().Trim() && c.DealerID == dealerId);
                                if (siteMap != null)
                                {
                                    siteMapLocation.SitemapId = siteMap.SitemapId;
                                }
                                db.SiteMapLocations.Attach(siteMapLocation);
                                db.Entry(siteMapLocation).State = EntityState.Modified;
                            }
                        }

                        menu.ParentId = menuModel.ParentId;
                        menu.Title = menuModel.Title;
                        menu.Name = menuModel.Name;
                        menu.Description = menuModel.Description;
                        menu.Url = menuModel.Url;
                        menu.DealerID = menuModel.DealerID;
                        menu.DisplayOrderId = menuModel.DisplayOrderId;
                        menu.IsActive = menuModel.IsActive;
                        menu.ShowOnMenu = menuModel.ShowOnMenu;
                        menu.ShowOnSitemap = menuModel.ShowOnSiteMap;
                        menu.ShowOnSitemapXML = menuModel.ShowOnSiteMapXML;
                        menu.SiteMapLocation = siteMapLocation;
                        db.Menus.Attach(menu);
                        db.Entry(menu).State = EntityState.Modified;
                    }
                    else
                    {
                        SiteMapLocation siteMapLocation = null;
                        if (menuModel.ParentId != null)
                        {
                            siteMapLocation = AddSiteMapLocation(db, menuModel, dealerId);
                        }
                        menu = new Menu()
                        {
                            ParentId = menuModel.ParentId,
                            Name = menuModel.Name,
                            Title = menuModel.Title,
                            Description = menuModel.Description,
                            Url = menuModel.Url,
                            DealerID = menuModel.DealerID,
                            DisplayOrderId = menuModel.DisplayOrderId,
                            IsActive = menuModel.IsActive,
                            ShowOnMenu = menuModel.ShowOnMenu,
                            ShowOnSitemap = menuModel.ShowOnSiteMap,
                            ShowOnSitemapXML = menuModel.ShowOnSiteMapXML,
                            ModifiedDt = DateTime.Now,
                            CreatedDt = DateTime.Now
                        };
                        menu.SiteMapLocation = siteMapLocation;
                        db.Menus.Add(menu);
                    }
                    var menuTran = db.Menu_Trans.FirstOrDefault(c => c.LanguageId == languageId && c.MenuId == menuModel.MenuId && c.DealerID == dealerId);
                    if (menuTran != null)
                    {
                        menuTran.Title = menuModel.Title;
                        db.Menu_Trans.Attach(menuTran);
                        db.Entry(menuTran).State = EntityState.Modified;
                    }
                    else
                    {
                        menuTran = new Menu_Trans()
                        {
                            LanguageId = languageId,
                            Menu = menu,
                            Title = menuModel.Title,
                            DealerID = dealerId
                        };
                        db.Menu_Trans.Add(menuTran);
                    }
                    db.SaveChanges();
                    return menu.MenuId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void SaveOrUpdateMenuSetting(MenuModel menuModel, ContentPageModel contentPageModel,
            ContentPagePostDataModel contentPagePostDataModel, int languageId, int dealerId)
        {
            var menuId = SaveOrUpdateMenu(menuModel, languageId, dealerId);
            contentPageModel.MenuId = menuId;
            var contentPageId = SaveOrUpdateContentPage(contentPageModel, languageId, dealerId);
            contentPagePostDataModel.ContentPageId = contentPageId;
            SaveOrUpdateContentPagePostData(contentPagePostDataModel, dealerId);
        }

        public static List<Language> GetAllLanguage(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                var lans = db.Languages.Where(c => c.Active && c.DealerID == dealerId).ToList();
                return lans;
            }
        }

        public static List<MenuModel> GetParents(int dealerId)
        {
            using (var db = new ServicePageDbContext())
            {
                var menus = db.Menus.Where(c => c.ParentId == null && c.DealerID == dealerId).Select(c => new MenuModel() { MenuId = c.MenuId, Title = c.Title }).ToList();
                return menus;
            }
        }

        private static SiteMapLocation AddSiteMapLocation(ServicePageDbContext db, MenuModel menuModel, int dealerId)
        {
            SiteMapLocation siteMapLocation = null;
            if (menuModel.ParentId != null)
            {
                siteMapLocation = db.SiteMapLocations.FirstOrDefault(c => c.Url.ToLower().Trim() == menuModel.Url.ToLower().Trim() && c.DealerID == dealerId);
                if (siteMapLocation == null)
                {
                    siteMapLocation = new SiteMapLocation()
                    {
                        LastModified = DateTime.Now.ToShortDateString(),
                        Priority = 1,
                        ChangeFrequency = SiteMapLocation.eChangeFrequency.daily,
                        Url = menuModel.Url,
                        DealerID = dealerId
                    };
                    var parent = db.Menus.FirstOrDefault(c => c.MenuId == menuModel.ParentId && c.DealerID == dealerId);
                    var title = parent != null ? parent.Title : string.Empty;
                    var description = parent != null ? parent.Description : string.Empty;
                    var siteMap = db.SiteMaps.FirstOrDefault(c => c.Title.ToLower().Trim() == title.ToLower().Trim() && c.DealerID == dealerId);
                    if (siteMap == null)
                    {
                        siteMap = new SiteMapData.Sitemap
                        {
                            Title = title,
                            Description = description,
                            DealerID = dealerId
                        };
                        db.SiteMaps.Add(siteMap);
                    }
                    siteMapLocation.Sitemap = siteMap;
                }
                db.SiteMapLocations.Add(siteMapLocation);
            }
            return siteMapLocation;
        }

        private static void CreateSubMenu(List<Menu> menus, TreeviewModel menu, int dealerId)
        {
            var items = menus.Where(c => c.ParentId == menu.NodeId && c.DealerID == dealerId).OrderBy(c => c.DisplayOrderId);
            if (items == null || items.Count() == 0)
            {
                return;
            }
            foreach (var item in items)
            {
                var menuModel = CreateTreviewMenuNode(item);
                menu.Nodes.Add(menuModel);
                CreateSubMenu(menus, menuModel, dealerId);
            }
        }

        private static TreeviewModel CreateTreviewMenuNode(Menu root)
        {
            var status = root.IsActive ? "Active" : "InActive";
            var isCMS = root.Url != null && root.Url.StartsWith("/content/");
            return new TreeviewModel()
            {
                Text = string.Format("{0} ({1})", root.Title, status),
                NodeId = root.MenuId,
                Url = root.Url,
                Title = root.Title,
                IsCMS = isCMS
            };
        }
    }
}