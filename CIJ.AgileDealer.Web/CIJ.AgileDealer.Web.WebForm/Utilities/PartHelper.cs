﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Parts;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class PartHelper
    {
        public static bool SavePartRequest(PartRequest partRequest)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    partRequest.CreatedDt = DateTime.Now;
                    partRequest.ModifiedDt = DateTime.Now;
                    partRequest.DealerID = 1;
                    db.PartRequests.Add(partRequest);
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("PartHelper.SavePartRequest() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: SavePartRequest");
            }
            return isSuccess;
        }

        public static PartRequest CreatePartRequest(PartRequest temp,int dealerId)
        {
            PartRequest newItem = new PartRequest();
            newItem.FirstName = temp.FirstName;
            newItem.LastName = temp.LastName;
            newItem.Email = temp.Email;
            newItem.PhoneNumber = temp.PhoneNumber;
            newItem.ContactBy = temp.ContactBy;
            newItem.PostalCode = temp.PostalCode;
            newItem.Year = temp.Year;
            newItem.Make = temp.Make;
            newItem.Model = temp.Model;
            newItem.Vin = temp.Vin;
            newItem.PartNumber = temp.PartNumber;
            newItem.DealerID = dealerId;
            return newItem;
        }
    }
}