﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgileDealer.Data.Entities.Buyer;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class BuyerSessionHelper
    {
        public static string BuyerSessionName = "BuyerSessionStoreId";
        public static BuyerSession CreateNewBuyerSession(HttpRequest request, string sessionId)
        {
            BuyerSession newBuyerSession = new BuyerSession()
            {
                BuyerIdentifier = Guid.NewGuid(),
                SessionId = sessionId,
                IpAddress = HttpHelper.GetCurrentIpAddressOfClient(request),
                BrowserType = HttpHelper.GetBrowserType(request),
                PlatformOs = HttpHelper.GetPlatform(request),
                MobileDeviceManufacturer = HttpHelper.GetMobileDeviceManufacturer(request),
                MobileDeviceModel = HttpHelper.GetMobileDeviceModel(request),
                IsMobile = HttpHelper.IsMobile(request),
                Url = HttpHelper.GetUrl(request) != null ? HttpHelper.GetUrl(request).ToString() : string.Empty,
                UrlReferer = HttpHelper.GetUrlDeferer(request) != null ? HttpHelper.GetUrlDeferer(request).ToString() : string.Empty,
                CreatedDt = DateTime.Now,
                ModifiedDt = DateTime.Now
               
            };

            return newBuyerSession;
        }
        
        public static int GetBuyerSessionBySession(System.Web.SessionState.HttpSessionState Session, HttpRequest request)
        {
            if(Session[BuyerSessionName] == null)
            {
                var buyerSession = CreateNewBuyerSession(request, Session.SessionID);
                SaveBuyerSession(buyerSession, Session);
            }
            int sessionId;
            if (int.TryParse(Session[BuyerSessionName].ToString(), out sessionId))
                return sessionId;
            else
            {
                var el = ExceptionHelper.LogException("BuyerSessionHelper.GetBuyerSessionBySession() Error: ", new Exception("Error getting Buyer Session"), string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: GetBuyerSessionBySession");
                EmailHelper.SendEmailForNotificationWhenFailing(el);
                return int.MinValue;
            }
        }

        public static async Task SaveBuyerSession(BuyerSession buyerSession, System.Web.SessionState.HttpSessionState Session)
        {
            try
            {
                Session[BuyerSessionName] = buyerSession.BuyerSessionId;
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    db.BuyerSessions.Add(buyerSession);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var el = ExceptionHelper.LogException("BuyerSessionHelper.SaveBuyerSession() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: SaveBuyerSession with Id: "+ buyerSession.BuyerSessionId);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }

        public static bool IsBuyerSessionExisted(string sessionId, ServicePageDbContext db)
        {
            bool isExisted = false;
            try
            {
                isExisted = db.BuyerSessions.Any(x => x.SessionId == sessionId);
            }
            catch (Exception ex)
            {
                isExisted = false;
                var el = ExceptionHelper.LogException("BuyerSessionHelper.IsBuyerSessionExisted() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: IsBuyerSessionExisted with Id: "+ sessionId);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
            return isExisted;
        }
    }
}