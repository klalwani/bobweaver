﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.ErrorLog;
using AgileDealer.Data.Entities.ScheduleService;
using CJI.AgileDealer.Web.WebForm.Models;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class EmailHelper
    {
        #region Private
        private static Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerInfoHelper.DealerID);
        static readonly string smtpServer = ConfigurationManager.AppSettings["smtpServer"];
        static readonly int smtpPort = Convert.ToInt16(ConfigurationManager.AppSettings["smtpPort"]);
        static readonly string smtpAcct = ConfigurationManager.AppSettings["smtpAcct"];
        static readonly string smtpAcctPwd = ConfigurationManager.AppSettings["smtpAcctPwd"];
        static private string bccEmail = ConfigurationManager.AppSettings["BCCEmail"];
        static readonly string errorLogEmail = ConfigurationManager.AppSettings["errorLogEmail"];

        public static string CreateMailContentWithTemplate(CustomerQuestion customerQuestion)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + customerQuestion.FirstName + " " + customerQuestion.LastName + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + customerQuestion.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + customerQuestion.PhoneNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Additional Information</td>");
            sb.AppendLine(@"<td>" + customerQuestion.Questions + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }
        public static string CreateMailContentWithTemplate(ScheduleService scheduleService)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Customer Name</td>");
            sb.AppendLine(@"<td>" + scheduleService.FirstName + " " + scheduleService.LastName + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Appointment Date</td>");
            sb.AppendLine(@"<td>" +
                          $"{scheduleService.ScheduleDate:D} {(string.IsNullOrEmpty(scheduleService.SaturdayHour) ? scheduleService.WeeklyHour : scheduleService.SaturdayHour)}" + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Email</td>");
            sb.AppendLine(@"<td>" + scheduleService.Email + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Phone Number</td>");
            sb.AppendLine(@"<td>" + scheduleService.PhoneNumber + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Additional Information</td>");
            sb.AppendLine(@"<td>" + scheduleService.AdditionalInformation + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Vehicle Info</strong></td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Year</td>");
            sb.AppendLine(@"<td>" + scheduleService.Year + "</td>");
            sb.AppendLine(@"</tr>");


            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Make</td>");
            sb.AppendLine(@"<td>" + scheduleService.Make + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td>Model</td>");
            sb.AppendLine(@"<td>" + scheduleService.Model + "</td>");
            sb.AppendLine(@"</tr>");

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }
        public static string CreateMailContentWithTemplateForFinance(ContactStructure applicant)
        {
            StringBuilder sb = new StringBuilder();
            if (applicant.ApplicantInfo != null)
            {
                sb.Append(@"<table class=""body-wrap"">");
                sb.AppendLine(@"<tbody>");
                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td><strong>Customer Info</strong></td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Customer Name</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.FirstName + " " + applicant.ApplicantInfo.LastName + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Email</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.Email + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Phone Number</td>");
                sb.AppendLine(@"<td>" + applicant.ApplicantInfo.PhoneNumber + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td><strong>Address Info</strong></td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>Address</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.CurrentAddress + "</td>");
                sb.AppendLine(@"</tr>");


                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>City</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.City + "</td>");
                sb.AppendLine(@"</tr>");


                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>State</td>");
                sb.AppendLine(@"<td>" + applicant.ContactInfo.State + "</td>");
                sb.AppendLine(@"</tr>");

                sb.AppendLine(@"</tbody>");
                sb.AppendLine(@"</table>");
            }
            return sb.ToString();
        }
        private static Email CreateConfirmationMailContent(string mailTo)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"<h2><strong><span style=""color: #000000;"">Thank you for contacting " + Dealer.Name + "&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">Someone will be contacting your shortly.&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">Sincerely,&nbsp;</span></strong></h2>");
            sb.AppendLine(@"<p>&nbsp;</p>");
            sb.AppendLine(@"<h2><strong><span style=""color: #000000;"">The " + Dealer.Name + " Team</span></strong></h2>");
            Email email = new Email()
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = smtpAcct,
                MailTo = mailTo,
                MailSubject = Dealer.Name,
                MailText = sb.ToString()
            };

            return email;
        }

        #endregion

        #region Public 
        public async static Task SendEmailForNotificationWhenFailing(ErrorLog error)
        {
            string errorLog = JsonConvert.SerializeObject(error);
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = smtpAcct,
                MailTo = errorLogEmail,
                MailSubject = string.Format("Error Log For WebForm: " + error.Message),
                MailText = errorLog
            };

            SendEmailAsync(email, string.Empty, true, string.Empty);
        }

        public static void SendEmailForLeadForFinance(ContactStructure applicant)
        {
            if (applicant != null)
            {
                string content = CreateMailContentWithTemplateForFinance(applicant);
                if (!string.IsNullOrEmpty(content))
                {
                    Email email = new Email()
                    {
                        CreatedUserId = 0,
                        LastChangedUserId = 0,
                        DateCreated = DateTime.Now,
                        DateModified = DateTime.Now,
                        MailBCC = bccEmail,
                        MailFrom = smtpAcct,
                        MailTo = Dealer.EleadTrackAdress,
                        MailSubject = $"An Online Credit Approval has been submitted by {applicant.ApplicantInfo.FirstName} {applicant.ApplicantInfo.LastName}",
                        MailText = content
                    };
                    SendEmail(email, string.Empty, true, bccEmail);
                }
            }
        }

        public static void SendConfirmationEmail(string mailTo, string replyAddress, bool isBodyHtml = true)
        {
            string mailContent = EmailGenerator.CreateConfirmationMailContent(mailTo);
            if (!string.IsNullOrEmpty(mailContent))
            {
                Email email = CreateConfirmationMailContent(mailTo);
                if (email != null)
                {
                    SendEmailAsync(email, replyAddress, isBodyHtml);
                }
            }
        }


        public static async void SendEmailAsync(Email em, string replyAddress, bool isBodyHtml = true, string bcc = "")
        {
            if (string.IsNullOrEmpty(bcc))
            {
                em.MailBCC = bccEmail;
            }
            MailMessage mm = new MailMessage();
            mm.Body = em.MailText;

            if (!string.IsNullOrEmpty(em.MailCC))
            {
                em.MailCC = em.MailCC.Replace(";", ",");
                foreach (var item in em.MailCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.CC.Add(item.Trim());
                }
            }
            if (!string.IsNullOrEmpty(em.MailBCC))
            {
                em.MailBCC = em.MailBCC.Replace(";", ",");
                foreach (var item in em.MailBCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.Bcc.Add(item.Trim());
                }
            }
            mm.From = new MailAddress(smtpAcct);

            if (!string.IsNullOrEmpty(em.MailTo))
            {
                em.MailTo = em.MailTo.Replace(";", ",");
                foreach (var item in em.MailTo.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.To.Add(item.Trim());
                }
            }

            mm.IsBodyHtml = isBodyHtml;
            if (em.MailFrom != null)
                mm.Sender = mm.From;
            mm.Subject = em.MailSubject;

            using (var client = new SmtpClient())
            {
                client.Port = smtpPort;
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;//important: This line of code must be executed before setting the NetworkCredentials object, otherwise the setting will be reset (a bug in .NET)
                System.Net.NetworkCredential cred = new System.Net.NetworkCredential(smtpAcct, smtpAcctPwd);
                client.Credentials = cred;
                await client.SendMailAsync(mm);
            }
        }

		public static async Task SendCMSEmail(string emailTo, string emailCC, string emailBCC, string title, string body)
		{
			MailMessage mm = new MailMessage();
			mm.CC.Add(emailCC);
			mm.Bcc.Add(emailBCC);
			mm.To.Add(emailTo);
			mm.Subject = title;
			mm.Sender = mm.From = new MailAddress(smtpAcct);
			mm.Body = body;
			using (var client = new SmtpClient())
			{
				client.Port = smtpPort;
				client.EnableSsl = true;
				client.UseDefaultCredentials = false;//important: This line of code must be executed before setting the NetworkCredentials object, otherwise the setting will be reset (a bug in .NET)
				System.Net.NetworkCredential cred = new System.Net.NetworkCredential(smtpAcct, smtpAcctPwd);
				client.Credentials = cred;
				await client.SendMailAsync(mm);
			}
		}

		public static void SendEmail(Email em, string replyAddress, bool isBodyHtml = true, string bcc = "")
        {
            if (string.IsNullOrEmpty(bcc))
            {
                em.MailBCC = bccEmail;
            }
            MailMessage mm = new MailMessage();
            mm.Body = em.MailText;

            if (!string.IsNullOrEmpty(em.MailCC))
            {
                em.MailCC = em.MailCC.Replace(";", ",");
                foreach (var item in em.MailCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.CC.Add(item.Trim());
                }
            }
            if (!string.IsNullOrEmpty(em.MailBCC))
            {
                em.MailBCC = em.MailBCC.Replace(";", ",");
                foreach (var item in em.MailBCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.Bcc.Add(item.Trim());
                }
            }
            mm.From = new MailAddress(smtpAcct);

            if (!string.IsNullOrEmpty(em.MailTo))
            {
                em.MailTo = em.MailTo.Replace(";", ",");
                foreach (var item in em.MailTo.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.To.Add(item.Trim());
                }
            }

            mm.IsBodyHtml = isBodyHtml;
            if (em.MailFrom != null)
                mm.Sender = mm.From;
            mm.Subject = em.MailSubject;

            SmtpClient client = new SmtpClient(smtpServer);
            client.Port = smtpPort;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;//important: This line of code must be executed before setting the NetworkCredentials object, otherwise the setting will be reset (a bug in .NET)
            System.Net.NetworkCredential cred = new System.Net.NetworkCredential(smtpAcct, smtpAcctPwd);
            client.Credentials = cred;
            client.Send(mm);
        }

        #endregion
    }
}