﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.ContactUs;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class ContactUsHelper
    {
        public static List<CustomerQuestion> GetContactUss(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.CustomerQuestions.Where(x => x.DealerID == dealerId).ToList();
            }
        }

        public static CustomerQuestion GetContactUsById(int id, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.CustomerQuestions.FirstOrDefault(x => x.CustomerQuestionId == id && x.DealerID == dealerId);
            }
        }

    }
}