﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.Base.Context;
using CIJ.AgileDealer.Core;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class NotificationHelper
    {
        private const string NotiticationCachedKey = "_notiticationCachedKey";

        public static List<NotificationSetting> GetNotificationSettings(int dealerId)
        {
            List<NotificationSetting> settings = new List<NotificationSetting>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                settings = db.NotificationSettings.Where(x => x.DealerID == dealerId).ToList();
            }
            return settings;
        }

        public static List<NotificationSetting> GetNotificationSettings()
        {
            List<NotificationSetting> settings = new List<NotificationSetting>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                settings = db.NotificationSettings.ToList();
            }
            return settings;
        }

        public static void SaveNotificationSettings(List<NotificationSetting> settings,int dealerId)
        {
            if (settings != null && settings.Any())
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    for (int i = 0; i < settings.Count; i++)
                    {
                        settings[i].ModifiedDt = DateTime.Now;
                        db.Entry(settings[i]).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}