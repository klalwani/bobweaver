﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class CheckAvailabilityHelper
    {
        public static List<VehicleFeedBackModel> GetVehicleFeedBacks(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                List<VehicleFeedBackModel> temp = (from vehicleFeedback in db.VehicleFeedBacks
                                                   join vehicle in db.AgileVehicles on vehicleFeedback.AgileVehicleId.ToString() equals vehicle.AgileVehicleId.ToString()
                                                   join dealer in db.Dealers on vehicle.DealerID equals dealer.DealerID
                                                   select new VehicleFeedBackModel()
                                                   {
                                                       AgileVehicleId = vehicle.AgileVehicleId.ToString(),
                                                       Vin = vehicle.Vin,
                                                       DealerID = dealer.DealerID,
                                                       DealerName = dealer.Name,
                                                       VehicleFeedBackId = vehicleFeedback.VehicleFeedBackId,
                                                       Name = vehicleFeedback.Name,
                                                       Phone = vehicleFeedback.Phone,
                                                       Email = vehicleFeedback.Email,
                                                       Message = vehicleFeedback.Message,
                                                       SubmittedDate = vehicleFeedback.SubmittedDate
                                                   }).ToList();

                return temp;
            }
        }

        public static VehicleFeedBackModel GetVehicleFeedBackById(int id, int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {

                VehicleFeedBackModel temp = (from vehicleFeedback in db.VehicleFeedBacks
                                             join vehicle in db.AgileVehicles on vehicleFeedback.AgileVehicleId.ToString() equals vehicle.AgileVehicleId.ToString()
                                             where vehicleFeedback.VehicleFeedBackId == id
                                             join dealer in db.Dealers on vehicle.DealerID equals dealer.DealerID
                                                   select new VehicleFeedBackModel()
                                                   {
                                                       AgileVehicleId = vehicle.AgileVehicleId.ToString(),
                                                       Vin = vehicle.Vin,
                                                       DealerID = dealer.DealerID,
                                                       DealerName = dealer.Name,
                                                       VehicleFeedBackId = vehicleFeedback.VehicleFeedBackId,
                                                       Name = vehicleFeedback.Name,
                                                       Phone = vehicleFeedback.Phone,
                                                       Email = vehicleFeedback.Email,
                                                       Message = vehicleFeedback.Message,
                                                       SubmittedDate = vehicleFeedback.SubmittedDate
                                                   }).FirstOrDefault();
                return temp;
            }
        }
    }
}