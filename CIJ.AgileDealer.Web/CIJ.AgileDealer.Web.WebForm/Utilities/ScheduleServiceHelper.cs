﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.ScheduleService;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public static class ScheduleServiceHelper
    {
        public static List<ScheduleService> GetScheduleServices(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                return db.ScheduleServices.Where(x => x.DealerID == dealerId).ToList();
            }
        }

        public static ScheduleService GetScheduleServiceById(int id, int dealerId)
        {
            ScheduleService result = new ScheduleService();
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                result = db.ScheduleServices.FirstOrDefault(x => x.ScheduleServiceId == id && x.DealerID == dealerId);
                if (result != null)
                {
                    result.ScheduleDateString = result.ScheduleDate.ToShortDateString();
                    result.CurrentIssueList = db.ScheduleCurrentIssues.Where(x => x.ScheduleServiceId == result.ScheduleServiceId && x.DealerID == dealerId).Select(a => a.CurrentIssueId).ToList();
                    result.ServiceRequestList = db.ScheduleServiceRequesteds.Where(x => x.ScheduleServiceId == result.ScheduleServiceId && x.DealerID == dealerId).Select(a => a.ServiceRequestedId).ToList();
                }
            }
            return result;
        }
    }
}