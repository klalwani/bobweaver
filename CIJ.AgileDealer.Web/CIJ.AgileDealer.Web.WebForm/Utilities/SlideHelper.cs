﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Home;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using System.Data.Entity;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public class SlideHelper
    {
        private HomePageSliderEntry HomePageSliderEntry;
        public SlideHelper(int dealerId)
        {
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                HomePageSliderEntry = db.HomePageSliderEntrys.Where(x=>x.DealerID == dealerId).First();
            }
        }

        public List<HomePageSlide> GetHomePageSlides(int skip, int take,int dealerId)
        {
            List<HomePageSlide> results = new List<HomePageSlide>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                results =
                    db.HomePageSlides.Where(x=>x.DealerID == dealerId).OrderBy(x => x.SlideNumber).Skip(skip).Take(take).ToList();

                if (results.Any())
                {
                    int count = results.Count;
                    for (int i = 0; i < count; i++)
                    {
                        results[i].IsFirstItem = db.HomePageSlides.Min(a => a.SlideNumber) == results[i].SlideNumber;
                        results[i].IsLastItem = db.HomePageSlides.Max(a => a.SlideNumber) == results[i].SlideNumber;
                    }
                }
            }
            return results;
        }

        public HomePageSlide GetHomePageSlideById(int id,int dealerId)
        {
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                return db.HomePageSlides.FirstOrDefault(x => x.SlideId == id && x.DealerID == dealerId);
            }
        }

        public int GetHomePageSlideCount(int dealerId)
        {
            int count = 0;
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                count = db.HomePageSlides.Count(x=>x.DealerID == dealerId);
            }
            return count;
        }

        public HomePageSlide CreateSlider(string slideImageName, string slideImagePath, string slideLink, string slideRelativeUrl,int dealerId)
        {
            int newSlideId = 0;
            int newSliderNumber = 0;
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                newSlideId = db.HomePageSlides.Max(x => x.SlideId) + 1;
                newSliderNumber = db.HomePageSlides.Max(x => x.SlideNumber) + 1;
            }


            HomePageSlide newItem = new HomePageSlide();
            newItem.SlideId = newSlideId;
            newItem.SlideImageName = slideImageName;
            newItem.SlideImagePath = slideImagePath;
            newItem.SlideLink = slideLink;
            newItem.SlideRelativeUrl = slideRelativeUrl;
            newItem.SlideEntryId = HomePageSliderEntry != null ? HomePageSliderEntry.SliderEntryId : 0;
            newItem.DealerID = dealerId;
            newItem.SlideNumber = newSliderNumber;
            return newItem;
        }

        public bool UpdateHomePageSlide(int slideId, string slideImageName,
            string slideImagePath, string slideLink, string slideRelativeUrl,int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    HomePageSlide currentItem = db.HomePageSlides.FirstOrDefault(x => x.SlideId == slideId && x.DealerID == dealerId);
                    if (currentItem != null)
                    {
                        currentItem.SlideImageName = slideImageName;
                        currentItem.SlideImagePath = slideImagePath;
                        currentItem.SlideLink = slideLink;
                        currentItem.SlideRelativeUrl = slideRelativeUrl;
                        db.Entry(currentItem).State = EntityState.Modified;
                        db.SaveChanges();
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("SlideHelper.UpdateHomePageSlide() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: UpdateHomePageSlide");
            }
            return isSuccess;
        }

        public bool SaveHomePageSlide(HomePageSlide newItem)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    db.HomePageSlides.Add(newItem);
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("SlideHelper.SaveHomePageSlide() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: SaveHomePageSlide");
            }
            return isSuccess;
        }

        public bool DeleteHomePageSlide(int id,int dealerId)
        {
            bool isSuccess = false;
            try
            {
                if (id > 0)
                {
                    using (ServicePageDbContext db = ServicePageDbContext.Create())
                    {
                        HomePageSlide deleteItem =
                            db.HomePageSlides.FirstOrDefault(x => x.SlideId == id && x.DealerID == dealerId);
                        if (deleteItem != null)
                        {
                            db.Entry(deleteItem).State = EntityState.Deleted;
                            db.SaveChanges();
                            isSuccess = true;
                        }
                    }

                    //Update SlideNumber for all the slides
                    using (ServicePageDbContext db = ServicePageDbContext.Create())
                    {
                        var SliderItems = db.HomePageSlides.Where(x => x.DealerID == dealerId);
                        int CurrentSlideNumber = 1;
                        if (SliderItems != null)
                        {
                            foreach (var sliderItem in SliderItems)
                            {
                                sliderItem.SlideNumber = CurrentSlideNumber;
                                db.Entry(sliderItem).State = EntityState.Modified;
                                CurrentSlideNumber++;
                            }
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("CustomRedirectInfo.DeleteCustomRedirectItem() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: DeleteCustomRedirectItem");
            }
            return isSuccess;
        }

        public bool UpdateHomePageSlideOrder(int id,int currentIndex,int newIndex,string currentCommand, int dealerId)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    HomePageSlide currentItem = db.HomePageSlides.FirstOrDefault(x => x.SlideId == id && x.DealerID == dealerId);
                    HomePageSlide switchItem=new HomePageSlide();
                    if (currentCommand == "Up")
                    {
                        switchItem = db.HomePageSlides.Where(x => x.SlideNumber < currentItem.SlideNumber && x.DealerID == dealerId).OrderByDescending(x=>x.SlideNumber).FirstOrDefault();
                    }
                    else if (currentCommand == "Down")
                    {
                        switchItem = db.HomePageSlides.Where(x => x.SlideNumber > currentItem.SlideNumber && x.DealerID == dealerId).OrderBy(x => x.SlideNumber).FirstOrDefault();
                    }
                    currentIndex = currentItem.SlideNumber;
                    newIndex = switchItem.SlideNumber;
                    int minOrder = db.HomePageSlides.Where(x=>x.DealerID == dealerId).Min(x => x.SlideNumber);
                    int maxOrder = db.HomePageSlides.Where(x => x.DealerID == dealerId).Max(x => x.SlideNumber);
                    if (currentItem != null && switchItem != null && currentItem.SlideNumber != switchItem.SlideNumber)
                    {
                        if (currentCommand == "Up")
                        {
                            if (currentIndex > minOrder)
                            {
                                currentItem.SlideNumber = newIndex;
                                switchItem.SlideNumber = currentIndex;
                            }
                        }
                        else if (currentCommand == "Down")
                        {
                            if (currentIndex < maxOrder)
                            {
                                currentItem.SlideNumber = newIndex;
                                switchItem.SlideNumber = currentIndex;
                            }
                        }

                        db.Entry(currentItem).State = EntityState.Modified;
                        db.Entry(switchItem).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("SlideHelper.UpdateHomePageSlideOrder() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: UpdateHomePageSlideOrder");
            }
            return isSuccess;
        }
    }
}