﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using Microsoft.AspNet.Identity;

namespace CJI.AgileDealer.Web.WebForm.Utilities
{
    public class CustomRedirectInfoHelper
    {
        public CustomRedirectInfoHelper()
        {

        }

        public List<CustomRedirectUrl> GetCustomRedirectUrls(int skip, int take, int dealerId)
        {
            List<CustomRedirectUrl> results = new List<CustomRedirectUrl>();
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                results = db.CustomRedirectUrls.Where(x=>x.DealerID == dealerId).OrderByDescending(x=>x.ModifiedDt).ThenBy(x => x.CustomRedirectUrlId).Skip(skip).Take(take).ToList();
            }
            return results;
        }

        public int GetCustomRedirectCount(int dealerId)
        {
            int count = 0;
            using (ServicePageDbContext db = ServicePageDbContext.Create())
            {
                count = db.CustomRedirectUrls.Count(x => x.DealerID == dealerId);
            }
            return count;
        }

        public CustomRedirectUrl CreateCustomRedirectInfo(string name, string oldUrl, string newUrl, bool isPermanentRedirect, int dealerId)
        {
            CustomRedirectUrl newItem = new CustomRedirectUrl();
            newItem.Name = name;
            newItem.OldUrl = oldUrl;
            newItem.NewUrl = newUrl;
            newItem.IsPermanentRedirect = isPermanentRedirect;
            newItem.CreatedDt = DateTime.Now;
            newItem.CreatedID = HttpContext.Current.User.Identity.GetUserId();
            newItem.ModifiedDt = DateTime.Now;
            newItem.ModifiedID = HttpContext.Current.User.Identity.GetUserId();
            newItem.DealerID = dealerId;

            return newItem;
        }

        public bool SaveCustomRedirectUrl(CustomRedirectUrl newItem)
        {
            bool isSuccess = false;
            try
            {
                using (ServicePageDbContext db = ServicePageDbContext.Create())
                {
                    db.CustomRedirectUrls.Add(newItem);
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("CustomRedirectInfo.SaveCustomRedirectUrl() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: SaveCustomRedirectUrl");
            }
            return isSuccess;
        }

        public bool DeleteCustomRedirectItem(int id,int dealerId)
        {
            bool isSuccess = false;
            try
            {
                if (id > 0)
                {
                    using (ServicePageDbContext db = ServicePageDbContext.Create())
                    {
                        CustomRedirectUrl deleteItem =
                            db.CustomRedirectUrls.FirstOrDefault(x => x.CustomRedirectUrlId == id && x.DealerID == dealerId);
                        if (deleteItem != null)
                        {
                            db.Entry(deleteItem).State = EntityState.Deleted;
                            db.SaveChanges();
                            isSuccess = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                ExceptionHelper.LogException("CustomRedirectInfo.DeleteCustomRedirectItem() Error: ", ex,
                    string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm",
                    "Error when execute method: DeleteCustomRedirectItem");
            }
            return isSuccess;
        }
    }
}