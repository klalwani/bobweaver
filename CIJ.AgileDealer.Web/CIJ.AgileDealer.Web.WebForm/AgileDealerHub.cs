﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using CJI.AgileDealer.Web.WebForm.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace CJI.AgileDealer.Web.WebForm
{
    [HubName("agileDealerHub")]
    public class AgileDealerHub : Hub
    {
        public static List<UserDetail> UserDetails = new List<UserDetail>();

        public void Hello()
        {
            Clients.All.hello();
        }

        public void IsLogin()
        {
            var user = Context.User;
            var connectionId = Context.ConnectionId;
            if (!user.Identity.IsAuthenticated)
            {
                Clients.Caller.login(connectionId);
            }
            else
            {
                if (UserDetails.All(x => x.ConnectionId != connectionId))
                {
                    UserDetails.Add(new UserDetail
                    {
                        ConnectionId = connectionId,
                        UserName = user.Identity.Name
                    });
                }
                ConnectToChatRoom(user.Identity.Name, connectionId);
            }
        }

        public void ConnectToChatRoom(string userName, string connectionId)
        {
            if (UserDetails.All(x => x.ConnectionId != connectionId))
            {
                UserDetails.Add(new UserDetail
                {
                    ConnectionId = connectionId,
                    UserName = userName
                });
            }
            Clients.Caller.onConnected(UserDetails, userName, connectionId);
            Clients.AllExcept(connectionId).onNewUserConnected(connectionId, userName);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var item = UserDetails.FirstOrDefault(c => c.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                UserDetails.Remove(item);
            }
            return base.OnDisconnected(true);
        }

        public void SendPrivateMessage(string toUserId, string message)
        {
            var fromUserId = Context.ConnectionId;
            var fromUser = UserDetails.FirstOrDefault(c => c.ConnectionId == fromUserId);
            var toUser = UserDetails.FirstOrDefault(c => c.ConnectionId == toUserId);
            if (toUser != null && fromUser != null)
            {
                Clients.Client(toUserId).sendPrivateMessage(fromUserId, fromUser.UserName, message);

                // send to caller user
                Clients.Caller.sendPrivateMessage(toUserId, fromUser.UserName, message);
            }
        }

        public void SendToAll(string message)
        {
            var fromUserId = Context.ConnectionId;
            var fromUser = UserDetails.FirstOrDefault(c => c.ConnectionId == fromUserId);
            if (fromUser != null) Clients.All.sendToAll(fromUser.UserName,message);
        }
    }
}