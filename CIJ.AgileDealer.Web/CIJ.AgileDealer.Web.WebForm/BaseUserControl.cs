﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CJI.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm
{
    public class BaseUserControl : System.Web.UI.UserControl, IBaseWebItem 
    {
        public int DealerID { get { return DealerInfoHelper.DealerID; } }

        public string DealerCity { get { return DealerInfoHelper.DealerCity; } }

        public string DealerState { get { return DealerInfoHelper.DealerState; } }

        public string DealerName { get { return DealerInfoHelper.DealerName; } }

        public string DealerImageGeneric { get { return DealerInfoHelper.DealerImageGeneric; } }

        public string ImageServer { get { return DealerInfoHelper.ImageServer; } }

        public string ManufacturerName { get { return DealerInfoHelper.ManufactureName; } }

        public string EleadTrackAdress { get { return DealerInfoHelper.EleadTrackAdress; } }

        public int LanguageId { get { return SiteInfoHelper.GetLanguageId(); } }
    }
}