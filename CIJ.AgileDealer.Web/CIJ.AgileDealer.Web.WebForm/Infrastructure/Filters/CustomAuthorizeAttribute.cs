﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CJI.AgileDealer.Web.Base.Models;
using Microsoft.AspNet.Identity;

namespace CJI.AgileDealer.Web.WebForm.Infrastructure.Filters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public CustomAuthorizeAttribute(params object[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }

        public override void OnAuthorization(AuthorizationContext context)
        {
            base.OnAuthorization(context);
            CheckIfUserIsAuthenticated(context);
        }

        private void CheckIfUserIsAuthenticated(AuthorizationContext context)
        {
            bool authorized = Roles.Any(role => HttpContext.Current.User.IsInRole(role.ToString()));

            if (!authorized)
            {
                var url = new UrlHelper(context.RequestContext);
                var logonUrl = $"../Errors/ErrorPage.aspx?error=" + "You do not have permission to get into this page, please contact your administrator for support!";
                context.Result = new RedirectResult(logonUrl);
            }
        }
    }
}