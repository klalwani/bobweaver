﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Service.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Services.Service" %>

<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Services" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

        <asp:FormView runat="server" ID="serviceForm" DefaultMode="ReadOnly" DataKeyNames="ServicePageID" RenderOuterTable="false">
            <ItemTemplate>
                <!--Display template start here-->
                <div class="top-container row mdl-bottom-shadow" style="background: #fff; background-color: #fff;">
                    <div class="inner-container">

                            <div class="col-md-8">
                                <h1><%=string.Format(Service.Body_Content_Card_H1,DealerName)%></h1>
                            </div>
                            <div class="col-md-4">
                                <div class="svc_header">
                                    <h5><%=Service.Svc_Header_H5_1%></h5>
                                    <a href="/service/appointment"><i class="glyphicon glyphicon-calendar"></i></a>
                                </div>
                            </div>

                    </div>
                </div>


                <div id="services-page" class="row">
                    <div class="body-content-wrapper">
                        <div class="body-content-card">
                            <%-- 
                            <div class="col-md-12" style="border-bottom: 1px solid #ccc; margin-bottom: 2rem; background-color: #1c394f; color: #fff; padding: 20px;">
                                <h2><%=string.Format(Service.Panel_Title,DealerName) %> </h2>
                            </div>
                                --%>


                            <div class="container-fluid">
                                <div class="row padded-15">
                                    <div class="col-lg-12 col-xs-12 bcc-content-block" id="">
                                        <%=string.Format(Service.Row_Padded_15_Bcc_Content_Block,DealerName, ManufacturerName) %>
                                        <%-- <%=string.Format(Service.Row_Padded_15_Bcc_Content_Block_1,DealerName) %> --%>
                                    </div>                      
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="row svc_item">
                                            <div class="col-md-4">
                                                <!--<a href="/service/oil-change">-->
                                                    <img runat="server" src='<%#Eval("OilImage")%>' /><!--</a>-->
                                            </div>
                                            <div class="col-md-8">
                                                <!--<a href="/service/oil-change">-->
                                                    <h3><%=Service.Col_Md_8_H3 %></h3>
                                                <!--</a>-->
                                                <p class="role"><%#HttpUtility.HtmlDecode((string)Eval("OilContent"))%></p>
                                            </div>
                                        </div>
                                        <div class="row svc_item ">
                                            <div class="col-md-4">
                                                <!--<a href="/service/brakes">-->
                                                    <img runat="server" src='<%#Eval("BrakeImage")%>' /><!--</a>-->
                                            </div>
                                            <div class="col-md-8">
                                                <!--<a href="/service/brakes">-->
                                                    <h3><%=Service.Col_Md_8_H3_1 %></h3>
                                                <!--</a>-->
                                                <p class="role"><%#HttpUtility.HtmlDecode((string)Eval("BrakeContent"))%></p>
                                            </div>
                                        </div>
                                        <div class="row svc_item">
                                            <div class="col-md-4">
                                                <!--<a href="/service/tire-center">-->
                                                    <img runat="server" src='<%#Eval("TireImage")%>' /><!--</a>-->
                                            </div>
                                            <div class="col-md-8">
                                                <!--<a href="/service/tire-center">-->
                                                    <h3><%=Service.Col_Md_8_H3_2 %></h3>
                                                <!--</a>-->
                                                <p class="role"><%#HttpUtility.HtmlDecode((string)Eval("TireContent"))%></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-md-offset-1 svc_hours mdl-gray-box">
                                        <h4><%=Service.Mdl_Gray_Box_H4 %></h4>
                                        <table>
                                            <asp:Repeater ID="rpterSvcHour" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="hour1"><%#Eval("ShortDayName")%>:&nbsp;</td>

                                                        <td class="hour2"><%# (Eval("OperationStatus") != null ? Eval("OperationStatus") :  Eval("StartTime"))%></td>
                                            
                                                        <td class="hour3">&nbsp;<%# Eval("EndTime")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                        <br>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



        </ItemTemplate>
    </asp:FormView>


</asp:Content>
