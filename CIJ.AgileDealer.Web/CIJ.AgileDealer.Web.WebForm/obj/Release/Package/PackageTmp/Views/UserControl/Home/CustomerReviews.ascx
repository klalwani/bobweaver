﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerReviews.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Home.CustomerReviews" %>
<%@ OutputCache Duration="300" VaryByParam="None" %>

<div class="review-list-wrap">
    <ul class="review-list">
        <asp:Repeater runat="server" ID="rptCustomerReview">
            <ItemTemplate>
                <li class="review">
                    <div class="row">
                        <div class="post">
                            <%# Eval("ReviewText") %>
                        </div>
                        <div class="star-rating">
                            <%#CreateStarRating(Eval("StartRating")) %>
                        </div>
                        <div class="author">
                            <%# Eval("ReviewName") %>
                        </div>
                    </div>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
