﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentCalculator.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.PaymentCalculator" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.PaymentCalculator" %>
<script src="../../Scripts/Pages/AgileDealer.PaymentCalculator.js"></script>
<input type="hidden" data-attribute="validation" name="ValidationError" value="<%=PaymentCalculator.ValidationError %>" />
<input type="hidden" data-attribute="validation" name="SuccessMessage" value="<%=PaymentCalculator.SuccessMessage %>" />
<div id="payment-calculator-control" class="">
    <div class="row text-center check-availability-title" id="payment-calculator-title">
        <div class="close-button">
        <h2  id="lblTitle"><%=PaymentCalculator.Inner_Container_Col_Md_12_H1 %></h2>
            <input type="button" value="x" id="btnClosePayment" class="btn" />
        </div>
    </div>
    <div class="detail">
        <div class="col-sm-4 col-xs-12 padded-15">
            <div class="form-group">
                <label><%=PaymentCalculator.Label_Group_Label_1 %></label>
            </div>
            <div class="form-group">
                <label><%=PaymentCalculator.Label_Group_Label_2 %></label>
                <input class="form-control" type="text" id="txtVehiclePrice" placeholder="20,000"/>
            </div>

            <div class="form-group">
                <label><%=PaymentCalculator.Label_Group_Label_3 %></label>
                <input class="form-control" type="text" id="txtInterestRate" placeholder="4.5%" />
            </div>

            <div class="form-group">
                <label><%=PaymentCalculator.Label_Group_Label_4 %></label>
                <input class="form-control" type="text" id="txtDownPayment" placeholder="5,000"/>
            </div>

            <div class="form-group">
                <label><%=PaymentCalculator.Label_Group_Label_5 %></label>
                <input class="form-control" type="text" id="txtTradeInValue"  placeholder="5,000"/>
            </div>


        </div>
        <div class="col-sm-4 col-xs-12 padded-15 text-center">

                <div class="form-group">
                    <label><%=PaymentCalculator.Label_Group_Label_6 %></label>
                </div>
                <div class="form-group">
                    <asp:Repeater runat="server" ID="rptPeriodType">
                        <ItemTemplate>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="periodType" value="<%# Eval("TermInMonths") %>"><%# Eval("PeriodTypeName") %></input>
                                </label>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            
            <div class="form-group padded-15 text-center">
                <input type="button" class="btn btn-primary" id="btnCalculate" title="<%=PaymentCalculator.Label_Group_Text_Center_Input_Title %>" value="<%=PaymentCalculator.Label_Group_Text_Center_Input_Value %>" />
            </div>         
            

        </div>

        <div class="col-sm-4 col-xs-12 padded-15">
            <div class="">
                <div class="label-group text-center">
                    <label id="lblResult"></label>
                </div>
                <div class="label-group text-center padded-15">
                    <input type="button" class="btn btn-primary" id="btnCloseBot" title="<%=PaymentCalculator.Btn_Primary_Title %>" value="<%=PaymentCalculator.Btn_Primary_Value %>" />
                </div>
                <div class="form-group padded-15">
                    <p>Values provided are estimates for monthly payment based on standard equipment which may vary from vehicle to vehicle.</p>
                    <p>All payments are estimates. Tax, title, registration and dealer documentation fees are extra.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:HiddenField runat="server" ID="HdfIsHideHeader"/>
<div class="clear-20"></div>