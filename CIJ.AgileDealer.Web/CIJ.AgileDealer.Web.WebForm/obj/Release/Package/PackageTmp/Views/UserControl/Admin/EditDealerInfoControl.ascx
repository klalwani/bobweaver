﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditDealerInfoControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.EditDealerInfoControl" %>
<script src="../../../Scripts/Pages/Admin/AgileDealer.EditDealerInfo.js" type="text/javascript"></script>
<div class="form-horizontal">
	<div class="form-group">
        <label class="col-md-2 control-label">Dealer Name</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="dealerName" placeholder="Dealer Name" value="">
		</div>
	</div>
	<div class="form-group">
        <label class="col-md-2 control-label">Dealer City</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="dealerCity" placeholder="Dealer City" value="">
		</div>
	</div>
	<div class="form-group">
        <label class="col-md-2 control-label">Dealer State</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="dealerState" placeholder="Dealer State" value="">
		</div>
	</div>
	<div class="form-group">
        <label class="col-md-2 control-label">Generic Image Server</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="genericImageServer" placeholder="Generic Image Server" value="">
		</div>
	</div>
	<div class="form-group">
        <label class="col-md-2 control-label">Stock Image Server</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="stockImageServer" placeholder="Stock Image Server" value="">
		</div>
	</div>
	<div class="form-group">
        <label class="col-md-2 control-label">Image Server</label>
		<div class="col-md-10">
            <input type="text" class="form-control" data-val="true" data-val-required="true" id="imageServer" placeholder="Image Server" value="">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
             <input type="button" class="btn btn-primary" id="btnSave" value="Save" />
		</div>
	</div>
</div>
