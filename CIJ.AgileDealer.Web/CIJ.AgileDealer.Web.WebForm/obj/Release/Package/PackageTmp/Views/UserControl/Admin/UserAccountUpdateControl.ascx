﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAccountUpdateControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.UserAccountUpdateControl" %>

<div class="container" id="userAccountUpdateControl">
    <div class="container row">
        <div class="row form-title">
            <span>User Account Update Page</span>
        </div>
        <div class="col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
            <div class="row content-padded">
                <div id="contactName">
                    <div class="col-md-12">
                        <fieldset class="form-group rowElem">
                            <label for="userName" class="sr-only">User Name</label>
                            <input type="text" class="form-control" data-val="true"  id="userName" runat="server" placeholder="User Name" />
                        </fieldset>
                    </div>

                    <div class="col-md-12">
                        <fieldset class="form-group rowElem">
                            <label for="email" class="sr-only">Email</label>
                            <input type="text" class="form-control" data-val="true" id="email" runat="server" placeholder="Email" />
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group rowElem">
                            <label for="phoneNumber" class="sr-only">Phone Number</label>
                            <input type="text" class="form-control" data-val="true"  id="phoneNumber" runat="server" placeholder="Phone Number" />
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group rowElem">
                            <%--<label for="currentRole" class="sr-only">Current Role</label>--%>
                            <asp:DropDownList ID="currentRoleData" CssClass="form-control" runat="server"
                                DataTextField="Name" DataValueField="IdString" ToolTip="Current Role">
                            </asp:DropDownList>
                            <%--<select class="form-control" runat="server" data-val="true" data-val-required="true" id="currentRole" placeholder="Current Role">
                                <asp:Repeater ID="rptRoles" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("IdString")%>"><%#Eval("Name")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>--%>
                        </fieldset>
                    </div>
                    <div class="row col-md-12">
                        <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_OnServerClick" CssClass="btn btn-primary" Text="Save Changes"/>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
