﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckAvailability.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.CheckAvailability" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.CheckAvailability" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.ScheduleService" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Common" %>
<script>
    $(document).ready(function () {

        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";


        $('#txtDate').datepicker(
        {
            minDate: +1,
            maxDate: +14,
            beforeShowDay: function (date) {
                var day = date.getDay();
                return [(day != 0), ''];
            },
            onSelect: function (dateText, inst) {
                var datePicker = $(this);
                if (typeof datePicker != "undefined" && datePicker != null) {
                    var saturdayTimeSelect = $('#saturday-hours');
                    var date = datePicker.datepicker('getDate');
                    var dayOfWeek = weekday[date.getDay()];
                    if (dayOfWeek == "Saturday") {
                        $("#saturday-hours").removeClass("hide");
                        $("#weekday-hours").addClass("hide");
                        $(".divhours").removeClass("hide");
                        $("#weekday-hours").val("");
                    } else {
                        $("#saturday-hours").addClass("hide");
                        $("#weekday-hours").removeClass("hide");
                        $(".divhours").removeClass("hide");
                        $("#saturday-hours").val("");
                    }
                }
                datePicker.blur();
            }
        });


        $('[id$="btnSend"').click(function () {
            if (validateData()) {
                submitUserMessage();
                $('#check-availability-control').modal('hide');
                resetValueForTextBox();
            }
        });

        $('[id$="btnClose"').click(function () {
            resetValueForTextBox();
            $('#check-availability-control').modal('hide');
        });

        function validateData() {
            var isValid = true;
            isValid = validateTextBox($('[id$="txtFullName"'));
            isValid = validateTextBox($('[id$="txtPhone"'));
            isValid = validatePhone($('[id$="txtPhone"'));
            isValid = validateTextBox($('[id$="txtMessage"'));
            isValid = validateTextBox($('[id$="txtEmail"'));
            isValid = validateTextBox($('[id$="txtPostalCode"'));
            isValid = validateEmail($('[id$="txtEmail"'));
            return isValid;
        }

        function testPattern(input, pattern) {
            var isValid = true;
            if (pattern.test(input.val())) {
                input.removeClass('input-validation-error');
                isValid = true;
            }
            else {
                input.addClass('input-validation-error');
                isValid = false;
            }
            return isValid;
        };

        function validateEmail(input) {
            var isValid = true;
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            isValid = testPattern(input, pattern);
            return isValid;
        };

        function validatePhone(input) {
            var isValid = true;
            var pattern = /^[0-9-+]+$/;
            isValid = testPattern(input, pattern);
            return isValid;
        };


        function validateTextBox(input) {
            var isValid = true;
            if (typeof input != "undefined" && input != null) {
                if (input.val() === '') {
                    input.addClass('input-validation-error');
                    isValid = false;
                } else {
                    input.removeClass('input-validation-error');
                    isValid = true;
                }
            }
            return isValid;
        };

        function resetValueForTextBox() {
            $('[id$="txtFullName"').val('').removeClass('input-validation-error');
            $('[id$="txtPhone"').val('').removeClass('input-validation-error');
            $('[id$="txtEmail"').val('').removeClass('input-validation-error');
            $('[id$="txtMessage"').val('').removeClass('input-validation-error');
            $('[id$="txtPostalCode"').val('').removeClass('input-validation-error');
            $('[id$="txtDate"').val('');
            $('[id$="weekday-hours"').val('');
            $('[id$="saturday-hours"').val('');
        };

        function submitUserMessage() {
            var name = $('[id$="txtFullName"').val();
            var phone = $('[id$="txtPhone"').val();
            var email = $('[id$="txtEmail"').val();
            var message = $('[id$="txtMessage"').val();
            var vehicleId = $('[id$="hdfAgileVehicleId"').val();
            var postalcode = $('[id$="txtPostalCode"').val();
            var scheduledate = $('[id$="txtDate"').val();
            var scheduleweekdaytime = $('[id$="weekday-hours"').val();
            var schedulesaturdaytime = $('[id$="saturday-hours"').val();
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/UserMesssage.ashx',
                type: 'GET',
                async: true,
                dataType: "json",
                cache: false,
                timeout: 30000,
                data: {
                    method: 'UserMesssage',
                    name: name,
                    phone: phone,
                    email: email,
                    message: message,
                    vehicleId: vehicleId,
                    postalcode: postalcode,
                    scheduledate:scheduledate,
                    scheduleweekdaytime:scheduleweekdaytime,
                    schedulesaturdaytime:schedulesaturdaytime
                },
                success: function (data) {
                    if (data == true) {
                        $('#check-availability-control').modal('hide');
                        resetValueForTextBox();
                    }
                }
            });


        }
    });
</script>
<input type="hidden" runat="server" id="hdfAgileVehicleId" />

<div id="check-availability-control" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" runat="server" id="lblTitle"></h4>
            </div>
        </div>

        <div class="modal-body col-xs-12 col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="col-sm-4 inputText col-xs-12">
                <div class="">
                    <div class="form-group">
                        <input type="text" runat="server" class="form-control" id="txtFullName" placeholder="<%#CheckAvailability.Column_InputText_Input_Placeholder %>" />
                    </div>
                    <div class="form-group">
                        <input type="text" runat="server" class="form-control" id="txtPhone" placeholder="<%#CheckAvailability.Column_InputText_Input_1_Placeholder %>" />
                    </div>
                    <div class="form-group">
                        <input type="text" runat="server" class="form-control" id="txtEmail" placeholder="<%#CheckAvailability.Column_InputText_Input_2_Placeholder %>" />
                    </div>
                    <div class="form-group">
                        <input type="text" runat="server" class="form-control" id="txtPostalCode" placeholder="<%#CheckAvailability.PostalCode_Label %>" maxlength="6" />
                    </div>
                    <div class="form-group">
                        <input type="text" id="txtDate" class="form-control" data-val="true" placeholder="Schedule A Test Drive:">
                    </div>
                    <div class="form-group divhours hide">
                        <select class="form-control hide" id="weekday-hours">
                            <option disabled="" value="" selected=""><%=ScheduleService.Weekday_Hours %></option>
                            <option disabled="" value=""></option>
                            <asp:Repeater ID="rptScheduleTimeNormal" runat="server">
                                <ItemTemplate>
                                    <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                       
                        <select class="form-control hide" id="saturday-hours">
                            <option disabled="" value="" selected=""><%=ScheduleService.Saturday_Hours %></option>
                            <option disabled="" value=""></option>
                            <asp:Repeater ID="rptScheduleTimeSaturday" runat="server">
                                <ItemTemplate>
                                    <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea type="text" runat="server" class="form-control" rows="10" id="txtMessage" placeholder="<%#CheckAvailability.Column_InputText_Input_3_Placeholder %>"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 vehicleInfo hidden-xs">
                <div class="">
                    <div class="image">
                        <img runat="server" id="imgVehicle" src="" />
                    </div>
                    <div class="infor">
                        <div class="carName">
                            <asp:Label runat="server" ID="lblCarName"></asp:Label>
                        </div>
                        <div class="label-group">
                            <label><%=CheckAvailability.Infor_Label_Group_Label %></label>
                            <asp:Label runat="server" ID="lblExtColor"></asp:Label>
                        </div>
                        <div class="label-group">
                            <label><%=CheckAvailability.Infor_Label_Group_Label_1 %></label>
                            <asp:Label runat="server" ID="lblVin"></asp:Label>
                        </div>
                        <div class="label-group">
                            <label><%=CheckAvailability.Infor_Label_Group_Label_2 %></label>
                            <asp:Label runat="server" ID="lblEngine"></asp:Label>
                        </div>
                        <div class="label-group">
                            <label><%=CheckAvailability.Infor_Label_Group_Label_3 %></label>
                            <asp:Label runat="server" ID="lblDrive"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 expectation hidden-xs">
                <div class="">
                    <div class="col-sm-12 infor">
                        <label runat="server" id="lblMessageFromDealer"></label>
                    </div>
                    <div class="col-sm-12 image">
                        <img runat="server" id="imgLogo" src="" />
                    </div>
                </div>
            </div>

        </div>

        <div class="modal-footer">
            <input type="button" class="btn btn-primary" id="btnSend" runat="server" title="<%#CheckAvailability.Column_Expectation_Button_Input_Title %>" value="<%#CheckAvailability.Column_Expectation_Button_Input_Value %>" />
        </div>
    </div>

</div>




