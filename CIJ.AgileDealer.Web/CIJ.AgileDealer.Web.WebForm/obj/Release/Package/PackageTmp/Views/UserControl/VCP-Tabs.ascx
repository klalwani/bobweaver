﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VCP-Tabs.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.VCP_Tabs" %>

<div id="vcp-ford-f-150" class="vcp">
  <div class="container">

      <div class="row vcp-tabs mdl-bottom-shadow">

        <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
          <li class=""><a href="#reviews" data-toggle="tab">Reviews</a></li>
          <li class="active"><a href="#inventory" data-toggle="tab">Inventory</a></li>
          <li class=""><a href="#more-info" data-toggle="tab">More Info</a></li>
        </ul>

        <div id="my-tab-content" class="tab-content ">

            <div class="tab-pane" id="reviews">
                <ul style="list-style: none; margin: 1em 0px; padding: 0px;">
                    <li style="list-style: none; margin: 0px 2em;">
                    <div style="padding: 1%; width: 24%; float: left;"><img style="width: 100%; box-shadow: 0px 8px 6px -6px black; -webkit-box-shadow: 0 8px 6px -6px black; -moz-box-shadow: 0 8px 6px -6px black;" src="https://storage.googleapis.com/www.savvydealer.com/new/Ford/F-150/All-Trims/2016/Main/2016-Ford-F-150.jpg"></div>
                    <div style="padding: 1%; width: 72%; float: left;">
                    <div style="line-height: 0; clear: both;">
                    <a class="sav_btn_cta" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(45, 150, 205); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/inventory/view/New/Model/F-150/">Inventory</a>
                    <a class="sav_btn_default" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(28, 57, 79); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/custom/2016-Ford-F-150-sda" target="_blank">Read More</a>
                    </div>
                    <h3 style="color: rgb(45, 150, 205); line-height: 20px; font-family: fordngbs-antenna-bold,Arial,sans-serif; font-size: 20px; margin-top: 0px; text-shadow: none;">2016 Ford F-150</h3>
                    <div style="line-height: 0; clear: both;"></div>
                    <p style="margin: 0px; padding: 10px; color: rgb(127, 127, 127); line-height: 1.5; letter-spacing: 0.01em; font-size: 15px !important; word-spacing: 0.05em;">The 2016 Ford F-150 is a full-size truck that provides excellent towing and hauling capabilities, a powerful engine lineup, and modern passenger comfort. Choose from a variety of configurations for the cabin and bed size that speaks to your needs. Inside, the F-150 cabin is stocked with the latest in tech and luxury features, including Ford's all-new Sync 3 infotainment system. You can also expect top-notch safety equipment. Come in and test drive a Ford today!</p>
                    </div>
                    <div style="height: 20px; clear: both;"></div>
                    </li>
                </ul>
                <ul style="list-style: none; margin: 1em 0px; padding: 0px;">
                    <li style="list-style: none; margin: 0px 2em;">
                    <div style="padding: 1%; width: 24%; float: left;"><img style="width: 100%; box-shadow: 0px 8px 6px -6px black; -webkit-box-shadow: 0 8px 6px -6px black; -moz-box-shadow: 0 8px 6px -6px black;" src="https://storage.googleapis.com/www.savvydealer.com/new/Ford/F-150/All-Trims/Upgrade/2016-vs-2012/Exterior/2016-Ford-F-150-vs-2012-F-150-Exterior.jpg"></div>
                    <div style="padding: 1%; width: 72%; float: left;">
                    <div style="line-height: 0; clear: both;">
                    <a class="sav_btn_cta" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(45, 150, 205); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/inventory/view/New/Model/F-150/">Inventory</a>
                    <a class="sav_btn_default" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(28, 57, 79); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/custom/2016-Ford-F-150-vs-2012-F-150-sda" target="_blank">Read More</a>
                    </div>
                    <h3 style="color: rgb(45, 150, 205); line-height: 20px; font-family: fordngbs-antenna-bold,Arial,sans-serif; font-size: 20px; margin-top: 0px; text-shadow: none;">2016 Ford F-150 vs 2012 F-150</h3>
                    <div style="line-height: 0; clear: both;"></div>
                    <p style="margin: 0px; padding: 10px; color: rgb(127, 127, 127); line-height: 1.5; letter-spacing: 0.01em; font-size: 15px !important; word-spacing: 0.05em;">The Ford F-150 has been America's bestselling vehicle for decades on end - and for good reason. Over the years, Ford has carefully monitored consumer need and tailored new models accordingly. Detailed below are just a few of the many ways that the modern 2016 Ford F-150 surpasses the used 2010 Ford F-150 truck.</p>
                    </div>
                    <div style="height: 20px; clear: both;"></div>
                    </li>
                </ul>              
                <ul style="list-style: none; margin: 1em 0px; padding: 0px;">
                    <li style="list-style: none; margin: 0px 2em;">
                    <div style="padding: 1%; width: 24%; float: left;"><img style="width: 100%; box-shadow: 0px 8px 6px -6px black; -webkit-box-shadow: 0 8px 6px -6px black; -moz-box-shadow: 0 8px 6px -6px black;" src="http://storage.googleapis.com/www.savvydealer.com/new/Ford/F-150/All-Trims/2015/Main/2015-Ford-F150-Truck.jpg"></div>
                    <div style="padding: 1%; width: 72%; float: left;">
                    <div style="line-height: 0; clear: both;">
                    <a class="sav_btn_cta" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(45, 150, 205); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/inventory/view/New/Model/F-150/">Inventory</a>
                    <a class="sav_btn_default" style="margin: 0px 1% !important; padding: 0.5% 1% !important; border-radius: 3px; border: 1px solid rgb(179, 179, 179); border-image: none; width: auto; color: rgb(255, 255, 255); line-height: 20px; font-family: fordngbs-antenna,Fordngbs-antenna,FordAntenna,Arial; font-size: 15px; font-weight: normal; text-decoration: none; float: right; display: block; max-width: 30%; background-color: rgb(28, 57, 79); -moz-border-radius: 3px; -webkit-border-radius: 3px; -webkit-box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.3); -moz-box-shadow: rgba(0, 0, 0, 3) 0 4px 4;" href="/custom/2015-F-150" target="_blank">Read More</a>
                    </div>
                    <h3 style="color: rgb(45, 150, 205); line-height: 20px; font-family: fordngbs-antenna-bold,Arial,sans-serif; font-size: 20px; margin-top: 0px; text-shadow: none;">2015 Ford F-150</h3>
                    <div style="line-height: 0; clear: both;"></div>
                    <p style="margin: 0px; padding: 10px; color: rgb(127, 127, 127); line-height: 1.5; letter-spacing: 0.01em; font-size: 15px !important; word-spacing: 0.05em;">The 2015 Ford F-150 is a rugged full-sized truck with surprising fuel economy, a variety of customization options, and lighter than ever aluminum body options. Ford's efforts to lighten the F-150 and offer new innovative features have given the 2015 F-150 a competitive advantage versus its rivals.</p>
                    </div>
                    <div style="height: 20px; clear: both;"></div>
                    </li>
                </ul>
            </div>

            <div class="tab-pane active" id="inventory">
              <div class="row">
                <iframe style="width: 100%; height: 1105px; margin-top: 30px;" src="https://agiledealer-demo.azurewebsites.net/new#contentP" height="550" frameBorder="0"></iframe>
                  <div class="clear"></div>
              </div>
            </div>

            <div class="tab-pane" id="more-info">
                <img style="width: 100%;" src="https://storage.googleapis.com/dave_personal/ford-details-example.PNG">
            </div>

        </div>
      
      </div>
    
    
  </div>
</div>