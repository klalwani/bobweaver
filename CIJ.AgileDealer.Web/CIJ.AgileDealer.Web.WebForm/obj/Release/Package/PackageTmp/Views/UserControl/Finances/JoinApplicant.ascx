﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JoinApplicant.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Finances.JoinApplicant" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Finances" %>
<form>
    <div class="row form-wrapper">
        <div class="row form-title">
            <span><%=JoinApplicant.Row_Form_Title_Span %></span>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="row content-padded">
                <div class="col-md-12 col-xs-12 application-title">
                    <h3><%=JoinApplicant.Application_Title_H3 %></h3>
                </div>
            </div>
            <div class="row content-padded" id="credit-panel">
                <asp:Repeater ID="rptNotification" runat="server">
                    <ItemTemplate>
                        <div class="panel <%#Eval("Class")%>">
                            <div class="panel-heading">
                                <h4><span class="glyphicon <%#Eval("Glyphicon") %>" aria-hidden="true"></span><%#Eval("Title")%></h4>
                            </div>
                            <div class="panel-body">
                                <p><%#Eval("Content")%></p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                
            </div>
        </div>

        <div class="col-md-8 col-xs-12">

            <div class="row content-padded">
                <div class="row content-padded">
                    <h3><%=JoinApplicant.Row_Content_Padded_H3 %></h3>
                </div>


                <fieldset class="form-group rowElem">
                    <label for="coContactStreetAddress" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem %></label>
                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactStreetAddress" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_PlaceHolder %>">
                </fieldset>

                <div class="row form-horizontal-input-padded">
                    <div id="coContactAddressFull">

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="coContactCity" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_1 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactCity" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_1 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-3">
                            <fieldset class="form-group rowElem">
                                <label for="coContactState" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_2 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="coContactState" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_2 %>">
                                    <asp:Repeater ID="rptState2" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-md-4">
                            <fieldset class="form-group rowElem">
                                <label for="coContactZip" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_3 %></label>
                                <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="coContactZip" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_3 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="form-group form-line-spacer"></div>

                <fieldset class="form-group rowElem">
                    <label for="coContactResidenceStatus" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_4 %></label>
                    <select class="form-control" data-val="true" data-val-required="true" id="coContactResidenceStatus" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_4 %>">
                        <option value="" selected disabled><%=JoinApplicant.Form_Group_Row_Elem_4 %></option>
                        <option value="" disabled></option>
                        <asp:Repeater ID="rptResidenceStatus" runat="server">
                            <ItemTemplate>
                                <option value="<%#Eval("ResidenceStatusId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </fieldset>


                <fieldset class="form-group rowElem">
                    <label for="coContactMonthlyHousing" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_5 %></label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" id="coContactMonthlyHousing" data-val="true" data-val-required="true" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_5 %>">
                        <div class="input-group-addon custom-popover"  data-val-regex-pattern="^[0-9]*$" data-content="Include the total paid for all mortgages, second mortgages, second homes in whole dollars." rel="popover" data-placement="left" data-original-title="What to include:" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>


                <div class="row form-horizontal-input-padded">
                    <label for="coContactResidence" class="control-label form-label-500"><%=JoinApplicant.Form_Label_500 %></label>
                    <div id="coContactResidence">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="coContactResidenceYears" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_6 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="coContactResidenceYears" placeholder="">
                                    <span class="input-group-addon" id="coContactResidenceYearsAddon"><%=JoinApplicant.Form_Group_Row_Elem_6_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="coContactResidenceMonths" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_7 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="coContactResidenceMonths" placeholder="">
                                    <span class="input-group-addon" id="coContactResidenceMonthsAddon"><%=JoinApplicant.Form_Group_Row_Elem_7_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                    </div>

                </div>

                <div id="priorResidence">
                    <div class="form-group form-line-spacer"></div>

                    <fieldset class="form-group rowElem">
                        <label for="coContactPreviousStreetAddress" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_8 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousStreetAddress" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_8 %>">
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <div id="coContactAddressFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactPreviousCity" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_9 %></label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousCity" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_9 %>">
                                </fieldset>
                            </div>

                            <div class="col-md-3">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactPreviousState" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_10 %></label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="coContactPreviousState">
                                       <asp:Repeater ID="rptState" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-md-4">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactPreviousZip" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_11 %></label>
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="coContactPreviousZip" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_11 %>">
                                </fieldset>
                            </div>

                        </div>
                    </div>
                </div>



                <div class="row content-padded">
                    <h3><%=JoinApplicant.Row_Content_Padded_1_H3 %></h3>
                </div>


                <div class="row form-horizontal-input-padded">
                    <label for="coContactDOB" class="control-label form-label-500"><%=JoinApplicant.Form_Label_500_1 %></label>
                    <div id="coContactDOB">

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="coContactBirthMonth" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_12 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthMonth">
                                    <option value="" selected disabled><%=JoinApplicant.Form_Group_Row_Elem_12 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptMonth" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("MonthId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="coContactBirthDay" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_13 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthDay">
                                    <option value="" selected disabled><%=JoinApplicant.Form_Group_Row_Elem_13 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptDate" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("DateId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="coContactBirthYear" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_14 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthYear">
                                    <option value="" selected disabled><%=JoinApplicant.Form_Group_Row_Elem_14 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptYear" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("YearId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div id="coContactNameFull">

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="coContactFirstName" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_15 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactFirstName" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_15 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-2">
                            <fieldset class="form-group rowElem">
                                <label for="coContactMiddleName" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_16 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactMiddleName" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_16 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="coContactLastName" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_17 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactLastName" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_17 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div id="coContactEmailConfirm">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="coContactEmail" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_18 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactEmail" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_18 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="coContactPhone" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_19 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPhone" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_19 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <fieldset class="form-group rowElem">
                    <div class="input-group">
                        <label for="coContactSSN" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_20 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactSSN" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_20 %>">
                        <div class="input-group-addon custom-popover" data-content="We protect the confidentiality of Social Security numbers, prohibit unlawful disclosure of Social Security Numbers, and limit access to Social Security Numbers. We limit the use to staff and process necessary for the purpose of facilitating a relationship or business transaction. You can  obtain more information by reviewing our privacy policy." data-placement="left" title="We Protect your Social Security Number" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>


                <div class="row content-padded">
                    <h3><%=JoinApplicant.Row_Content_Padded_2_H3 %></h3>
                </div>

                <fieldset class="form-group rowElem">
                    <label for="coContactCurrentEmployer" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_21 %></label>
                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactCurrentEmployer" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_21 %>">
                </fieldset>

                <fieldset class="form-group rowElem">
                    <label for="coContactMonthlyIncome" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_22 %></label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="coContactMonthlyIncome" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_22 %>">
                        <div class="input-group-addon custom-popover" data-content="Include your yearly income from salary, dividends, interest.  If retired include Social Security, Pension, Annuities, 401k etc. You only need to inlcude alimonry, child support, or maintenance income if you wish to have it considered as a basis for repayment.  Use whole dollars." data-placement="left" title="What to include:" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>

                <%--<fieldset class="form-group">
                    <label for="coContactEmployerDuration" class="control-label form-label-500"><%=JoinApplicant.Form_Label_500_2 %></label>
                    <div class="btn-group multiButtonDefault" id="coContactEmployerDuration" data-toggle="buttons">

                        <label class="btn btn-default active" data-toggle="collapse" data-target="#coPriorEmployer">

                            <input type="radio" name="coContactEmployerDuration" value="1" autocomplete="off" checked>
                            <%=JoinApplicant.Btn_Default_Active %> 
                   
                        </label>

                        <label class="btn btn-default" data-toggle="collapse" data-target="#coPriorEmployer">

                            <input type="radio" name="coContactEmployerDuration" value="2" autocomplete="off">
                            <%=JoinApplicant.Btn_Default %> 
                        </label>

                    </div>
                </fieldset>--%>

                <div class="row form-horizontal-input-padded">
                    <label for="contactEmployerDuration" class="control-label form-label-500"><%=ContactInfo.Form_Label_500_3 %></label>
                    <div id="contactEmployerDuration">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactResidenceYears" class="sr-only"><%=ContactInfo.Form_Group_RowElem_6 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true"  id="coContactEmployerDurationYear" placeholder="">
                                    <span class="input-group-addon" id="coContactEmployerDurationYearLabel"><%=ContactInfo.Form_Group_RowElem_6_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactResidenceMonths" class="sr-only"><%=ContactInfo.Form_Group_RowElem_7 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true"  id="coContactEmployerDurationMonth" placeholder="">
                                    <span class="input-group-addon" id="coContactEmployerDurationMonthLabel"><%=ContactInfo.Form_Group_RowElem_7_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div id="coPriorEmployer" class="collapse">

                    <fieldset class="form-group rowElem">
                        <label for="coContactPreviousEmployer" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_23 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousEmployer" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_23 %>">
                    </fieldset>
                    <div class="form-group form-line-spacer"></div>

                </div>

                <fieldset class="form-group rowElem">
                    <label for="coContactAdditionalIncome" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_24 %></label>
                    <input type="text" class="form-control" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_24 %>">
                </fieldset>

                <fieldset class="form-group rowElem">
                    <label for="coContactAdditionalAmount" class="sr-only"><%=JoinApplicant.Form_Group_Row_Elem_25 %></label>

                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" id="coContactAdditionalAmount" placeholder="<%=JoinApplicant.Form_Group_Row_Elem_25 %>">
                    </div>
                </fieldset>

            </div>

            <div class="row content-padded">
                <input type="button" id="btnSubmit" class="btn btn-primary" value="<%=JoinApplicant.Row_Content_Padded_Input %>" />
            </div>

        </div>

    </div>
</form>
