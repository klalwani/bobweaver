﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAccountControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.UserAccountControl" %>

<div id="userAccountDashboard" class="container">

    <script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.UserAccount.js"></script>
    <style scoped>
        #userAccountDashboard {
              background: #fff;
              background-color: #fff;
              text-align: center;
              padding: 0;
              margin-top: 25px;
              -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);  
              -moz-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
              box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);  
        }
        #userAccountDashboard .form-title {
            margin-top: 0;
        }
        #tableUserAccount_length, #tableUserAccount_info, #tableUserAccount_filter {margin: 0 15px;}
        #tableUserAccount_paginate {margin: 10px 15px;}
        #tableUserAccount {width: 98% !important;}
    </style>
    <div class="row form-title"><span>User Account Dashboard</span></div>
    <table id="tableUserAccount" class="hover row-border cell-border display stripe" data-order='[[ 0, "asc" ]]' data-page-length='10' >
        <thead>
            <tr>
                <th>UserId</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Email Confirmed</th>
                <th>Phone Number</th>
                <th>Current Role</th>
                <th>Update</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>UserId</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Email Confirmed</th>
                <th>Phone Number</th>
                <th>Current Role</th>
                <th>Update</th>
            </tr>
        </tfoot>
    </table>
</div>