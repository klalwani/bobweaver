﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobManagementControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.JobManagementControl" %>

<script src="../../../Scripts/Pages/Admin/AgileDealer.JobManagement.js"></script>
<script src="../../../Scripts/Pages/Enums/Job.Enum.js"></script>
<div id="jobManagementDashboard" class="container">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Manual Overrides</span>
                <input type="button" id="btnTriggerIncentivePuller" name="btnTriggerIncentivePuller" value="Update Incentives" class="btn btn-success" />
                <input type="button" id="btnTriggerRecon" name="btnTriggerRecon" value="Update Inventory" class="btn btn-success" />

            </div>
            <table id="tableJobManagement" class="table table-hover hover row-border cell-border display stripe" style="width: 100% !important;" data-order='[[4, "desc" ]]' data-page-length='10'>
                <thead>
                    <tr>
                        <th>JobTrackingId</th>
                        <th>Service</th>
                        <th>Status</th>
                        <th>Requestor</th>
                        <th>Queued</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>JobTrackingId</th>
                        <th>Service</th>
                        <th>Status</th>
                        <th>Requestor</th>
                        <th>Queued</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
