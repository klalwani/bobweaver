﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HideIncentivePrograms.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.HideIncentivePrograms" %>

 <div class="clear-20"></div>
<div id="divHideIncentivePrograms" class="container">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Hide Incentive Programs Dashboard</span>
                <%-- <button type="button" class="btn btn-primary" id="btnAddNewProgram">Add New</button>--%>
            </div>
            <asp:UpdatePanel ID="upnlvehicleOptionsDashboard" runat="server">
                <ContentTemplate>
                    <div class="col-xs-12 col-lg-8 col-md-8 col-sm-12">
                        <div class="container-fluid">
                            <div class="offer-cards row-fluid">
                                <div class="form-group row">
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label class="label-group marginTop5">Program Number</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="txtProgramNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label class="label-group marginTop5">Update For</label>
                                    </div>
                                    <div class="col-md-8 btn-group multiButtonDefault" id="HideIncentiveUpdateFor" data-toggle="buttons">
                                        <label class="btn btn-default active HideIncentiveUpdateForAllVehicle">
                                            <input type="radio" name="chkHideIncentiveUpdateFor" value="0" data-target="all" checked />All Vehicles
                                        </label>
                                        <label class="btn btn-default HideIncentiveUpdateForVin">
                                            <input type="radio" name="chkHideIncentiveUpdateFor" value="1" data-target="HideIncentiveVinSection" />Vin
                                        </label>
                                        <label class="btn btn-default HideIncentiveUpdateForModel">
                                            <input type="radio" name="chkHideIncentiveUpdateFor" value="2" data-target="HideIncentiveModelSection" />Model
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row collapse" id="HideIncentiveVinSection">
                                    <div class="col-md-4">
                                        <label class="label-group marginTop5">Vin</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="txtVIN" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group row collapse" id="HideIncentiveModelSection">
                                    <div class="col-md-4">
                                        <label class="label-group marginTop5">Models</label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:DropDownList ID="ddlHideIncentiveModel" runat="server" CssClass="form-control">
                                             
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-8 text-left">
                                        <asp:Button ID="btnSaveHideIncentiveProgram" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return confirm('Are you sure you want to hide the program number?');" OnClick="btnSaveHideIncentiveProgram_Click" />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <asp:GridView ID="grdvwHideIncentivePrograms" OnRowCommand="grdvwHideIncentivePrograms_RowCommand" DataKeyNames="ProgramNumber" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-bordered table-hover table-striped">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ProgramNumber" ItemStyle-HorizontalAlign="Left" HeaderText="ProgramNumber" />
                                <asp:BoundField DataField="VIN" ItemStyle-HorizontalAlign="Left" HeaderText="VIN" />
                                 <asp:BoundField DataField="ModelName" ItemStyle-HorizontalAlign="Left" HeaderText="Model Name" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Left">

                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteProgram" CommandArgument='<%#Eval("Id")%>' Text="Delete" OnClientClick="return confirm('Are you sure you want to delete?');"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</div>
<script>

    function ShowProgressBar() {
        $("#MainContent_divHideIncentivePrograms_lblError").html("");
        $.blockUI(siteMaster.loadingWheel());
        window.setTimeout('HideLoadingWheetl()', 1000);
    }
    function HideLoadingWheetl() {
        $.unblockUI(siteMaster.loadingWheel());
    }
    $(document).on('click', '#HideIncentiveUpdateFor label', function () {
        var that = $(this);
        var input = that.find('input');
        var value = input.val();
        var target = input.data('target');
        if (target == 'all') {
            $('#HideIncentiveVinSection').removeClass('in').addClass('collapse');
            $('#HideIncentiveModelSection').removeClass('in').addClass('collapse');
            $("#MainContent_HideIncentiveProgramsControl_txtVIN").val("");
            $("#MainContent_HideIncentiveProgramsControl_ddlHideIncentiveModel").val("0");
        }
        else if (target == 'HideIncentiveVinSection') {
            $('#HideIncentiveVinSection').removeClass('collapse').addClass('in');
            $('#HideIncentiveModelSection').removeClass('in').addClass('collapse');
            $("#MainContent_HideIncentiveProgramsControl_ddlHideIncentiveModel").val("0");
        }
        else if (target == 'HideIncentiveModelSection') {
            $('#HideIncentiveVinSection').removeClass('in').addClass('collapse');
            $('#HideIncentiveModelSection').removeClass('collapse').addClass('in');
            $("#MainContent_HideIncentiveProgramsControl_txtVIN").val("");
        }
    });
</script>
