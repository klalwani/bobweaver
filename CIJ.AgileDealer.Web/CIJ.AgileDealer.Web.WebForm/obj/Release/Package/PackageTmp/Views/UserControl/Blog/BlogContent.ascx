﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogContent.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog.BlogContent" %>

<%@ Register Src="~/Views/UserControl/Blog/BlogEntries.ascx" TagName="blogEntries" TagPrefix="BlogEntries" %>
<%@ Register Src="~/Views/UserControl/Blog/BlogEntryDetail.ascx" TagName="blogEntryDetail" TagPrefix="BlogEntryDetail" %>
<%@ Register Src="~/Views/UserControl/Blog/BlogSearchControl.ascx" TagPrefix="blogSearchControl" TagName="BlogSearchControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Blog" %>
<script type="text/javascript">
    $(document).ready(function () {
        if ($('#MainContent_BlogContent_UCBlogEntries_hdfDealerShipTitle').val() !== '')
            $('#MainContent_BlogContent_DealerShipTitle').text($('#MainContent_BlogContent_UCBlogEntries_hdfDealerShipTitle').val());


    });
</script>
<asp:MultiView ID="MultiBlog" runat="server" ActiveViewIndex="0">
    <asp:View ID="vwBlogEntries" runat="server">
        <div class="top-container col-12">
                <div class="inner-container">
                   
                        <h1>
                            <span runat="server" ID="DealerShipTitle"><%=BlogContent.DealerShipTitle %></span>
                        </h1>
                    
                </div>
            </div>
        <div id="blog-page" class="container">
            <div class="row">
                <div class="blog-entry col-md-9">
                    <BlogEntries:blogEntries ID="UCBlogEntries" runat="server" />
                </div>
                <div class="blog-category col-md-3">
                    <blogSearchControl:BlogSearchControl runat="server" ID="BlogSearchControl" />
                </div>
            </div>
        </div>
    </asp:View>
    <asp:View ID="vwBlogEntryDetail" runat="server">
        <BlogEntryDetail:blogEntryDetail ID="UCBlogEntryDetail" runat="server" />
    </asp:View>
</asp:MultiView>



