﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffEditControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.StaffEditControl" %>
<script src="../../../Scripts/Pages/Admin/AgileDealer.EmployeeUpdate.js"></script>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Admin" %>
<script>
    $(function () {
        // you may want to add an additional check to make sure that the parent is the element you expect it to be
        $('[checked="checked"]').parent().addClass('active');
    })

</script>

<div class="row form-wrapper">

    <div class="col-xs-12">
        <div class="row form-horizontal-input-padded">

            <div class="col-xs-12" id="contactInfo">

                <div class="row content-padded">

                    <div class="col-xs-12 col-sm-4">
                        <img alt="vehicle-image" id="vehicleImage" class="bcc-hero-image mdl-box-shadow" src="<%= DealerImageGeneric %>content/staff/staffnotavailable.jpg">
                    </div>


                    <div class="col-xs-12 col-sm-8">
                        <div class="row form-horizontal">

                            <fieldset class="form-group rowElem">
                                <label for="firstName" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_FirstName %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="firstName" placeholder="<%=StaffEditControl.Form_Group_FirstName %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="lastName" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_LastName %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="lastName" placeholder="<%=StaffEditControl.Form_Group_LastName %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="email" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Email %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="email" placeholder="<%=StaffEditControl.Form_Group_Email %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="officePhone" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Office_Phone %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="officePhone" placeholder="<%=StaffEditControl.Form_Group_Office_Phone %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="cellPhone" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Cell_Phone %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="cellPhone" placeholder="<%=StaffEditControl.Form_Group_Cell_Phone %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="ext" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Ext %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="ext" placeholder="<%=StaffEditControl.Form_Group_Ext %>" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="order" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Order %></label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="order" placeholder="<%=StaffEditControl.Form_Group_Order %>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="department" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Departments %></label>
                                <div class="col-xs-7">
                                    <select  id="department" multiple="multiple" placeholder="<%=StaffEditControl.Form_Group_Departments %>" title="<%=StaffEditControl.Form_Group_Departments %>">
                                        <%--<option value="">Select Departmetns</option>--%>
                                    </select>
                                </div>
                            </fieldset>
                            
                            <fieldset class="form-group hidden" id="titleSection">
                                <label for="title" class="col-xs-4 control-label"><%=StaffEditControl.Form_Group_Hidden %></label>
                                
                            </fieldset>

                            <fieldset class="form-group checkbox edit-vehicle-checkbox">
                                <div class="col-xs-offset-4 col-xs-7">
                                    <label class="">
                                        <input type="checkbox" name="isNew" id="isNew" title="<%=StaffEditControl.Form_Group_CheckBox_IsNew %>"><%=StaffEditControl.Form_Group_CheckBox_IsNew %>
                                    </label>
                                </div>
                            </fieldset>

                            <fieldset class="form-group checkbox edit-vehicle-checkbox">
                                <div class="col-xs-offset-4 col-xs-7">
                                    <label class="">
                                        <input type="checkbox" name="isDeleted" id="isDeleted" title="<%=StaffEditControl.Form_Group_CheckBox_IsDelete %>"><%=StaffEditControl.Form_Group_CheckBox_IsDelete %>
                                    </label>
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="order" class="col-xs-4 control-label"><%=StaffEditControl.Control_Label %></label>
                                <div class="col-xs-7">
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <label class="btn btn-primary">
                                                <%=StaffEditControl.Btn_Primary %>
                                            <input id="employeeImage" type="file" style="display: none;" multiple>
                                            </label>
                                        </label>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </div>


                <div class="row content-padded ">

                    <fieldset class="form-group">
                        <input type="button" class="btn btn-primary" id="btnUpdateEmployee" value="<%=StaffEditControl.Btn_Primary_Save %>" />
                        <input type="button" class="btn btn-default" id="btnBackEmployee" value="<%=StaffEditControl.Btn_Default_Cancel %>" />
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField runat="server" ID="EmployeeId" />
<asp:HiddenField runat="server" ID="Departments" />
<input type="hidden" id="tempImageName" value="" />
