﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RebateControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.RebateControl.RebateControl" %>

<%: Scripts.Render("~/bundles/RebateControl") %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.RebateControl" %>
<!-- Modal -->
<div id="rebateControl" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="lblOffer"><%=RebateControl.Rebate_Title_Close_Button_H4 %></h4>
            </div>
        </div>

        <div class="modal-body">
            <div class="container-fluid">
                <div class="offer-cards row-fluid">
                    <ul class="col-xs-12 list-unstyled " id="singleProgramContent"></ul>
                    <div class="clear"></div>
                    <h2 class="text-center " id="cashProgramTitle"><%=RebateControl.Manufacturer_Offers %></h2>
                    <ul class="col-xs-12 list-unstyled " id="cashProgramContent"></ul>
                    <div class="clear"></div>
                    <h2 class="text-center " id="conditionalProgramTitle"><%=RebateControl.Conditional_Offers %></h2>
                    <ul class="col-xs-12 list-unstyled " id="conditionalProgramContent"></ul>
                    <div class="clear"></div>
                    <h2 class="text-center " id="aprProgramTitle"><%=RebateControl.Finance_Offers %></h2>
                    <ul class="col-xs-12 list-unstyled " id="aprProgramContent"></ul>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <h5 class="text-center ctaLink" id="viewCompleteOffer"><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span><%=RebateControl.Text_Center_A %><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></h5>
                <div class="clear"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>

        <%-- <div class="col-lg-12 offer-cards">
            <div>
                <h2 class="text-center" id="cashProgramTitle"><%=RebateControl.Manufacturer_Offers %></h2>
                <ul class="offer-card-holder" id="cashProgramContent">
                </ul>
            </div>
            <div class="clear-10"></div>
            <div>
                <h2 class="text-center" id="conditionalProgramTitle"><%=RebateControl.Conditional_Offers %></h2>
                <ul class="offer-card-holder" id="conditionalProgramContent">
                </ul>
            </div>
            <div class="clear-10"></div>
            <div>
                <h2 class="text-center" id="aprProgramTitle"><%=RebateControl.Finance_Offers %></h2>
                <ul class="offer-card-holder" id="aprProgramContent">
                </ul>
            </div>
            <div class="clear"></div>
        </div>--%>
    </div>
    <div class="clear-20"></div>
</div>

<script type="text/x-jquery-tmpl" id="singleProgramTemplate">
    <li class="row-fluid">
        <div class="alert alert-success text-center incentive-detail-section">
            <h5 class="incentive-detail-title">${Name}</h5>
            <p class="incentive-detail-amount">{{html Amount}}</p>
            <div class="clear-20">---</div>
            {{if Id>0}}
                <p class="cash-disclaimer">Program #${Id}: ${Disclaimer} See dealer for complete details.</p>
            {{else (Name=='GM Employee Appreciation' || Name=='GM Employee Appreciation Certificate Program - Purchase') }}
                <p class="cash-disclaimer">
                    Effective Dates: ${StartDate} - ${EndDate}<br />
                    Only customers with a certificate qualify for the incentive. Each certificate can only be used once and can only be used on the purchase or lease of one vehicle. No facsimiles or mechanical reproductions of certificate other than from GMGlobalConnect.com will be allowed.
                    <br />
                    Promotion Contains following program(s).
                    <br />
                    ${Name} - {{html Amount}}
            </p>
            {{else}}
                <p class="cash-disclaimer">
                     Effective Dates: ${StartDate} - ${EndDate}<br />
                    ${Disclaimer} See dealer for complete details.</p>
            {{/if}}
        </div>
    </li>
</script>

<script type="text/x-jquery-tmpl" id="ORItemTemplate">
    <p class="text-center"><strong>OR</strong></p>
</script>


<script type="text/x-jquery-tmpl" id="cashTemplate">
    <li class="row-fluid">
        <div class="alert alert-success text-center incentive-detail-section">
            <h5 class="incentive-detail-title">${Name}</h5>
            <p class="incentive-detail-amount">{{html Amount}}</p>
            <div class="clear-20">---</div>
            {{if Id>0}}
                <p class="cash-disclaimer">Program #${Id}: ${Disclaimer} See dealer for complete details.</p>
            {{else}}
                <p class="cash-disclaimer">${Disclaimer} See dealer for complete details.</p>
            {{/if}}
        </div>
    </li>
</script>

<script type="text/x-jquery-tmpl" id="conditionalTemplate">
    <li class="row-fluid">
        <div class="alert alert-success text-center incentive-detail-section">
            <h5 class="incentive-detail-title">${Name}</h5>
            <p class="incentive-detail-amount">{{html Amount}}</p>
            <div class="clear-20">---</div>
            {{if Id>0}}
                <p class="cash-disclaimer">Program #${Id}: ${Disclaimer} See dealer for complete details.</p>
           {{else (Name=='GM Employee Appreciation' || Name=='GM Employee Appreciation Certificate Program - Purchase') }}
                <p class="cash-disclaimer text-left">
                    Effective Dates: ${StartDate} - ${EndDate}<br />
                    Only customers with a certificate qualify for the incentive. Each certificate can only be used once and can only be used on the purchase or lease of one vehicle. No facsimiles or mechanical reproductions of certificate other than from GMGlobalConnect.com will be allowed.
                    <br />
                    Promotion Contains following program(s).
                    <br />
                    ${Name} - {{html Amount}}
            </p>
            {{else}}
                <p class="cash-disclaimer">${Disclaimer} See dealer for complete details.</p>
            {{/if}}
        </div>
    </li>
</script>

<script type="text/x-jquery-tmpl" id="financeTemplate">
    <li class="row-fluid">
        <div class="alert alert-success text-center incentive-detail-section">
            <h5 class="incentive-detail-title">${Name}</h5>
            <p class="incentive-detail-amount">{{html Amount}}</p>
            <div class="clear-20">---</div>
            {{if Id>0}}
                <p class="cash-disclaimer">Program #${Id}: ${Disclaimer} See dealer for complete details.</p>
            {{else}}
                <p class="cash-disclaimer">${Disclaimer} See dealer for complete details.</p>
            {{/if}}
        </div>
    </li>
</script>

<script type="text/x-jquery-tmpl" id="orProgramTemplate">
    <li class="row-fluid">
        <div class="separator text-center">
            <h4>or</h4>
        </div>
    </li>
</script>
