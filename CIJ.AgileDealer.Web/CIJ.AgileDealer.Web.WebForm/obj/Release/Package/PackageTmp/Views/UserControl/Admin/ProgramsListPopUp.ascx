﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramsListPopUp.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.ProgramsListPopUp" %>

<script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.ProgramListPopup.js"></script>
<div id="program-list-update" class="container col-xs-12">
    <div class="col-xs-12">
        <fieldset class="form-group">
            <select id="nameplateList" name="nameplateList" >
                <option value="">-- Select Model --</option>
            </select>
            <select id="nameplateTrimList" name="nameplateTrimList" >
                <option value="">-- Select Model Trim --</option>
            </select>
        </fieldset>

        <fieldset class="form-group">
            <div id="">
                <table id="tableProgramList" class="hover row-border cell-border display stripe" data-order='[[ 0, "asc" ]]' data-page-length='10'>
                    <thead>
                        <tr>
                            <th>ProgramId</th>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Disclaimer</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ProgramId</th>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Disclaimer</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Amount</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </fieldset>
        <fieldset class="form-group">
            <input type="button" id="btnSelectPrograms" name="btnSelectPrograms" value="Select Program(s)" class="btn btn-success" />
        </fieldset>
    </div>
</div>
<asp:HiddenField runat="server" ID="NamePlateHdf"/>