﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveEditControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveEditControl" %>

<script src="../../Scripts/Pages/AgileDealer.IncentiveTranslatedUpdate.js"></script>
<div id="incentiveEditControl" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="lblProgramName"></h4>
            </div>
        </div>

        <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div class="container-fluid">
                <div class="offer-cards row-fluid">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Display Section</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" id="ddlDisplaySection">
                                <option value="">Select Position</option>
                                <option value="1">Manufacturer</option>
                                <option value="2">Conditional</option>
                                <option value="3">Finance</option>
                                <option value="4">Lease</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row hide">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Status</label>
                        </div>
                        <div class="col-md-8 btn-group multiButtonDefault" id="c" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="chkStatus" value="Active" autocomplete="off" checked>Active
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="chkStatus" value="Hidden" autocomplete="off">Hidden
                            </label>
                        </div>
                    </div>

                    <div class="form-group row hide">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Position</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="txtPosition" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Translated Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="txtTranslatedName" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Update For</label>
                        </div>
                        <div class="col-md-8 btn-group multiButtonDefault" id="updateFor" data-toggle="buttons">
                            <label class="btn btn-default active updateForAllVehicle">
                                <input type="radio" name="chkUpdateFor" value="1" data-target="all" checked />All Vehicles
                            </label>
                            <label class="btn btn-default updateForVin">
                                <input type="radio" name="chkUpdateFor" value="2" data-target="vinSection" />Vin
                            </label>
                            <label class="btn btn-default updateForModel">
                                <input type="radio" name="chkUpdateFor" value="3" data-target="modelSection" />Model
                            </label>
                        </div>
                    </div>

                    <div class="form-group row collapse" id="vinSection">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Vin</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="txtVin" />
                        </div>
                    </div>

                    <div class="form-group row collapse" id="modelSection">
                        <div class="col-md-4">
                            <label class="label-group marginTop5">Models</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" id="ddlModel">
                                <option value="0">Select Model</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-lg-12">
                <div class="text-left col-lg-4">
                    <button type="button" class="btn btn-warning" id="btnReset">Reset</button>
                </div>

                <div class="text-right col-lg-8">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveChange">Save Changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnClose">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>

    <div id="modal-confirm-reset" class="modal fade" role="dialog" tabindex='-1'>
        <div class="modal-dialog modal-xs" style="border: 1px solid; border-color: #212020; top: 50px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reset Warning!</h4>
                </div>
            </div>

            <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
                <div class="container-fluid">
                    <div class="offer-cards row-fluid">
                        <p>This will remove all saved changes from this item and return it to the default values.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="btnConfirmReset">Reset</button>
                <button type="button" class="btn btn-default" id="btnCancelReset">Cancel</button>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="HdfIsEditForVin" />
</div>
