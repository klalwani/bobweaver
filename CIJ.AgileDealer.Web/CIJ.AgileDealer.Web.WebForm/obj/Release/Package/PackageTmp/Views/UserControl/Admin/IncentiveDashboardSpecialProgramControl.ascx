﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveDashboardSpecialProgramControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveDashboardSpecialProgramControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveEditSpecialProgramControl.ascx" TagPrefix="IncentiveEditControl" TagName="IncentiveEditControl" %>


<script src="../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.IncentiveManagementSpecialProgram.js"></script>

<div id="incentiveManagementDashboardSpecialProgram" class="container">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Custom Incentives Dashboard</span>
                <button type="button" class="btn btn-primary" id="btnAddNewSpecialProgramIncentive">Add New</button>
            </div>
            <div class="clear-20"></div>
            <table id="tableIncentiveManagementSpecialProgram" class="table table-hover hover row-border cell-border display stripe" style="width: 100% !important;" data-page-length='10'>
                <thead>
                    <tr>
                        <th>Section</th>
                        <th>Translated Name</th>
                        <th>Program Name</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Exclude Engine Codes</th>
                         <th>Exclude VIN(s)</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                        <th></th>
                        <th></th>

                        
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Section</th>
                        <th>Translated Name</th>
                        <th>Program Name</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Exclude Engine Codes</th>
                         <th>Exclude VIN(s)</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                        <th></th>

                         
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <IncentiveEditControl:IncentiveEditControl runat="server" ID="IncentiveEditControlSpecialProgram" />
        <asp:HiddenField runat="server" ID="ModelList" />
        <asp:HiddenField runat="server" ID="EngineList" />
        <asp:HiddenField runat="server" ID="ExcludeVinList" />
    </div>
</div>






