﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogSearchControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog.BlogSearchControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Blog" %>
<div class="search row justify-content-start">
    <div class="input-group col-9">
        <asp:TextBox CssClass="form-control" ID="txtSearch" placeholder="Search" runat="server" />
        <span class="input-group-btn">
            <asp:Button CssClass="btn btn-primary" runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" Text="<%#BlogSearchControl.Input_Group_Btn %>" />
        </span>
    </div>
</div>
<div class="row">
    <div id="categories" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><%=BlogSearchControl.Panel_Title %></h3>
        </div>
        <div class="panel-body">
            <div class="list-group">
                <asp:Repeater ID="rptCategory" runat="server">
                    <ItemTemplate>
                        <a href='<%#: GetRouteUrl("BlogByCategory", new {CategorySelected = Eval("Url") }) %>' class="list-group-item"><%#Eval("CategoryText") %></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div id="tags" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><%=BlogSearchControl.Panel_Title_1 %></h3>
        </div>
        <div class="panel-body">
            <div class="list-tag">
                <asp:CheckBoxList runat="server" ID="rptTag" AutoPostBack="True" RepeatDirection="Vertical" DataTextField="Value" DataValueField="Id" CssClass="chkBox"></asp:CheckBoxList>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#categories .list-group a').click(function (e) {
        $(this).addClass("active");
    });

    $("#categories .list-group a").each(function (index, a) {
        var id = $('[id$="hdfCategoryID"]').val();
        if (a.innerText === id)
            $(this).addClass("active");
    });
</script>
