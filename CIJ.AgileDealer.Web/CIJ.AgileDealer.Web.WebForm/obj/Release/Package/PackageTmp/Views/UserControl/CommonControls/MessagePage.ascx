﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagePage.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.CommonControls.MessagePage" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.CommonControl" %>
<form>
    <div class="row form-wrapper">
        <div class="col-md-8 col-xs-12">
            <div class="row content-padded">
                <div class="col-md-12 col-xs-12 application-title">
                    <h3><%=MessagePage.Application_Title %></h3>
                </div>
            </div>
            <div class="row content-padded">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span><%=MessagePage.Glyphicon_Inbox %></h4>
                    </div>
                    <div class="panel-body">
                        <p runat="server" id="MessageContent"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
