﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Home.FooterControl" %>
<%@ OutputCache Duration="60" VaryByParam="None" Shared="true" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views" %>
<div class="container">
    <%--<p>&copy; <%: DateTime.Now.Year %> - Agile Dealer</p>--%>
    <div class="row">
        <div class="col-md-3 col-xs-12" style="color: white">
            <h5><%=SiteMaster.Col_Sm_3_Store %></h5>
            <asp:Repeater runat="server" ID="rptStoreInfo">
                <ItemTemplate>
                    <li><%# Eval("Street1") %></li>
                    <li><%# Eval("City") %> <%# Eval("State") %> <%# Eval("Zip") %></li>
                    <li><%# Eval("SalesPhone") %></li>
                    <li><%# Eval("ServicePhone") %></li>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="col-md-3 col-xs-12" style="color: white">
            <h5><%=SiteMaster.Col_Sm_3_Sale %></h5>
            <asp:Repeater runat="server" ID="rptSale">
                <ItemTemplate>
                    <li style="" />
                    <%# Eval("DayHour") %>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="col-md-3 col-xs-12" style="color: white">
            <h5><%=SiteMaster.Col_Sm_3_Service_Hours %></h5>
            <asp:Repeater runat="server" ID="rptService">
                <ItemTemplate>
                    <li style="" />
                    <%# Eval("DayHour") %>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="col-md-3 col-xs-12 social" style="color: white">
            <h5><%=SiteMaster.Col_Sm_3_Follow_Us %></h5>
            <asp:Repeater runat="server" ID="rptSocial">
                <ItemTemplate>
                    <a title='<%#Eval("FacebookLinkImageAltText") %>' href='<%#Eval("FacebookLink") %>'>
                        <img width="30" height="30" runat="server" src='<%#Eval("FacebookLinkImage") %>' /></a>
                    <a title='<%#Eval("TwitterImageAltText") %>' href='<%#Eval("TwitterLink") %>'>
                        <img width="30" height="30" runat="server" src='<%#Eval("TwitterImage") %>' alt='<%#Eval("TwitterImageAltText") %>' /></a>
                    <a title='<%#Eval("GooglePlusImageAltText") %>' href='<%#Eval("GooglePlusLink") %>'>
                        <img width="30" height="30" runat="server" src='<%#Eval("GooglePlusImage") %>' alt='<%#Eval("GooglePlusImageAltText") %>' /></a>
                    <a target="_blank" title='<%#Eval("InstagramLinkImageAltText") %>' href='<%#Eval("InstagramLink") %>'>
                        <img width="30" height="30" runat="server" src='<%#Eval("InstagramLinkImage") %>' alt='<%#Eval("InstagramLinkImageAltText") %>' /></a>
                    <a target="_blank" title='Napa Ford YouTube' href='https://www.youtube.com/user/BobWeaverGMChrysler'>
                        <img width="30" height="30" src='https://agiledealer.blob.core.windows.net/generic/social/youtube.jpg' alt='Napa Ford YouTube' /></a>

                </ItemTemplate>
            </asp:Repeater>
            <div class="row">
                <a class="darkLink" title='Site Map' href='/sitemap'><%=SiteMaster.Row_Site_Map %></a>
                <br>
                <a class="darkLink" title='Site Map Xml' href='/sitemapxml'><%=SiteMaster.Row_Site_Map_Xml %></a>
                <br>
                <a class="darkLink" title='Privacy | Disclaimer' href='/privacy'><%=SiteMaster.Row_Privacy %></a>
                <br>
                <a class="darkLink" title='Account Login' href='/account/login'><%=SiteMaster.Row_Login %></a>
            </div>
        </div>
    </div>
</div>
