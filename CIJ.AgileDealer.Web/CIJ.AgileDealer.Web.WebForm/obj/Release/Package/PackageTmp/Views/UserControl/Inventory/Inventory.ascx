﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Inventory.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Inventory.Inventory" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.NewCars" %>
<%@ Register Src="~/Views/UserControl/SearchControl/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchUserControl" %>

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1><%=TypeString + " " + string.Format(New.Inner_Container_Col_Md_12_H1,DealerCity,DealerState) %></h1>
            </div>
        </div>
    </div>

<searchUserControl:SearchControl runat="server" ID="SearchControl" />
