﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduleService.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.ScheduleService.ScheduleService" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.ScheduleService" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Common" %>

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Online Appointment Scheduling</h1>
            </div>
        </div>
    </div>

        <div class="body-content-wrapper">
            <div class="body-content-card">

                
                <div class="row form-wrapper">
                    <div class="row form-title">
                        <span><%=ScheduleService.Row_Form_Title %></span>
                    </div>  

                    <div class="col-md-12">

                        <div class="row">

                            <div class="col-md-6 col-sm-12">
                                <div class="row content-padded">
                                    <h3><%=ScheduleService.Content_Padded_H3 %>
                                    </h3>
                                    <input type="text" id="serviceDate" class="form-control" data-val="true" data-val-required="true">
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="row content-padded" id="timepicker">
                                    <h3><%=ScheduleService.Content_Padded_H3_1 %></h3>

                                    <select class="form-control" id="weekday-hours" data-val="true" data-val-required="true">
                                        <option disabled="" value="" selected=""><%=ScheduleService.Weekday_Hours %></option>
                                        <option disabled="" value=""></option>
                                        <asp:Repeater ID="rptScheduleTimeNormal" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>

                                    <select class="form-control" id="saturday-hours" >
                                        <option disabled="" value="" selected=""><%=ScheduleService.Saturday_Hours %></option>
                                        <option disabled="" value=""></option>
                                        <asp:Repeater ID="rptScheduleTimeSaturday" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </div>


                            </div>


                        </div>

                        <div class="row form-horizontal-input-padded">

                            <div class="col-md-6" id="contactInfo">
                                <div class="row content-padded">
                                    <div class="row content-padded">
                                        <h3><%=ScheduleService.Content_Padded_H3_2 %></h3>
                                    </div>

                                    <fieldset class="form-group">
                                        <label for="contactFirstName" class="sr-only" ><%=ScheduleService.ContactInfo_Form_Group_Label_1 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactFirstName" placeholder="<%=ScheduleService.ContactInfo_Form_Group_Label_1 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="contactLastName" class="sr-only"><%=ScheduleService.ContactInfo_Form_Group_Label_2 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactLastName" placeholder="<%=ScheduleService.ContactInfo_Form_Group_Label_2 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="contactPhone" class="sr-only"><%=ScheduleService.ContactInfo_Form_Group_Label_3 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPhone" placeholder="<%=ScheduleService.ContactInfo_Form_Group_Label_3 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="contactEmail" class="sr-only"><%=ScheduleService.ContactInfo_Form_Group_Label_4 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactEmail" placeholder="<%=ScheduleService.ContactInfo_Form_Group_Label_4 %>">
                                    </fieldset>
                                </div>
                            </div>

                            <div class="col-md-6" id="vehicleInfo">

                                <div class="row content-padded">
                                    <div class="row content-padded">
                                        <h3><%=ScheduleService.Content_Padded_H3_3 %></h3>
                                    </div>

                                    <fieldset class="form-group">
                                        <label for="vehicleYear" class="sr-only"><%=ScheduleService.VehicleInfo_Form_Group_Label_1 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleYear" placeholder="<%=ScheduleService.VehicleInfo_Form_Group_Label_1 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="vehicleMake" class="sr-only"><%=ScheduleService.VehicleInfo_Form_Group_Label_2 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleMake" placeholder="<%=ScheduleService.VehicleInfo_Form_Group_Label_2 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="vehicleModel" class="sr-only"><%=ScheduleService.VehicleInfo_Form_Group_Label_3 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleModel" placeholder="<%=ScheduleService.VehicleInfo_Form_Group_Label_3 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="vehicleMileage" class="sr-only"><%=ScheduleService.VehicleInfo_Form_Group_Label_4 %></label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleMileage" placeholder="<%=ScheduleService.VehicleInfo_Form_Group_Label_4 %>">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="serviceWarranty" class="control-label form-label-500"><%=ScheduleService.Form_Label_500 %></label>
                                        <div class="btn-group multiButtonDefault" id="serviceWarranty" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="serviceWarranty" value="1" autocomplete="off" checked>
                                                <%=ScheduleService.Form_Group_Label_1 %>
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="serviceWarranty" value="2" autocomplete="off">
                                                <%=ScheduleService.Form_Group_Label_2 %>
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="serviceWarranty" value="3" autocomplete="off">
                                                <%=ScheduleService.Form_Group_Label_3 %>
                                            </label>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>

                        </div>

                        <div class="row content-padded">

                            <div class="col-md-6 col-sm-12 service-sub-card" id="serviceRequested">
                                <div class="row form-sub-title">
                                    <span><%=ScheduleService.Row_Form_Sub_Title %></span>
                                </div>
                                <div class="row content-padded">
                                    <asp:Repeater ID="rptServiceRequested" runat="server">
                                        <ItemTemplate>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="<%#Eval("ServiceRequestedId")%>"><%#Eval("Name")%>
                                                </label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 service-sub-card" id="currentIssue">
                                <div class="row form-sub-title">
                                    <span><%=ScheduleService.Row_Form_Sub_Title_1 %></span>
                                </div>

                                <div class="row content-padded">
                                    <asp:Repeater ID="rptCurrentIssue" runat="server">
                                        <ItemTemplate>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="<%#Eval("CurrentIssueId")%>"><%#Eval("Name")%>
                                                </label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div class="col-md-12 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="contactMessage" class="sr-only"><%=ScheduleService.ContactMessage %></label>
                                    <textarea class="form-control" id="contactMessage" rows="3" placeholder="<%=ScheduleService.ContactMessage %>"></textarea>
                                </fieldset>
                            </div>
                        </div>



                        <div class="row content-padded">

                            <input type="button" class="btn btn-primary" id="btnSubmit" value="<%=ScheduleService.Row_Content_Padded_Button %>"/>
                        </div>

                    </div>

                </div>
                <input hidden name="ValidationError" data-attribute="validation" value="<%=Common.ValidationError %>" />


            </div>
        </div>