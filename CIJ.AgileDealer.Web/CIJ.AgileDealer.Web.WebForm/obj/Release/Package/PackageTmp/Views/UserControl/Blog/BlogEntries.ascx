﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogEntries.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog.BlogEntries" %>
<%@ Import Namespace="System.Web.Routing" %>

<div>
    <script type="text/javascript">
        var justchecked = false;
        $(document).ready(function () {
            //var currentUrl = siteMaster.currentUrl() + location.pathname;
            //currentUrl = currentUrl.replace('%20', '-');
            //window.history.pushState(null, null, currentUrl);
        });
    </script>
    <asp:ListView ID="rptEntry" runat="server" GroupItemCount="1" ItemPlaceholderID="itemsGoHere"
        GroupPlaceholderID="groupsGoHere">
        <LayoutTemplate>
            <asp:PlaceHolder runat="server" ID="groupsGoHere"></asp:PlaceHolder>
        </LayoutTemplate>

        <GroupTemplate>
            <asp:PlaceHolder runat="server" ID="itemsGoHere"></asp:PlaceHolder>
        </GroupTemplate>
        <ItemTemplate>
            <section class="section blogroll-card">
                <div class="section-inner">
                    <div class="content">
                        <div class="item featured text-left">
                            <h3 class="blog-card-title">
                                <a class="postId" href='<%#GetRouteUrl("BlogDetail", new {PostUrl = Eval("PostUrl")})%>'><%# Eval("PostTitle") %> </a>
                            </h3>
                            <div class="featured-image">
                                <a class="postId" href='<%#GetRouteUrl("BlogDetail", new {PostUrl = Eval("PostUrl") })%>'> 
                                    <img class="img-responsive project-image" src='<%#Eval("PostFeatureImage") %>' alt=""></a>

                            </div>
                            <div class="desc text-left">
                                <%#Eval("PostSummary") %>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </ItemTemplate>
    </asp:ListView>
    <asp:DataPager ID="dataPager" runat="server" PagedControlID="rptEntry" QueryStringField="page" PageSize="10">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="True" ShowNextPageButton="False" />
            <asp:NumericPagerField />
            <asp:NextPreviousPagerField ShowLastPageButton="True" ShowPreviousPageButton="False" />
        </Fields>
    </asp:DataPager>
</div>
<asp:HiddenField runat="server" ID="hdfDealerShipTitle" Value="" />
<asp:HiddenField runat="server" ID="hdfCategoryID" Value="" />
<asp:HiddenField runat="server" ID="hdfTagID" Value="" />
<asp:HiddenField runat="server" ID="hdfPostId" Value="" />


