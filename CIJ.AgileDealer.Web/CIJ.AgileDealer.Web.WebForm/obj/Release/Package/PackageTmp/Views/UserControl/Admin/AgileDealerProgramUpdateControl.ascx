﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgileDealerProgramUpdateControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.AgileDealerProgramUpdateControl" %>
<%@ Register Src="~/Views/UserControl/Admin/ProgramsListPopUp.ascx" TagPrefix="uc1" TagName="ProgramsListPopUp" %>

<div id="vehicleProgramUpdateControl" class="">
    <script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.VehicleProgram.js"></script>

<%--    <fieldset class="form-group">
        <input type="button" id="btnAddNewProgram" name="btnAddNewProgram" value="Add New Program" class="btn btn-success" disabled="disabled"/>
    </fieldset>--%>

    <table id="tableVehicleProgram" class="hover row-border cell-border display stripe" data-order='[[ 0, "asc" ]]' data-page-length='5'>
        <thead>
            <tr>
                <th>ProgramId</th>
                <th>Name</th>
                <th>Translated Name</th>
                <th>Type</th>
                <th>Disclaimer</th>
                <th>Start-End</th>
                <th>Id</th>
                <th>Amount</th>
                <th>Status</th>
                <%--<th>Is Deleted</th>--%>
                <th>Active</th>
                <th>In-Active</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ProgramId</th>
                <th>Name</th>
                <th>Translated Name</th>
                <th>Type</th>
                <th>Disclaimer</th>
                <th>Start-End</th>
                <th>Id</th>
                <th>Amount</th>
                <th>Status</th>
                <%--<th>Is Deleted</th>--%>
                <th>Active</th>
                <th>In-Active</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    <div class="hidden">
        <div id="program-list" class="col-xs-12">
            <uc1:ProgramsListPopUp runat="server" ID="ProgramsListPopUp" />
        </div>
    </div>
</div>
