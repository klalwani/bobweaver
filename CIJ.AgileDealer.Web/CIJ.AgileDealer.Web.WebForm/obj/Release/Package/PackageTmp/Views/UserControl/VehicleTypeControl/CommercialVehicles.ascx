﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommercialVehicles.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.VehicleTypeControl.CommercialVehicles" %>
<%@ Register Src="~/Views/UserControl/SearchControl/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchUserControl" %>

<div class="top-container row">
    <div class="inner-container">
        <div class="col-md-12">
            <h1><%= DealerCity %> Vans<br />
                <small><%= DealerName %>'s Van Department</small></h1>
        </div>
    </div>
</div>


<div id="truck-department-page" class="row">
    <div class="body-content-wrapper">
        <div class="body-content-card">
            <div class="container-fluid">
                <div class="row">

                    <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
                        <li id="" class="active"><a href="#inventory" data-toggle="tab">Inventory</a></li>
                        <li id="" class=""><a href="#reviews" data-toggle="tab">Research</a></li>
                        <li id="" class=""><a href="#specials" data-toggle="tab">Specials</a></li>
                    </ul>

                    <div id="my-tab-content" class="tab-content ">
                        <div class="tab-pane padded-15 active" id="inventory">
                            <h2>Inventory Tab Content</h2>
                            <div style="height: 800px;">
                                <searchUserControl:SearchControl runat="server" id="SearchControl" />
                            </div>
                        </div>
                        <%--<div class="tab-pane padded-15" id="reviews">
                            <h2>Review Content</h2>
                            <div style="height: 800px;"></div>
                        </div>
                        <div class="tab-pane padded-15" id="specials">
                            <h2>Specials Content</h2>
                            <div style="height: 800px;"></div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>

        <div class="clear-20"></div>
    </div>
</div>
