﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgileVehicleEditControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.AgileVehicleEditControl" %>
<%@ Register Src="~/Views/UserControl/Admin/AgileDealerProgramUpdateControl.ascx" TagPrefix="agileDealerProgramUpdateControl" TagName="AgileDealerProgramUpdateControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveEditControl.ascx" TagPrefix="IncentiveEditControl" TagName="IncentiveEditControl" %>
<script src="../../../Scripts/Pages/Admin/AgileDealer.VehicleUpdate.js"></script>
<script src="../../../Scripts/jquery.mask.js"></script>
<script>
    $(function () {
        // you may want to add an additional check to make sure that the parent is the element you expect it to be
        $('[checked="checked"]').parent().addClass('active');
    })
</script>

<div class="row form-title" style="height: auto; line-height: 1.5;">
    <span id="vehicleTitle"></span>
</div>

<div class="row form-wrapper">

    <div class="col-xs-12">
        <div class="row form-horizontal-input-padded">

            <div class="col-xs-12" id="contactInfo">

                <div class="row content-padded">
                    <div class="form-horizontal">
                        <div class="col-xs-12 col-sm-4">
                            <img alt="vehicle-image" id="vehicleImage" class="bcc-hero-image mdl-box-shadow" src="">
                            <div class="input-group col-xs-12">
                                <div class="input-group-addon">VIN</div>
                                <input type="text" class="form-control" data-val="true" readonly="readonly" data-val-required="true" id="vin" placeholder="Vin">
                            </div>
                            <div class="padded-15">
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox" id="option-lifted-truck">
                                        <label class="pull-right">
                                            <input type="checkbox" name="isLift" id="isLift" title="Is Lift">Lifted Truck
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox edit-vehicle-two" id="option-current-special">
                                        <label class="pull-right">
                                            <input type="checkbox" name="isSpecial" id="isSpecial" title="Is Special">Current Special
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox " id="option-current-visible">
                                        <label class="pull-right">
                                            <input type="checkbox" name="isHidden" id="isHidden" title="Hidden">Hidden
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-8">

                            <div class="col-xs-12 col-sm-12">

                                <fieldset class="form-group">
                                    <label for="msrp" class="col-sm-4 control-label" id="lblMSRPTitle" runat="server"></label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="msrp" placeholder="MSRP">
                                    </div>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="sellingPrice" class="col-sm-4 col-xs-12 control-label">Price</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="sellingPrice" placeholder="Price">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="discount" class="col-sm-4 col-xs-12 control-label">Discount</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="discount" placeholder="Discount">
                                    </div>
                                </fieldset>

                                <fieldset class="form-group">
                                    <div class="input-group col-sm-11 col-xs-12">
                                        <p class="pull-right ctaColor">Override one value at a time</p>
                                    </div>
                                </fieldset>
                                  <fieldset class="form-group">
                                    <label for="msrp" class="col-sm-4 col-xs-12 control-label"  id="lblMSRPOverrideTitle" runat="server"></label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" data-val="true"   id="msrpoverride" placeholder="MSRP Override">
                                    </div>
                                </fieldset>
                                 <fieldset class="form-group">
                                    <label for="overridePrice" class="col-sm-4 col-xs-12 control-label">Price Override</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" id="overridePrice" placeholder="Price Override">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="discount" class="col-sm-4 col-xs-12 control-label">Discount Override</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" data-val="true"  id="discountoverride" placeholder="Discount Override">
                                    </div>
                                </fieldset>
                               


                                <fieldset class="form-group">
                                    <label for="priceDate" class="col-sm-4 col-xs-12 control-label">Override Expiration</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="btn-group" data-toggle="buttons" id="priceDate">
                                            <label class="btn btn-default active" data-toggle="out" href="#dateRangeField" name="ultilSold">
                                                <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                                Until Sold
                                            </label>
                                            <label class="btn btn-default" data-toggle="in" href="#dateRangeField" name="setDate">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Set Dates
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <!-- This should be displayed if the override is set to "Set Dates" and have today as the default start date -->
                                <fieldset class="form-group collapse" id="dateRangeField">
                                    <label for="priceDateStart" class="col-xs-4 control-label">Date Range</label>
                                    <div class="col-xs-3 no-left-pad">
                                        <input type="text" class="form-control" id="priceDateStart">
                                    </div>
                                    <div class="col-xs-3 no-left-pad">
                                        <input type="text" class="form-control" id="priceDateEnd">
                                    </div>
                                </fieldset>
                                <hr>
                                <fieldset class="form-group" style="margin-left: 100px !important;">
                                        <input type="button" class="btn btn-primary" id="btnUpdateVehicle" value="Save Changes" />
                                        <input type="button" class="btn btn-default" id="btnBack" value="Cancel" />
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row content-padded">

                    <!-- Temporarily disabling 
                            <fieldset class="form-group">
                                        <label for="overrideDescription" class="">Override Description</label>
                                        <textarea rows="8" class="form-control" id="overrideDescription" placeholder="Override Description"></textarea>
                                    </fieldset>
                            Temporarily disabling -->
                    <fieldset class="form-group">
                        <!-- Autowriter description -->
                        <label for="autoWriterDescription" class="">Description Override</label>
                        <textarea rows="12" class="form-control" id="autoWriterDescription" placeholder="Auto Writer Description"></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <div id="vehicle-program-updated">
                            <agileDealerProgramUpdateControl:AgileDealerProgramUpdateControl runat="server" ID="AgileDealerProgramUpdateControl" />
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <input type="button" class="btn btn-primary" id="btnUpdateVehicle" value="Save Changes" />
                        <input type="button" class="btn btn-default" id="btnBack" value="Cancel" />
                    </fieldset>
                     <div class="clear-20"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<IncentiveEditControl:IncentiveEditControl runat="server" ID="IncentiveEditControl" />
<asp:HiddenField runat="server" ID="ModelList"/>

<asp:HiddenField runat="server" ID="VinHdf" />
<asp:HiddenField runat="server" ID="AgileVehicleId" />


<script>

    $(document).ready(function () {
        $('#msrpoverride').on("blur", function () {
            $('#msrpoverride').val(CommaFormatted($('#msrpoverride').val()));
        });
        $('#discountoverride').on("blur", function () {
            $('#discountoverride').val(CommaFormatted($('#discountoverride').val()));
        });
        $('#overridePrice').on("blur", function () {
            $('#overridePrice').val(CommaFormatted($('#overridePrice').val()));
        });
    });

</script>