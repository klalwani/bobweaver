﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduleServiceDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.ScheduleServiceDashboardControl" %>

<script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.ScheduleService.js"></script>
<div id="scheduleServiceDashboard" class="container">
    <table id="tableScheduleService" style="width: 100% !important;" class="hover row-border cell-border display stripe" data-order='[[8, "desc" ]]' data-page-length='10'>
        <thead>
            <tr>
                <th>ScheduleServiceId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Schedule Date</th>
                <th>Weekly Hour</th>
                <th>Saturday Hour</th>
                <th>Submitted Date</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ScheduleServiceId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Schedule Date</th>
                <th>Weekly Hour</th>
                <th>Saturday Hour</th>
                <th>Submitted Date</th>
                <th>Detail</th>
            </tr>
        </tfoot>
    </table>
</div>
