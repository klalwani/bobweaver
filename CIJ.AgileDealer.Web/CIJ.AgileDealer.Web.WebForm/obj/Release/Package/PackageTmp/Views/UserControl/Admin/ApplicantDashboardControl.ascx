﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicantDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.ApplicantDashboardControl" %>

<div id="applicantDashboard" class="container">
    <script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.Applicant.js"></script>

    <style scoped>
        #applicantDashboard .form-title {
            margin-top: 0;
        }

        #tableApplicant_length, #tableApplicant_info, #tableApplicant_filter {
            margin: 0 15px;
        }

        #tableApplicant_paginate {
            margin: 10px 15px;
        }

        #tableApplicant {
            width: 98% !important;
        }

        #applicantTypeddl {
            float: left;
            margin: 15px;
        }
    </style>
    <select id="applicantTypeddl" class="dropdown left">
        <option value="false">Current</option>
        <option value="true">Including Archive</option>
    </select>
    <table id="tableApplicant" class="hover row-border cell-border display stripe" data-order='[[7, "desc" ]]' data-page-length='10'>
        <thead>
            <tr>
                <th>ApplicantId</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>City</th>
                <th>State</th>
                <th>Number Of Applicants</th>
                <th>Status</th>
                <th>Date Created</th>
                <th>Completed?</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ApplicantId</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>City</th>
                <th>State</th>
                <th>Number Of Applicants</th>
                <th>Status</th>
                <th>Date Created</th>
                <th>Completed?</th>
                <th>Detail</th>
            </tr>
        </tfoot>
    </table>
</div>
