﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Blog_CMS.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Blogs.Blog" %>

<%@ Register Src="~/Views/UserControl/Blog/BlogContent.ascx" TagPrefix="BlogContent" TagName="blogContent" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <BlogContent:blogContent runat="server" id="BlogContent" />
</asp:Content>
