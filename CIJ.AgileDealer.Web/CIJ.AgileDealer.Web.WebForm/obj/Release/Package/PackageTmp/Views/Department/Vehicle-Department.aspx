﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Vehicle-Department.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Department.Vehicle_Department" %>

<%@ Register Src="~/Views/UserControl/VehicleTypeControl/VehicleTypeControl.ascx" TagPrefix="uc1" TagName="VehicleTypeControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Department" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <%--<h1>Tampa {VehicleType}s<br />
                    <small>Generic Ford's {VehicleType} Departmnet</small></h1>--%>
				<h1><%=string.Format(Vehicle_Department.Inner_Container_H1,DealerCity,!string.IsNullOrEmpty(VehicleType) ? VehicleType + "s" : string.Empty,DealerName) %></h1>
            </div>
        </div>
    </div>    

    <uc1:VehicleTypeControl runat="server" ID="VehicleTypeControl" />
    
</asp:Content>