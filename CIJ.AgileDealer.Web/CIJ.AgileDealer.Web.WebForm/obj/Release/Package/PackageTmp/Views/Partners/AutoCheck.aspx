﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AutoCheck.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Partners.AutoCheck" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .placeholderContent {
            width: 100%;
            min-height: 850px;
        }
        #siteNav {
            margin-bottom: 0;
        }
        html {
            background-color: rgba(6, 120, 188, 0.1);}
        body {
            background-color: transparent !important;
        }
         
    </style>
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>AutoCheck at <% =DealerName %><br />
                    <small>A part of Experian</small></h1>
            </div>
        </div>
    </div>

    <div id="">
        <div class="body-content-wrapper">
            <div class="body-content-card">
                <div class="container-fluid padded-15 box-600 placeholderhere">
                    <%--<iframe style="width: 100%; min-height: 850px;" srcdoc="" runat="server" id="PlaceHolderContent"></iframe>--%>
                    <asp:PlaceHolder ID="PlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
