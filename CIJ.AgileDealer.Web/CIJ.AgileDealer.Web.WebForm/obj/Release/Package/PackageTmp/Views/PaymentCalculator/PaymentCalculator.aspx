﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PaymentCalculator.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.PaymentCalculator.PaymentCalculator" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.PaymentCalculator" %>
<%@ Register Src="~/Views/UserControl/PaymentCalculator.ascx" TagName="PaymentCalculatorControl" TagPrefix="paymentCalculatorControl" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <div class="top-container row">
            <div class="inner-container">
                <div class="col-md-12">
                    <h1><%=PaymentCalculator.Inner_Container_Col_Md_12_H1 %></h1>
                </div>
            </div>
        </div>
        <div class="body-content-wrapper payment-calculator-style">
            <div class="body-content-card">
                <div class="">
                    <paymentCalculatorControl:PaymentCalculatorControl runat="server" ID="PaymentCalculatorControl" />
                </div>
            </div>
        </div>
</asp:Content>
