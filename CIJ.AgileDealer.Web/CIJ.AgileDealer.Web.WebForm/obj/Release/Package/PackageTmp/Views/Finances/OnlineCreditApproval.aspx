﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="OnlineCreditApproval.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Finances.OnlineCreditApproval" %>

<%-- User Controls --%>
<%@ Register Src="~/Views/UserControl/Finances/InstantlyPreQualify.ascx" TagName="InstantlyPreQualifyControl" TagPrefix="instantlyPreQualifyControl" %>
<%@ Register Src="~/Views/UserControl/Finances/ContactInfo.ascx" TagName="ContactInfoControl" TagPrefix="contactInfoControl" %>
<%@ Register Src="~/Views/UserControl/Finances/JoinApplicant.ascx" TagName="JoinApplicantControl" TagPrefix="joinApplicantControl" %>
<%@ Register Src="~/Views/UserControl/Finances/CompleteInfo.ascx" TagPrefix="CompleteInfoControl" TagName="completeInfoControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Finances" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Common" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/AgileDealer.Validation.js"></script>

    <script src="../../Scripts/Pages/Finance.js"></script>
    <script src="../../Scripts/Pages/Finance.InstantlyPreQualify.js"></script>
    <script src="../../Scripts/Pages/Finance.ContactInfo.js"></script>
    <script src="../../Scripts/Pages/Finance.JoinApplicant.js"></script>
    <script src="../../Scripts/Pages/Enums/Finance.Enum.js"></script>

    <input type="hidden" id="currentForm" value="instantlyCtr">
    <input type="hidden" id="applicantId" value="0">
    <input type="hidden" id="numberOfApplicant" value="1">
    <input type="hidden" name="ErrorMessage" data-attribute="validation" value="<%=Common.ErrorMessage %>"/>
    <input type="hidden" name="ValidationError" data-attribute="validation" value="<%=Common.ValidationError %>"/>
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1><%=OnlineCreditApproval.Inner_Container_H1 %></h1>
            </div>
        </div>
    </div>
    <div class="body-content-wrapper">
        <div class="body-content-card">
            <div class="active" id="instantlyCtr">
                <instantlyPreQualifyControl:InstantlyPreQualifyControl runat="server" ID="instantlyPreQualifyControl" />
            </div>
            <div class="container hidden" id="contactInfoCtr">
                <contactInfoControl:ContactInfoControl runat="server" ID="contactInfoControl" />
            </div>
            <div class="container hidden" id="joinApplicantCtr">
                <joinApplicantControl:JoinApplicantControl runat="server" ID="joinApplicantControl" />
            </div>
            <div class="container hidden" id="completeCtr">
                <CompleteInfoControl:completeInfoControl runat="server" ID="CompleteInfo" />
            </div>
        </div>
    </div>

    <div class="clear-20"></div>
</asp:Content>
