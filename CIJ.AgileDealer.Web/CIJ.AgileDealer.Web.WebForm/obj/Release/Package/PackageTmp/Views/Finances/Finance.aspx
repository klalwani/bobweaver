﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Finance.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Finances.Finance" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Finances" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        #finance-page {margin-top: 40px;}

        .savvy-50-5 {
          width: 48%;
          margin: 1%;
          float: left;
        }

        .savvy-card {
          border-radius: 2px;
          background: #fff;
          background-color: #fff;
          -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);  
          -moz-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
          box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);  
          font-family: Helvetica Neue,Helvetica,Arial,sans-serif; 
        }

        .savvy-card-title {
          background: #264b68;
          color: #eee;
          font-size: large;
          line-height: 1.5;
          padding: .8rem;
        }

        .savvy-card-text {
          padding: 1rem;
          font-size: xx-large;
          text-align: center;
        }

        .savvy-card-text-small {
          padding: 1rem .5rem;
          font-size: small;
          text-align: left;  
        }


        .savvy-card-footer {
          border-top: 1px solid #eee;
          font-size: 1.5rem;
          line-height: 1.5;
          padding: .5rem;
          text-align: right;
        }

        .savvy-card-footer a {
          text-decoration: none;
          color: #264b68;
        }

        .savvy-standout {
            background: #2d96cd;
            color: #fff;
            text-align:center;
            width: 80%;
            margin: 40px 10%;
        }
        .savvy-standout h2 {
            line-height: 1.75;
            margin: 0;
            padding:  5px 0;
            border-bottom: 1px solid #336;
        }
        .savvy-standout p {
            font-size: x-large;
            border-bottom: 1px solid #336;
            padding: 10px 20px;
        }
        .savvy-standout a {
            color: #fff;
            font-size: x-large;
            padding: 10px 20px;
            text-align: right;
        }
</style>


    <div class="top-container row">
        <div class="inner-container">
                <div class="col-md-12">
                    <h1><%=string.Format(Finance.Inner_Container_Col_Md_12_H1, DealerCity, DealerState, DealerName, ManufacturerName) %></h1>
                </div>
            </div>
    </div>

    <div id="finance-page">
        <div class="body-content-wrapper">
        <div class="body-content-card">
            <div class="container-fluid padded-15">
            <div class="row">
                <h2 style="margin-left: 15px; margin-top: 0;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span><%=Finance.Container_Fluid_Padded_15_Row_H2 %></h2>
                <div class="col-xs-12 col-md-8">
                    
                    <%=string.Format(Finance.Col_Xs_12_Col_Md_8,DealerName,DealerName, ManufacturerName) %>
                    <a href="/finance/car-loan" class="btn btn-primary"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span><%=Finance.Col_Xs_12_Col_Md_8_A %></a>
                </div>
                <div class="col-xs-12 col-md-4">
                    <a href="/finance/car-loan">
                        <img class="savvy-full-image" alt="" src="https://storage.googleapis.com/dave_personal/agile-dealer/images/apply-financing.jpg" />
                    </a>
                </div>
            </div>

            <!-- Temporarily Disabled
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="savvy-standout">
                        <h2>Buying vs Leasing</h2>
                        <p>When it comes to buying or leasing a car the options can be confusing. To help you make the best decision we have provided some useful information for you.</p>
                        
                        <div class="clear"><a class="#" href="/contactus"><span class="">Learn More</span></a></div>
                    </div>
                </div>
            </div>
                -->

            <div class="row">
                <!-- Disabling until counpon interface is finalized
                <div class="col-xs-6 col-md-6">
                    <div class="savvy-card">
                    <div class="savvy-card-title">2016 Ford Escape - Lease Offer</div>
                    <div class="savvy-card-text">
                        <span>Lease: $139 /month + tax
                      </span>
                    </div>
                    <div class="savvy-card-text-small">
                        <span>24 month/10,500 annual mileage
                          $3000 down payment + 1st month's payment + licensing fee due at signing.
                          Price includes all applicable rebates with approved Ford Credit financing.
                      </span>
                    </div>

                    <div class="savvy-card-footer">
                        <a class="" href="/contactus"><span class="">Click Here</span></a>
                    </div>

                    </div>
                </div> -->

                <div class="col-xs-6 col-md-6">
                    <%=string.Format(Finance.Col_Xs_6_Col_Md_6,DealerName) %>
                    <%--<a href="/finance/trade-in-car" class="btn btn-primary"> <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Get Started</a>--%>
                </div>
            </div>

        </div>

        </div>
        </div>
    </div>
    <div class="clear-20"></div>
</asp:Content>
