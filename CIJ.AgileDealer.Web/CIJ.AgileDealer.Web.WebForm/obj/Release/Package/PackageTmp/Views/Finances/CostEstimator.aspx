﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CostEstimator.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Finances.CostEstimator" %>

<%@ Register Src="~/Views/UserControl/CommonControls/MessagePage.ascx" TagPrefix="CommonControls" TagName="MessagePage" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container commonPage">
        <CommonControls:MessagePage runat="server" ID="MessagePage" />
    </div>
</asp:Content>
