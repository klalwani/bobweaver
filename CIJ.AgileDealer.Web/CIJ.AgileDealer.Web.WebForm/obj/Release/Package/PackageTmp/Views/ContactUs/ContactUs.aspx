﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ContactUs.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.ContactUs.ContactUs" %>

<%@ Register Src="~/Views/UserControl/ContactUs/ContactUs.ascx" TagName="ContactUs" TagPrefix="contactUsUserControl" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/AgileDealer.Validation.js"></script>
    <script src="../../Scripts/Pages/AgileDealer.ContactUs.js"></script>
    <div class="contact-us">
        <contactUsUserControl:ContactUs ID="UCContactUs" runat="server" />
    </div>
</asp:Content>
