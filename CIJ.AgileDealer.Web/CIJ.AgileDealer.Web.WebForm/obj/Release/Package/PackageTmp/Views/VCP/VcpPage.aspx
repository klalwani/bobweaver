﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Blog_CMS.Master" CodeBehind="VcpPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.VCP.VcpPage" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.VCP" %>
<%@ Register Src="~/Views/UserControl/SearchControl/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchUserControl" %>
<%@ Register Src="~/Views/VCP/BlogControl/VcpBaseModel.ascx" TagPrefix="vcpCtrl" TagName="blogEntryDetail" %>
<%@ Register Src="~/Views/VCP/BlogControl/BlogEntryControl.ascx" TagPrefix="vcpCtrl" TagName="BlogEntries" %>
<%@ Register Src="~/Views/UserControl/FatFooter/FatFooterControl.ascx" TagName="FatFooter" TagPrefix="FatFooterControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Scripts.Render("~/bundles/VCP") %>
    <input type="hidden" name="Expand" data-attribute="validation" value="<%=VcpPage.SeeMore2_Expand %>"/>
    <input type="hidden" name="Collapse" data-attribute="validation" value="<%=VcpPage.SeeMore2_Collapse %>"/>
    <div id="vcp-Ford-F-150" class="vcp">
        <div class="container">
            <div class="row vcp-nav">
                <div class="vcp-title">
                    <% if (Make != null && ModelTranslated != null && !String.IsNullOrEmpty(Type))
                        { %>
                    <h1 class="vcp-main-title"><%= String.Format("{0}{1}",Type.Substring(0,1).ToUpper() ,Type.Substring(1).ToLower()) %>&nbsp;<%= Make.Name %>&nbsp;<%= ModelTranslated.Name %></h1>
                    <%  }
                        else {%>
                    <h1><%=VcpPage.Vcp_Title_H1_Not_Exit %></h1>
                    <%  } %>
                </div>

                <div class="mdl-bottom-shadow">
                    <a id="newTag"  href='<%=GetRouteUrl("VCP", new {Type = "new" ,Make = Make.Name.ToLower(), Model = ModelTranslated.TranslatedLinkName }) %>'>
                        <div class='col-md-6 <%= Type.ToLower() == "new" ? "vcp-cta" : "vcp-default"%>'>
                            <h4><%=VcpPage.Mdl_Bottom_Shadow_H4_1 %></h4>
                        </div>
                    </a>
                    <%--<a id="certifiedTag"  href='<%=GetRouteUrl("VCP", new {Type = "certified" ,Make = Make.Name.ToLower(), Model = ModelTranslated.TranslatedLinkName }) %>'>
                        <div class='col-md-6 vcp-certifed-nav <%= Type.ToLower() == "certified" ? "vcp-cta" : "vcp-default"%>'>
                            <h4><%=VcpPage.Mdl_Bottom_Shadow_H4_2 %></h4>
                        </div>
                    </a>--%>
                    <a id="usedTag"  href='<%=GetRouteUrl("VCP", new {Type = "used" , Make = Make.Name.ToLower(), Model = ModelTranslated.TranslatedLinkName }) %>'>
                        <div class='col-md-6 <%= Type.ToLower() == "used" ? "vcp-cta" : "vcp-default"%>'>
                            <h4><%=VcpPage.Mdl_Bottom_Shadow_H4_3 %></h4>
                        </div>
                    </a>

                    <div class="clear"></div>
                    <div class="row-fluid clearfix in" id="vehicleImage">
                        <div class="">
                          <img class="vcp-main-image" alt="<%= Type + " " + Make.Name + " " + Model.Name %>" title="<%=Type + " " + Make.Name + " " + Model.Name %>" src="<%= VehicleImageUrl %>">
                        </div>
                    </div>
                    <div class="clear"></div>

                    <!-- new expander -->
                    <div class="fade-white" runat="server" ID="shortBlogDescriptionSection">
                        <p runat="server" ID="shortBlogDescriptionContent" class="in">
                            <!-- Summary Goes Here -->
                            
                            <!-- Summary Goes Here -->
                        </p>
                    </div>
                     <!-- end new -->
                    <!-- new colapse button -->
                    <div id="collapseTop" class="collapse in">
                        <span class="SeeMore2 vcp-cta vcp-read-more" data-toggle="collapse" data-target="#collapseTwo, #MainContent_shortBlogDescriptionContent, #collapseBottom"><%=VcpPage.SeeMore2_Expand %></span>
                    </div>
                    <!-- end collapse -->

                </div>
            </div>

            <div class="row vcp-tabs" runat="server" id="RelateSection">
                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                    <div id="collapseTwo" class="accordion-body collapse clearfix">
                        <asp:Literal ID="ltrPostText" runat="server" />
                        <!-- Base Article -->
                        <div class="tab-pane" id="Review">
                            <div class="blog-entry-detail">
                                <vcpCtrl:blogEntryDetail runat="server" ID="BlogEntryDetail" />
                            </div>
                            <div class="blog-entries container">
                                <vcpCtrl:BlogEntries runat="server" ID="BlogEntries" />
                            </div>
                        </div>
                        <!-- Base Article End -->
                    </div>
                    </div>
                </div>
                <div>
                    <!-- bottom colapse button -->
                    <div id="collapseBottom" class="collapse">
                        <a class="vcp-cta vcp-read-more" data-toggle="collapse" data-target="#collapseTwo, #MainContent_shortBlogDescriptionContent, #collapseBottom">Collapse</a>
                    </div>
                    <!-- end bottom collapse -->
                </div>
                <div class="tab-pane active" id="Inventory">
                    <searchUserControl:SearchControl runat="server" ID="SearchControl" />
                </div>
            </div>

        </div>
        <div class="clear-25"></div>
    </div>
    <FatFooterControl:FatFooter ID="FatFooter" runat="server" />
    <asp:HiddenField runat="server" ID="CurrentTab" Value="inventoryTab" />
    <asp:HiddenField runat="server" ID="CurrentModel" Value="" />
    <asp:HiddenField runat="server" ID="CurrentType" Value="" />

    <script>
        var localizations = common.getLocalizations();
        if ($("#blog-detail").length) { // do this if blog exists
            $('.SeeMore2').click(function () {
                var $this = $(this);
                $this.toggleClass('SeeMore2');
                if ($this.hasClass('SeeMore2')) {
                    $this.text(localizations["Expand"]);
                } else {
                    $this.text(localizations["Collapse"]);
                }
            });
        }
        else {
            $('.SeeMore2').hide();
        }

    </script>
</asp:Content>
