﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VcpBaseModel.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.VcpBaseModel" %>

<div id="blog-detail" class="container">
    <section class="section">
        <div class="section-inner">
            <div class="content">
                <div class="item featured text-left">
                    <!-- Temp Disabling
                    <div class="blog-herobox">
                        <div class="featured-image">
                            <asp:Image ID="imgPostFeatureImage" CssClass="img-responsive project-image" runat="server" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    Temp Disabling -->
                    <div class="desc blog-card">
                        <asp:Literal ID="ltrPostText" runat="server" />
                    </div>
                    <h3>
                        <asp:Literal ID="ltrMessages" Visible="False" runat="server" Text="Blog Could Not Found" /></h3>
                </div>
            </div>
        </div>
    </section>
</div>