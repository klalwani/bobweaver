﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="VehicleDisplay.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.NewCars.VehicleDisplay" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.NewCars" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../../Content/Style.css" rel="stylesheet" />
    <div id="vehicle-display" class="container">
        <section id="header" class="row">
            <div class="col-md-3 left nopadding">
                <div class="row">
                    <div class="col-md-4 nopadding">
                        <asp:Image ID="logo" CssClass="logo" runat="server" ImageUrl="~/Content/Images/logo.gif" />
                    </div>
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_BodyStyle %></b>
                                    <asp:Label ID="lblBody" runat="server">Sedan</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_ModelCode %></b>
                                    <asp:Label ID="lblModelNo" runat="server">POL</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Engine %></b>
                                    <asp:Label ID="lblEngine" runat="server">4 Cyl - 2.0 L</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Transmissions %></b>
                                    <asp:Label ID="lblTrans" runat="server">Variable</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_DriveType %></b>
                                    <asp:Label ID="lblDriveType" runat="server">FWD</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Ext %></b>
                                    <asp:Label ID="lblExtColor" runat="server">Ingot Silver</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Int %></b>
                                    <asp:Label ID="lblIntColor" runat="server" Text="Charcoal Black" />
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Vin %></b>
                                    <asp:Label ID="lblVin" Style="font-size: 9px;" runat="server">3FA6P0LU9GR102563</asp:Label>
                                </div>
                                <div class="row">
                                    <b><%=VehicleDisplay.Panel_Body_Row_Stock %></b>
                                    <asp:Label ID="lblStock" runat="server" Text="FG0004" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-left: 5px;">
                    <div class="row" style="margin-top: 10px; font-weight: bold;">
                        <asp:Label ID="lblcarinfo" CssClass="carinfo" runat="server">BUILD YOUR 2015 Ford</asp:Label>
                    </div>
                    <div class="row" style="font-weight: 900;">
                        <asp:Label ID="lblmodel" CssClass="model" runat="server">MURANO®</asp:Label>  <asp:Label ID="lblTextStyle" CssClass="text" runat="server">S</asp:Label>
                    </div>
                    <div class="row fuel">
                        <div class="col-lg-1" style="padding: 0px;">
                          
                        </div>
                        <div class="col-lg-7">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div style="float: left; width: 40px;">
                                        <div class="row">
                                            <%=VehicleDisplay.Panel_Body_Row_City %>
                                        </div>
                                        <div class="row" style="font-weight: 900; font-size: 18px;">
                                            <asp:Label ID="lblcity" runat="server">44</asp:Label>
                                        </div>
                                    </div>
                                    <div style="float: left;">
                                        <asp:Image ID="imageofgas" runat="server" ImageUrl="<%= DealerImageGeneric %>content/icons/gas-pump-40.png" />
                                    </div>
                                    <div style="float: left; width: 40px;">
                                        <div class="row">
                                            <%=VehicleDisplay.Panel_Body_Row_Hwy %>
                                        </div>
                                        <div class="row" style="font-weight: 900; font-size: 18px;">
                                            <asp:Label ID="lblhwy" runat="server">41</asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 midle nopadding">
                <div class="row">
                    <img runat="server" id="imgChrome" src="https://content.homenetiol.com/stock_images/5/2008FRD003a_640/2008FRD003a_640_01.jpg" />
                </div>
                <div class="row">
                    <button class="btn btn-default"><%=VehicleDisplay.Btn_Default_Exterior %></button>
                    <button class="btn btn-default"><%=VehicleDisplay.Btn_Default_Interior %></button>
                </div>
            </div>
            <div class="col-md-4 right nopadding">
                <div class="livechat">
                    <button type="button" class="btn btn-default <%=VehicleDisplay.Btnlivechat %>">Live Chat</button>
                </div>
                <div class="info">
                    <div class="price">
                        <div class="row" style="height: 15px; line-height: 15px;">
                            <div class="col-sm-5">
                                <span><%=VehicleDisplay.Col_Sm_5_MSRP %></span>
                            </div>
                            <div class="col-sm-6" style="text-align: right; text-decoration: line-through;">
                                <span>
                                    <asp:Label ID="lblMsrp" runat="server">$29,195</asp:Label></span>
                            </div>
                        </div>
                        <div class="row" style="height: 15px; line-height: 15px;">
                            <div class="col-sm-5">
                                <span><%=VehicleDisplay.Col_Sm_5_InternetPrice %></span>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                <span>
                                    <asp:Label ID="lblInternetPrice" runat="server">$26,406</asp:Label></span>
                            </div>
                        </div>
                    </div>
                    <div class="yousave">
                        <div class="col-sm-5">
                            <span><%=VehicleDisplay.Col_Sm_5_YS %></span>
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                            <span>
                                <asp:Label ID="lblSave" runat="server">$2,789</asp:Label></span>
                        </div>
                    </div>
                    <div class="row note">
                        <%=VehicleDisplay.Row_Note %>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-warning btnmenu"><%=VehicleDisplay.Btnmenu %></button>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-info btnmenu2"><%=VehicleDisplay.Btnmenu_2 %></button>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-info btnmenu2"><%=VehicleDisplay.Btnmenu_2_Value_Your_Trade %></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="content">
            <div class="row">
                <div class="leftInfo col-md-8 nopadding">
                    <div class="row images">
                        <asp:Repeater ID="rptImgList" runat="server">
                            <ItemTemplate>
                                <div class="product">
                                    <img src='<%# Container.DataItem.ToString() %>' alt="125x125">
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="clr"></div>
                    </div>
                    <div class="row">
                        <div class="info-product">
                            <ul id="features-product" class='nav nav-pills menu-option'>
                                <li class="active">
                                    <a href="#features"><%=VehicleDisplay.Menu_Option_Active %></a>
                                </li>
                                <li>
                                    <a href="#options"><%=VehicleDisplay.Menu_Option_1 %></a>
                                </li>
                                <li>
                                    <a href="#specs"><%=VehicleDisplay.Menu_Option_2 %></a>
                                </li>
                                <li>
                                    <a href="#warranty"><%=VehicleDisplay.Menu_Option_3 %></a>
                                </li>
                            </ul>
                            <div class="row tab-content" style="height: 440px; overflow-y: auto;">
                                <div id="features" class="tab-pane active">
                                    <%=VehicleDisplay.Tab_Pane_Active %>
                                </div>
                                <div id="options" class="tab-pane">
                                    <%=VehicleDisplay.Tab_Pane_1 %>
                                </div>
                                <div id="specs" class="tab-pane">
                                    <%=VehicleDisplay.Tab_Pane_2 %>
                                </div>
                                <div id="warranty" class="tab-pane">
                                    <%=VehicleDisplay.Tab_Pane_3 %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="relatedProduct">
                        <div class="row title title-35">
                            <%=VehicleDisplay.Row_Title_Ttitle_35 %>
                        </div>
                        <div class="productlist">
                            <div class="product">
                                <div class="row">
                                    <img src="../../Content/Images/Vehicle/related-product.png" />
                                </div>
                                <div class="row" style="margin-top: 5px !important">
                                    <p class="name">
                                        2015 Ford Fiesta SE
                                    </p>
                                </div>
                                <div class="row">
                                    <p class="price">
                                        <%=VehicleDisplay.Row_Price %>: $15,061
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                    </div>
                                    <div class="col-lg-7">
                                        0
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_BodyStyle %>
                                    </div>
                                    <div class="col-lg-7">
                                        Sedan
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                    </div>
                                    <div class="col-lg-7">
                                        Tuxedo Black
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 15px !important">
                                    <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="row">
                                    <img src="../../Content/Images/Vehicle/related-product.png" />
                                </div>
                                <div class="row" style="margin-top: 5px !important">
                                    <p class="name">
                                        2015 Ford Fiesta SE
                                    </p>
                                </div>
                                <div class="row">
                                    <p class="price">
                                        <%=VehicleDisplay.Row_Price %>: $15,061
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                    </div>
                                    <div class="col-lg-7">
                                        0
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_BodyStyle %>
                                    </div>
                                    <div class="col-lg-7">
                                        Sedan
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                    </div>
                                    <div class="col-lg-7">
                                        Tuxedo Black
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 15px !important">
                                    <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                                </div>
                            </div>
                            <div class="product" style="margin-right: 0px;">
                                <div class="row">
                                    <img src="../../Content/Images/Vehicle/related-product.png" />
                                </div>
                                <div class="row" style="margin-top: 5px !important">
                                    <p class="name">
                                        2015 Ford Fiesta SE
                                    </p>
                                </div>
                                <div class="row">
                                    <p class="price">
                                        <%=VehicleDisplay.Row_Price %>: $15,061
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                    </div>
                                    <div class="col-lg-7">
                                        0
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_BodyStyle %>
                                    </div>
                                    <div class="col-lg-7">
                                        Sedan
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 bold">
                                        <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                    </div>
                                    <div class="col-lg-7">
                                        Tuxedo Black
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 15px !important">
                                    <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
                <div class="rightInfo col-md-4">
                    <div class="row" style="padding: 0px 10px;">
                        <p><%=VehicleDisplay.RightInfo_Row_P %></p>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b>$29,560</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-7" style="color: #707070;">
                                      <asp:Label ID="lblModel1" CssClass="text" runat="server">Murano</asp:Label><sup>®</sup>&nbsp;<asp:Label ID="lblTextStyle1" CssClass="text" runat="server">S</asp:Label>
                                </div>
                                <div class="col-lg-5">
                                    <b>-</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-7" style="color: #707070;">
                                    <%=VehicleDisplay.Col_Lg_7 %>
                                </div>
                                <div class="col-lg-5">
                                    <b>-</b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_1 %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b><%=VehicleDisplay.Col_Lg_5_B %></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8" style="color: #707070;">
                                   <%=VehicleDisplay.Col_Lg_8 %>
                                </div>
                                <div class="col-lg-4" style="text-align: right;">
                                    <b>-</b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_2 %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_3 %></b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_4 %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b>-</b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_5 %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b>-</b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_6 %></b>
                                </div>
                                <div class="col-lg-5">
                                    <b>$885</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9" style="color: #6a6a6a; padding: 10px 0px 0px;">
                                    <%=VehicleDisplay.Col_Lg_9 %>
                                </div>
                                <div class="col-lg-3" style="text-align: right; padding: 10px 0px 0px;">
                                    <b>$885</b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-7">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_7 %>
                                    <br />
                                        <%=VehicleDisplay.Col_Lg_7_B_8 %> <span style="color: #c3002f;">[*]</span></b>
                                </div>
                                <div class="col-lg-5" style="font-size: 24px;">
                                    <b><sup>$</sup>30,445</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-7" style="margin-top: 15px;">
                                    <b><%=VehicleDisplay.Col_Lg_7_B_9 %></b>
                                </div>
                            </div>
                        </div>
                        <hr class="divider">
                        <div class="row">
                            <div class="col-lg-4">
                                <button class="button button-black button-25"><%=VehicleDisplay.Button_Black %></button>
                            </div>
                            <div class="col-lg-4">
                                <button class="button button-gray button-25"><%=VehicleDisplay.Button_Gray %></button>
                            </div>
                            <div class="col-lg-4" style="text-align: center;">
                                <div style="vertical-align: middle;">
                                    <span>$</span>
                                    <b style="font-size: 20px;">540</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8" style="color: #6a6a6a; margin: 10px 0px;">
                                    <%=VehicleDisplay.Col_Lg_8_1 %>
                            <br />
                                    <%=VehicleDisplay.Col_Lg_8_2 %>
                                </div>
                            </div>
                            <div class="row">
                                <b>> <%=VehicleDisplay.Row_B %></b>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 0px 0px 0px 5px;">
                        <div class="interested">
                            <div class="row title title-30" style="text-align: center;">
                                <%=VehicleDisplay.Title_30 %>
                            </div>
                            <div class="info">
                                <div class="form-group">
                                    <input class="form-control" placeholder="<%=VehicleDisplay.Info_Form_Group_Input_Placeholder_1 %>" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="<%=VehicleDisplay.Info_Form_Group_Input_Placeholder_2 %>" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="<%=VehicleDisplay.Info_Form_Group_Input_Placeholder_3 %>" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="<%=VehicleDisplay.Info_Form_Group_Input_Placeholder_4 %>" />
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control comment" placeholder="Comment" rows="3"></textarea>
                                </div>
                                <div class="radio-list">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                <%=VehicleDisplay.Radio_Label %>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked>
                                                <%=VehicleDisplay.Radio_Label_1 %>
                                            </label>
                                        </div>
                                        <div class="row" style="height: 20px; line-height: 20px;">
                                            <div style="float: left;">$</div>
                                            <div class="col-sm-5" style="padding: 0px; float: left;">
                                                <input class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option1" checked>
                                                <%=VehicleDisplay.Radio_Label_2 %>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked>
                                                <%=VehicleDisplay.Radio_Label_3 %>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 10px;">
                                    <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary_1 %></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            <div class="row">
                <div class="recentView">
                    <div class="row title title-30">
                        <%=VehicleDisplay.Title_30_1 %>
                    </div>
                    <div class="row productlist">
                        <div class="product">
                            <div class="row">
                                <img src="../../Content/Images/Vehicle/related-product.png" />
                            </div>
                            <div class="row" style="margin-top: 5px !important">
                                <p class="name">
                                    2015 Ford Fiesta SE
                                </p>
                            </div>
                            <div class="row">
                                <p class="price">
                                     <%=VehicleDisplay.Row_Price %>: $15,061
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                </div>
                                <div class="col-lg-6">
                                    0
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Body %>
                                </div>
                                <div class="col-lg-6">
                                    Sedan
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                </div>
                                <div class="col-lg-6">
                                    Tuxedo Black
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                            </div>
                        </div>
                        <div class="product">
                            <div class="row">
                                <img src="../../Content/Images/Vehicle/related-product.png" />
                            </div>
                            <div class="row" style="margin-top: 5px !important">
                                <p class="name">
                                    2015 Ford Fiesta SE
                                </p>
                            </div>
                            <div class="row">
                                <p class="price">
                                    <%=VehicleDisplay.Row_Price %>: $15,061
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                </div>
                                <div class="col-lg-6">
                                    0
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Body %>
                                </div>
                                <div class="col-lg-6">
                                    Sedan
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                </div>
                                <div class="col-lg-6">
                                    Tuxedo Black
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button class="btn btn-primary">Details</button>
                            </div>
                        </div>
                        <div class="product">
                            <div class="row">
                                <img src="../../Content/Images/Vehicle/related-product.png" />
                            </div>
                            <div class="row" style="margin-top: 5px !important">
                                <p class="name">
                                    2015 Ford Fiesta SE
                                </p>
                            </div>
                            <div class="row">
                                <p class="price">
                                    <%=VehicleDisplay.Row_Price %>: $15,061
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                </div>
                                <div class="col-lg-6">
                                    0
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Body %>
                                </div>
                                <div class="col-lg-6">
                                    Sedan
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                </div>
                                <div class="col-lg-6">
                                    Tuxedo Black
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                            </div>
                        </div>
                        <div class="product">
                            <div class="row">
                                <img src="../../Content/Images/Vehicle/related-product.png" />
                            </div>
                            <div class="row" style="margin-top: 5px !important">
                                <p class="name">
                                    2015 Ford Fiesta SE
                                </p>
                            </div>
                            <div class="row">
                                <p class="price">
                                    <%=VehicleDisplay.Row_Price %>: $15,061
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                </div>
                                <div class="col-lg-6">
                                    0
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_BodyStyle %>
                                </div>
                                <div class="col-lg-6">
                                    Sedan
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                </div>
                                <div class="col-lg-6">
                                    Tuxedo Black
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                            </div>
                        </div>
                        <div class="product">
                            <div class="row">
                                <img src="../../Content/Images/Vehicle/related-product.png" />
                            </div>
                            <div class="row" style="margin-top: 5px !important">
                                <p class="name">
                                    2015 Ford Fiesta SE
                                </p>
                            </div>
                            <div class="row">
                                <p class="price">
                                    <%=VehicleDisplay.Row_Price %>: $15,061
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Col_Lg_4_Bold_Mil %>
                                </div>
                                <div class="col-lg-6">
                                    0
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_BodyStyle %>
                                </div>
                                <div class="col-lg-6">
                                    Sedan
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 bold">
                                    <%=VehicleDisplay.Panel_Body_Row_Ext %>
                                </div>
                                <div class="col-lg-6">
                                    Tuxedo Black
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button class="btn btn-primary"><%=VehicleDisplay.Btn_Primary %></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer">
        </section>
    </div>
    <script type="text/javascript">
        $('#features-product li').click(function (e) {
            e.preventDefault();
            $(this).find("a:eq(0)").tab('show');
            $(this).css("active");
        });
    </script>
</asp:Content>

