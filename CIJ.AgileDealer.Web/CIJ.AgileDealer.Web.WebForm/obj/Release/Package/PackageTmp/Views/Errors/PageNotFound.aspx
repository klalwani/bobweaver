﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master"  CodeBehind="PageNotFound.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Errors.PageNotFound" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Errors" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1><%=PageNotFound.Inner_Container_Col_Md_12_H1 %></h1>
            </div>
        </div>
    </div>
    
    <div class="body-content-wrapper">
        <div class="body-content-card">

            

            <div class="row padded-15">
                
                <div class="col-md-12">
                    <a class="hidden-xs" title="<%= DealerName %> 404 Page Logo" href="/"><img src="<%= DealerImageGeneric %>content/404-logo.jpg" style="max-width: 100%; float: right;" alt="<%= DealerName %> 404 Page Logo"></a>
                    <h2><%=PageNotFound.Col_Md_12_H2 %></h2>
                   
                </div>
                <div class="col-xs-6 col-xs-offset-2 col-sm-6 col-sm-offset-0" id="404-nav-section">
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" class="active"><a href="/" class="ctaLink"><span class="glyphicon glyphicon-home padded-15-right" aria-hidden="true"></span><%=PageNotFound.Nav_Stacked_Li_1 %></a></li>
                        <li role="presentation"><a href="/new" class="ctaLink"><%=PageNotFound.Nav_Stacked_Li_2 %></a></li>
                        <li role="presentation"><a href="/used" class="ctaLink"><%=PageNotFound.Nav_Stacked_Li_3 %></a></li>
                        <li role="presentation"><a href="/research" class="ctaLink"><%=PageNotFound.Nav_Stacked_Li_4 %></a></li>
                        <li role="presentation"><a href="/contactus" class="ctaLink"><%=PageNotFound.Nav_Stacked_Li_5 %></a></li>
                    </ul>


                </div>
                <div class="clear-20"></div>
            </div>



            
        </div>
    </div>
    <div class="clear-20"></div>
</asp:Content>
