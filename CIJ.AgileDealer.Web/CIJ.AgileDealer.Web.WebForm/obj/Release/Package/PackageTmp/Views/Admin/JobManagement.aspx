﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobManagement.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.JobManagement" %>

<%@ Register Src="~/Views/UserControl/Admin/JobManagementControl.ascx" TagPrefix="jobManagementControl" TagName="JobManagementControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Inventory Management<br />
                    <small>Manually trigger Inventory and Incentive services</small></h1>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>
    <div class="col-xs-12">
        <jobManagementControl:JobManagementControl runat="server" ID="JobManagementControl" />
    </div>
    <div class="clear-20"></div>
</asp:Content>
