﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="EmployeeDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.EmployeeDashboard" %>


<%@ Register Src="~/Views/UserControl/Admin/EmployeeDashboardControl.ascx" TagPrefix="EmployeeDashboard" TagName="EmployeeDashboardControl" %>
<%@ Register TagPrefix="staffEditControl" TagName="StaffEditControl" Src="~/Views/UserControl/Admin/StaffEditControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <EmployeeDashboard:EmployeeDashboardControl runat="server" ID="EmployeeDashboardControl" />
    <div class="hidden">
        <div id="employee-update" class="col-xs-12">
            <staffEditControl:StaffEditControl runat="server" ID="StaffEditControl" />
        </div>
    </div>
</asp:Content>


