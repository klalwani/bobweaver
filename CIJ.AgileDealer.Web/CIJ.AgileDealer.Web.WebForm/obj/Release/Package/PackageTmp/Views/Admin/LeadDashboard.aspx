﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LeadDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.LeadDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/ApplicantDashboardControl.ascx" TagPrefix="LeadDashboard" TagName="ApplicantDashboard" %>
<%@ Register Src="~/Views/UserControl/Admin/ContactUsControl.ascx" TagPrefix="LeadDashboard" TagName="ContactUsControl" %>
<%@ Register Src="~/Views/UserControl/Admin/ScheduleServiceDashboardControl.ascx" TagPrefix="LeadDashboard" TagName="ScheduleServiceDashboardControl" %>
<%@ Register Src="~/Views/UserControl/Admin/CheckAvailabilityDashboardControl.ascx" TagPrefix="LeadDashboard" TagName="CheckAvailabilityDashboardControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="../../Scripts/Pages/Admin/AgileDealer.LeadDashboard.js"></script>

    <div id="leadDashboards" class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#applicationDashboard">Finance</a></li>
            <li><a data-toggle="tab" href="#contactUsDashboard">Contact Us</a></li>
            <li><a data-toggle="tab" href="#serviceDashboard">Schedule Service</a></li>
            <li><a data-toggle="tab" href="#checkAvailabilityDashboard">Check Availability</a></li>
        </ul>

        <div class="tab-content ">
            <div id="applicationDashboard" class="tab-pane fade in active">
                <LeadDashboard:ApplicantDashboard runat="server" ID="ApplicantDashboard" />
            </div>
            <div id="contactUsDashboard" class="tab-pane fade">
                <LeadDashboard:ContactUsControl runat="server" ID="ContactUsControl" />
            </div>
            <div id="serviceDashboard" class="tab-pane fade">
                <LeadDashboard:ScheduleServiceDashboardControl runat="server" ID="ScheduleServiceDashboardControl" />
            </div>
            <div id="checkAvailabilityDashboard" class="tab-pane fade">
                <LeadDashboard:CheckAvailabilityDashboardControl runat="server" ID="CheckAvailabilityDashboardControl" />
            </div>
        </div>
    </div>
    <div class="clear-20"></div>
</asp:Content>


