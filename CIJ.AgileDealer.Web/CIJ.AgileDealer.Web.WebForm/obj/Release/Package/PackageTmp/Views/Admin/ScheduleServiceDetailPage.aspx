﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScheduleServiceDetailPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.ScheduleServiceDetailPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/Admin/AgileDealer.ScheduleServiceDetail.js"></script>
    <div class="container" id="scheduleServiceCtr">
        <div class="row form-wrapper">
            <div class="row form-title">
                <span>Schedule Service Online</span>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="row content-padded">
                            <h3>Choose a Date:
                            </h3>
                            <input type="text" id="serviceDate" class="form-control" data-val="true" data-val-required="true">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="row content-padded" id="timepicker">
                            <h3>Choose a Time: </h3>
                            <select class="form-control" id="weekday-hours" data-val="true" data-val-required="true">
                                <option disabled="" value="" selected="">Weekly hours</option>
                                <option disabled="" value=""></option>
                                <asp:Repeater ID="rptScheduleTimeNormal" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                            <select class="form-control" id="saturday-hours">
                                <option disabled="" value="" selected="">Saturday hours</option>
                                <option disabled="" value=""></option>
                                <asp:Repeater ID="rptScheduleTimeSaturday" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("Hour")%>"><%#Eval("Hour")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div class="col-md-6" id="contactInfo">
                        <div class="row content-padded">
                            <div class="row content-padded">
                                <h3>Contact Info</h3>
                            </div>

                            <fieldset class="form-group">
                                <label for="contactFirstName" class="sr-only">First Name</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactFirstName" placeholder="First Name">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="contactLastName" class="sr-only">Last Name</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactLastName" placeholder="Last Name">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="contactPhone" class="sr-only">Phone Number</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPhone" placeholder="Phone Number">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="contactEmail" class="sr-only">Email Address</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactEmail" placeholder="Email Address">
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-md-6" id="vehicleInfo">

                        <div class="row content-padded">
                            <div class="row content-padded">
                                <h3>Vehicle Info</h3>
                            </div>

                            <fieldset class="form-group">
                                <label for="vehicleYear" class="sr-only">Year</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleYear" placeholder="Year">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="vehicleMake" class="sr-only">Make</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleMake" placeholder="Make">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="vehicleModel" class="sr-only">Model</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleModel" placeholder="Model">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="vehicleMileage" class="sr-only">Mileage</label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="vehicleMileage" placeholder="Mileage">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="serviceWarranty" class="control-label form-label-500">Under Warranty?</label>
                                <div class="btn-group multiButtonDefault" id="serviceWarranty" data-toggle="buttons">
                                    <label class="btn btn-default active">
                                        <input type="radio" name="serviceWarranty" value="1" autocomplete="off" checked>
                                        No
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="serviceWarranty" value="2" autocomplete="off">
                                        Not Sure
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="serviceWarranty" value="3" autocomplete="off">
                                        Yes, Under Warantee
                                    </label>
                                </div>
                            </fieldset>

                        </div>
                    </div>

                </div>

                <div class="row content-padded">

                    <div class="col-md-6 col-sm-12 service-sub-card" id="serviceRequested">
                        <div class="row form-sub-title">
                            <span>Services Requested</span>
                        </div>
                        <div class="row content-padded">
                            <asp:Repeater ID="rptServiceRequested" runat="server">
                                <ItemTemplate>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="<%#Eval("ServiceRequestedId")%>"><%#Eval("Name")%>
                                        </label>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 service-sub-card" id="currentIssue">
                        <div class="row form-sub-title">
                            <span>Current Issues</span>
                        </div>

                        <div class="row content-padded">
                            <asp:Repeater ID="rptCurrentIssue" runat="server">
                                <ItemTemplate>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="<%#Eval("CurrentIssueId")%>"><%#Eval("Name")%>
                                        </label>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <fieldset class="form-group">
                            <label for="contactMessage" class="sr-only">Additional Information</label>
                            <textarea class="form-control" id="contactMessage" rows="3" placeholder="Additional Information"></textarea>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField id="scheduleServiceId"  runat="server"/>
</asp:Content>
