﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Admin.Dashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/ApplicantDashboardControl.ascx" TagPrefix="ApplicantDashboard" TagName="ApplicantDashboard" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <ApplicantDashboard:ApplicantDashboard runat="server" ID="ApplicantDashboard" />
</asp:Content>




