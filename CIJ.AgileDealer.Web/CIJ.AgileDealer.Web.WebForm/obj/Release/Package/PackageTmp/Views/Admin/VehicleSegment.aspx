﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VehicleSegment.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.VehicleSegment" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.SegmentUpdate.js"></script>
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Vehicle Segments<br />
                    <small>Change, update, or add missing</small></h1>
            </div>
        </div>
    </div>

    <div id="vehicle-segment-page">
        <div class="body-content-wrapper">
            <div class="body-content-card">
                <div class="container-fluid padded-15">
                    <div class="row col-xs-5 padded-15 ">
                        <select id="ddlMake" class="form-control" title="Select Segment">
                            <option value="">-- Select Brand --</option>
                        </select>
                    </div>
                    <div class="row padded-15">
                        <table id="tableSegment" class="table table-hover table-condensed row-border cell-border display stripe" data-order='[[ 3, "asc" ]]' data-page-length='25'>
                            <thead>
                                <tr>
                                    <th>CompetitiveModelId</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Segment</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>CompetitiveModelId</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Segment</th>
                                    <th>Update</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Example Popup -->
    <div class="hidden">
        <input type="hidden" id="competitiveModelId" value="" />
        <div id="segment-list" class="container col-md-12">
            <div class="padded-15">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Make</label>
                        <div class="col-sm-8">
                            <input class="form-control" id="txtMake" type="text" placeholder="Make" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Model</label>
                        <div class="col-sm-8">
                            <input class="form-control" id="txtModel" type="text" placeholder="Model" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Segment</label>
                        <div class="col-sm-8">
                            <%--<input class="form-control" type="text" placeholder="Segment">--%>
                            <select id="ddlSegment" class="form-control" title="Select Segment">
                            </select>
                            <!-- This would be a drop down with the options avaible -->
                        </div>
                    </div>

                    <button type="button" id="btnUpdateSegment" class="btn btn-primary pull-right">Update</button>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="SegmentHdf" />
    <asp:HiddenField runat="server" ID="ModelHdf" />


</asp:Content>
