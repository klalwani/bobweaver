﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicantDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.ApplicantDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/ApplicantDashboardControl.ascx" TagPrefix="ApplicantDashboard" TagName="ApplicantDashboardControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <ApplicantDashboard:ApplicantDashboardControl runat="server" ID="ApplicantDashboardControl" />
</asp:Content>




