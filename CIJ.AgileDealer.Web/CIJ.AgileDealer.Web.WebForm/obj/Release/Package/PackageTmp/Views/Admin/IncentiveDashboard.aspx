﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="IncentiveDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveDashboard" %>


<%@ Register Src="~/Views/UserControl/Admin/IncentiveDashboardControl.ascx" TagPrefix="IncentiveDashboard" TagName="IncentiveDashboardControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveDashboardSpecialProgramControl.ascx" TagPrefix="IncentiveDashboardSepcialProgram" TagName="IncentiveDashboardSpecialProgramControl" %>
<%@ Register Src="~/Views/UserControl/Admin/HideIncentivePrograms.ascx" TagPrefix="HideIncentivePrograms" TagName="HideIncentiveProgramsControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveDashboardCustomDiscountsControl.ascx" TagPrefix="IncentiveDashboardCustomDiscounts" TagName="IncentiveDashboardCustomDiscountsControl" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Incentive Management<br />
                    <small>Edit, update, or remove custom incentives</small></h1>
            </div>
        </div>
    </div>
    <div class="body-content-wrapper">
        <div class="container-fluid">

            <div id="leadDashboards" class="container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#ProgramIncentiveDash">Manufacturer Incentives</a></li>
                    <li><a data-toggle="tab" href="#SpecialCoupons">Custom Incentives</a></li>
                    <li><a data-toggle="tab" href="#HideIncentivePrograms">Hide Incentive Programs</a></li>
                    <li><a data-toggle="tab" href="#CustomDiscount">Custom Discounts</a></li>
                </ul>

                <div class="tab-content ">
                    <div id="ProgramIncentiveDash" class="tab-pane fade in active">
                        <IncentiveDashboard:IncentiveDashboardControl runat="server" ID="IncentiveDashboardControl" />
                    </div>
                    <div id="SpecialCoupons" class="tab-pane fade">
                        <IncentiveDashboardSepcialProgram:IncentiveDashboardSpecialProgramControl runat="server" ID="IncentiveDashboardSpecialCoupons" />
                    </div>
                    <div id="HideIncentivePrograms" class="tab-pane fade">
                        <HideIncentivePrograms:HideIncentiveProgramsControl runat="server" ID="HideIncentiveProgramsControl" />
                    </div>
                    <div id="CustomDiscount" class="tab-pane fade">
                        <IncentiveDashboardCustomDiscounts:IncentiveDashboardCustomDiscountsControl runat="server" ID="IncentiveDashboardCustomDiscounts" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear-20"></div>

    <script>



        $(document).ready(function () {


            $('select[multiple]').multiselect({
                columns: 2,
                placeholder: 'Select options'
            });

        });
    </script>
</asp:Content>

