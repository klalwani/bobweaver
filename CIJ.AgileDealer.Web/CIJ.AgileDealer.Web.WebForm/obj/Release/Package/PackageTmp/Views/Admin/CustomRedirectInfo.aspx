﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CustomRedirectInfo.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.CustomRedirectInfo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Custom Redirect Info<br />
                    <small>Instantly update 301 redirect info</small></h1>
            </div>
        </div>
    </div>

    <div id="customRedirectDashboard" class="">
        <div class="row form-title">
            <span>Customer Redirect Info Dashboard</span>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <asp:GridView ID="grdCustomRedirect" UseAccessibleHeader="True" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-hover table-bordered table-striped"
                        DataKeyNames="CustomRedirectUrlId" AllowPaging="True" PageSize="15" AllowCustomPaging="True" OnRowDataBound="grdCustomRedirect_OnRowDataBound"
                        OnRowDeleting="grdCustomRedirect_OnRowDeleting" OnPageIndexChanging="grdCustomRedirect_OnPageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="CustomRedirectUrlId" HeaderText="ID" ReadOnly="True" Visible="False"/>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="OldUrl" HeaderText="Orld Url" SortExpression="OldUrl">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NewUrl" HeaderText="New Url" SortExpression="NewUrl">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IsPermanentRedirect" HeaderText="Is Permanent" />
                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" />
                        </Columns>
                        <HeaderStyle BorderWidth="0" Font-Bold="True" ForeColor="Black" />
                    </asp:GridView>
                </div>
            </div>
        </div>


        <div class="addSection col-lg-12 mdl-white-box margintop10">
            <div class="clear-20"></div>

            <div class="row col-md-12">
                <div class="label-group col-md-3">
                    <label>Name</label>
                    <asp:TextBox TextMode="MultiLine" Rows="3" runat="server" ID="txtName" />
                </div>
                <div class="label-group col-md-3">
                    <label>Old Url</label>
                    <asp:TextBox TextMode="MultiLine" Rows="3" runat="server" ID="txtOrlUrl" />
                </div>
                <div class="label-group col-md-3">
                    <label>New Orl</label>
                    <asp:TextBox TextMode="MultiLine" Rows="3"  runat="server" ID="txtNewUrl" />
                </div>

                <div class="label-group col-md-3 breakline-checkbox">
                    <label>
                        Is Permanent Redirect </br>
                        <asp:CheckBox runat="server" ID="isPermanentRedirect" />
                    </label>
                </div>
            </div>

            <div class="clear-20"></div>

            <div class="row label-group text-center">
                <input type="button" runat="server" class="btn btn-primary" id="btnSave" onserverclick="btnSave_OnServerClick" title="Add" value="Add" />
                <input type="button" runat="server" class="btn btn-primary" ID="btnClear" OnServerClick="btnClear_OnServerClick" title="Click to clear the form" value="Cancel" />
            </div>

            <div class="clear-20"></div>
        </div>
    </div>
</asp:Content>
