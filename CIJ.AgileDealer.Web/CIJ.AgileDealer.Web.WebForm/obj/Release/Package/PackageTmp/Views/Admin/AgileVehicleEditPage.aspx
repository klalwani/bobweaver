﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AgileVehicleEditPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.AgileVehicleEditPage" %>

<%@ Register Src="~/Views/UserControl/Admin/AgileVehicleEditControl.ascx" TagPrefix="agileVehicleEditControl" TagName="AgileVehicleEditControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Edit Vehicle Listing<br />
                    <small>Instantly update website values</small></h1>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>
    <div id="vehicle-update" class="container mdl-box-shadow edit-vehicle-listing">
        <agileVehicleEditControl:AgileVehicleEditControl runat="server" ID="AgileVehicleEditControl" />
    </div>
    <div class="clear-20"></div>
</asp:Content>
