﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckAvailabilityDetail.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.CheckAvailabilityDetail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/Admin/AgileDealer.CheckAvailabilityDetail.js"></script>
    <input type="hidden" id="IsArchived" />
    <div class="container" id="contactUsForm">
        <div class="container row standardForm">
            <div class="row form-title">
                <span>Check Availability Detail</span>
                <input type="button" id="btnBack" name="btnBack" value="Go Back" class="btn btn-default pull-right margin10 no-print" />
            </div>
        </div>
        <div class="container row standardForm">
            <div class="col-md-12 col-xs-12">
                <div class="row content-padded">
                    <div class="row form-horizontal-input-padded">
                        <div id="contactName">
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label>Customer Name</label>
                                    <label for="custName" class="sr-only">Customer Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="custName" placeholder="Customer Name">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label>Email</label>

                                    <label for="email" class="sr-only">Email</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="email" placeholder="Email">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label>Phone Number</label>

                                    <label for="phoneNumber" class="sr-only">Phone Number</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="phoneNumber" placeholder="Phone Number">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label>Vin</label>

                                    <label for="vin" class="sr-only">Vin</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="vin" placeholder="Vin">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label>Dealer Name</label>

                                    <label for="dealerName" class="sr-only">Dealer Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="dealerName" placeholder="Dealer Name">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset class="form-group rowElem">
                                    <label>Message</label>
                                    <textarea class="form-control" rows="5" id="userMessage" placeholder="Message"></textarea>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="vehicleFeedbackId" runat="server" />
</asp:Content>
