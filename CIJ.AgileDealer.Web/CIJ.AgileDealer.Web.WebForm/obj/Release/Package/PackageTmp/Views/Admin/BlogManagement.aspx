﻿<%@ Page Language="C#"   MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BlogManagement.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.BlogManagement" %>

<%@ Register Src="~/Views/UserControl/Admin/BlogManagementControl.ascx" TagPrefix="BlogManagementDashboard" TagName="BlogManagementDashboard" %>
<%@ Register Src="~/Views/UserControl/Admin/BlogEditControl.ascx" TagPrefix="BlogManagementDashboard" TagName="BlogEditControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <BlogManagementDashboard:BlogManagementDashboard runat="server" ID="BlogManagementDashboard" />
    <div class="hidden">
        <div id="blog-update" class="col-xs-12">
            <BlogManagementDashboard:BlogEditControl runat="server" ID="BlogEditControl" />
        </div>
    </div>
    <div class="clear-20"></div>
</asp:Content>
