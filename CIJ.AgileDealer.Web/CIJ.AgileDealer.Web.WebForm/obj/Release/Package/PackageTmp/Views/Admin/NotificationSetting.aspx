﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotificationSetting.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.NotificationSetting" %>

<%@ Register Src="~/Views/UserControl/Admin/NotificationSettingControl.ascx" TagPrefix="notificationSettingControl" TagName="NotificationSettingControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <notificationSettingControl:NotificationSettingControl runat="server" id="NotificationSettingControl" />
    <div class="clear-20"></div>
</asp:Content>
