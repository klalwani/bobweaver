﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicantDetail.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.ApplicantDetail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Scripts.Render("~/bundles/ApplicantDetail") %>
    <%--<script src="../../../Scripts/Pages/Dashboards/AgileDealer.ApplicantDetail.js"></script>--%>
    <asp:HiddenField runat="server" ID="ApplicantId" />
    <input type="hidden" id="IsArchived" />
    <div class="container" id="applicantDetailForm">
        <div class="container row" id="instantlyCtr">
            <div class="row form-title">
                <span>Online Credit Approval Detail</span>
                <input type="button" id="btnBack" name="btnBack" value="Go Back" class="btn btn-default pull-right margin10 no-print" />
                <input type="button" id="btnPrintApplicantDetail" name="btnPrintApplicantDetail" value="Print this Application" class="btn btn-default pull-right margin10 no-print" />
                <input type="button" runat="server" id="btnPrintApplicantDetailServer" onserverclick="btnPrintApplicantDetailServer_OnServerClick" name="btnPrintApplicantDetailServer" value="Export to Excel" class="btn btn-default pull-right margin10 no-print" />
                <input type="button" id="btnArchived" name="btnArchived" value="Archive Application" class="btn btn-default pull-right margin10 no-print" />

            </div>
            <div class="col-md-12 col-xs-12">
                <div class="row content-padded">
                    <h1>Instantly Pre-Qualify</h1>
                </div>


                <div class="row content-padded">

                    <fieldset class="form-group">
                        <label for="creditAppType" class="control-label form-label-500">Application Type</label>
                        <div class="btn-group multiButtonDefault" id="creditAppType" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="creditAppType" value="1" id="option1" autocomplete="off" checked>
                                Finance 
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="creditAppType" value="2" id="option2" autocomplete="off">
                                Not Sure 
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="creditAppType" value="3" id="option3" autocomplete="off">
                                Lease 
                            </label>
                        </div>
                    </fieldset>


                    <div class="row form-horizontal-input-padded">
                        <div id="contactName">
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactFirstName" class="field-label" style="margin-left:0px">First Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantFirstName" placeholder="First Name">
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactLastName" class="field-label" style="margin-left:0px">Last Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantLastName" placeholder="Last Name">
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div class="row form-horizontal-input-padded">
                        <div id="contactPhoneZip">
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactPhone" class="field-label" style="margin-left:0px">Phone Number</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantPhone" placeholder="Phone Number">
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactZip" class="field-label" style="margin-left:0px">Zip Code</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantZip" placeholder="Zip Code">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container row" id="contactInfoCtr">
            <div class="col-md-12 col-xs-12">

                <div class="row content-padded">
                    <div class="row content-padded">
                        <h1>Contact Info</h1>
                    </div>

                    <fieldset class="form-group rowElem">
                        <label for="contactStreetAddress" class="field-label" style="margin-left:0px">Street Address</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactStreetAddress" placeholder="Current Address">
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <div id="contactAddressFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="contactCity" class="field-label" style="margin-left:0px">City</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCity" placeholder="City">
                                </fieldset>
                            </div>

                            <div class="col-md-3">
                                <fieldset class="form-group rowElem">
                                    <label for="contactState" class="field-label" style="margin-left:0px">State</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="contactState" placeholder="State">
                                        <asp:Repeater ID="rptState" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-md-4">
                                <fieldset class="form-group rowElem">
                                    <label for="contactZip" class="field-label" style="margin-left:0px">Zip Code</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactZip" placeholder="Zip Code">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="form-group form-line-spacer"></div>

                    <fieldset class="form-group rowElem">
                        <label for="contactResidenceStatus" class="field-label" style="margin-left:0px">Residence Status</label>
                        <select class="form-control" data-val="true" data-val-required="true" id="contactResidenceStatus" placeholder="Residence Status">
                            <option value="" selected disabled>Residence Status</option>
                            <option value="" disabled></option>
                            <asp:Repeater ID="rptResidenceStatus" runat="server">
                                <ItemTemplate>
                                    <option value="<%#Eval("ResidenceStatusId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </fieldset>


                    <fieldset class="form-group rowElem">
                        <label for="contactMonthlyHousing" class="field-label" style="margin-left:0px">Monthly Mortgage or Rent</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactMonthlyHousing" placeholder="Monthly Mortgage or Rent">
                            <div class="input-group-addon custom-popover" data-content="Include the total paid for all mortgages, second mortgages, second homes in whole dollars." rel="popover" data-placement="left" data-original-title="What to include:" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>


                    <div class="row form-horizontal-input-padded">
                        <label for="contactResidence" class="control-label form-label-500">How long have you lived at your current address?</label>
                        <div id="contactResidence">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactResidenceYears" class="field-label" style="margin-left:0px">Years at this address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactResidenceYears" placeholder="">
                                        <span class="input-group-addon" id="contactResidenceYearsAddon">Years</span>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactResidenceMonths" class="field-label" style="margin-left:0px">Months at this address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactResidenceMonths" placeholder="">
                                        <span class="input-group-addon" id="contactResidenceMonthsAddon">Months</span>
                                    </div>
                                </fieldset>
                            </div>

                        </div>

                    </div>

                    <div id="priorResidence">
                        <div class="form-group form-line-spacer"></div>

                        <fieldset class="form-group rowElem">
                            <label for="contactPreviousStreetAddress" class="field-label" style="margin-left:0px">Previous Address</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousStreetAddress" placeholder="Previous Address">
                        </fieldset>

                        <div class="row form-horizontal-input-padded">
                            <div id="contactAddressFull">

                                <div class="col-md-5">
                                    <fieldset class="form-group rowElem">
                                        <label for="contactPreviousCity" class="field-label" style="margin-left:0px">Previous City</label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousCity" placeholder="Previous City">
                                    </fieldset>
                                </div>

                                <div class="col-md-3">
                                    <fieldset class="form-group rowElem">
                                        <label for="contactPreviousState" class="field-label" style="margin-left:0px">Previous State</label>
                                        <select class="form-control" data-val="true" data-val-required="true" id="contactPreviousState">
                                            <asp:Repeater ID="rptState2" runat="server">
                                                <ItemTemplate>
                                                    <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <fieldset class="form-group rowElem">
                                        <label for="contactPreviousZip" class="field-label" style="margin-left:0px">Previous Zip Code</label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousZip" placeholder="Previous Zip Code">
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>



                    <div class="row content-padded">
                        <h3>Applicant Info</h3>
                    </div>

                    <fieldset class="form-group">
                        <label for="creditAppParties" class="control-label form-label-500">Number of Applicants</label>
                        <div class="btn-group multiButtonDefault" id="creditAppParties" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="creditAppParties" value="1" autocomplete="off" checked>
                                Individual 
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="creditAppParties" value="2" autocomplete="off">
                                Joint (Co-Applicant) 
                            </label>
                        </div>
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <label for="contactDOB" class="control-label form-label-500">Date of Birth</label>
                        <div id="contactDOB">

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="contactBirthMonth" class="field-label" style="margin-left:0px">Month</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="contactBirthMonth">
                                        <option value="" selected disabled>Month</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptMonth" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("MonthId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="contactBirthDay" class="field-label" style="margin-left:0px">Day</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="contactBirthDay">
                                        <option value="" selected disabled>Day</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptDate" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("DateId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="contactBirthYear" class="field-label" style="margin-left:0px">Year</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="contactBirthYear">
                                        <option value="" selected disabled>Year</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptYear" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("YearId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="row form-horizontal-input-padded">
                        <div id="contactNameFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="contactFirstName" class="field-label" style="margin-left:0px">First Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactFirstName" placeholder="First Name">
                                </fieldset>
                            </div>

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <label for="contactMiddleName" class="field-label" style="margin-left:0px">Middle</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactMiddleName" placeholder="Middle">
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="contactLastName" class="field-label" style="margin-left:0px">Last Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactLastName" placeholder="Last Name">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="row form-horizontal-input-padded">
                        <div id="contactEmailConfirm">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactEmail" class="field-label" style="margin-left:0px">Email Address</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactEmail" placeholder="Email Address">
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactPhone" class="field-label" style="margin-left:0px">Phone Number</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPhone" placeholder="Phone Number">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <fieldset class="form-group rowElem">
                        <div class="input-group">
                            <label for="contactSSN" class="field-label" style="margin-left:0px">Social Security Number</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactSSN" placeholder="Social Security Number">
                            <div class="input-group-addon custom-popover" data-content="We protect the confidentiality of Social Security numbers, prohibit unlawful disclosure of Social Security Numbers, and limit access to Social Security Numbers. We limit the use to staff and process necessary for the purpose of facilitating a relationship or business transaction. You can  obtain more information by reviewing our privacy policy." data-placement="left" title="We Protect your Social Security Number" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>


                    <div class="row content-padded">
                        <h3>Employment Info</h3>
                    </div>

                    <fieldset class="form-group rowElem">
                        <label for="contactCurrentEmployer" class="field-label" style="margin-left:0px">Current Employer</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCurrentEmployer" placeholder="Current Employer">
                    </fieldset>
                    <fieldset class="form-group rowElem">
                        <label for="contactCurrentEmployerTelePhone" class="field-label" style="margin-left:0px">Employer Contact Number (Telephone)</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCurrentEmployerTelePhone" placeholder="Employer Contact Number (Telephone)">
                    </fieldset>
                    <fieldset class="form-group rowElem">
                        <label for="contactJobTitle" class="field-label" style="margin-left:0px">Job Title</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactJobTitle" placeholder="Job Title">
                    </fieldset>
                    <fieldset class="form-group rowElem">
                        <label for="contactMonthlyIncome" class="field-label" style="margin-left:0px">Gross Monthly Income</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactMonthlyIncome" placeholder="Gross Monthly Income">
                            <div class="input-group-addon custom-popover" data-content="Include your yearly income from salary, dividends, interest.  If retired include Social Security, Pension, Annuities, 401k etc. You only need to inlcude alimonry, child support, or maintenance income if you wish to have it considered as a basis for repayment.  Use whole dollars." data-placement="left" title="Total income before taxes" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <label for="contactEmployerDuration" class="control-label form-label-500">How long have you worked for this employer?</label>
                        <div id="contactEmployerDuration">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactEmployerDurationYear" class="field-label" style="margin-left:0px">Years</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactEmployerDurationYear" placeholder="">
                                        <span class="input-group-addon" id="contactEmployerDurationYearLabel">Years</span>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactEmployerDurationMonth" class="field-label" style="margin-left:0px">Months</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactEmployerDurationMonth" placeholder="">
                                        <span class="input-group-addon" id="contactEmployerDurationMonthLabel">Months</span>
                                    </div>
                                </fieldset>
                            </div>

                        </div>
                    </div>




                    <div id="priorEmployer" class="collapse">

                        <fieldset class="form-group rowElem">
                            <label for="contactPreviousEmployer" class="field-label" style="margin-left:0px">Previous Employer</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousEmployer" placeholder="Previous Employer">
                        </fieldset>
                        <div class="form-group form-line-spacer"></div>

                    </div>

                    <fieldset class="form-group rowElem">
                        <label for="contactAdditionalIncome" class="field-label" style="margin-left:0px">Additional Income Source</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactAdditionalIncome" placeholder="Additional Income Source">
                    </fieldset>

                    <fieldset class="form-group rowElem">
                        <label for="contactAdditionalAmount" class="field-label" style="margin-left:0px">Additional Income Amount</label>

                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactAdditionalAmount" placeholder="Additional Income Amount">
                        </div>
                    </fieldset>

                </div>
            </div>
        </div>
        <div class="container row" id="joinApplicantCtr">
            <div class="col-md-12 col-xs-12">

                <div class="row content-padded">
                    <div class="row content-padded">
                        <h1>Joint Contact Info</h1>
                    </div>


                    <fieldset class="form-group rowElem">
                        <label for="coContactStreetAddress" class="field-label" style="margin-left:0px">Street Address</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactStreetAddress" placeholder="Current Address">
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <div id="coContactAddressFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactCity" class="field-label" style="margin-left:0px">City</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactCity" placeholder="City">
                                </fieldset>
                            </div>

                            <div class="col-md-3">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactState" class="field-label" style="margin-left:0px">State</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="coContactState" placeholder="State">
                                        <asp:Repeater ID="rptState3" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-md-4">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactZip" class="field-label" style="margin-left:0px">Zip Code</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactZip" placeholder="Zip Code">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="form-group form-line-spacer"></div>

                    <fieldset class="form-group rowElem">
                        <label for="coContactResidenceStatus" class="field-label" style="margin-left:0px">Residence Status</label>
                        <select class="form-control" data-val="true" data-val-required="true" id="coContactResidenceStatus" placeholder="Residence Status">
                            <option value="" selected disabled>Residence Status</option>
                            <option value="" disabled></option>
                            <asp:Repeater ID="rptResidenceStatus2" runat="server">
                                <ItemTemplate>
                                    <option value="<%#Eval("ResidenceStatusId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </fieldset>


                    <fieldset class="form-group rowElem">
                        <label for="coContactMonthlyHousing" class="field-label" style="margin-left:0px">Monthly Mortgage or Rent</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" id="coContactMonthlyHousing" data-val="true" data-val-required="true" placeholder="Monthly Mortgage or Rent">
                            <div class="input-group-addon custom-popover" data-content="Include the total paid for all mortgages, second mortgages, second homes in whole dollars." rel="popover" data-placement="left" data-original-title="What to include:" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>


                    <div class="row form-horizontal-input-padded">
                        <label for="coContactResidence" class="control-label form-label-500">How long have you lived at your current address?</label>
                        <div id="coContactResidence">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactResidenceYears" class="field-label" style="margin-left:0px">Years at this address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactResidenceYears" placeholder="">
                                        <span class="input-group-addon" id="coContactResidenceYearsAddon">Years</span>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactResidenceMonths" class="field-label" style="margin-left:0px">Months at this address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactResidenceMonths" placeholder="">
                                        <span class="input-group-addon" id="coContactResidenceMonthsAddon">Months</span>
                                    </div>
                                </fieldset>
                            </div>

                        </div>

                    </div>

                    <div id="priorResidence">
                        <div class="form-group form-line-spacer"></div>

                        <fieldset class="form-group rowElem">
                            <label for="coContactPreviousStreetAddress" class="field-label" style="margin-left:0px">Previous Address</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousStreetAddress" placeholder="Previous Address">
                        </fieldset>

                        <div class="row form-horizontal-input-padded">
                            <div id="coContactAddressFull">

                                <div class="col-md-5">
                                    <fieldset class="form-group rowElem">
                                        <label for="coContactPreviousCity" class="field-label" style="margin-left:0px">Previous City</label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousCity" placeholder="Previous City">
                                    </fieldset>
                                </div>

                                <div class="col-md-3">
                                    <fieldset class="form-group rowElem">
                                        <label for="coContactPreviousState" class="field-label" style="margin-left:0px">Previous State</label>
                                        <select class="form-control" data-val="true" data-val-required="true" id="coContactPreviousState">
                                            <asp:Repeater ID="rptState4" runat="server">
                                                <ItemTemplate>
                                                    <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <fieldset class="form-group rowElem">
                                        <label for="coContactPreviousZip" class="field-label" style="margin-left:0px">Previous Zip Code</label>
                                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousZip" placeholder="Previous Zip Code">
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>



                    <div class="row content-padded">
                        <h3>Joint Applicant Info</h3>
                    </div>


                    <div class="row form-horizontal-input-padded">
                        <label for="coContactDOB" class="control-label form-label-500">Date of Birth</label>
                        <div id="coContactDOB">

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactBirthMonth" class="field-label" style="margin-left:0px">Month</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthMonth">
                                        <option value="" selected disabled>Month</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptMonth2" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("MonthId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactBirthDay" class="field-label" style="margin-left:0px">Day</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthDay">
                                        <option value="" selected disabled>Day</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptDate2" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("DateId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-sm-4">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactBirthYear" class="field-label" style="margin-left:0px">Year</label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="coContactBirthYear">
                                        <option value="" selected disabled>Year</option>
                                        <option value="" disabled></option>
                                        <asp:Repeater ID="rptYear2" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("YearId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="row form-horizontal-input-padded">
                        <div id="coContactNameFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactFirstName" class="field-label" style="margin-left:0px">First Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactFirstName" placeholder="First Name">
                                </fieldset>
                            </div>

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactMiddleName" class="field-label" style="margin-left:0px">Middle</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactMiddleName" placeholder="Middle">
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactLastName" class="field-label" style="margin-left:0px">Last Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactLastName" placeholder="Last Name">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div class="row form-horizontal-input-padded">
                        <div id="coContactEmailConfirm">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactEmail" class="field-label" style="margin-left:0px">Email Address</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactEmail" placeholder="Email Address">
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="coContactPhone" class="field-label" style="margin-left:0px">Phone Number</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPhone" placeholder="Phone Number">
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <fieldset class="form-group rowElem">
                        <div class="input-group">
                            <label for="coContactSSN" class="field-label" style="margin-left:0px">Social Security Number</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactSSN" placeholder="Social Security Number">
                            <div class="input-group-addon custom-popover" data-content="We protect the confidentiality of Social Security numbers, prohibit unlawful disclosure of Social Security Numbers, and limit access to Social Security Numbers. We limit the use to staff and process necessary for the purpose of facilitating a relationship or business transaction. You can  obtain more information by reviewing our privacy policy." data-placement="left" title="We Protect your Social Security Number" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>


                    <div class="row content-padded">
                        <h3>Joint Employment Info</h3>
                    </div>

                    <fieldset class="form-group rowElem">
                        <label for="coContactCurrentEmployer" class="field-label" style="margin-left:0px">Current Employer</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactCurrentEmployer" placeholder="Current Employer">
                    </fieldset>

                    <fieldset class="form-group rowElem">
                        <label for="coContactMonthlyIncome" class="field-label" style="margin-left:0px">Gross Monthly Income</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactMonthlyIncome" placeholder="Gross Monthly Income">
                            <div class="input-group-addon custom-popover" data-content="Include your yearly income from salary, dividends, interest.  If retired include Social Security, Pension, Annuities, 401k etc. You only need to inlcude alimonry, child support, or maintenance income if you wish to have it considered as a basis for repayment.  Use whole dollars." data-placement="left" title="What to include:" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                        </div>
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <label for="coContactEmployerDuration" class="control-label form-label-500">How long have you worked for this employer?</label>
                        <div id="coContactEmployerDuration">

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactResidenceYears" class="field-label" style="margin-left:0px">Years</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" id="coContactEmployerDurationYear" placeholder="">
                                        <span class="input-group-addon" id="coContactEmployerDurationYearLabel">Years</span>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="contactResidenceMonths" class="field-label" style="margin-left:0px">Months</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" id="coContactEmployerDurationMonth" placeholder="">
                                        <span class="input-group-addon" id="coContactEmployerDurationMonthLabel">Months</span>
                                    </div>
                                </fieldset>
                            </div>

                        </div>
                    </div>

                    <div id="coPriorEmployer" class="collapse">

                        <fieldset class="form-group rowElem">
                            <label for="coContactPreviousEmployer" class="field-label" style="margin-left:0px">Previous Employer</label>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactPreviousEmployer" placeholder="Previous Employer">
                        </fieldset>
                        <div class="form-group form-line-spacer"></div>

                    </div>

                    <fieldset class="form-group rowElem">
                        <label for="coContactAdditionalIncome" class="field-label" style="margin-left:0px">Additional Income Source</label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactAdditionalIncome" placeholder="Additional Income Source">
                    </fieldset>

                    <fieldset class="form-group rowElem">
                        <label for="coContactAdditionalAmount" class="field-label" style="margin-left:0px">Additional Income Amount</label>

                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control" data-val="true" data-val-required="true" id="coContactAdditionalAmount" placeholder="Additional Income Amount">
                        </div>
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
    <style>
        .field-label {
          
            font-weight:500;
        }
    </style>
</asp:Content>
