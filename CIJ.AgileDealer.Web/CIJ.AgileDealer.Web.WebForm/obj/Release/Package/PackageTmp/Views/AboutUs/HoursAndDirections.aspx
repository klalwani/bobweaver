﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="HoursAndDirections.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.AboutUs.HoursAndDirections" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.AboutUs" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<style>#H-D {padding: 15px 30px; background: #fff;}</style>
<div style="clear: both; height: 20px;"></div>
<div class="container mdl-box-shadow" id="H-D">
  <div class="row">
  <div class="col-md-7 col-sm-6 col-xs-12">
      <%=string.Format(HoursAndDirections.Col_Xs_12,DealerName,DealerCity,DealerState,DealerName, DealerCity,DealerCity)%>
  <%--<h1>Hours and Directions to <%= DealerName %> in <%= DealerCity %>, <%= DealerState %></h1>
  <p><strong><%= DealerName %></strong> is conveniently located 1 mile east of Interstate 75 on Interstate 4 at exit 10 between Camping World RV at McIntosh Road and  LazyDays RV Super Center at County Road 579. We are the same exit as Bob Evans and Cracker Barrel restaurants and for fast food there is an Arby's and Popeyes located in the TA Travel Center which offers some of the lowest gas and diesel fuel prices in <%= DealerCity %>.</p>
  <p>The Flying J Travel Centeris directly across County Road 579 adjacent to the Country Inn &amp; Suites. If you are coming from out of town we are next door the Hampton Inn Suites hotel located on <%= DealerCity %> Gateway Blvd.</p>--%>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12"><img style="width: 100%;" src='http://cdnedge.vinsolutions.com/dealerimages/Dealer%205748%20Images/ServiceBaysComm&Rv400wide.jpg'></div>
  <div style="clear: both; height: 20px;"></div>
</div>



<div class="row">
        <div class="col-md-12">
            <%=string.Format(HoursAndDirections.Col_Md_12,DealerCity,DealerState,DealerState,DealerName,DealerCity,DealerCity,DealerCity,DealerCity,DealerName) %>
                <%--<p>If you are coming from out of town there are several world class attractions within driving distance of our dealership. Locally here in <%= DealerCity %> we are 5 miles to the east of the Seminole Hard Rock Hotel &amp; Casinowhich is adjacent to the  <%= DealerState %> State Fairgrounds home of the <%= DealerState %> State Fair. We are 10 miles southeast of Busch Gardenstheme park and Adventure Island water park.</p>
                <p>As you drive towards Orlando we are a 35 mile drive to the Fantasy of Flightattraction, 61 miles to Walt Disney World, 64 miles to SeaWorld the world class marine attraction and 68 miles from Universal Orlando Resort. The newest attraction in our area is Legolandl ocated 42 miles from <%= DealerName %>.</p>
                <p>The <%= DealerCity %> area is also home to the <%= DealerCity %> Buccaneers football team of the National Football League, the <%= DealerCity %> Bay Lightning of the National Hockey League and the <%= DealerCity %> Rays professional baseball team.</p>
                <p>If you are planning to fly in and would like to rent a motorhome during your visit you can check out our friends at Suncoast RV Rental and Management located just a few miles from <%= DealerName %>.</p>--%>
          <a href="/Contactus" class="btn btn-primary" role="button"><%=HoursAndDirections.Btn_Primary %></a>
        </div>
</div>
</div>

</asp:Content>
