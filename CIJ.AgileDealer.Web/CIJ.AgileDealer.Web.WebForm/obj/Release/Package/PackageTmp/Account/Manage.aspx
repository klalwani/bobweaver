﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Account.Manage" %>

<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <% if (User.IsInRole(RoleEnum.Administrator) || User.IsInRole(RoleEnum.Dealer))
        { %>
    <div class="row container ">
        <h2>Manage Dashboards & Settings</h2>
        <% if (User.IsInRole(RoleEnum.Administrator))
            { %>
        <div class="dropdown">
            <ul class="mdl-white-box nav navbar-nav">
                <li id="navBar_Dashboard" class="dropdown">
                    <a  class="dropdown-toggle active">Dashboard Menu<span class="caret"></span></a>
                    <ul id="navBar_Dashboard" class="dropdown-menu">

                        <li><a href="/Admin/LeadDashboard" name="Lead Dashboard">Lead Dashboard</a></li>
                        <li><a href="/admin/jobmanagement" name="Job Management Dashboard">Inventory Management Dashboard</a></li>
                         <li><a href="/admin/incentivemanagement" name="Incentive Management Dashboard">Incentive Management Dashboard</a></li>
                         <li><a href="/Admin/vehicleoptionsdashboard" name="Vehicle Options Management Dashboard">Vehicle Options Management Dashboard</a></li>
                        <li><a href="/Admin/UserAccount" name="Lead Dashboard">User Account Dashboard</a></li>
                        <li><a href="/Admin/Staff" name="Lead Dashboard">Staff Dashboard</a></li>
                        <li><a href="/Admin/VehicleSegment" name="Lead Dashboard">Vehicle Segment Dashboard</a></li>
                          <li><a href="/Admin/Vehiclefeaturemanagement" name="Vehicle Feature Dashboard">Vehicle Feature Dashboard</a></li>
                        <%--<li><a href="/Admin/Dashboard" name="Lead Dashboard">Admin Dashboard</a></li>--%>
                        <li><a href="/Admin/BlogManagement" name="Lead Dashboard">Blog Management Dashboard</a></li>
                        <li><a href="/Admin/SlideManagement" name="Slide Dashboard">Slide Management Dashboard</a></li>
                         <li><a href="/Admin/bulkupdatevehicledatadashboard" name="Vehicle Bulk Update Dashboard">Vehicle Bulk Update Dashboard</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class= "dropdown col-sm-offset-2 col-lg-offset-2 col-xs-offset-2">
            <ul class="mdl-white-box nav navbar-nav">
                <li id="navBar_Setting" class="dropdown">
                    <a  class="dropdown-toggle active">Setting Menu<span class="caret"></span></a>
                    <ul id="navBar_Setting" class="dropdown-menu">

                        <li><a href="/Admin/CustomRedirectInfo" name="Lead Dashboard">Edit Custom Redirect Info</a></li>
                     <%--   <li><a href="/Admin/EditDealerInfoPage" name="Lead Dashboard">Edit Dealer Info</a></li>--%>
                        <li><a href="/Admin/NotificationSetting" name="Lead Dashboard">Edit Email Notification Settings</a></li>
                       <%-- <li><a href="/localizationAdmin/index">Edit Site Language</a></li>--%>
                        <li> <a href="/Admin/ConfigContent">Config CMS</a></li>
                    </ul>
                </li>
            </ul>
        </div>



        <% } %>

        <% if (User.IsInRole(RoleEnum.Dealer))
            { %>
        <a href="/Admin/FinanceApplicant" class="btn btn-primary" id="btnApplicant">Finance Applicants</a>
        <% } %>
    </div>
    <% } %>
    <div class="row container">
        <h2><%: Title %>.</h2>

        <div>
            <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
                <p class="text-success"><%: SuccessMessage %></p>
            </asp:PlaceHolder>
        </div>

        <div class="row">
            <div class="col-md-12">
                <section id="passwordForm">
                    <asp:PlaceHolder runat="server" ID="setPassword" Visible="false">
                        <p>
                            You do not have a local password for this site. Add a local
                        password so you can log in without an external login.
                        </p>
                        <div class="form-horizontal">
                            <h4>Set Password Form</h4>
                            <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                            <hr />
                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="password" CssClass="col-md-2 control-label">Password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="password" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="password"
                                        CssClass="text-danger" ErrorMessage="The password field is required."
                                        Display="Dynamic" ValidationGroup="SetPassword" />
                                    <asp:ModelErrorMessage runat="server" ModelStateKey="NewPassword" AssociatedControlID="password"
                                        CssClass="text-error" SetFocusOnError="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" AssociatedControlID="confirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="confirmPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="confirmPassword"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required."
                                        ValidationGroup="SetPassword" />
                                    <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="confirmPassword"
                                        CssClass="text-error" Display="Dynamic" ErrorMessage="The password and confirmation password do not match."
                                        ValidationGroup="SetPassword" />

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" Text="Set Password" ValidationGroup="SetPassword" OnClick="SetPassword_Click" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder runat="server" ID="changePasswordHolder" Visible="false">
                        <p>You're logged in as <strong><%: User.Identity.GetUserName() %></strong>.</p>
                        <div class="form-horizontal">
                            <h4>Change Password Form</h4>
                            <hr />
                            <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                            <div class="form-group">
                                <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword" CssClass="col-md-2 control-label">Current password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="CurrentPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CurrentPassword"
                                        CssClass="text-danger" ErrorMessage="The current password field is required."
                                        ValidationGroup="ChangePassword" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword" CssClass="col-md-2 control-label">New password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPassword"
                                        CssClass="text-danger" ErrorMessage="The new password is required."
                                        ValidationGroup="ChangePassword" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword" CssClass="col-md-2 control-label">Confirm new password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmNewPassword"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="Confirm new password is required."
                                        ValidationGroup="ChangePassword" />
                                    <asp:CompareValidator runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The new password and confirmation password do not match."
                                        ValidationGroup="ChangePassword" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" Text="Change Password" ValidationGroup="ChangePassword" OnClick="ChangePassword_Click" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </section>

                <%-- <section id="externalLoginsForm">

                    <asp:ListView runat="server"
                        ItemType="Microsoft.AspNet.Identity.UserLoginInfo"
                        SelectMethod="GetLogins" DeleteMethod="RemoveLogin" DataKeyNames="LoginProvider,ProviderKey">

                        <LayoutTemplate>
                            <h4>Registered Logins</h4>
                            <table class="table">
                                <tbody>
                                    <tr runat="server" id="itemPlaceholder"></tr>
                                </tbody>
                            </table>

                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#: Item.LoginProvider %></td>
                                <td>
                                    <asp:Button runat="server" Text="Remove" CommandName="Delete" CausesValidation="false"
                                        ToolTip='<%# "Remove this " + Item.LoginProvider + " login from your account" %>'
                                        Visible="<%# CanRemoveExternalLogins %>" CssClass="btn btn-default" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>

                    <uc:OpenAuthProviders runat="server" ReturnUrl="~/Account/Manage" />
                </section>--%>
            </div>
        </div>
    </div>
</asp:Content>
