﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" MasterPageFile="~/Blog_CMS.Master" CodeBehind="CMSPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.CMS.CMSPage" %>

<%@ Register Src="~/Views/UserControl/SearchControl/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchUserControl" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!--<div class="container">-->
        <asp:Panel ID="pnlBodyTopContent" runat="server">
        </asp:Panel>

        <div class="tab-pane active" id="Inventory">
            <searchUserControl:SearchControl runat="server" ID="SearchControl" />
        </div>
         <asp:Panel ID="pnlBodyBottomContent" runat="server">
        </asp:Panel>
    <!--</div>-->  <div class="clear-25"></div>

</asp:Content>
