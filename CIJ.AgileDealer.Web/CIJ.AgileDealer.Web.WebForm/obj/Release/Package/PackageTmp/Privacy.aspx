﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Privacy.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Privacy" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1><%=Privacy.Col_Md_12 %></h1>
            </div>
        </div>
    </div>
    <div id="privacy-disclaimer-page">
        <div class="body-content-wrapper">
            <div class="body-content-card">
                <div class="container-fluid padded-15">
                    <div class="row padded-15">
                        <%=Privacy.Row_Padded_15 %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-20"></div>
    </div>
</asp:Content>
