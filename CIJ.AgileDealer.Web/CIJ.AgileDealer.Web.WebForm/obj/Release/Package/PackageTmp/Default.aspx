﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm._Default" %>

<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>
<%@ Register Src="~/Views/UserControl/Home/CustomerReviews.ascx" TagName="CustomerReviews" TagPrefix="CustomerReviewsControl" %>
<%@ Register Src="~/Views/UserControl/FatFooter/FatFooterControl.ascx" TagName="FatFooter" TagPrefix="FatFooterControl" %>
<asp:Content ID="cphSearch" ContentPlaceHolderID="cphSearchContent" runat="server">


    <!-- Search Box -->

    <h1>Bob Weaver Auto helps you<span>find your dream car</span></h1>
    <div class="searchvicle">
        <input type="text" id="txtSearchHome" placeholder="<%=Default.Main_Search_Input %>" autocomplete="off" class="form-control">
        <button class="btnAccess" id="mainSearchButton">&nbsp;</button>
    </div>


    <% if (User.IsInRole(RoleEnum.Administrator) || User.IsInRole(RoleEnum.Dealer))
        { %>
    <a href="/Admin/SlideManagement" class="btn btn-danger" style="float: right; margin-right: 50px;" id="btnEditHomePageSlide" title="Edit Home Page Slides">Edit Home Page Slides</a>
    <% } %>

    <!-- End Search Box -->
</asp:Content>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">




    <section class="shopMain">
        <div class="container-fluid">
            <div class="shopby">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#make" aria-controls="make" role="tab" title="Shop by Make" data-toggle="tab">Shop by Make <i class="fa fa-angle-right"></i></a></li>
                    <li role="presentation"><a href="#home" aria-controls="home" role="tab" title="Shop by Body Style" data-toggle="tab">Shop by Body Style <i class="fa fa-angle-right"></i></a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" title="Shop by Price">Shop by Price <i class="fa fa-angle-right"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="make">
                        <div class="container-fluid">
                            <!-- experimental slider -->
                            <div class="clear"></div>
                            <div class="vcp-carousel-wrapper mdl-box-shadow">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="vcp-carousel">

                                              <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/renegade">
                                                    <img class="vcp-selector" alt="New Jeep Renegade" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-renegade.jpg">
                                                    <h4 class="vcp-label">Renegade</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/cherokee">
                                                    <img class="vcp-selector" alt="New Jeep Cherokee" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-cherokee.jpg">
                                                    <h4 class="vcp-label">Cherokee</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/grand-cherokee">
                                                    <img class="vcp-selector" alt="New Jeep Grand Cherokee" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-grand-cherokee.jpg">
                                                    <h4 class="vcp-label">Grand Cherokee</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/compass">
                                                    <img class="vcp-selector" alt="New Jeep Compass" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-compass.jpg">
                                                    <h4 class="vcp-label">Compass</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/wrangler-unlimited">
                                                    <img class="vcp-selector" alt="New Jeep Wrangler Unlimited" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-wrangler-unlimited.jpg">
                                                    <h4 class="vcp-label">Wrangler Unlimited</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-jeep">
                                                <a href="/vcp/new/jeep/wrangler-jk-unlimited">
                                                    <img class="vcp-selector" alt="New Jeep Wrangler JK Unlimited" src="https://storage.googleapis.com/agiledealer/vcp-slider/jeep/new-jeep-wrangler-jk-unlimited.jpg">
                                                    <h4 class="vcp-label">Wrangler JK Unlimited</h4>
                                                </a>
                                            </div>
                                             <div class="vcp-model filter-ram">
                                                <a href="/vcp/new/ram/1500">
                                                    <img class="vcp-selector lazy" alt="New Ram 1500" src="https://storage.googleapis.com/agiledealer/vcp-slider/ram/new-ram-1500.jpg">
                                                    <h4 class="vcp-label">1500</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-ram">
                                                <a href="/vcp/new/ram/2500">
                                                    <img class="vcp-selector lazy" alt="New Ram 2500" src="https://storage.googleapis.com/agiledealer/vcp-slider/ram/new-ram-2500.jpg">
                                                    <h4 class="vcp-label">2500</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-ram">
                                                <a href="/vcp/new/dodge/journey">
                                                    <img class="vcp-selector lazy" alt="New Dodge Journey" src="https://storage.googleapis.com/agiledealer/vcp-slider/dodge/new-dodge-journey.jpg">
                                                    <h4 class="vcp-label">Journey</h4>
                                                </a>
                                            </div>

                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/silverado-1500">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Silverado" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-silverado-1500.jpg">
                                                    <h4 class="vcp-label">Silverado 1500</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/cruze">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Cruze" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-cruze.jpg">
                                                    <h4 class="vcp-label">Cruze</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/trax">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Trax" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-trax.jpg">
                                                    <h4 class="vcp-label">Trax</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/equinox">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Equinox" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-equinox.jpg">
                                                    <h4 class="vcp-label">Equinox</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/malibu">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Malibu" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-malibu.jpg">
                                                    <h4 class="vcp-label">Malibu</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/traverse">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Traverse" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-traverse.jpg">
                                                    <h4 class="vcp-label">Traverse</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/corvette">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Corvette" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-corvette.jpg">
                                                    <h4 class="vcp-label">Corvette</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/colorado">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Colorado" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-colorado.jpg">
                                                    <h4 class="vcp-label">Colorado</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/silverado-1500-ld">
                                                    <img class="vcp-selector" alt="New Chevrolet Silverado 1500 LD" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-silverado-1500-ld.jpg">
                                                    <h4 class="vcp-label">Silverado 1500 LD</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/suburban">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Suburban" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-suburban.jpg">
                                                    <h4 class="vcp-label">Suburban</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/sonic">
                                                    <img class="vcp-selector lazy" alt="New Chevrolet Sonic" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-sonic.jpg">
                                                    <h4 class="vcp-label">Sonic</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-chevrolet">
                                                <a href="/vcp/new/chevrolet/tahoe">
                                                    <img class="vcp-selector" alt="New Chevrolet Tahoe" src="https://storage.googleapis.com/agiledealer/vcp-slider/chevrolet/new-chevrolet-tahoe.jpg">
                                                    <h4 class="vcp-label">Tahoe</h4>
                                                </a>
                                            </div>



                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/canyon">
                                                    <img class="vcp-selector lazy" alt="New GMC Canyon" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-canyon.jpg">
                                                    <h4 class="vcp-label">Canyon</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/acadia">
                                                    <img class="vcp-selector lazy" alt="New GMC Arcadia" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-acadia.jpg">
                                                    <h4 class="vcp-label">Acadia</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/sierra-1500-limited">
                                                    <img class="vcp-selector lazy" alt="New GMC Sierra 1500 Limited" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-sierra-1500-limited.jpg">
                                                    <h4 class="vcp-label">Sierra 1500 Limited</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/terrain">
                                                    <img class="vcp-selector lazy" alt="New GMC Terrain" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-terrain.jpg">
                                                    <h4 class="vcp-label">Terrain</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/sierra-1500">
                                                    <img class="vcp-selector lazy" alt="New GMC Sierra 1500" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-sierra-1500.jpg">
                                                    <h4 class="vcp-label">Sierra 1500</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/sierra-2500hd">
                                                    <img class="vcp-selector lazy" alt="New GMC Sierra 2500HD" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-sierra-2500hd.jpg">
                                                    <h4 class="vcp-label">Sierra 2500HD</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-gmc">
                                                <a href="/vcp/new/gmc/yukon-xl">
                                                    <img class="vcp-selector lazy" alt="New GMC Yukon XL" src="https://storage.googleapis.com/agiledealer/vcp-slider/gmc/new-gmc-yukon-xl.jpg">
                                                    <h4 class="vcp-label">Yukon XL</h4>
                                                </a>
                                            </div>




                                          


                                            <div class="vcp-model filter-buick">
                                                <a href="/vcp/new/buick/encore">
                                                    <img class="vcp-selector lazy" alt="New Buick Encore" src="https://storage.googleapis.com/agiledealer/vcp-slider/buick/new-buick-encore.jpg">
                                                    <h4 class="vcp-label">Encore</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-buick">
                                                <a href="/vcp/new/buick/envision">
                                                    <img class="vcp-selector lazy" alt="New Buick Envision" src="https://storage.googleapis.com/agiledealer/vcp-slider/buick/new-buick-envision.jpg">
                                                    <h4 class="vcp-label">Envision</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-buick">
                                                <a href="/vcp/new/buick/cascada">
                                                    <img class="vcp-selector lazy" alt="New Buick Cascada" src="https://storage.googleapis.com/agiledealer/vcp-slider/buick/new-buick-cascada.jpg">
                                                    <h4 class="vcp-label">Cascada</h4>
                                                </a>
                                            </div>
                                            <div class="vcp-model filter-buick">
                                                <a href="/vcp/new/buick/regal-tourx">
                                                    <img class="vcp-selector lazy" alt="New Buick Regal Tourx" src="https://storage.googleapis.com/agiledealer/vcp-slider/buick/new-buick-regal-tourx.jpg">
                                                    <h4 class="vcp-label">Regal TourX</h4>
                                                </a>
                                            </div>


                                           
                                            <div class="clear"></div>
                                        </div>

                                        <div class="clear"></div>
                                        <div class="vehicleType-wrapper">
                                            <ul class="vehicleType">
                                                <li id="jeep">
                                                    <span class="btn btn-primary vcp-button">Jeep</span>
                                                </li>
                                                <li id="ram">
                                                    <span class="btn btn-default vcp-button">Ram / Dodge</span>
                                                </li>
                                                <li id="chevrolet">
                                                    <span class="btn btn-default  vcp-button">Chevrolet</span>
                                                </li>
                                                <li id="gmc">
                                                    <span class="btn btn-default vcp-button">GMC</span>
                                                </li>
                                                <li id="buick">
                                                    <span class="btn btn-default vcp-button">Buick</span>
                                                </li>
                                                 <li id="journey">
                                                    <span class="btn btn-default vcp-button">Journey</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end experimental slider -->
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="home">
                        <div class="container">
                            <div class="relstSrch">
                                <ul class="list-swcar">
                                    <li><a href="/car-department/used/car" title="Browse Sedan Bodystle">
                                        <div class="swstyle">
                                            <img title="Sedan" src="https://agiledealer.blob.core.windows.net/images/homepage/Sedan.png" alt="Sedan">
                                            <p>Sedan</p>
                                        </div>
                                    </a></li>
                                    <li><a href="/suv-department/used-certified/crossover–suv" title="Browse SUV Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Suv.png" alt="Suv" title="Suv">
                                            <p>Suv</p>
                                        </div>
                                    </a></li>
                                    <li><a href="/vehicle-department/used-certified/truck" title="Browse Pickup Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Pickup.png" alt="Pickup" title="Pickup">
                                            <p>Pickup</p>
                                        </div>
                                    </a></li>
                                    <li><a href="/vehicle-department/used-certified/van" title="Browse Minivan Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Minivan.png" alt="Minivan" title="Minivan">
                                            <p>Minivan</p>
                                        </div>
                                    </a></li>
                                    <li><a href="/inventory?keywords=hatchback+used" title="Browse Hatchback/Wagon Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Hatchback.png" alt="Hatchback/Wagon" title="Hatchback/Wagon">
                                            <p>Hatchback/ <span>Wagon </span></p>
                                        </div>
                                    </a></li>
                                    <li><a href="/inventory?keywords=convertible+used" title="Browse Convertible Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Convertible.png" alt="Convertible" title="Convertible">
                                            <p>Convertible</p>
                                        </div>
                                    </a></li>
                                    <li><a href="/car-department/used-certified/car" title="Browse Coupe Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Coupe.png" alt="Coupe" title="Coupe">
                                            <p>Coupe</p>
                                        </div>
                                    </a></li>
                                    <%--<li><a href="/inventory?keywords=station+used" title="Browse Hybrid Bodystle">
                                        <div class="swstyle">
                                            <img src="https://agiledealer.blob.core.windows.net/images/homepage/Hybrid.png" alt="Hybrid" title="Hybrid">
                                            <p>Hybrid</p>
                                        </div>
                                    </a></li>--%>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--End one tab contant-->
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="relstSrch">
                            <div class="container">
                                <div class="relstSrch">
                                    <ul class="list-swcar">
                                        <li><a href="/price-department/used-certified/0-10000?sort=3" title="Browse prices under  $10k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/10k.jpg" class="img-responsive" alt="Browse prices under  $10k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/10000-15000" title="Browse prices between $10k - $15k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/10-15.jpg" class="img-responsive" alt="Browse prices between $10k - $15k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/15000-20000" title="Browse prices between $15k - $20k">
                                            <div class="swstyle">

                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/15-20.jpg" class="img-responsive" alt="Browse prices between $15k - $20k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/20000-25000" title="Browse prices between $20k - $25k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/20-25.jpg" class="img-responsive" alt="Browse prices between $20k - $25k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/25000-30000" title="Browse prices between $25k - $30k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/25-30.jpg" class="img-responsive" alt="Browse prices between $25k - $30k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/30000-35000" title="Browse prices between $30k - $35k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/30-35.jpg" class="img-responsive" alt="Browse prices between $30k - $35k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/35000-40000" title="Browse prices between $35k - $40k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/35-40.jpg" class="img-responsive" alt="Browse prices between $35k - $40k">
                                            </div>
                                        </a></li>
                                        <li><a href="/price-department/used-certified/40000-2147483647" title="Browse prices between $35k - $40k">
                                            <div class="swstyle">
                                                <img src="https://agiledealer.blob.core.windows.net/images/prices/40k.jpg" class="img-responsive" alt="Browse prices over $40k">
                                            </div>
                                        </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End one 2 contant-->

                </div>
            </div>
        </div>
    </section>
    <section class="dealerships">
        <div class="container">
            <div class="row">
                <div class="titlesame">
                    <h2>Buick, Chevrolet, Chrysler, Dodge, GMC, Jeep, RAM Dealership in Pottsville, PA</h2>
                    <p>Welcome to Bob Weaver Auto, your source for all things Chevrolet, Jeep, Dodge, RAM and GMC in the Pottsville, PA, area. Our state-of-the-art facility houses a huge selection of new, used and certified pre-owned Chevrolet, Jeep, Dodge, RAM and GMC models. Whether it’s your first time shopping with us or you have been a customer for a number of years, you can always expect to be treated like a VIP at Bob Weaver Auto.</p>
                </div>
                <div class="col-sm-8">
                    <div class="sowpe-about">
                        <p>At Bob Weaver Auto we want the car shopping process to be fun, easy and personal. That’s why we offer great financing options and a variety of ways to shop. You can browse our complete inventory online or stop by and see it in person. Let us help you find your dream car today! You can always expect the best selection, lowest prices and exceptional customer service when you shop at Bob Weaver Auto. In fact, it’s our personal approach and world-class customer service experience that continues to bring our customers back for their second, third and fourth vehicle.</p>
                        <p>At the end of the day, Bob Weaver Auto is a family owned operation. We have proudly served drivers from across the state for over 35 years. Since we first opened our doors our No.1 priority has always been to serve the customer. Whether you visit us for service, financing, to shop or maybe to simply ask a few questions, you can expect consistent excellence from our team.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="videoMain">
                        <div class="youtube-player" data-id="vavGE0wZrXc"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <p>Need genuine Chevrolet, Jeep, Dodge, RAM and GMC repair and maintenance? The experts in our state-of-the-art service department are factory trained and certified and offer a wide variety of services. Whether your vehicle is due for an oil change or some complicated repair, there is no project too big or too small for our team.</p>
                </div>
                <div class="col-sm-12">
                    <p>We invite you to browse our extensive inventory online or stop by and see us today! We are conveniently located in Pottsville, PA, and proudly serve drivers from all over the state, including Schuylkill Haven, Tremont, Mt. Carmel, Reading and Harrisburg, PA.</p>
                    <div class="readbtn"><a href="/content/about-us" class="btnRead" title="Read more about us">Read more about us</a> </div>
                </div>

                <%--<div class="col-sm-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3025.913393139031!2d-76.22706288445046!3d40.67587717933575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c5ee8a443cfca5%3A0x6beef0da1388b971!2sBob+Weaver+Auto!5e0!3m2!1sen!2sus!4v1534883315875" width="100%" height="450" frameborder="0" style="border:0; margin-top: 5rem;" allowfullscreen></iframe>
                </div>--%>
            </div>
        </div>
    </section>

    <%-- <section class="ourBrand">
        <div class="container">
            <div class="sliderbrand">
                <div class="owl-carousel">
                    <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.bobswopeford.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Bob Swope Ford Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-ford.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.swopehyundai.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Hyundai Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-hyundai.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.swopemitsubishi.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Mitsubishi Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-mitsubishi.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.swopenissan.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Nissan Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-nissan.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                           <a href="https://www.swopetoyota.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Toyota Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-toyota.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.swopechryslerdodgejeep.com/?utm_source=Swope&utm_medium=Referral&utm_campaign=Chrysler" target="_blank"><img class="img-responsive" alt="Swope Chrysler Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-chrysler.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                           <a href="https://www.swopechryslerdodgejeep.com/?utm_source=Swope&utm_medium=Referral&utm_campaign=Dodge" target="_blank"><img class="img-responsive" alt="Swope Dodge Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-dodge.jpg" /></a>
                        </div>
                    </div>
                     <div class="item">
                        <div class="logobrnd">
                            <a href="https://www.swopechryslerdodgejeep.com/?utm_source=Swope&utm_medium=Referral&utm_campaign=Jeep" target="_blank"><img class="img-responsive" alt="Swope Jeep Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-jeep.jpg" /></a>
                        </div>
                    </div>
                    
                     <div class="item">
                        <div class="logobrnd">
                           <a href="https://www.swopechryslerdodgejeep.com/?utm_source=Swope&utm_medium=Referral&utm_campaign=Ram" target="_blank"><img class="img-responsive" alt="Swope Ram Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-ram.jpg" /></a>
                        </div>
                    </div>
                     <div class="item">
                        <div class="logobrnd">
                           <a href="http://www.swopemuseum.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope's Cars of Yesteryear Museum" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-museum.jpg" /></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="logobrnd">
                          <a href="http://www.swopefreedomplan.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Freedom Plan Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-freedom-plan.jpg" /></a>
                        </div>
                    </div>
                     <div class="item">
                        <div class="logobrnd">
                          <a href="https://www.swopecertified.com/?utm_source=Swope&utm_medium=Referral" target="_blank"><img class="img-responsive" alt="Swope Freedom Plan Location" src="https://agiledealer.blob.core.windows.net/images/dealerships/swope-certified.jpg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="guaranteed">
        <div class="guaranteed-logo">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="bloksingle">
                            <div class="grntlogo">
                                <img src="https://agiledealer.blob.core.windows.net/images/homepage/guaranteed-logo.png" alt="">
                            </div>
                            <div class="textSingle">
                                <h2>Bob Weaver Auto Satisfaction Guaranteed!</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="maintenance-blok">
            <div class="container">
                <div class="row">
                   <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3025.913393139031!2d-76.22706288445046!3d40.67587717933575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c5ee8a443cfca5%3A0x6beef0da1388b971!2sBob+Weaver+Auto!5e0!3m2!1sen!2sus!4v1534883315875" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                </div>
            </div>
        </div>
    </section> --%>

    <%--<section class="reviews">
        <div id="google-reviews"></div>
    </section>--%>

    <div id="map-anchor" class="row customer-reviews mdl-top-shadow maps">


        <%--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3522.5242522494077!2d-82.30137344915174!3d28.00844831871637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c2cbe15af02191%3A0x306ca794c0ec2428!2sGator+Ford!5e0!3m2!1sen!2sus!4v1453852199802"
                class="map-fill-width" height="450" style="border: 0"></iframe>--%>



        <div class="col-xs-1 visible-xs"></div>

        <div class="col-xs-10 col-sm-4 col-sm-offset-7 col-md-5 cold-md-offset-6 col-lg-5 col-lg-offset-6 custom-card review-card">
            <div class="row title">
                <h3><%=Default.Row_Title_H3 %></h3>
            </div>
            <CustomerReviewsControl:CustomerReviews ID="ucCustomerReviews" runat="server" />

        </div>

         <FatFooterControl:FatFooter ID="FatFooter" runat="server" />


    </div>


    <% if (!Request.Browser.IsMobileDevice)
        { %>
    <script>        //allows popovers to function on mouseover

        if ($(window).height() < 700 && $(window).height() > 600) {
            $(".bnrimg").css("height", $(window).height() - 200 + "px");
        }
        else if ($(window).height() < 600) {
            $(".bnrimg").css("height", $(window).height() - 100 + "px");
        }
    </script>
    <% } %>

    <script>
        document.addEventListener("DOMContentLoaded",
            function () {
                var div, n,
                    v = document.getElementsByClassName("youtube-player");
                for (n = 0; n < v.length; n++) {
                    div = document.createElement("div");
                    div.setAttribute("data-id", v[n].dataset.id);
                    div.innerHTML = labnolThumb(v[n].dataset.id);
                    div.onclick = labnolIframe;
                    v[n].appendChild(div);
                }
            });

        function labnolThumb(id) {
            var thumb = '<img src="https://storage.googleapis.com/bob-weaver/dealership/bob-weaver-auto.jpg">',
                play = '<div class="play"></div>';
            return thumb.replace("ID", id) + play;
        }

        function labnolIframe() {
            var iframe = document.createElement("iframe");
            var embed = "https://www.youtube.com/embed/ID?rel=0&showinfo=0&autoplay=1";
            iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "1");
            this.parentNode.replaceChild(iframe, this);
        }


        $('.maps').click(function () {
            $('.maps iframe').css("pointer-events", "auto");
        });

        $(".maps").mouseleave(function () {
            $('.maps iframe').css("pointer-events", "none");
        });


        function createMapIframe() {

            var my_map = document.createElement("iframe");
            my_map.src = "https://www.google.com/maps/embed/v1/place?q=place_id:ChIJpfw8RIruxYkRcbmIE9rw7ms&key=AIzaSyBdir-3DWdNDER_bzEROToCr1DQr7jDMuA";
            my_map.className = "map-fill-width";
            my_map.setAttribute("style", "border: 0");
            my_map.height = "450";
            var my_anchor = document.getElementById("map-anchor");

            my_anchor.insertBefore(my_map, my_anchor.childNodes[0]);


        }


        $('#category-tabs li').click(function (e) {
            e.preventDefault();
            $(this).find("a:eq(0)").tab('show');
            $(this).css("active");
        });
        $(document).ready(function () {

            $('[data-toggle="popover"]').popover({
                placement: 'top',
                trigger: 'hover'
            });


            setTimeout(createMapIframe, 30); // lets create map iframe 30 milliseconds after the page is loaded

            $('.review-list').slick({
                dots: false,
                infinite: true,
                arrows: false,
                speed: 1000,
                slidesToShow: 1,
                adaptiveHeight: true,
                autoplay: true,
                fade: true,
                cssEase: 'linear'
            });

            $('.vcp-carousel').slick({
                slidesToShow: 7,
                slidesToScroll: 3,
                infinite: false,
                dots: false,
                arrows: true,
                touchMove: true,
                responsive: [{
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 3,
                        arrows: true,
                    }
                }, {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });

           
        });

        var filtered = false;

        $('.vcp-button').on('click', function () {
            
            var filtername = $(this).parent('li').attr('id');
            var currentclass = $(this).attr('class');
            if (filtered === false) {
                 $('.vcp-carousel').slick('slickUnfilter');
                 $('.vcp-carousel').slick('slickFilter', '.filter-' + filtername);
                $('.vcp-button').attr('class', 'btn btn-default vcp-button');
                $(this).attr('class', 'btn btn-primary vcp-button');

            }
        });

        $(document).ready(function () {

            $('#jeep .vcp-button').click();
        });
    </script>

    <style>
        .customer-reviews {
            padding: 4em;
            background: #f6f6f6;
        }

            .customer-reviews.row {
                margin-top: 20px;
                margin-left: 0px;
                margin-right: 0px;
            }

            .customer-reviews .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .customer-reviews .title {
                background: none;
            }

                .customer-reviews .title h1 {
                    text-align: center;
                    font-size: 30px;
                    font-weight: bold;
                    text-transform: uppercase;
                }

            .customer-reviews .review-list {
                padding: 10px;
            }

                .customer-reviews .review-list .review {
                    list-style-type: none;
                    float: left;
                    width: 100%;
                    height: auto;
                }

                    .customer-reviews .review-list .review .star-rating s:hover {
                        color: #f75600;
                    }

                    .customer-reviews .review-list .review .star-rating s {
                        color: #f75600;
                        font-size: 25px;
                        cursor: default;
                        text-decoration: none;
                        line-height: 20px;
                    }

                    .customer-reviews .review-list .review .star-rating {
                        padding: 2px;
                        float: right;
                        clear: right;
                    }

                        .customer-reviews .review-list .review .star-rating s.rated:before {
                            content: "\2605";
                            color: #f75600;
                        }

                        .customer-reviews .review-list .review .star-rating s:before {
                            content: "\2606";
                        }

                    .customer-reviews .review-list .review .post {
                        font-size: 22px;
                        line-height: 1.4;
                        font-style: italic;
                        padding: 1.5em 0;
                        font-weight: 300;
                        border-radius: 5px;
                        margin-bottom: .5em;
                        outline: none;
                        float: right;
                        clear: right;
                    }

                    .customer-reviews .review-list .review .author {
                        font-size: 22px;
                        font-weight: bold;
                        float: right;
                        clear: right;
                    }

                    .customer-reviews .review-list .review .post {
                        font-size: 14px;
                        padding: .5em 0;
                    }

                    .customer-reviews .review-list .review .post {
                        line-height: 1.4;
                        font-style: italic;
                        font-weight: 300;
                        border-radius: 5px;
                        margin-bottom: .5em;
                        outline: none;
                        float: right;
                        clear: right;
                    }

                    .customer-reviews .review-list .review .author {
                        font-size: 22px;
                        font-weight: bold;
                        float: right;
                        clear: right;
                    }

            .customer-reviews.row {
                margin-top: 0;
            }

            .customer-reviews.row {
                margin-left: 0;
                margin-right: 0;
            }

        .customer-reviews {
            padding: 0;
        }

        .customer-reviews {
            background: #f6f6f6;
        }

        .map-fill-width {
            width: 100%;
            min-height: 450px;
        }

        .custom-card:hover {
            transition: all .1s ease 0s;
            -webkit-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14), 0 3px 1px -1px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 3px 3px rgba(0,0,0,.14), 0 3px 1px -1px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
        }

        .review-card {
            position: absolute;
            margin-top: -435px;
            margin-bottom: 0;
        }

        .custom-card {
            background-color: #fff;
            -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
        }

        .half-card-container {
            padding: 24px 36px;
        }

        .custom-card:hover {
            transition: all 0.1s ease 0s;
            -webkit-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
        }

            .custom-card:hover img {
                -webkit-filter: brightness(75%);
                -webkit-transition: all 0.3s ease .1s;
                -moz-transition: all 0.3s ease .1s;
                -o-transition: all 0.3s ease .1s;
                transition: all 0.3s ease .1s;
            }

        .card-link, .card-link:hover, .card-link:visited {
            color: inherit;
            text-decoration: inherit;
        }

        .card-image img {
            width: 100%;
        }

        .custom-card h3 {
            font-size: 24px;
        }

        .custom-card h3 {
            margin-top: 20px;
            margin-left: 10px;
            margin-bottom: 10px;
        }

        .custom-card h3 {
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        .customer-reviews .review-list {
            padding: 0 10px;
        }
    </style>
</asp:Content>

