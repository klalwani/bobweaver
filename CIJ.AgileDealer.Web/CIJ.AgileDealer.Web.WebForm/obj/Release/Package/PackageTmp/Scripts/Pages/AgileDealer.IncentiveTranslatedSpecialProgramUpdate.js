﻿function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

$(document).ready(function () {
    incentiveTranslatedUpdateSpecialProgram.initialize();
});


var incentiveTranslatedUpdateSpecialProgram = {
    initialize: function () {
        incentiveTranslatedUpdateSpecialProgram.registerEvents();
    },
    isResetAll: false,
    currentTable: {},
    registerEvents: function () {
        $(document).on('click', '#btnSaveChangeSpecialProgram', function () {
            incentiveTranslatedUpdateSpecialProgram.prepareDataForSubmitting();
            incentiveTranslatedUpdateSpecialProgram.submitData();
        });

        $(document).on('click', '#btnResetSpecialProgram', function () {
            $('#modal-confirm-resetSpecialProgram').modal('show');
        });

        $(document).on('click', '#btnConfirmResetSpecialProgram', function () {
            incentiveTranslatedUpdateSpecialProgram.prepareDataForSubmitting();
            incentiveTranslatedUpdateSpecialProgram.resetIncentiveTranslated();
            $('#modal-confirm-resetSpecialProgram').modal('toggle');
        });

        $(document).on('click', '#btnCancelResetSpecialProgram', function () {
            $('#modal-confirm-resetSpecialProgram').modal('toggle');
        });

        $(document).on('click', '#updateForSpecialProgram label', function () {
            var that = $(this);
            var input = that.find('input');
            var value = input.val();
            var target = input.data('target');
            if (target == 'all') {
                $('#vinSectionSpecialProgram').removeClass('in').addClass('collapse');
                $('#modelSectionSpecialProgram').removeClass('in').addClass('collapse');
            }
            else if (target == 'vinSection') {
                $('#vinSectionSpecialProgram').removeClass('collapse').addClass('in');
                $('#modelSectionSpecialProgram').removeClass('in').addClass('collapse');
            }
            else if (target == 'modelSection') {
                $('#vinSectionSpecialProgram').removeClass('in').addClass('collapse');
                $('#modelSectionSpecialProgram').removeClass('collapse').addClass('in');
            }
        });

        $(document).on('shown.bs.modal', '#incentiveEditSpecialProgramControl', function () {
            if (typeof incentiveTranslatedUpdateSpecialProgram.programData != "undefined" && incentiveTranslatedUpdateSpecialProgram.programData != null) {
                $('#lblProgramNameSpecialProgram').text(incentiveTranslatedUpdateSpecialProgram.programData.name);
                if (incentiveTranslatedUpdateSpecialProgram.programData.displaySection != '0' && incentiveTranslatedUpdateSpecialProgram.programData.displaySection != '')
                    $('#ddlDisplaySectionSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.displaySection);
                $('input[name="chkStatusSpecialProgram"][value="' + incentiveTranslatedUpdateSpecialProgram.programData.status + '"]').click();

                $('#CashCouponAmountSpecialProgram').removeClass('collapse').addClass('in');
                $('#DisclaimerSpecialProgram').removeClass('collapse').addClass('in');
                $("#txtCashCouponAmountSpecialProgram").val(CommaFormatted(incentiveTranslatedUpdateSpecialProgram.programData.amount).replace("$", ""));
                $("#txtDisclaimerSpecialProgram").val(incentiveTranslatedUpdateSpecialProgram.programData.disclaimer);
                $('#txtDateStart').val(incentiveTranslatedUpdateSpecialProgram.programData.startdate);
                $('#txtDateEnd').val(incentiveTranslatedUpdateSpecialProgram.programData.enddate);
                // $('#modelSection').removeClass('in').addClass('collapse');


                $('#txtPositionSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.position);
                $('#txtTranslatedNameSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.translatedName);

                $('#updateForSpecialProgram label input[value="' + incentiveTranslatedUpdateSpecialProgram.programData.updateFor + '"]').click();
                if (incentiveTranslatedUpdateSpecialProgram.programData.updateFor == UPDATEFOR.VIN) {
                    $('#txtVinSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.updateForVin);
                }
                else if (incentiveTranslatedUpdateSpecialProgram.programData.updateFor == UPDATEFOR.MODEL) {
                    $('#ddlModelSpecialProgram').val(incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId);
                }

                var valEngineCodesArr = "0".split(",");
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes != null) {
                    valEngineCodesArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes.split(",");
                }



                var i = 0;
                var size = valEngineCodesArr.length;
                var selectedEngines = "";
                for (i = 0; i < size; i++) {
                    $("#engineSectionSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valEngineCodesArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == valEngineCodesArr - 1) {
                                selectedEngines = selectedEngines + $(this).find("input").prop('title');
                            }
                            else
                                selectedEngines = selectedEngines + $(this).find("input").prop('title') + ",";


                        }
                    });
                    if (selectedEngines.toString().length > 70) {
                        $("#engineSectionSpecialProgram .ms-options-wrap button").text(selectedEngines.substr(1, 69) + "...");
                        $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", selectedEngines);
                    }
                    if (selectedEngines.toString().length > 1) {

                        $("#engineSectionSpecialProgram .ms-options-wrap button").text(selectedEngines);
                        $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", selectedEngines);
                    }


                }
                $("#ddlEnginesSpecialProgram").val(valEngineCodesArr);

                var valVinArr = "0";
                if (incentiveTranslatedUpdateSpecialProgram.programData.excludeVINs != null) {
                    valVinArr = incentiveTranslatedUpdateSpecialProgram.programData.excludeVINs.split(",");
                }



                i = 0;
                var sizeVinArr = valVinArr.length;
                var selectedVins = "";

                for (i = 0; i < sizeVinArr; i++) {
                    valVinArr[i] = $.trim(valVinArr[i]);
                    $("#excludeVinSectionSpecialProgram .ms-options ul li").each(function () {
                        if ($(this).find("input").val() == valVinArr[i]) {
                            $(this).addClass("selected");
                            $(this).find("input").prop('checked', true);
                            $(this).find("input").attr('checked', true);
                            $(this).find("input").addClass("focused");
                            if (i == sizeVinArr - 1) {
                                selectedVins = selectedVins + $(this).find("input").prop('title');
                            }
                            else
                                selectedVins = selectedVins + $(this).find("input").prop('title') + ",";

                        }
                    });
                    if (selectedVins.toString().length > 70) {
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text(selectedVins.substr(1, 69) + "...");
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", selectedVins);
                    }
                    if (selectedVins.toString().length > 1) {

                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text(selectedVins);
                        $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", selectedVins);
                    }


                }
                $("#ddlVinSpecialProgram").val(valVinArr);

                if (incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange == 'True') {
                    incentiveManagementSpecialProgram.setOverrideExperationButtonSpecialPrograms('setDate');

                }

            }
        });
    },
    prepareDataForSubmitting: function () {
        incentiveTranslatedUpdateSpecialProgram.programData.id = incentiveTranslatedUpdateSpecialProgram.programData.id;
        incentiveTranslatedUpdateSpecialProgram.programData.displaySection = $('#ddlDisplaySectionSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.position = $('#txtPositionSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.translatedName = $('#txtTranslatedNameSpecialProgram').val();
        if ($('input[name="chkStatusSpecialProgram"]:checked')[0].value == "Hidden") {
            incentiveTranslatedUpdateSpecialProgram.programData.isActive = false;
        } else {
            incentiveTranslatedUpdateSpecialProgram.programData.isActive = true;
        }

        incentiveTranslatedUpdateSpecialProgram.programData.updateFor = $('#updateForSpecialProgram label.active input').val();
        incentiveTranslatedUpdateSpecialProgram.programData.updateForVin = $('#txtVinSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.updateForModelId = $('#ddlModelSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.isResetAll = incentiveTranslatedUpdateSpecialProgram.isResetAll;

        incentiveTranslatedUpdateSpecialProgram.programData.isCashCoupon = true;

        incentiveTranslatedUpdateSpecialProgram.programData.amount = $("#txtCashCouponAmountSpecialProgram").val().replace(/,/gi, "").replace("$", "");
        incentiveTranslatedUpdateSpecialProgram.programData.disclaimer = $("#txtDisclaimerSpecialProgram").val();
        incentiveTranslatedUpdateSpecialProgram.programData.excludeenginecodes = $('#ddlEnginesSpecialProgram').val();
        incentiveTranslatedUpdateSpecialProgram.programData.excludeVINArrayData = $('#ddlVinSpecialProgram').val();

       

        var isSelectedMode = $('#priceDateSpecialPrograms').find('label.active');
        if ($(isSelectedMode).data('toggle') == 'in') {
            incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange = true;
            incentiveTranslatedUpdateSpecialProgram.programData.startdate = $("#txtDateStart").val();
            incentiveTranslatedUpdateSpecialProgram.programData.enddate = $("#txtDateEnd").val();

        } else {
            incentiveTranslatedUpdateSpecialProgram.programData.hassetDateRange = false;
            incentiveTranslatedUpdateSpecialProgram.programData.startdate = null;
            incentiveTranslatedUpdateSpecialProgram.programData.enddate = null;
        }

    },
    submitData: function () {
        var method = 'UpdateIncentiveTranslatedSpecialProgram';
        var isValid = true;
        if (incentiveTranslatedUpdateSpecialProgram.programData.amount == "") {
            $("#txtCashCouponAmountSpecialProgram").addClass("input-validation-error");
            isValid = false;
        }
        if (incentiveTranslatedUpdateSpecialProgram.programData.displaySection == "") {
            $("#ddlDisplaySectionSpecialProgram").addClass("input-validation-error");
            isValid = false;
        }
        if (incentiveTranslatedUpdateSpecialProgram.programData.translatedName == "") {
            $("#txtTranslatedNameSpecialProgram").addClass("input-validation-error");
            isValid = false;
        }

        if (isValid) {

            var result = true;
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 30000,
                data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        result = true;
                        if (typeof incentiveTranslatedUpdateSpecialProgram.currentTable != 'undefined' && incentiveTranslatedUpdateSpecialProgram.currentTable != null)
                            $('.modal').modal('hide');
                        incentiveTranslatedUpdateSpecialProgram.currentTable.ajax.reload();
                    }
                }
            });
        }
        else {
            // $('.modal').modal('hide');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            // $('#incentiveEditSpecialProgramControl').modal('toggle');
            alert("Please input all required information!");
            document.getElementById('incentiveEditSpecialProgramControl').style.display = "block";
            return false;
        }
    },
    resetIncentiveTranslated: function () {
        var method = 'ResetIncentiveTranslatedSpecialProgram';
        var isResetAll = false;

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdateSpecialProgram.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdateSpecialProgram.currentTable != 'undefined' && incentiveTranslatedUpdateSpecialProgram.currentTable != null)
                        $('.modal').modal('hide');
                    incentiveTranslatedUpdateSpecialProgram.currentTable.ajax.reload();
                }
            }
        });
    }
};