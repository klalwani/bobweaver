﻿var FinanceContactInfo = {
    getContactInfoInformation: function () {
        var postData = {
            contactInfo: {
                CurrentAddress: '',
                City: '',
                State: '',
                ZipCode: '',
                ResidenceStatus: '',
                MonthlyMortage: '',
                MonthAttended: '',
                YearAttended: '',
                PreviousAddress: '',
                PreviousCity: '',
                PreviousState: '',
                PreviousZipCode: '',
                IsCorrespondingContactInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: ''
            },
            applicantInfo: {
                FirstName: '',
                LastName: '',
                MiddleName: '',
                Email: '',
                PhoneNumber: '',
                SocialSercurityNumber: '',
                DateOfBirth: '',
                MonthOfBirth: '',
                YearOfBirth: '',
                IsCorrespondingApplicantInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: ''
            },
            employmentInfo: {
                CurrentEmployer: '',
                MonthlyIncome: '',
                WorkingPeriod: '',
                PreviousEmployer: '',
                AdditionalIncomeSource: '',
                AdditionalIncomeAmount: '',
                IsCorrespondingEmploymentInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: '',
                CurrentEmployerTelephone: '',
                JobTitle:''
            }

        };
        postData.contactInfo.CurrentAddress = $('#contactStreetAddress').val();
        postData.contactInfo.City = $('#contactCity').val();
        postData.contactInfo.State = $('#contactState').val();
        postData.contactInfo.ZipCode = $('#contactZip').val();
        postData.contactInfo.ResidenceStatus = $('#contactResidenceStatus').val();
        postData.contactInfo.MonthlyMortage = $('#contactMonthlyHousing').val();
        postData.contactInfo.MonthAttended = $('#contactResidenceMonths').val();
        postData.contactInfo.YearAttended = $('#contactResidenceYears').val();
        postData.contactInfo.PreviousAddress = $('#contactPreviousStreetAddress').val();
        postData.contactInfo.PreviousCity = $('#contactPreviousCity').val();
        postData.contactInfo.PreviousState = $('#contactPreviousState').val();
        postData.contactInfo.PreviousZipCode = $('#contactPreviousZip').val();
        postData.contactInfo.IsCorrespondingContactInfo = true;
        postData.contactInfo.NumberOfApplicant = $('input[name="creditAppParties"]:checked').val();
        postData.contactInfo.IsActive = true;
        postData.contactInfo.ApplicantId = $('#applicantId').val();


        postData.applicantInfo.FirstName = $('#contactFirstName').val();
        postData.applicantInfo.LastName = $('#contactLastName').val();
        postData.applicantInfo.MiddleName = $('#contactMiddleName').val();
        postData.applicantInfo.Email = $('#contactEmail').val();
        postData.applicantInfo.PhoneNumber = $('#contactPhone').val();
        postData.applicantInfo.SocialSercurityNumber = $('#contactSSN').val();
        postData.applicantInfo.DateOfBirth = $('#contactBirthDay').val();
        postData.applicantInfo.MonthOfBirth = $('#contactBirthMonth').val();
        postData.applicantInfo.YearOfBirth = $('#contactBirthYear').val();
        postData.applicantInfo.IsCorrespondingApplicantInfo = true;
        postData.applicantInfo.NumberOfApplicant = $('input[name="creditAppParties"]:checked').val();
        postData.applicantInfo.IsActive = true;
        postData.applicantInfo.ApplicantId = $('#applicantId').val();

        postData.employmentInfo.CurrentEmployer = $('#contactCurrentEmployer').val();
        postData.employmentInfo.CurrentEmployerTelephone = $('#contactCurrentEmployerTelePhone').val();
        postData.employmentInfo.JobTitle = $('#contactJobTitle').val();
        postData.employmentInfo.MonthlyIncome = $('#contactMonthlyIncome').val();
        postData.employmentInfo.WorkingPeriod = $('#contactEmployerDurationYear').val() + ';' + $('#contactEmployerDurationMonth').val();
        postData.employmentInfo.PreviousEmployer = $('#contactPreviousEmployer').val();

        if (typeof $('#contactAdditionalIncome').val() == undefined || $('#contactAdditionalIncome').val() == '')
            postData.employmentInfo.AdditionalIncomeSource = '';
        else
            postData.employmentInfo.AdditionalIncomeSource = $('#contactAdditionalIncome').val();

        if (typeof $('#contactAdditionalAmount').val() == undefined || $('#contactAdditionalAmount').val() == '')
            postData.employmentInfo.AdditionalIncomeAmount = 0;
        else
            postData.employmentInfo.AdditionalIncomeAmount = $('#contactAdditionalAmount').val();


        postData.employmentInfo.IsCorrespondingEmploymentInfo = true;
        postData.employmentInfo.NumberOfApplicant = $('input[name="creditAppParties"]:checked').val();
        postData.employmentInfo.IsActive = true;
        postData.employmentInfo.ApplicantId = $('#applicantId').val();

        return postData;
    }
};