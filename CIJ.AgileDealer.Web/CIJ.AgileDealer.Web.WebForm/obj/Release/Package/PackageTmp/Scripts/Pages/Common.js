﻿$(document).ready(function () {
    common.registerPrototype();
})
var common = {
    registerPrototype: function () {
        if (!String.prototype.format) {
            String.prototype.format = function () {
                var formatted = this;
                for (var i = 0; i < arguments.length; i++) {
                    var regexp = new RegExp('\\{' + i + '\\}', 'gi');
                    formatted = formatted.replace(regexp, arguments[i]);
                }
                return formatted;
            };
        }
    },
    testPattern: function (input, pattern) {
        var isValid = true;
        if (pattern.test(input.val())) {
            input.removeClass('input-validation-error');
            isValid = true;
        }
        else {
            input.addClass('input-validation-error');
            isValid = false;
        }
        return isValid;
    },
    validateTextBox: function (input) {
        var isValid = true;
        if (typeof input != "undefined" && input != null) {
            if (input.val() === '') {
                input.addClass('input-validation-error');
                isValid = false;
            } else {
                input.removeClass('input-validation-error');
                isValid = true;
            }
        }
        return isValid;
    },
    validateEmail: function (input) {
        var isValid = true;
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        isValid = common.testPattern(input, pattern);
        return isValid;
    },
    validatePhone: function (input) {
        var isValid = true;
        var pattern = /^[0-9-+]+$/;
        isValid = common.testPattern(input, pattern);
        return isValid;
    },
    getCurrentLanguage: function () {
        var lan = (navigator.language || navigator.browserLanguage).split('-')[0];
        return lan;
    },
    getLocalizations: function (){
        var inputs = $('input:hidden[data-attribute="validation"]');
        var localizations = {};
        inputs.each(function (i, v) {
            var name = $(v).attr("name");
            var value = $(v).val();
            localizations[name] = value;
        });
        return localizations;
    }
};

