﻿$(document).ready(function () {
    AgileDealerValidation.InitialValidationEvent();
    editDealerInfo.initialize();
    editDealerInfo.getDealerInfo();
});
var editDealerInfo = {
    self:'dealerInfo',
    initialize: function () {
        editDealerInfo.registerEvent();
    },

    registerEvent: function () {
        $('#btnSave').click(function () {
            siteMaster.touchUpRequireFields(editDealerInfo.self);
            if (siteMaster.checkValidationOfCurrentForm(editDealerInfo.self)) {
                editDealerInfo.submitData();
            } else {
                alert('Please input all required information!');
            }
        });
    },

    bindingData: function (dealerInfo) {
        $('#dealerName').val(dealerInfo.DealerName);
        $('#genericImageServer').val(dealerInfo.ImageServerGeneric);
        $('#stockImageServer').val(dealerInfo.VcpImageStockServer);
        $('#imageServer').val(dealerInfo.ImageServer);
        $('#dealerCity').val(dealerInfo.DealerCity);
        $('#dealerState').val(dealerInfo.DealerState);
    }, 

    getDealerInfo: function () {
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/EditDealerInfo.ashx?method=' + 'GetDealerInfo',
            type: 'GET',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            success: function (dataResult) {
                editDealerInfo.bindingData(dataResult);
                $.unblockUI(siteMaster.loadingWheel());
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert("Error when contacting to server, please try again later!");
            }
        });
    },

    submitData: function () {
        var data = {
            name: $('#dealerName').val(),
            imageServerGeneric: $('#genericImageServer').val(),
            vcpImageStockServer: $('#stockImageServer').val(),
            imageServer: $('#imageServer').val(),
            locations: [{ city: $('#dealerCity').val(),state:$('#dealerState').val(),}]
        }
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/EditDealerInfo.ashx?method=' + 'UpdateDealerInfo',
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                alert("Record updated successfully!");
                editDealerInfo.bindingData(dataResult);
                $.unblockUI(siteMaster.loadingWheel());
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert("Error when contacting to server, please try again later!");
            }
        });
    }
}