﻿$(document).ready(function () {
    jobManagement.initialize();
});

var jobColumns =
[
    { "sName": "JobTrackingId", sWidth: "200px", sClass: "", 'bSortable': true, 'bVisible': false },
    { "sName": "JobName", sWidth: "200px", sClass: "", 'bSortable': true },
    { "sName": "Status", sWidth: "200px", sClass: "", 'bSortable': true },
    { "sName": "TriggerBy", sWidth: "200px", sClass: "", 'bSortable': true },
    { "sName": "TriggeredTime", sWidth: "200px", sClass: "", 'bSortable': true },
    { "sName": "StarTime", sWidth: "200px", sClass: "", 'bSortable': true },
    { "sName": "EndTime", sWidth: "200px", sClass: "", 'bSortable': true }
];

var oTable;


var jobManagement = {
    self: 'job-management',
    url: function (method) {
        return siteMaster.currentUrl() + '/Controllers/JobController.ashx?method=' + method;
    },
    initialize: function () {
        jobManagement.loadJobDashboard();
        jobManagement.registerEvent();

    },
    registerEvent: function () {
        $('#btnTriggerRecon').click(function () {
            jobManagement.triggerJobManually('TriggerReconJobManually', 'Recon');
        });

        $('#btnTriggerIncentivePuller').click(function () {
            jobManagement.triggerJobManually('TriggerIncentiveManually', 'IncentivePuller');
        });
    },
    triggerJobManually: function (method, jobName) {
        if (method != '' && jobName != '') {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: jobManagement.url(method),
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                success: function (dataResult) {
                    $.unblockUI(siteMaster.loadingWheel());
                    if (dataResult.isSuccess) {
                        if (dataResult.message == JobStatusEnum.Triggered) {
                            alert('The ' + jobName + ' is triggered, please wait until it is started!');
                        }
                        if (dataResult.message == JobStatusEnum.Running) {
                            alert('The ' + jobName + ' is running, please try again later!');
                        }
                    } else {
                        alert("There was something wrong, please try again later!");
                    }
                    oTable.ajax.reload();
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    loadJobDashboard: function () {
        var method = 'GetJobTrackingDashboard';
        oTable = $('#tableJobManagement').DataTable({
            ajax: {
                url: jobManagement.url(method),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                //data: function () {
                //    return JSON.stringify($('#applicantTypeddl').val());
                //}
            },
            language: {
                emptyTable: "No record found!"
            },
            columns: [
                { sName: 'JobTrackingId', "visible": false },
                { sName: 'JobName' },
                { sName: 'Status' },
                { sName: 'TriggerBy' },
                { sName: 'TriggeredTime' },
                { sName: 'StarTime' },
                { sName: 'EndTime' }
            ],
            order: [[4, "desc"]]
        });
    }
};