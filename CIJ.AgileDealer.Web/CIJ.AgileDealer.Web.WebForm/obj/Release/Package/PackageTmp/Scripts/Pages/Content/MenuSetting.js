﻿var menuSetting = {
	init: function () {
		menuSetting.pageLoad();
		menuSetting.registerEvent();
	},
	pageLoad: function () {
		$('#menu').find('li.parent').each(function (e, el) {
			var li = $(el).find('li')
			li.each(function (i, el) {
				var isCMS = $(el).attr('data-iscms');
				if (isCMS == "True") {
					$(el).css("background", "chartreuse");
				}
			});
		});

		$("ul[id^='MainContent_tree_subNavList']").each(function (i, el) {
			$(el).sortable({
				placeholder: "ui-state-highlight",
				connectWith: ".connectedSortable",
				update: function (event, ui) {
					menuSetting.changeOrder();
				}
			})
			$(el).disableSelection();
		});
	},
	changeOrder: function () {
		var dataPost = [];
		var parentIndex = 0;
		$('#menu').find('li.parent').each(function (e, el) {
			var id = $(el).attr('data-id');
			var li = $(el).find('li')
			var nodes = [];
			var index = 0;
			li.each(function (i, el) {
				nodes.push({ NodeId: $(el).attr('data-id'), Text: '', Nodes: [], Index: index });
				index++;
			});
			dataPost.push({ NodeId: id, Text: '', Nodes: nodes, Index: parentIndex })
			parentIndex++;
		})
		$.blockUI(siteMaster.loadingWheel());
		$.ajax({
			url: siteMaster.currentUrl() + '/Controllers/MenuSettingController.ashx',
			type: 'POST',
			async: true,
			dataType: "json",
			contentType: "application/json",
			cache: false,
			timeout: 180000,
			data: JSON.stringify(dataPost),
			success: function (dataResult) {
				$.unblockUI(siteMaster.loadingWheel());
			},
			error: function () {
				$.unblockUI(siteMaster.loadingWheel());
				alert("Error when contacting to server, please try again later!");
			}
		});
	},
	registerEvent: function () {
		$('#headerBuiltInVariables').click(function () {
			$('#bodyBuiltInVariables').toggle("slow", function () {
				// Animation complete.
			});
		});

		$('#resetOrder').click(function () {
			var dataPost = [];
			var parentIndex = 0;
			$('#menu').find('li.parent').each(function (e, el) {
				var id = $(el).attr('data-id');
				var li = $(el).find('li')
				var nodes = [];
				var index = 0;
				li.each(function (i, el) {
					nodes.push({ NodeId: $(el).attr('data-id'), Text: '', Nodes: [], Index: index });
					index++;
				});
				dataPost.push({ NodeId: id, Text: '', Nodes: nodes, Index: parentIndex })
				parentIndex++;
			})
			$.blockUI(siteMaster.loadingWheel());
			$.ajax({
				url: siteMaster.currentUrl() + '/Controllers/MenuSettingController.ashx',
				type: 'POST',
				async: true,
				dataType: "json",
				contentType: "application/json",
				cache: false,
				timeout: 180000,
				data: JSON.stringify(dataPost),
				success: function (dataResult) {
					$.unblockUI(siteMaster.loadingWheel());
				},
				error: function () {
					$.unblockUI(siteMaster.loadingWheel());
					alert("Error when contacting to server, please try again later!");
				}
			});
		})

		$('.spinner .btn:first-of-type').on('click', function () {
			$('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
		});

		$('.spinner .btn:last-of-type').on('click', function () {
			$('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
		});

		$('#MainContent_order').keypress(function (e) {
			if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
				e.preventDefault();
			}
		})
	}
}