﻿$(document).ready(function () {
    employeeDashboard.initialize();
});

var oTable;

var employeeDashboard = {
    isRunningAsPopup: function () {
        return true;
    },
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'EmployeeDashboard';
    },
    initialize: function () {
        employeeDashboard.loademployeeDashboard();
        employeeDashboard.registerEvent();
    },
    registerEvent: function () {
        $('#tableEmployee').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = oTable.row(row).data()[0];
            $('[id$="EmployeeId"]').val(id);
            $("#employee-update").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Update Employee',
                show: { effect: "fade", duration: 300 },
                position: { my: "center", at: "center", of: window },
                width: "768px",
                height: "auto"
            });

            //location.href = siteMaster.currentUrl() + '/Admin/StaffEdit/' + id;
        });

        $('#tableEmployee').on('click', 'a.editor_delete', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var employeeId = oTable.row(row).data()[0];
            var that = $(this);
            var parentDiv = $(that).closest('.staff-card');
            if (typeof employeeId != "undefined" && employeeId != null && employeeId > 0) {
                var employee = {
                    employeeId: employeeId
                };
                $("#dialog-confirm").dialog({
                    resizable: false,
                    modal: true,
                    closeOnEscape: true,
                    show: { effect: "fade", duration: 300 },
                    position: { my: "center", at: "center", of: window },
                    width: "auto",
                    height: "auto",
                    buttons: {
                        "Yes, I am sure": function () {
                            employeeDashboard.submitData(employee, 'DeleteEmployee', null, null);
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }

        });

        $('#btnAddNewEmployee').click(function () {
            $('[id$="EmployeeId"]').val('');
            siteMaster.clearAllInput('employee-update');
            siteMaster.removeValidationError('employee-update');
            $("#employee-update").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Add New Employee',
                show: { effect: "fade", duration: 300 },
                position: { my: "center", at: "center", of: window },
                width: "768px",
                height: "auto"
            });
            $('#vehicleImage').attr('src', 'https://agiledealer.blob.core.windows.net/generic/content/staff/staffnotavailable.jpg'); //Clear out source image

        });
    },
    submitData: function (employee, method, callback, parentDiv) {
        var data = employee;
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/EmployeeController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        if (typeof callback == "function" && typeof callback != "undefined" && callback != null) {
                            callback(parentDiv);
                        }
                    } else {
                        alert("Something went wrong when updating this staff.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                    oTable.ajax.reload();
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    loademployeeDashboard: function () {
        oTable = $('#tableEmployee')
            .DataTable({
                ajax: employeeDashboard.url(),
                columns: [
                    { sName: 'EmployeeId', "visible": false },
                    { sName: 'FirstName' },
                    { sName: 'LastName' },
                    { sName: 'Email' },
                    { sName: 'IsNew' },
                    { sName: 'Order' },
                    { sName: 'IsDeleted' },
                    { sName: 'DateCreated' },
                    {
                        sName: 'Employee',
                        defaultContent: '<a href="" class="editor_edit">Update</a>'
                    },
                    {
                        sName: 'Delete',
                        defaultContent: '<a href="" class="editor_delete">Delete</a>'
                    }
                ]
            });
    }
}