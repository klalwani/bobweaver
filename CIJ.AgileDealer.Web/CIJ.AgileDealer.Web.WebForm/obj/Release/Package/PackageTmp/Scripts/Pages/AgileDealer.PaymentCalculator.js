﻿$(document).ready(function () {
    paymentCalculator.initialize();
});

var paymentCalculator = {
    localizations:{},
    initialize: function () {
        paymentCalculator.registerEvent();
        paymentCalculator.maskInput();
        paymentCalculator.setHeaderVisible();
        paymentCalculator.localizations = common.getLocalizations();
    },
    setHeaderVisible: function() {
        var isShow = $('[id$="HdfIsHideHeader"]').val();
        if (isShow == "True") {
            $('#payment-calculator-title').hide();
            $('#btnCloseBot').hide();
            $('[id$="VehiclePrice"]').val("50.000");
            paymentCalculator.setDefaultValue();
            paymentCalculator.calculatePayment();
        } else {
            $('#payment-calculator-title').show();
            $('#btnCloseBot').show();
        }

    },
    registerEvent: function () {
        $("#ucPaymentCalculator").on("dialogopen", function (event, ui) {
            $('#lblResult').text('');
            paymentCalculator.maskInput();
            paymentCalculator.setDefaultValue();
            paymentCalculator.calculatePayment();
        });
        $('#ucPaymentCalculator').on('click', '#btnClosePayment,#btnCloseBot', function () {
            $('#ucPaymentCalculator').dialog('close');
        });
        $(document).on('click', '#btnCalculate', function () {
            paymentCalculator.calculatePayment();
        });

    },
    calculatePayment: function() {
        var prices = paymentCalculator.getPriceToCalculate();
        if (typeof prices != "undefined" && prices != null) {
            var total = paymentCalculator.calculatePaymentPerMonth(prices);
            var message = '';
            if (isNaN(total)) {
                message = paymentCalculator.localizations["ValidationError"];
            }
            else {
                var total = total.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var price = prices.termInMonths;
                message = paymentCalculator.localizations["SuccessMessage"].format(total, price);
            }
            $('#lblResult').text(message);
        }
    },
    setDefaultValue: function () {
        var vehiclePrice = $('[id$="VehiclePrice"]').val();
        $('#txtVehiclePrice').blur().val(vehiclePrice);
        $('#txtInterestRate').val('4.5%');
        $('#txtDownPayment').val(0);
        $('#txtTradeInValue').val(0);
        $('input[name="periodType"][value=72]').attr('checked', 'checked');

    },
    maskInput: function () {
        $('#txtVehiclePrice').mask("###,###,###", { reverse: true });
        $("#txtInterestRate").mask("99.99%");
        $('#txtDownPayment').mask("###,###,###", { reverse: true });
        $('#txtTradeInValue').mask("###,###,###", { reverse: true });
    },
    tryParseValue: function (value) {
        return parseFloat(value);
    },
    calculatePaymentPerMonth: function (prices) {
        var vehiclePrice = paymentCalculator.tryParseValue(prices.vehiclePrice.replace(",","")) ;
        var interestPrice = paymentCalculator.tryParseValue(prices.interestPrice);
        var downPayment = paymentCalculator.tryParseValue(prices.downPayment.replace(",", ""));
        var tradeInvalue = paymentCalculator.tryParseValue(prices.tradeInvalue.replace(",",""));
        var termInMonths = paymentCalculator.tryParseValue(prices.termInMonths);

        var calPrice = vehiclePrice - downPayment - tradeInvalue;

        var x = Math.pow(1 + interestPrice/1200.00, termInMonths);
        return (calPrice * x * interestPrice/1200.00) / (x - 1);


    },
    getPriceToCalculate: function () {
        var prices = {
            vehiclePrice: '',
            interestPrice: '',
            downPayment: '',
            tradeInvalue: '',
            termInMonths: ''
        };

        prices.vehiclePrice = $('#txtVehiclePrice').val();
        prices.interestPrice = $('#txtInterestRate').val();
        prices.downPayment = $('#txtDownPayment').val();
        prices.tradeInvalue = $('#txtTradeInValue').val();
        prices.termInMonths = paymentCalculator.getTermInMonths();

        return prices;
    },
    getTermInMonths: function () {
        return $('input[name="periodType"]:checked').val();
    }
};
