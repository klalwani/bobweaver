﻿$(document).ready(function () {
    rebateControl.initialize();
});

var isShowSingleSection = false;
var isShowCashSection = false;
var isShowConditionalSection = false;
var isShowFinanceSection = false;

var rebateControl = {
    initialize: function() {
        rebateControl.registerEvent();
    },
    registerEvent: function() {
        $('#viewCompleteOffer').click(function(e) {
            rebateControl.showAllPrograms();
        });
    },
    showAllPrograms: function () {
        if (isShowSingleSection) {
            $('#cashProgramContent').hide();
            $('#cashProgramTitle').hide();
            $('#conditionalProgramContent').hide();
            $('#conditionalProgramTitle').hide();
            $('#aprProgramContent').hide();
            $('#aprProgramTitle').hide();
            $('#singleProgramContent').show();

            isShowSingleSection = false;
           
        } else {
            isShowSingleSection = true;
            $('#singleProgramContent').hide();
            if (isShowCashSection) {
                $('#cashProgramContent').show();
                $('#cashProgramTitle').show();
            } else {
                $('#cashProgramContent').hide();
                $('#cashProgramTitle').hide();
            }

            if (isShowConditionalSection) {
                $('#conditionalProgramContent').show();
                $('#conditionalProgramTitle').show();
            } else {
                $('#conditionalProgramContent').hide();
                $('#conditionalProgramTitle').hide();
            }


            if (isShowFinanceSection) {
                $('#aprProgramContent').show();
                $('#aprProgramTitle').show();
            } else {
                $('#aprProgramContent').hide();
                $('#aprProgramTitle').hide();
            }

        }
    },
    loadRebateForVehicle: function (data, method) {
        isShowSingleSection = false;
        isShowCashSection = false;
        isShowConditionalSection = false;
        isShowFinanceSection = false;

        if (typeof data != undefined && data != null) {
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/VehicleController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 30000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    $('#cashProgramContent').empty();
                    $('#cashProgramTitle').hide();
                    $('#conditionalProgramContent').empty();
                    $('#conditionalProgramTitle').hide();
                    $('#aprProgramContent').empty();
                    $('#aprProgramTitle').hide();
                    $('#singleProgramContent').empty();

                    $('.modal-body').scrollTop(0);

                    if (dataResult.isSuccess === true) {
                        if (typeof dataResult.cashPrograms != "undefined" && dataResult.cashPrograms != null) {
                            if (dataResult.cashPrograms.length > 0) {
                                isShowCashSection = true;
//                                $('#cashProgramTitle').show();
                            }
                            for (var i = 0; i < dataResult.cashPrograms.length; i++) {
                                var tempCashProgram = dataResult.cashPrograms[i];
                                $.tmpl($('#cashTemplate'), tempCashProgram).appendTo('#cashProgramContent');
                            }

                        }
                        if (typeof dataResult.conditionalPrograms != "undefined" &&
                            dataResult.conditionalPrograms != null) {
                            if (dataResult.conditionalPrograms.length > 0) {
                                isShowConditionalSection = true;
                            //    $('#conditionalProgramTitle').show();
                            }
                            for (var i = 0; i < dataResult.conditionalPrograms.length; i++) {
                                var tempConditionalProgram = dataResult.conditionalPrograms[i];
                                $.tmpl($('#conditionalTemplate'), tempConditionalProgram)
                                    .appendTo('#conditionalProgramContent');
                            }
                        }
                        if (typeof dataResult.aprPrograms != "undefined" && dataResult.aprPrograms != null) {
                            if (dataResult.aprPrograms.length > 0) {
                                isShowFinanceSection = true;
                                //$('#aprProgramTitle').show();
                            }
                            for (var i = 0; i < dataResult.aprPrograms.length; i++) {
                                var tempAprProgram = dataResult.aprPrograms[i];
                                if (tempAprProgram.IsAprParent && i > 0) {
                                    $.tmpl($('#orProgramTemplate')).appendTo('#aprProgramContent');
                                }
                                $.tmpl($('#financeTemplate'), tempAprProgram).appendTo('#aprProgramContent');

                            }
                        }
                        if (typeof dataResult.singleProgramDatas != "undefined" && dataResult.singleProgramDatas != null) {
                            if (dataResult.singleProgramDatas.length > 0) {
                                isShowSingleSection = true;
                                //$('#singleProgramTemplate').show();
                            }
                            for (var i = 0; i < dataResult.singleProgramDatas.length; i++) {
                                var tempSingleProgram = dataResult.singleProgramDatas[i];
                                $.tmpl($('#singleProgramTemplate'), tempSingleProgram).appendTo('#singleProgramContent');
                                if (dataResult.singleProgramDatas.length > 1 && i < dataResult.singleProgramDatas.length-1)
                                    $.tmpl($('#ORItemTemplate'), tempSingleProgram).appendTo('#singleProgramContent');
                            }
                        }
                        rebateControl.showAllPrograms();
                    }
                }
            });
        }
    }
};

