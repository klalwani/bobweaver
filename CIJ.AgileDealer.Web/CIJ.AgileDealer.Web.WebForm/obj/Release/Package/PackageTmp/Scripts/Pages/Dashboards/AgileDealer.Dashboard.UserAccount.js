﻿$(document).ready(function () {
    userAccountDashboard.initialize();
});

var oTable;

var userAccountDashboard = {
    url: function (method) {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method;
    },
    initialize: function () {
        userAccountDashboard.loadApplicantDashboard();
        userAccountDashboard.registerEvent();
    },
    registerEvent: function () {
        $('#tableUserAccount').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = oTable.row(row).data()[0];
            location.href = siteMaster.currentUrl() + '/Admin/UserAccount/Update/' + id;
        });
    },
    loadApplicantDashboard: function () {
        oTable = $('#tableUserAccount').DataTable({
            ajax: userAccountDashboard.url('UserAccountDashboard'),
            columns: [
                { sName: 'UserId', "visible": false },
                { sName: 'UserName' },
                { sName: 'Email' },
                { sName: 'EmailConfirmed' },
                { sName: 'PhoneNumber' },
                { sName: 'Rurrent Role' },
                {
                    sName: 'ApplicantId',
                    defaultContent: '<a href="" class="editor_edit">Update</a>'
                }
            ]
        });
    }
}