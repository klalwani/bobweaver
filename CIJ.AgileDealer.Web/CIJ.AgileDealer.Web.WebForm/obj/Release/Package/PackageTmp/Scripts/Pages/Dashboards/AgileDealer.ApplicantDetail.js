﻿$(document).ready(function () {
    applicantDetail.initialize();
});
var tempData;

var applicantDetail = {
    applicantId: function () {
        return $('[id$="ApplicantId"]').val();
    },
    initialize: function () {
        applicantDetail.getDataOfApplicant();
        applicantDetail.registerEvents();
    },
    registerEvents: function () {
        $('#btnBack').click(function() {
            parent.history.back();
            return false;
        });

        $("[name='btnArchived']").click(function() {
            var that = $(this);
            var isArchived = $('#IsArchived').val();
            if (isArchived == 'false') {
                isArchived = true;
                that.val('De-Archive Application');
            } else {
                isArchived = false;
                that.val('Archive Application');
            }
            $('#IsArchived').val(isArchived);
            var data = {
                applicantId: applicantDetail.applicantId(),
                isArchived: isArchived
            };
            var method = 'ArchiveApplicant';
            applicantDetail.archiveApplication(data, method);
        });

        $('#btnPrintApplicantDetail').on('click', function () {
            $('#applicantDetailForm').print({
                globalStyles: true,
                mediaPrint: false,
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                deferred: $.Deferred(),
                timeout: 2000,
                title: null,
                doctype: '<!doctype html>'
            });
        });
    },
    disableControls: function () {
        $('#applicantDetailForm input:not(.btn),select').attr('disabled', 'disabled');
        $('div.btn-group').find('label:not(.active)').attr('disabled', '');
    },
    hookUpDataToUI: function (applicantDetailData) {
        if (applicantDetailData != null) {
            //InstanlyPreQualify
            $('input[name="creditAppType"][value="' + applicantDetailData.Applicant.ApplicantTypeId + '"]').trigger('click');
            $('input[name="creditAppType"]:not([value="' + applicantDetailData.Applicant.ApplicantTypeId + '"])').closest('label').addClass('no-print');
            $('#instantFirstName').val(applicantDetailData.Applicant.FirstName);
            $('#instantLastName').val(applicantDetailData.Applicant.LastName);
            $('#instantPhone').val(applicantDetailData.Applicant.PhoneNumber);
            $('#instantZip').val(applicantDetailData.Applicant.ZipCode);

            //Contact Info
            $('#contactStreetAddress').val(applicantDetailData.ContactInfo.CurrentAddress);
            $('#contactCity').val(applicantDetailData.ContactInfo.City);
            $('#contactState option[value="' + applicantDetailData.ContactInfo.State + '"]').attr('selected', 'selected');
            $('#contactZip').val(applicantDetailData.ContactInfo.ZipCode);

            $('#contactResidenceStatus option[value="' + applicantDetailData.ContactInfo.ResidenceStatus + '"]').attr('selected', 'selected');

            $('#contactMonthlyHousing').val(applicantDetailData.ContactInfo.MonthlyMortage);
            $('#contactResidenceMonths').val(applicantDetailData.ContactInfo.MonthAttended);
            $('#contactResidenceYears').val(applicantDetailData.ContactInfo.YearAttended);
            $('#contactPreviousStreetAddress').val(applicantDetailData.ContactInfo.PreviousAddress);
            $('#contactPreviousCity').val(applicantDetailData.ContactInfo.PreviousCity);

            $('#contactPreviousState option[value="' + applicantDetailData.ContactInfo.PreviousState + '"]').attr('selected', 'selected');

            $('#contactPreviousZip').val(applicantDetailData.ContactInfo.PreviousZipCode);
            $('input[name="creditAppParties"][value="' + applicantDetailData.ContactInfo.NumberOfApplicant + '"]').trigger('click');
            $('input[name="creditAppParties"]:not([value="' + applicantDetailData.ContactInfo.NumberOfApplicant + '"])').closest('label').addClass('no-print');
            $('#contactFirstName').val(applicantDetailData.ApplicantInfo.FirstName);
            $('#contactLastName').val(applicantDetailData.ApplicantInfo.LastName);
            $('#contactMiddleName').val(applicantDetailData.ApplicantInfo.MiddleName);
            $('#contactEmail').val(applicantDetailData.ApplicantInfo.Email);
            $('#contactPhone').val(applicantDetailData.ApplicantInfo.PhoneNumber);
            $('#contactSSN').val(applicantDetailData.ApplicantInfo.SocialSercurityNumber);

            $('#contactBirthDay option[value="' + applicantDetailData.ApplicantInfo.DateOfBirth + '"]').attr('selected','selected');
            $('#contactBirthMonth option[value="' + applicantDetailData.ApplicantInfo.MonthOfBirth + '"]').attr('selected', 'selected');
            $('#contactBirthYear option[value="' + applicantDetailData.ApplicantInfo.YearOfBirth + '"]').attr('selected', 'selected');


            $('#contactCurrentEmployer').val(applicantDetailData.EmploymentInfo.CurrentEmployer);
            $('#contactCurrentEmployerTelePhone').val(applicantDetailData.EmploymentInfo.CurrentEmployerTelephone);
            $('#contactJobTitle').val(applicantDetailData.EmploymentInfo.JobTitle);
            $('#contactMonthlyIncome').val(applicantDetailData.EmploymentInfo.MonthlyIncome);
            if (typeof applicantDetailData.EmploymentInfo.WorkingPeriod != undefined && applicantDetailData.EmploymentInfo.WorkingPeriod != '')
            {
                var year = applicantDetailData.EmploymentInfo.WorkingPeriod.split(';')[0];
                var month = applicantDetailData.EmploymentInfo.WorkingPeriod.split(';')[1];
                $('#contactEmployerDurationYear').val(year);
                $('#contactEmployerDurationMonth').val(month);
            }
            
            //$('input[name="contactEmployerDuration"]:not([value="' + applicantDetailData.EmploymentInfo.WorkingPeriod + '"])').closest('label').addClass('no-print');
            $('#contactPreviousEmployer').val(applicantDetailData.EmploymentInfo.PreviousEmployer);
            if (applicantDetailData.EmploymentInfo.WorkingPeriod != 1) {
                $('#contactPreviousEmployer').show();
            } else {
                $('#contactPreviousEmployer').hide();
            }

            $('#contactAdditionalIncome').val(applicantDetailData.EmploymentInfo.AdditionalIncomeSource);
            $('#contactAdditionalAmount').val(applicantDetailData.EmploymentInfo.AdditionalIncomeAmount);


            //Joined Applicant
            if (applicantDetailData.JoinedContactInfo != null && applicantDetailData.JoinedApplicantInfo != null && applicantDetailData.JoinedEmploymentInfo != null) {
                $('#joinApplicantCtr').show();
                $('#coContactStreetAddress').val(applicantDetailData.JoinedContactInfo.CurrentAddress);
                $('#coContactCity').val(applicantDetailData.JoinedContactInfo.City);
                $('#coContactState option[value="' + applicantDetailData.JoinedContactInfo.State + '"]').attr('selected', 'selected');
                $('#coContactZip').val(applicantDetailData.JoinedContactInfo.ZipCode);

                $('#coContactResidenceStatus option[value="' + applicantDetailData.JoinedContactInfo.ResidenceStatus + '"]').attr('selected', 'selected');

                $('#coContactMonthlyHousing').val(applicantDetailData.JoinedContactInfo.MonthlyMortage);
                $('#coContactResidenceMonths').val(applicantDetailData.JoinedContactInfo.MonthAttended);
                $('#coContactResidenceYears').val(applicantDetailData.JoinedContactInfo.YearAttended);
                $('#coContactPreviousStreetAddress').val(applicantDetailData.JoinedContactInfo.PreviousAddress);
                $('#coContactPreviousCity').val(applicantDetailData.JoinedContactInfo.PreviousCity);
                $('#coContactPreviousState option[value="' + applicantDetailData.JoinedContactInfo.PreviousState + '"]').attr('selected', 'selected');
                $('#coContactPreviousZip').val(applicantDetailData.JoinedContactInfo.PreviousZipCode);


                $('#coContactFirstName').val(applicantDetailData.JoinedApplicantInfo.FirstName);
                $('#coContactLastName').val(applicantDetailData.JoinedApplicantInfo.LastName);
                $('#coContactMiddleName').val(applicantDetailData.JoinedApplicantInfo.MiddleName);
                $('#coContactEmail').val(applicantDetailData.JoinedApplicantInfo.Email);
                $('#coContactPhone').val(applicantDetailData.JoinedApplicantInfo.PhoneNumber);
                $('#coContactSSN').val(applicantDetailData.JoinedApplicantInfo.SocialSercurityNumber);

                $('#coContactBirthDay option[value="' + applicantDetailData.JoinedApplicantInfo.DateOfBirth + '"]').attr('selected', 'selected');
                $('#coContactBirthMonth option[value="' + applicantDetailData.JoinedApplicantInfo.MonthOfBirth + '"]').attr('selected', 'selected');
                $('#coContactBirthYear option[value="' + applicantDetailData.JoinedApplicantInfo.YearOfBirth + '"]').attr('selected', 'selected');



                $('#coContactCurrentEmployer').val(applicantDetailData.JoinedEmploymentInfo.CurrentEmployer);
                $('#coContactMonthlyIncome').val(applicantDetailData.JoinedEmploymentInfo.MonthlyIncome);

                if (typeof applicantDetailData.JoinedEmploymentInfo.WorkingPeriod != undefined && applicantDetailData.JoinedEmploymentInfo.WorkingPeriod != '') {
                    var year = applicantDetailData.JoinedEmploymentInfo.WorkingPeriod.split(';')[0];
                    var month = applicantDetailData.JoinedEmploymentInfo.WorkingPeriod.split(';')[1];
                    $("#coContactEmployerDurationYear").val(year);
                    $("#coContactEmployerDurationMonth").val(month);
                }

                $('#coContactPreviousEmployer').val(applicantDetailData.JoinedEmploymentInfo.PreviousEmployer);
                if (applicantDetailData.JoinedEmploymentInfo.WorkingPeriod != 1) {
                    $('#coContactPreviousEmployer').show();
                } else {
                    $('#coContactPreviousEmployer').hide();
                }
                $('#coContactAdditionalIncome').val(applicantDetailData.JoinedEmploymentInfo.AdditionalIncomeSource);
                $('#coContactAdditionalAmount').val(applicantDetailData.JoinedEmploymentInfo.AdditionalIncomeAmount);
            } else {
                $('#joinApplicantCtr').hide();
            }
            applicantDetail.disableControls();
        }
    },
    archiveApplication: function (data, method) {
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/FinanceController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {

                    } else {
                        alert("Something went wrong when updating this program.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    getDataOfApplicant: function () {
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/FinanceController.ashx?method=' + 'GetApplicanttById' + '&ApplicantId=' + applicantDetail.applicantId(),
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 180000,
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    if (dataResult.applicantDetail != null) {
                        var applicantDetailData = dataResult.applicantDetail;
                        tempData = applicantDetailData;
                        applicantDetail.hookUpDataToUI(applicantDetailData);
                        $('#IsArchived').val(applicantDetailData.Applicant.IsArchived);
                        if (applicantDetailData.Applicant.IsArchived == false) {
                            $('#btnArchived').val('Archive Application');
                        } else {
                            $('#btnArchived').val('De-Archive Application');
                        }
                    }
                }
            },
            error: function () {
                alert("Error when contacting to server, please try again later!");
            }
        });
    }
};