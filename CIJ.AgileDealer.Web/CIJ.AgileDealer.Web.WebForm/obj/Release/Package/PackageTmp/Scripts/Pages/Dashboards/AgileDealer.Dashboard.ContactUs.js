﻿$(document).ready(function () {
    //contactUsDashboard.initialize();
});

var contactUsTable;

var contactUsDashboard = {
    self: 'contactUsDashboard',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadContactUsDashboard';
    },
    initialize: function () {
        contactUsDashboard.registerEvents();
        contactUsDashboard.loadData();
    },
    registerEvents: function () {
        $('#tableContactUs').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = contactUsTable.row(row).data()[0];
            location.href = siteMaster.currentUrl() + '/Admin/ContactUsDetail/' + id;
        });
    },
    loadData: function() {
        contactUsTable = $('#tableContactUs').DataTable({
            ajax: contactUsDashboard.url(),
            //stateSave: true,
            //stateDuration: 60 * 60,
            language: {
                emptyTable: "No record found!"
            },
            columns: [
                { sName: 'CustomerQuestionId', "visible": false },
                { sName: 'FirstName' },
                { sName: 'LastName' },
                { sName: 'Email' },
                { sName: 'Phone' },
                { sName: 'DateCreated' },
                {
                    sName: 'CustomerQuestionId',
                    defaultContent: '<a href="" class="editor_edit">Detail</a>'
                }
            ],
            order: [[5, "desc"]]
        });
    }
};