﻿$(document).ready(function () {
    scheduleServiceDetail.initialize();
});

var scheduleServiceDetail = {
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/ScheduleServiceController.ashx?method=' + 'GetScheduleServiceById';
    },
    initialize: function () {
        scheduleServiceDetail.loadContactUsData();
        scheduleServiceDetail.registerEvent();
    },
    registerEvent: function () {
        $('#btnBack').click(function () {
            parent.history.back();
            return false;
        });
    },
    mapDataForscheduleServiceDetail: function (data) {
        if (typeof data != "undefined" && data != null) {
            $('#serviceDate').val(data.ScheduleDateString);
            $('#weekday-hours').val(data.WeeklyHour);
            $('#saturday-hours').val(data.SaturdayHour);
            $('#contactFirstName').val(data.FirstName);
            $('#contactLastName').val(data.LastName);
            $('#contactPhone').val(data.PhoneNumber);
            $('#contactEmail').val(data.Email);
            $('#vehicleYear').val(data.Year);
            $('#vehicleMake').val(data.Make);
            $('#vehicleModel').val(data.Model);
            $('#vehicleMileage').val(data.Mileage);
            if (typeof data.ServiceRequestList != 'undefined' &&
                data.ServiceRequestList != null &&
                data.ServiceRequestList.length > 0) {
                for (var i = 0; i < data.ServiceRequestList.length; i++) {
                    $('#serviceRequested').find('input[type=checkbox][value="' + data.ServiceRequestList[i] + '"]').prop('checked','checked');
                }
            }

            if (typeof data.CurrentIssueList != 'undefined' &&
                data.CurrentIssueList != null &&
                data.CurrentIssueList.length > 0) {
                for (var i = 0; i < data.CurrentIssueList.length; i++) {
                    $('#currentIssue').find('input[type=checkbox][value="' + data.CurrentIssueList[i] + '"]').prop('checked', 'checked');
                }
            }
            $('#contactMessage').val(data.AdditionalInformation);
            $('input').prop('readonly', 'readonly');
            $('input[type=checkbox]').prop('disabled', 'disabled');
            $('label.btn').attr('disabled', 'disabled');
            $('select').attr('disabled', 'disabled');
        }
    },
    getCheckboxesData: function (checkbox) {
        var result = [];
        if (typeof checkbox !== "undefined" && checkbox !== null) {
            for (var i = 0; i < checkbox.length; i++) {
                result.push(checkbox[i].value);
            }
        }
        return result;
    },
    loadContactUsData: function () {
        var data = $('[id$="scheduleServiceId"]').val();
        if (typeof data != "undefined" && data != null) {
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: scheduleServiceDetail.url(),
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        scheduleServiceDetail.mapDataForscheduleServiceDetail(dataResult.aaData);
                    } else {
                        alert("Something went wrong when getting this customer info.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    }
};