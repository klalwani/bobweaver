﻿$(document).ready(function () {
    vcpObj.initialize();
    
});

var vcpObj = {
    initialize: function() {
        vcpObj.registerEvents();
        vcpObj.setDefaultTab();
        //vcpObj.setTabVisible();
    },
    registerEvents: function() {
        $("#tabs li").click(function () {
            var that = $(this);
            $('[id$="CurrentTab"]').val(that.attr('id'));
            vcpObj.sessionDefaultTabSession('CurrentTab', that.attr('id'));
        });
    },
    setTabVisible: function() {
        var currentModel = $('[id$="CurrentModel"]').val();
        var currentType = $('[id$="CurrentType"]').val();
        if (currentType == 'new' && currentModel == 'F-150') {
            $('#usedTag').hide();
            $('#certifiedTag').hide();
        } else {
            $('#usedTag').show();
            $('#certifiedTag').show();
        }
    },
    sessionDefaultTabSession: function(sessionName,sessionValue) {
        var method = 'SetVcpCurrentTab';
        var data = { sessionName: sessionName, sessionValue: sessionValue };
        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/SiteMasterController.ashx?method=' + method,
            type: 'POST',
            async: true,
            timeout:30000,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                }
            }
        });
    },
    setDefaultTab: function() {
        var currentTab = $('[id$="CurrentTab"]').val();
        if (currentTab.indexOf('reviewTab') != -1) {
            $('[id$="reviewTab"] a').trigger('click');
        }
        else if (currentTab.indexOf('inventoryTab') != -1) {
            $('[id$="inventoryTab"] a').trigger('click');
        }
    }
};