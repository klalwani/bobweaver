﻿$.fn.CustomDataTable = function (options) {
    var rootUrl = siteMaster.currentUrl();

    var mask = {
        css: {
            backgroundColor: 'transparent',
            border: 'none',
            zIndex: 10002
        },
        message: '<img width="50" height="50" src="' + rootUrl + 'Content/Images/new-spinner.gif" />'
    };
    var defaults = {
        leftFixedColumns: 2,
        bServerSide: true,
        iDisplayLength: 25,
        bProcessing: true,
        sPaginationType: "full_numbers",
        bFilter: false,
        bInfo: false,
        sAjaxDataProp: "aaData",
        bAutoWidth: false,
        callback: null,
        callBackAfterSuccessGetData: null,
        initComplete: null,
        isDirty: null,
        resetCheckIsDirty: null,
        orderType: 'asc',
        isSearch: false,
        textSearch: '',
        isCustomParam: null,
        customParam: null,
        isFixColumn: null,
        customPageSize: null,
        isCustomPageSize: null,
        clearCustomParam: null,
        sScrollY: "500px",
        bLengthChange: true,
        bPaginate: true
    };

    options = $.extend(defaults, options);
    var table = $(this).dataTable({
        "bServerSide": options.bServerSide,
        "iTabIndex": -1,
        "iDisplayLength": options.iDisplayLength,
        "bLengthChange": options.bLengthChange,
        "bPaginate": options.bPaginate,
        "sPaginationType": options.sPaginationType,
        "bFilter": options.bFilter,
        "bInfo": options.bInfo,
        "sScrollY": options.sScrollY,
        "sScrollX": "100%",
        "sScrollXInner": "140px",
        "sAjaxSource": options.url,
        "sAjaxDataProp": options.sAjaxDataProp,
        "fnServerData": function(url, data, callback, oSettings) {
            if (typeof options.fnPreProcess != 'undefined' && typeof options.fnPreProcess == 'function') {
                options.fnPreProcess(url, data, callback, oSettings);
            }
            var that = this;
            $.ajax({
                url: url,
                async: true,
                data: data,
                success: function (data) {
                    if (typeof callback == 'function') {
                        var oSettings = that.dataTable().fnSettings();
                        if (data.iDisplayStart != undefined) {
                            oSettings._iDisplayStart = data.iDisplayStart;
                        }
                        if (typeof options.callBackAfterSuccessGetData == 'function') {
                            data = options.callBackAfterSuccessGetData(data);
                        }
                        callback(data);
                    }
                },
                dataType: "json",
                type: "POST",
                cache: false,
                timeout: 180000,
                error: function (xhr, status, err) {
                    $.unblockUI(mask);
                    mask = null;
                    if (status == 'timeout') {
                        //jMessageBox({
                        //    message: "Request timeOut",
                        //    title: "Error",
                        //    type: "error"
                        //});
                    } else {
                        //jMessageBox({
                        //    message: "Error while connect to server. Please contact the administrator for details.",
                        //    title: "Error",
                        //    type: "error"
                        //});
                    }
                }
            });
        },
        "fnInitComplete": function () {
            if (typeof options.initComplete == 'function') {
                options.initComplete(this);
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "Prev"
            },
            "sEmptyTable": "There is no record"
        },
        "bAutoWidth": options.bAutoWidth,
        "aoColumns": options.aoColumns,
    });

};