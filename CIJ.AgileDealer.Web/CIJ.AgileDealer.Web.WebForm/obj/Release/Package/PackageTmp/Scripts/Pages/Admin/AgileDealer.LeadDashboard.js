﻿$(document).ready(function () {
    leadDashboard.initialize();
});

var leadDashboard = {
    self: 'leadDashboard',
    initialize: function () {
        leadDashboard.registerEvents();
        applicantDashboard.initialize();
        contactUsDashboard.initialize();
        scheduleServiceDashboard.initialize();
        checkAvailabilityDashboard.initialize();
    },
    registerEvents: function () {

    },
}