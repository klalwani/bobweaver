﻿var FinanceJoinApplicant = {
    getJoinApplicantInformation: function () {
        var postData = {
            contactInfo: {
                CurrentAddress: '',
                City: '',
                State: '',
                ZipCode: '',
                ResidenceStatus: '',
                MonthlyMortage: '',
                MonthAttended: '',
                YearAttended: '',
                PreviousAddress: '',
                PreviousCity: '',
                PreviousState: '',
                PreviousZipCode: '',
                IsCorrespondingContactInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: ''
            },
            applicantInfo: {
                FirstName: '',
                LastName: '',
                MiddleName: '',
                Email: '',
                PhoneNumber: '',
                SocialSercurityNumber: '',
                DateOfBirth: '',
                MonthOfBirth: '',
                YearOfBirth: '',
                IsCorrespondingApplicantInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: ''
            },
            employmentInfo: {
                CurrentEmployer: '',
                MonthlyIncome: '',
                WorkingPeriod: '',
                PreviousEmployer: '',
                AdditionalIncomeSource: '',
                AdditionalIncomeAmount: '',
                IsCorrespondingEmploymentInfo: '',
                NumberOfApplicant: '',
                IsActive: '',
                ApplicantId: ''
            }

        };
        postData.contactInfo.CurrentAddress = $('#coContactStreetAddress').val();
        postData.contactInfo.City = $('#coContactCity').val();
        postData.contactInfo.State = $('#coContactState').val();
        postData.contactInfo.ZipCode = $('#coContactZip').val();
        postData.contactInfo.ResidenceStatus = $('#coContactResidenceStatus').val();
        postData.contactInfo.MonthlyMortage = $('#coContactMonthlyHousing').val();
        postData.contactInfo.MonthAttended = $('#coContactResidenceMonths').val();
        postData.contactInfo.YearAttended = $('#coContactResidenceYears').val();
        postData.contactInfo.PreviousAddress = $('#coContactPreviousStreetAddress').val();
        postData.contactInfo.PreviousCity = $('#coContactPreviousCity').val();
        postData.contactInfo.PreviousState = $('#coContactPreviousState').val();
        postData.contactInfo.PreviousZipCode = $('#coContactPreviousZip').val();
        postData.contactInfo.IsCorrespondingContactInfo = false;
        postData.contactInfo.NumberOfApplicant = 2;
        postData.contactInfo.IsActive = true;
        postData.contactInfo.ApplicantId = $('#applicantId').val();


        postData.applicantInfo.FirstName = $('#coContactFirstName').val();
        postData.applicantInfo.LastName = $('#coContactLastName').val();
        postData.applicantInfo.MiddleName = $('#coContactMiddleName').val();
        postData.applicantInfo.Email = $('#coContactEmail').val();
        postData.applicantInfo.PhoneNumber = $('#coContactPhone').val();
        postData.applicantInfo.SocialSercurityNumber = $('#coContactSSN').val();
        postData.applicantInfo.DateOfBirth = $('#coContactBirthDay').val();
        postData.applicantInfo.MonthOfBirth = $('#coContactBirthMonth').val();
        postData.applicantInfo.YearOfBirth = $('#coContactBirthYear').val();
        postData.applicantInfo.IsCorrespondingApplicantInfo = false;
        postData.applicantInfo.NumberOfApplicant = 2;
        postData.applicantInfo.IsActive = true;
        postData.applicantInfo.ApplicantId = $('#applicantId').val();

        postData.employmentInfo.CurrentEmployer = $('#coContactCurrentEmployer').val();
        postData.employmentInfo.MonthlyIncome = $('#coContactMonthlyIncome').val();
        postData.employmentInfo.WorkingPeriod = $('#coContactEmployerDurationYear').val() + ';' + $('#coContactEmployerDurationMonth').val();
        postData.employmentInfo.PreviousEmployer = $('#coContactPreviousEmployer').val();


        if (typeof $('#coContactAdditionalIncome').val() == undefined || $('#coContactAdditionalIncome').val() == '')
            postData.employmentInfo.AdditionalIncomeSource = '';
        else
            postData.employmentInfo.AdditionalIncomeSource = $('#coContactAdditionalIncome').val();

        if (typeof $('#coContactAdditionalAmount').val() == undefined || $('#coContactAdditionalAmount').val() == '')
            postData.employmentInfo.AdditionalIncomeAmount = 0;
        else
            postData.employmentInfo.AdditionalIncomeAmount = $('#coContactAdditionalAmount').val();


        postData.employmentInfo.IsCorrespondingEmploymentInfo = false;
        postData.employmentInfo.NumberOfApplicant = 2;
        postData.employmentInfo.IsActive = true;
        postData.employmentInfo.ApplicantId = $('#applicantId').val();

        return postData;
    }
};