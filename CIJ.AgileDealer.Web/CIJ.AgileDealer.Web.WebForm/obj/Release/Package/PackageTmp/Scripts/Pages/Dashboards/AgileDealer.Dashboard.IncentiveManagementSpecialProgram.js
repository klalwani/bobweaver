﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};
function CommaFormatted(nStr) {
    if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    return nStr;
}

$(document).ready(function () {
    incentiveManagementSpecialProgram.initialize();
});

var incentiveManagementTableSpecialProgram;

var incentiveManagementSpecialProgram = {
    self: 'incentiveManagementDashboardSpecialProgram',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadIncentiveManagementDashboardSpecialPrograms';
    },
    programData: {},
    initialize: function () {
        incentiveManagementSpecialProgram.registerEvents();
        incentiveManagementSpecialProgram.loadMasterData();
        incentiveManagementSpecialProgram.loadData();
        
    },
    registerEvents: function () {

        $('#priceDateSpecialPrograms').on('click', 'label', null, function (e) {
            var that = $(this);
            var toggleState = that.data('toggle');
            var toggleControl = that.attr('href');

            if (toggleState == 'in') {
                $('#dateRangeFieldSpecialPrograms').removeClass('collapse');
                // agilevehicle.setRequireFieldForDateRange(true);
            } else {
                $('#dateRangeFieldSpecialPrograms').addClass('collapse');
                //agilevehicle.setRequireFieldForDateRange(false);
            }
        });

        $('#incentiveManagementDashboardSpecialProgram').on('click', '.programEdit,.itemActive,.itemHidden', function () {

            var that = $(this);
            var thisRow = that.closest('tr');
            $('#incentiveEditSpecialProgramControl').modal('show');
            $("#txtVinSpecialProgram").val('');
            $("#ddlModelSpecialProgram").val('0');
            var rowData = incentiveManagementTableSpecialProgram.row(thisRow).data();
            incentiveManagementSpecialProgram.setProgramData(rowData, that);
        });
        $('#btnAddNewSpecialProgramIncentive').on('click', function () {
            
          
           
            $('#incentiveEditSpecialProgramControl').modal('show');

            $('#txtDateStart').datepicker({
                minDate: 0
            });

            $('#txtDateEnd').datepicker({
                minDate: 0,
                defaultDate: "+1M"
            });
           
            $("#txtVinSpecialProgram").val('');
            $("#ddlModelSpecialProgram").val('0');
            $("#ddlEnginesSpecialProgram").val('0');
            $("#ddlVinSpecialProgram").val('0');
            $('#txtDateStart').val('');
            $('#txtDateEnd').val('');
            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").text("Select Vin(s)");
            $("#excludeVinSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Vin(s)");
            $("#engineSectionSpecialProgram .ms-options-wrap button").text("Select Engine(s)");
            $("#engineSectionSpecialProgram .ms-options-wrap button").attr("title", "Select Engine(s)");
            $(".ms-options ul li").removeClass("selected");
            $(".ms-options ul li input").addClass("focused");
            $(".ms-options ul li input").attr('checked', false);
            $(".ms-options ul li input").prop('checked', false);

            var d = new Date();
            var StartDate = getFormattedDate(d);
            var EndDate = getFormattedDate(new Date(d.getFullYear(), d.getMonth() + 1, d.getDate()));
            $('#txtDateStart').val(EndDate);
            $('#txtDateEnd').val(StartDate);

            var data = {
                name: "",
                type: "Cash",
                isActive: false,
                amount: "",
                disclaimer: "",
                updateFor: 1, // All Vehicles
                updateForVin: '',
                updateForModelId: 0,
                id: 0,
                excludeenginecodes: "",
                excludeVINs: "",
                excludeVINArrayData: "",
                startdate: StartDate,
                enddate: EndDate,
                hassetDateRange: false
            };
            data.status = 'Active';
            data.displaySection = "";
            data.translatedName = "";
            data.position = "";
            data.updateFor = UPDATEFOR.ALL;
            excludeenginecodes = "";
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
            incentiveTranslatedUpdateSpecialProgram.programData = data;

        });

    },
    loadMasterData: function () {
        var modelListSource = $('[id$="ModelList"]').val();
        var models = $.parseJSON(modelListSource);
        for (var i = 0; i < models.length; i++) {
            var model = models[i];
            var option = '<option value="' + model.Id + '">' + model.Name + '</option>';
            $('#ddlModelSpecialProgram').append(option);
        }

        var engineListSource = $('[id$="EngineList"]').val();
        var engines = $.parseJSON(engineListSource);
        for (var i = 0; i < engines.length; i++) {
            var engine = engines[i];
            var option = '<option value="' + engine.Id + '">' + engine.Name + '</option>';
            $('#ddlEnginesSpecialProgram').append(option);
        }

        var vinListSource = $('[id$="ExcludeVinList"]').val();
        var vins = $.parseJSON(vinListSource);
        for (var i = 0; i < vins.length; i++) {
            var vin = vins[i];
            var option = '<option value="' + vin.Name + '">' + vin.Name + '</option>';
            $('#ddlVinSpecialProgram').append(option);
        }
    },

    setProgramData: function (rowData, row) {
        var isActive = 'Active';
        var vin = '';
        var modelid = '';

        var data = {
            //displaySection: rowData[7],
            //status: rowData[1],
            //position: rowData[8],
            //translatedName: rowData[2],
            name: rowData[2],
            type: rowData[3],
            isActive: false,
            amount: rowData[4],
            disclaimer: rowData[10],
            updateFor: 1, // All Vehicles
            updateForVin: '',
            updateForModelId: 0,
            id: rowData[13],
            excludeVINs: rowData[6],
            excludeenginecodes: rowData[9],
            excludeVINArrayData: rowData[6],
            startdate: rowData[7],
            enddate: rowData[8],
            hassetDateRange: rowData[14]
        };
        data.displaySection = row.attr('displaySection');
        data.translatedName = row.attr('translatedName');
        data.position = row.attr('position');

        if (row.is('.itemActive')) {
            data.status = 'Active';
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
        }
        else if (row.is('.itemHidden')) {
            data.status = 'Hidden';
            incentiveTranslatedUpdateSpecialProgram.isResetAll = false;
        } else {
            incentiveTranslatedUpdateSpecialProgram.isResetAll = true;
        }

        if (row.attr('isUpdateFor') == 'all') {
            data.updateFor = UPDATEFOR.ALL;
        }
        else if (row.attr('isUpdateFor') == 'vinSection') {
            data.updateFor = UPDATEFOR.VIN;
            data.updateForVin = row.attr('vin');;
        }
        else if (row.attr('isUpdateFor') == 'modelSection') {
            data.updateFor = UPDATEFOR.MODEL;
            data.updateForModelId = row.attr('modelid');
        }


        incentiveTranslatedUpdateSpecialProgram.programData = data;
    },
    setOverrideExperationButtonSpecialPrograms: function (name) {
        $('#priceDateSpecialPrograms').find('label[name="' + name + '"]').click();
    },
    loadData: function () {
        var data = $('#ddlLanguageSpecialProgram').val();
        incentiveManagementTableSpecialProgram = $('#tableIncentiveManagementSpecialProgram').DataTable({
            ajax: {
                url: incentiveManagementSpecialProgram.url(),
                type: 'post',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                data: function () {
                    return JSON.stringify(data);
                }
            },
            columns: [
                { sName: 'DisplaySection', width: "10%" },
                { sName: 'TranslatedName', width: "15%", sClass: "textLeft" },
                 { sName: 'ProgramName', width: "15%", sClass: "textLeft" },
                { sName: 'ProgramType', width: "5%" },
                { sName: 'Amount', sClass: 'breakText textLeft', width: "10%" },
                 { sName: 'ExcludeEngingCodeUserFriendlyName', width: "15%" },
                  { sName: 'ExcludeVINs', width: "15%" },
                   { sName: 'StartDate', width: "10%" },
                    { sName: 'EndDate', width: "10%" },
                   { sName: 'excludeenginecodes', "visible": false },
                 { sName: 'Disclaimer', "visible": false },


                { sName: 'DisplaySectionId', "visible": false },
                { sName: 'Position', "visible": false },
                { sName: 'Id', "visible": false },
                { sName: 'HasSetDateRange', "visible": false }
            ],
            "initComplete": function (settings, json) {
                incentiveTranslatedUpdateSpecialProgram.currentTable = incentiveManagementTableSpecialProgram;
            }
        });

    }
};

function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}
