﻿var UPDATEFOR = {
    ALL: 1,
    VIN: 2,
    MODEL: 3,
    MAKE: 4
};

$(document).ready(function () {
    incentiveTranslatedUpdate.initialize();
});


var incentiveTranslatedUpdate = {
    initialize: function () {
        incentiveTranslatedUpdate.registerEvents();
    },
    isResetAll: false,
    currentTable: {},
    registerEvents: function () {
        $(document).on('click', '#btnSaveChange', function () {
            incentiveTranslatedUpdate.prepareDataForSubmitting();
            incentiveTranslatedUpdate.submitData();
        });

        $(document).on('click', '#btnReset', function () {
            $('#modal-confirm-reset').modal('show');
        });

        $(document).on('click', '#btnConfirmReset', function () {
            incentiveTranslatedUpdate.prepareDataForSubmitting();
            incentiveTranslatedUpdate.resetIncentiveTranslated();
            $('#modal-confirm-reset').modal('toggle');
        });

        $(document).on('click', '#btnCancelReset', function () {
            $('#modal-confirm-reset').modal('toggle');
        });

        $(document).on('click', '#updateFor label', function () {
            var that = $(this);
            var input = that.find('input');
            var value = input.val();
            var target = input.data('target');
            if (target == 'all') {
                $('#vinSection').removeClass('in').addClass('collapse');
                $('#modelSection').removeClass('in').addClass('collapse');
            }
            else if (target == 'vinSection') {
                $('#vinSection').removeClass('collapse').addClass('in');
                $('#modelSection').removeClass('in').addClass('collapse');
            }
            else if (target == 'modelSection') {
                $('#vinSection').removeClass('in').addClass('collapse');
                $('#modelSection').removeClass('collapse').addClass('in');
            }
        });

        $(document).on('shown.bs.modal', '#incentiveEditControl', function () {
            if (typeof incentiveTranslatedUpdate.programData != "undefined" && incentiveTranslatedUpdate.programData != null) {
                $('#lblProgramName').text(incentiveTranslatedUpdate.programData.name);
                if (incentiveTranslatedUpdate.programData.displaySection != '0' && incentiveTranslatedUpdate.programData.displaySection != '')
                    $('#ddlDisplaySection').val(incentiveTranslatedUpdate.programData.displaySection);
                $('input[name="chkStatus"][value="' + incentiveTranslatedUpdate.programData.status + '"]').click();
                $('#txtPosition').val(incentiveTranslatedUpdate.programData.position);
                $('#txtTranslatedName').val(incentiveTranslatedUpdate.programData.translatedName);

                $('#updateFor label input[value="' + incentiveTranslatedUpdate.programData.updateFor + '"]').click();
                if (incentiveTranslatedUpdate.programData.updateFor == UPDATEFOR.VIN) {
                    $('#txtVin').val(incentiveTranslatedUpdate.programData.updateForVin);
                }
                else if (incentiveTranslatedUpdate.programData.updateFor == UPDATEFOR.MODEL) {
                    $('#ddlModel').val(incentiveTranslatedUpdate.programData.updateForModelId);
                }
            }
        });
    },
    prepareDataForSubmitting: function () {
        incentiveTranslatedUpdate.programData.displaySection = $('#ddlDisplaySection').val();
        incentiveTranslatedUpdate.programData.position = $('#txtPosition').val();
        incentiveTranslatedUpdate.programData.translatedName = $('#txtTranslatedName').val();
        if ($('input[name="chkStatus"]:checked')[0].value == "Hidden") {
            incentiveTranslatedUpdate.programData.isActive = false;
        } else {
            incentiveTranslatedUpdate.programData.isActive = true;
        }

        incentiveTranslatedUpdate.programData.updateFor = $('#updateFor label.active input').val();
        incentiveTranslatedUpdate.programData.updateForVin = $('#txtVin').val();
        incentiveTranslatedUpdate.programData.updateForModelId = $('#ddlModel').val();
        incentiveTranslatedUpdate.programData.isResetAll = incentiveTranslatedUpdate.isResetAll;
    },
    submitData: function () {
        var method = 'UpdateIncentiveTranslated';

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdate.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdate.currentTable != 'undefined' && incentiveTranslatedUpdate.currentTable != null)
                        $('.modal').modal('hide');
                        incentiveTranslatedUpdate.currentTable.ajax.reload();
                }
            }
        });
    },
    resetIncentiveTranslated: function () {
        var method = 'ResetIncentiveTranslated';
        var isResetAll = false;

        var result = true;
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 30000,
            data: JSON.stringify(incentiveTranslatedUpdate.programData),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    result = true;
                    if (typeof incentiveTranslatedUpdate.currentTable != 'undefined' && incentiveTranslatedUpdate.currentTable != null)
                        $('.modal').modal('hide');
                        incentiveTranslatedUpdate.currentTable.ajax.reload();
                }
            }
        });
    }
};