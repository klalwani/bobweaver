﻿$(document).ready(function() {
    blog.init();
});
var blog = {
    isLogin : false,
    init: function () {
        $("#divChat").hide();
        $('#divLogin').hide();
        var hub = $.connection.agileDealerHub;
        $.connection.hub
          .start()
          .done(function () {
            hub.server.isLogin();
          });
        blog.registerEvents(hub);
        blog.registerClientCall(hub);
    },
    adduser: function (id, name, hub) {
        $("#divChat").show();
        var code = $('<div class="user"' + '><a href="#" id=' + id + '>' + name + '</a></div>');
        code.click(function () {
            var connectionId = $('#hdConnectionId').val();
            var ctrId = 'private_' + id;
            if ($('#' + ctrId).length == 0 && connectionId!=id) {
                blog.createPrivateChat(name, id,hub);
            }
        });
        $('#divChat').find(".list-user").append(code);
    },
    createPrivateChat:function(name,id,hub) {
        var user = [{ UserName: "Talk to " + name, Id: 'private_' + id }];
        $("#chatPrivateTemplate").tmpl(user).prependTo("#container");
        var ctrId = $("#" + 'private_' + id);
        ctrId.draggable();
        ctrId.find("#btnSendMessage").click(function () {
            var text = $('#txtSendMessage').val();
            blog.sendPrivateMessage(text, hub,id);
            $('#txtSendMessage').val('');
        });
        ctrId.find('.close').click(function() {
            ctrId.remove();
        });
        $('#txtSendMessage').keydown(function (e) {
            var that = ($(this));
            if (e.keyCode == 13) {
                var text = that.val();
                blog.sendPrivateMessage(text,hub,id);
                that.val('');
            }
        });
    },
    sendPrivateMessage:function(text,hub,id) {
        if (text != '') {
            hub.server.sendPrivateMessage(id, text);
        }
    },
    registerClientCall:function(hub) {
        hub.client.login = function (connectionId) {
            $('#hdConnectionId').val(connectionId);
            $('#connectionId').val(connectionId);
            $('#divLogin').show();
        }
        hub.client.onNewUserConnected = function (id, name) {
            if (!blog.isLogin) return;
            $("#divChat").show();
            blog.adduser(id, name,hub);
        }
        hub.client.onConnected = function (users, userName, connectionId) {
            $('#header').text('Welcome ' + userName + ' to Chat Room');
            blog.isLogin = true;
            $('#hdUserName').val(userName);
            $('#hdConnectionId').val(connectionId);
            for (var i = 0; i < users.length; i++) {
                blog.adduser(users[i].ConnectionId, users[i].UserName,hub);
            }
        }
        hub.client.sendPrivateMessage = function (windowId, fromUserName, message) {
            var ctrId = 'private_' + windowId;
            if ($('#' + ctrId).length == 0) {
                blog.createPrivateChat(fromUserName, windowId,hub);
            }
            $('#' + ctrId).find("#divMessage").append('<div><span>' + fromUserName + ': </span>'+ message +'</div>');
        }
        hub.client.sendToAll = function(userName,message) {
            $('#divChat').find("#divAllMessage").append('<div><span>' + userName + ': </span>' + message + '</div>');
        }
    },
    registerEvents : function(hub) {
        $('#startButton').click(function (e) {
            var text = $('#txtName').val();
            if (text != "") {
                $('#header').text('Welcome ' + name + ' to Chat Room');
                var connectionId = $('#hdConnectionId').val();
                $('#hdUserName').val(text);
                hub.server.connectToChatRoom(text, connectionId);
                $('#divLogin').hide();
                $("#divChat").show();
                $('#txtName').val('');
                blog.isLogin = true;
            }
        });
        $('#btnSendToAll').click(function() {
            var text = $('#txtSendToAll').val();
            hub.server.sendToAll(text);
            $('#txtSendToAll').val('');
        });
    }
};