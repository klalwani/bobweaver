﻿$(document).ready(function () {
    blogUpdate.initialize();
    //AgileDealerValidation.InitialValidationEvent();
});
var isUpdateNewImage = false;
var blogTemp;
var tags;
var categories;
var languages;
var editorInitialized = false;

var blogUpdate = {
    self: 'blog-update',
    currentEmployee: function () {
        return blogTemp;
    },
    initialize: function () {
        blogUpdate.populateLanguages();
        blogUpdate.populateTagAndCategory();
        blogUpdate.registerEvent();
    },
    hookupEditor: function () {
        if (!editorInitialized) {
            editorInitialized = true;
            CKEDITOR.replace('postText');
            CKEDITOR.editorConfig = function (config) {
                config.height = 300;
                config.toolbar = [
                    {
                        name: 'clipboard',
                        items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                    },
                    { name: 'editing', items: ['Scayt'] },
                    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
                    { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar'] },
                    { name: 'tools', items: ['Maximize'] },
                    { name: 'document', items: ['Source'] },
                    '/',
                    {
                        name: 'basicstyles',
                        items: [
                            'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                        ]
                    },
                    {
                        name: 'paragraph',
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
                    },
                    { name: 'styles', items: ['Styles', 'Format'] },
                    { name: 'about', items: ['About'] }
                ];
            };
        }
    },
    setDefaultButtonText: function () {
        $('#button1Text').val('Inventory');
        $('#button1Path').val('/vcp/new/make/{new value here}');
        $('#button2Text').val('Contact');
        $('#button2Path').val('/contactus');
    },
    populateTagAndCategory: function () {
        tags = $.parseJSON($('[id$="BlogTag"]').val());
        categories = $.parseJSON($('[id$="BlogCategory"]').val());
        if (typeof categories != "undefined" && categories != null && categories.length > 0) {
            var catoptions;
            $.each(categories,
                function (i, item) {
                    catoptions += "<option value='" + item.BlogCategoryId + "'>" + item.CategoryText + "</option>";
                });
            $('#blogCategoriesddl').append(catoptions);
            $('#blogCategoriesddl')
                .multiselect({
                    // text to use in dummy input
                    placeholder: 'Select Categories',
                    // how many columns should be use to show options
                    columns: 2,
                    // include option search box  
                    search: false,
                    // search filter options
                    searchOptions: {
                        default: 'Search', // search input placeholder text
                        showOptGroups: false // show option group titles if no options remaining
                    },
                    // add select all option
                    selectAll: false,
                    // select entire optgroup  
                    selectGroup: false,
                    // minimum height of option overlay
                    minHeight: 200,
                    // maximum height of option overlay
                    maxHeight: null,
                    // display the checkbox to the user
                    showCheckbox: true,
                    // options for jquery.actual
                    jqActualOpts: {},
                    // maximum width of option overlay (or selector)
                    maxWidth: null,
                    // minimum number of items that can be selected
                    minSelect: false,
                    // maximum number of items that can be selected
                    maxSelect: false,
                    onOptionClick: function () {
                        var selectItems = $('#blogCategoriesddl').val();
                        if (typeof selectItems != "undefined" && selectItems != null && selectItems.length > 0) {
                            $('#blogCategoriesddl').removeClass('input-validation-error').addClass('valid');
                            $('#blogCategoriesddl').parent().find('.ms-options-wrap').css('border', 'none');
                            $('#blogCategoriesddl').parent().find('.ms-options-wrap').find('button').attr('style', 'border-color: none');
                        } else {
                            $('#blogCategoriesddl').addClass('input-validation-error').removeClass('valid');
                            $('#blogCategoriesddl').find('.ms-options-wrap').parent().find('button').attr('style', 'border-color: red !important;');
                            $('#blogCategoriesddl').find('.ms-options-wrap').parent().css('border', '1px solid');
                        }
                    }
                });
        }

        if (typeof tags != "undefined" && tags != null && tags.length > 0) {
            var tagoptions;
            $.each(tags,
                function (i, item) {
                    tagoptions += "<option value='" + item.BlogTagId + "'>" + item.TagText + "</option>";
                });
            $('#blogTagsddl').append(tagoptions);
            $('#blogTagsddl')
                .multiselect({
                    // text to use in dummy input
                    placeholder: 'Select Tags',
                    // how many columns should be use to show options
                    columns: 2,
                    // include option search box  
                    search: false,
                    // search filter options
                    searchOptions: {
                        default: 'Search', // search input placeholder text
                        showOptGroups: false // show option group titles if no options remaining
                    },
                    // add select all option
                    selectAll: false,
                    // select entire optgroup  
                    selectGroup: false,
                    // minimum height of option overlay
                    minHeight: 200,
                    // maximum height of option overlay
                    maxHeight: null,
                    // display the checkbox to the user
                    showCheckbox: true,
                    // options for jquery.actual
                    jqActualOpts: {},
                    // maximum width of option overlay (or selector)
                    maxWidth: null,
                    // minimum number of items that can be selected
                    minSelect: false,
                    // maximum number of items that can be selected
                    maxSelect: false,
                    onOptionClick: function () {
                        var selectItems = $('#blogTagsddl').val();
                        if (typeof selectItems != "undefined" && selectItems != null && selectItems.length > 0) {
                            $('#blogTagsddl').removeClass('input-validation-error').addClass('valid');
                            $('#blogTagsddl').find('.ms-options-wrap').css('border', 'none');
                            $('#blogTagsddl').find('.ms-options-wrap').find('button').attr('style', 'border-color: none');
                        } else {
                            $('#blogTagsddl').addClass('input-validation-error').removeClass('valid');
                            $('#blogTagsddl').find('.ms-options-wrap').find('button').attr('style', 'border-color: red !important;');
                            $('#blogTagsddl').find('.ms-options-wrap').css('border', '1px solid');
                        }
                    }
                });
        }
    },
    populateLanguages: function() {
        languages = $.parseJSON($('[id$="Languages"]').val());
        if (typeof languages != "undefined" && languages != null && languages.length > 0) {
            for (var j = 0; j < languages.length; j++) {
                $('#languageddl').append("<option value='" + languages[j].LanguageId + "'>" + languages[j].Description + "</option>");
            }
        }
    },
    registerEvent: function () {
        $("#blog-update").on("dialogopen", function () {
            blogUpdate.clearData();
            blogUpdate.setDefaultButtonText();
            blogUpdate.hookupEditor();
            blogUpdate.loadBlogUpdateData();
        });
        $("#blog-update").on("dialogclose", function () {
            if (typeof CKEDITOR.instances.postText != "undefined" && CKEDITOR.instances.postText != null)
                CKEDITOR.instances.postText.setData('');
        });

        $('#btnUpdateBlog').click(function () {
            blogUpdate.submitData();
            if (typeof CKEDITOR.instances.postText != "undefined" && CKEDITOR.instances.postText != null)
                CKEDITOR.instances.postText.setData('');
            $("#blog-update").dialog("close");
            
        });

        $('#btnCancelBlog').click(function () {
            if (typeof CKEDITOR.instances.postText != "undefined" && CKEDITOR.instances.postText != null)
                CKEDITOR.instances.postText.setData('');
            $("#blog-update").dialog("close");
        });

        $('#' + blogUpdate.self).on('change', ':file', function () {
            var input = $(this);
            var numFiles = input.get(0).files ? input.get(0).files.length : 1;
            var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            var file = this.files[0];
            if (file) {
                blogUpdate.sendFile(file, blogUpdate.setImageAfterUploaded);
            }
        });

        $(':file').on('fileselect', function (event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });
    },
    setTitleForSelectedDepartment: function (tempDepartments) {
        if (typeof tempDepartments != "undefined" && tempDepartments != null && tempDepartments.length > 0) {
            var size = tempDepartments.length;
            for (var i = 0; i < size; i++) {
                var currentItem = tempDepartments[i];
                $('#titleSection').find('input[id="' + currentItem.DepartmentId + '_title"]').val(currentItem.Title);
            }
        }
    },
    getSelectedBlogCategoryItem: function (id) {
        if (typeof id != "undefined" && id != null && id != 0) {
            for (var i = 0; i < categories.length; i++) {
                var currentItem = categories[i];
                if (currentItem.BlogCategoryId == id) {
                    return currentItem;
                }
            }
        }
    },
    getSelectedBlogTagItem: function (id) {
        if (typeof id != "undefined" && id != null && id != 0) {
            for (var i = 0; i < tags.length; i++) {
                var currentItem = tags[i];
                if (currentItem.BlogTagId == id) {
                    return currentItem;
                }
            }
        }
    },
    sendFile: function (file, callback) {
        var formData = new FormData();
        formData.append('file', file);
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            type: 'post',
            url: siteMaster.currentUrl() + '/Controllers/SiteMasterController.ashx?method=' + 'UploadBlogImage',
            data: formData,
            success: function (result) {
                if (typeof result != 'undefined' && result != null && result != '') {
                    result = $.parseJSON(result);
                    if (result.isSuccess == true) {
                        isUpdateNewImage = true;
                        $('#postFeatureImageText').val(result.azureLink);
                        callback(result.imageName);
                        $.unblockUI(siteMaster.loadingWheel());
                    }
                }
            },
            processData: false,
            contentType: false,
            error: function () {
                alert("Oops, something went wrong!");
                $.unblockUI(siteMaster.loadingWheel());
            }
        });
    },
    setImageAfterUploaded: function (url) {
        if (typeof url != "undefined" && url != null && url != '') {
            $('#tempImageName').attr('src', url);
            $('#tempImageName').val(url);
        }
    },
    mapDataForBlog: function (blog) {
        if (typeof blog !== "undefined" && blog !== null) {
            $('#postTitle').val(blog.PostTitle);
            if (blog.PostFeatureImage != null && blog.PostFeatureImage != '') {
                $('#postFeatureImage').attr('src', blog.PostFeatureImage);
                $('#postFeatureImageText').val(blog.PostFeatureImage);
            }
            $('#postSummary').val(blog.PostSummary);
            if (typeof CKEDITOR.instances.postText != "undefined" && CKEDITOR.instances.postText != null)
                CKEDITOR.instances.postText.setData(blog.PostText);

            $('#postText').val(blog.PostText);
            $('#button1Text').val(blog.Button1Text);
            $('#button2Text').val(blog.Button2Text);
            $('#button1Path').val(blog.Button1Path);
            $('#button2Path').val(blog.Button2Path);

            $('#isPublish').prop('checked', blog.IsPublish ? 'checked' : false);
            $('#isDeleted').prop('checked', blog.IsDeleted ? 'checked' : false);
            if (typeof blog.LanguageId != "undefined" && blog.LanguageId != null)
                $('#languageddl').val(blog.LanguageId);
            else
                $('#languageddl').val(1);

            $('#tempImageName').val(blog.ImageName);
            if (typeof blog.BlogCategoryIds != "undefined" && blog.BlogCategoryIds.length > 0) {
                var tempCategorylist = blogUpdate.populateSelectedBlogCategoryForBlogEntry(blog.BlogCategoryIds);
                $('#blogCategoriesddl[multiple]').multiselect('loadOptions', tempCategorylist, true, true);
            }
            if (typeof blog.BlogTagIds != "undefined" && blog.BlogTagIds.length > 0) {
                var tempTaglist = blogUpdate.populateSelectedBlogTagForBlogEntry(blog.BlogTagIds);
                $('#blogTagsddl[multiple]').multiselect('loadOptions', tempTaglist, true, true);
            }
        }
    },
    populateSelectedBlogCategoryForBlogEntry: function (tempCategories) {
        var templist = [];
        categories = $.parseJSON($('[id$="BlogCategory"]').val());
        var size = categories.length;
        for (var i = 0; i < size; i++) {
            var ischecked = false;
            for (var j = 0; j < tempCategories.length; j++) {
                if (categories[i].BlogCategoryId == tempCategories[j])
                    ischecked = true;
            }
            var currentItem = {
                value: categories[i].BlogCategoryId,
                name: blogUpdate.getSelectedBlogCategoryItem(categories[i].BlogCategoryId).CategoryText,
                checked: ischecked
            };
            templist.push(currentItem);
        }
        return templist;
    },
    populateSelectedBlogTagForBlogEntry: function (tempTags) {
        var templist = [];
        tags = $.parseJSON($('[id$="BlogTag"]').val());
        var size = tags.length;
        for (var i = 0; i < size; i++) {
            var ischecked = false;
            for (var j = 0; j < tempTags.length; j++) {
                if (tags[i].BlogTagId == tempTags[j])
                    ischecked = true;
            }
            var currentItem = {
                value: tags[i].BlogTagId,
                name: blogUpdate.getSelectedBlogTagItem(tags[i].BlogTagId).TagText,
                checked: ischecked
            };
            templist.push(currentItem);
        }
        return templist;
    },
    loadBlogUpdateData: function () {
        var id = $('[id$="BlogEntryId"]').val();
        if (typeof id !== "undefined" && id !== null && id !== '') {
            var data = {
                BlogEntryId: id
            };
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/BlogController.ashx?method=' + 'GetBlogEntryById',
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        isUpdateNewImage = false;
                        blogUpdate.mapDataForBlog(dataResult.blog);
                    } else {
                        alert("Something went wrong when getting this blog.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    },
    clearData: function () {
        $('#postTitle').val('');
        $('#postFeatureImage').val('');
        $('#tempImageName').val('');
        $('#postSummary').val('');

        $('#button1Text').val('');
        $('#button1Path').val('');
        $('#button2Text').val('');
        $('#button2Path').val('');
    },
    getBlogDataForSubmit: function () {
        var data = {
            blogEntryId: $('[id$="BlogEntryId"]').val(),
            postTitle: $('#postTitle').val(),
            postFeatureImage: $('#postFeatureImageText').val(),
            postSummary: $('#postSummary').val(),
            postText: CKEDITOR.instances.postText.getData(),
            button1Text: $('#button1Text').val(),
            button1Path: $('#button1Path').val(),
            button2Text: $('#button2Text').val(),
            button2Path: $('#button2Path').val(),
            imageName: $('#tempImageName').val(),
            languageId: $('#languageddl').val(),
            isUpdateNewImage: isUpdateNewImage,
            blogCategoryIds: [],
            blogTagIds: []
        };

        data.isPublish = $('#isPublish:checked').length > 0;
        data.isDeleted = $('#isDeleted:checked').length > 0;

        var selectedCategories = $('#blogCategoriesddl').val();
        var selectedTags = $('#blogTagsddl').val();

        if (typeof selectedCategories != "undefined" && selectedCategories != null && selectedCategories.length > 0) {
            for (var i = 0; i < selectedCategories.length; i++) {
                data.blogCategoryIds.push(selectedCategories[i]);
            }
        }

        if (typeof selectedTags != "undefined" && selectedTags != null && selectedTags.length > 0) {
            for (var i = 0; i < selectedTags.length; i++) {
                data.blogTagIds.push(selectedTags[i]);
            }
        }

        return data;
    },
    submitData: function () {
        var data = blogUpdate.getBlogDataForSubmit();
        var method = 'UpdateBlogEntry';
        var message = 'updated';

        if (typeof data !== "undefined" && data !== null) {
            if (data.blogEntryId == '' || data.blogEntryId == 0) {
                data.blogEntryId = 0;
                data.isUpdateNewImage = true;
                method = 'CreateBlogEntry';
                message = 'added';
            }
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/BlogController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        isUpdateNewImage = false;
                        alert("You have " + message + " this blog successfully!");
                        blogManagementTable.ajax.reload();
                    } else {
                        alert("Something went wrong when updating this blog.");
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert("Error when contacting to server, please try again later!");
                }
            });
        }
    }
}