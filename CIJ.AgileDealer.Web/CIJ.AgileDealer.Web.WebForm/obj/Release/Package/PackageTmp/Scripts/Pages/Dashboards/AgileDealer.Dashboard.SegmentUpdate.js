﻿$(document).ready(function () {
    segment.initialize();
});

var oSegmentTable;

var segment = {
    initialize: function () {
        segment.loadMasterData();
        segment.registerEvents();
        segment.loadSegments();
    },
    registerEvents: function () {
        $('#ddlMake').on('change', function () {
            var that = $(this);
            segment.loadSegments();
        });

        $('#tableSegment').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = oSegmentTable.row(row).data()[0];
            var make = oSegmentTable.row(row).data()[1];
            var model = oSegmentTable.row(row).data()[2];
            var segment = oSegmentTable.row(row).data()[3];

            $("#segment-list").dialog({
                resizable: false,
                modal: true,
                closeOnEscape: true,
                title: 'Update Segment',
                show: { effect: "fade", duration: 300 },
                position: { my: "center", at: "center", of: window },
                width: "auto",
                height: "auto",
                open: function () {
                    $('#competitiveModelId').val(id);
                    $('#txtMake').val(make);
                    $('#txtModel').val(model);
                    if (segment != "-None-") {
                        $('#ddlSegment').val(segment);
                    }
                },
                close: function () {
                    $('#competitiveModelId').val('');
                    $('#txtMake').val('');
                    $('#txtModel').val('');
                    $('#ddlSegment').val('');
                }
            });
        });

        $('#btnUpdateSegment').click(function () {
            segment.submitData();
        });
    },
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'GetCompetitiveModels';
    },
    submitData: function () {
        var method = 'UpdateCompetitiveModel';
        var data = {
            competitiveModelId: $('#competitiveModelId').val(),
            makeName: $('#txtMake').val(),
            name: $('#txtModel').val(),
            segment: $('#ddlSegment').val(),
            description: $('#txtModel').val()
        };
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            url: siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + method,
            type: 'POST',
            async: true,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 300000,
            data: JSON.stringify(data),
            success: function (dataResult) {
                if (dataResult.isSuccess === true) {
                    $("#segment-list").dialog('close');
                } else {
                    alert("Something went wrong when updating this segment.");
                }
                $.unblockUI(siteMaster.loadingWheel());
                oSegmentTable.ajax.reload();
            },
            error: function () {
                $.unblockUI(siteMaster.loadingWheel());
                alert("Error when contacting to server, please try again later!");
            }
        });
    },
    loadMasterData: function () {
        segment.loadSegmentList();
        segment.loadModelList();
    },
    loadSegmentList: function () {
        var segments = $.parseJSON($('[id$="SegmentHdf"]').val());
        $.each(segments, function (index, value) {
            var that = value;
            $('#ddlSegment').append('<option value="' + that.Name + '"> ' + that.Name + ' </>');
        });
    },
    loadModelList: function () {
        var models = $.parseJSON($('[id$="ModelHdf"]').val());
        $.each(models, function (index, value) {
            var that = value;
            $('#ddlMake').append('<option value="' + that.IdString + '"> ' + that.Name + ' </>');
        });
    },
    loadSegments: function () {
        
        if (typeof oSegmentTable != "undefined" && oSegmentTable != null) {
            oSegmentTable.ajax.reload(null,false);
        } else {
            oSegmentTable = $('#tableSegment')
               .DataTable({
                   ajax: {
                       url: segment.url(),
                       type: 'post',
                       async: true,
                       dataType: "json",
                       contentType: "application/json",
                       cache: false,
                       data: function () {
                           var data = {
                               name: $('#ddlMake').val()
                           };
                           return JSON.stringify(data);
                       }
                   },
                   columns: [
                       { sName: 'SegmentId', "visible": false },
                       { sName: 'Make', sClass: 'col-xs-4' },
                       { sName: 'Model', sClass: 'col-xs-4' },
                       { sName: 'Segment', sClass: 'col-xs-4 segment' },
                       {
                           sName: 'Update',
                           defaultContent: '<a href="" class="editor_edit">Update</a>'
                       }
                   ],
                   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                       if (aData[3] == "-None-") {
                           $(nRow).find('td.segment').addClass('danger');
                       }
                   }
               });
        }
    }
};