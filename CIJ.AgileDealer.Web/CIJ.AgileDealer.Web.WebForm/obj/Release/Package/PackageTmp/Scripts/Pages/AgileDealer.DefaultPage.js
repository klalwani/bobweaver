﻿$(document).ready(function () {
    defaultPage.initialize();
});
var defaultPage = {
    initialize: function () {
        defaultPage.registerEvents();
    },
    registerEvents: function () {
        $('#mainSearchButton').click(function () {
            var searchText = $('#txtSearchHome').val();
            searchText = $.trim(searchText);

            defaultPage.performSearch(searchText);
            return false;
        });

        $('#txtSearchHome').on('keypress', function (e) {
            var searchText = $('#txtSearchHome').val();
            searchText = $.trim(searchText);

            if (e.keyCode == 13) {
                e.preventDefault();
                e.stopPropagation();
                $('#txtSearch').val(searchText);
                defaultPage.performSearch(searchText);
            }
        });

    },
    performSearch: function (searchText) {

        if (typeof searchText != 'undefined' && searchText != null && searchText != '') {
            var temp = searchText.toLowerCase();

            var formatedText = searchText.replace(' ', '+');

            var url = '/inventory?keywords=' + formatedText;

            window.location.href = url;

        }
    }
}