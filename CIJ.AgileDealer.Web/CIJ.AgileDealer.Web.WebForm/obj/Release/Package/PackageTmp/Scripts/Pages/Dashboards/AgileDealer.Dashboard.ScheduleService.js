﻿$(document).ready(function () {
    //contactUsDashboard.initialize();
});

var scheduleServiceTable;

var scheduleServiceDashboard = {
    self: 'scheduleServiceDashboard',
    url: function () {
        return siteMaster.currentUrl() + '/Controllers/DashboardController.ashx?method=' + 'LoadScheduleServiceDashboard';
    },
    initialize: function () {
        scheduleServiceDashboard.registerEvents();
        scheduleServiceDashboard.loadData();
    },
    registerEvents: function () {
        $('#tableScheduleService').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            var row = $(this).closest('tr');
            var id = scheduleServiceTable.row(row).data()[0];
            location.href = siteMaster.currentUrl() + '/Admin/ScheduleServiceDetail/' + id;
        });
    },
    loadData: function () {
        scheduleServiceTable = $('#tableScheduleService').DataTable({
            ajax: scheduleServiceDashboard.url(),
            //stateSave: true,
            //stateDuration: 60 * 60,
            language: {
                emptyTable: "No record found!"
            },
            columns: [
                { sName: 'ScheduleServiceId', "visible": false },
                { sName: 'FirstName' },
                { sName: 'LastName' },
                { sName: 'Email' },
                { sName: 'Phone' },
                { sName: 'ScheduleDate' },
                { sName: 'WeekelyHour' },
                { sName: 'SaturdayHour' },
                { sName: 'SubmittedDate' },
                {
                    sName: 'ScheduleServiceId',
                    defaultContent: '<a href="" class="editor_edit">Detail</a>'
                }
            ],
            order: [[8, "desc"]]
        });
    }
};