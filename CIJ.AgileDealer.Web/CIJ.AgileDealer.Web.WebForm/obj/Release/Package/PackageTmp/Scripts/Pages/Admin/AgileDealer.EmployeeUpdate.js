﻿$(document).ready(function () {
    employee.initialize();
    AgileDealerValidation.InitialValidationEvent();
});
var employeeTemp;
var deparments;

var employee = {
    self: 'employee-update',
    currentEmployee: function () {
        return employeeTemp;
    },
    initialize: function () {
        employee.populateDepartments();
        employee.registerEvent();
    },
    populateDepartments: function () {
        deparments = $.parseJSON($('[id$="Departments"]').val());
        if (typeof deparments != "undefined" && deparments != null && deparments.length > 0) {
            var options;
            $.each(deparments,
                function (i, item) {
                    options += "<option value='" + item.DepartmentId + "'>" + item.Name + "</option>";
                });
            $('#department').append(options);
            $('#department')
                .multiselect({
                    // text to use in dummy input
                    placeholder: localization.employeeUpdate[common.getCurrentLanguage()].department_PlaceHolder,
                    // how many columns should be use to show options
                    columns: 2,
                    // include option search box  
                    search: false,
                    // search filter options
                    searchOptions: {
                        default: 'Search', // search input placeholder text
                        showOptGroups: false // show option group titles if no options remaining
                    },
                    // add select all option
                    selectAll: false,
                    // select entire optgroup  
                    selectGroup: false,
                    // minimum height of option overlay
                    minHeight: 200,
                    // maximum height of option overlay
                    maxHeight: null,
                    // display the checkbox to the user
                    showCheckbox: true,
                    // options for jquery.actual
                    jqActualOpts: {},
                    // maximum width of option overlay (or selector)
                    maxWidth: null,
                    // minimum number of items that can be selected
                    minSelect: false,
                    // maximum number of items that can be selected
                    maxSelect: false,
                    onOptionClick: function (e) {
                        var selectItems = $('#department').val();
                        if (typeof selectItems != "undefined" && selectItems != null && selectItems.length > 0) {
                            $('#department').removeClass('input-validation-error').addClass('valid');
                            $('.ms-options-wrap').css('border', 'none');
                            $('.ms-options-wrap').find('button').attr('style', 'border-color: none');
                        } else {
                            $('#department').addClass('input-validation-error').removeClass('valid');
                            $('.ms-options-wrap').find('button').attr('style', 'border-color: red !important;');
                            $('.ms-options-wrap').css('border', '1px solid');
                        }
                        employee.createInputTitleForSelectedDepartment();
                    }
                });
        }
    },
    clearDataForBlogForm: function() {
        
    },
    registerEvent: function () {
        $("#employee-update").on("dialogopen", function (event, ui) {
            employee.loadEmployeeData();
        });
        $("#employee-update").on("dialogclose", function (event, ui) {
            $("#employee-update").dialog('destroy');
        });
        $('#btnUpdateEmployee').click(function () {
            siteMaster.touchUpRequireFields(employee.self);
            if (siteMaster.checkValidationOfCurrentForm(employee.self)) {
                employee.submitData();
                $("#employee-update").dialog("close");
            } else {
                alert(localization.employeeUpdate[common.getCurrentLanguage()].errorMessage);
            }
        });

        $('#btnBackEmployee').click(function () {
            if (typeof employeeDashboard != 'undefined' && employeeDashboard.isRunningAsPopup)
                $("#employee-update").dialog("close");
            else {
                parent.history.back();
                return false;
            }
        });

        $('#' + employee.self).on('change', ':file', function () {
            var input = $(this);
            var numFiles = input.get(0).files ? input.get(0).files.length : 1;
            var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            var file = this.files[0];
            if (file) {
                employee.sendFile(file, employee.setImageAfterUploaded);
            }
        });

        $(':file').on('fileselect', function (event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });
    },
    setTitleForSelectedDepartment: function (tempDepartments) {
        if (typeof tempDepartments != "undefined" && tempDepartments != null && tempDepartments.length > 0) {
            var size = tempDepartments.length;
            for (var i = 0; i < size; i++) {
                var currentItem = tempDepartments[i];
                $('#titleSection').find('input[id="' + currentItem.DepartmentId + '_title"]').val(currentItem.Title);
            }
        }
    },

    createInputTitleForSelectedDepartment: function () {
        var selectItems = $('#department').val();
        $('#titleSection').find('input').closest('div').remove();
        $('#titleSection').find('#note').remove();
        if (typeof selectItems != "undefined" && selectItems != null && selectItems.length > 0) {
            $('#titleSection').removeClass('hidden');
            for (var i = 0; i < selectItems.length; i++) {
                var selectItem = employee.getSelectedItem(selectItems[i]);
                if (typeof selectItem != "undefined" && selectItem != null) {
                    var newInput;
                    if (i == 0) {
                        newInput =
                            '<div class="col-xs-7 marginBot15"><input type="text" class="form-control" data-val="true" data-val-required="true" id="' + selectItem.DepartmentId + '_title" placeholder="' + selectItem.Name + ' Title' + '" value=""></div>';
                    } else {
                        newInput =
                            '<div class="col-xs-offset-4 col-xs-7 marginBot15"><input type="text" class="form-control" data-val="true" data-val-required="true" id="' + selectItem.DepartmentId + '_title" placeholder="' + selectItem.Name + ' Title' + '" value=""></div>';
                    }
                    $('#titleSection').append(newInput);
                }
            }
            var noteCtr = localization.employeeUpdate[common.getCurrentLanguage()].titleSection
            $('#titleSection').append('<label for="note" id="note" class="col-xs-offset-4 col-xs-7">' + noteCtr + '</label>');
        } else {
            $('#titleSection').addClass('hidden');
        }
    },
    getSelectedItem: function (id) {
        if (typeof id != "undefined" && id != null && id != 0) {
            for (var i = 0; i < deparments.length; i++) {
                var currentItem = deparments[i];
                if (currentItem.DepartmentId == id) {
                    return currentItem;
                }
            }
        }
    },
    sendFile: function (file, callback) {
        var formData = new FormData();
        formData.append('file', file);
        $.blockUI(siteMaster.loadingWheel());
        $.ajax({
            type: 'post',
            url: siteMaster.currentUrl() + '/Controllers/SiteMasterController.ashx?method=' + 'UploadStaffImage',
            data: formData,
            success: function (result) {
                if (typeof result != 'undefined' && result != null && result != '') {
                    result = $.parseJSON(result);
                    if (result.isSuccess == true) {
                        $('#tempImageName').val(result.imageName);
                        callback(result.imageUrl);
                        $.unblockUI(siteMaster.loadingWheel());
                    }
                }
            },
            processData: false,
            contentType: false,
            error: function () {
                alert(localization.employeeUpdate[common.getCurrentLanguage()].sendFile_Error);
            }
        });
    },
    loadEmployeeData: function () {
        var id = $('[id$="EmployeeId"]').val();
        if (typeof id !== "undefined" && id !== null && id !== '') {
            var data = {
                employeeId: id
            };
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/EmployeeController.ashx?method=' + 'GetEmployee',
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        employee.mapDataForEmployee(dataResult.employee);
                    } else {
                        alert(localization.employeeUpdate[common.getCurrentLanguage()].sendFile_Error);
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert(localization.employeeUpdate[common.getCurrentLanguage()].ajax_Error);
                }
            });
        }
    },
    setImageAfterUploaded: function (url) {
        if (typeof url != "undefined" && url != '') {
            $('#vehicleImage').attr('src', url);
        }
    },
    mapDataForEmployee: function (employeeInfo) {
        if (typeof employeeInfo !== "undefined" && employeeInfo !== null) {
            $('#firstName').val(employeeInfo.FirstName);
            $('#lastName').val(employeeInfo.LastName);
            if (typeof employeeInfo.Order !== "undefined" && employeeInfo.Order !== null && employeeInfo.Order !== 0)
                $('#order').val(employeeInfo.Order);
            if (employeeInfo.Photo != null && employeeInfo.Photo != '')
                $('#vehicleImage').attr('src', employeeInfo.Photo);
            $('#email').val(employeeInfo.Email);
            $('#officePhone').val(employeeInfo.OfficePhone);
            $('#cellPhone').val(employeeInfo.CellPhone);
            $('#ext').val(employeeInfo.Ext);

            $('#isNew').prop('checked', employeeInfo.IsNew ? 'checked' : false);
            $('#isDeleted').prop('checked', employeeInfo.IsDeleted ? 'checked' : false);

            if (typeof employeeInfo.Departments != "undefined" && employeeInfo.Departments.length > 0) {
                var templist = employee.populateSelectedDepartmentForEmployee(employeeInfo.Departments);
                $('#department[multiple]').multiselect('loadOptions', templist, true, true);
                employee.createInputTitleForSelectedDepartment();
                employee.setTitleForSelectedDepartment(employeeInfo.Departments);
            }
        }
    },
    populateSelectedDepartmentForEmployee: function (tempDepartments) {
        var templist = [];
        deparments = $.parseJSON($('[id$="Departments"]').val());
        var size = deparments.length;
        for (var i = 0; i < size; i++) {
            var ischecked = false;
            for (var j = 0; j < tempDepartments.length; j++) {
                if (deparments[i].DepartmentId == tempDepartments[j].DepartmentId)
                    ischecked = true;
            }
            var currentItem = {
                value: deparments[i].DepartmentId,
                name: employee.getSelectedItem(deparments[i].DepartmentId).Name,
                checked: ischecked
            };
            templist.push(currentItem);
        }
        return templist;
    },
    getEmployeeDataForSubmit: function () {
        var data = {
            employeeId: $('[id$="EmployeeId"]').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            email: $('#email').val(),
            officePhone: $('#officePhone').val(),
            cellPhone: $('#cellPhone').val(),
            ext: $('#ext').val(),
            imageName: $('#tempImageName').val(),
            departments: []
        };

        data.isNew = $('#isNew:checked').length > 0;
        data.isDeleted = $('#isDeleted:checked').length > 0;

        var orderValue = $('#order').val();
        if (orderValue === '' || orderValue === 0) {
            data.order = null;
        } else {
            data.order = orderValue;
        }
        var selectedDepartments = $('#department').val();

        if (typeof selectedDepartments != "undefined" && selectedDepartments != null && selectedDepartments.length > 0) {
            for (var i = 0; i < selectedDepartments.length; i++) {
                var currentTitle = $('input[id="' + selectedDepartments[i] + '_title"]').val();
                var newItem = {
                    departmentId: selectedDepartments[i],
                    Title: currentTitle
                };
                data.departments.push(newItem);
            }
        }

        return data;
    },
    submitData: function () {
        var data = employee.getEmployeeDataForSubmit();
        var method = 'UpdateEmployee';
        var message = 'updated';

        if (typeof data !== "undefined" && data !== null) {
            if (data.employeeId == '') {
                data.employeeId = 0;
                method = 'AddEmployee';
                message = 'added';
            }
            $.blockUI(siteMaster.loadingWheel());
            $.ajax({
                url: siteMaster.currentUrl() + '/Controllers/EmployeeController.ashx?method=' + method,
                type: 'POST',
                async: true,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                timeout: 300000,
                data: JSON.stringify(data),
                success: function (dataResult) {
                    if (dataResult.isSuccess === true) {
                        var successMessage = localization.employeeUpdate[common.getCurrentLanguage()].submitData_Success.format(message);
                        alert(successMessage);
                        if (typeof employeeDashboard != 'undefined' && employeeDashboard.isRunningAsPopup) {
                            oTable.ajax.reload();
                        } else {
                            location.reload();
                        }
                    } else {
                        alert(localization.employeeUpdate[common.getCurrentLanguage()].submitData_UnSuccess);
                    }
                    $.unblockUI(siteMaster.loadingWheel());
                },
                error: function () {
                    $.unblockUI(siteMaster.loadingWheel());
                    alert(localization.employeeUpdate[common.getCurrentLanguage()].ajax_Error);
                }
            });
        }
    }
}