﻿var localization = {
    paymentCalculator: {
        en: {
            message : "Please input all information to perform the calculation!",
            successMessage : "Payments are ${0} for {1} months."
        },
        es: {
            message: "¡Por favor ingrese toda la información para realizar el cálculo!",
            successMessage: "Los pagos son ${0} para {1} meses."
        }
    },
    contactUs: {
        en: {
            successMessage: "Thank you for contacting {0}!"
        },
        es: {
            successMessage: "Thank you for contacting {0}!"
        }
    },
    staff: {
        en: {
            errorMessage: "Error when contacting to server, please try again later!",
            employeeUpdateTitle: "Update Employee",
            updateFailed: "Something went wrong when updating this staff.",
            buttonYes: "Yes, I am sure",
            buttonNo: "Cancel"
        },
        es: {
            errorMessage: "Error when contacting to server, please try again later!",
            employeeUpdateTitle: "Update Employee",
            updateFailed: "Something went wrong when updating this staff.",
            buttonYes: "Yes, I am sure",
            buttonNo: "Cancel"
        }
    },
    employeeUpdate: {
        en: {
            department_PlaceHolder:"Select Departments",
            errorMessage:"Please input all required information!",
            titleSection: 'Note: If you put the word "Manager" into any Department Title field, this staff will be set as "Manager" of that Department.',
            sendFile_Error: "Whoops something went wrong!",
            ajax_Error: "Error when contacting to server, please try again later!",
            loadEmployeeData_UnSuccess: "Something went wrong when getting this staff.",
            submitData_Success: "You have {0} this staff successfully!",
            submitData_UnSuccess: "Something went wrong when updating this vehicle."
        },
        es: {
            department_PlaceHolder: "Select Departments",
            errorMessage: "Please input all required information!",
            titleSection:'Note: If you put the word "Manager" into any Department Title field, this staff will be set as "Manager" of that Department.',
            sendFile_Error: "Whoops something went wrong!",
            ajax_Error: "Error when contacting to server, please try again later!",
            loadEmployeeData_UnSuccess: "Something went wrong when getting this staff.",
            submitData_Success: "You have {0} this staff successfully!",
            submitData_UnSuccess: "Something went wrong when updating this vehicle."
        }
    }
}