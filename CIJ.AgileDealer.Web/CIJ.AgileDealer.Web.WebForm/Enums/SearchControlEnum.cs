﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Enums
{
    public enum SearchControlEnum
    {
        PriceLowToHigh = 1,
        PriceHighToLow = 2,
        YearLowToHigh = 3,
        YearHighToLow = 4,
        MpgLowToHigh = 5,
        MpgHighToLow = 6,
        MilegeLowToHigh = 7,
        MilegeHighToLow = 8,
        StockDate = 9
    }

    public enum SearchFilterType
    {
        Type,
        Make,
        Model,
        Year,
        ExtColor,
        BodyStyle,
        Engine,
        FuelType,
        Price,
        Trim,
        VehicleFeature,
        Trans

    }
}