﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Enums
{
    public enum DealerIncentiveTypeEnum
    {
        Normal = 1,
        Detail = 2
    }
}