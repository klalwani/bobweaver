﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Enums
{
    public enum BlogCategoryEnums
    {
        Inventory = 1,
        Review = 2,
        Comparison = 3,
        BuyingGuide = 4,
        Upgrade = 5,
        BlogPost = 6
    }
}