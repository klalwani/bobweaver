﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Enums
{
    public static class RoleEnum
    {
        public const string Administrator = "Admin";
        public const string Dealer = "Dealer";
        public const string User = "User";
    }
}