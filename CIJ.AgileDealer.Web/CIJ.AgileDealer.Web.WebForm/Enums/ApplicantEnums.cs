﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Enums
{
    public enum ApplicantEnums
    {
        InComplete = 0,
        Individual = 1,
        JoinedApplicant = 2
    }
}