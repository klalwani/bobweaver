﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm
{
    public class CustomRedirectModule : IHttpModule
    {
        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += OnBeginRequest;
            httpApp.EndRequest += OnEndRequest;
            httpApp.PreSendRequestHeaders += OnHeaderSent;
        }

        public void OnHeaderSent(object sender, EventArgs e)
        {

        }

        // Record the time of the begin request event.
        public void OnBeginRequest(Object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;

            // Lets do custom redirect here
            var rawUrl = (((context != null && context.Request != null) ? context.Request.RawUrl : string.Empty) ?? string.Empty).Trim();

            var rawUrl2 = rawUrl + "/";

            if (!string.IsNullOrEmpty(rawUrl) &&
                SiteInfoHelper.RedirectUlrList.Any(a => (a.OldUrl.ToLower() == rawUrl.ToLower() || a.OldUrl.ToLower() == rawUrl2.ToLower()) && a.NewUrl.ToLower() != a.OldUrl.ToLower() && a.DealerID > 0))
            {
                var redirect = SiteInfoHelper.RedirectUlrList.Where(a => (a.OldUrl.ToLower() == rawUrl.ToLower() || a.OldUrl.ToLower() == rawUrl2.ToLower()) && a.DealerID>0 ).FirstOrDefault();

                if (redirect.IsPermanentRedirect)
                    context.Response.RedirectPermanent(redirect.NewUrl);
                else
                    context.Response.Redirect(redirect.NewUrl);
            }
        }

        public void OnEndRequest(Object sender, EventArgs e)
        {


        }

        public void Dispose()
        { /* Not needed */
        }
    }
}