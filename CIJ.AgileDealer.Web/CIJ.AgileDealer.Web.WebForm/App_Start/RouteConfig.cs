using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace CJI.AgileDealer.Web.WebForm
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings { AutoRedirectMode = RedirectMode.Permanent };
            routes.EnableFriendlyUrls(settings);
            routes.MapPageRoute("Service Specials", "service/specials", "~/Views/Services/ServiceSpecial.aspx");
            routes.MapPageRoute("Service", "service", "~/Views/Services/Service.aspx");
            routes.MapPageRoute("Appointment", "service/appointment", "~/Views/Services/Appointment.aspx");
            routes.MapPageRoute("OilChange", "service/oil-change", "~/Views/Services/OilChange.aspx");
            routes.MapPageRoute("Brakes", "service/brakes", "~/Views/Services/Brakes.aspx");
            routes.MapPageRoute("Tires", "service/tires", "~/Views/Services/Tires.aspx");
            routes.MapPageRoute("Parts", "service/parts", "~/Views/Services/Parts.aspx");
            routes.MapPageRoute("PageNotFound", "PageNotFound", "~/Views/Errors/PageNoFound.aspx");
            routes.MapPageRoute("ErrorPage", "Error", "~/Views/Errors/ErrorPage.aspx");

            routes.MapPageRoute("NewCar", "new", "~/Views/NewCars/New.aspx");
            routes.MapPageRoute("CertifiedCar", "certified", "~/Views/Certified/Certified.aspx");
            routes.MapPageRoute("Used", "used", "~/Views/UseCars/Used.aspx");
            routes.MapPageRoute("Inventory", "inventory", "~/Views/Inventory/Inventory.aspx");
            //This route is using character �(&#8211;) instead of -. Because it will be duplicated with F-150, F-250....
            // routes.MapPageRoute("VCP", "VCP/{type}�{make}�{model}", "~/Views/VCP/VCPPage.aspx");
            // Updated this to use '/' as it is valid for seo.
            routes.MapPageRoute("VCP", "vcp/{type}/{make}/{model}", "~/Views/VCP/VcpPage.aspx");
            routes.MapPageRoute("PaymentCalculator", "paymentcalculator", "~/Views/PaymentCalculator/PaymentCalculator.aspx");
            routes.MapPageRoute("AutoCheck", "partners/autocheck", "~/Views/Partners/AutoCheck.aspx");
            routes.MapPageRoute("VehicleContent", "new/{make}-{model}", "~/Views/NewCars/VehicleContent.aspx");
            routes.MapPageRoute("New/VehicleDisplay", "new/{make}-{model}/{Vin}", "~/Views/NewCars/VehicleDisplay.aspx");


            //used-certified-2015-gmc-sierra-1500-1gtv2uec3fz366162
            routes.MapPageRoute("VehicleInfoF", "vehicle-info/used-certified-{year}-{make}-{model}-{trim}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");

            //used-certified-2015-gmc-sierra-1500-1gtv2uec3fz366162
            routes.MapPageRoute("VehicleInfoG", "vehicle-info/used-certified-{year}-{make}-{model}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");

            //new-2019-ford-super-duty-f-250-srw-1ft7w2bt2ked62921
            routes.MapPageRoute("VehicleInfoA", "vehicle-info/{type}-{year}-{make}-{model}-{model1}-{model2}-{model3}-{trim}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");
            //new-2018-chevrolet-express-commercial-cargo-cutaway-1gb0grfg0j1339670
            routes.MapPageRoute("VehicleInfoB", "vehicle-info/{type}-{year}-{make}-{model}-{model1}-{model2}-{trim}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");
            //new-2019-ford-transit-connect-van-nm0ls7e24k1388080
            routes.MapPageRoute("VehicleInfoC", "vehicle-info/{type}-{year}-{make}-{model}-{model1}-{trim}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");
            //new-2019-ford-fusion-hybrid-3fa6p0lu6kr169338
            routes.MapPageRoute("VehicleInfoD", "vehicle-info/{type}-{year}-{make}-{model}-{trim}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");
            routes.MapPageRoute("VehicleInfoE", "vehicle-info/{type}-{year}-{make}-{model}-{Vin}", "~/Views/NewCars/NewVehicleInfo.aspx");



            routes.MapPageRoute("CategoryContent", "used/category/{category}", "~/Views/UseCars/CategoryContent.aspx");
            routes.MapPageRoute("UsedContent", "used/make-model", "~/Views/UseCars/UsedContent.aspx");
            routes.MapPageRoute("Used/VehicleDisplay", "used/make-model/{Vin}", "~/Views/UseCars/VehicleDisplay.aspx");
            routes.MapPageRoute("Specials", "specials", "~/Views/Special/Special.aspx");
            routes.MapPageRoute("ManufacturerIncentives", "specials/manufacturer-incentives",
                "~/Views/Special/ManufacturerIncentives.aspx");
            routes.MapPageRoute("Weekly-ad", "specials/weekly-ad", "~/Views/Special/WeeklyAd.aspx");
            routes.MapPageRoute("Finances", "finance", "~/Views/Finances/Finance.aspx");
            routes.MapPageRoute("OnlineCreditApproval", "finance/car-loan", "~/Views/Finances/OnlineCreditApproval.aspx");
            routes.MapPageRoute("Trade-In-Car", "finance/trade-in-car", "~/Views/Finances/CostEstimator.aspx");
            routes.MapPageRoute("Contact Us", "contactus", "~/Views/ContactUs/ContactUs.aspx");


            // Commercial Fleet Pages
            routes.MapPageRoute("commercial-truck", "commercial-fleet/{type}/{model}", "~/Views/Department/commercial.aspx");


            //Departmnet Pages
            routes.MapPageRoute("truck-department", "truck-department", "~/Views/Department/truck-department.aspx");
            routes.MapPageRoute("van-department", "van-department", "~/Views/Department/van-department.aspx");
            routes.MapPageRoute("vehicle-department", "vehicle-department/{type}/{model}", "~/Views/Department/{model}-department.aspx");
            routes.MapPageRoute("vehicle-department-lifted", "vehicle-department-lifted/{type}/{lifted}/{model}", "~/Views/Department/{model}-department.aspx");
			routes.MapPageRoute("vehicle-department-special", "vehicle-department-special/{type}/{special}", "~/Views/Department/Vehicle-Department.aspx");
            routes.MapPageRoute("vehicle-department-loaner-specials", "vehicle-department-loaner-specials/{type}/{loanerspecial}", "~/Views/Department/Vehicle-Department.aspx");
            routes.MapPageRoute("suv-department", "suv-department/{type}/{model}", "~/Views/Department/crossover�suv-department.aspx");
            routes.MapPageRoute("car-department", "car-department/{type}/{model}", "~/Views/Department/Vehicle-Department.aspx");
            routes.MapPageRoute("price-department", "price-department/{type}/{price}", "~/Views/Department/Vehicle-Department.aspx");

            // VCP Pages
            routes.MapPageRoute("New Ford F-150", "vcp/ford-new-f-150", "~/Views/VCP/New/F-150.aspx");
            routes.MapPageRoute("New Ford F-250 Super Duty", "vcp/ford-new-f-250-super-duty", "~/Views/VCP/New/F-250.aspx");
            routes.MapPageRoute("New Ford F-350 Super Duty", "vcp/ford-new-f-350-super-duty", "~/Views/VCP/New/F-350.aspx");
            routes.MapPageRoute("New Ford F-450 Super Duty", "vcp/ford-new-f-450-super-duty", "~/Views/VCP/New/F-450.aspx");
            routes.MapPageRoute("New Ford F-550", "vcp/ford-new-f-550", "~/Views/VCP/New/F-550.aspx");
            routes.MapPageRoute("New Ford F-650", "vcp/ford-new-f-650", "~/Views/VCP/New/F-650.aspx");
            routes.MapPageRoute("New Ford F-750", "vcp/ford-new-f-750", "~/Views/VCP/New/F-750.aspx");

            routes.MapPageRoute("Certified Ford F-150", "vcp/ford-certified-f-150", "~/Views/VCP/Certified/F-150.aspx");
            routes.MapPageRoute("Certified Ford F-250 Super Duty", "vcp/ford-certified-f-250-super-duty", "~/Views/VCP/Certified/F-250.aspx");

            routes.MapPageRoute("Used Ford F-150", "vcp/ford-used-f-150", "~/Views/VCP/Used/F-150.aspx");
            routes.MapPageRoute("Used Ford F-250", "vcp/ford-used-f-250-super-duty", "~/Views/VCP/Used/F-250.aspx");

            routes.MapPageRoute("HoursAndDirections", "dealership-info/hours-and-directions", "~/Views/ContactUs/ContactUs.aspx");
            routes.MapPageRoute("Employment", "dealership-info/employment", "~/Views/AboutUs/Employment.aspx");
            routes.MapPageRoute("Staff", "dealership-info/staff", "~/Views/AboutUs/Staff.aspx");
            routes.MapPageRoute("AboutUs", "dealership-info", "~/Views/AboutUs/AboutUs.aspx");

            routes.MapPageRoute("Sitemap", "Sitemap", "~/Sitemap.aspx");
            routes.MapPageRoute("SitemapXml", "SitemapXml", "~/SitemapXml.aspx");
            routes.MapPageRoute("Privacy", "Privacy", "~/Privacy.aspx");

            // Add Routes Blog.
            routes.MapPageRoute("Research", "research", "~/Views/Blogs/Blog.aspx");
            routes.MapPageRoute("BlogByCategory", "research/category/{CategorySelected}", "~/Views/Blogs/Blog.aspx");
            routes.MapPageRoute("BlogByTag", "research/tag/{TagSelected}", "~/Views/Blogs/Blog.aspx");
            routes.MapPageRoute("BlogBySearch", "research/{SearchTerm}", "~/Views/Blogs/Blog.aspx");
            routes.MapPageRoute("BlogDetail", "research/detail/{PostUrl}", "~/Views/Blogs/Blog.aspx");

            // Dashboard
            routes.MapPageRoute("AdminFinance", "Admin/FinanceApplicant", "~/Views/Admin/ApplicantDashboard.aspx");
            routes.MapPageRoute("LeadDashboard", "Admin/LeadDashboard", "~/Views/Admin/LeadDashboard.aspx");
            routes.MapPageRoute("Vehicle", "Admin/VehicleImageManager", "~/Views/Admin/VehicleImageManager.aspx");
            routes.MapPageRoute("AdminDashboard", "Admin/Dashboard", "~/Views/Admin/AdminDashboard.aspx");
            routes.MapPageRoute("AdminStaff", "Admin/Staff", "~/Views/Admin/EmployeeDashboard.aspx");
            routes.MapPageRoute("AdminFinanceDetail", "Admin/FinanceApplicant/Detail/{ApplicantId}", "~/Views/Admin/ApplicantDetail.aspx");
            routes.MapPageRoute("AdminUserAccount", "Admin/UserAccount", "~/Views/Admin/UserAccountDashboard.aspx");
            routes.MapPageRoute("AdminUserAccountUpdate", "Admin/UserAccount/Update/{UserId}", "~/Views/Admin/UserAccountUpdate.aspx");
            routes.MapPageRoute("AdminNotificationSetting", "Admin/NotificationSetting", "~/Views/Admin/NotificationSetting.aspx");
            routes.MapPageRoute("VehicleSegment", "Admin/VehicleSegment", "~/Views/Admin/VehicleSegment.aspx");
            routes.MapPageRoute("AdminSlide", "Admin/SlideManagement", "~/Views/Admin/SliderManagementDashboard.aspx");
	
			//Admin
			routes.MapPageRoute("JobManagement", "admin/jobmanagement/", "~/Views/Admin/JobManagement.aspx");
			routes.MapPageRoute("VehicleEdit", "Admin/VehicleEdit/{Vin}", "~/Views/Admin/AgileVehicleEditPage.aspx");
            routes.MapPageRoute("StaffEdit", "Admin/StaffEdit/{EmployeeId}", "~/Views/Admin/StaffPageEdit.aspx");
            routes.MapPageRoute("ContactUsDetail", "Admin/ContactUsDetail/{contactusId}", "~/Views/Admin/ContactUsDetail.aspx");
            routes.MapPageRoute("ScheduleServiceDetail", "Admin/ScheduleServiceDetail/{scheduleServiceId}", "~/Views/Admin/ScheduleServiceDetailPage.aspx");
            routes.MapPageRoute("CheckAvailabilityDetail", "Admin/CheckAvailabilityDetail/{vehicleFeedbackId}", "~/Views/Admin/CheckAvailabilityDetail.aspx");
            routes.MapPageRoute("BlogManagement", "Admin/BlogManagement", "~/Views/Admin/BlogManagement.aspx");
			routes.MapPageRoute("EditDealerInfoPage", "Admin/EditDealerInfoPage", "~/Views/Admin/EditDealerInfoPage.aspx");
			routes.MapPageRoute("BlogEdit", "Admin/BlogEdit", "~/Views/Admin/BlogEdit.aspx");
			routes.MapPageRoute("CustomRedirectInfo", "Admin/CustomRedirectInfo", "~/Views/Admin/CustomRedirectInfo.aspx");
            routes.MapPageRoute("LocalizationAdmin", "LocalizationAdmin", "~/LocalizationAdmin/Index.aspx");
            routes.MapPageRoute("IncentiveDashboard", "admin/incentivemanagement", "~/Views/Admin/IncentiveDashboard.aspx");
            routes.MapPageRoute("VehicleOptionsDashboard", "admin/vehicleoptionsdashboard", "~/Views/Admin/VehicleOptionsDashboard.aspx");
            routes.MapPageRoute("VehicleFeatureManagementDashboard", "admin/Vehiclefeaturemanagement", "~/Views/Admin/VehicleFeatureDashboard.aspx");
            routes.MapPageRoute("Bulk Update Vehicle Data Dashboard", "Admin/bulkupdatevehicledatadashboard", "~/Views/Admin/VehicleBulkUpdateDashboard.aspx");

            routes.MapPageRoute("Customer Reviews", "customer-reviews", "~/Views/CustomerReviews/CustomerReviews.aspx");

            routes.MapPageRoute("content", "content/{*tags}", "~/CMS/CMSPage.aspx");

            routes.MapPageRoute("Config Content", "Admin/ConfigContent", "~/Views/Admin/MenuSetting.aspx");

            //routes.MapPageRoute("cmsPage", "{*tags}", "~/CMS/CMSPage.aspx");

        }
    }
}