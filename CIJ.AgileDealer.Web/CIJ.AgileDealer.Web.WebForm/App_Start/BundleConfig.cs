﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace CJI.AgileDealer.Web.WebForm
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/ImageZoom.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            // reference by chat.aspx
            bundles.Add(new ScriptBundle("~/bundle/chat")
                //.Include("~/Scripts/jquery.signalR-2.2.0.js")
                //.Include("~/signalr/hubs")
                .Include("~/Scripts/jquery-ui.js")
                //.Include("~/Scripts/Pages/blog.js")
                .Include("~/Scripts/jquery.tmpl.js")
                );



            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            // Use the Development version of Modernizr to develop with and learn from. Then, when you’re
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            // *** We need this bundle "~/bundles/jquery" to avoid this error "WebForms UnobtrusiveValidationMode requires a ScriptResourceMapping for 'jquery'."
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryPrint")/*.Include("~/Scripts/jquery.print.js")*/);
            //bundles.Add(new ScriptBundle("~/bundles/jqueryUI")./*Include("~/Scripts/jquery-ui.js")*/);
            bundles.Add(new ScriptBundle("~/bundles/dataTableEdior").Include("~/Scripts/jquery.dataTables.editor.js"));
            //bundles.Add(new ScriptBundle("~/bundles/slick").Include("~/Scripts/slick.min.js"));
            //bundles.Add(new ScriptBundle("~/bundles/blog").Include("~/Scripts/Pages/blog.js"));
            //bundles.Add(new ScriptBundle("~/bundles/signalR").Include("~/Scripts/jquery.signalR-2.2.0.js"));
            //bundles.Add(new ScriptBundle("~/signalr/hubs").Include("~/signalr/hubs"));
            ////bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jquerytmpl").Include("~/Scripts/jquery.tmpl.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryMasked").Include("~/Scripts/jquery.mask.min.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryBootpag").Include("~/Scripts/jquery.bootpag.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryDataTable").Include("~/Scripts/jquery.datatable.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryBlockUI").Include("~/Scripts/jquery.blockUI.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryLazyLoad").Include("~/Scripts/jquery.lazyload.min.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryMaskedMoney")/*.Include("~/Scripts/jquery.maskMoney.js")*/);
            //bundles.Add(new ScriptBundle("~/bundles/jquerySession").Include("~/Scripts/jquery.cookie.js"));

            //Common
            bundles.Add(new ScriptBundle("~/bundles/common")
                .Include("~/Scripts/modernizr-*")
                .Include("~/Scripts/slick.min.js")
                .Include("~/Scripts/jquery.print.js")
                .Include("~/Scripts/jquery-{version}.js") // needs this here to as kept getting script error "jQuery is undefined"
                .Include("~/Scripts/jquery-ui.js")
                .Include("~/Scripts/Pages/Common.js")
                .Include("~/Scripts/Pages/AgileDealer.Validation.js")
                .Include("~/Scripts/Localization/AgileDealer.Localization.js")

                .Include("~/Scripts/jquery.mask.min.js")
                .Include("~/Scripts/jquery.maskMoney.js")
                .Include("~/Scripts/jquery.bootpag.js")
                .Include("~/Scripts/Pages/AgileDealer.SiteMaster.js")
                .Include("~/Scripts/Pages/AgileDealer.Custom.js")
                .Include("~/Scripts/jquery.blockUI.js")
                .Include("~/Scripts/jquery.tmpl.js")
                .Include("~/Scripts/jquery.lazyload.min.js")

                .Include("~/Scripts/bootstrap-switch.min.js")
                .Include("~/Scripts/jquery.cookie.js")
                .Include("~/Scripts/jquery.multiselect.js")
                .Include("~/Scripts/jquery.datatable.js")

                );

            //bundles.Add(new ScriptBundle("~/bundles/SiteMaster")
            //    .Include("~/Scripts/Pages/AgileDealer.SiteMaster.js")
            //    );

            bundles.Add(new ScriptBundle("~/bundles/DefaultPage")
                .Include("~/Scripts/Pages/AgileDealer.DefaultPage.js")
                );


            bundles.Add(new ScriptBundle("~/bundles/VehicleInfo")
                .Include("~/Scripts/Pages/AgileDealer.VehicleInfo.js")
                .Include("~/Scripts/Pages/Enums/Dealer.Enum.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/RebateControl")
                .Include("~/Scripts/Pages/AgileDealer.Rebate.js")
                );

            //bundles.Add(new ScriptBundle("~/bundles/jqueryMultiSelect")
            //    .Include("~/Scripts/jquery.multiselect.js")
            //    );
            bundles.Add(new ScriptBundle("~/bundles/VCP")
                .Include("~/Scripts/Pages/AgileDealer.VCP.js")
                );

            //bundles.Add(new ScriptBundle("~/bundles/Custom")
            //    .Include("~/Scripts/Pages/AgileDealer.Custom.js")
            //    );

            bundles.Add(new ScriptBundle("~/bundles/BlogDetail")
               .Include("~/Scripts/Pages/AgileDealer.BlogDetail.js")
               );

            bundles.Add(new ScriptBundle("~/bundles/ApplicantDetail")
                .Include("~/Scripts/Pages/Dashboards/AgileDealer.ApplicantDetail.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/SearchControl")
                .Include("~/Scripts/Pages/AgileDealer.SearchControl.js")
                .Include("~/Scripts/Pages/Enums/Dealer.Enum.js")
                );

            //bundles.Add(new ScriptBundle("~/bundles/bootstrapSwitch")
            //    .Include("~/Scripts/bootstrap-switch.min.js")
            //    );

            bundles.Add(new ScriptBundle("~/bundles/NotificationSetting")
                .Include("~/Scripts/Pages/Admin/AgileDealer.NotificationSetting.js")
                .Include("~/Scripts/Pages/Enums/NotificationSetting.Enum.js")
                );

            bundles.Add(new StyleBundle("~/Content/styles").Include(
                "~/Content/Style.css"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/jquery.multiselect.css")
                .Include("~/Content/bootstrap-switch.css")
                .Include("~/Content/bootstrap-switch.min.css")
                .Include("~/Content/main.css")
                .Include("~/Content/savvy.css")
                .Include("~/Content/fonts/font-awesome.min.css")
                .Include("~/Content/Style.css")
                .Include("~/Content/jquery.dataTables.css")
                .Include("~/Content/slick.css")
                .Include("~/Content/jquery-ui.css")
                .Include("~/Content/override.css")
                .Include("~/Content/override-section.css")
                );

            bundles.Add(new StyleBundle("~/Content/UITheme").Include("~/Content/themes/base/*.css")
);


            bundles.Add(new StyleBundle("~/Content/DefaultPage")
           .Include("~/Content/HomePage/bootstrap.min.css")
                .Include("~/Content/HomePage/font-awesome.min.css")
                .Include("~/Content/HomePage/owl.carousel.min.css")
           );

            bundles.Add(new StyleBundle("~/Content/DefaultPageStyles")
              .Include("~/Content/HomePage/style.min.css")
              .Include("~/Content/HomePage/responsive.min.css")
               .Include("~/Content/slick.min.css")
              .Include("~/Content/jquery-ui.min.css")
         );

            bundles.Add(new StyleBundle("~/Content/DefaultPageOverrideStyles")
              .Include("~/Content/override.min.css")
              .Include("~/Content/override-section.min.css")
         );

            //Common
            bundles.Add(new ScriptBundle("~/bundles/commonDefaultPage")
               .Include("~/Scripts/Pages/AgileDealer.DefaultPage.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/DefaultPageScripts")
               
              .Include("~/Scripts/owl.carousel.min.js") // needs this here to as kept getting script error "jQuery is undefined"
                
                  .Include("~/Scripts/custom.js")
               );

            bundles.Add(new StyleBundle("~/Content/bootstrap4-override")
                .Include("~/Content/bootstrap-4-override.min.css")
               );
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;


            ScriptManager.ScriptResourceMapping.AddDefinition(
                "respond",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/respond.min.js",
                    DebugPath = "~/Scripts/respond.js",
                });
        }
    }
}