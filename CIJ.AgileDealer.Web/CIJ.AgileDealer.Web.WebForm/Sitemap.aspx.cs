﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm
{
    public partial class Sitemap : BasedPage
    {
		private List<TreeviewModel> SiteMaps;
        protected void Page_Load(object sender, EventArgs e)
        {
			SiteMaps = CMSHelper.GetMenus(DealerID,false);
            SiteMapNodeCollection dataSource = SiteMap.RootNode.ChildNodes;
            parent.DataSource = SiteMaps;
            parent.DataBind();
        }

        protected void parent_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var currentRepeaterNode = (TreeviewModel)e.Item.DataItem;
			//var nodes = new SiteMapNodeCollection();
			//foreach (SiteMapNode n in currentRepeaterNode.ChildNodes)
			//{
			//    nodes.Add(n);
			//}
			var nodes = currentRepeaterNode.Nodes;
			if (nodes.Count <= 0) return;
            ((Repeater) e.Item.FindControl("child")).DataSource = nodes;
            ((Repeater) e.Item.FindControl("child")).DataBind();
        }
    }
}