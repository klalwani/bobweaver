﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.Certified
{
    public partial class Certified : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				DefaultFilter filters = new DefaultFilter
				{
					DefaultTypes = new List<string> { "certified" },
					DefaultModels = new List<int>()
				};
				InventoryUserControl.DealerCity = DealerCity;
				InventoryUserControl.DealerState = DealerState;
				InventoryUserControl.TypeString = "Certified ";
				InventoryUserControl.DefaultFilter = filters;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}