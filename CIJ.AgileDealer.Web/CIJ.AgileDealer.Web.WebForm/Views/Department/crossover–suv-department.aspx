﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="crossover–suv-department.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Department.suv_department" %>

<%@ Register Src="~/Views/UserControl/VehicleTypeControl/VehicleTypeControl.ascx" TagPrefix="uc1" TagName="VehicleTypeControl" %>
<%@ Register Src="~/Views/UserControl/SearchControl/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchUserControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Department" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1><%=string.Format(Suv_Department.Inner_Container_H1,DealerCity,DealerName) %></h1>
            </div>
        </div>
    </div>

    <div class="clear-20"></div>
    <div class="row">
        <div class="container sub-nav-menu">
            <nav class="navbar navbar-inverse primaryBg">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-menu-navbar-collapse" aria-expanded="false">
                        <span class="sr-only"><%=Suv_Department.Sr_Only %></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/vehicle-department/new-used-certified/suv"><%=Suv_Department.Navbar_Brand %></a>
                </div>
                
                
                <div class="collapse navbar-collapse" id="sub-menu-navbar-collapse">
                    <ul id="tabs" class="nav navbar-nav" data-tabs="tabs">
                        <!-- <li id="" class="active"><a href="#inventory" data-toggle="tab">Inventory</a></li> -->
                        <li id="" class="dropdown active">
                            <a href="#" class="dropdown-toggle" id="vehicle-department-subnav" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=Suv_Department.Dropdown_Toggle %><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/suv-department/new-used-certified/crossover–suv"><%=Suv_Department.Dropdown_Menu_Li_A_1 %></a></li>
                                <li><a href="/suv-department/new/crossover–suv"><%=Suv_Department.Dropdown_Menu_Li_A_2 %></a></li>
                                <li><a href="/suv-department/used-certified/crossover–suv"><%=Suv_Department.Dropdown_Menu_Li_A_3 %></a></li>
                            </ul>
                        </li>
                        <!-- <li id="" class=""><a href="#reviews" data-toggle="tab">Research</a></li> -->
                        <!-- <li id="" class=""><a href="#specials" data-toggle="tab">Specials</a></li> -->
                    </ul>
                </div>

            </nav>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div id="my-tab-content" class="tab-content ">
                <div class="tab-pane padded-15 active" id="inventory">

                    <uc1:VehicleTypeControl runat="server" ID="VehicleTypeControl" />

                </div>
                <%--<div class="tab-pane padded-15" id="reviews">
                    <div style="min-height: 800px;">
                        <h2><%=Suv_Department.Tab_Pane_Padded_15_Review %></h2>
                    </div>
                </div>
                <div class="tab-pane padded-15" id="specials">
                    <div style="min-height: 800px;">
                        <h2><%=Suv_Department.Tab_Pane_Padded_15_Special %></h2>
                    </div>
                </div>--%>
            </div>

        </div>
    </div>


</asp:Content>
