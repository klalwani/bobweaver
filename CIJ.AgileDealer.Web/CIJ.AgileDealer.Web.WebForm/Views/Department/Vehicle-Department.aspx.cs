﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.Base.Helpers;

namespace CJI.AgileDealer.Web.WebForm.Views.Department
{
    public partial class Vehicle_Department : BasedPage
    {
        private char[] charSeparators = { '-' };
        private char[] specialCharSeparators = { '–' };

        private string _vehicleType;
        public string VehicleType
        {
            get
            {

                string vehicleTypeString = !string.IsNullOrEmpty(Page.RouteData.Values["model"] as string)
                    ? Page.RouteData.Values["model"] as string
                    : !string.IsNullOrEmpty(Page.RouteData.Values["special"] as string)
                        ? Page.RouteData.Values["special"] as string
                         : !string.IsNullOrEmpty(Page.RouteData.Values["loanerspecial"] as string)
                        ? Page.RouteData.Values["loanerspecial"] as string : string.Empty;
                if (!string.IsNullOrEmpty(vehicleTypeString))
                {
                    _vehicleType =
                        vehicleTypeString.Split(specialCharSeparators, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                    if (!string.IsNullOrEmpty(_vehicleType))
                        _vehicleType = CommonHelper.FirstLetterToUpper(_vehicleType);
                }
                else
                {
                    _vehicleType = string.Empty;
                }
                return _vehicleType;
            }
            set
            {
                _vehicleType = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(VehicleType))
            {
                if (VehicleType.IndexOf("Loaner", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    headingText.InnerHtml = string.Format(CJI.AgileDealer.Web.WebForm.Resources.Views.Department.Vehicle_Department.Inner_Container_H1.Replace("Department", ""), DealerCity, "Loaner Specials", DealerName);

                }
                else
                {
                    headingText.InnerHtml = string.Format(CJI.AgileDealer.Web.WebForm.Resources.Views.Department.Vehicle_Department.Inner_Container_H1, DealerCity, !string.IsNullOrEmpty(VehicleType) ? VehicleType + "s" : string.Empty, DealerName);
                }
            }
            else
            {
                headingText.InnerHtml = string.Format(CJI.AgileDealer.Web.WebForm.Resources.Views.Department.Vehicle_Department.Inner_Container_H1, DealerCity, !string.IsNullOrEmpty(VehicleType) ? VehicleType + "s" : string.Empty, DealerName);
            }
        }

    }
}