﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Staff.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.AboutUs.Staff" %>

<%@ Register Src="~/Views/UserControl/StaffView.ascx" TagName="StaffView" TagPrefix="uc1" %>
<%@ Register Src="~/Views/UserControl/StaffEdit.ascx" TagName="StaffEdit" TagPrefix="uc2" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:StaffView ID="UCStaffView" runat="server" />
</asp:Content>
