﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    [Authorize(Roles = "Admin,Dealer")]
    public partial class UserAccountDashboard : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
            {
                Response.Redirect("~/Account/Login", false);
            }
        }
    }
}