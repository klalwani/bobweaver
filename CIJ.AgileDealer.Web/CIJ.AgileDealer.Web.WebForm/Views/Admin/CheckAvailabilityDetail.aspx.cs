﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class CheckAvailabilityDetail : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
                if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
                {
                    Response.Redirect("~/Account/Login", false);
                }
                if (!string.IsNullOrEmpty(Page.RouteData.Values["vehicleFeedbackId"] as string))
				{
					vehicleFeedbackId.Value = Page.RouteData.Values["vehicleFeedbackId"] as string;
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}