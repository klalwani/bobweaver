﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="MenuSetting.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.MenuSetting" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<script src="../../Scripts/Pages/Content/MenuSetting.js"></script>
	<script>
		$(document).ready(function () {
			menuSetting.init();
		});
	</script>
	<link rel="stylesheet" href="../../Content/fonts/font-awesome.css" />
	<style>
		div#menuSetting { 
			height: 940px; overflow-y: scroll;overflow-x:hidden 
		}
		div#menuSetting::-webkit-scrollbar {
			width: 12px;
			background-color: #F5F5F5;
		}

		div#menuSetting::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			border-radius: 10px;
			background-color: #F5F5F5;
		}

		div#menuSetting::-webkit-scrollbar-thumb {
			border-radius: 10px;
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
			background-color: #555;
		}
		input[type=checkbox], input[type=radio] {
			margin: 13px 9px 0px;
		}

		.list-group-item:hover {
			color: #00ff90;
		}

		a:hover {
			text-decoration: none;
		}

		li {
			cursor: pointer
		}
		.ui-state-default{
			border:0px!important;
		}
		.ui-state-default a{
			color:#337ab7!important
		}
		.connectedSortable {
			min-height: 10px;
		}

		.spinner {
			width: 100px;
		}

		.spinner input {
			text-align: right;
		}

		.input-group-btn-vertical {
			position: relative;
			white-space: nowrap;
			width: 1%;
			vertical-align: middle;
			display: table-cell;
		}

		.input-group-btn-vertical > .btn {
			display: block;
			float: none;
			width: 100%;
			max-width: 100%;
			padding: 8px;
			margin-left: -1px;
			position: relative;
			border-radius: 0;
		}

		.input-group-btn-vertical > .btn:first-child {
			border-top-right-radius: 4px;
		}

		.input-group-btn-vertical > .btn:last-child {
			margin-top: -2px;
			border-bottom-right-radius: 4px;
		}

		.input-group-btn-vertical i {
			position: absolute;
			top: 0;
			left: 4px;
		}

		#headerBuiltInVariables {
			cursor: pointer
		}
	</style>
	<!--<div class="container">
		<div class="row">
			<h1>Menu Setting</h1>
		</div>
	</div>-->
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Content Management<br>
                    <small>Create and modify menu and custom pages</small></h1>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>
    <div class="body-content-wrapper">
	    <div class="container">
		<div class="row">
			<div class="col-md-3 mdl-box-shadow bg-white padded-15">
				<ul class="list-unstyled" id="menu">
					<asp:Repeater ID="tree" runat="server" OnItemDataBound="tree_ItemDataBound" OnItemCommand="tree_ItemCommand">
						<ItemTemplate>
							<li class="parent" data-id="<%#Eval("NodeId") %>">
								<asp:LinkButton runat="server" CommandArgument='<%#Eval("NodeId") %>' CommandName="Click"><%# Eval("Text") %></asp:LinkButton>
								<ul id="subNavList" runat="server" class="connectedSortable">
									<asp:Repeater ID="subTree" runat="server" OnItemDataBound="subTree_ItemDataBound" OnItemCommand="tree_ItemCommand">
										<ItemTemplate>
											<li class="ui-state-default" data-id="<%#Eval("NodeId") %>" data-iscms="<%#Eval("IsCMS") %>">
												<asp:LinkButton runat="server" CommandArgument='<%#Eval("NodeId") %>' CommandName="Click"><%# Eval("Text") %></asp:LinkButton>
											</li>
										</ItemTemplate>
									</asp:Repeater>
								</ul>
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
			</div>
			<div class="col-md-9">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="cbLan">Language:</label>
						<div class="col-sm-4">
							<select class="form-control" id="cbLan" runat="server">
							</select>
						</div>
						<div class="col-sm-6">
							<asp:Button runat="server" ID="btnResetMenuCache" OnClick="btnResetMenuCache_Click" CssClass="btn btn-default" Text="Reset Menu Cache" />
							<%--<input type="button" id="resetOrder" class="btn btn-default" value="Change Order" />--%>
							<asp:Button runat="server" ID="saveMenuSetting" OnClick="saveMenuSetting_Click" CssClass="btn btn-primary" Text="Save Menu Setting" />
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div id="headerBuiltInVariables" class="panel-heading">Built-in Variables for HTML</div>
					<div class="panel-body" id="bodyBuiltInVariables" style="display:none;">
						<table class="table table-bordered">
							<asp:Repeater ID="rptShortCode" runat="server">
								<ItemTemplate>
									<tr>
										<td>
											<%# DataBinder.Eval((System.Collections.Generic.KeyValuePair<string, string>)Container.DataItem,"Key") %>
										</td>
										<td>
											<%# DataBinder.Eval((System.Collections.Generic.KeyValuePair<string, string>)Container.DataItem,"Value") %>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</table>
					</div>
				</div>
				<div class="panel panel-default" id="menuSetting">
					<div class="panel-heading">Menu Detail</div>
					<div class="panel-body">
						<br />
						<input id="hdId" type="hidden" runat="server" />
						<input id="dealer" type="hidden" runat="server" value="1" />
						<div class="form-horizontal" style="padding-left: 10px;">
							<div class="form-group" runat="server" id="divParent">
								<label class="control-label col-sm-2" for="cbParent">Parent:</label>
								<div class="col-sm-4">
									<select class="form-control" runat="server" id="cbParent"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="name">Name:</label>
								<div class="col-sm-10">
									<input type="text" runat="server" class="form-control" id="name" placeholder="Enter Name" name="name">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="title">Title:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" runat="server" id="title" placeholder="Enter Title" name="title">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="description">Description:</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="description" runat="server" placeholder="Enter Description" name="description"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="url">Url:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" runat="server" id="url" placeholder="Enter Url" name="url">
								</div>
							</div>
							<div class="form-group" runat="server" id="errorMessage">
								<label class="control-label col-sm-2" for="url"></label>
								<div class="col-sm-10">
									<label style="color: red" id="error" runat="server"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="dealerName">Dealer:</label>
								<div class="col-sm-10">
									<%--<input class="form-control" id="dealer" runat="server" placeholder="Enter Dealer" name="dealer">--%>
									<label style="margin-top: 8px;" id="dealerName" runat="server">Gato</label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="order">Display Order:</label>
								<div class="col-sm-10">
									<div class="input-group spinner">
										<input type="text" class="form-control" runat="server" value="0" id="order" placeholder="Enter Order" name="order">
										<div class="input-group-btn-vertical">
											<button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
											<button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
										</div>
										<%--<input type="text" class="form-control" runat="server" id="order" placeholder="Enter Order" name="order">--%>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckIsActive">Active:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckIsActive" type="checkbox" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckShowOnMenu">Show On Menu:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckShowOnMenu" type="checkbox" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckShowOnSiteMap">SiteMap:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckShowOnSiteMap" type="checkbox" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckShowOnSiteMapXML">SiteMap XML:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckShowOnSiteMapXML" type="checkbox" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckIsAdminOnly">Admin Only:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckIsAdminOnly" type="checkbox" />
									</div>
								</div>
								<%--<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<asp:Button ID="btnAddMenu" runat="server" OnClick="btnAddMenu_Click" CssClass="btn btn-default" Text="Save Menu" />
										<asp:Button runat="server" ID="btnClear" OnClick="btnClear_Click" CssClass="btn btn-default" Text="Clear" />
									</div>
								</div>--%>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">Content Page</div>
						<div class="panel-body">
							<br />
							<input type="hidden" id="hdContentPageId" runat="server" />
							<div class="form-horizontal">
								<div class="form-group">
									<label class="control-label col-sm-2" for="name">Title:</label>
									<div class="col-sm-10">
										<input type="text" runat="server" class="form-control" id="contentPageTitle" placeholder="Enter Title" name="contentPageTitle">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="contentPageDescription">Description:</label>
									<div class="col-sm-9">
										<textarea class="form-control" id="contentPageDescription" runat="server" placeholder="Enter Description" name="contentPageDescription"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="canonical">Canonical:</label>
									<div class="col-sm-10">
										<input type="text" runat="server" class="form-control" id="canonical" placeholder="Enter Canonical" name="canonical">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ampLink">AmpLink:</label>
									<div class="col-sm-10">
										<input type="text" runat="server" class="form-control" id="ampLink" placeholder="Enter AmpLink" name="ampLink">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="ckIsActiveContentPage">Is Active:</label>
									<div class="col-sm-9">
										<input runat="server" id="ckIsActiveContentPage" type="checkbox" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="bodyContent">Body Content:</label>
									<div class="col-sm-9">
										<textarea class="form-control" id="bodyContent" rows="10" runat="server" placeholder="Enter Body Content" name="contentPageDescription"></textarea>
									</div>
								</div>
                                <div class="form-group">
									<label class="control-label col-sm-2" for="bodyContentBottom">Body Content After Inventory:</label>
									<div class="col-sm-9">
										<textarea class="form-control" id="bodyContentBottom" rows="10" runat="server" placeholder="Enter Bottom Body Content"></textarea>
									</div>
								</div>
                                <div class="form-group">
									<label class="control-label col-sm-2" for="chkShowInventory">Show Inventory:</label>
									<div class="col-sm-9">
										<input runat="server" id="chkShowInventory" type="checkbox" />
									</div>
								</div>
                                <div class="form-group">
									<label class="control-label col-sm-2" for="inventoryFilters">Inventory Filters:</label>
									<div class="col-sm-9">
										<textarea  runat="server" class="form-control"  rows="2" id="inventoryFilters" placeholder="Type=new&Make=gmc&Model=Terrain&Years=2019,2018&Keywords=3GKALVEX9KL169877" name="inventoryFilters"></textarea>
                                        <span style="font-size:12px">(i.e. Type=new&Make=gmc&Model=Terrain&Years=2019,2018&Keywords=3GKALVEX9KL169877)</span>
									</div>
								</div>
							<%--	<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<asp:Button runat="server" ID="btnAddContentPage" OnClick="btnAddContentPage_Click" CssClass="btn btn-default" Text="Save Content" />
									</div>
								</div>--%>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">Content Page Post Data(Optional)</div>
						<div class="panel-body">
							<br />
							<input type="hidden" id="hdContentPagePostDataId" runat="server" />
							<div class="form-horizontal">
								<div class="form-group">
									<label class="control-label col-sm-2" for="emailField">Email Field:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="emailField" placeholder="Enter Email Field" name="emailField">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="userField">User Field:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="userField" placeholder="Enter User Field" name="userField">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="url">Post Title:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="postTitle" placeholder="Post Title" name="postTitle">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="postToMethod">Post To Method:</label>
									<div class="col-sm-4">
										<select class="form-control" id="cbPostToMethod" runat="server">
											<option>Email</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="emailTo">Email To:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="emailTo" placeholder="Email To" name="emailTo">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="emailCC">Email CC:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="emailCC" placeholder="Email CC" name="emailCC">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="emailBCC">Email BCC:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" runat="server" id="emailBCC" placeholder="Email BCC" name="emailBCC">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="dataFormat">Data Format:</label>
									<div class="col-sm-9">
										<textarea rows="5" type="text" class="form-control" runat="server" id="dataFormat" placeholder="Data Format" name="dataFormat"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-2" for="responseText">Response Text:</label>
									<div class="col-sm-9">
										<textarea type="text" rows="5" class="form-control" runat="server" id="responseText" placeholder="Response Text" name="responseText"></textarea>
									</div>
								</div>
								<%--<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<asp:Button runat="server" ID="btnAddPostData" OnClick="btnAddPostData_Click" CssClass="btn btn-default" Text="Save Post Data" />
									</div>
								</div>--%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
    <div class="clear-20"></div>
</asp:Content>
