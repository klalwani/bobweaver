﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.WebForm.Infrastructure.Filters;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    [CustomAuthorize(Roles = "Admin;Dealer")]
    public partial class EmployeeDashboard : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
				{
					Response.Redirect("~/Account/Login", false);
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}