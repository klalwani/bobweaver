﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditDealerInfoPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.EditDealerInfoPage" %>

<%@ Register Src="~/Views/UserControl/Admin/EditDealerInfoControl.ascx" TagPrefix="EditDealerInfo" TagName="EditDealerInfo" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Edit Dealer Info</h1>
            </div>
        </div>
    </div>
    <div id="dealerInfo" class="col-xs-12">
        <EditDealerInfo:EditDealerInfo runat="server" />
    </div>
</asp:Content>
