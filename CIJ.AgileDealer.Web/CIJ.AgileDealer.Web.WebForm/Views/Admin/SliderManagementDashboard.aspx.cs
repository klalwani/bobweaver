﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Home;
using AjaxControlToolkit;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class SliderManagementDashboard : BasedPage
    {
        private SlideHelper _slideHelper;

        public SlideHelper SlideHelper
        {
            get
            {
                if (_slideHelper == null)
                    _slideHelper = new SlideHelper(DealerID);
                return _slideHelper;
            }
            set { _slideHelper = value; }
        }


        private const int PageSize = 15;
        private int _skip = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
				{
					Response.Redirect("~/Account/Login", false);
				}

				if (!IsPostBack)
				{
					LoadHomePageSlides(DealerID);
					Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        private void LoadHomePageSlides(int dealerId)
        {
            grdSlide.DataSource = SlideHelper.GetHomePageSlides(_skip, PageSize,dealerId);
            grdSlide.VirtualItemCount = SlideHelper.GetHomePageSlideCount(dealerId);
            grdSlide.DataBind();
        }

        protected void grdSlide_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string item = e.Row.Cells[2].Text;
                foreach (LinkButton link in e.Row.Cells[9].Controls.OfType<LinkButton>())
                {
                    if (link.CommandName == "Delete")
                    {
                        link.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item +
                                                       "?')){ return false; };";
                    }
                }
            }
        }

        protected void grdSlide_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
           // if (Session["update"].ToString() == ViewState["update"].ToString())
            //{
                int id = Convert.ToInt32(e.Keys["SlideId"]);
                SlideHelper.DeleteHomePageSlide(id, DealerID);
                LoadHomePageSlides(DealerID);
                Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
            //}
        }

        protected void grdSlide_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            _skip = (e.NewPageIndex) * PageSize;
            grdSlide.PageIndex = e.NewPageIndex;
            grdSlide.DataSource = SlideHelper.GetHomePageSlides(_skip, PageSize,DealerID);
            grdSlide.DataBind();
        }

        private void ClearValues()
        {
            txtSlideImageName.Text = string.Empty;
            txtSlideImagePath.Text = string.Empty;
            txtSlideLink.Text = string.Empty;
            //txtSlideNumber.Text = string.Empty;
            txtSlideRelativeUrl.Text = string.Empty;
            ShowEditSection.Value = "false";
            SlideId.Value = string.Empty;
            btnSave.Value = "Add";
        }

        private void CreateHomePageSlide()
        {
            if (!string.IsNullOrEmpty(txtSlideImageName.Text)
                && !string.IsNullOrEmpty(txtSlideImagePath.Text) && !string.IsNullOrEmpty(txtSlideLink.Text))
            {
                HomePageSlide homePageSlide = SlideHelper.CreateSlider(txtSlideImageName.Text, txtSlideImagePath.Text, txtSlideLink.Text, txtSlideRelativeUrl.Text, DealerID);
                if (homePageSlide != null)
                {
                    SlideHelper.SaveHomePageSlide(homePageSlide);
                }
            }
        }

        private void UpdateHomePageSlide()
        {
            if (!string.IsNullOrEmpty(txtSlideImageName.Text)
                && !string.IsNullOrEmpty(txtSlideImagePath.Text) && !string.IsNullOrEmpty(txtSlideLink.Text))
            {
                int id = int.Parse(SlideId.Value);
                SlideHelper.UpdateHomePageSlide(id, txtSlideImageName.Text, txtSlideImagePath.Text, txtSlideLink.Text, txtSlideRelativeUrl.Text, DealerID);
            }
        }

        private void MapSlideInfoForEdit(int id)
        {
            HomePageSlide homePageSlide = SlideHelper.GetHomePageSlideById(id, DealerID);
            if (homePageSlide != null)
            {
                ShowEditSection.Value = "true";
                SlideId.Value = homePageSlide.SlideId.ToString();
                txtSlideImageName.Text = homePageSlide.SlideImageName;
                txtSlideImagePath.Text = homePageSlide.SlideImagePath;
                txtSlideLink.Text = homePageSlide.SlideLink;
                txtSlideRelativeUrl.Text = homePageSlide.SlideRelativeUrl;
            }
        }

        protected void btnSave_OnServerClick(object sender, EventArgs e)
        {
            if (Session["update"].ToString() == ViewState["update"].ToString())
            {
                if (ShowEditSection.Value == "true")
                {
                    UpdateHomePageSlide();
                }
                else
                {
                    CreateHomePageSlide();
                }
                ClearValues();
                LoadHomePageSlides(DealerID);
                Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
            }
        }

        protected void btnClear_OnServerClick(object sender, EventArgs e)
        {
            ClearValues();
        }

        protected void grdSlide_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (Session["update"].ToString() == ViewState["update"].ToString())
            {
                int id = Convert.ToInt32(e.CommandArgument);
                int newIndex = 0;

                if (e.CommandName == "UpdateSlide")
                {
                    btnSave.Value = "Update";

                    if (id != null && id > 0)
                    {
                        MapSlideInfoForEdit(id);
                    }
                }
                else if (e.CommandName == "Up")
                {
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = gvr.RowIndex;
                    newIndex = index - 1;
                    SlideHelper.UpdateHomePageSlideOrder(id, index, newIndex, "Up",DealerID);
                    LoadHomePageSlides(DealerID);
                }

                else if (e.CommandName == "Down")
                {
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int index = gvr.RowIndex;
                    newIndex = index + 1;
                    SlideHelper.UpdateHomePageSlideOrder(id, index, newIndex, "Down", DealerID);
                    LoadHomePageSlides(DealerID);
                }

                Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ViewState["update"] = Session["update"];
        }
    }
}