﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class CustomRedirectInfo : BasedPage
    {
        private CustomRedirectInfoHelper _customRedirectInfoHelper;

        public CustomRedirectInfoHelper CustomRedirectInfoHelper
        {
            get
            {
                if (_customRedirectInfoHelper == null)
                    _customRedirectInfoHelper = new CustomRedirectInfoHelper();
                return _customRedirectInfoHelper;
            }
            set { _customRedirectInfoHelper = value; }
        }

        private const int PageSize = 15;
        private int _skip = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!IsPostBack)
				{
					LoadCustomRedirectData();
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        private void LoadCustomRedirectData()
        {
            grdCustomRedirect.DataSource = CustomRedirectInfoHelper.GetCustomRedirectUrls(_skip, PageSize, DealerID);
            grdCustomRedirect.VirtualItemCount = CustomRedirectInfoHelper.GetCustomRedirectCount(DealerID);
            grdCustomRedirect.DataBind();
        }

        protected void grdCustomRedirect_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            _skip = (e.NewPageIndex) * PageSize;
            grdCustomRedirect.PageIndex = e.NewPageIndex;
            grdCustomRedirect.DataSource = CustomRedirectInfoHelper.GetCustomRedirectUrls(_skip, PageSize, DealerID);
            grdCustomRedirect.DataBind();
        }

        protected void grdCustomRedirect_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = grdCustomRedirect.Rows[e.RowIndex];
            int id = Convert.ToInt32(e.Keys["CustomRedirectUrlId"]);
            CustomRedirectInfoHelper.DeleteCustomRedirectItem(id,DealerID);
            LoadCustomRedirectData();
        }

        protected void grdCustomRedirect_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string item = e.Row.Cells[1].Text;
                foreach (LinkButton link in e.Row.Cells[5].Controls.OfType<LinkButton>())
                {
                    if (link.CommandName == "Delete")
                    {
                        link.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item +
                                                       "?')){ return false; };";
                    }
                }
            }
        }

        protected void btnSave_OnServerClick(object sender, EventArgs e)
        {
            CreateCustomRedirectInfo();
            ClearValues();
            LoadCustomRedirectData();
        }

        protected void btnClear_OnServerClick(object sender, EventArgs e)
        {
            ClearValues();
        }

        private void ClearValues()
        {
            txtName.Text = string.Empty;
            txtNewUrl.Text = string.Empty;
            txtOrlUrl.Text = string.Empty;
            isPermanentRedirect.Checked = false;
        }

        private void CreateCustomRedirectInfo()
        {
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtNewUrl.Text) &&
                !string.IsNullOrEmpty(txtOrlUrl.Text))
            {
                CustomRedirectUrl newItem = CustomRedirectInfoHelper.CreateCustomRedirectInfo(txtName.Text, txtOrlUrl.Text, txtNewUrl.Text, isPermanentRedirect.Checked, DealerID);
                if (newItem != null)
                {
                    CustomRedirectInfoHelper.SaveCustomRedirectUrl(newItem);
                }
            }
        }
    }
}