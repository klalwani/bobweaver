﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SliderManagementDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.SliderManagementDashboard" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $(document).ready(function () {
            var isShowEditSection = $('[id$="ShowEditSection"]').val();
            if (isShowEditSection == 'true')
                $('#slideupdate').toggleClass('in');
            //$('.btnEditSlide').click(function () {

            //});
        });
    </script>

    <div id="slideManagementDashboard" class="container">
        <div id="vehicle-segment-page">
            <div class="body-content-wrapper">
                <div class="container-fluid">
                    <div class="row form-title">
                        <span>Slide Dashboard</span>

                        <input type="button" id="btnAddNewSlide" name="btnAddNewSlide" value="Add New Slide" class="btn btn-success" data-toggle="collapse" data-parent="#accordion-add" href="#slideupdate" />
                    </div>
                    <asp:GridView ID="grdSlide" UseAccessibleHeader="True" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-hover table-bordered table-striped"
                        DataKeyNames="SlideId" AllowPaging="True" PageSize="15" AllowCustomPaging="True" OnRowDataBound="grdSlide_OnRowDataBound"
                        OnRowDeleting="grdSlide_OnRowDeleting" OnPageIndexChanging="grdSlide_OnPageIndexChanging" OnRowCommand="grdSlide_OnRowCommand">
                        <Columns>
                            <asp:BoundField DataField="SlideId" HeaderText="ID" ReadOnly="True" Visible="False" />
                            <asp:BoundField DataField="SlideNumber" HeaderText="Order" ReadOnly="True" Visible="False" />
                            <%--<asp:BoundField DataField="SlideName" HeaderText="Slide Name" SortExpression="Slide Name">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>--%>
                            <asp:BoundField DataField="SlideImageName" HeaderText="Slide Image Name" SortExpression="SlideImageName">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SlideImagePath" HeaderText="Slide Image Path" SortExpression="SlideImagePath">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SlideLink" HeaderText="Slide Link" SortExpression="SlideLink">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SlideRelativeUrl" HeaderText="Slide Relative Url" SortExpression="SlideRelativeUrl">
                                <ItemStyle CssClass="break-word" />
                            </asp:BoundField>
                            
                            <asp:TemplateField HeaderText="Up">
                                <ItemTemplate>
                                    <asp:Button ID="btnUp" runat="server"
                                        CausesValidation="false"
                                        CommandName="Up" Text="↑" CommandArgument='<%# Eval("SlideId") %>'  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Down">
                                <ItemTemplate>
                                    <asp:Button ID="btnDown" runat="server"
                                        CausesValidation="false"
                                        CommandName="Down" Text="↓" CommandArgument='<%# Eval("SlideId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditSlide" CssClass="btnEditSlide" ClientIDMode="Static" runat="server" CausesValidation="false" CommandName="UpdateSlide"
                                        Text="Edit" CommandArgument='<%# Eval("SlideId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Link" />
                        </Columns>
                        <HeaderStyle BorderWidth="0" Font-Bold="True" ForeColor="Black" />
                    </asp:GridView>
                </div>
            </div>
        </div>

        <div id="slideupdate" clientidmode="Static" runat="server" class="addSection col-lg-12 mdl-white-box margintop10 panel-collapse collapse ">
            <div class="clear-20"></div>
            <div class="row form-wrapper">
                <div class="col-md-12">
                    <div class="row form-horizontal-input-padded">

                        <div class="col-md-12">
                           <%-- <div class="row content-padded">
                                <label class="col-md-2 control-label">Slide Name</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtName" runat="server" />
                                </fieldset>
                            </div>--%>
                            <%--<div class="row content-padded">
                                <label class="col-md-2 control-label">Order</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtSlideNumber" runat="server" />
                                </fieldset>
                            </div>--%>
                            <div class="row content-padded">
                                <label class="col-md-2 control-label">Slide Image Name</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtSlideImageName" runat="server" />
                                </fieldset>
                            </div>
                            <div class="row content-padded">
                                <label class="col-md-2 control-label">Slide Image Path</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtSlideImagePath" runat="server" />
                                </fieldset>
                            </div>
                            <div class="row content-padded">
                                <label class="col-md-2 control-label">Slide Link</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtSlideLink" runat="server" />
                                </fieldset>
                            </div>
                            <div class="row content-padded">
                                <label class="col-md-2 control-label">Slide Relative Url</label>
                                <fieldset class="form-group">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" CssClass="form-control" ID="txtSlideRelativeUrl" runat="server" />
                                </fieldset>
                            </div>
                            <div class="row label-group text-center">
                                <input type="button" runat="server" class="btn btn-primary" id="btnSave" onserverclick="btnSave_OnServerClick" title="Add" value="Add" />
                                <input type="button" runat="server" class="btn btn-primary" id="btnClear" onserverclick="btnClear_OnServerClick" title="Click to clear the form" value="Cancel" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear-20"></div>
        </div>
    </div>
    <div class="clear-20"></div>
    <asp:HiddenField runat="server" ID="ShowEditSection" Value="false" />
    <asp:HiddenField runat="server" ID="SlideId" Value="0" />
</asp:Content>
