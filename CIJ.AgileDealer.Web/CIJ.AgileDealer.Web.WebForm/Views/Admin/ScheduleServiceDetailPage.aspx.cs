﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using AgileDealer.Data.Entities.ScheduleService;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class ScheduleServiceDetailPage : BasedPage
    {
        private List<ScheduleTime> _scheduleTimeNormal;
        private List<ScheduleTime> ScheduleTimeNormal
        {
            get
            {
                if (_scheduleTimeNormal == null)
                {
                    _scheduleTimeNormal = MasterDataHelper.GetScheduleTime(false, DealerID);
                }

                return _scheduleTimeNormal;
            }
            set { _scheduleTimeNormal = value; }
        }

        private List<ScheduleTime> _scheduleTimeSaturday;
        private List<ScheduleTime> ScheduleTimeSaturday
        {
            get
            {
                if (_scheduleTimeSaturday == null)
                {
                    _scheduleTimeSaturday = MasterDataHelper.GetScheduleTime(true, DealerID);
                }

                return _scheduleTimeSaturday;
            }
            set { _scheduleTimeSaturday = value; }
        }

        private List<ServiceRequestedModel> _serviceRequested;
        private List<ServiceRequestedModel> ServiceRequested
        {
            get
            {
                if (_serviceRequested == null)
                {
                    _serviceRequested = MasterDataHelper.GetServiceRequested(LanguageId, DealerID);
                }

                return _serviceRequested;
            }
            set { _serviceRequested = value; }
        }

        private List<CurrentIssueModel> _currentIssue;
        private List<CurrentIssueModel> CurrentIssue
        {
            get
            {
                if (_currentIssue == null)
                {
                    _currentIssue = MasterDataHelper.GetCurrentIssue(LanguageId, DealerID);
                }

                return _currentIssue;
            }
            set { _currentIssue = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
                if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
                {
                    Response.Redirect("~/Account/Login", false);
                }
                BindingMasterData();
				if (!string.IsNullOrEmpty(Page.RouteData.Values["scheduleServiceId"] as string))
				{
					scheduleServiceId.Value = Page.RouteData.Values["scheduleServiceId"] as string;
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        private void BindingMasterData()
        {
            rptScheduleTimeNormal.DataSource = ScheduleTimeNormal;
            rptScheduleTimeNormal.DataBind();

            rptScheduleTimeSaturday.DataSource = ScheduleTimeSaturday;
            rptScheduleTimeSaturday.DataBind();

            rptServiceRequested.DataSource = ServiceRequested;
            rptServiceRequested.DataBind();

            rptCurrentIssue.DataSource = CurrentIssue;
            rptCurrentIssue.DataBind();
        }
    }
}