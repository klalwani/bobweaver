﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Async="true"  AutoEventWireup="true" CodeBehind="VehicleOptionsDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.VehicleOptionsDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/VehicleOptionsDashboardControl.ascx" TagPrefix="VehicleOptionsDashboard" TagName="VehicleOptionsDashboardControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Vehicle Option code management</h1>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <VehicleOptionsDashboard:VehicleOptionsDashboardControl runat="server" ID="VehicleOptionsDashboardControl" />
    </div>
    <div class="clear-20"></div>
</asp:Content>
