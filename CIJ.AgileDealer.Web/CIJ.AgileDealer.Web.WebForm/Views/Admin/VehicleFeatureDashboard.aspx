﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Async="true"  AutoEventWireup="true" CodeBehind="VehicleFeatureDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.VehicleFeatureDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/VehicleFeatureDashboardControl.ascx" TagPrefix="VehicleFeatureDashboard" TagName="VehicleFeatureDashboardControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Inventory Feature Management<br /><small>Display, hide, or rename vehicle features</small></h1>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <VehicleFeatureDashboard:VehicleFeatureDashboardControl runat="server" ID="VehicleFeatureDashboardControl" />
    </div>
    <div class="clear-20"></div>
</asp:Content>
