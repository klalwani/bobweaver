﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="VehicleImageManager.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.VehicleImageManager" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="leadDashboards" class="container">
        <div id="VehicleImageManagerApp"></div>
    </div>
    <%--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--%>
<%--    <script src="../../Scripts/Pages/Admin/VehicleImageManager.js"></script>--%>
    <script src="../../vehicleimagemanager/content/dist/js/app.js"></script>
    <link href="../../vehicleimagemanager/content/dist/css/app.css" rel="stylesheet" />
</asp:Content>
