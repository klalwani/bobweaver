﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactUsDetail.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.ContactUsDetail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/Admin/AgileDealer.ContactUsDetail.js"></script>
    <input type="hidden" id="IsArchived" />
    <div class="container" id="contactUsForm">
        <div class="container row standardForm">
            <div class="row form-title">
                <span>Contact Us Detail</span>
                <input type="button" id="btnBack" name="btnBack" value="Go Back" class="btn btn-default pull-right margin10 no-print" />
                <%--<input type="button" id="btnArchived" name="btnArchived" value="Archive Application" class="btn btn-default pull-right margin10 no-print" />--%>
            </div>
        </div>
        <div class="container row standardForm">
            <div class="col-md-12 col-xs-12">
                <div class="row content-padded">
                    <div class="row form-horizontal-input-padded">
                        <div id="contactName">
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="firstName" class="sr-only">First Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="firstName" placeholder="First Name">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="lastName" class="sr-only">Last Name</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="lastName" placeholder="Last Name">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="email" placeholder="Email">
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group rowElem">
                                    <label for="phoneNumber" class="sr-only">Phone Number</label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="phoneNumber" placeholder="Phone Number">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" rows="5" id="userMessage" placeholder="Message"></textarea>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<asp:HiddenField id="contactusId"  runat="server"/>
</asp:Content>
