﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class ContactUsDetail : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!string.IsNullOrEmpty(Page.RouteData.Values["contactusId"] as string))
				{
					contactusId.Value = Page.RouteData.Values["contactusId"] as string;
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}