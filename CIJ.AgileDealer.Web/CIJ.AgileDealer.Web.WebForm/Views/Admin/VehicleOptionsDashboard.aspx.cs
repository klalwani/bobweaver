﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class VehicleOptionsDashboard : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
                {
                    Response.Redirect("~/account/login");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }
    }
}