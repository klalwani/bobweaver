﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Competitives;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class VehicleSegment : BasedPage
    {
        private List<Segment> _segments;
        public List<Segment> Segments
        {
            get
            {
                if (_segments == null)
                    _segments = VehicleHelper.GetSegments(DealerID);
                return _segments;
            }
            set { _segments = value; }
        }

        private List<MasterData> _models;
        public List<MasterData> Models
        {
            get
            {
                if (_models == null)
                    _models = VehicleHelper.GetCompetitiveModelAsMasterData(DealerID);
                return _models;
            }
            set { _models = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
				{
					Response.Redirect("~/Account/Login",false);
				}
				GetMasterData();
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        private void GetMasterData()
        {
            if (Segments != null)
            {
                SegmentHdf.Value = JsonHelper.Serialize(Segments);
            }
            if (Models != null)
            {
                ModelHdf.Value = JsonHelper.Serialize(Models);
            }
        }
    }
}