﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities.Helpers;
using ApplicantModel = AgileDealer.Data.Entities.Applicants;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
    public partial class ApplicantDetail : BasedPage
    {
        private const int PageIndex = 2;
        private List<NotificationModel> _notifications;
        private List<NotificationModel> Notifications
        {
            get
            {
                if (_notifications == null)
                {
                    _notifications = MasterDataHelper.GetNotifications(PageIndex, LanguageId, DealerID);
                }

                return _notifications;
            }
            set { _notifications = value; }
        }

        private List<State> _states;
        private List<State> States
        {
            get
            {
                if (_states == null)
                {
                    _states = MasterDataHelper.GetStates(DealerID);
                }

                return _states;
            }
            set { _states = value; }
        }

        private List<ResidenceStatus> _residenceStatus;
        private List<ResidenceStatus> ResidenceStatuss
        {
            get
            {
                if (_residenceStatus == null)
                {
                    _residenceStatus = MasterDataHelper.GetResidenceStatus(DealerID);
                }

                return _residenceStatus;
            }
            set { _residenceStatus = value; }
        }
        private List<Year> _years;
        private List<Year> Years
        {
            get
            {
                if (_years == null)
                {
                    _years = MasterDataHelper.GetYears(DealerID);
                }

                return _years;
            }
            set { _years = value; }
        }
        private List<MonthModel> _months;
        private List<MonthModel> Months
        {
            get
            {
                if (_months == null)
                {
                    _months = MasterDataHelper.GetMonths(LanguageId, DealerID);
                }

                return _months;
            }
            set { _months = value; }
        }
        private List<Date> _dates;
        private List<Date> Dates
        {
            get
            {
                if (_dates == null)
                {
                    _dates = MasterDataHelper.GetDates(DealerID);
                }

                return _dates;
            }
            set { _dates = value; }
        }

        private GridView gridView;

        public GridView GridView
        {
            get
            {
                if (gridView == null)
                {
                    gridView = new GridView();
                    gridView.RowDataBound -= GridViewOnRowDataBound;
                    gridView.RowDataBound += GridViewOnRowDataBound;
                }
                return gridView;
            }
            set { gridView = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
				{
					Response.Redirect("~/Account/Login", false);
				}

				BindingMasterData();
				string applicantId = Page.RouteData.Values["ApplicantId"] as string;
				if (!string.IsNullOrEmpty(applicantId))
				{
					ApplicantId.Value = applicantId;
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
        private void BindingMasterData()
        {
            rptState.DataSource = States;
            rptState.DataBind();

            rptResidenceStatus.DataSource = ResidenceStatuss;
            rptResidenceStatus.DataBind();

            rptResidenceStatus2.DataSource = ResidenceStatuss;
            rptResidenceStatus2.DataBind();

            rptState2.DataSource = States;
            rptState2.DataBind();

            rptState3.DataSource = States;
            rptState3.DataBind();

            rptState4.DataSource = States;
            rptState4.DataBind();

            rptYear.DataSource = Years;
            rptYear.DataBind();

            rptMonth.DataSource = Months;
            rptMonth.DataBind();

            rptDate.DataSource = Dates;
            rptDate.DataBind();


            rptYear2.DataSource = Years;
            rptYear2.DataBind();

            rptMonth2.DataSource = Months;
            rptMonth2.DataBind();

            rptDate2.DataSource = Dates;
            rptDate2.DataBind();
        }

        protected void btnPrintApplicantDetailServer_OnServerClick(object sender, EventArgs e)
        {


            int applicantId = int.Parse(ApplicantId.Value);
            ApplicantModel.ApplicantDetail applicantDetail = ApplicantHelper.GetApplicantByApplicantId(applicantId);

            if (applicantDetail != null)
            {
                Response.AddHeader("content-disposition",
                    "attachment; filename=" + applicantDetail.Applicant.FirstName + "_" + applicantDetail.Applicant.LastName + ".xls");
                Response.ContentType = "application/excel";

                StringWriter output = ExportToExcel(applicantDetail);
                if (output != null)
                {
                    HttpContext.Current.Response.Write(output);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
        }

        private List<KeyValuePair<string, string>> GenerateRow<T>(T model)
        {
            List<KeyValuePair<string, string>> dictionaryData = new List<KeyValuePair<string, string>>();
            if (model != null)
            {
                var props = model.GetType().GetFilteredProperties();
                foreach (var column in props)
                {
                    string columnName = column.Name;
                    string columnValue = string.Empty;
                    if (column.GetValue(model) != null)
                    {
                        columnValue = column.GetValue(model).ToString();
                    }
                    dictionaryData.Add(new KeyValuePair<string, string>(columnName, columnValue)); // Add Column Name as key and Column Data As Value
                }
            }
            return dictionaryData;
        }

        private void AddFieldValue(List<KeyValuePair<string, string>> source, List<KeyValuePair<string, string>> target)
        {
            if (source.Any())
                foreach (KeyValuePair<string, string> data in source)
                {
                    target.Add(new KeyValuePair<string, string>(data.Key, data.Value));
                }
        }

        public StringWriter ExportToExcel(ApplicantModel.ApplicantDetail applicantDetail)
        {
            List<KeyValuePair<string, string>> dictionaryData = new List<KeyValuePair<string, string>>();
            if (applicantDetail != null)
            {
                List<KeyValuePair<string, string>> tempData = new List<KeyValuePair<string, string>>();
                //Applicant
                dictionaryData.Add(new KeyValuePair<string, string>("Instantly Pre-Qualify", "Title"));
                tempData = GenerateRow(applicantDetail.Applicant);
                AddFieldValue(tempData, dictionaryData);

                //ContactInfo
                dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                dictionaryData.Add(new KeyValuePair<string, string>("Contact Info", "Title"));
                tempData = GenerateRow(applicantDetail.ContactInfo);
                AddFieldValue(tempData, dictionaryData);

                //Applicant Info
                dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                dictionaryData.Add(new KeyValuePair<string, string>("Applicant Info", "Title"));
                tempData = GenerateRow(applicantDetail.ApplicantInfo);
                AddFieldValue(tempData, dictionaryData);

                //Employement Info
                dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                dictionaryData.Add(new KeyValuePair<string, string>("Employment Info", "Title"));
                tempData = GenerateRow(applicantDetail.EmploymentInfo);
                AddFieldValue(tempData, dictionaryData);

                if (applicantDetail.ContactInfo.NumberOfApplicant != (int)ApplicantEnums.Individual)
                {
                    //Employement Info
                    dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                    dictionaryData.Add(new KeyValuePair<string, string>("Joined Contact Info", "Title"));
                    tempData = GenerateRow(applicantDetail.JoinedContactInfo);
                    AddFieldValue(tempData, dictionaryData);

                    //Employement Info
                    dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                    dictionaryData.Add(new KeyValuePair<string, string>("Joined Applicant Info", "Title"));
                    tempData = GenerateRow(applicantDetail.JoinedApplicantInfo);
                    AddFieldValue(tempData, dictionaryData);

                    //Employement Info
                    dictionaryData.Add(new KeyValuePair<string, string>(string.Empty, "Title"));
                    dictionaryData.Add(new KeyValuePair<string, string>("Joined Employment Info", "Title"));
                    tempData = GenerateRow(applicantDetail.JoinedEmploymentInfo);
                    AddFieldValue(tempData, dictionaryData);
                }
            }


            //Clean data
            GridView.DataSource = null;
            GridView.DataBind();

            GridView.DataSource = dictionaryData;
            GridView.DataBind();

            GridView.AlternatingRowStyle.HorizontalAlign = HorizontalAlign.Left;
            GridView.ShowHeader = false;
            GridView.AutoGenerateColumns = false;


            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            GridView.RenderControl(htw);

            return sw;
        }

        private void GridViewOnRowDataBound(object sender, GridViewRowEventArgs gridViewRowEventArgs)
        {
            GridViewRow row = gridViewRowEventArgs.Row;
            if (row.DataItem != null)
            {
                if (((KeyValuePair<string, string>) row.DataItem).Value == "Title")
                {
                    row.Cells[0].Font.Bold = true;
                    row.Cells[0].BackColor = Color.Yellow;
                    row.Cells[1].BackColor = Color.Yellow;
                    row.Cells[1].Text = string.Empty;
                }

                row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            }
            else
            {
                row.Visible = false;
            }
        }
    }
}