﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserAccountDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.UserAccountDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/UserAccountControl.ascx" TagPrefix="UserAccountControl" TagName="UserAccountControl" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <UserAccountControl:UserAccountControl runat="server" ID="UserAccountControl" />
</asp:Content>

