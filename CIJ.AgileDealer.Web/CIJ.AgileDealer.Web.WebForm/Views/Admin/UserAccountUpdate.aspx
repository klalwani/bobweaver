﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserAccountUpdate.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.UserAccountUpdate" %>

<%@ Register Src="~/Views/UserControl/Admin/UserAccountUpdateControl.ascx" TagPrefix="UserAccountUpdate" TagName="UserAccountUpdate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <UserAccountUpdate:UserAccountUpdate runat="server" id="UserAccountUpdateControl" />
</asp:Content>