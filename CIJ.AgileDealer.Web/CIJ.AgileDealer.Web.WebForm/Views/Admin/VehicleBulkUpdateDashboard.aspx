﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Async="true"  AutoEventWireup="true" CodeBehind="VehicleBulkUpdateDashboard.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.VehicleBulkUpdateDashboard" %>

<%@ Register Src="~/Views/UserControl/Admin/VehicleBulkUpdateDashboardControl.ascx" TagPrefix="VehicleBulkUpdateDashboard" TagName="VehicleBulkUpdateDashboardControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Vehicle Bulk Update Dashboard<br /><small>Change price override, msrp orverride</small></h1>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <VehicleBulkUpdateDashboard:VehicleBulkUpdateDashboardControl runat="server" ID="VehicleBulkUpdateDashboardControl" />
    </div>
    <div class="clear-20"></div>
</asp:Content>
