﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="StaffPageEdit.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Admin.StaffPageEdit" %>

<%@ Register Src="~/Views/UserControl/Admin/StaffEditControl.ascx" TagPrefix="staffEditControl" TagName="StaffEditControl" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Edit Staff Listing<br />
                    <small>Instantly update staff infos</small></h1>
            </div>
        </div>
    </div>

    <div id="employee-update" class="container mdl-box-shadow edit-vehicle-listing">
        <staffEditControl:StaffEditControl runat="server" ID="StaffEditControl" />
    </div>
    
</asp:Content>
