﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Localization;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Views.Admin
{
	public partial class MenuSetting : BasedPage
	{
        const string CONFIG_CS = "Config Content";

        public List<TreeviewModel> Menus { get; set; }
		public List<Language> Lans { get; set; }
		public List<MenuModel> Parents { get; set; }
		public Dictionary<string,string> SortCodes { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!Request.IsAuthenticated || (!User.IsInRole("Admin") && !User.IsInRole("Dealer")))
				{
					Response.Redirect("~/Account/Login", false);
				}
				if (!IsPostBack)
				{
					Menus = CMSHelper.GetMenus(DealerID,true).ToList();
					Lans = CMSHelper.GetAllLanguage(DealerID);
					Parents = CMSHelper.GetParents(DealerID);
					Parents.Insert(0, new MenuModel { }); // inject null parent
					BindingData();
					SetSortCode();
					rptShortCode.DataSource = SortCodes;
					rptShortCode.DataBind();
				}
				errorMessage.Visible = false;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
		}

		protected void tree_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var currentRepeaterNode = (TreeviewModel)e.Item.DataItem;
			var nodes = new List<TreeviewModel>();
			currentRepeaterNode.Nodes.ForEach(c => { nodes.Add(c); });
			var subTree = ((Repeater)e.Item.FindControl("subTree"));
			if (subTree != null)
			{
				subTree.DataSource = nodes;
				subTree.DataBind();
			}
		}

		protected void subTree_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{

		}

		protected void tree_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "Click")
			{
				ClearControl();
				var menuId = 0;
				var languageId = 0;
				int.TryParse(e.CommandArgument.ToString(), out menuId);
				int.TryParse(cbLan.Value, out languageId);
				var menu = CMSHelper.GetMenuById(menuId, languageId,DealerID);
				var contentPage = CMSHelper.GetContentPage(menuId, languageId, DealerID);
				var contentPagePostData = CMSHelper.GetContentPagePostData(menuId, DealerID);
				if (menu != null)
				{
					name.Value = menu.Name;
					title.Value = menu.Title;
					description.Value = menu.Description;
					url.Value = menu.Url;
					ckShowOnMenu.Checked = menu.ShowOnMenu;
					ckShowOnSiteMap.Checked = menu.ShowOnSiteMap;
					ckShowOnSiteMapXML.Checked = menu.ShowOnSiteMapXML;
					ckIsActive.Checked = menu.IsActive;
					ckIsAdminOnly.Checked = menu.IsAdminOnly;
					order.Value = menu.DisplayOrderId.ToString();
					dealer.Value = menu.DealerID.ToString();
					if (menu.ParentId != null)
					{
						SelectedIndex(cbParent, menu.ParentId.Value);
					}
					else
					{
						divParent.Visible = false;
					}
				}

				if (contentPage != null)
				{
					contentPageTitle.Value = contentPage.Title;
					contentPageDescription.Value = contentPage.Description;
					canonical.Value = contentPage.Canonical;
					ampLink.Value = contentPage.AmpLink;
					ckIsActiveContentPage.Checked = contentPage.IsActive;
					bodyContent.Value = contentPage.BodyContent;
                    bodyContentBottom.Value = contentPage.BodyContentBottom;
                    chkShowInventory.Checked = contentPage.ShowInventory;
                    inventoryFilters.Value = contentPage.InventoryFilters;
                    hdContentPageId.Value = contentPage.ContentPageId.ToString();
				}

				if (contentPagePostData != null)
				{
					emailField.Value = contentPagePostData.EmailField;
					userField.Value = contentPagePostData.UserEmailField;
					postTitle.Value = contentPagePostData.PostTitle;
					emailTo.Value = contentPagePostData.EmailTo;
					emailCC.Value = contentPagePostData.EmailCC;
					emailBCC.Value = contentPagePostData.EmailBCC;
					dataFormat.Value = contentPagePostData.DataFormat;
					responseText.Value = contentPagePostData.ResponseText;
					hdContentPagePostDataId.Value = contentPagePostData.ContentPagePostDataId.ToString();
				}
				hdId.Value = menuId.ToString();
			}
		}

		private void ClearControl()
		{
			name.Value = string.Empty;
			title.Value = string.Empty;
			description.Value = string.Empty;
			url.Value = string.Empty;
			ckIsActive.Checked = false;
			ckShowOnMenu.Checked = false;
			ckShowOnSiteMap.Checked = false;
			ckShowOnSiteMapXML.Checked = false;

			contentPageTitle.Value = string.Empty;
			contentPageDescription.Value = string.Empty;
			canonical.Value = string.Empty;
			ampLink.Value = string.Empty;
			ckIsActiveContentPage.Checked = false;
			bodyContent.Value = string.Empty;
			
			emailField.Value = string.Empty;
			userField.Value = string.Empty;
			postTitle.Value = string.Empty;
			emailTo.Value = string.Empty;
			emailCC.Value = string.Empty;
			dataFormat.Value = string.Empty;
			responseText.Value = string.Empty;
			emailBCC.Value = string.Empty;
		}

		private void SaveorUpdateMenu()
		{
			// menu
			var parentId = 0;
			var dealerId = 0;
			var displayOrderId = 0;
			var menuId = 0;
			var languageId = 0;
			int.TryParse(cbParent.Value, out parentId);
			int.TryParse(dealer.Value, out dealerId);
			int.TryParse(order.Value, out displayOrderId);
			int.TryParse(hdId.Value, out menuId);
			int.TryParse(cbLan.Value, out languageId);
			var menuModel = new MenuModel()
			{
				MenuId = menuId,
				ParentId = parentId,
				Name = name.Value,
				Title = title.Value,
				Description = description.Value,
				Url = url.Value,
				DealerID = dealerId,
				DisplayOrderId = displayOrderId,
				IsActive = ckIsActive.Checked,
				ShowOnMenu = ckShowOnMenu.Checked,
				ShowOnSiteMap = ckShowOnSiteMap.Checked,
				ShowOnSiteMapXML = ckShowOnSiteMapXML.Checked
			};

			if (menuModel.ParentId == 0)
				menuModel.ParentId = null;

			// content page
			var orderId = 0;
			var contentPageId = 0;
			int.TryParse(order.Value, out orderId);
			int.TryParse(hdContentPageId.Value, out contentPageId);
			var contentPageModel = new ContentPageModel()
			{
				Title = contentPageTitle.Value,
				Description = contentPageDescription.Value,
				Canonical = canonical.Value,
				AmpLink = ampLink.Value,
				//Order = orderId,
				IsActive = ckIsActiveContentPage.Checked,
				BodyContent = bodyContent.Value,
                BodyContentBottom= bodyContentBottom.Value,
                ShowInventory=chkShowInventory.Checked,
                InventoryFilters=inventoryFilters.Value.Trim(),
                MenuId = menuId,
				ContentPageId = contentPageId
			};

			// Post Data
			var contentPagePostDataId = 0;
			int.TryParse(hdContentPagePostDataId.Value, out contentPagePostDataId);
			var contentPagePostData = new ContentPagePostDataModel()
			{
				ContentPagePostDataId = contentPagePostDataId,
				EmailField = emailField.Value,
				UserEmailField = userField.Value,
				PostTitle = postTitle.Value,
				EmailTo = emailTo.Value,
				EmailCC = emailCC.Value,
				DataFormat = dataFormat.Value,
				ResponseText = responseText.Value,
				EmailBCC = emailBCC.Value,
				ContentPageId = contentPageId
			};

			CMSHelper.SaveOrUpdateMenuSetting(menuModel, contentPageModel, contentPagePostData, languageId,dealerId);
		}

		private void SelectedIndex(HtmlSelect select, int value)
		{
			foreach (ListItem item in select.Items)
			{
				item.Selected = false;
			}
			foreach (ListItem item in select.Items)
			{
				if (item.Value == value.ToString())
				{
					item.Selected = true;
					break;
				}
			}
		}

		private void BindingData()
		{
			tree.DataSource = Menus;
			tree.DataBind();
			cbLan.DataTextField = "Description";
			cbLan.DataValueField = "LanguageId";
			cbLan.DataSource = Lans;
			cbLan.DataBind();
			cbParent.DataTextField = "Title";
			cbParent.DataValueField = "MenuId";
			cbParent.DataSource = Parents;
			cbParent.DataBind();
			dealerName.InnerText = DealerInfoHelper.DealerName;
			dealer.Value = DealerInfoHelper.DealerID.ToString();
		}

		private void SetSortCode()
		{
			SortCodes = new Dictionary<string, string>
			{
				{ "{DealerName}", DealerName },
				{ "{DealerCity}", DealerCity },
				{ "{DealerState}", DealerState },
				{ "{DealerImageGeneric}", DealerImageGeneric },
				{ "{ManufacturerName}", ManufacturerName }
			};
		}

		protected void btnResetMenuCache_Click(object sender, EventArgs e)
		{
			SiteInfoHelper.ResetSiteInfoInstance();
			Response.RedirectToRoute(CONFIG_CS);
		}

		protected void saveMenuSetting_Click(object sender, EventArgs e)
		{
			SaveorUpdateMenu();
			Response.RedirectToRoute(CONFIG_CS);
		}
	}
}