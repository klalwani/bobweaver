﻿using System;
using System.Web.UI;

namespace CJI.AgileDealer.Web.WebForm.Views.Localization
{
    public partial class SetLanguage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string redirectUrl = "~/"; // default redirect to home page

            // avoid circulation
            if (Page.Request.UrlReferrer != null 
                && !Page.Request.UrlReferrer.AbsolutePath.StartsWith("setlanguage", StringComparison.OrdinalIgnoreCase))
            {
                redirectUrl = string.Format("~/{0}", Page.Request.UrlReferrer.AbsolutePath);
            }
            
            // set language in session variable 
            Session["CurrentUI"] = Page.Request.QueryString["lan"];
             
            // redirect back -- from now on the language will be based on session variable
            Response.Redirect(redirectUrl, true);
        }
    }
}