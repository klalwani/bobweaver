﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CJI.AgileDealer.Web.WebForm.Views.VCP {
    
    
    public partial class VcpPage {
        
        /// <summary>
        /// shortBlogDescriptionSection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl shortBlogDescriptionSection;
        
        /// <summary>
        /// shortBlogDescriptionContent control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl shortBlogDescriptionContent;
        
        /// <summary>
        /// RelateSection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl RelateSection;
        
        /// <summary>
        /// ltrPostText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrPostText;
        
        /// <summary>
        /// BlogEntryDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.VcpBaseModel BlogEntryDetail;
        
        /// <summary>
        /// BlogEntries control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.BlogEntryControl BlogEntries;
        
        /// <summary>
        /// SearchControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CJI.AgileDealer.Web.WebForm.Views.UserControl.SearchControl.SearchControl SearchControl;
        
        /// <summary>
        /// FatFooter control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CJI.AgileDealer.Web.WebForm.Views.UserControl.FatFooter.FatFooterControl FatFooter;
        
        /// <summary>
        /// CurrentTab control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CurrentTab;
        
        /// <summary>
        /// CurrentModel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CurrentModel;
        
        /// <summary>
        /// CurrentType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CurrentType;
    }
}
