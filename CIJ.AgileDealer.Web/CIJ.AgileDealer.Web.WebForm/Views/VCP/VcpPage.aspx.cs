﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.Ajax.Utilities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.VCP
{
    public partial class VcpPage : BasedPage
    {
        private string defaultTab => "inventoryTab";
        #region Properties

        //private Dealer Dealer = MasterDataHelper.GetDefaultDealer(_dealerID);

        private Model _model;
        public Model Model
        {
            get
            {
                if (_model != null) return _model;
                string modelString = Page.RouteData.Values["Model"].ToString();

                _model = MasterDataHelper.GetModelByModelTranslatedName(modelString, DealerID);
                if (_model == null && _modelTranslated != null)
                    _model = MasterDataHelper.GetModelById(ModelTranslated.ModelTranslatedId, DealerID);
                    
                return _model;
            }
            set { _model = value; }
        }

        private ModelTranslated _modelTranslated;
        public ModelTranslated ModelTranslated
        {
            get
            {
                if (_modelTranslated != null) return _modelTranslated;
                string modelString = Page.RouteData.Values["Model"].ToString();

                _modelTranslated = MasterDataHelper.GetModelByTranslatedName(modelString, DealerID);
                return _modelTranslated;
            }
            set { _modelTranslated = value; }
        }

        private Make _make;
        public Make Make
        {
            get
            {
                if (_make != null) return _make;
                string makeString = Page.RouteData.Values["Make"].ToString();
                _make = MasterDataHelper.GetMakeByName(makeString, DealerID);
                return _make;
            }
            set { _make = value; }
        }

        private string _type;
        public string Type
        {
            get
            {
                if (!string.IsNullOrEmpty(_type)) return _type;
                string typeString = Page.RouteData.Values["Type"].ToString();
                _type = typeString;
                return _type;
            }
            set { _type = value; }
        }

        public bool HasBlogBaseModel { get; set; }

        public DefaultFilter DefaultFilter
        {
            get
            {
                if (ViewState["DefaultFilter"] != null) return (DefaultFilter)ViewState["DefaultFilter"];
                DefaultFilter filters = new DefaultFilter
                {
                    DefaultTypes = new List<string> { Type },
                    DefaultModels = new List<int>() { ModelTranslated.ModelTranslatedId },
                    DefaultMakes = new List<int>() { Make.MakeId }
                };
                ViewState["DefaultFilter"] = filters;
                return (DefaultFilter)ViewState["DefaultFilter"];
            }
            set
            {
                ViewState["DefaultFilter"] = value;
            }
        }

        private FordVehicleContent _selectedVehicle;
        public FordVehicleContent SelectedVehicle
        {
            set { _selectedVehicle = value; }
            get
            {
                if (_selectedVehicle == null)
                    if (ModelTranslated != null && Type != null)
                        _selectedVehicle =
                            MasterDataHelper.GetFordVehicleContentByTypeAndModel(Type, ModelTranslated.Name, DealerID);
                return _selectedVehicle;

            }
        }

        public string VehicleImageUrl { get; set; }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ModelTranslated != null)
                {
                    int languageId = SiteInfoHelper.GetLanguageId();
                    int baseModelid = Type.ToLower() == "used"
                        ? (int) BlogTagEnums.BaseModelUsed
                        : (int) BlogTagEnums.BaseModel;
                    CurrentTab.Value = defaultTab;
                    CurrentModel.Value = ModelTranslated.Name;
                    CurrentType.Value = Type;
                    DefaultFilter filters = new DefaultFilter
                    {
                        DefaultTypes = new List<string> {Type},
                        DefaultModels = new List<int> {ModelTranslated != null ? ModelTranslated.ModelTranslatedId : 0},
                        DefaultModelTrims =
                            new List<VehicleModelTrim>()
                            {
                                new VehicleModelTrim()
                                {
                                    modelId = ModelTranslated != null ? ModelTranslated.ModelTranslatedId : 0,
                                    trimId = 0
                                }
                            },
                        IsExpandModel = true,
                        IsExpandType = true
                    };
                    SearchControl.DefaultFilter = filters;
                    FatFooter.DefaultFilter = filters;
                    BlogTag defaultBlogTag =
                        MasterDataHelper.GetBlogTagByName((Model != null ? Model.Name : string.Empty), DealerID)
                            .FirstOrDefault();
                    BlogTag defaultBlogTagType = MasterDataHelper.GetBlogTagByName(Type, DealerID).FirstOrDefault();

                    int blogEntryId = 0;

                    var defaultBlogEntry =
                        VehicleHelper.GetBlogEntryIdsByTag(new List<int>
                            {
                                baseModelid,
                                defaultBlogTag != null ? defaultBlogTag.BlogTagId : 0,
                                defaultBlogTagType != null ? defaultBlogTagType.BlogTagId : 0
                            }, languageId, DealerID)
                            .FirstOrDefault(
                                x =>
                                    defaultBlogTag != null &&
                                    x.PostTitle.ToLower().Contains(defaultBlogTag.TagText.ToLower())); // Base Model
                    if (defaultBlogEntry != null)
                    {
                        HasBlogBaseModel = true;
                        BlogEntryDetail.BlogId = defaultBlogEntry.BlogEntryId;
                        BlogEntries.BaseModelId = defaultBlogEntry.BlogEntryId;
                        shortBlogDescriptionSection.Visible = true;
                        shortBlogDescriptionContent.InnerText =
                            BlogHelper.GetBlogContentById(defaultBlogEntry.BlogEntryId, languageId, DealerID);
                        blogEntryId = defaultBlogEntry.BlogEntryId;
                    }
                    else
                    {
                        HasBlogBaseModel = false;
                        BlogEntryDetail.Visible = false;
                        shortBlogDescriptionSection.Visible = false;
                    }

                    var dealer = MasterDataHelper.GetDefaultDealer(DealerID);

                    VehicleImageUrl = MasterDataHelper.GetVcpImageUrl(blogEntryId, GetRoutingData("Type"),
                        GetRoutingData("Make"),
                        GetRoutingData("Model"), (dealer != null ? dealer.VcpImageStockServer : null), DealerID);

                    BlogDefaultFilter blogFilters = new BlogDefaultFilter
                    {
                        DefaultTag =
                            new List<int>
                            {
                                defaultBlogTag != null ? defaultBlogTag.BlogTagId : 0,
                                defaultBlogTagType != null ? defaultBlogTagType.BlogTagId : 0
                            }
                    };

                    BlogEntries.BlogDefaultFilter = blogFilters;

                }
                else
                {
                    //var logonUrl = $"~/Error?error=" + "This model is not in stock.  This needs to redirect to the 404 page.";
                    Response.Redirect("~/inventory");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        public void VisibleReviewTab(bool hasBlogEntry = true)
        {
            //reviewTab.Visible = HasBlogBaseModel || (BlogEntryDetail.Visible && hasBlogEntry);
            ////inventoryTab.Visible = SearchControl.VehicleCount > 0;
            //RelateSection.Visible = reviewTab.Visible || inventoryTab.Visible;
        }


        #endregion

        #region Functions
        private string GetRoutingData(string key)
        {
            return Page.RouteData.Values[key].ToString();
        }
        #endregion

    }

}