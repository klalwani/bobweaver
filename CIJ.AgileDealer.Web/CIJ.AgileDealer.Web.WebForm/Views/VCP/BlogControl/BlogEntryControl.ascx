﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogEntryControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.BlogEntryControl" %>

<div>
    <asp:ListView ID="rptEntry" runat="server" GroupItemCount="1" ItemPlaceholderID="itemsGoHere"
        GroupPlaceholderID="groupsGoHere">
        <LayoutTemplate>
            <h3>Learn more</h3>
            <asp:PlaceHolder runat="server" ID="groupsGoHere"></asp:PlaceHolder>
        </LayoutTemplate>

        <GroupTemplate>
            <asp:PlaceHolder runat="server" ID="itemsGoHere"></asp:PlaceHolder>
        </GroupTemplate>
        <ItemTemplate>
            <section>
                <div class="col-xs-12 col-md-6 col-lg-4 padding-15">

                    <div class="vcp-article-card">
                        <h4 class="blog-card-title">
                            <a class="postId" href='<%#GetRouteUrl("BlogDetail", new {PostUrl = Eval("PostUrl")  })%>'><%# Eval("PostTitle") %> </a>
                        </h4>
                        <div class="featured-image">
                            <a class="postId" href='<%#GetRouteUrl("BlogDetail", new {PostUrl = Eval("PostUrl")})%>'>
                                <img class="img-responsive project-image learn-more-image" src='<%#Eval("PostFeatureImage") %>' alt="<%# Eval("PostTitle") %>"></a>

                        </div>
                        <div class="desc text-left padding-15">
                            <p class="blog-card-summary"><%#Eval("PostSummary") %></p>
                        </div>
                    </div>

                </div>
            </section>
        </ItemTemplate>
    </asp:ListView>
    <asp:DataPager ID="dataPager" runat="server" PagedControlID="rptEntry" QueryStringField="page" PageSize="10">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="True" ShowNextPageButton="False" />
            <asp:NumericPagerField />
            <asp:NextPreviousPagerField ShowLastPageButton="True" ShowPreviousPageButton="False" />
        </Fields>
    </asp:DataPager>
</div>
<asp:HiddenField runat="server" ID="hdfDealerShipTitle" Value="" />
<asp:HiddenField runat="server" ID="hdfCategoryID" Value="" />
<asp:HiddenField runat="server" ID="hdfTagID" Value="" />

