﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogDetailControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.BlogDetailControl" %>

<div id="blog-detail" class="container">
    <section class="section">
        <div class="section-inner">
            <div class="content">
                <div class="item featured text-left">
                    <h3 class="blog-title">
                        <asp:Literal ID="ltrPostTitle" runat="server" />
                    </h3>
                    <div class="blog-herobox">
                        <div class="featured-image">
                            <asp:Image ID="imgPostFeatureImage" CssClass="img-responsive project-image" runat="server" />
                        </div>
                        <div class="button-link">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <asp:Button runat="server" ID="btnText1" CssClass="btn btn-cta-ford" />
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <asp:Button runat="server" ID="btnText2" CssClass="btn btn-def-ford" />
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="desc blog-card">
                        <asp:Literal ID="ltrPostText" runat="server" />
                    </div>
                    <h3>
                        <asp:Literal ID="ltrMessages" Visible="False" runat="server" Text="Blog Could Not Found" /></h3>
                </div>
            </div>
        </div>
    </section>
</div>