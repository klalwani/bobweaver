﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl
{
    public partial class BlogDetailControl : BaseUserControl
    {
        private int _blogId;
        public int BlogId
        {
            get
            {
                if (_blogId > 0)
                {
                    return _blogId;
                }
                string blogIdString = Page.RouteData.Values["PostID"] as string;
                if (!string.IsNullOrEmpty(blogIdString))
                    _blogId = int.Parse(blogIdString);
                return _blogId;
            }
            set { _blogId = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDetail();
            }
        }

        private void LoadDetail()
        {
            try
            {
                int languageId = SiteInfoHelper.GetLanguageId();
                var blogEntry = MasterDataHelper.GetBlogEntryById(BlogId, languageId, DealerID);
                if (blogEntry != null)
                {
                    ltrMessages.Visible = false;

                    ltrPostTitle.Text = blogEntry.PostTitle;
                    ltrPostText.Text = blogEntry.PostText;
                    imgPostFeatureImage.ImageUrl = blogEntry.PostFeatureImage;
                    btnText1.Text = blogEntry.Button1Text;
                    btnText2.Text = blogEntry.Button2Text;
                }
                else
                {
                    ltrMessages.Visible = true;
                }
                
            }
            catch (Exception ex)
            {
                ltrMessages.Visible = true;
                var el = ExceptionHelper.LogException("BlogDetailControl.LoadDetail() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: LoadDetail with BlogEntryId: " + BlogId);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
        }
    }
}