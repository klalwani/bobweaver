﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Blog;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl
{
    public partial class VcpBlogSearchControl : BaseUserControl
    {
        #region Properties
        private List<BlogTag> _blogTags;
        public List<BlogTag> BlogTags
        {
            get
            {
                if (_blogTags == null)
                {
                    _blogTags = MasterDataHelper.GetBlogTags(DealerID);
                }

                return _blogTags;
            }
            set { _blogTags = value; }
        }

        private List<int> _blogTagIdSelecteds;
        public List<int> BlogTagIdSelecteds
        {
            get
            {
                _blogTagIdSelecteds.Clear();
                _blogTagIdSelecteds = new List<int>();
                foreach (ListItem li in rptTag.Items)
                {
                    if (li.Selected)
                    {
                        int id = Convert.ToInt32(li.Value);
                        if (!_blogTagIdSelecteds.Contains(id))
                            _blogTagIdSelecteds.Add(id);
                    }
                }

                Session["TagsSelected"] = _blogTagIdSelecteds;

                return _blogTagIdSelecteds;
            }
            set { _blogTagIdSelecteds = value; }
        }

        private List<BlogCategory> _blogCategories;
        public List<BlogCategory> BlogCategories
        {
            get
            {
                if (_blogCategories == null)
                {
                    _blogCategories = MasterDataHelper.GetBlogCategories(DealerID);
                }

                return _blogCategories;
            }
            set { _blogCategories = value; }
        }

        private string _searchString;
        public string SearchString
        {
            get
            {
                if (_searchString == null)
                {
                    _searchString = string.Empty;
                }

                return _searchString;
            }
            set
            {
                if (_searchString != value)
                {
                    txtSearch.Text = value;
                    _searchString = value;
                }
            }
        }

        private string _blogCategoryHiden;
        public string BlogCategoryHiden
        {
            get
            {
                if (_blogCategoryHiden == null)
                {
                    _blogCategoryHiden = string.Empty;
                }

                return _blogCategoryHiden;
            }
            set
            {
                if (_blogCategoryHiden != value)
                {
                    _blogCategoryHiden = value;
                }
            }
        }

        private string _blogTagHidden;
        public string BlogTagHidden
        {
            get
            {
                if (_blogTagHidden == null)
                {
                    _blogTagHidden = string.Empty;
                }

                return _blogTagHidden;
            }
            set
            {
                if (_blogTagHidden != value)
                {
                    _blogTagHidden = value;
                }
            }
        }

        private string Query => Request.QueryString["page"];
        #endregion

        #region Functions
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadListBlogCategory();
                LoadListBlogTag();
            }
            PrepairDataForSelection();
        }

        public void PrepairDataForSelection()
        {
            if (Session["TagsSelected"] != null)
            {
                List<int> tags = (List<int>)Session["TagsSelected"];
                if (tags.Any())
                {
                    if (rptTag.Items.Count == 0)
                    {
                        LoadListBlogTag();
                    }
                    foreach (ListItem li in rptTag.Items)
                    {
                        int value = Convert.ToInt16(li.Value);
                        if (tags.Contains(value))
                        {
                            li.Selected = true;
                        }
                        else
                        {
                            li.Selected = false;
                        }
                    }
                }
            }
        }

        public void GetSelectedTags(string query = "")
        {
            List<int> list = new List<int>();


            foreach (ListItem li in rptTag.Items)
            {
                if (li.Selected)
                {
                    int id = Convert.ToInt32(li.Value);
                    if (!list.Contains(id))
                        list.Add(id);
                }
            }

            if (!list.Any() && !string.IsNullOrEmpty(query) && rptTag.Items.Count == 0)
            {
                list = (List<int>)Session["TagsSelected"];
            }

            Session["TagsSelected"] = list;
        }

        public void LoadListBlogTag()
        {
            var result = BlogTags.Select(t => new
            {
                Id = t.BlogTagId,
                Value = t.TagText,
                FontSize = ReturnFontSize(t.TagText)
            });

            rptTag.DataSource = result.ToList();
            rptTag.DataBind();
        }

        private void LoadListBlogCategory()
        {
            rptCategory.DataSource = BlogCategories;
            rptCategory.DataBind();
        }

        private double GetMaxTag()
        {
            double max = 1;
            foreach (var tag in BlogTags)
            {
                double temp = BlogTags.Count(t => t.TagText == tag.TagText);
                if (max < temp)
                    max = temp;
            }
            return max;
        }
        private string ReturnFontSize(string tagtext)
        {
            double max = GetMaxTag();
            double count = BlogTags.Count(t => t.TagText == tagtext.ToString());
            double percent = (count / max);
            return " " + percent + "em";
        }

        #endregion

        #region Events
        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                Response.RedirectToRoute("BlogBySearch", new { SearchTerm = txtSearch.Text.Trim() });
            }
        }
        #endregion
    }
}