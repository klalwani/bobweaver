﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VcpBlogSearchControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.VCP.BlogControl.VcpBlogSearchControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.VCP.BlogControl" %>
<div class="search row">
    <div class="input-group">
        <asp:TextBox CssClass="form-control" ID="txtSearch" placeholder="Search" runat="server" />
        <span class="input-group-btn">
            <asp:Button CssClass="btn btn-default" runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" Text="Go" />
        </span>
    </div>
</div>
<div class="row">
    <div id="categories" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><%=VcpBlogSearchControl.Categories_panel_heading_panel_title %></h3>
        </div>
        <div class="panel-body">
            <div class="list-group">
                <asp:Repeater ID="rptCategory" runat="server">
                    <ItemTemplate>
                        <a href='<%#: GetRouteUrl("BlogByCategory", new {CategorySelected = Eval("CategoryText") }) %>' class="list-group-item"><%#Eval("CategoryText") %></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div id="tags" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><%=VcpBlogSearchControl.Tags_panel_heading_panel_title %></h3>
        </div>
        <div class="panel-body">
            <div class="list-tag">
                <asp:CheckBoxList runat="server" ID="rptTag" AutoPostBack="True" RepeatDirection="Vertical" DataTextField="Value" DataValueField="Id" CssClass="chkBox"></asp:CheckBoxList>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#categories .list-group a').click(function (e) {
        $(this).addClass("active");
    });

    $("#categories .list-group a").each(function (index, a) {
        var id = $('[id$="hdfCategoryID"]').val();
        if (a.innerText === id)
            $(this).addClass("active");
    });
</script>

