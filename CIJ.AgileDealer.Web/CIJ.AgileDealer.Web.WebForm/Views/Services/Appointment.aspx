﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Appointment.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Services.Appointment" %>

<%@ Register Src="~/Views/UserControl/ScheduleService/ScheduleService.ascx" TagPrefix="ScheduleService" TagName="scheduleService" %>
<%@ Register Src="~/Views/UserControl/ScheduleService/CompleteInfo.ascx" TagPrefix="ScheduleService" TagName="completeInfo" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/AgileDealer.Validation.js"></script>
    <script src="../../Scripts/Pages/Service.ScheduleService.js"></script>
    <script src="../../Scripts/Pages/Enums/Service.Enum.js"></script>
    
    <div class="active" id="scheduleServiceCtr">
        <ScheduleService:scheduleService runat="server" ID="scheduleService" />
    </div>
    <div class="hidden" id="completeCtr">
        <ScheduleService:completeInfo runat="server" ID="CompleteInfo" />
    </div>
    <div class="clear-20"></div>
</asp:Content>