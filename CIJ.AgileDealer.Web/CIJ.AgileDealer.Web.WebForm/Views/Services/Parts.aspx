﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Parts.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Services.Parts" %>

<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Services" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/Pages/AgileDealer.PartDepartment.js"></script>
    <div class="top-container row">
        <div class="inner-container">
            <div class="col-md-12">
                <h1>Parts Department at <% =DealerName %><br />
                    <small>Order parts online serving <%= DealerCity %>, <%= DealerState %> </small>
                </h1>
            </div>
        </div>
    </div>
    <div class="body-content-wrapper">
        <div class="body-content-card">

            <div class="" id="partDepartmentForm">
                <div class="row" id="instantlyCtr">
                    <div class="row form-title">
                        <span><%=Parts.Row_Form_Title %></span>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="row content-padded">
                            <h3><%=Parts.Row_Content_Padded_H3 %></h3>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="firstName" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_1 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstName" placeholder="<%=Parts.Form_Group_PlaceHolder_1 %>" data-val="true" data-val-required="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastName" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_2 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastName" placeholder="<%=Parts.Form_Group_PlaceHolder_2 %>" data-val="true" data-val-required="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contactBy" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_3 %></label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="contactBy">
                                        <option value="email" selected="selected">Email</option>
                                        <option value="phone">Phone</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_4 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="email" placeholder="<%=Parts.Form_Group_PlaceHolder_3 %>" data-val="true" data-val-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_5 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="phone" placeholder="<%=Parts.Form_Group_PlaceHolder_4 %>" data-val="true" data-val-required="true">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="postalCode" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_6 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="postalCode" placeholder="<%=Parts.Form_Group_PlaceHolder_5 %>" data-val="true" data-val-required="true">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="row content-padded">
                            <h3><%=Parts.Row_Content_Padded_H3_1 %></h3>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="year" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_7 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="year" placeholder="<%=Parts.Form_Group_PlaceHolder_6 %>" data-val="true" data-val-required="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="make" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_8 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="make" placeholder="<%=Parts.Form_Group_PlaceHolder_7 %>" data-val="true" data-val-required="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="model" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_9 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="model" placeholder="<%=Parts.Form_Group_PlaceHolder_8 %>" data-val="true" data-val-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vin" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_10 %></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="vin" placeholder="<%=Parts.Form_Group_Label_10 %>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="row content-padded">
                            <h3><%=Parts.Row_Content_Padded_H3_2 %></h3>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="partNumber" class="col-sm-2 control-label"><%=Parts.Form_Group_Label_11 %></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="10" id="partNumber" placeholder="<%=Parts.Form_Group_PlaceHolder_9 %>" data-val="true" data-val-required="true"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" id="btnSubmit" class="btn btn-primary"><%=Parts.Btn_Success %></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <input type="hidden" value="<% =DealerName %>" id="dealerName">
        </div>
    </div>
    <div class="clear-20"></div>
</asp:Content>
