﻿using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Context;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using static CIJ.AgileDealer.Web.WebForm.Common.Enum;

namespace CJI.AgileDealer.Web.WebForm.Views.Services
{
    public partial class Service : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ServicePageDbContext())
            {
                var sPageQuery = from servicePage in db.ServicePages.Where(c => c.DealerID == DealerID)
                                 join servicePage_Trans in db.ServicePage_Trans.Where(c => c.LanguageId == LanguageId)
                                 on servicePage.ServicePageID equals servicePage_Trans.ServicePageId into tbl_Trans
                                 from tbl in tbl_Trans.DefaultIfEmpty()
                                 select new ServicePageModel
                                 {
                                     OilImage = servicePage.OilImage,
                                     OilContent = tbl.OilContent ?? servicePage.OilContent,
                                     BrakeImage = servicePage.BrakeImage,
                                     BrakeContent = tbl.BrakeContent ?? servicePage.BrakeContent,
                                     TireImage = servicePage.TireImage,
                                     TireContent = tbl.TireContent ?? servicePage.TireContent
                                 };
                var sPage = sPageQuery.FirstOrDefault();
                var svcHoursQuery = MasterDataHelper.GetHours(DealerID, LanguageId, (int)ServiceHourType.Service);

                var svcHours = svcHoursQuery.ToList();
                serviceForm.DataSource = new List<ServicePageModel>() { sPage };
                serviceForm.DataBind();
                var rpterSvcHour = serviceForm.FindControl("rpterSvcHour") as Repeater;
                if (rpterSvcHour != null)
                {
                    rpterSvcHour.DataSource = svcHours;
                    rpterSvcHour.DataBind();
                }
            }
        }
    }
}