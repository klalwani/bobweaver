﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Chat.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Blogs.Chat" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Scripts.Render("~/bundle/chat") %>
    <br />
    <div class="container" style="width: 600px" id="container">
        <div class="panel panel-default" id="divLogin">
            <div class="panel-heading clearfix">
                Login
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="txtName">Your Name:</label>
                    <input type="text" class="form-control" id="txtName">
                </div>
                <button type="button" id="startButton" class="btn btn-default">Start Chat</button>
            </div>
        </div>
        <div class="panel panel-default" id="divChat">
            <div class="panel-heading" id="header"></div>
            <div class="panel-body" style="padding: 0px;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 chatwindow">
                            <div id="divAllMessage">
                            </div>
                        </div>
                        <div class="col-sm-4 list-user">
                        </div>
                    </div>
                </div>
                <div class="container" style="border-top-color: #ddd; border-top-style: solid; border-top-width: 1px;">
                    <div class="row form-inline">
                        <input class="form-control" type="text" id="txtSendToAll" />
                        <button class="btn btn-default" id="btnSendToAll" type="button">Send</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="text" hidden="" value="" id="connectionId" />
        <input type="text" hidden="" value="" id="userName" />
    </div>
    <script id="chatPrivateTemplate" type="text/x-jQuery-tmpl">
        <div class="panel panel-default" style="width: 400px;" id="${Id}">
            <div class="panel-heading clearfix">
                ${UserName}
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <div class="container">
                    <div class="row">
                        <div class="chatprivatetwindow" id="divMessage">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="border-top-color: #ddd; border-top-style: solid; border-top-width: 1px;">
                <div class="row form-inline">
                    <input class="form-control" type="text" id="txtSendMessage" />
                    <button class="btn btn-default" id="btnSendMessage" type="button">Send</button>
                </div>
            </div>
        </div>
    </script>
    <input hidden="" type="text" value="" id="hdConnectionId" />
    <input hidden="" type="text" value="" id="hdUserName" />
</asp:Content>
