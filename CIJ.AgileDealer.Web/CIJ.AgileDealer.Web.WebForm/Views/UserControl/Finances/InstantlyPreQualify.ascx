﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InstantlyPreQualify.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Finances.InstantlyPreQualify" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Finances" %>
<form>
    <div class="row form-wrapper">

        <div class="row form-title">
            <span><%=InstantlyPreQualify.Row_Form_Ttitle_Span %></span>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="row content-padded">
                <div class="col-md-12 col-xs-12 application-title">
                    <h3><%=InstantlyPreQualify.Application_Title_H3 %></h3>
                </div>
            </div>
            <div class="row content-padded" id="credit-panel">
                <asp:Repeater ID="rptNotification" runat="server">
                    <ItemTemplate>
                        <div class="panel <%#Eval("Class")%>">
                            <div class="panel-heading">
                                <h4><span class="glyphicon <%#Eval("Glyphicon") %>" aria-hidden="true"></span><%#Eval("Title")%></h4>
                            </div>
                            <div class="panel-body">
                                <p><%#Eval("Content")%></p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="col-md-8 col-xs-12">
            <div class="row content-padded">
                <h3><%=InstantlyPreQualify.Row_Content_Padded_H3 %></h3>
            </div>

            <div class="row content-padded">

                <fieldset class="form-group">
                    <label for="creditAppType" class="control-label form-label-500"><%=InstantlyPreQualify.Form_Label_500 %></label>
                    <div class="btn-group multiButtonDefault" id="creditAppType" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="creditAppType" value="1" id="option1" autocomplete="off" checked>
                            <%=InstantlyPreQualify.Btn_Default_Active %>
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="creditAppType" value="2" id="option2" autocomplete="off">
                            <%=InstantlyPreQualify.Btn_Default_1 %>
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="creditAppType" value="3" id="option3" autocomplete="off">
                            <%=InstantlyPreQualify.Btn_Default_2 %>
                        </label>
                    </div>
                </fieldset>


                <div class="row form-horizontal-input-padded">
                    <div id="contactName">
                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="instantFirstName" class="sr-only"><%=InstantlyPreQualify.Form_Group_RowElem_Label %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantFirstName" placeholder="<%=InstantlyPreQualify.Form_Group_RowElem_Label %>">
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="instantLastName" class="sr-only"><%=InstantlyPreQualify.Form_Group_RowElem_Label_1 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantLastName" placeholder="<%=InstantlyPreQualify.Form_Group_RowElem_Label_1 %>">
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div id="contactPhoneZip">
                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="instantPhone" class="sr-only"><%=InstantlyPreQualify.Form_Group_RowElem_Label_2 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="instantPhone" placeholder="<%=InstantlyPreQualify.Form_Group_RowElem_Label_2 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="instantZip" class="sr-only"><%=InstantlyPreQualify.Form_Group_RowElem_Label_3 %></label>
                                <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="instantZip" placeholder="<%=InstantlyPreQualify.Form_Group_RowElem_Label_3 %>">
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row content-padded">
                <input type="button" aria-label="Submit" id="btnSubmit" class="btn btn-primary" value="<%=InstantlyPreQualify.Content_Padded_Input %>" />
            </div>
        </div>
    </div>
</form>
