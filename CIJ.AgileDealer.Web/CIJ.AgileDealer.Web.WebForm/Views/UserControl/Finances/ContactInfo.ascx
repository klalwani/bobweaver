﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInfo.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Finances.ContactInfo" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Finances" %>
<form>
    <div class="row form-wrapper">
        <div class="row form-title">
            <span><%=ContactInfo.Row_Form_Title_Span %></span>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="row content-padded">
                <div class="col-md-12 col-xs-12 application-title">
                    <h3><%=ContactInfo.Application_Title_H3 %></h3>
                </div>
            </div>
            <div class="row content-padded" id="credit-panel">
                <asp:Repeater ID="rptNotification" runat="server">
                    <ItemTemplate>
                        <div class="panel <%#Eval("Class")%>">
                            <div class="panel-heading">
                                <h4><span class="glyphicon <%#Eval("Glyphicon") %>" aria-hidden="true"></span><%#Eval("Title")%></h4>
                            </div>
                            <div class="panel-body">
                                <p><%#Eval("Content")%></p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="col-md-8 col-xs-12">

            <div class="row content-padded">
                <div class="row content-padded">
                    <h3><%=ContactInfo.Row_Content_Padded_H3 %></h3>
                </div>

                <fieldset class="form-group rowElem">
                    <label for="contactStreetAddress" class="sr-only"><%=ContactInfo.Form_Group_RowElem %></label>
                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactStreetAddress" placeholder="<%=ContactInfo.Form_Group_RowElem_PlaceHolder %>">
                </fieldset>

                <div class="row form-horizontal-input-padded">
                    <div id="contactAddressFull">

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="contactCity" class="sr-only"><%=ContactInfo.Form_Group_RowElem_1 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCity" placeholder="<%=ContactInfo.Form_Group_RowElem_1 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-3">
                            <fieldset class="form-group rowElem">
                                <label for="contactState" class="sr-only"><%=ContactInfo.Form_Group_RowElem_2 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="contactState" placeholder="<%=ContactInfo.Form_Group_RowElem_2 %>">
                                    <asp:Repeater ID="rptState" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-md-4">
                            <fieldset class="form-group rowElem">
                                <label for="contactZip" class="sr-only"><%=ContactInfo.Form_Group_RowElem_3 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-regex-pattern="^[0-9]*$" data-val-required="true" id="contactZip" placeholder="<%=ContactInfo.Form_Group_RowElem_3 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="form-group form-line-spacer"></div>

                <fieldset class="form-group rowElem">
                    <label for="contactResidenceStatus" class="sr-only"><%=ContactInfo.Form_Group_RowElem_4 %></label>
                    <select class="form-control" data-val="true" data-val-required="true" id="contactResidenceStatus" placeholder="<%=ContactInfo.Form_Group_RowElem_4 %>">
                        <option value="" selected disabled><%=ContactInfo.Form_Group_RowElem_4 %></option>
                        <option value="" disabled></option>
                        <asp:Repeater ID="rptResidenceStatus" runat="server">
                            <ItemTemplate>
                                <option value="<%#Eval("ResidenceStatusId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </fieldset>


                <fieldset class="form-group rowElem">
                    <label for="contactMonthlyHousing" class="sr-only"><%=ContactInfo.Form_Group_RowElem_5 %></label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*(\.\d{2})?$" data-val="true" data-val-required="true" id="contactMonthlyHousing" placeholder="<%=ContactInfo.Form_Group_RowElem_5 %>">
                        <div class="input-group-addon custom-popover" data-content="<%=ContactInfo.Custom_Popover_Data_Content_1 %>" rel="popover" data-placement="left" data-original-title="<%=ContactInfo.Custom_Popover_Data_Content_Data_Original_Title_1 %>" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>


                <div class="row form-horizontal-input-padded">
                    <label for="contactResidence" class="control-label form-label-500"><%=ContactInfo.Form_Label_500 %></label>
                    <div id="contactResidence">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactResidenceYears" class="sr-only"><%=ContactInfo.Form_Group_RowElem_6 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactResidenceYears" placeholder="">
                                    <span class="input-group-addon" id="contactResidenceYearsAddon"><%=ContactInfo.Form_Group_RowElem_6_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactResidenceMonths" class="sr-only"><%=ContactInfo.Form_Group_RowElem_7 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactResidenceMonths" placeholder="">
                                    <span class="input-group-addon" id="contactResidenceMonthsAddon"><%=ContactInfo.Form_Group_RowElem_7_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                    </div>

                </div>

                <div id="priorResidence">
                    <div class="form-group form-line-spacer"></div>

                    <fieldset class="form-group rowElem">
                        <label for="contactPreviousStreetAddress" class="sr-only"><%=ContactInfo.Form_Group_RowElem_8 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousStreetAddress" placeholder="<%=ContactInfo.Form_Group_RowElem_8 %>">
                    </fieldset>

                    <div class="row form-horizontal-input-padded">
                        <div id="contactAddressFull">

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <label for="contactPreviousCity" class="sr-only"><%=ContactInfo.Form_Group_RowElem_9 %></label>
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousCity" placeholder="<%=ContactInfo.Form_Group_RowElem_9 %>">
                                </fieldset>
                            </div>

                            <div class="col-md-3">
                                <fieldset class="form-group rowElem">
                                    <label for="contactPreviousState" class="sr-only"><%=ContactInfo.Form_Group_RowElem_10 %></label>
                                    <select class="form-control" data-val="true" data-val-required="true" id="contactPreviousState">
                                        <asp:Repeater ID="rptState2" runat="server">
                                            <ItemTemplate>
                                                <option value="<%#Eval("Code")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-md-4">
                                <fieldset class="form-group rowElem">
                                    <label for="contactPreviousZip" class="sr-only"><%=ContactInfo.Form_Group_RowElem_11 %></label>
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactPreviousZip" placeholder="<%=ContactInfo.Form_Group_RowElem_11 %>">
                                </fieldset>
                            </div>

                        </div>
                    </div>
                </div>



                <div class="row content-padded">
                    <h3><%=ContactInfo.Row_Content_Padded_1_H3 %></h3>
                </div>

                <fieldset class="form-group">
                    <label for="creditAppParties" class="control-label form-label-500"><%=ContactInfo.Form_Label_500_1 %></label>
                    <div class="btn-group multiButtonDefault" id="creditAppParties" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="creditAppParties" value="1" autocomplete="off" checked>
                            <%=ContactInfo.Btn_Default_Active %>
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="creditAppParties" value="2" autocomplete="off">
                            <%=ContactInfo.Btn_Default %>
                        </label>
                    </div>
                </fieldset>

                <div class="row form-horizontal-input-padded">
                    <label for="contactDOB" class="control-label form-label-500"><%=ContactInfo.Form_Label_500_2 %></label>
                    <div id="contactDOB">

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="contactBirthMonth" class="sr-only"><%=ContactInfo.Form_Group_RowElem_12 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="contactBirthMonth">
                                    <option value="" selected disabled><%=ContactInfo.Form_Group_RowElem_12 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptMonth" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("MonthId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="contactBirthDay" class="sr-only"><%=ContactInfo.Form_Group_RowElem_13 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="contactBirthDay">
                                    <option value="" selected disabled><%=ContactInfo.Form_Group_RowElem_13 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptDate" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("DateId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                        <div class="col-sm-4">
                            <fieldset class="form-group rowElem">
                                <label for="contactBirthYear" class="sr-only"><%=ContactInfo.Form_Group_RowElem_14 %></label>
                                <select class="form-control" data-val="true" data-val-required="true" id="contactBirthYear">
                                    <option value="" selected disabled><%=ContactInfo.Form_Group_RowElem_14 %></option>
                                    <option value="" disabled></option>
                                    <asp:Repeater ID="rptYear" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("YearId")%>" <%# (bool) Eval("IsDisabled") ? "disabled" : "" %>><%#Eval("Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div id="contactNameFull">

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="contactFirstName" class="sr-only"><%=ContactInfo.Form_Group_RowElem_15 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactFirstName" placeholder="<%=ContactInfo.Form_Group_RowElem_15 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-2">
                            <fieldset class="form-group rowElem">
                                <label for="contactMiddleName" class="sr-only"><%=ContactInfo.Form_Group_RowElem_16 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactMiddleName" placeholder="<%=ContactInfo.Form_Group_RowElem_16 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-5">
                            <fieldset class="form-group rowElem">
                                <label for="contactLastName" class="sr-only"><%=ContactInfo.Form_Group_RowElem_17 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactLastName" placeholder="<%=ContactInfo.Form_Group_RowElem_17 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <div class="row form-horizontal-input-padded">
                    <div id="contactEmailConfirm">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactEmail" class="sr-only"><%=ContactInfo.Form_Group_RowElem_18 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactEmail" placeholder="<%=ContactInfo.Form_Group_RowElem_18 %>">
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactPhone" class="sr-only"><%=ContactInfo.Form_Group_RowElem_19 %></label>
                                <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPhone" placeholder="<%=ContactInfo.Form_Group_RowElem_19 %>">
                            </fieldset>
                        </div>

                    </div>
                </div>

                <fieldset class="form-group rowElem">
                    <div class="input-group">
                        <label for="contactSSN" class="sr-only"><%=ContactInfo.Form_Group_RowElem_20 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactSSN" placeholder="<%=ContactInfo.Form_Group_RowElem_20 %>">
                        <div class="input-group-addon custom-popover" data-content="<%=ContactInfo.Custom_Popover_Data_Content_2 %>" data-placement="left" title="<%=ContactInfo.Custom_Popover_Data_Content_Data_Original_Title_2 %>" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>


                <div class="row content-padded">
                    <h3><%=ContactInfo.Row_Content_Padded_2_H3 %></h3>
                </div>

                <fieldset class="form-group rowElem">
                    <label for="contactCurrentEmployer" class="sr-only"><%=ContactInfo.Form_Group_RowElem_21 %></label>
                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCurrentEmployer" placeholder="<%=ContactInfo.Form_Group_RowElem_21 %>">
                </fieldset>
                <fieldset class="form-group rowElem">
                    <label for="contactCurrentEmployerTelePhone" class="sr-only"><%=ContactInfo.Employer_Contact_Telephone_Label %></label>
                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactCurrentEmployerTelePhone" placeholder="<%=ContactInfo.Employer_Contact_Telephone_Label %>">
                </fieldset>
                 <fieldset class="form-group rowElem">
                        <label for="contactJobTitle" class="sr-only"><%=ContactInfo.Job_Title_Label %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactJobTitle" placeholder="<%=ContactInfo.Job_Title_Label %>">
                    </fieldset>
                <fieldset class="form-group rowElem">
                    <label for="contactMonthlyIncome" class="sr-only"><%=ContactInfo.Form_Group_RowElem_22 %></label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*(\.\d{2})?$" data-val="true" data-val-required="true" id="contactMonthlyIncome" placeholder="<%=ContactInfo.Form_Group_RowElem_22 %>">
                        <div class="input-group-addon custom-popover" data-content="Total income before taxes" data-placement="left" title="Total income before taxes" rel="popover" data-trigger="hover"><span class="glyphicon glyphicon-question-sign"></span></div>
                    </div>
                </fieldset>

                <%--  <fieldset class="form-group">
                    <label for="contactEmployerDuration" class="control-label form-label-500"><%=ContactInfo.Form_Label_500_3 %></label>
                    <div class="btn-group multiButtonDefault" id="contactEmployerDuration" data-toggle="buttons">

                        <label class="btn btn-default active" data-toggle="collapse" data-target="#priorEmployer">

                            <input type="radio" name="contactEmployerDuration" value="1" id="option1" autocomplete="off" checked>
                            <%=ContactInfo.Btn_Default_Active_1 %> 
                   
                        </label>

                        <label class="btn btn-default" data-toggle="collapse" data-target="#priorEmployer">

                            <input type="radio" name="contactEmployerDuration" value="2" id="option2" autocomplete="off">
                            <%=ContactInfo.Btn_Default_1 %> 
                        </label>

                    </div>
                </fieldset>--%>

                <div class="row form-horizontal-input-padded">
                    <label for="contactEmployerDuration" class="control-label form-label-500"><%=ContactInfo.Form_Label_500_3 %></label>
                    <div id="contactEmployerDuration">

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactEmployerDurationYear" class="sr-only"><%=ContactInfo.Form_Group_RowElem_6 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactEmployerDurationYear" placeholder="">
                                    <span class="input-group-addon" id="contactEmployerDurationYearLabel"><%=ContactInfo.Form_Group_RowElem_6_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset class="form-group rowElem">
                                <label for="contactEmployerDurationMonth" class="sr-only"><%=ContactInfo.Form_Group_RowElem_7 %></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*$" data-val="true" data-val-required="true" id="contactEmployerDurationMonth" placeholder="">
                                    <span class="input-group-addon" id="contactEmployerDurationMonthLabel"><%=ContactInfo.Form_Group_RowElem_7_Span %></span>
                                </div>
                            </fieldset>
                        </div>

                    </div>
                </div>




                <div id="priorEmployer" class="collapse">

                    <fieldset class="form-group rowElem">
                        <label for="contactPreviousEmployer" class="sr-only"><%=ContactInfo.Form_Group_RowElem_23 %></label>
                        <input type="text" class="form-control" data-val="true" data-val-required="true" id="contactPreviousEmployer" placeholder="<%=ContactInfo.Form_Group_RowElem_23 %>">
                    </fieldset>
                    <div class="form-group form-line-spacer"></div>

                </div>

                <fieldset class="form-group rowElem">
                    <label for="contactAdditionalIncome" class="sr-only"><%=ContactInfo.Form_Group_RowElem_24 %></label>
                    <input type="text" class="form-control" id="contactAdditionalIncome" placeholder="<%=ContactInfo.Form_Group_RowElem_24 %>">
                </fieldset>

                <fieldset class="form-group rowElem">
                    <label for="contactAdditionalAmount" class="sr-only"><%=ContactInfo.Form_Group_RowElem_25 %></label>

                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control" data-val-regex-pattern="^[0-9]*(\.\d{2})?$" id="contactAdditionalAmount" placeholder="<%=ContactInfo.Form_Group_RowElem_25 %>">
                    </div>
                </fieldset>

            </div>
            <div class="row content-padded">
                <input type="button" id="btnSubmit" class="btn btn-primary" value="<%=ContactInfo.Row_Content_Padded_Input %>" />
            </div>

        </div>

    </div>
</form>
