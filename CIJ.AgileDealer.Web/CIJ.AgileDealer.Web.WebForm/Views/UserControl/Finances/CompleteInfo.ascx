﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompleteInfo.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Finances.CompleteInfo" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Finances" %>
<form>
    <div class="row form-wrapper">
        <div class="col-md-8 col-xs-12">
            <div class="row content-padded">
                <div class="col-md-12 col-xs-12 application-title">
                    <h3><%=CompleteInfo.Application_Title_H3 %></h3>
                </div>
            </div>
            <div class="row content-padded" id="credit-panel">
                <asp:Repeater ID="rptNotification" runat="server">
                    <ItemTemplate>
                        <div class="panel <%#Eval("Class")%>">
                            <div class="panel-heading">
                                <h4><span class="glyphicon <%#Eval("Glyphicon") %>" aria-hidden="true"></span><%#Eval("Title")%></h4>
                            </div>
                            <div class="panel-body">
                                <p><%#Eval("Content")%></p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</form>
