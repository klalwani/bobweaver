﻿using System;
using System.Collections.Generic;
using AgileDealer.Data.Entities.Applicants;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Finances
{
    public partial class ContactInfo : BaseUserControl
    {
        private const int PageIndex = 2;
        private List<NotificationModel> _notifications;
        private List<NotificationModel> Notifications
        {
            get
            {
                if (_notifications == null)
                {
                    _notifications = MasterDataHelper.GetNotifications(PageIndex,LanguageId, DealerID);
                }

                return _notifications;
            }
            set { _notifications = value; }
        }

        private List<State> _states;
        private List<State> States
        {
            get
            {
                if (_states == null)
                {
                    _states = MasterDataHelper.GetStates(DealerID);
                }

                return _states;
            }
            set { _states = value; }
        }

        private List<ResidenceStatus> _residenceStatus;
        private List<ResidenceStatus> ResidenceStatuss
        {
            get
            {
                if (_residenceStatus == null)
                {
                    _residenceStatus = MasterDataHelper.GetResidenceStatus(DealerID);
                }

                return _residenceStatus;
            }
            set { _residenceStatus = value; }
        }
        private List<Year> _years;
        private List<Year> Years
        {
            get
            {
                if (_years == null)
                {
                    _years = MasterDataHelper.GetYears(DealerID);
                }

                return _years;
            }
            set { _years = value; }
        }
        private List<MonthModel> _months;
        private List<MonthModel> Months
        {
            get
            {
                if (_months == null)
                {
                    _months = MasterDataHelper.GetMonths(LanguageId, DealerID);
                }

                return _months;
            }
            set { _months = value; }
        }
        private List<Date> _dates;
        private List<Date> Dates
        {
            get
            {
                if (_dates == null)
                {
                    _dates = MasterDataHelper.GetDates(DealerID);
                }

                return _dates;
            }
            set { _dates = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindingMasterData();
        }

        private void BindingMasterData()
        {
            //Notifications
            rptNotification.DataSource = Notifications;
            rptNotification.DataBind();

            rptState.DataSource = States;
            rptState.DataBind();

            rptResidenceStatus.DataSource = ResidenceStatuss;
            rptResidenceStatus.DataBind();

            rptState2.DataSource = States;
            rptState2.DataBind();

            rptYear.DataSource = Years;
            rptYear.DataBind();

            rptMonth.DataSource = Months;
            rptMonth.DataBind();

            rptDate.DataSource = Dates;
            rptDate.DataBind();
        }
    }
}