﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl
{
    public partial class PaymentCalculator : BaseUserControl
    {
        public bool _isHideHeader;

        public bool IsHideHeader {
            get { return _isHideHeader; }
            set { _isHideHeader = value; }
        }
        private List<PeriodTypeModel> _periodTypes;
        private List<PeriodTypeModel> PeriodTypes
        {
            get
            {
                if (_periodTypes == null)
                {
                    _periodTypes = MasterDataHelper.GetPeridTypes(LanguageId, DealerID);
                }

                return _periodTypes;
            }
            set { _periodTypes = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HdfIsHideHeader.Value = IsHideHeader.ToString();
            BindingMasterData();
        }

        private void BindingMasterData()
        {
            rptPeriodType.DataSource = PeriodTypes;
            rptPeriodType.DataBind();
        }
    }
}