﻿using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using AgileDealer.Data.Entities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.FatFooter
{
    public partial class FatFooterControl : System.Web.UI.UserControl
    {
        public string strPopular_Similar = "Popular";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ltrFatFooterUsed.Text = "";
                bool isHeaderAppended = false;
                StringBuilder sbFatFooter = new StringBuilder();
                List<FatFooterModel> lstFatFooter = SiteInfoHelper.GetFatFooter();
                List<FatFooterModel> lstFatFooterNew = new List<FatFooterModel>();
                List<FatFooterModel> lstFatFooterUsed = new List<FatFooterModel>();
                List<FatFooterModel> lstFatFooterCertified = new List<FatFooterModel>();
                FatFooterModel FilteredItem = new FatFooterModel();
                if (DefaultFilter != null)
                {
                    if (DefaultFilter.DefaultTypes.Any())
                    {
                        if (DefaultFilter.DefaultTypes.Contains("new") || DefaultFilter.DefaultTypes.Contains("New"))
                        {
                            FilteredItem = lstFatFooter.Where(x => x.Isnew && x.ModelTranslatedId == DefaultFilter.DefaultModels.FirstOrDefault() && !x.IsCertified).FirstOrDefault();
                            if (!string.IsNullOrEmpty(FilteredItem.BodyStyle))
                                lstFatFooterNew = lstFatFooter.Where(x => x.Isnew && x.ModelTranslatedId != FilteredItem.ModelTranslatedId).ToList();

                        }
                        if (DefaultFilter.DefaultTypes.Contains("certified") || DefaultFilter.DefaultTypes.Contains("Certified"))
                        {
                            FilteredItem = lstFatFooter.Where(x => !x.Isnew && x.ModelTranslatedId == DefaultFilter.DefaultModels.FirstOrDefault() && x.IsCertified).FirstOrDefault();
                            if (!string.IsNullOrEmpty(FilteredItem.BodyStyle))
                                lstFatFooterCertified = lstFatFooter.Where(x => !x.Isnew && x.BodyStyle == FilteredItem.BodyStyle && x.ModelTranslatedId != FilteredItem.ModelTranslatedId && x.IsCertified).ToList();
                            strPopular_Similar = "Certified Pre-Owned ";
                        }
                        if (DefaultFilter.DefaultTypes.Contains("used") || DefaultFilter.DefaultTypes.Contains("used"))
                        {
                            FilteredItem = lstFatFooter.Where(x => !x.Isnew && x.ModelTranslatedId == DefaultFilter.DefaultModels.FirstOrDefault() && !x.IsCertified).FirstOrDefault();
                            if (!string.IsNullOrEmpty(FilteredItem.BodyStyle))
                                lstFatFooterUsed = lstFatFooter.Where(x => !x.Isnew && x.BodyStyle == FilteredItem.BodyStyle && x.ModelTranslatedId != FilteredItem.ModelTranslatedId && !x.IsCertified).ToList();
                            strPopular_Similar = "Similar";
                        }
                    }
                    else
                    {
                        lstFatFooterNew = lstFatFooter.Where(x => x.Isnew).ToList();
                        lstFatFooterUsed = lstFatFooter.Where(x => !x.Isnew && !x.IsCertified).ToList();
                        lstFatFooterCertified = lstFatFooter.Where(x => !x.Isnew && x.IsCertified).ToList();
                    }
                }
                else
                {
                    lstFatFooterNew = lstFatFooter.Where(x => x.Isnew).ToList();
                    lstFatFooterUsed = lstFatFooter.Where(x => !x.Isnew && !x.IsCertified).ToList();
                    lstFatFooterCertified = lstFatFooter.Where(x => !x.Isnew && x.IsCertified).ToList();
                }



                hdrNewCarsHeader.Visible = lstFatFooterNew.Count > 0;
                ulFatFooterNew.Visible = lstFatFooterNew.Count > 0;
                rptNewCars.DataSource = lstFatFooterNew;
                rptNewCars.DataBind();

                FatFooterModel PreviousItem = new FatFooterModel();
                int iCounter = 0;

                foreach (var item in lstFatFooterCertified)
                {

                    if (PreviousItem.BodyStyle != item.BodyStyle)
                        isHeaderAppended = false;

                    if (!isHeaderAppended)
                    {

                        if (iCounter > 0)
                            sbFatFooter.Append("</ul>");

                        //sbFatFooter.Append("<header>");
                        sbFatFooter.Append("<h4 class=\"fat-footer-section-title\">");
                        sbFatFooter.Append("Certified Pre-Owned " + item.BodyStyle + "s");
                        sbFatFooter.Append("</h4>");
                        //sbFatFooter.Append("</header>");
                        sbFatFooter.Append("<ul class=\"fat-footer-group\">");
                        isHeaderAppended = true;



                    }
                    sbFatFooter.Append("<li class=\"fat-footer-item\">");
                    sbFatFooter.Append("<span class=\"fat-footer-item-wrapper\">");
                    //if (!item.IsCertified)
                    //{
                    //    sbFatFooter.Append("<h5 class=\"fat-footer-item-title\"><a href=\"" + item.VcpLink + "\" class=\"ctaLink\">Used " + item.Make + " " + item.Model + "</a></h5>");
                    //    sbFatFooter.Append("<p class=\"fat-footer-item-body\">" + item.TotalCount + " listings starting at $" + item.Price + "</p>");
                    //}
                    //else
                    //{
                    sbFatFooter.Append("<h5 class=\"fat-footer-item-title\"><a href=\"" + item.VcpLink + "\" class=\"ctaLink\">" + item.Make + " " + item.Model + " For Sale</a></h5>");
                    sbFatFooter.Append("<p class=\"fat-footer-item-body\">" + item.TotalCount + " listings starting at $" + item.Price + "</p>");

                    //}

                    sbFatFooter.Append("</span></li>");
                    iCounter++;
                    PreviousItem = item;

                }
                sbFatFooter.Append("</ul>");


                isHeaderAppended = false;
                if (lstFatFooterUsed != null)
                {



                    foreach (var item in lstFatFooterUsed)
                    {

                        if (PreviousItem.BodyStyle != item.BodyStyle)
                            isHeaderAppended = false;

                        if (!isHeaderAppended)
                        {

                            if (iCounter > 0)
                                sbFatFooter.Append("</ul>");

                            //sbFatFooter.Append("<header>");
                            sbFatFooter.Append("<h4 class=\"fat-footer-section-title\">");
                            sbFatFooter.Append(strPopular_Similar + " Used " + item.BodyStyle + "s");
                            sbFatFooter.Append("</h4>");
                            //sbFatFooter.Append("</header>");
                            sbFatFooter.Append("<ul class=\"fat-footer-group\">");
                            isHeaderAppended = true;



                        }
                        sbFatFooter.Append("<li class=\"fat-footer-item\">");
                        sbFatFooter.Append("<span class=\"fat-footer-item-wrapper\">");
                        if (!item.IsCertified)
                        {
                            sbFatFooter.Append("<h5 class=\"fat-footer-item-title\"><a href=\"" + item.VcpLink + "\" class=\"ctaLink\">Used " + item.Make + " " + item.Model + "</a></h5>");
                            sbFatFooter.Append("<p class=\"fat-footer-item-body\">" + item.TotalCount + " listings starting at $" + item.Price + "</p>");
                        }
                        else
                        {
                            sbFatFooter.Append("<h5 class=\"fat-footer-item-title\"><a href=\"" + item.VcpLink + "\" class=\"ctaLink\">Certified Pre-Owned " + item.Make + " " + item.Model + " For Sale</a></h5>");
                            sbFatFooter.Append("<p class=\"fat-footer-item-body\">Certified Pre-Owned For Sale</p>");

                        }

                        sbFatFooter.Append("</span></li>");
                        iCounter++;
                        PreviousItem = item;

                    }
                    sbFatFooter.Append("</ul>");
                    ltrFatFooterUsed.Text = sbFatFooter.ToString();
                }
            }
            catch (Exception ex)
            {
                var el = ExceptionHelper.LogException("FatFooterControl Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "");
            }
        }


        public DefaultFilter DefaultFilter
        {
            get
            {
                if (ViewState["DefaultFilter"] == null)
                    return new DefaultFilter();
                return (DefaultFilter)ViewState["DefaultFilter"];
            }
            set
            {
                ViewState["DefaultFilter"] = value;
            }
        }

    }



}