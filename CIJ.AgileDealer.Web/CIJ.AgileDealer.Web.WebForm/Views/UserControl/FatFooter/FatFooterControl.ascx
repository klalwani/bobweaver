﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FatFooterControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.FatFooter.FatFooterControl" %>

<section>
     
        <h4 class="fat-footer-section-title" id="hdrNewCarsHeader" runat="server"><%=strPopular_Similar %> New Cars
        </h4>
     <ul class="fat-footer-group" id="ulFatFooterNew" runat="server">
        <asp:Repeater ID="rptNewCars" runat="server">
            <ItemTemplate>
                <li class="fat-footer-item">
                    <span class="fat-footer-item-wrapper">
                        <h5 class="fat-footer-item-title">
                            <asp:HyperLink ID="hylnkVCPlink" runat="server" Text='<%#"New " + Eval("Make") + " " + Eval("Model") %>' NavigateUrl='<%#Eval("Vcplink") %>' CssClass="ctaLink"></asp:HyperLink>
                        </h5>
                        <p class="fat-footer-item-body">
                            <%#Eval("TotalCount")%> listings starting at $<%#Eval("Price")%>
                        </p>
                    </span>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
        <style>
        .fat-footer-section-title {
            font-weight: 600;
            font-size: 1em;
            margin-bottom: .25em;
            text-align: center;
        }
        .fat-footer-group {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
            padding: .5em;

        }
        .fat-footer-item {
            list-style: none;
            flex-basis: 50%;
            max-width: 50%;
            padding-left: 0;
        }
        .fat-footer-item-title {
            font-weight: 600;
            margin-top: 0;
            margin-bottom: .5em;
            padding-right: .5em;
        }
        .fat-footer-item-body {
            display: none;
            color: #909090;
            font-size: .8em;
        }
        @media only screen and (min-width: 768px) {
            .fat-footer-item {
                flex-basis: 50%;
                max-width: 50%;
                padding-left: 3.45em;
            }
           .fat-footer-item-body {
                display: flex;
            }
        }
        @media only screen and (min-width: 1024px) {
            .fat-footer-item {
                flex-basis: 33.3%;
                max-width: 33.3%;
                padding: .625em;
            }
        }
        @media only screen and (min-width: 1280px) {
            .fat-footer-item {
                flex-basis: 25%;
                max-width: 25%;
            }
        }
    </style>
    <asp:Literal ID="ltrFatFooterUsed" runat="server"></asp:Literal>
</section>
