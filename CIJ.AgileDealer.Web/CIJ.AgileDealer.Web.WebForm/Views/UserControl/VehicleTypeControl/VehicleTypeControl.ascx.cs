﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.VehicleTypeControl
{
    public partial class VehicleTypeControl : BaseUserControl
    {
        private char[] charSeparators = { '-' };
        private char[] specialCharSeparators = { '–' };

        private List<string> _vehicleType;
        private static string VehicleMake = ConfigurationManager.AppSettings["VehicleMake"] as string;
        public List<string> VehicleType
        {
            get
            {

                string vehicleTypeString = Page.RouteData.Values["model"] as string;
                if (!string.IsNullOrEmpty(vehicleTypeString))
                {
                    _vehicleType = new List<string>();
                    List<string> tempNormal = vehicleTypeString.ToLower().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
                    List<string> tempSpecial = vehicleTypeString.ToLower().Split(specialCharSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if(tempNormal.Any())
                        _vehicleType.AddRange(tempNormal);
                    if (tempSpecial.Any())
                        _vehicleType.AddRange(tempSpecial);
                }
                else
                {
                    _vehicleType = new List<string>();
                }
                return _vehicleType;
            }
            set
            {
                _vehicleType = value;
            }
        }

        private List<string> _type;
        public List<string> Type
        {
            get
            {
                string typeString = Page.RouteData.Values["type"] as string;
                if (!string.IsNullOrEmpty(typeString))
                {
                    _type = typeString.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
                else
                {
                    _type = new List<string>();
                }
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        private decimal _maxPrice;
        public decimal MaxPrice
        {
            get
            {
                string priceString = Page.RouteData.Values["price"] as string;
                if (!string.IsNullOrEmpty(priceString))
                {
                    var temp = priceString.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList<int>();
                    if (temp.Any() && temp.Count > 1)
                        _maxPrice = temp.Max();
                    else
                    {
                        _maxPrice = 0;
                    }
                }

                return _maxPrice;
            }
            set
            {
                _maxPrice = value;
            }
        }

        private decimal _minPrice;
        public decimal MinPrice
        {
            get
            {
                string priceString = Page.RouteData.Values["price"] as string;
                if (!string.IsNullOrEmpty(priceString))
                {
                    var temp = priceString.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList<int>();
                    if (temp.Any() && temp.Count > 1)
                        _minPrice = temp.Min();
                    else
                    {
                        _minPrice = 0;
                    }
                }

                return _minPrice;
            }
            set
            {
                _minPrice = value;
            }
        }

        private bool _lift;
        public bool Lift
        {
            get
            {
                string typeString = Page.RouteData.Values["lifted"] as string;
                if (!string.IsNullOrEmpty(typeString))
                {
                    _lift = true;
                }
                return _lift;
            }
            set
            {
                _lift = value;
            }
        }

        private bool _upfitted;
        public bool Upfitted
        {
            get
            {
                string typeString = Page.RouteData.Values["upfitted"] as string;
                if (!string.IsNullOrEmpty(typeString))
                {
                    _upfitted = true;
                }
                return _upfitted;
            }
            set
            {
                _upfitted = value;
            }
        }


        private bool _special;
        public bool Special
        {
            get
            {
                string specialString = Page.RouteData.Values["special"] as string;
                if (!string.IsNullOrEmpty(specialString))
                {
                    _special = true;
                }
                return _special;
            }
            set
            {
                _special = value;
            }
        }
        private bool _rentalspecial;
        public bool RentalSpecial
        {
            get
            {
                string rentalspecialString = Page.RouteData.Values["loanerspecial"] as string;
                if (!string.IsNullOrEmpty(rentalspecialString))
                {
                    _rentalspecial = true;
                }
                return _rentalspecial;
            }
            set
            {
                _rentalspecial = value;
            }
        }
        private bool _limo;
        public bool Limo
        {
            get
            {
                string typeString = Page.RouteData.Values["limo"] as string;
                if (!string.IsNullOrEmpty(typeString))
                {
                    _limo = true;
                }
                return _limo;
            }
            set
            {
                _limo = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<ModelTranslated> models = new List<ModelTranslated>();
            List<VehicleStandardBody> bodyStyleTypes = new List<VehicleStandardBody>();

            string model = VehicleMake;
            if (!Type.Contains("new"))
            {
                model = string.Empty;
            }
            models = VehicleHelper.GetModelTranslatedForVehicleTypePage(
                string.Empty, string.Empty, VehicleType, DealerID);

            bodyStyleTypes = VehicleHelper.GetBodyStyleTypesOfStandardBodyForVehicleTypePage(VehicleType, DealerID);

            DefaultFilter filters = new DefaultFilter
            {
                DefaultTypes = Type,
                DefaultModels = new List<int>(),
                DefaultModelTrims = new List<VehicleModelTrim>(),
                DefaultVehicleBodyStandardBodyStyle = new List<VehicleBodyStandardBodyStyle>(),
                DefaultBodyStyleTypes = new List<int>(),
                DefaultPrices = new List<int>(),
                IsExpandModel = Type.Any(x => x == "new" || x == "certified"),
                IsExpandBodyStyle = Type.Any(x => x == "used"),
                IsLifted = Lift,
                IsUpfitted = Upfitted,
                IsLimo = Limo,
                IsSpecial = Special,
                IsRentalSpecial = RentalSpecial
            };
            if (!Limo && !Special && !Lift)
            {
                if (models.Any())
                {
                    filters.DefaultModels = models.Select(x => x.ModelTranslatedId).ToList();
                    filters.DefaultModelTrims = models.Select(x => new VehicleModelTrim()
                    {
                        modelId = x.ModelTranslatedId,
                        trimId = 0
                    }).ToList();
                }
                if (bodyStyleTypes.Any())
                {
                    filters.DefaultVehicleBodyStandardBodyStyle = bodyStyleTypes.Select(x => new VehicleBodyStandardBodyStyle()
                    {
                        standardBodyId = x.VehicleStandardBodyId,
                        bodyStyleTypeId = 0
                    }).ToList();
                }
            }

            if (MaxPrice != 0)
            {
                Price currentPrice = MasterDataHelper.GetPricesByRange(MinPrice, MaxPrice);
                if (currentPrice != null)
                    filters.DefaultPrices.Add(currentPrice.PriceId);
            }

            SearchControl.DefaultFilter = filters;
        }
    }
}