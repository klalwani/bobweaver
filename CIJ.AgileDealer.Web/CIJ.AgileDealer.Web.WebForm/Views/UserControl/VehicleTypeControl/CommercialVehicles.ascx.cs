﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.VehicleTypeControl
{
    public partial class CommercialVehicles : BaseUserControl
    {
        private string _vehicleType;
        public string VehicleType
        {
            get
            {
                return _vehicleType;
            }
            set
            {
                _vehicleType = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> segments = new List<string>() { "commercial-truck" };
            List<ModelTranslated> models = VehicleHelper.GetModelTranslatedForVehicleTypePage(string.Empty,string.Empty, segments, DealerID);

            DefaultFilter filters = new DefaultFilter
            {
                DefaultTypes = new List<string> { "new" },
                DefaultModels = new List<int>()
            };

            if (models.Any())
            {
                List<int> modelIds = models.Select(x => x.ModelTranslatedId).ToList();
                filters.DefaultModels = modelIds;
            }
            SearchControl.DefaultFilter = filters;
        }
    }
}