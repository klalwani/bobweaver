﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Views.VCP;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.SearchControl
{
    public partial class SearchControl : BaseUserControl
    {
        #region Public Properties


        private int PageSize => 15;

        private bool _isViewAll;
        private bool IsViewAll
        {
            get
            {
                return _isViewAll;
            }
            set { _isViewAll = value; }
        }

        private int _pageNumber;
        private int PageNumber
        {
            get
            {
                string pageNumberString = currentPage.Value;
                _pageNumber = Convert.ToInt16(pageNumberString);
                return _pageNumber;
            }
            set { _pageNumber = value; }
        }

        public DefaultFilter DefaultFilter
        {
            get
            {
                if (ViewState["DefaultFilter"] == null)
                    return new DefaultFilter();
                return (DefaultFilter)ViewState["DefaultFilter"];
            }
            set
            {
                ViewState["DefaultFilter"] = value;
            }
        }

        private VehicleSearchCriteria _vehicleSearchCriteria;
        public VehicleSearchCriteria VehicleSearchCriteria
        {
            get
            {
                if (_vehicleSearchCriteria == null)
                    _vehicleSearchCriteria = new VehicleSearchCriteria();
                return _vehicleSearchCriteria;
            }
            set
            {
                _vehicleSearchCriteria = value;
            }
        }


        private VehicleMasterDataCounter _vehicleMasterDataCounter;
        public VehicleMasterDataCounter VehicleMasterDataCounter
        {
            get
            {
                List<VehicleKeyPairValueData> transmissions = new List<VehicleKeyPairValueData>();
                transmissions.Add(new VehicleKeyPairValueData() { Name = "Manual", Id = 0 });
                transmissions.Add(new VehicleKeyPairValueData() { Name = "Automatic", Id = 1 });

                if (_vehicleMasterDataCounter == null)
                    _vehicleMasterDataCounter = new VehicleMasterDataCounter()
                    {
                        IsNew = 0,
                        IsUsed = 0,
                        IsCertified = 0,
                        IsLift = 0,
                        IsUpfitted = 0,
                        Engines = (from e in EngineTypeList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name, SubList = e.SubList }).ToList(),
                        FuelTypes = (from e in FuelTypeList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        Drives = (from e in DriveList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        BodyStyles = (from e in BodyStyleTypeList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name, SubList = e.SubList }).ToList(),
                        Prices = (from e in PriceList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, From = e.From, To = e.To, Name = e.Name }).ToList(),
                        Years = (from e in YearList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        Mileages = (from e in MillageList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, From = e.From, To = e.To, Name = e.Name }).ToList(),
                        Models = (from e in ModelList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name, SubList = e.SubList }).ToList(),
                        ExteriorColors = (from e in ExteriorColorList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        Makes = (from e in MakeList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        VehicleFeatures = (from e in VehicleFeatureMastersList select new VehicleKeyPairValueData { Id = e.Id, Count = 0, Name = e.Name }).ToList(),
                        Transmissions = transmissions
                    };
                return _vehicleMasterDataCounter;
            }
            set
            {
                _vehicleMasterDataCounter = value;
            }
        }


        public int _vehicleCount;
        public int VehicleCount
        {
            get
            {
                return _vehicleCount;
            }
            set { _vehicleCount = value; }
        }
        #endregion

        #region Private Properties

        private string _vehicleMake = ConfigurationManager.AppSettings["VehicleMake"];

        private List<MasterData> _makes;
        private List<MasterData> MakeList
        {
            get
            {
                if (_makes == null)
                {
                    _makes = MasterDataHelper.GetMakes();
                }

                return _makes;
            }
            set { _makes = value; }
        }

        private List<MasterData> _vehicleFeatureMasters;
        private List<MasterData> VehicleFeatureMastersList
        {
            get
            {
                if (_vehicleFeatureMasters == null)
                {
                    if (DefaultFilter.DefaultTypes.Any())
                    {
                        if (DefaultFilter.DefaultTypes.Contains("new") || DefaultFilter.DefaultTypes.Contains("New"))
                        {
                            VehicleSearchCriteria.IsNew = true;
                        }
                        if (DefaultFilter.DefaultTypes.Contains("certified") || DefaultFilter.DefaultTypes.Contains("Certified"))
                        {
                            VehicleSearchCriteria.IsCertified = true;
                        }
                        if (DefaultFilter.DefaultTypes.Contains("used") || DefaultFilter.DefaultTypes.Contains("used"))
                        {
                            VehicleSearchCriteria.IsUsed = true;
                        }
                    }
                    _vehicleFeatureMasters = MasterDataHelper.GetVehicleFeaturMasters(VehicleSearchCriteria);

                }

                return _vehicleFeatureMasters;
            }
            set { _vehicleFeatureMasters = value; }
        }


        private List<MasterData> _bodyStyleTypes;
        private List<MasterData> BodyStyleTypeList
        {
            get { return _bodyStyleTypes ?? (_bodyStyleTypes = MasterDataHelper.GetVehicleStandardBodyStyle(new List<int>())); }
            set { _bodyStyleTypes = value; }
        }

        private List<MasterData> _fuelTypes;
        private List<MasterData> FuelTypeList
        {
            get { return _fuelTypes ?? (_fuelTypes = MasterDataHelper.GetFuelType()); }
            set { _fuelTypes = value; }
        }

        private List<MasterData> _drives;
        private List<MasterData> DriveList
        {
            get { return _drives ?? (_drives = MasterDataHelper.GetDrive()); }
            set { _drives = value; }
        }

        private List<MasterData> _mileages;
        private List<MasterData> MillageList
        {
            get { return _mileages ?? (_mileages = MasterDataHelper.Getmileage()); }
            set { _mileages = value; }
        }

        private List<MasterData> _exteriorColors;
        private List<MasterData> ExteriorColorList
        {
            get { return _exteriorColors ?? (_exteriorColors = MasterDataHelper.GetExteriorColor()); }
            set { _exteriorColors = value; }
        }

        private List<MasterData> _transmissions;
        private List<MasterData> TransmissionList
        {
            get { return _transmissions ?? (_transmissions = MasterDataHelper.GetTransmission()); }
            set { _transmissions = value; }
        }

        private List<MasterData> _models;
        private List<MasterData> ModelList
        {
            get { return _models ?? (_models = MasterDataHelper.GetModelTranslatedWithSubList(new List<int>())); }
            set { _models = value; }
        }

        private List<MasterData> _engineTypes;
        private List<MasterData> EngineTypeList
        {
            get { return _engineTypes ?? (_engineTypes = MasterDataHelper.GetEngineListWithSubList()); }
            set { _engineTypes = value; }
        }

        private List<MasterData> _years;
        private List<MasterData> YearList
        {
            get { return _years ?? (_years = MasterDataHelper.GetYearList()); }
            set { _years = value; }
        }

        private List<MasterData> _prices;
        private List<MasterData> PriceList
        {
            get { return _prices ?? (_prices = MasterDataHelper.GetPrices()); }
            set { _prices = value; }
        }
        #endregion

        #region Public Event
        protected void Page_Load(object sender, EventArgs e)
        {
            var isMobileRequest = this.Request.Browser.IsMobileDevice;
            if (isMobileRequest)
            {
                isMobile.Value = "true";
            }
            CanBeEdit.Value = (HttpContext.Current.User.IsInRole(RoleEnum.Administrator) || HttpContext.Current.User.IsInRole(RoleEnum.Dealer)).ToString();

            pageSize.Value = PageSize.ToString();
            string pageNumber = Request.QueryString["page"];
            string searchText = Request.QueryString["keywords"];
            string isRememberHistory = Request.QueryString["rememberHistory"];
            if (string.IsNullOrEmpty(isRememberHistory))
            {
                isRememberHistory = "false";
            }
            HdfType.Value = DefaultFilter.DefaultTypes.FirstOrDefault();
            if (searchText != null)
            {
                searchText = searchText.Replace("+", " ");
                HdfSearchText.Value = searchText;
            }
            if (!IsPostBack)
            {
                rememberHistory.Value = "true";
                isRememberHistory = "false";
                GetDefaultFilter();
                GetDefaultMasterDataCounter();
                PopulateMasterData();
                if (!string.IsNullOrEmpty(pageNumber))
                {
                    currentPage.Value = pageNumber;
                }

                if (HttpContext.Current.Request.QueryString["Sort"] != null)
                {
                    try
                    {
                        VehicleSearchCriteria.SortItem = Convert.ToInt32(HttpContext.Current.Request.QueryString["Sort"]);
                    }
                    catch
                    {
                    }
                }
            }

            if (Session["Vehicle-Data"] != null && isRememberHistory == "true")
            {
                previousVehicleData.Value = JsonHelper.Serialize(Session["Vehicle-Data"]);
                Session["Vehicle-Data"] = null;//Clean out
            }

            defaultFilter.Value = JsonHelper.Serialize(VehicleSearchCriteria);

            if (Session["NewPage_IsViewAll"] != null)
            {
                IsViewAll = (bool)Session["NewPage_IsViewAll"];
            }

            Session["NewPage_IsSearch"] = string.Empty;

            //ReBindDataSource();
        }
        #endregion

        #region Private Function

        private void GetDefaultMasterDataCounter()
        {
            if (VehicleMasterDataCounter != null)
            {
                defaultMasterDataCounter.Value = JsonHelper.Serialize(VehicleMasterDataCounter);
            }
        }

        private void GetDefaultFilter()
        {
            if (DefaultFilter != null)
            {
                VehicleSearchCriteria.PageSize = PageSize;
                VehicleSearchCriteria.PageNumber = PageNumber;

                if (DefaultFilter.DefaultTypes.Any())
                {
                    if (DefaultFilter.DefaultTypes.Contains("new") || DefaultFilter.DefaultTypes.Contains("New"))
                    {
                        VehicleSearchCriteria.IsNew = true;
                    }
                    if (DefaultFilter.DefaultTypes.Contains("certified") || DefaultFilter.DefaultTypes.Contains("Certified"))
                    {
                        VehicleSearchCriteria.IsCertified = true;
                    }
                    if (DefaultFilter.DefaultTypes.Contains("used") || DefaultFilter.DefaultTypes.Contains("used"))
                    {
                        VehicleSearchCriteria.IsUsed = true;
                        VehicleSearchCriteria.SortItem = (int)SearchControlEnum.StockDate;
                    }
                }

                if (DefaultFilter.DefaultMakes.Any())
                {
                    DefaultFilter.InLitmitedMode = true;
                    VehicleSearchCriteria.Makes = DefaultFilter.DefaultMakes;
                }

                if (DefaultFilter.DefaultModelTrims.Any())
                {
                    DefaultFilter.InLitmitedMode = true;
                    VehicleSearchCriteria.ModelTrims = DefaultFilter.DefaultModelTrims;
                    VehicleSearchCriteria.IsSelectingModel = true;
                }

                if (DefaultFilter.DefaultVehicleBodyStandardBodyStyle.Any())
                {
                    DefaultFilter.InLitmitedMode = true;
                    VehicleSearchCriteria.VehicleBodyStandardBodyStyles = DefaultFilter.DefaultVehicleBodyStandardBodyStyle;
                    VehicleSearchCriteria.IsSelectingBody = true;
                    VehicleSearchCriteria.IsSelectingModel = false;// cannot select both BodyStyle and Model at the same time
                }

                if (DefaultFilter.DefaultPrices.Any())
                {
                    DefaultFilter.InLitmitedMode = true;
                    VehicleSearchCriteria.Prices = DefaultFilter.DefaultPrices;
                }

                VehicleSearchCriteria.IsExpandType = DefaultFilter.IsExpandType;
                VehicleSearchCriteria.IsExpandModel = DefaultFilter.IsExpandModel;
                VehicleSearchCriteria.IsExpandBodyStyle = DefaultFilter.IsExpandBodyStyle;
                VehicleSearchCriteria.IsLift = DefaultFilter.IsLifted;
                VehicleSearchCriteria.IsUpfitted = DefaultFilter.IsUpfitted;
                VehicleSearchCriteria.IsLimo = DefaultFilter.IsLimo;
                VehicleSearchCriteria.IsSpecial = DefaultFilter.IsSpecial;
                VehicleSearchCriteria.IsRentalSpecial = DefaultFilter.IsRentalSpecial;
                VehicleSearchCriteria.InLitmitedMode = DefaultFilter.InLitmitedMode;


            }
        }


        private void PopulateMasterData()
        {
            PopulateDataForMake();
            PopulateDataForYear();
            PopulateDataForModel();
            PopulateDataForBodyStyle();
            PopulateDataForExteriorColor();
            PopulateDataForFuelType();
            PopulateDataForDrive();
            PopulateDataForEngineType();
            PopulateDataFormileage();
            PopulateDataForPrice();
            PopulateDataForTransmissions();
        }
        private void PopulateDataForTransmissions()
        {
            rptTransmissions.DataSource = TransmissionList;
            rptTransmissions.DataBind();
        }
        private void PopulateDataForMake()
        {
            rptMake.DataSource = MakeList;
            rptMake.DataBind();
        }

        private void PopulateDataForModel()
        {
            rptModel.DataSource = ModelList;
            rptModel.DataBind();
        }

        private void PopulateDataForBodyStyle()
        {
            rbtBodyStyle.DataSource = BodyStyleTypeList;
            rbtBodyStyle.DataBind();
        }

        private void PopulateDataForExteriorColor()
        {
            rptExteriorColor.DataSource = ExteriorColorList;
            rptExteriorColor.DataBind();
        }

        private void PopulateDataForFuelType()
        {
            rptFuelType.DataSource = FuelTypeList;
            rptFuelType.DataBind();
        }

        private void PopulateDataForEngineType()
        {
            rptEngine.DataSource = EngineTypeList;
            rptEngine.DataBind();
        }

        private void PopulateDataForDrive()
        {
            rptDrive.DataSource = DriveList;
            rptDrive.DataBind();
        }

        private void PopulateDataFormileage()
        {
            rptMileage.DataSource = MillageList;
            rptMileage.DataBind();
        }

        private void PopulateDataForYear()
        {
            rptYear.DataSource = YearList;
            rptYear.DataBind();
        }

        private void PopulateDataForPrice()
        {
            rptPrice.DataSource = PriceList;
            rptPrice.DataBind();
        }
        #endregion

        protected void rptModel_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptTrims = (Repeater)e.Item.FindControl("rptTrims");
            if (rptTrims != null)
            {
                MasterData temp = (MasterData)e.Item.DataItem;
                if (temp.HasSubList)
                {
                    rptTrims.DataSource = temp.SubList;
                    rptTrims.DataBind();
                }
            }
        }
        protected void rptEngine_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptEngineSub = (Repeater)e.Item.FindControl("rptEngineSub");
            if (rptEngineSub != null)
            {
                MasterData temp = (MasterData)e.Item.DataItem;
                if (temp.HasSubList)
                {
                    rptEngineSub.DataSource = temp.SubList;
                    rptEngineSub.DataBind();
                }
            }
        }
        protected void rptBodyStyle_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptBodyStyleSub = (Repeater)e.Item.FindControl("rptBodyStyleSub");
            if (rptBodyStyleSub != null)
            {
                MasterData temp = (MasterData)e.Item.DataItem;
                if (temp.HasSubList)
                {
                    rptBodyStyleSub.DataSource = temp.SubList;
                    rptBodyStyleSub.DataBind();
                }
            }
        }
    }
}