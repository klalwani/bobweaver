﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.SearchControl.SearchControl" %>
<%: Scripts.Render("~/bundles/SearchControl") %>
<%@ Register Src="~/Views/UserControl/RebateControl/RebateControl.ascx" TagPrefix="RebateControl" TagName="rebateControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.SearchControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Common" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>
<%--<script src="../../../Scripts/Pages/AgileDealer.SearchControl.js"></script>--%>
<div style="margin-bottom: 20px">
    <div class="container-fluid alert alert-danger text-center hide divNoSearchData">
    </div>
</div>

<input type="hidden" data-attribute="validation" name="ErrorMessage" value="<%=Common.ErrorMessage %>" />
<input type="hidden" data-attribute="validation" name="VehiclesFound" value="<%=SearchControl.VehiclesFound %>" />
<div class="container" id="new-vehicle">
    <div id="left-menu-inventory-page">
        <div class="container-fluid">
            <div class="panel-group" id="navbarToggleButton" style="display: none">
                <div class="panel panel-default">
                    <div class="panel-heading accordion-heading-select">
                        <div id="btnEnbleFilter" data-toggle="modal" data-target="#MainMenu" class="filter-item btn-primary collapsed" aria-expanded="false"><span class="glyphicon padded-glyphicon glyphicon-search" aria-hidden="true"></span></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12" id="MainMenu" role="dialog" tabindex="-1">
                <div id="row">
                    <div class="panel-group" id="accordion-refresh">
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-heading-select">
                                <div id="newSearch" class="list-group-item panel-default btn-primary" data-parent="#accordion-refresh"><span class="glyphicon glyphicon-refresh padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Accordion_Heading_Select_A %></div>
                            </div>
                        </div>
                    </div>
                    <!--
						<div class="panel-group" id="accordion-vin">
							<div class="panel panel-default">
								<div class="panel-heading accordion-heading-select">
									<input id="txtSearch" class="text"/>
									<span class="" id="btnSearchVin">Search</span>
								</div>
							</div>
						</div>
						-->

                    <div class="panel-group" id="vin-search-btn">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <!-- old <a data-toggle="collapse" data-parent="#vin-search-btn" href="#vin-lookup"><span class="glyphicon glyphicon-search padded-glyphicon" aria-hidden="true"></span>VIN & Stock # Lookup</a> -->
                                <%--  <span class="glyphicon glyphicon-plus glyphicon-search padded-glyphicon" aria-hidden="true"></span>
									<input id="txtSearch" class="text" placeholder="<%=SearchControl.Panel_Heading_Input_PlaceHolder %>" style="max-width: 79%; border: groove 1px #ccc; padding-left: 3px;">
									<a href="#" id="btnRemoveSearchText"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>--%>
                                <div id="searchInputGroup" class="input-group">
                                    <div class="form-group has-feedback has-clear">
                                        <input type="text" class="form-control" id="txtSearch" aria-label="Search" placeholder="<%=SearchControl.Panel_Heading_Input_PlaceHolder %>">
                                        <div id="btnRemoveSearchText" class="form-control-clear glyphicon glyphicon-remove form-control-feedback hidden"></div>
                                    </div>
                                    <div class="input-group-btn">
                                        <div class="btn btn-primary" id="btnSearchVin">
                                            <span class="glyphicon glyphicon-search padded-glyphicon" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel-group" id="accordion-type-age">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-type-age" href="#collapseType">
                                <a><span class="glyphicon glyphicon-plus glyphicon-minus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A %> </a>
                            </div>
                            <div id="collapseType" class="panel-collapse collapse in">
                                <ul class="list-group" data-type="type">
                                    <%=SearchControl.Collapse_In_UL %>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-model">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-model" href="#collapseModel">
                                <a><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_1 %></a>
                            </div>
                            <div id="collapseModel" class="panel-collapse collapse">
                                <ul class="list-group" data-type="model">
                                    <li class="list-group-item active list-group-custom-default" value=""><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li %></li>
                                    <asp:Repeater ID="rptModel" runat="server" OnItemDataBound="rptModel_OnItemDataBound">
                                        <ItemTemplate>
                                            <li parentid="ModelSubTrim<%#Eval("Id")%>" hassublist="<%# Eval("HasSubList")%>" value='<%#Eval("Id")%>' class="list-group-item <%# (bool)Eval("HasSubList") ? "hassublist": "" %>" data-toggle="collapse">
                                                <span class="glyphicon glyphicon-plus padded-glyphicon parentItem" style='display: <%# (bool)Eval("HasSubList") ? "inline": "none" %>' aria-hidden="true"></span>
                                                <span class="badge" isparentmodelcount="true">0</span><%#Eval("Name")%>
                                            </li>

                                            <div class="collapse list-group-submenu" id="ModelSubTrim<%#Eval("Id")%>">
                                                <asp:Repeater ID="rptTrims" runat="server">
                                                    <ItemTemplate>
                                                        <li class="list-group-item sublistItem" value='<%#Eval("SubListId")%>'><span class="badge" ischildcount="true">0</span><%#Eval("SubListName")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-lift">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-lift" href="#collapseLift">
                                <a><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_2 %></a>
                            </div>
                            <div id="collapseLift" class="panel-collapse collapse">
                                <ul class="list-group" data-type="lift">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_1 %></li>
                                    <li class="list-group-item" value="isLift"><span class="badge">0</span><%=SearchControl.List_Group_Li %></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                      <div class="panel-group" id="accordion-upfitted">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-upfitted" href="#collapseUpfitted">
                                <a><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span>Upfitted</a>
                            </div>
                            <div id="collapseUpfitted" class="panel-collapse collapse">
                                <ul class="list-group" data-type="upfitted">
                                    <li class="list-group-item" value="isUpfitted"><span class="badge">0</span>All Upfitted</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-bodystyle">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-bodystyle" href="#collapseStyle">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_3 %></a>
                            </div>
                            <div id="collapseStyle" class="panel-collapse collapse">
                                <ul class="list-group" data-type="style">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_2 %></li>
                                    <asp:Repeater ID="rbtBodyStyle" runat="server" OnItemDataBound="rptBodyStyle_OnItemDataBound">
                                        <%--	<ItemTemplate>
												<li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
											</ItemTemplate>--%>
                                        <ItemTemplate>
                                            <li parentid="BodyStyleSub<%#Eval("Id")%>" hassublist="<%# Eval("HasSubList")%>" value='<%#Eval("Id")%>' class="list-group-item <%# (bool)Eval("HasSubList") ? "hassublist": "" %>" data-toggle="collapse">
                                                <span class="glyphicon glyphicon-plus padded-glyphicon parentItem" style='display: <%# (bool)Eval("HasSubList") ? "inline": "none" %>' aria-hidden="true"></span>
                                                <span class="badge" isparentmodelcount="true">0</span><%#Eval("Name")%>
                                            </li>

                                            <div class="collapse list-group-submenu" id="BodyStyleSub<%#Eval("Id")%>">
                                                <asp:Repeater ID="rptBodyStyleSub" runat="server">
                                                    <ItemTemplate>
                                                        <li class="list-group-item sublistItem" value='<%#Eval("SubListId")%>'><span class="badge" ischildcount="true">0</span><%#Eval("SubListName")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-vehiclefeatures">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-vehiclefeatures" href="#collapsevehiclefeatures">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Features_Heading_Text %></a>
                            </div>
                            <div id="collapsevehiclefeatures" class="panel-collapse collapse">

                                <ul class="list-group" data-type="vehiclefeatures">
                                    <li class="list-group-item active list-group-custom-default">All Features</li>
                                    <asp:Repeater ID="rbtFeatures" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge"><%#Eval("Count")%></span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-price">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-price" href="#collapsePrice">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_4 %></a>
                            </div>
                            <div id="collapsePrice" class="panel-collapse collapse">
                                <ul class="list-group" data-type="price">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_3 %></li>
                                    <asp:Repeater ID="rptPrice" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-year">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-year" href="#collapseYear">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_5 %></a>
                            </div>
                            <div id="collapseYear" class="panel-collapse collapse">
                                <ul class="list-group" data-type="year">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_4 %></li>
                                    <asp:Repeater ID="rptYear" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-mileage">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-mileage" href="#collapseMile">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_6 %></a>
                            </div>
                            <div id="collapseMile" class="panel-collapse collapse">
                                <ul class="list-group" data-type="mile">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_5 %></li>
                                    <asp:Repeater ID="rptMileage" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-make">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-make" href="#collapseMake">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_7 %></a>
                            </div>
                            <div id="collapseMake" class="panel-collapse collapse">
                                <ul class="list-group" data-type="make">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_6 %></li>
                                    <asp:Repeater ID="rptMake" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <%--                        <div class="panel-group" id="accordion-engine">
							<div class="panel panel-default">
								<div class="panel-heading">
									<a data-toggle="collapse" data-parent="#accordion-engine" href="#collapseEngine" class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span>Engine</a>
								</div>
								<div id="collapseEngine" class="panel-collapse collapse">
									<ul class="list-group" data-type="engine">
										<li class="list-group-item active list-group-custom-default"><span class="badge">0</span>All Engines</li>
										<asp:Repeater ID="rptEngine" runat="server">
											<ItemTemplate>
												<li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
											</ItemTemplate>
										</asp:Repeater>
									</ul>
								</div>
							</div>
						</div>
                    --%>
                    <div class="panel-group" id="accordion-engine">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-engine" href="#collapseEngine">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_8 %> </a>
                            </div>
                            <div id="collapseEngine" class="panel-collapse collapse">
                                <ul class="list-group" data-type="engine">
                                    <li class="list-group-item active list-group-custom-default" value=""><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_7 %></li>
                                    <asp:Repeater ID="rptEngine" runat="server" OnItemDataBound="rptEngine_OnItemDataBound">
                                        <ItemTemplate>
                                            <li parentid="EngineSub<%#Eval("Id")%>" hassublist="<%# Eval("HasSubList")%>" value='<%#Eval("Id")%>' class="list-group-item <%# (bool)Eval("HasSubList") ? "hassublist": "" %>" data-toggle="collapse">
                                                <span class="glyphicon glyphicon-plus padded-glyphicon parentItem" style='display: <%# (bool)Eval("HasSubList") ? "inline": "none" %>' aria-hidden="true"></span>
                                                <span class="badge" isparentmodelcount="true">0</span><%#Eval("Name")%>
                                            </li>

                                            <div class="collapse list-group-submenu" id="EngineSub<%#Eval("Id")%>">
                                                <asp:Repeater ID="rptEngineSub" runat="server">
                                                    <ItemTemplate>
                                                        <li class="list-group-item sublistItem" value='<%#Eval("SubListId")%>'><span class="badge" ischildcount="true">0</span><%#Eval("SubListName")%></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-fuel">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-fuel" href="#collapseFuel">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_9 %></a>
                            </div>
                            <div id="collapseFuel" class="panel-collapse collapse">
                                <ul class="list-group" data-type="fuel">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_8 %></li>
                                    <asp:Repeater ID="rptFuelType" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion-drive">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-drive" href="#collapseDrive">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_10 %></a>
                            </div>
                            <div id="collapseDrive" class="panel-collapse collapse">
                                <ul class="list-group" data-type="drive">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">0</span><%=SearchControl.List_Group_Custom_Default_Li_9 %></li>
                                    <asp:Repeater ID="rptDrive" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-transmission">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-transmission" href="#collapseTransmission">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span>Transmission</a>
                            </div>
                            <div id="collapseTransmission" class="panel-collapse collapse">
                                <ul class="list-group" data-type="transmission">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">600</span>All Transmissions</li>
                                    <asp:Repeater ID="rptTransmissions" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion-exterior-color">
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-exterior-color" href="#collapseColor">
                                <a class="collapsed"><span class="glyphicon glyphicon-plus padded-glyphicon" aria-hidden="true"></span><%=SearchControl.Panel_Heading_A_11 %></a>
                            </div>
                            <div id="collapseColor" class="panel-collapse collapse">
                                <ul class="list-group" data-type="color">
                                    <li class="list-group-item active list-group-custom-default"><span class="badge">600</span><%=SearchControl.List_Group_Custom_Default_Li_10 %></li>
                                    <asp:Repeater ID="rptExteriorColor" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item" value='<%#Eval("Id")%>'><span class="badge">0</span><%#Eval("Name")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-9 col-sm-8 col-xs-12 no-gutters">

                <div class="inventory-content">
                    <div class="container-fluid">
                        <div class="row">
                            <span id="lblCount">0 Vehicles Found</span>
                            <div style="float: right;" class="">
                                <span><%=SearchControl.Container_Fluid_Row_Div_Span %></span>
                                <span style="font-size: 1.0em; font-weight: bold; color: gray; width: 50%;">
                                    <%=SearchControl.Container_Fluid_Row_Div_Span_1 %>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <!--    Temporarily Disabling

					<div class="panel panel-default panel-collapse collapse" id="vin-lookup">
						<div class="panel-heading accordion-heading-select">
							<h4 class="panel-title" style="padding: 10px 15px;">VIN & Stock # Lookup</h4>
						</div>
						<div class="panel-body">
							<div class="row-fluid">
								<div class="col-md-12 padded-15">
									<h4 class="alert alert-danger hidden" id="search-res-msg">There were no vehicles matching that VIN or Stock #</h4>
									<p>Enter part of all of the full VIN number or Stock # without additional spaces or characters.</p>

									<div class="panel-group" id="accordion-vin" style="max-width: 300px;">
										<div class="panel panel-default">
											<div class="panel-heading accordion-heading-select">
												<input id="txtSearch" class="text" />
												<span class="" id="btnSearchVin"><i class="glyphicon glyphicon-search padded-glyphicon"></i>Search</span> 
											</div>
										</div>
									</div>



									<ul style="list-style: none; padding: 10px;">
										<li>VIN's are 17-characters long, input atleast 6 characters to perform the search</li>
										<li>VIN's do not contain the letters  I (i), O (o), and Q (q) (to avoid confusion with numerals 1 and 0)</li>
										<li>VIN's can be input in upper or lower case</li>
										<li>Stock #'s can be input in upper or lower case</li>
									</ul>
								</div>
							</div>

						</div>
					</div>

						Temporarily Disabling  -->

                <div class="" style="min-height: 55px;">
                    <nav class="vsr-pagenation" aria-label="<%=SearchControl.Vsr_Pagenation_Aria_Label %>">
                        <div id="vsr-pagenationTop">
                        </div>
                    </nav>
                </div>
                <div class="non-pagenation">
                    <%=SearchControl.Non_Pagenation %>
                    <input type="checkbox" name="my-checkbox" rel="canonical" data-animate="false" aria-label="non-pagenation" />
                </div>
                <div class="col-xs-12" id="vehicleList">
                </div>
                <input type="hidden" id="pageNumber" currentpage="1" />

                <div class="row col-xs-12">
                    <nav class="vsr-pagenation" aria-label="<%=SearchControl.Vsr_Pagenation_Aria_Label %>">
                        <div id="vsr-pagenationBottom">
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>



<asp:HiddenField runat="server" ID="HdfType" />
<asp:HiddenField runat="server" ID="HdfSearchText" />
<asp:HiddenField runat="server" ID="CanBeEdit" />
<asp:HiddenField runat="server" ID="defaultFilter" />
<asp:HiddenField runat="server" ID="previousVehicleData" Value="" />
<asp:HiddenField runat="server" ID="defaultMasterDataCounter" />
<asp:HiddenField runat="server" ID="rememberHistory" Value="false" />
<asp:HiddenField runat="server" ID="isMobile" Value="false" />
<asp:HiddenField runat="server" ID="currentPage" Value="1" />
<asp:HiddenField runat="server" ID="pageSize" />
<asp:HiddenField runat="server" ID="IsSetPagingVisible" Value="true" />
<asp:HiddenField runat="server" ID="total" Value="5" />


<script type="text/x-jquery-tmpl" id="vehicleListTemplate">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body ${IsHiddenCard}">
                <div id="vBox" class="row-fluid">
                    <a href="${VehicleInfoUrl}" class="col-md-4 col-xs-12 vBox-image">
                        <img class="lazy" src="${ImageFile}" alt="${Vin} ${Year} ${Make} ${Model} ${Trim}" />
                    </a>

                    <div class="col-md-4 col-xs-12 vBox-details">
                        {{if IsCertified || IsUsed }}
						  <h3><a class="vehicleCardTitle" href="${VehicleInfoUrl}">${Year} ${Make} ${Model} ${Trim}</a></h3>
                        {{else}}
						  <h3><a class="vehicleCardTitle" href="${VehicleInfoUrl}">${Year} ${Model} ${Trim}</a></h3>
                        {{/if}}
						<div class="label-group mileage ${IsDisplayMPG}">
                            <span>${MPGCity}&nbsp;CITY&nbsp;</span>
                            <img class="image" id="vbox-mileage-icon" alt="${MPGHighway}" src="<%= DealerImageGeneric %>content/icons/gas-pump-40.png" />
                            <span>&nbsp;${MPGHighway}&nbsp;HWY</span>
                        </div>

                        <div class="label-group vehicleCardExtColor ${IsDisplayExtColor}">
                            <label><%=SearchControl.Label_Group_Label_1 %></label>
                            ${ExtColor}
                        </div>

                        <div class="label-group vehicleCardMileage ${IsDisplayMileage}">
                            <label><%=SearchControl.Label_Group_Label_2 %></label>
                            ${Mileage}
                        </div>

                        <div class="label-group vehicleCardVin">
                            <label><%=SearchControl.Label_Group_Label_3 %></label>
                            ${Vin}
                        </div>
                        <div class="label-group  vehicleCardEngDescription">
                            <label><%=SearchControl.Label_Group_Label_4 %></label>
                            ${EngDescription}
                        </div>
                        <div class="label-group vehicleCardDriveTrain">
                            <label><%=SearchControl.Label_Group_Label_5 %></label>
                            ${DriveTrain}
                        </div>
                        <div class="label-group vehicleCardStockNum">
                            <label><%=SearchControl.Label_Group_Label_6 %></label>
                            ${StockNum}
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12 vBox-price  ${Vin}">
                        {{if IncentiveDisplayTypeId == <%=(int)DealerIncentiveTypeEnum.Normal %> || IsCertified || IsUsed}}
						  <div class="row vBox-price-wrapper">
                              {{if (SellingPrice!='Call' && MSRP!=0) || (SellingPrice!='Call' && MSRP!=0 && ConditionalSellingPrice!='Call' && NormalSellingPrice!='Call')}}
                                       <div class="col-md-12 ">
                                           {{if SellingPrice>MSRP}}
                                                <label class="msrpLabel pricing-label-small"><%=VehicleInfo.Used_Vehicle_MSRP_Override_Label %></label>
                                           <div class="price">$${SellingPrice}</div>

                                           {{else}}
                                                <label class="msrpLabel pricing-label-small">
                                                    {{if IsUsed }}
						                              <%=VehicleInfo.Used_Vehicle_MSRP_Label %>
                                                  {{else IsCertified}}
						                              <%=VehicleInfo.Certified_Vehicle_MSRP_Label %>
                                                    
                                                    {{/if}}</label>
                                           <div class="msrp">${MSRP}</div>
                                           {{/if}}
                                      
                                          

                                       </div>
                              {{/if}}
                               {{if IsUpfitted && ((SellingPrice!='Call' || MSRP!=0) || (SellingPrice!='Call' && MSRP!=0 && ConditionalSellingPrice!='Call' && NormalSellingPrice!='Call'))}}
                              <div class="col-md-12 upfitted ${IsUpfitted}">
                                  <label class="pricing-label-small"><%=SearchControl.Upfitting_Label %></label>
                                  <div class="rebate openRebate" vin="${Vin}" agilevehicleid="${AgileVehicleId}" isupfitted='true' modelid="${ModelTranslatedId}" iscashcoupon="false" value='${UpfittedCost}' programid="0">${UpfittedCost}</div>
                              </div>
                              {{/if}}
                               <div class="col-md-12  rebateDiv">
                                   {{each NormalOffers}}
									                 {{if IsCashCoupon}}
									                 							                 <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                   {{else}}
                                                                                <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Program #${Id}: ${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                   {{/if}}
                                          <div class="rebate openRebate" vin="${Vin}" iscashcoupon="${IsCashCoupon}" isupfitted='false'  modelid="${ModelTranslatedId}" incentivedisplaytypeid="${IncentiveDisplayTypeId}" agilevehicleid="${AgileVehicleId}" programid="${ProgramId}" value='${DoubleAmount}'>${DoubleAmount}</div>
                                   {{/each}}                              
                               </div>
                              <div class="col-md-12 ${IsDisplayDealerSaving}">
                                  <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Price includes Dealer Discount, please see Dealer for details.'><%=SearchControl.Pricing_Label_Small_3 %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                  <div class="price" value='${DealerSaving}'>${DealerSaving}</div>
                              </div>
                              {{if (SellingPrice!='Call' && MSRP!=0) || (SellingPrice!='Call' && MSRP!=0 && ConditionalSellingPrice!='Call' && NormalSellingPrice!='Call')}}
                              <div class="col-md-12   border-top-dash">
                                  <label class="pricing-label-small"><%=SearchControl.Pricing_Label_Small_4 %></label>
                                  {{if (SellingPrice=='Call')}}
                                    <div class="price your-price" value='$${SellingPrice}'>${SellingPrice}</div>
                                  {{else}}
                                  <div class="price your-price" value='$${SellingPrice}'>$${SellingPrice}</div>
                                  {{/if}}
                              </div>
                              {{else}}
                              <div class="col-md-12">
                                  {{if (SellingPrice=='Call')}}
                                  <label class="pricing-label-small"><%=SearchControl.Pricing_Label_Small_4 %></label>
                                  <div class="price your-price" value='$${SellingPrice}'>${SellingPrice}</div>
                                  {{else}}
                                  <label class="pricing-label-small"><%=SearchControl.Pricing_Label_Small_4 %>**</label>
                                  <div class="price your-price" value='$${SellingPrice}'>$${SellingPrice}</div>
                                  <div class="col-xs-12 nopadding text-center vic-disclaimer">
                                      <small>*Price includes Dealer Discount.  See dealer for details.</small>
                                  </div>
                                  {{/if}}
                              </div>

                              {{/if}}
                          </div>
                        {{else}}
						  <div class="row vBox-price-wrapper">
                              <%//Display in Detail %>
                                    {{if (SellingPrice=='Call' && MSRP==0) || (SellingPrice=='Call' && MSRP==0 && ConditionalSellingPrice=='Call' && NormalSellingPrice=='Call')}}
                                        <div class="col-md-12 ">

                                            <label class="msrpLabel pricing-label-small"><%=VehicleInfo.Price_Label %></label>
                                            <div>Call</div>
                                        </div>
                              {{else}}    
                                        <div class="col-md-12 ">
                                            {{if SellingPrice>MSRP+UpfittedCost}}
                                                <label class="msrpLabel pricing-label-small"><%=VehicleInfo.Asking_Price_Label %></label>
                                            <div class="msrp">${SellingPrice-UpfittedCost}</div>

                                            {{else}}
                                                <label class="msrpLabel pricing-label-small">
                                                    {{if IsUsed }}
						                              <%=VehicleInfo.Used_Vehicle_MSRP_Label %>
                                                  {{else IsCertified}}
						                              <%=VehicleInfo.Certified_Vehicle_MSRP_Label %>
                                                    {{else IsNew}}
						                              <%=VehicleInfo.New_Vehicle_MSRP_Label %>
                                       
                                                    {{/if}}</label>
                                            <div class="msrp">${MSRP}</div>

                                            {{/if}}
                                      

                                        </div>
                                        <div class="col-md-12 ${IsEmployeePrice}">
                                                <label class="msrpLabel pricing-label-small">Employee Price</label>
                                                <div class="price" >${EmployeePrice}</div>
                                            </div>
                              {{if IsUpfitted && ((SellingPrice!='Call' || MSRP!=0) || (SellingPrice!='Call' && MSRP!=0 && ConditionalSellingPrice!='Call' && NormalSellingPrice!='Call'))}}
                              <div class="col-md-12 upfitted ${IsUpfitted}">
                                  <label class="pricing-label-small">Upfitting</label>
                                  <div class="rebate openRebate" vin="${Vin}" agilevehicleid="${AgileVehicleId}" modelid="${ModelTranslatedId}" isupfitted='true' programid="0" iscashcoupon="false" value='${UpfittedCost}'>${UpfittedCost}</div>
                              </div>
                              
                              {{/if}}
                                  <div class="col-md-12  rebateDiv ${IsDisplayRebate}">
                                      {{each NormalOffers}}
									          {{if IsCashCoupon}}
 									                 <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                      {{else}}
                                                     <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Program #${Id}: ${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                      {{/if}}
                                          <div class="rebate openRebate" vin="${Vin}" iscashcoupon="${IsCashCoupon}" modelid="${ModelTranslatedId}" incentivedisplaytypeid="${IncentiveDisplayTypeId}" agilevehicleid="${AgileVehicleId}" programid="${ProgramId}" value='${DoubleAmount}'>${DoubleAmount}</div>
                                      {{/each}}                              
                                  </div>

                                  <div class="col-md-12  ${IsDisplayDealerSaving}">
                                      <label style="cursor: pointer" class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Price includes Dealer Discount, please see dealer for details.'><%=SearchControl.Pricing_Label_Small_3 %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                      <div class="price" value='${DealerSaving}'>${DealerSaving}</div>
                                  </div>

                                  <div class="col-md-12  border-top-dash">
                                      <label class="pricing-label-small"><%=SearchControl.Pricing_Label_Small_5 %>**</label>
                                      <div class="price your-price" value='${NormalSellingPrice}'>${NormalSellingPrice}</div>
                                  </div>


                                  <div class="col-md-12  rebateDiv ${IsDisplayConditionalPrice}">
                                      <span style="float: left;">Conditional offers: </span>
                                      <br />
                                      {{each ConditionalOffers}}
									         {{if IsCashCoupon}}
	 								                 <label class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" style="cursor: pointer;" data-original-title='${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                      {{else}}
                                                     <label class="pricing-label-small lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" style="cursor: pointer;" data-original-title='Program #${Id}: ${Disclaimer} See dealer for complete details.'>${Name} <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                      {{/if}}
                                          {{if DoubleAmount > 0 }}
                                            <div class="rebate openRebate" vin="${Vin}" iscashcoupon="${IsCashCoupon}" modelid="${ModelTranslatedId}" incentivedisplaytypeid="${IncentiveDisplayTypeId}" agilevehicleid="${AgileVehicleId}" programid="${ProgramId}" value='${DoubleAmount}'>${DoubleAmount}</div>
                                      {{else}}
                                            <div class="openRebate" vin="${Vin}" iscashcoupon="${IsCashCoupon}" modelid="${ModelTranslatedId}" incentivedisplaytypeid="${IncentiveDisplayTypeId}" agilevehicleid="${AgileVehicleId}" programid="${ProgramId}" value='${DoubleAmount}'>Qualify</div>
                                      {{/if}}
                                  
                                          {{/each}} 
                                  </div>
                                  <%-- <div class="col-md-12  border-top-dash ${IsDisplayConditionalPrice}">
                                          <label class="pricing-label-small"><%=SearchControl.Pricing_Label_Small_6 %> up to*</label>
                                          <div class="price your-price" value='${ConditionalSellingPrice}'>${ConditionalSellingPrice}</div>
                                      </div>--%>
                                  <div class="col-xs-12 nopadding text-center vic-disclaimer">
                                      <small>*Price includes Dealer Discount.  See dealer for details.</small>
                                  </div>
                                  {{/if}}
    </div>
    {{/if}}
						
						<div class="row vBox-button">
                            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                                <a href="${VehicleInfoUrl}" class="btn btn-primary btnInfo" id="btnInfo" title="<%=SearchControl.VBox_Button_Col_Xs_12 %>"><%=SearchControl.VBox_Button_Col_Xs_12 %></a>
                                <a href="${EditUrl}" class="btn btn-danger ${IsDisplayEditButton}" style="width: 100%; margin-top: 10px" id="btnEdit" title="Click to edit this vehicle">Edit</a>
                                <%--<a href="" target="_blank" vin="${Vin}" class="autocheck ${IdDisplayAutoCheck}">
                                    <img src="https://agiledealer.blob.core.windows.net/generic/AutoCheckFree.png" id="AutoCheckVehicleButton" alt="Experian AutoCheck" />
                                </a>--%>
                            </div>
                        </div>
                              <div class="clear-20 hidden-sm hidden-xs"></div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
</script>

<div id="ucRebate">
    <RebateControl:rebateControl runat="server" ID="RebateControl" />
</div>

<style>
    #new-vehicle .upfitted .rebate::before {
    content: "+$";
}
</style>