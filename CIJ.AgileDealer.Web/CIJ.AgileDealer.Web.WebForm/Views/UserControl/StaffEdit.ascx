﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffEdit.ascx.cs" Inherits="CIJ.AgileDealer.Web.WebForm.Views.UserControl.StaffEdit" %>
<%@ Register Src="~/Views/UserControl/Uploader/Uploader.ascx" TagName="Uploader" TagPrefix="uc1" %>

<div class="container">
    <h1>Edit Staff</h1>
    <section id="editStaff">
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-2">Frist Name</div>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtFristName" aria-label="First Name" CssClass="form-control" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Last Name</div>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtLastName" aria-label="Last Name" CssClass="form-control" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Email</div>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtEmail" aria-label="Email" CssClass="form-control" placeholder="email@domain.com" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Phone</div>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtPhone" aria-label="Phone" CssClass="form-control" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Photo</div>
                <div class="col-sm-10">
                    <a href="javascript:void(0)" id="link_upload_profile">Upload<br />
                        Profile Picture</a>
                    <uc1:Uploader ID="Uploader1" runat="server" IsUploadImage="1" ClientFormID="Profile" />
                </div>
            </div>
        </form>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#link_upload_profile').click(function () {
            openFileUploadProfile();
        });
    });

    function showFileUploadingProfile() {
    }

    function callbackUploadFileProfile(name, file, response, size, dimension) {
        if (response == 'Success') {

        } else {
            alert(response);
            $('#txtSelectFile').val('Image Name');
        }
    }
</script>
