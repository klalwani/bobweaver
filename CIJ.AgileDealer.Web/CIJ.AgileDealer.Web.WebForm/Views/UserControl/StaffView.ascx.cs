﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Enums;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
using CJI.AgileDealer.Web.WebForm;
using System.Web.UI.HtmlControls;

namespace CIJ.AgileDealer.Web.WebForm.Views.UserControl
{
    public partial class StaffView : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CanBeEdit.Value = (HttpContext.Current.User.IsInRole(RoleEnum.Administrator) || HttpContext.Current.User.IsInRole(RoleEnum.Dealer)).ToString();
            if (!IsPostBack)
            {
                var parameter = Request["departmentId"] != null 
                    ? Request["departmentId"].ToString() : string.Empty;
                var departmentId = 0;
                int.TryParse(parameter, out departmentId);
                DepartmentId = departmentId;
                LoadStaff();
                LoadDepartment();
            }
        }

        public int DepartmentId { get; set; }
        private void LoadStaff()
        {
            using (var db = new ServicePageDbContext())
            {
                var data = (from emp in db.Employees
                            join emp_depart in db.EmployeeDepartments
                            on emp.EmployeeId equals emp_depart.EmployeeId
                            where emp.DealerID == DealerID && emp.IsDeleted == false && (emp_depart.DepartmentId == DepartmentId || DepartmentId == 0)
                            select new
                            {
                                StaffID = emp.EmployeeId,
                                Email = emp.Email,
                                Avata = emp.Photo ?? @DealerImageGeneric + "content/staff/staffnotavailable.jpg",
                                StaffName = (emp.FirstName + " " + emp.LastName).Trim(),
                                Phone = (emp.OfficePhone) + ((!string.IsNullOrEmpty(emp.Ext)) ? (" x " + emp.Ext) : string.Empty),
                                IsHasAvata = emp.Photo != null ? "avata" : "none-avata",
                                IsManager = emp_depart.IsManager,
                                StaffRole = emp_depart.Title,
                                DeparmentId = emp_depart.DepartmentId,
                                Order = emp.Order
                            });
                var staffs = data.OrderBy(x => x.Order).ToList();
                rptStaff.DataSource = staffs;
                rptStaff.DataBind();
            }
        }
        private void LoadDepartment()
        {
            using (var db = new ServicePageDbContext())
            {
                var data = from department in db.Departments.Where(x=>x.DealerID == DealerID)
                           orderby department.Order ascending
                           select new
                           {
                               Name = department.Name,
                               DepartmentId = department.DepartmentId,
                               Url = "/dealership-info/staff" + "?departmentId=" + department.DepartmentId,
                               Order = department.Order 
                           };
                var departments = data.ToList();
                departments.Add(new {Name = "All", DepartmentId = 0, Url= "/dealership-info/staff", Order = 0 });
                departments = departments.OrderBy(c => c.Order).ToList();
                rptDepartment.DataSource = departments;
                rptDepartment.DataBind();
            }
        }

        private object LoadNewStaff()
        {
            using (var db = new ServicePageDbContext())
            {
                var data = (from emp in db.Employees
                            join emp_depart in db.EmployeeDepartments
                            on emp.EmployeeId equals emp_depart.EmployeeId
                            where emp.IsDeleted == false && emp.IsNew == true && emp.DealerID == DealerID
                            select new
                            {
                                StaffID = emp.EmployeeId,
                                Email = emp.Email,
                                Avata = emp.Photo ?? @DealerImageGeneric + "content/staff/staffnotavailable.jpg",
                                StaffName = (emp.FirstName + " " + emp.LastName).Trim(),
                                Phone = (emp.OfficePhone) + ((!string.IsNullOrEmpty(emp.Ext)) ? (" x " + emp.Ext) : string.Empty),
                                IsHasAvata = emp.Photo != null ? "avata" : "none-avata",
                                IsManager = emp_depart.IsManager,
                                StaffRole = emp_depart.Title,
                                DeparmentId = emp_depart.DepartmentId,
                                Order = emp.Order
                            });
                return data.OrderBy(x => x.Order).ToList();
            }
        }

        private object LoaExistStaff()
        {
            using (var db = new ServicePageDbContext())
            {
                var temp = (db.EmployeeDepartments.Where(x=>x.DealerID == DealerID)
                    .GroupBy(bet => bet.EmployeeId)
                    .Select(grp => new { grp, count = grp.Count() })
                    .Where(t => t.count > 1)
                    .Select(t => new
                    {
                        t.grp.Key
                    })).ToList();
                List<int> listInt = temp.Select(x => x.Key).ToList();

                var data = (from emp in db.Employees.Where(x => x.DealerID == DealerID)
                            join emp_depart in db.EmployeeDepartments
                            on emp.EmployeeId equals emp_depart.EmployeeId
                            where emp.IsDeleted == false
                            && listInt.Contains(emp.EmployeeId)
                            select new
                            {
                                StaffID = emp.EmployeeId,
                                Email = emp.Email,
                                Avata = emp.Photo ?? @DealerImageGeneric + "content/staff/staffnotavailable.jpg",
                                StaffName = (emp.FirstName + " " + emp.LastName).Trim(),
                                Phone = (emp.OfficePhone) + ((!string.IsNullOrEmpty(emp.Ext)) ? (" x " + emp.Ext) : string.Empty),
                                IsHasAvata = emp.Photo != null ? "avata" : "none-avata",
                                IsManager = emp_depart.IsManager,
                                StaffRole = emp_depart.Title,
                                DeparmentId = emp_depart.DepartmentId,
                                Order = emp.Order
                            });
                return data.OrderBy(x => x.Order).ToList();
            }
        }

        private object LoadStaffByDepartment(Enum.Department department)
        {
            using (var db = new ServicePageDbContext())
            {
                var data = (from emp in db.Employees.Where(x => x.DealerID == DealerID)
                            join emp_depart in db.EmployeeDepartments
                            on emp.EmployeeId equals emp_depart.EmployeeId
                            where emp.IsDeleted == false
                            select new
                            {
                                StaffID = emp.EmployeeId,
                                Email = emp.Email,
                                Avata = emp.Photo ?? @DealerImageGeneric + "content/staff/staffnotavailable.jpg",
                                StaffName = (emp.FirstName + " " + emp.LastName).Trim(),
                                Phone = (emp.OfficePhone) + ((!string.IsNullOrEmpty(emp.Ext)) ? (" x " + emp.Ext) : string.Empty),
                                IsHasAvata = emp.Photo != null ? "avata" : "none-avata",
                                IsManager = emp_depart.IsManager,
                                StaffRole = emp_depart.Title,
                                DeparmentId = emp_depart.DepartmentId,
                                Order = emp.Order
                            });
                if (department != Enum.Department.All)
                {
                    data = data.Where(x => x.DeparmentId == (int)department);
                }
                return data.OrderBy(x => x.Order).ToList();
            }
        }
    }
}