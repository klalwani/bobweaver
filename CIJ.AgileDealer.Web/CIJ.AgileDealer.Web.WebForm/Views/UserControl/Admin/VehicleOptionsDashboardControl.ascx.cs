﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class VehicleOptionsDashboardControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindVehicleFeatures();
        }
        private void BindVehicleFeatures()
        {
            var VehicleFactoryCodes = VehicleHelper.GetVehicleFactoryCodesOfAgileVehicles();
            grdvwVehicleOptionCodes.DataSource = VehicleFactoryCodes;
            grdvwVehicleOptionCodes.DataBind();
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.Visible = false;
            try
            {
                foreach (GridViewRow item in grdvwVehicleOptionCodes.Rows)
                {
                    if (!((HiddenField)item.FindControl("hdnFactoryCodes")).Value.Equals(((TextBox)item.FindControl("txtFactoryCodes")).Text,StringComparison.OrdinalIgnoreCase))
                    {
                        var agileVehicleFactoryCodeModel = new AgileVehicleFactoryCodeModel()
                        {
                            AgileVehicleId = Convert.ToInt32(grdvwVehicleOptionCodes.DataKeys[item.RowIndex].Value),
                            FactoryCodes = ((TextBox)item.FindControl("txtFactoryCodes")).Text 
                             
                        };
                        VehicleHelper.UpdateVehicleFactoryCodes(agileVehicleFactoryCodeModel);
                    }
                    
                }
                lblError.Text = "Records updated successfully.";
                lblError.Visible = true;
            }

            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
            BindVehicleFeatures();
        }
    }
}