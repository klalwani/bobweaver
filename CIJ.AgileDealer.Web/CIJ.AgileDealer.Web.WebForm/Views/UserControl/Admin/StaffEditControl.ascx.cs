﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class StaffEditControl : BaseUserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeId.Value = Page.RouteData.Values["EmployeeId"] as string;
            PopulateDataSource();
        }

        private void PopulateDataSource()
        {
            List<global::AgileDealer.Data.Entities.Content.Department> departments = GetDepartments();
            Departments.Value = JsonHelper.Serialize(departments);
        }

        private List<global::AgileDealer.Data.Entities.Content.Department> GetDepartments()
        {
            return MasterDataHelper.GetDepartments(DealerID);
        }
    }
}