﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class MissingIncentiveProgramsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!IsPostBack)
            {
                BindMissingPrograms();
            }
        }

        protected void btnSaveMissingIncentiveProgram_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtProgramNumber.Text.Trim()))
            {
                MissingIncentiveProgram(txtProgramNumber.Text.Trim());
            }
            else
            {
                lblError.Text = "Please enter Program Name.";
            }
        }

        protected void grdvwMissingIncentivePrograms_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteProgram")
            {
                DeleteIncentiveProgram(Convert.ToInt32(e.CommandArgument));
            }
        }

        private void BindMissingPrograms()
        {
            grdvwMissingIncentivePrograms.DataSource = VehicleHelper.GetMissingIncentivePrograms();
            grdvwMissingIncentivePrograms.DataBind();
        }


        private async void DeleteIncentiveProgram(int id)
        {
            await DeleteIncentiveProgramAsync(id);
            BindMissingPrograms();
            lblError.Text = "Record Deleted Successfully!";
        }

        private Task<bool> DeleteIncentiveProgramAsync(int id)
        {
            return Task.Run<bool>(() => VehicleHelper.DeleteMissingIncentiveProgram(id));
        }

        private async void MissingIncentiveProgram(string programNumber)
        {
            bool result = await MissingIncentiveProgramAsync(programNumber, Page.User.Identity.Name);
            if (result)
            {
                BindMissingPrograms();
                lblError.Text = "Record Updated Successfully!";
                txtProgramNumber.Text = string.Empty;
            }
        }

        private Task<bool> MissingIncentiveProgramAsync(string programNumber, string createdId)
        {
            return Task.Run<bool>(() => VehicleHelper.AddMissingIncentiveProgramAsync(programNumber, createdId));
        }
    }
}