﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgileVehicleEditControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.AgileVehicleEditControl" %>
<%--<%@ Register Src="~/Views/UserControl/Admin/AgileDealerProgramUpdateControl.ascx" TagPrefix="agileDealerProgramUpdateControl" TagName="AgileDealerProgramUpdateControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveEditControl.ascx" TagPrefix="IncentiveEditControl" TagName="IncentiveEditControl" %>--%>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>
<script src="../../../Scripts/Pages/Admin/AgileDealer.VehicleUpdate.js"></script>
<script src="../../../Scripts/jquery.mask.js"></script>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo" %>


<script>
    $(function () {
        // you may want to add an additional check to make sure that the parent is the element you expect it to be
        $('[checked="checked"]').parent().addClass('active');
    })
</script>

<div class="row form-title" style="height: auto; line-height: 1.5;">
    <span id="vehicleTitle"></span>
</div>

<div class="row form-wrapper">

    <div class="col-xs-12">
        <div class="row form-horizontal-input-padded">

            <div class="col-xs-12" id="contactInfo">

                <div class="row content-padded">
                    <div class="form-horizontal">
                        <div class="col-xs-12 col-sm-4">
                            <img alt="vehicle-image" id="vehicleImage" class="bcc-hero-image mdl-box-shadow" src="">
                            <div class="input-group col-xs-12">
                                <div class="input-group-addon">VIN</div>
                                <input type="text" class="form-control" data-val="true" readonly="readonly" data-val-required="true" id="vin" placeholder="Vin">
                            </div>
                            <div class="padded-15">
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox" id="option-lifted-truck">
                                        <label class="pull-right">
                                            <input type="checkbox" aria-label="isLift" name="isLift" id="isLift" title="Is Lift">Lifted Truck
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox edit-vehicle-two" id="option-current-special">
                                        <label class="pull-right">
                                            <input type="checkbox" aria-label="isSpecial" name="isSpecial" id="isSpecial" title="Is Special">Current Special
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox edit-vehicle-two" id="option-rental-special">
                                        <label class="pull-right">
                                            <input aria-label="isRentalSpecial" type="checkbox" name="isRentalSpecial" id="isRentalSpecial" title="Is Loaner Special">Loaner Specials
                                        </label>
                                    </fieldset>
                                </div>
                                 <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox edit-vehicle-two" id="option-upfitted">
                                        <label class="pull-right">
                                            <input type="checkbox" aria-label="isUpfitted" name="isUpfitted" id="isUpfitted" title="Is Upfitted">Upfitted
                                        </label>
                                    </fieldset>
                                </div>
                                 <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox edit-vehicle-two" id="option-hide-incentives">
                                        <label class="pull-right">
                                            <input aria-label="hideIncentives" type="checkbox" name="hideIncentives" id="hideIncentives" title="Hide Incentives">Hide Incentives
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12">
                                    <fieldset class="checkbox edit-vehicle-checkbox " id="option-current-visible">
                                        <label class="pull-right">
                                            <input type="checkbox" aria-label="isHidden" name="isHidden" id="isHidden" title="Hidden">Hidden
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-8">

                            <div class="col-xs-12 col-sm-12">

                                <fieldset class="form-group">
                                    <label for="msrp" class="col-sm-4 control-label" id="lblMSRPTitle" runat="server"></label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="msrp" placeholder="MSRP">
                                    </div>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label for="sellingPrice" class="col-sm-4 col-xs-12 control-label">Price</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="sellingPrice" placeholder="Price">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="employeeprice" class="col-sm-4 col-xs-12 control-label">Employee Price</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" id="employeeprice" placeholder="Employee Price">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="discount" class="col-sm-4 col-xs-12 control-label">Discount</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price" data-val="true" readonly="readonly" id="discount" placeholder="Discount">
                                    </div>
                                </fieldset>

                                <fieldset class="form-group">
                                    <div class="input-group col-sm-11 col-xs-12">
                                        <p class="pull-right ctaColor">Override one value at a time</p>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="msrp" class="col-sm-4 col-xs-12 control-label" id="lblMSRPOverrideTitle" runat="server"></label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" data-val="true" id="msrpoverride" placeholder="MSRP Override">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="overridePrice" class="col-sm-4 col-xs-12 control-label">Price Override</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" id="overridePrice" placeholder="Price Override">
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="discount" class="col-sm-4 col-xs-12 control-label">Discount Override</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control price ctaLink" data-val="true" id="discountoverride" placeholder="Discount Override">
                                    </div>
                                </fieldset>



                                <fieldset class="form-group">
                                    <label for="priceDate" class="col-sm-4 col-xs-12 control-label">Override Expiration</label>
                                    <div class="input-group col-sm-7 col-xs-12">
                                        <div class="btn-group" data-toggle="buttons" id="priceDate">
                                            <label class="btn btn-default active" data-toggle="out" href="#dateRangeField" name="ultilSold">
                                                <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                                Until Sold
                                            </label>
                                            <label class="btn btn-default" data-toggle="in" href="#dateRangeField" name="setDate">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Set Dates
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <!-- This should be displayed if the override is set to "Set Dates" and have today as the default start date -->
                                <fieldset class="form-group collapse" id="dateRangeField">
                                    <label for="priceDateStart" class="col-xs-4 control-label">Date Range</label>
                                    <div class="col-xs-3 no-left-pad">
                                        <input type="text" class="form-control" id="priceDateStart">
                                    </div>
                                    <div class="col-xs-3 no-left-pad">
                                        <input type="text" class="form-control" id="priceDateEnd">
                                    </div>
                                </fieldset>
                                <hr>
                                <fieldset class="form-group" style="margin-left: 100px !important;">
                                    <input type="button" class="btn btn-primary" id="btnUpdateVehicle" value="Save Changes" />
                                    <input type="button" class="btn btn-default" id="btnBack" value="Cancel" />
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row content-padded">

                    <!-- Temporarily disabling 
                            <fieldset class="form-group">
                                        <label for="overrideDescription" class="">Override Description</label>
                                        <textarea rows="8" class="form-control" id="overrideDescription" placeholder="Override Description"></textarea>
                                    </fieldset>
                            Temporarily disabling -->

                     <fieldset class="form-group text-right">
                        <label for="upfittedCost" class="col-sm-9 col-xs-12 control-label">Upfitted Cost</label>
                        <div class="input-group col-sm-3 col-xs-12">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control price ctaLink" data-val="true" id="upfittedCost" placeholder="Upfitted Cost">
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <!-- Autowriter description -->
                        <label for="upfittedDescription" class="">Upfitted Description</label>
                        <textarea rows="4" class="form-control" id="upfittedDescription" placeholder="Upfitted Description"></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <!-- Autowriter description -->
                        <label for="autoWriterDescription" class="">Description Override</label>
                        <textarea rows="12" class="form-control" id="autoWriterDescription" placeholder="Auto Writer Description"></textarea>
                    </fieldset>

                    <fieldset class="form-group">

                        <div id="vehicle-program-updated">

                            <%-- <asp:UpdatePanel ID="upnlVehicleIncentives" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                            <div class="col-md-12">
                                <div class="col-md-offset-7 col-md-5">
                                    <div class="panel pricing">
                                        <% if (IsDisplayOfferDetail == true)
                                            { %>
                                        <div class="row" id="msrpSection" runat="server">
                                            <div class="col-xs-6 text-left">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblMSRPSectionTitle" Style="font-weight: 700; margin-left: 0px;"></asp:Label>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblMSRP"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="employeePriceSection" runat="server">
                                            <div class="col-xs-6 text-left">
                                                <label style="font-weight: 700">Employee Price</label>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblEmployeePrice"></asp:Label>
                                            </div>
                                        </div>
                                        <% if (IsUpfitted == true)
                                            { %>
                                        <div class="row">
                                            <div class="col-xs-6 text-left">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblUpfittedCostTitle" Style="font-weight: 700"></asp:Label>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <asp:Label runat="server" CssClass="msrp-price rebate openRebate" data-toggle="modal" isUpfitted="true" ID="lblUpfittedCost"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 text-left">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblMarketValueTitle" Style="font-weight: 700"></asp:Label>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <asp:Label runat="server" CssClass="msrp-price" ID="lblMarketValue"></asp:Label>
                                            </div>
                                        </div>

                                        <% } %>

                                        <div class="row" id="normalOfferSection">
                                            <asp:Repeater runat="server" ID="rptNormalOffer">
                                                <ItemTemplate>
                                                    <div class="col-xs-12 nopadding">
                                                        <div class="col-xs-6 text-left ">
                                                            <label class="lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='<%# Eval("Disclaimer") %>' style="cursor: pointer;"><%# Eval("Name") %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                                        </div>
                                                        <div class="col-xs-6 text-right">
                                                            <label class="rebate openRebate"><%# Eval("DoubleAmount") %></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>

                                        <div class="row border-bottom" id="totalSavingSection">
                                            <div class="col-xs-6 text-left">
                                                <label class="lablTooltip" style="cursor: pointer;" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Price includes Dealer Discount and rebates, not all customers will qualify for all rebates, please see Dealer for details.'><%=VehicleInfo.Row_Col_Xs_6_Label_2 %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                            </div>
                                            <div class="col-md-6 text-right">
                                                <asp:Label runat="server" CssClass="dealersaving" ID="lblDealerSave"></asp:Label>
                                            </div>
                                        </div>


                                        <% if (IncentiveDisplayTypeId == (int)DealerIncentiveTypeEnum.Normal || !IsNew)
                                            { %>
                                        <div class="row">
                                            <div class="col-xs-12 nopadding">
                                                <div class="col-xs-6 text-left">
                                                    <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_5 %></label>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <asp:Label CssClass="price-main" runat="server" ID="lblPrice"></asp:Label>
                                                </div>
                                            </div>

                                            <% }
                                                else
                                                { %>

                                            <div class="row">
                                                <div class="col-xs-12 col-lg-12">
                                                    <div class="col-xs-6 col-lg-6">
                                                        <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_3 %></label>
                                                    </div>
                                                    <div class="col-xs-6 col-lg-6 text-right">
                                                        <asp:Label CssClass="price-main" runat="server" ID="lblNormalOfferPrice"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row margintop10" id="conditionalOfferSection" runat="server">
                                                <div class="col-md-12">
                                                    <strong style="float: left; color: #ff8d28">Conditional Offers: </strong>
                                                </div>
                                                <asp:Repeater runat="server" ID="rptConditionalOffer">

                                                    <ItemTemplate>
                                                        <div class="col-xs-12  col-lg-12">
                                                            <div class="col-xs-6 col-lg-6">
                                                                <label class="lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='<%# Eval("Disclaimer") %>' style="cursor: pointer;"><%# Eval("Name") %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                                                            </div>
                                                            <div class="col-xs-6 col-lg-6 text-right">
                                                                <label class="rebate openRebate"><%# Eval("DoubleAmount") %></label>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <div class="row border-top hide" runat="server" id="conditionalSection">
                                                <div class="col-xs-12 nopadding">
                                                    <div class="col-xs-6 text-left">
                                                        <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_4 %></label>
                                                    </div>
                                                    <div class="col-xs-6 text-right">
                                                        <asp:Label CssClass="price-main" runat="server" ID="lblConditionalPrice"></asp:Label>
                                                    </div>
                                                </div>
                                                <%--                   --%>
                                            </div>

                                            <% } %>
                                            <% }
                                                else
                                                { %>
                                            <div class="row">
                                                <div class="col-xs-12 nopadding">
                                                    <div class="col-xs-6 text-left">
                                                        <label class="price-label"><%=VehicleInfo.Price_Label %></label>
                                                    </div>
                                                    <div class="col-xs-6 text-right">
                                                        <strong>Call</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <% } %>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <td class="alert-danger" style="width: 50px;">&nbsp;</td>
                                        <td>&nbsp- In-Active Incentives </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-md-12">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-md-12">

                                <asp:GridView ID="grdvwVehiclePrograms" OnRowDataBound="grdvwVehiclePrograms_RowDataBound" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-bordered table-hover">
                                    <Columns>
                                        <asp:BoundField DataField="Id" ItemStyle-HorizontalAlign="Left" HeaderText="Program Number" />
                                        <asp:BoundField DataField="Name" ItemStyle-HorizontalAlign="Left" HeaderText="Name" />
                                        <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" />
                                        <asp:BoundField DataField="EndDate" ItemStyle-HorizontalAlign="Left" HeaderText="End Date" />
                                        <asp:BoundField DataField="Amount" ItemStyle-HorizontalAlign="Left" HeaderText="Amount" />
                                        <asp:BoundField DataField="Position" ItemStyle-HorizontalAlign="Left" HeaderText="Type" />
                                        <asp:BoundField DataField="IncentiveType" ItemStyle-HorizontalAlign="Left" HeaderText="Incentive Type" />
                                        <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIncentiveTranslatedId" runat="server" Visible="false" Text='<%#Eval("IncentiveTranslatedId") %>'></asp:Label>
                                                <asp:Label ID="lblActive" runat="server" Visible="false" Text='<%#Eval("Active") %>'></asp:Label>
                                                <asp:Label ID="lblProgramId" runat="server" Visible="false" Text='<%#Eval("ProgramId") %>'></asp:Label>
                                                <asp:Label ID="lblDiscountType" runat="server" Visible="false" Text='<%#Eval("DiscountType") %>'></asp:Label>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("Active")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="col-md-12">
                                <label>Custom Incentives/Discounts</label>
                            </div>
                            <div class="col-md-12">
                                <asp:GridView ID="grdvwVehicleCustomIncentives" OnRowDataBound="grdvwVehicleCustomIncentives_RowDataBound" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-bordered table-hover">
                                    <Columns>
                                        <asp:BoundField DataField="Id" ItemStyle-HorizontalAlign="Left" HeaderText="Program Number" />
                                        <asp:BoundField DataField="Name" ItemStyle-HorizontalAlign="Left" HeaderText="Name" />
                                        <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" />
                                        <asp:BoundField DataField="EndDate" ItemStyle-HorizontalAlign="Left" HeaderText="End Date" />
                                        <asp:BoundField DataField="Amount" ItemStyle-HorizontalAlign="Left" HeaderText="Amount" />
                                        <asp:BoundField DataField="Position" ItemStyle-HorizontalAlign="Left" HeaderText="Type" />
                                        <asp:BoundField DataField="IncentiveType" ItemStyle-HorizontalAlign="Left" HeaderText="Incentive Type" />
                                        <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIncentiveTranslatedId" runat="server" Visible="false" Text='<%#Eval("IncentiveTranslatedId") %>'></asp:Label>
                                                <asp:Label ID="lblActive" runat="server" Visible="false" Text='<%#Eval("Active") %>'></asp:Label>
                                                <asp:Label ID="lblProgramId" runat="server" Visible="false" Text='<%#Eval("ProgramId") %>'></asp:Label>
                                                <asp:Label ID="lblDiscountType" runat="server" Visible="false" Text='<%#Eval("DiscountType") %>'></asp:Label>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("Active")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Standalone" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsHideOtherIncentives" runat="server" Checked='<%# Convert.ToBoolean(Eval("HideOtherIncentives")) %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Master" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsMasterIncentives" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsMasterIncentive")) %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>

                            <%--<agileDealerProgramUpdateControl:AgileDealerProgramUpdateControl runat="server" ID="AgileDealerProgramUpdateControl" />--%>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div class="col-md-12">

                            <asp:Button ID="btnSaveIncentiveChanges" runat="server" CssClass="btn btn-primary" Text="Save Incentives" OnClientClick="return ConfirmUpdate();" OnClick="btnSaveIncentiveChanges_Click" />
                        </div>
                    </fieldset>
                    <div class="clear-20"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<IncentiveEditControl:IncentiveEditControl runat="server" ID="IncentiveEditControl" />
<asp:HiddenField runat="server" ID="ModelList"/>--%>

<asp:HiddenField runat="server" ID="VinHdf" />
<asp:HiddenField runat="server" ID="AgileVehicleId" />
<asp:HiddenField ID="hdnIsShowUpfittedMSRP" runat="server" />


<script>

    $(document).ready(function () {
        $('#msrpoverride').on("blur", function () {
            $('#msrpoverride').val(CommaFormatted($('#msrpoverride').val()));
        });
        $('#employeeprice').on("blur", function () {
            $('#employeeprice').val(CommaFormatted($('#employeeprice').val()));
        });
        $('#upfittedCost').on("blur", function () {
            $('#upfittedCost').val(CommaFormatted($('#upfittedCost').val()));
        });
        $('#discountoverride').on("blur", function () {
            $('#discountoverride').val(CommaFormatted($('#discountoverride').val()));
        });
        $('#overridePrice').on("blur", function () {
            $('#overridePrice').val(CommaFormatted($('#overridePrice').val()));
        });
    });

    function HideLoader() {
        $.unblockUI(siteMaster.loadingWheel());
    }
    function ConfirmUpdate() {

        var isConfirm = confirm('Are you sure you want to change the status of Incentive?');
        if (isConfirm)
            $.blockUI(siteMaster.loadingWheel());

        return isConfirm;
    }
    $('[data-toggle="tooltip"]').tooltip();

    if ($('[id$="lblDealerSave"]').text() == '$0' || $('[id$="lblDealerSave"]').text() == '- $0' || $('[id$="lblPrice"]').text().startsWith('$0')) {
        $('#totalSavingSection').hide();
    } else {
        $('#totalSavingSection').show();
    }
</script>
