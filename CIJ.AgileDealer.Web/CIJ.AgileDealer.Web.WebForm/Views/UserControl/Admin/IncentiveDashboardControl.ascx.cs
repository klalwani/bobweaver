﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class IncentiveDashboardControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<MasterData> modelTranslateds = MasterDataHelper.GetModelTranslated(new List<int>());
            ModelList.Value = JsonHelper.Serialize(modelTranslateds.OrderBy(x => x.Name));
            IncentiveEditControl.IsEditForVin = false;
        }
    }
}