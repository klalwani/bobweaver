﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogManagementControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.BlogManagementControl" %>

<script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.BlogManagement.js"></script>
<div id="blogManagementDashboard" class="container">
    <div id="vehicle-segment-page">
        <div class="body-content-wrapper">
            <div class="container-fluid">
                <div class="row form-title">
                    <span>Blog Dashboard</span>

                    <input type="button" id="btnAddNewBlog" name="btnAddNewBlog" value="Add New Blog" class="btn btn-success" />

                </div>
                <table id="tableBlogManagement" class="table table-hover hover row-border cell-border display stripe" style="width: 100% !important;" data-order='[[4, "desc" ]]' data-page-length='10'>
                    <thead>
                        <tr>
                            <th>BlogEntryId</th>
                            <th>Blog Title</th>
                            <th>Blog Categories</th>
                            <th>Blog Tags</th>
                            <th>Date Created</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>BlogEntryId</th>
                            <th>Blog Title</th>
                            <th>Blog Categories</th>
                            <th>Blog Tags</th>
                            <th>Date Created</th>
                            <th>Detail</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
