﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Web.UI.HtmlControls;
using Microsoft.Ajax.Utilities;
using System.Text;
using Dynamite.Extensions;
using AgileDealer.Data.Entities.Incentives;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class VehicleBulkIncentiveUpdateDashboardControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                lblTotalRecords.Text = "";
                lblError.Text = "";
                if (!IsPostBack)
                {
                    ViewState["IncentiveTranslatedId"] = 0;
                    ViewState["IncludeUsed"] = 1;
                    //ddlyear.DataSource = MasterDataHelper.GetYearList().OrderByDescending(x => Convert.ToInt32(x.Name));
                    //ddlMake.DataSource = MasterDataHelper.GetMakes().OrderBy(x => x.Name);
                    //ddlModel.DataSource = MasterDataHelper.GetModels(1).OrderBy(x => x.Name);
                    //ddlTrims.DataSource = MasterDataHelper.GetTrimList();
                    //ddlChromeStyles.DataSource = MasterDataHelper.GetChromeStyleIds();

                    //ddlyear.DataBind();
                    //ddlModel.DataBind();
                    //ddlMake.DataBind();
                    //ddlTrims.DataBind();
                    //ddlChromeStyles.DataBind();

                    ddlyear.Items.Insert(0, new ListItem("Select Year", ""));
                    ddlMake.Items.Insert(0, new ListItem("Select Make", ""));
                    ddlModel.Items.Insert(0, new ListItem("Select Model", ""));
                    ddlTrims.Items.Insert(0, new ListItem("Select Trim", ""));
                    ddlChromeStyles.Items.Insert(0, new ListItem("Select Style", ""));

                    chkIncludeUsed.Checked = false;
                    hdnFilteredText.Value = "";
                    BindCustomIncentives();

                    ViewState["sortdr"] = "asc";

                    //  divFilterContent.Visible = false;
                    GetVehicles("Year", 0, chkIncludeUsed.Checked);
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }

        }
        private void BindCustomIncentives()
        {
            List<VehicleIncentivesModel> lstVehicleIncentives = VehicleHelper.GetMasterCustomIncentives(DealerInfoHelper.DealerID, chkIncludeUsed.Checked);

            grdvwVehicleCustomIncentives.DataSource = lstVehicleIncentives.Where(x => x.IncentivetranslatedId != 0);
            grdvwVehicleCustomIncentives.DataBind();
        }
        private void GetVehicles(string SortExpression = "Year", int IncentiveTranslatedId = 0, bool IncludeUsed = false, string DropdownFilter = "", bool ResetDropdowns = false)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "asc")
                ViewState["sortdr"] = "desc";
            else
                ViewState["sortdr"] = "asc";

            List<VehicleData> BulkUpdateVehicles = VehicleHelper.GetVehiclesForBulkIncentiveUpdate(ddlyear.SelectedValue, ddlMake.SelectedValue, ddlModel.SelectedValue, ddlTrims.SelectedValue, IncludeUsed, ddlChromeStyles.SelectedValue, txtVin.Text.Trim(), IncentiveTranslatedId, SortExpression, Convert.ToString(ViewState["sortdr"]));
            grdvwVehicleBulkUpdateDashboard.DataSource = BulkUpdateVehicles.OrderBy(SortExpression + " " + ViewState["sortdr"]);
            grdvwVehicleBulkUpdateDashboard.DataBind();

            lblTotalRecords.Text = "Total Rows: " + grdvwVehicleBulkUpdateDashboard.Rows.Count.ToString();

            btnApplyIncentives_Bottom.Visible = grdvwVehicleBulkUpdateDashboard.Rows.Count > 0;
            btnApplyIncentives_Top.Visible = grdvwVehicleBulkUpdateDashboard.Rows.Count > 0;

            //  btnRemoveIncentives_Bottom.Visible = grdvwVehicleBulkUpdateDashboard.Rows.Count > 0;
            /// btnRemoveIncentives_Top.Visible = grdvwVehicleBulkUpdateDashboard.Rows.Count > 0;

            System.Threading.Thread.Sleep(2000);
            //  ScriptManager.RegisterStartupScript(upnlVehicleBulkUpdateDashboard, this.GetType(), "HideLoader", "HideLoadingWheel();", true);

            if (!IsPostBack || ResetDropdowns)
            {
                BindDropdowns(BulkUpdateVehicles);
            }

            switch (DropdownFilter)
            {
                case "":
                    break;
                case "Year":
                    ddlMake.Items.Clear();
                    ddlMake.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.MakeId, Name = item.Make }).DistinctBy(p => new { p.Id, p.Name });
                    ddlMake.DataBind();
                    ddlMake.Items.Insert(0, new ListItem("Select Make", ""));
                    ddlModel.Items.Clear();
                    ddlModel.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.ModelId, Name = item.Model }).DistinctBy(p => new { p.Id, p.Name });
                    ddlModel.DataBind();
                    ddlModel.Items.Insert(0, new ListItem("Select Model", ""));
                    ddlTrims.Items.Clear();
                    ddlTrims.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.TrimId, Name = item.Trim }).DistinctBy(p => new { p.Id, p.Name });
                    ddlTrims.DataBind();
                    ddlTrims.Items.Insert(0, new ListItem("Select Trim", ""));
                    ddlChromeStyles.Items.Clear();
                    ddlChromeStyles.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Name = item.StandardStyle }).DistinctBy(p => new { p.Id, p.Name });
                    ddlChromeStyles.DataBind();
                    ddlChromeStyles.Items.Insert(0, new ListItem("Select Style", ""));

                    break;
                case "Make":

                    if (ddlModel.SelectedValue.Equals(""))
                    {
                        ddlModel.Items.Clear();
                        ddlModel.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.ModelId, Name = item.Model }).DistinctBy(p => new { p.Id, p.Name });
                        ddlModel.DataBind();
                        ddlModel.Items.Insert(0, new ListItem("Select Model", ""));
                    }
                    if (ddlTrims.SelectedValue.Equals(""))
                    {
                        ddlTrims.Items.Clear();
                        ddlTrims.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.TrimId, Name = item.Trim }).DistinctBy(p => new { p.Id, p.Name });
                        ddlTrims.DataBind();
                        ddlTrims.Items.Insert(0, new ListItem("Select Trim", ""));
                    }
                    if (ddlChromeStyles.SelectedValue.Equals(""))
                    {
                        ddlChromeStyles.Items.Clear();
                        ddlChromeStyles.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Name = item.StandardStyle }).DistinctBy(p => new { p.Id, p.Name });
                        ddlChromeStyles.DataBind();
                        ddlChromeStyles.Items.Insert(0, new ListItem("Select Style", ""));
                    }

                    break;
                case "Model":
                    if (ddlTrims.SelectedValue.Equals(""))
                    {
                        ddlTrims.Items.Clear();
                        ddlTrims.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.TrimId, Name = item.Trim }).DistinctBy(p => new { p.Id, p.Name });
                        ddlTrims.DataBind();
                        ddlTrims.Items.Insert(0, new ListItem("Select Trim", ""));
                    }
                    if (ddlChromeStyles.SelectedValue.Equals(""))
                    {
                        ddlChromeStyles.Items.Clear();
                        ddlChromeStyles.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Name = item.StandardStyle }).DistinctBy(p => new { p.Id, p.Name });
                        ddlChromeStyles.DataBind();
                        ddlChromeStyles.Items.Insert(0, new ListItem("Select Style", ""));
                    }

                    break;
                case "Trims":
                    if (ddlChromeStyles.SelectedValue.Equals(""))
                    {
                        ddlChromeStyles.Items.Clear();
                        ddlChromeStyles.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Name = item.StandardStyle }).DistinctBy(p => new { p.Id, p.Name });
                        ddlChromeStyles.DataBind();
                        ddlChromeStyles.Items.Insert(0, new ListItem("Select Style", ""));
                    }
                    break;
            };

            //if (Convert.ToString(ViewState["IncentiveTranslatedId"]) != "0")
            //{
            //    foreach (GridViewRow gridviewRow in grdvwVehicleBulkUpdateDashboard.Rows)
            //    {
            //        ((HtmlInputCheckBox)gridviewRow.FindControl("chkSelectAllVehicles")).Checked = true;
            //    }
            //}
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked);
            BuildSearchString();
        }
        private void BuildSearchString()
        {
            divFilterContent.Attributes.Add("class", "show");
            lblFilterItem.Text = hdnFilteredText.Value;
            if (!string.IsNullOrEmpty(ddlyear.SelectedValue))
                lblFilterItem.Text += " " + ddlyear.SelectedValue;
            if (!string.IsNullOrEmpty(ddlMake.SelectedValue))
                lblFilterItem.Text += " " + ddlMake.SelectedItem.Text;
            if (!string.IsNullOrEmpty(ddlModel.SelectedValue))
                lblFilterItem.Text += "  " + ddlModel.SelectedItem.Text;
            if (!string.IsNullOrEmpty(ddlTrims.SelectedValue))
                lblFilterItem.Text += " " + ddlTrims.SelectedItem.Text;
            if (!string.IsNullOrEmpty(ddlChromeStyles.SelectedValue))
                lblFilterItem.Text += " " + ddlChromeStyles.SelectedItem.Text;
            if (!string.IsNullOrEmpty(txtVin.Text))
                lblFilterItem.Text += " " + txtVin.Text.Trim();

        }
        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            ClearFilters();
            divFilterContent.Attributes.Add("class", "hide");
        }
        private void ClearFilters()
        {
            ddlyear.SelectedValue = "";
            ddlMake.SelectedValue = "";
            ddlModel.SelectedValue = "";
            ddlTrims.SelectedValue = "";
            ddlChromeStyles.SelectedValue = "";
            txtVin.Text = "";
            ViewState["IncentiveTranslatedId"] = 0;
            ViewState["IncludeUsed"] = chkIncludeUsed.Checked;
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked);
            lblFilterItem.Text = "";
            hdnFilteredText.Value = "";

        }

        protected void grdvwVehicleBulkUpdateDashboard_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (e.SortExpression != "OriginalSellingPrice")
            {
                GetVehicles(e.SortExpression);
                for (int iCounter = 0; iCounter < grdvwVehicleBulkUpdateDashboard.HeaderRow.Cells.Count-1; iCounter++)
                { 
                    grdvwVehicleBulkUpdateDashboard.HeaderRow.Cells[iCounter].CssClass = "sorting";
                }
                if (Convert.ToString(ViewState["sortdr"]) == "asc")
                    grdvwVehicleBulkUpdateDashboard.HeaderRow.Cells[GetIndex(e.SortExpression)].CssClass = "sorting_asc";
                else
                    grdvwVehicleBulkUpdateDashboard.HeaderRow.Cells[GetIndex(e.SortExpression)].CssClass = "sorting_desc";


            }
        }

        private int GetIndex(string SortExp)
        {
            int i = 0;
            foreach (DataControlField c in grdvwVehicleBulkUpdateDashboard.Columns)
            {
                if (c.SortExpression == SortExp)
                    return i;
                else
                    i += 1;
            }

            return i;
        }


        protected void grdvwVehicleCustomIncentives_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //InActive
                //                if (((Label)e.Row.FindControl("lblActive")).Text == "0")
                //                  e.Row.CssClass = "alert-danger";

                //if (((Label)e.Row.FindControl("lblIncentiveTranslatedId")).Text != "0")
                //  e.Row.CssClass = "alert-success";

                if (e.Row.Cells[5].Text == "0")
                    e.Row.Cells[5].Text = "";
                else
                {
                    if (((Label)e.Row.FindControl("lblDiscountType")).Text == "2")
                        e.Row.Cells[5].Text = e.Row.Cells[5].Text + "%";
                    else
                        e.Row.Cells[5].Text = "$" + e.Row.Cells[5].Text;
                }

            }
        }

        protected void grdvwVehicleCustomIncentives_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TotalVehicles")
                {
                    ViewState["IncentiveTranslatedId"] = e.CommandArgument;
                    ViewState["IncludeUsed"] = 1;
                    GetVehicles("Year", Convert.ToInt32(e.CommandArgument), chkIncludeUsed.Checked, "", true);
                    BuildSearchString();
                    foreach (GridViewRow gridviewRow in grdvwVehicleBulkUpdateDashboard.Rows)
                    {
                        ((HtmlInputCheckBox)gridviewRow.FindControl("chkSelectAllVehicles")).Checked = true;
                    }
                    foreach (GridViewRow gridviewRow in grdvwVehicleCustomIncentives.Rows)
                    {
                        if (((Label)gridviewRow.FindControl("lblIncentiveTranslatedId")).Text.Equals(e.CommandArgument))
                        {
                            ((HtmlInputCheckBox)gridviewRow.FindControl("chkSelectCustomIncentive")).Checked = true;
                            gridviewRow.CssClass = "alert-info";
                        }
                        else
                        {
                            ((HtmlInputCheckBox)gridviewRow.FindControl("chkSelectCustomIncentive")).Checked = false;
                            gridviewRow.CssClass = "";
                        }
                    }

                    //lblFilterItem.Text= grdvwVehicleCustomIncentives.SelectedRow.FindControl("")
                    // ScriptManager.RegisterStartupScript((GridView)sender, this.GetType(), "HideLoader", "HideLoadingWheel();", true);
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void lnkRemoveFilter_ServerClick(object sender, EventArgs e)
        {
            ClearFilters();
            divFilterContent.Attributes.Add("class", "hide");

        }

        protected void btnApplyIncentives_Top_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbIncludeVins = new StringBuilder();
                StringBuilder sbExcludeVins = new StringBuilder();
                foreach (GridViewRow rowItem in grdvwVehicleBulkUpdateDashboard.Rows)
                {
                    if (((HtmlInputCheckBox)rowItem.FindControl("chkSelectAllVehicles")).Checked)
                    {
                        sbIncludeVins.Append(((Label)rowItem.FindControl("lblVin")).Text + ",");
                    }
                    else
                    {
                        sbExcludeVins.Append(((Label)rowItem.FindControl("lblVin")).Text + ",");
                    }
                }
                string strIncludeVins = "";
                string strExcludeVins = "";

                //This means user has cleared filters so only apply incentive to selected vehicels and don't exclude anyone which is not selected.
                if (Convert.ToString(ViewState["IncentiveTranslatedId"]) == "0")
                {
                    strExcludeVins = "";
                    sbExcludeVins = new StringBuilder();
                }
                foreach (GridViewRow rowItem in grdvwVehicleCustomIncentives.Rows)
                {
                    if (((HtmlInputCheckBox)rowItem.FindControl("chkSelectCustomIncentive")).Checked)
                    {
                        ViewState["IncentiveTranslatedId"] = ((Label)rowItem.FindControl("lblIncentiveTranslatedId")).Text;

                        IncentiveTranslated incentiveTranslated = VehicleHelper.GetExcludeVinsOfIncentive(Convert.ToInt32(((Label)rowItem.FindControl("lblIncentiveTranslatedId")).Text));
                        if (incentiveTranslated != null)
                        {
                            sbExcludeVins.Append(incentiveTranslated.ExcludeVINs);
                            sbIncludeVins.Append(incentiveTranslated.Vin);
                        }

                        if (sbIncludeVins != null)
                        {
                            foreach (var item in sbIncludeVins.ToString().Split(new char[] { ',' }))
                            {
                                if (item != "")
                                {
                                    sbExcludeVins.Replace(item, "");
                                    sbExcludeVins.Replace(",,", ",");
                                }
                            }
                        }
                        strIncludeVins = Convert.ToString(sbIncludeVins).TrimEnd(',');
                        strExcludeVins = Convert.ToString(sbExcludeVins).TrimEnd(',');

                        VehicleHelper.UpdateBulkVehicleIncentives(Convert.ToInt32(((Label)rowItem.FindControl("lblIncentiveTranslatedId")).Text), strExcludeVins, strIncludeVins);
                    }
                }

                BindCustomIncentives();
                //foreach (GridViewRow gridviewRow in grdvwVehicleCustomIncentives.Rows)
                //{
                //    if (((Label)gridviewRow.FindControl("lblIncentiveTranslatedId")).Text.Equals(ViewState["IncentiveTranslatedId"]))
                //    {
                //        ((HtmlInputCheckBox)gridviewRow.FindControl("chkSelectCustomIncentive")).Checked = true;
                //        gridviewRow.CssClass = "alert-info";
                //    }
                //}
                ClearFilters();
                divFilterContent.Attributes.Add("class", "hide");
                lblError.Text = "Incentives have been applied successfully.";

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked, "Year");
            BuildSearchString();

        }

        protected void ddlMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked, "Make");
            BuildSearchString();
        }

        protected void ddlModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked, "Model");
            BuildSearchString();
        }

        protected void ddlTrims_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked, "Trims");
            BuildSearchString();
        }

        protected void chkIncludeUsed_CheckedChanged(object sender, EventArgs e)
        {
            ClearFilters();
            BindCustomIncentives();
            GetVehicles("Year", 0, chkIncludeUsed.Checked, "", true);
        }

        private void BindDropdowns(List<VehicleData> BulkUpdateVehicles)
        {
            ddlyear.Items.Clear();
            ddlyear.SelectedValue = null;

            ddlyear.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.Year }).OrderByDescending(x => x.Id).DistinctBy(p => new { p.Id });
            ddlyear.DataBind();
            ddlyear.Items.Insert(0, new ListItem("All Years", ""));

            if (ddlyear.Items.Count == 2)
                ddlyear.SelectedIndex = 1;

            ddlMake.Items.Clear();
            ddlMake.SelectedValue = null;
            ddlMake.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.MakeId, Name = item.Make }).DistinctBy(p => new { p.Id, p.Name });
            ddlMake.DataBind();
            ddlMake.Items.Insert(0, new ListItem("All Makes", ""));

            if (ddlMake.Items.Count == 2)
                ddlMake.SelectedIndex = 1;

            ddlModel.Items.Clear();
            ddlModel.SelectedValue = null;
            ddlModel.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.ModelId, Name = item.Model }).DistinctBy(p => new { p.Id, p.Name });
            ddlModel.DataBind();
            ddlModel.Items.Insert(0, new ListItem("All Models", ""));

            if (ddlModel.Items.Count == 2)
                ddlModel.SelectedIndex = 1;


            ddlTrims.Items.Clear();
            ddlTrims.SelectedValue = null;
            ddlTrims.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Id = item.TrimId, Name = item.Trim }).DistinctBy(p => new { p.Id, p.Name });
            ddlTrims.DataBind();
            ddlTrims.Items.Insert(0, new ListItem("All Trims", ""));

            if (ddlTrims.Items.Count == 2)
                ddlTrims.SelectedIndex = 1;


            ddlChromeStyles.Items.Clear();
            ddlChromeStyles.SelectedValue = null;
            ddlChromeStyles.DataSource = BulkUpdateVehicles.Select(item => new MasterData { Name = item.StandardStyle }).DistinctBy(p => new { p.Id, p.Name });
            ddlChromeStyles.DataBind();
            ddlChromeStyles.Items.Insert(0, new ListItem("All Styles", ""));

            if (ddlChromeStyles.Items.Count == 2)
                ddlChromeStyles.SelectedIndex = 1;

        }

        protected void ddlChromeStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetVehicles("Year", Convert.ToInt32(ViewState["IncentiveTranslatedId"]), chkIncludeUsed.Checked, "StyleId");
            BuildSearchString();
        }
    }
}