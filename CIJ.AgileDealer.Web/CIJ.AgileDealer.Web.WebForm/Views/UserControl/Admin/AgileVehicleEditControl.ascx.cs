﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using AgileDealer.Data.Entities.Incentives;
using CJI.AgileDealer.Web.WebForm.Models;


namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class AgileVehicleEditControl : BaseUserControl
    {
        public string Vin { get; set; }
        public int IncentiveDisplayTypeId { get; set; }
        public int ModelTranslatedId { get; set; }
        public bool IsNew { get; set; }
        public string VehicleImage { get; set; }
        public bool IsDisplayOfferDetail { get; set; }

        public bool IsUpfitted { get; set; }
        public bool InTransit { get; set; }

        public int TotalVehicleImages { get; set; }
        //public decimal? UpfittedCost { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            VinHdf.Value = Page.RouteData.Values["Vin"] as string;
            if (!IsPostBack)
            {
                if (VinHdf.Value != null)
                {
                    try
                    {
                        VehicleData AgileVehicle = VehicleHelper.GetVehicleByVin(VinHdf.Value);

                        AgileVehicleId.Value = AgileVehicle.AgileVehicleId.ToString(); // VehicleHelper.GetAgileVehicleByVin(VinHdf.Value, DealerID).AgileVehicleId.ToString();
                                                                                       //  var AgileVehicle = VehicleHelper.GetAgileVehicleByVin(VinHdf.Value, DealerID);
                        AgileVehicleId.Value = AgileVehicle.AgileVehicleId.ToString();

                        if (AgileVehicle.IsCert)
                        {
                            lblMSRPTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Label;
                            lblMSRPOverrideTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Override_Label;
                        }
                        else if (AgileVehicle.IsNew)
                        {
                            lblMSRPTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Label;
                            lblMSRPOverrideTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Override_Label;
                            //     rblCompanySpecialVehicleTypeNew.Checked = true;
                        }
                        else
                        {
                            lblMSRPTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Label;
                            lblMSRPOverrideTitle.InnerText = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Override_Label;
                        }


                        Vin = AgileVehicle.Vin.ToUpper();
                        //List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds();
                        //VehicleHelper.SortOutProgramsForSearch(AgileVehicle.NormalOffers, AgileVehicle.ConditionalOffers,
                        //    AgileVehicle.AprOffers, translateds);
                        AgileVehicleId.Value = AgileVehicle.AgileVehicleId.ToString();
                        ModelTranslatedId = AgileVehicle.ModelTranslatedId;
                        IsNew = AgileVehicle.IsNew;

                        //UpfittedCost = vehicle.UpfittedCost;
                        if (AgileVehicle.IsCert)
                        {
                            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Label;
                        }
                        if (IsNew)
                        {
                            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Label;
                        }

                        else if (!AgileVehicle.IsNew)
                        {
                            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Label;
                        }

                        BindData(AgileVehicle);

                        GetVehicleIncentives();
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = ex.Message;
                    }
                }
            }
        }

        private void BindData(VehicleData vehicle)
        {
            string trim = vehicle.Trim == "Other" ? "" : vehicle.Trim;
            //lblVehicle. = vehicle.Year + " " + vehicle.Model + " " + trim;

            if (vehicle.MSRPOverride > 0)
            {
                if (vehicle.MSRPOverride + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
                    lblMSRP.Text = $"{vehicle.OriginalSellingPrice - vehicle.UpfittedCost:C}";
                else
                    lblMSRP.Text = (vehicle.MSRPOverride > 0 ? $"{vehicle.MSRPOverride:C}" : "$0");
            }
            else
            {
                if (vehicle.MSRP + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
                    lblMSRP.Text = $"{vehicle.OriginalSellingPrice - vehicle.UpfittedCost:C}";
                else
                    lblMSRP.Text = (vehicle.MSRP > 0 ? $"{vehicle.MSRP:C}" : "$0");
            }

            lblPrice.Text = vehicle.SellingPrice > 0 ? $"{vehicle.SellingPrice:C}" : "$0";

            IsUpfitted = vehicle.IsUpfitted;
            hdnIsShowUpfittedMSRP.Value = "0";

            if (IsUpfitted)
            {
                lblUpfittedCost.Text = $"+ {vehicle.UpfittedCost:C}";
                lblUpfittedCost.Attributes.Add("modelId", ModelTranslatedId.ToString());
                lblUpfittedCost.Attributes.Add("vin", vehicle.Vin);

                //if (vehicle.MSRP + vehicle.UpfittedCost > vehicle.SellingPrice)
                hdnIsShowUpfittedMSRP.Value = "1";
            }

            lblUpfittedCostTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Upfitting_Label;

            if (vehicle.EmployeePrice > 0)
            {
                employeePriceSection.Visible = true;
                lblEmployeePrice.Text = $"{vehicle.EmployeePrice:C}";
                vehicle.OriginalSellingPrice = Convert.ToDecimal(vehicle.EmployeePrice);
            }

            lblNormalOfferPrice.Text = vehicle.NormalSellingPrice > 0 ? $"{vehicle.NormalSellingPrice:C}" : "$0";
            lblConditionalPrice.Text = vehicle.ConditionalSellingPrice > 0 ? $"{vehicle.ConditionalSellingPrice:C}" : "$0";
            conditionalSection.Visible = vehicle.ConditionalOffers != null && vehicle.ConditionalOffers.Any();





            if (vehicle.NormalOffers != null && vehicle.NormalOffers.Any())
            {
                var normalOffers = vehicle.NormalOffers.Select(x => new
                {
                    Name = x.Name,
                    DoubleAmount = $"- {x.DoubleAmount:C}",
                    ProgramId = x.ProgramId.ToString(),
                    Amount = x.Amount,
                    Type = x.Type,
                    Disclaimer = (x.IsCashCoupon ? x.Disclaimer.Replace("'", "") : "Program #" + x.Id + ":" + x.Disclaimer.Replace("'", "")) + "  See dealer for complete details.",
                    IsCashCoupon = x.IsCashCoupon.ToString().ToLower()

                });
                rptNormalOffer.DataSource = normalOffers;
                rptNormalOffer.DataBind();
                rptNormalOffer.Visible = true;
                hdnIsShowUpfittedMSRP.Value = "1";
            }
            else
                rptNormalOffer.Visible = false;

            if (vehicle.ConditionalOffers != null && vehicle.ConditionalOffers.Any())
            {
                var conditionalOffers = vehicle.ConditionalOffers.Select(x => new
                {
                    Name = x.Name,
                    DoubleAmount = $"- {x.DoubleAmount:C}",
                    ProgramId = x.ProgramId.ToString(),
                    Amount = x.Amount,
                    Type = x.Type,
                    Disclaimer = (x.IsCashCoupon ? x.Disclaimer.Replace("'", "") : "Program #" + x.Id + ":" + x.Disclaimer.Replace("'", "")) + "  See dealer for complete details.",
                    IsCashCoupon = x.IsCashCoupon.ToString().ToLower()

                });
                rptConditionalOffer.DataSource = conditionalOffers;
                rptConditionalOffer.DataBind();

                conditionalOfferSection.Visible = true;
            }
            else
                conditionalOfferSection.Visible = false;

            //if (vehicle.MSRPOverride > 0)
            //    vehicle.DealerSaving = (vehicle.MSRPOverride > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRPOverride > vehicle.OriginalSellingPrice) ? (vehicle.MSRPOverride - vehicle.OriginalSellingPrice) : 0;
            //else
            //    vehicle.DealerSaving = (vehicle.MSRP > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRP > vehicle.OriginalSellingPrice) ? (vehicle.MSRP - vehicle.OriginalSellingPrice) : 0;

            lblDealerSave.Text = vehicle.DealerSaving > 0 ? $"- {vehicle.DealerSaving:C}" : "$0";

            //if (vehicle.IsOverridePrice)
            //{
            //    lblNormalOfferPrice.Text = vehicle.OriginalSellingPrice > 0 ? $"{vehicle.OriginalSellingPrice:C}" : "$0";

            //    lblPrice.Text = vehicle.OriginalSellingPrice > 0 ? $"{vehicle.OriginalSellingPrice:C}" : "$0";

            //    conditionalSection.Visible = false;
            //    rptNormalOffer.Visible = false;
            //    rptConditionalOffer.Visible = false;
            //}

            if (vehicle.MSRP + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
            {
                //  lblMSRP.Visible = false;
                if (vehicle.MSRPOverride < vehicle.OriginalSellingPrice)
                {
                    lblDealerSave.Text = "$0";
                }
                if (vehicle.IsCert)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Override_Label;
                }
                if (IsNew)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Asking_Price_Label;
                }

                else if (!vehicle.IsNew)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Override_Label;
                }
            }

            if (vehicle.MSRP <= 0 && vehicle.OriginalSellingPrice <= 0 && vehicle.MSRPOverride <= 0 && vehicle.SellingPrice <= 0)
                IsDisplayOfferDetail = false;
            else
                IsDisplayOfferDetail = true;

            // btnSticker.PostBackUrl = $"http://www.windowsticker.forddirect.com/windowsticker.pdf?vin={vehicle.Vin}";
            // btnSticker.Visible = !vehicle.IsNew;
            //   lblRebate.Text = $"- {Math.Abs(vehicle.Rebate):C}";
        }

        protected void grdvwVehiclePrograms_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //InActive
                if (((Label)e.Row.FindControl("lblActive")).Text == "0")
                    e.Row.CssClass = "alert-danger";

                if (((Label)e.Row.FindControl("lblIncentiveTranslatedId")).Text != "0")
                    e.Row.CssClass = "alert-success";

                if (((Label)e.Row.FindControl("lblDiscountType")).Text == "2")
                    e.Row.Cells[4].Text = e.Row.Cells[4].Text + "%";
                else
                    e.Row.Cells[4].Text = "$" + e.Row.Cells[4].Text;

            }
        }

        //protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
        //{
        //    lblMessage.Text = "";
        //    try
        //    {
        //        GridViewRow dataRow = (GridViewRow) ((CheckBox)sender).Parent.Parent;
        //        VehicleIncentivesModel vehicleIncentivesModel = new VehicleIncentivesModel()
        //        {
        //            Active = ((CheckBox)sender).Checked ? 1 : 0,
        //            AgileVehicleId = Convert.ToInt32(AgileVehicleId.Value),
        //            IncentivetranslatedId = Convert.ToInt32(((Label)dataRow.FindControl("lblIncentiveTranslatedId")).Text),
        //            Vin= VinHdf.Value,
        //            Id=dataRow.Cells[0].Text,
        //            DealerId=DealerID,
        //            ProgramID = Convert.ToInt32(((Label)dataRow.FindControl("lblProgramId")).Text),

        //        };
        //        VehicleHelper.UpdateVehicleIncentive(vehicleIncentivesModel);

        //        VehicleData AgileVehicle = VehicleHelper.GetVehicleByVin(VinHdf.Value);
        //        InTransit = AgileVehicle.InTransit;

        //        Vin = AgileVehicle.Vin.ToUpper();
        //        List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds();
        //        VehicleHelper.SortOutProgramsForSearch(AgileVehicle.NormalOffers, AgileVehicle.ConditionalOffers,
        //            AgileVehicle.AprOffers, translateds);
        //        AgileVehicleId.Value = AgileVehicle.AgileVehicleId.ToString();
        //        ModelTranslatedId = AgileVehicle.ModelTranslatedId;
        //        IsNew = AgileVehicle.IsNew;

        //        //UpfittedCost = vehicle.UpfittedCost;
        //        if (AgileVehicle.IsCert)
        //        {
        //            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Label;
        //        }
        //        if (IsNew)
        //        {
        //            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Label;
        //        }

        //        else if (!AgileVehicle.IsNew)
        //        {
        //            lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Label;
        //        }

        //        BindData(AgileVehicle);
        //        GetVehicleIncentives();
        //        lblMessage.Text = "Record updated successfully.";
        //        ScriptManager.RegisterStartupScript(upnlVehicleIncentives, this.GetType(), "HideLoader", "HideLoader();", true);
        //    }
        //    catch(Exception ex) {
        //        lblMessage.Text = ex.Message;
        //        ScriptManager.RegisterStartupScript(upnlVehicleIncentives, this.GetType(), "HideLoader", "HideLoader();", true);
        //    }

        //}

        private void GetVehicleIncentives()
        {
            List<VehicleIncentivesModel> lstVehicleIncentives = VehicleHelper.GetVehicleProgramDatasByVehicelId(Convert.ToInt32(AgileVehicleId.Value), DealerID);
            grdvwVehiclePrograms.DataSource = lstVehicleIncentives.Where(x => x.IncentivetranslatedId == 0);
            grdvwVehiclePrograms.DataBind();

            grdvwVehicleCustomIncentives.DataSource = lstVehicleIncentives.Where(x => x.IncentivetranslatedId != 0);
            grdvwVehicleCustomIncentives.DataBind();
        }

        protected void btnSaveIncentiveChanges_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow dataRow in grdvwVehiclePrograms.Rows)
                {
                    if ((((Label)dataRow.FindControl("lblActive")).Text == "1" && !((CheckBox)dataRow.FindControl("chkIsActive")).Checked) || (((Label)dataRow.FindControl("lblActive")).Text == "0" && ((CheckBox)dataRow.FindControl("chkIsActive")).Checked))
                    {
                        VehicleIncentivesModel vehicleIncentivesModel = new VehicleIncentivesModel()
                        {
                            Active = ((CheckBox)dataRow.FindControl("chkIsActive")).Checked ? 1 : 0,
                            AgileVehicleId = Convert.ToInt32(AgileVehicleId.Value),
                            IncentivetranslatedId = Convert.ToInt32(((Label)dataRow.FindControl("lblIncentiveTranslatedId")).Text),
                            Vin = VinHdf.Value,
                            Id = dataRow.Cells[0].Text,
                            DealerId = DealerID,
                            ProgramID = Convert.ToInt32(((Label)dataRow.FindControl("lblProgramId")).Text),

                        };
                        VehicleHelper.UpdateVehicleIncentive(vehicleIncentivesModel);
                    }
                }

                foreach (GridViewRow dataRow in grdvwVehicleCustomIncentives.Rows)
                {
                    VehicleIncentivesModel vehicleIncentivesModel = new VehicleIncentivesModel()
                    {
                        Active = ((CheckBox)dataRow.FindControl("chkIsActive")).Checked ? 1 : 0,
                        AgileVehicleId = Convert.ToInt32(AgileVehicleId.Value),
                        IncentivetranslatedId = Convert.ToInt32(((Label)dataRow.FindControl("lblIncentiveTranslatedId")).Text),
                        Vin = VinHdf.Value,
                        Id = dataRow.Cells[0].Text,
                        DealerId = DealerID,
                        ProgramID = Convert.ToInt32(((Label)dataRow.FindControl("lblProgramId")).Text),

                    };
                    VehicleHelper.UpdateVehicleIncentive(vehicleIncentivesModel);
                }


                VehicleData AgileVehicle = VehicleHelper.GetVehicleByVin(VinHdf.Value);

                Vin = AgileVehicle.Vin.ToUpper();
                //List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds();
                //VehicleHelper.SortOutProgramsForSearch(AgileVehicle.NormalOffers, AgileVehicle.ConditionalOffers,
                //    AgileVehicle.AprOffers, translateds);
                AgileVehicleId.Value = AgileVehicle.AgileVehicleId.ToString();
                ModelTranslatedId = AgileVehicle.ModelTranslatedId;
                IsNew = AgileVehicle.IsNew;

                //UpfittedCost = vehicle.UpfittedCost;
                if (AgileVehicle.IsCert)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Label;
                }
                if (IsNew)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Label;
                }

                else if (!AgileVehicle.IsNew)
                {
                    lblMSRPSectionTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Label;
                }

                BindData(AgileVehicle);
                GetVehicleIncentives();
                lblMessage.Text = "Record updated successfully.";
                //   ScriptManager.RegisterStartupScript(upnlVehicleIncentives, this.GetType(), "HideLoader", "HideLoader();", true);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // ScriptManager.RegisterStartupScript(upnlVehicleIncentives, this.GetType(), "HideLoader", "HideLoader();", true);
            }

        }

        protected void grdvwVehicleCustomIncentives_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //InActive
                if (((Label)e.Row.FindControl("lblActive")).Text == "0")
                    e.Row.CssClass = "alert-danger";

                if (((Label)e.Row.FindControl("lblIncentiveTranslatedId")).Text != "0")
                    e.Row.CssClass = "alert-success";

                if (((Label)e.Row.FindControl("lblDiscountType")).Text == "2")
                    e.Row.Cells[4].Text = e.Row.Cells[4].Text + "%";
                else
                    e.Row.Cells[4].Text = "$" + e.Row.Cells[4].Text;

            }
        }
    }
}
