﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class VehicleBulkUpdateDashboardControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblTotalRecords.Text = "";
            lblError.Text = "";
            if (!IsPostBack)
            {


                ddlyear.DataSource = MasterDataHelper.GetYearList().OrderByDescending(x=>Convert.ToInt32(x.Name));
                ddlMake.DataSource = MasterDataHelper.GetMakes().OrderBy(x => x.Name);
                ddlModel.DataSource = MasterDataHelper.GetModels(1).OrderBy(x => x.Name);
                //ddlTrims.DataSource=MasterDataHelper.Getmo

                ddlyear.DataBind();
                ddlModel.DataBind();
                ddlMake.DataBind();

                ddlyear.Items.Insert(0, new ListItem("Select Year", ""));
                ddlMake.Items.Insert(0, new ListItem("Select Make", ""));
                ddlModel.Items.Insert(0, new ListItem("Select Model", ""));

                ViewState["sortdr"] = "asc";

                BindBulkUploadVehicleFeatures();
                
            }

        }
        private void BindBulkUploadVehicleFeatures(string SortExpression="Year")
        {
            if (Convert.ToString(ViewState["sortdr"]) == "asc")
                ViewState["sortdr"] = "desc";
            else
                ViewState["sortdr"] = "asc";

            var BulkUpdateVehicles = VehicleHelper.GetBulkUpdateVehicleData(ddlyear.SelectedValue, ddlMake.SelectedValue, ddlModel.SelectedValue, SortExpression, Convert.ToString(ViewState["sortdr"]));
            grdvwVehicleBulkUpdateDashboard.DataSource = BulkUpdateVehicles;
            grdvwVehicleBulkUpdateDashboard.DataBind();

            lblTotalRecords.Text = "Total Rows: " + grdvwVehicleBulkUpdateDashboard.Rows.Count.ToString();
        }

        protected async void btnSaveDetails_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.Visible = false;
            try
            {
                foreach (GridViewRow item in grdvwVehicleBulkUpdateDashboard.Rows)
                {
                    try
                    {
                        if (((HiddenField)item.FindControl("hdnPriceOverride")).Value != ((TextBox)item.FindControl("txtPriceOverride")).Text || ((HiddenField)item.FindControl("hdnMSRPOverridePrice")).Value != ((TextBox)item.FindControl("txtMSRPOverridePrice")).Text || ((HiddenField)item.FindControl("hdnEmployeePrice")).Value != ((TextBox)item.FindControl("txtEmployeePrice")).Text)
                        {
                            var agileVehicle = new AgileVehicle()
                            {
                                AgileVehicleId = Convert.ToInt32(grdvwVehicleBulkUpdateDashboard.DataKeys[item.RowIndex].Value),
                                OverridePrice = string.IsNullOrEmpty(((TextBox)item.FindControl("txtPriceOverride")).Text) ? Convert.ToDecimal(0) : Convert.ToDecimal(((TextBox)item.FindControl("txtPriceOverride")).Text.Replace(",", "")),
                                MSRPOverride = string.IsNullOrEmpty(((TextBox)item.FindControl("txtMSRPOverridePrice")).Text) ? Convert.ToDecimal(0) : Convert.ToDecimal(((TextBox)item.FindControl("txtMSRPOverridePrice")).Text.Replace(",", "")),
                                EmployeePrice = string.IsNullOrEmpty(((TextBox)item.FindControl("txtEmployeePrice")).Text) ? Convert.ToDecimal(0) : Convert.ToDecimal(((TextBox)item.FindControl("txtEmployeePrice")).Text.Replace(",", ""))
                            };
                            await VehicleHelper.UpdateBulkVehicleData(agileVehicle);
                        }
                    }
                    catch (Exception ex)
                    { }

                }
                lblError.Text = "Records updated successfully.";
                lblError.Visible = true;
            }

            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindBulkUploadVehicleFeatures();
        }

        protected async void btnResetPriceOverride_Click(object sender, EventArgs e)
        {
           
            foreach (GridViewRow item in grdvwVehicleBulkUpdateDashboard.Rows)
            {
                if (((TextBox)item.FindControl("txtPriceOverride")).Text!="0.00" || ((TextBox)item.FindControl("txtMSRPOverridePrice")).Text!="0.00")
                {
                    await VehicleHelper.ResetPriceOverride(Convert.ToInt32(grdvwVehicleBulkUpdateDashboard.DataKeys[item.RowIndex].Value), Page.User.Identity.Name);
                }
            }
           
            BindBulkUploadVehicleFeatures();
            lblError.Text = "Records updated successfully.";
            lblError.Visible = true;
        }

        protected void grdvwVehicleBulkUpdateDashboard_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //If Salary is less than 10000 than set the row Background Color to Cyan  
                if (!string.IsNullOrEmpty(((Label)e.Row.FindControl("lblOriginalSellingPrice")).Text))
                {
                    if (Convert.ToDecimal(((Label)e.Row.FindControl("lblMSRP")).Text) > 0 && Convert.ToDecimal(((Label)e.Row.FindControl("lblOriginalSellingPrice")).Text) > 0)
                    {
                        ((Label)e.Row.FindControl("lblDealerSaving")).Text = string.Format("{0:C}", Convert.ToDecimal(((Label)e.Row.FindControl("lblMSRP")).Text) - Convert.ToDecimal(((Label)e.Row.FindControl("lblOriginalSellingPrice")).Text));
                    }
                }
           }
        }

        protected void grdvwVehicleBulkUpdateDashboard_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (e.SortExpression != "OriginalSellingPrice")
            {
                BindBulkUploadVehicleFeatures(e.SortExpression);
            }
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            ddlyear.SelectedValue = "";
            ddlMake.SelectedValue = "";
            ddlModel.SelectedValue = "";
            BindBulkUploadVehicleFeatures();
        }

        protected void grdvwVehicleBulkUpdateDashboard_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Sort") && e.CommandArgument.ToString()== "OriginalSellingPrice")
            {
                BindBulkUploadVehicleFeatures(e.CommandArgument.ToString());
            }
            
        }
    }
}