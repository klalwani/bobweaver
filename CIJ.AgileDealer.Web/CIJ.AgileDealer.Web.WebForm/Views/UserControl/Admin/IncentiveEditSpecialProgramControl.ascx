﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveEditSpecialProgramControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveEditSpecialProgramControl" %>

<script src="../../Scripts/Pages/AgileDealer.IncentiveTranslatedSpecialProgramUpdate.js"></script>
<div id="incentiveEditSpecialProgramControl" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="lblProgramNameSpecialProgram"></h4>
            </div>
        </div>

        <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div class="container-fluid">
                <div class="offer-cards row-fluid">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Display Section</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control" id="ddlDisplaySectionSpecialProgram">
                                <option value="">Select Position</option>
                                <option value="1">Manufacturer</option>
                                <option value="2">Conditional</option>
                                <option value="3">Finance</option>
                                <option value="4">Lease</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row hide">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Status</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 btn-group multiButtonDefault" id="c" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="chkStatusSpecialProgram" value="Active" autocomplete="off" checked>Active
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="chkStatusSpecialProgram" value="Hidden" autocomplete="off">Hidden
                            </label>
                        </div>
                    </div>
                    <div class="form-group row hide">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Position</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtPositionSpecialProgram" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Translated Name</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtTranslatedNameSpecialProgram" />
                        </div>
                    </div>
                     <div class="form-group row">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">&nbsp;</label>
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left">
                        <input type="checkbox" id="chkIsMasterIncentiveSpecialProgram" name="chkIsMasterIncentiveSpecialProgram" onclick="return HideShowExcludeOptionsSpecialPrograms();" />
                        Master Incentive
                    </div>
                </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Update For</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 btn-group multiButtonDefault" id="updateForSpecialProgram" data-toggle="buttons">
                            <label class="btn btn-default active updateForAllVehicle isMasterSpecialProgramsTab">
                                <input type="radio" name="chkUpdateForSpecialProgram" value="1" data-target="all" checked />All Vehicles
                            </label>
                            <label class="btn btn-default updateForVin isMasterSpecialProgramsTab">
                                <input type="radio" name="chkUpdateForSpecialProgram" value="2" data-target="vinSection" />Vin
                            </label>
                            <label class="btn btn-default updateForModel isMasterSpecialProgramsTab">
                                <input type="radio" name="chkUpdateForSpecialProgram" value="3" data-target="modelSection" />Model
                            </label>
                             <label class="btn btn-default updateForMasterIncentiveSpecialPrograms " disabled="disabled">
                                <input type="radio" name="chkUpdateForSpecialProgram" value="4" />Master
                            </label>
                        </div>
                    </div>

                    <div class="form-group row collapse isMasterSpecialPrograms" id="vinSectionSpecialProgram">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Vin</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtVinSpecialProgram" />
                        </div>
                    </div>

                    <div class="form-group row collapse isMasterSpecialPrograms" id="modelSectionSpecialProgram">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Models</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlModelSpecialProgram">
                                <option value="0">Select Model</option>
                            </select>
                        </div>
                        <div class="clear-20">&nbsp;</div>
                         <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Year</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlModelSpecialProgramYear">
                                <option value="0">Years</option>
                            </select>
                        </div>
                          <div class="clear-20">&nbsp;</div>
                         <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">StyleID</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlModelSpecialProgramStyles">
                                <option value="0">Styles</option>
                            </select>
                        </div>
                      
                       
                    </div>
                </div>
                <div class="form-group row isMasterSpecialPrograms" id="excludeYearsSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Exclude Year(s)</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <select class="form-control 4col active" id="ddlExcludeYearsSpecialProgram" multiple="multiple">
                            <option value="0">Select Year(s)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row isMasterSpecialPrograms" id="excludeTrimsSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Exclude Trim(s)</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <select class="form-control 4col active" id="ddlExcludeTrimsSpecialProgram" multiple="multiple">
                            <option value="0">Select Trim(s)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row isMasterSpecialPrograms" id="engineSectionSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Exclude Engine(s)</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <select class="form-control 4col active" id="ddlEnginesSpecialProgram" multiple="multiple">
                            <option value="-1">Select Engine(s)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row isMasterSpecialPrograms" id="excludeVinSectionSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Exclude Vin(s)</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <select class="form-control 4col active" id="ddlVinSpecialProgram" multiple="multiple">
                            <option value="0">Select Vin(s)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row isMasterSpecialPrograms" id="excludeBodyStyleSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Exclude BodyStyle(s)</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <select class="form-control 4col active" id="ddlExcludeBodyStyleSpecialProgram" multiple="multiple">
                            <option value="0">Select BodyStyle(s)</option>
                        </select>
                    </div>
                </div>
               
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">&nbsp;</label>
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left">
                        <input type="checkbox" id="chkHideAllOtherIncentivesSpecialProgram" name="chkHideAllOtherIncentivesSpecialProgram" />
                        Hide Other Incentives
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">&nbsp;</label>
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left">
                        <input type="checkbox" id="chkHideDealerDiscountsSpecialProgram" name="chkHideDealerDiscountsSpecialProgram" />
                        Hide Dealer Discounts
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Discount Type</label>
                    </div>
                    <div class="col-xs-12 col-sm-8 text-left">
                        <select class="form-control 4col active" id="ddlDiscountTypeSpecialProgram">
                            <option value="2" selected="selected">% of MSRP</option>
                            <option value="1">Flat Amount</option>


                        </select>
                    </div>
                </div>
                <div class="form-group row " id="divCashCouponPercentageSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Percentage (%)</label>
                    </div>
                    <div class="input-group col-xs-12 col-sm-8">
                        <input type="text" class="form-control price" id="txtCashCouponPercentageSpecialProgram" />
                    </div>
                </div>
                <div class="form-group row hide" id="divCashCouponAmountSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Amount</label>
                    </div>
                    <div class="input-group col-xs-12 col-sm-8">
                        <div class="input-group-addon">$</div>
                        <input type="text" class="form-control price" id="txtCashCouponAmountSpecialProgram" />

                    </div>
                </div>
                <div class="form-group row">
                    <label for="priceDate" class="col-sm-4 col-xs-12 control-label">Override Expiration</label>
                    <div class="input-group col-sm-8 col-xs-12 text-left">
                        <div class="btn-group" data-toggle="buttons" id="priceDateSpecialPrograms">
                            <label class="btn btn-default active" data-toggle="out" href="#dateRangeFieldSpecialPrograms" name="ultilSold">
                                <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                Until Sold
                            </label>
                            <label class="btn btn-default" data-toggle="in" href="#dateRangeFieldSpecialPrograms" name="setDate">
                                <input type="radio" name="options" id="option2" autocomplete="off">
                                Set Dates
                            </label>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="dateRangeFieldSpecialPrograms">
                    <div class="form-group row ">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Start Date</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">

                            <input type="text" class="form-control" id="txtDateStart">
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">End Date</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">

                            <input type="text" class="form-control" id="txtDateEnd">
                        </div>
                    </div>
                </div>
                <div class="form-group row " id="DisclaimerSpecialProgram">
                    <div class="col-xs-12 col-sm-4">
                        <label class="label-group marginTop5">Disclaimer</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">

                        <textarea class="form-control" id="txtDisclaimerSpecialProgram" rows="4" cols="50"></textarea>

                    </div>
                </div>


            </div>

        </div>
        <div class="modal-footer clear">
            <div class="col-xs-12">
                <div class="text-left col-xs-4">
                    <button type="button" class="btn btn-danger" id="btnResetSpecialProgram">Delete</button>
                </div>

                <div class="text-right col-xs-8">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveChangeSpecialProgram">Save Changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseSpecialProgram">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>

    <div id="modal-confirm-resetSpecialProgram" class="modal fade" role="dialog" tabindex='-1'>
        <div class="modal-dialog modal-xs" style="border: 1px solid; border-color: #212020; top: 50px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Warning!</h4>
                </div>
            </div>

            <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
                <div class="container-fluid">
                    <div class="offer-cards row-fluid">
                        <p>You are about to permanently delete this Custom Incentive. This cannot be undone, though you can Add a new incentive again.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer clear">
                <button type="button" class="btn btn-danger" id="btnConfirmResetSpecialProgram">Delete</button>
                <button type="button" class="btn btn-default" id="btnCancelResetSpecialProgram">Cancel</button>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="HdfIsEditForVinSpecialProgram" />
</div>

<style>
    #ui-datepicker-div {
        z-index: 100000 !important;
    }
</style>

<script>

    $(document).ready(function () {
        $('#txtCashCouponAmountSpecialProgram').on("blur", function () {
            $('#txtCashCouponAmountSpecialProgram').val(CommaFormatted($('#txtCashCouponAmountSpecialProgram').val()));
        });


    });
    $("#ui-datepicker-div").css("z-index", "10000 !important");

    $("#txtCashCouponPercentageSpecialProgram").on("blur", function () {
        if ($(this).val() > 100) {

            alert('Please enter correct % of MSRP');
            $("#txtCashCouponPercentageSpecialProgram").val("");
        }
        if (isNaN($(this).val())) {
            alert('Please enter correct % of MSRP');
            $("#txtCashCouponPercentageSpecialProgram").val("");
        }

    });


    $("#ddlDiscountTypeSpecialProgram").on("change", function () {

        if ($(this).val() == "1") {
            $("#divCashCouponAmountSpecialProgram").removeClass("hide");
            $("#divCashCouponPercentageSpecialProgram").addClass("hide");
        }
        else {
            $("#divCashCouponAmountSpecialProgram").addClass("hide");
            $("#divCashCouponPercentageSpecialProgram").removeClass("hide");

        }
    });
    function HideShowExcludeOptionsSpecialPrograms() {

        if ($("#chkIsMasterIncentiveSpecialProgram").is(":checked")) {
            $(".isMasterSpecialPrograms").addClass("hide");
            $(".isMasterSpecialProgramsTab").attr("disabled", "disabled");
            $(".updateForMasterIncentiveSpecialPrograms").removeAttr("disabled");
            $('#updateForSpecialProgram label input[value="4"]').click();
        }
        else {
            $(".isMasterSpecialPrograms").removeClass("hide");
            $(".isMasterSpecialProgramsTab").removeAttr("disabled");
            $('#updateForSpecialProgram label input[value="1"]').click();
            $(".updateForMasterIncentiveSpecialPrograms").attr("disabled", "disabled");
        }

    }


</script>
