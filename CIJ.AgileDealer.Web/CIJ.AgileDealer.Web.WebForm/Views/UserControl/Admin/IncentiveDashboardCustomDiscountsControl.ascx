﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveDashboardCustomDiscountsControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveDashboardCustomDiscountsControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveEditCustomDiscountsControl.ascx" TagPrefix="IncentiveEditCustomDiscountsControl" TagName="IncentiveEditCustomDiscountsControl" %>


<script src="../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.IncentiveManagementCustomDiscounts.js?111"></script>

<div id="incentiveManagementDashboardCustomDiscounts" >
    
        <div class="container-fluid">
             <div class="row form-title">
                <span>Custom Discounts Dashboard</span>
                 <button type="button" class="btn btn-primary" id="btnAddNewCustomDiscountsIncentive">Add New</button>
            </div>
            <div class="clear-20"></div>
        
            <table id="tableIncentiveManagementCustomDiscounts" class="table table-hover hover row-border cell-border display stripe" style="width: 100% !important;" data-page-length='10'>
                 <thead>
                    <tr>
                        <th>Section</th>
                        <th>Translated Name</th>
                        <th>Program Name</th>
                        <th>Type</th>
                        <th>Amount / Percentage</th>
                        <th>Discount Type</th>
                        <th>Exclude Engine Codes</th>
                         <th>Exclude VIN(s)</th>
                         <th>Start Date</th>
                         <th>End Date</th>
                        <th></th>
                        <th></th>

                        
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Section</th>
                        <th>Translated Name</th>
                        <th>Program Name</th>
                        <th>Type</th>
                        <th>Amount / Percentage</th>
                        <th>Discount Type</th>
                        <th>Exclude Engine Codes</th>
                         <th>Exclude VIN(s)</th>
                        <th>Start Date</th>
                         <th>End Date</th>
                          <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <IncentiveEditCustomDiscountsControl:IncentiveEditCustomDiscountsControl runat="server" ID="IncentiveEditControlCustomDiscounts" />
          <asp:HiddenField runat="server" ID="ModelListCustomDiscounts" />
        <asp:HiddenField runat="server" ID="EngineListCustomDiscounts" />
        <asp:HiddenField runat="server" ID="ExcludeVinListCustomDiscounts" />
        <asp:HiddenField runat="server" ID="TrimListCustomDiscounts" />
        <asp:HiddenField runat="server" ID="YearListCustomDiscounts" />
        <asp:HiddenField runat="server" ID="BodyStyleListCustomDiscounts" />
    
</div>






