﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveEditCustomDiscountsControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveEditCustomDiscountsControl" %>

<script src="../../Scripts/Pages/AgileDealer.IncentiveTranslatedCustomDiscountsUpdate.js?111"></script>
<div id="incentiveEditCustomDiscountsControl" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="lblProgramNameCustomDiscounts"></h4>
            </div>
        </div>

        <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div class="container-fluid">
                <div class="offer-cards row-fluid">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Display Section</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control" id="ddlDisplaySectionCustomDiscounts">
                                <option value="">Select Position</option>
                                <option value="1">Manufacturer</option>
                                <option value="2">Conditional</option>
                                <option value="3">Finance</option>
                                <option value="4">Lease</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row hide">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Status</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 btn-group multiButtonDefault" id="c" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="chkStatusCustomDiscounts" value="Active" autocomplete="off" checked>Active
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="chkStatusCustomDiscounts" value="Hidden" autocomplete="off">Hidden
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Position</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtPositionCustomDiscounts" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Translated Name</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtTranslatedNameCustomDiscounts" />
                        </div>
                    </div>
                     <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">&nbsp;</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 text-left">
                            <input type="checkbox" id="chkIsMasterIncentiveCustomDiscounts" name="chkIsMasterIncentiveCustomDiscounts"  onclick="return HideShowExcludeOptionsCustomDiscounts();" />
                            Master Incentive
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Update For</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 btn-group multiButtonDefault" id="updateForCustomDiscounts" data-toggle="buttons">
                            <label class="btn btn-default active updateForAllVehicle isMasterCustomDiscountsTab">
                                <input type="radio" name="chkUpdateForCustomDiscounts" value="1" data-target="all" checked />All Vehicles
                            </label>
                            <label class="btn btn-default updateForVin isMasterCustomDiscountsTab">
                                <input type="radio" name="chkUpdateForCustomDiscounts" value="2" data-target="vinSection" />Vin
                            </label>
                            <label class="btn btn-default updateForModel isMasterCustomDiscountsTab">
                                <input type="radio" name="chkUpdateForCustomDiscounts" value="3" data-target="modelSection" />Model
                            </label>
                             <label class="btn btn-default updateForMasterIncentiveCustomDiscounts" disabled="disabled">
                                <input type="radio" name="chkUpdateForCustomDiscounts" value="4" />Master
                            </label>
                        </div>
                    </div>

                    <div class="form-group row collapse isMasterCustomDiscounts" id="vinSectionCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Vin</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtVinCustomDiscounts" />
                        </div>
                    </div>

                    <div class="form-group row collapse isMasterCustomDiscounts" id="modelSectionCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Models</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control" id="ddlModelCustomDiscounts">
                                <option value="0">Select Model</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row isMasterCustomDiscounts" id="excludeYearsCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Exclude Year(s)</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlExcludeYearsCustomDiscounts" multiple="multiple">
                                <option value="0">Select Year(s)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row isMasterCustomDiscounts" id="excludeTrimsCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Exclude Trim(s)</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlExcludeTrimsCustomDiscounts" multiple="multiple">
                                <option value="0">Select Trim(s)</option>
                            </select>
                        </div>
                    </div>
                                        <div class="form-group row isMasterCustomDiscounts" id="engineSectionCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Exclude Engine(s)</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlEnginesCustomDiscounts" multiple="multiple">
                                <option value="-1">Select Engine(s)</option>
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group row isMasterCustomDiscounts" id="excludeVinSectionCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Exclude Vin(s)</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlVinCustomDiscounts" multiple="multiple">
                                <option value="0">Select Vin(s)</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group row isMasterCustomDiscounts" id="excludeBodyStyleCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Exclude BodyStyle(s)</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control 4col active" id="ddlExcludeBodyStylesCustomDiscounts" multiple="multiple">
                                <option value="0">Select BodyStyle(s)</option>
                            </select>
                        </div>
                    </div>
                    
                     <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">&nbsp;</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 text-left">
                            <input type="checkbox" id="chkHideAllOtherIncentivesCustomDiscounts" name="chkHideAllOtherIncentivesCustomDiscounts" />
                            Hide Other Incentives
                        </div>
                    </div>
                     <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">&nbsp;</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 text-left">
                            <input type="checkbox" id="chkHideDealerDiscountsCustomDiscounts" name="chkHideDealerDiscountsCustomDiscounts" />
                            Hide Dealer Discounts
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Discount Type</label>
                        </div>
                        <div class="col-xs-12 col-sm-8 text-left">
                            <select class="form-control 4col active" id="ddlDiscountTypeCustomDiscounts">
                                <option value="2"  selected="selected">% of MSRP</option>
                                 <option value="1">Flat Amount</option>
                                
                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group row " id="divCashCouponPercentageCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Percentage (%)</label>
                        </div>
                        <div class="input-group col-xs-12 col-sm-8">
                             <input type="text" class="form-control price"  id="txtCashCouponPercentageCustomDiscounts" />
                        </div>
                    </div>
                    <div class="form-group row hide" id="divCashCouponAmountCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Amount</label>
                        </div>
                        <div class="input-group col-xs-12 col-sm-8">
                            <div class="input-group-addon">$</div>
                            <input type="text" class="form-control price"  id="txtCashCouponAmountCustomDiscounts" />

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="priceDate" class="col-sm-4 col-xs-12 control-label">Override Expiration</label>
                        <div class="input-group col-sm-8 col-xs-12 text-left">
                            <div class="btn-group" data-toggle="buttons" id="priceDate">
                                <label class="btn btn-default active" data-toggle="out" href="#dateRangeField" name="ultilSold">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                    Until Sold
                                </label>
                                <label class="btn btn-default" data-toggle="in" href="#dateRangeField" name="setDate">
                                    <input type="radio" name="options" id="option2" autocomplete="off">
                                    Set Dates
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="collapse" id="dateRangeField">
                        <div class="form-group row ">
                            <div class="col-xs-12 col-sm-4">
                                <label class="label-group marginTop5">Start Date</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">

                                <input type="text" class="form-control clsDatePicker" id="txtDateStartCustomDiscounts">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <div class="col-xs-12 col-sm-4">
                                <label class="label-group marginTop5">End Date</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">

                                <input type="text" class="form-control clsDatePicker" id="txtDateEndCustomDiscounts">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row " id="DisclaimerCustomDiscounts">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Disclaimer</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">

                            <textarea class="form-control " id="txtDisclaimerCustomDiscounts" rows="4" cols="50"></textarea>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clear">
            <div class="col-xs-12">
                <div class="text-left col-xs-4">
                    <button type="button" class="btn btn-danger" id="btnResetCustomDiscounts">Delete</button>
                </div>

                <div class="text-right col-xs-8">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveChangeCustomDiscounts">Save Changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseCustomDiscounts">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clear-20"></div>

    <div id="modal-confirm-resetCustomDiscounts" class="modal fade" role="dialog" tabindex='-1'>
        <div class="modal-dialog modal-xs" style="border: 1px solid; border-color: #212020; top: 50px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Warning!</h4>
                </div>
            </div>

            <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
                <div class="container-fluid">
                    <div class="offer-cards row-fluid">
                        <p>You are about to permanently delete this Custom Incentive. This cannot be undone, though you can Add a new incentive again.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer clear">
                <button type="button" class="btn btn-danger" id="btnConfirmResetCustomDiscounts">Delete</button>
                <button type="button" class="btn btn-default" id="btnCancelResetCustomDiscounts">Cancel</button>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="HdfIsEditForVinCustomDiscounts" />
</div>

<style>
    #ui-datepicker-div {
        z-index: 100000 !important;
    }
</style>
<script>

    $(document).ready(function () {
        $('#txtCashCouponAmountCustomDiscounts').on("blur", function () {
            $('#txtCashCouponAmountCustomDiscounts').val(CommaFormatted($('#txtCashCouponAmountCustomDiscounts').val()));
        });



    });

    $("#ui-datepicker-div").css("z-index", "10000 !important");

    $("#txtCashCouponPercentageCustomDiscounts").on("blur", function()
    {
        if ($(this).val() > 100) {

            alert('Please enter correct % of MSRP');
            $("#txtCashCouponPercentageCustomDiscounts").val("");
        }
        if (isNaN($(this).val())) {
            alert('Please enter correct % of MSRP');
            $("#txtCashCouponPercentageCustomDiscounts").val("");
        }
        
    });


    $("#ddlDiscountTypeCustomDiscounts").on("change", function () {

        if ($(this).val() == "1") {
            $("#divCashCouponAmountCustomDiscounts").removeClass("hide");
            $("#divCashCouponPercentageCustomDiscounts").addClass("hide");
        }
        else {
            $("#divCashCouponAmountCustomDiscounts").addClass("hide");
            $("#divCashCouponPercentageCustomDiscounts").removeClass("hide");

        }
    });

    function HideShowExcludeOptionsCustomDiscounts() {

        if ($("#chkIsMasterIncentiveCustomDiscounts").is(":checked")) {
            $(".isMasterCustomDiscounts").addClass("hide");
            $(".isMasterCustomDiscountsTab").attr("disabled", "disabled");
            $(".updateForMasterIncentiveCustomDiscounts").removeAttr("disabled");
            $('#updateForCustomDiscounts label input[value="4"]').click();
        }
        else {
            $(".isMasterCustomDiscounts").removeClass("hide");
            $(".isMasterCustomDiscountsTab").removeAttr("disabled");
            $('#updateForCustomDiscounts label input[value="1"]').click();
            $(".updateForMasterIncentiveCustomDiscounts").attr("disabled","disabled");
        }

    }
     
</script>
