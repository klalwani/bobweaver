﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUsControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.ContactUsControl" %>
<script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.ContactUs.js"></script>
<div id="contactUsDashboard" class="container">
    <table id="tableContactUs" class="table hover row-border cell-border display stripe" data-order='[[5, "desc" ]]' data-page-length='10' style="width: 100%;">
        <thead>
            <tr>
                <th>CustomerQuestionId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Date Submitted</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>CustomerQuestionId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Date Submitted</th>
                <th>Detail</th>
            </tr>
        </tfoot>
    </table>
</div>
