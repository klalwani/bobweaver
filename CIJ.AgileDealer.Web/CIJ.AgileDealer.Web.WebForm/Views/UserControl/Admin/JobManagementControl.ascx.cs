﻿using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class JobManagementControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel(false);
        }
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    //required to avoid the run time error "  
        //    //Control 'GridView1' of type 'Grid View' must be placed inside a form tag with runat=server."  
        //}
        private void ExportGridToExcel(bool IsNew = true)
        {
            GridView GridView1 = new GridView();
            List<InventoryExcelDataModel> lstInventoryExcelData = new List<InventoryExcelDataModel>();
            if (IsNew)
                lstInventoryExcelData = SiteInfoHelper.GetInventoryDataInExcel().Where(x => x.TYPE.Equals("new", StringComparison.OrdinalIgnoreCase)).ToList();
            else
                lstInventoryExcelData = SiteInfoHelper.GetInventoryDataInExcel();

            GridView1.DataSource = lstInventoryExcelData;
            GridView1.DataBind();
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "BobWeaver_Inventory_" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            GridView1.GridLines = GridLines.Both;
            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();

        }

        protected void btnExportNewInventory_Click(object sender, EventArgs e)
        {
            ExportGridToExcel(true);
        }
    }
}