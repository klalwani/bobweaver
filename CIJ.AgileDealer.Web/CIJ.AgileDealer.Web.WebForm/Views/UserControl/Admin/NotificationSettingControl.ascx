﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationSettingControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.NotificationSettingControl" %>
<%: Scripts.Render("~/bundles/NotificationSetting") %>

<div id="notificationSetting" class="body-content-wrapper">
    <div class="body-content-card">
        <div class="active" id="instantlyCtr">
            <div class="row form-wrapper">

                <div class="row form-title center-text">
                    <span>Email Notification Settings</span>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="row content-padded  center-text">
                        <h3>** Note: Seperate Multiple email address by semicolon **</h3>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="row form-horizontal-input-padded AllLead">
                            <div class="col-md-2">
                               
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>ADF</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Plain Text</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                        </div>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="row content-padded">
                        <div class="row form-horizontal-input-padded AllLead">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>All Leads</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtAllLeadAdf" data-val="true" rows="3" placeholder="All ADF Leads"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtAllLeadPlain" rows="3" placeholder="All Plain Text Leads"></textarea>
                                </fieldset>
                            </div>

                        </div>

                        <div class="row form-horizontal-input-padded Contact">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Contact Only</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtContactAdf" rows="3" placeholder="Contact ADF Only"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtContactPlain" rows="3" placeholder="Contact Plain Text Only"></textarea>
                                </fieldset>
                            </div>

                        </div>

                        <div class="row form-horizontal-input-padded Finance">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Finance Only</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtFinanceAdf" rows="3" placeholder="Finance ADF Only"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtFinancePlain" rows="3" placeholder="Finance Plain Text Only"></textarea>
                                </fieldset>
                            </div>

                        </div>

                        <div class="row form-horizontal-input-padded Service">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Service Only</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtServiceAdf" rows="3" placeholder="Service ADF Only"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtServicePlain" rows="3" placeholder="Service Plain Text Only"></textarea>
                                </fieldset>
                            </div>

                        </div>

                        <div class="row form-horizontal-input-padded Availability">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Vehicle Only</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtAvailabilityAdf" rows="3" placeholder="Check Availability ADF Only"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtAvailabilityPlain" rows="3" placeholder="Check Availability Plain Text Only"></textarea>
                                </fieldset>
                            </div>

                        </div>
                        
                        <div class="row form-horizontal-input-padded PartRequest">

                            <div class="col-md-2">
                                <fieldset class="form-group rowElem">
                                    <div class='square-box'>
                                        <div class='square-content'>
                                            <div><span>Parts Only</span></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtPartRequestAdf" rows="3" placeholder="Part Request ADF Only"></textarea>
                                </fieldset>
                            </div>

                            <div class="col-md-5">
                                <fieldset class="form-group rowElem">
                                    <textarea class="form-control" id="txtPartRequestPlain" rows="3" placeholder="Part Request Plain Text Only"></textarea>
                                </fieldset>
                            </div>

                        </div>

                        <div class="row form-horizontal-input-padded">
                            <div class="col-md-3">
                                <fieldset class="form-group rowElem  ">
                                    <input type="button" class="btn btn-primary" id="btnSubmit" value="Save Changes" />
                                </fieldset>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
