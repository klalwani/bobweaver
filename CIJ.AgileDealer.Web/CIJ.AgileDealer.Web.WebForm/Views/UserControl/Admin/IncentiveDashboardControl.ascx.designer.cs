﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin {
    
    
    public partial class IncentiveDashboardControl {
        
        /// <summary>
        /// IncentiveEditControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveEditControl IncentiveEditControl;
        
        /// <summary>
        /// ModelList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ModelList;
    }
}
