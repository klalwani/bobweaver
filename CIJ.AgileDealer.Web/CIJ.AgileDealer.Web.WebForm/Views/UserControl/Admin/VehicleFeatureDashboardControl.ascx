﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="VehicleFeatureDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.VehicleFeatureDashboardControl" %>
<div class="clear-20"></div>
<div id="vehicleFeatureDashboard" class="container">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Feature Management Dashboard</span>
                <asp:Button ID="btnSaveDetails" ClientIDMode="Static" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveDetails_Click" OnClientClick="ShowProgressBar();" />
                <%--<input type="button" id="btnTriggerRecon" name="btnTriggerRecon" value="Update Inventory" class="btn btn-success" />--%>
            </div>
            <asp:UpdatePanel ID="upnlVehicleFeatureDashboard" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="grdvwVehicleFeature" DataKeyNames="FeatureID" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-hover hover row-border cell-border display stripe">
                        <Columns>
                            <asp:BoundField DataField="FeatureID" Visible="false" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="FeatureName" HeaderText="Feature" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%" />
                            <asp:TemplateField ItemStyle-Width="40%">
                                <HeaderTemplate>
                                    <h3>Label Override</h3>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="form-group vfd-label-group">
                                        <asp:TextBox ID="txtLabelOverride" runat="server" Text='<%#Eval("LabelOverride") %>' CssClass="form-control input-lg" Max-Width="100%" MaxLength="200"></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="40%">
                                <HeaderTemplate>
                                    <div class="text-center">
                                        <h3 class="">Active</h3><div class="clear"></div>
                                        <label>
                                            <input type="checkbox" id="cbCheckAll" onclick="CheckUnCheckItems(this);" /> Select All
                                        </label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkIsSearchEnabled" runat="server" CssClass="cbVehicleFeature" Checked='<%#Convert.ToBoolean(Eval("IsSearchable")) %>'></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSaveDetails" EventName="Click" />
                </Triggers>
               
            </asp:UpdatePanel>

        </div>
    </div>
</div>
<script>
    function CheckUnCheckItems(obj) {
        if (document.getElementById(obj.id).checked) {
            $(".cbVehicleFeature input").prop('checked', true);
        }
        else
            $(".cbVehicleFeature input").prop('checked', false);
    }
    function ShowProgressBar()
    {
        $.blockUI(siteMaster.loadingWheel());
        window.setTimeout('HideLoadingWheetl()', 3000);
    }
    function HideLoadingWheetl()
    {
        $.unblockUI(siteMaster.loadingWheel());
    }
</script>