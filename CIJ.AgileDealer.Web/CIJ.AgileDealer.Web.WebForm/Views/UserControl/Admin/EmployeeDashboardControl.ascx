﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.EmployeeDashboardControl" %>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div id="employeeDashboard" class="container">
    <script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.Employee.js"></script>

    <div class="row form-title">
        <span>Employee Dashboard</span>

        <input type="button" id="btnAddNewEmployee" name="btnAddNewEmployee" value="Add New" class="btn btn-success"/>

    </div>
    
    <table id="tableEmployee" class="hover row-border cell-border display stripe" data-order='[[ 0, "asc" ]]' data-page-length='10'>
        <thead>
            <tr>
                <th>EmployeeId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Is New</th>
                <th>Order</th>
                <th>Is Deleted</th>
                <th>Date Created</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>EmployeeId</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Is New</th>
                <th>Order</th>
                <th>Is Deleted</th>
                <th>Date Created</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="hidden">
    <div id="dialog-confirm" title="Are you sure to delete this staff?">
        <p>
            <span class="ui-icon ui-icon-alert"></span>This staff will be permanently deleted and cannot be recovered. Are you sure?
        </p>
    </div>
</div>