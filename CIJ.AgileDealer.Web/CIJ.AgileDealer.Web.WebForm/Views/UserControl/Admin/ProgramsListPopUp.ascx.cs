﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class ProgramsListPopUp : BaseUserControl
    {
        private List<VehicleKeyPairValueData> _namePlates;
        public List<VehicleKeyPairValueData> NamePlates {
            get
            {
                if(_namePlates == null)
                    _namePlates = VehicleHelper.GetNamePlates(DealerID);
                return _namePlates;
            }
            set { _namePlates = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            GetMasterData();
        }
        private void GetMasterData()
        {
            if (NamePlates != null)
            {
                NamePlateHdf.Value = JsonHelper.Serialize(NamePlates);
            }
        }

    }
}