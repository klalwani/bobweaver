﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogEditControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.BlogEditControl" %>
<script src="../../../Scripts/ckeditor/ckeditor.js"></script>
<script src="../../../Scripts/Pages/Admin/AgileDealer.BlogEdit.js"></script>
<script>
    $(function () {
        // you may want to add an additional check to make sure that the parent is the element you expect it to be
        $('[checked="checked"]').parent().addClass('active');
    })

</script>
<div class="row form-wrapper">
    <div class="col-xs-12">
        <div class="row form-horizontal-input-padded">

            <div class="col-xs-12" id="contactInfo">
               <%-- <div class="row content-padded">

                    <div class="col-xs-2 col-sm-2 text-center">
                        <img alt="blog-image" id="blogImage" class="bcc-hero-image mdl-box-shadow" src="https://gatorford.blob.core.windows.net/images/content/NoImageFound.png">
                    </div>
                </div>--%>

                <div class="row content-padded">
                    <div class="col-xs-12 col-sm-12">
                        <div class="row form-horizontal">
                            <fieldset class="form-group rowElem">
                                <label for="postTitle" class="col-xs-2 control-label">Post Title</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="postTitle" placeholder="Post Title" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label for="blogCategoriesddl" class="col-xs-2 control-label">Categories</label>
                                <div class="col-xs-10">
                                    <select id="blogCategoriesddl" class="form-control" data-val="true" data-val-required="true" multiple="multiple" placeholder="Blog Categories" title="Blog Categories">
                                    </select>
                                </div>
                            </fieldset>
                            
                            <fieldset class="form-group">
                                <label for="blogTagsddl" class="col-xs-2 control-label">Tags</label>
                                <div class="col-xs-10">
                                    <select id="blogTagsddl" class="form-control" data-val="true" data-val-required="true" multiple="multiple" placeholder="Blog Tags" title="Blog Tags">
                                    </select>
                                </div>
                            </fieldset>

                            

                            <fieldset class="form-group">
                                <label for="postFeatureImage" class="col-xs-2 control-label">Post Feature Image</label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <label class="btn btn-primary">
                                                Browse&hellip;
                                            <input id="postFeatureImage" type="file" style="display: none;" multiple>
                                            </label>
                                        </label>
                                        <input type="text" id="postFeatureImageText" class="form-control" readonly>
                                    </div>
                                </div>
                            
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="postOverrideImageUrl" class="col-xs-2 control-label">Override Image Url</label>
                                <div class="col-xs-10">
                                    
                                        <input type="text" id="postOverrideImageUrl" class="form-control" maxlength="500">
                                     
                                </div>
                            </fieldset>
                            <fieldset class="form-group rowElem">
                                <label for="postSummary" class="col-xs-2 control-label">Post Summary</label>
                                <div class="col-xs-10">
                                    <textarea class="form-control" rows="5" data-val="true" data-val-required="true" id="postSummary" placeholder="Post Summary"></textarea>
                                </div>
                            </fieldset>
                            
                            <fieldset class="form-group rowElem">
                                <label for="postText" class="col-xs-2 control-label">Post Text</label>
                                <div class="col-xs-10">
                                    <textarea  data-val="true" data-val-required="true" name="postText" id="postText" placeholder="Post Text"></textarea>
                                </div>
                            </fieldset>

                            <fieldset class="form-group rowElem">
                                <label for="button1Text" class="col-xs-2 control-label">Button 1 Text</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="button1Text" placeholder="Button 1 Text" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group rowElem">
                                <label for="button1Path" class="col-xs-2 control-label">Button 1 Path</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="button1Path" placeholder="Button 1 Text" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group rowElem">
                                <label for="button2Text" class="col-xs-2 control-label">Button 2 Text</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="button2Text" placeholder="Button 2 Text" value="">
                                </div>
                            </fieldset>

                            <fieldset class="form-group rowElem">
                                <label for="button2Path" class="col-xs-2 control-label">Button 2 Path</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" data-val="true" data-val-required="true" id="button2Path" placeholder="Button 2 Text" value="">
                                </div>
                            </fieldset>
                            
                            <fieldset class="form-group rowElem">
                                <label for="languageddl" class="col-xs-2 control-label">Language</label>
                                <div class="col-xs-10">
                                    <select id="languageddl" class="form-control" data-val="true" data-val-required="true" title="Languages">
                                    </select>
                                </div>
                            </fieldset>
                            
                            
                            <fieldset class="form-group checkbox edit-vehicle-checkbox">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <label class="">
                                        <input type="checkbox" name="isPublish" id="isPublish" title="Is Publish">Publish
                                    </label>
                                </div>
                            </fieldset>

                            <fieldset class="form-group checkbox edit-vehicle-checkbox">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <label class="">
                                        <input type="checkbox" name="isDeleted" id="isDeleted" title="Is Deleted">Deleted
                                    </label>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </div>


                <div class=" row content-padded ">
                    <fieldset class=" col-xs-offset-2 form-group">
                        <input type="button" class="btn btn-primary" id="btnUpdateBlog" value="Save Changes" />
                        <input type="button" class="btn btn-default" id="btnCancelBlog" value="Cancel" />
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField runat="server" ID="BlogEntryId" />
<asp:HiddenField runat="server" ID="BlogCategory" />
<asp:HiddenField runat="server" ID="BlogTag" />
<asp:HiddenField runat="server" ID="Languages" />
<input type="hidden" id="tempImageName" value="" />
