﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Blog;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class BlogEditControl : BaseUserControl
    {
        private List<BlogTag> _blogTags;

        private List<BlogTag> BlogTags
        {
            get
            {
                _blogTags = MasterDataHelper.GetBlogTags(DealerID,true);
                return _blogTags;
            }
            set { _blogTags = value; }
        }

        private List<BlogCategory> _blogCategories;

        public List<BlogCategory> BlogCategories
        {
            get
            {
                _blogCategories = MasterDataHelper.GetBlogCategories(DealerID);
                return _blogCategories;
            }
            set { _blogCategories = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BlogCategory.Value = JsonHelper.Serialize(BlogCategories);
            BlogTag.Value = JsonHelper.Serialize(BlogTags);
            Languages.Value = JsonHelper.Serialize(SiteInfoHelper.Languages);
        }
    }
}