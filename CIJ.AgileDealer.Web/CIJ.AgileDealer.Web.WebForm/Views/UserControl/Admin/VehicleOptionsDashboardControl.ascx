﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="VehicleOptionsDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.VehicleOptionsDashboardControl" %>
<div class="clear-20"></div>
<div id="vehicleOptionsDashboard" class="container">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Vehicle OptionCodes Management Dashboard</span>
                <asp:Button ID="btnSaveDetails" ClientIDMode="Static" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnSaveDetails_Click" OnClientClick="ShowProgressBar();" />
                <%--<input type="button" id="btnTriggerRecon" name="btnTriggerRecon" value="Update Inventory" class="btn btn-success" />--%>
            </div>
            <asp:UpdatePanel ID="upnlvehicleOptionsDashboard" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="grdvwVehicleOptionCodes" DataKeyNames="AgilevehicleId" AutoGenerateColumns="false" AllowPaging="false" runat="server"  CssClass="table table-bordered table-hover table-striped">
                        <Columns>
                            <asp:BoundField DataField="AgilevehicleId" HeaderText="AgileVehicleId"  ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Vin"  ItemStyle-HorizontalAlign="Left" HeaderText="Vin" />
                             <asp:TemplateField HeaderText="Factory Codes">
                                <ItemTemplate>
                                    <div class="form-group vfd-label-group">
                                        <asp:HiddenField ID="hdnFactoryCodes" runat="server" value='<%#Eval("FactoryCodes") %>'></asp:HiddenField>
                                        <asp:TextBox ID="txtFactoryCodes" runat="server" Text='<%#Eval("FactoryCodes") %>' CssClass="form-control input-lg" Max-Width="100%" MaxLength="500"></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSaveDetails" EventName="Click" />
                </Triggers>
               
            </asp:UpdatePanel>

        </div>
    </div>
</div>
<script>
     
    function ShowProgressBar()
    {
        $("#MainContent_VehicleOptionsDashboardControl_lblError").html("");
        $.blockUI(siteMaster.loadingWheel());
        window.setTimeout('HideLoadingWheetl()', 1000);
    }
    function HideLoadingWheetl()
    {
        $.unblockUI(siteMaster.loadingWheel());
    }
</script>