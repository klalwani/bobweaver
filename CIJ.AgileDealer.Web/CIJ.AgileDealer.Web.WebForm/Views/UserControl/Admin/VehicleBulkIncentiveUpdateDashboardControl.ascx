﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleBulkIncentiveUpdateDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.VehicleBulkIncentiveUpdateDashboardControl" %>
<div id="vehicleFeatureDashboard" style="text-align: center; font-size: 12px;">

    <div class="row form-title">
        <span>Vehicle Bulk Incentive Update Dashboard</span>
    </div>


    <asp:UpdateProgress ID="uprogress" runat="server" AssociatedUpdatePanelID="upnlVehicleBulkUpdateDashboard">
        <ProgressTemplate>
            <div id="loading">
                <img id="loading-image" src="/Content/Images/new-spinner.gif" alt="Loading..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upnlVehicleBulkUpdateDashboard" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                Sys.Application.add_load(LoadScripts);
                function LoadScripts() {
                    $('[id$="grdvwVehicleCustomIncentives"]').find('tr').each(function (i, el) {
                        if (!$(el).hasClass("HeaderFreez")) {
                            $(this).find('td.filter').each(function (i, tdel) {
                                var inputEl = $(tdel).text().toLowerCase();
                                if (inputEl.indexOf($("#txtSearchCustomIncentives").val().toLowerCase()) > -1) {
                                    isTextExists = true;
                                }
                            });
                            if (isTextExists)
                                $(el).css("display", "");
                            else
                                $(el).css("display", "none");

                            isTextExists = false;
                        }
                    });

                    $("#txtSearchCustomIncentives").on("keyup", function () {
                        var isTextExists = false;
                        $('[id$="grdvwVehicleCustomIncentives"]').find('tr').each(function (i, el) {
                            if (!$(el).hasClass("HeaderFreez")) {
                                $(this).find('td.filter').each(function (i, tdel) {
                                    var inputEl = $(tdel).text().toLowerCase();
                                    if (inputEl.indexOf($("#txtSearchCustomIncentives").val().toLowerCase()) > -1) {
                                        isTextExists = true;
                                    }
                                });
                                if (isTextExists)
                                    $(el).css("display", "");
                                else
                                    $(el).css("display", "none");

                                isTextExists = false;
                            }
                        });

                    });

                    $('[id$="txtVin"]').on("keyup", function () {
                        var isTextExists = false;
                        var vinText = $(this).val();
                        var TotalRecords = 0;
                        $('[id$="grdvwVehicleBulkUpdateDashboard"]').find('tr').each(function (i, el) {
                            if (!$(el).hasClass("HeaderFreez")) {
                                $(this).find('td.filter').each(function (i, tdel) {
                                    var inputEl = $(tdel).text().toLowerCase();
                                    if (inputEl.indexOf(vinText.toLowerCase()) > -1) {
                                        isTextExists = true;
                                    }
                                });
                                if (isTextExists) {
                                    TotalRecords++;
                                    $(el).css("display", "");
                                }
                                else
                                    $(el).css("display", "none");

                                isTextExists = false;
                            }
                        });
                        $('[id$="lblTotalRecords"]').text("Total Rows: " + parseInt(TotalRecords));
                    });

                }
            </script>
            <div class="col-md-12 text-left" style="font-size: 18px;">
                <asp:CheckBox ID="chkIncludeUsed" runat="server" Text="&nbsp;Include Used Vehicles" AutoPostBack="true" OnCheckedChanged="chkIncludeUsed_CheckedChanged" />

            </div>
            <div style="clear: both">&nbsp;</div>
            <div class="col-md-12">
                <div class="col-md-6 text-left" style="padding: 0">
                    <label>Master Incentives/Custom Incentives/Custom Discounts</label>

                </div>
                <div style="font-size: 18px;" class="col-md-6 text-right">
                    <div class="dataTables_filter">
                        <label>Search:<asp:TextBox id="txtSearchCustomIncentives" runat="server" ClientIDMode="Static"></asp:TextBox></label>
                    </div>
                </div>


            </div>

            <div class="col-lg-12" style="height: 250px; overflow: auto;">

                <asp:GridView ID="grdvwVehicleCustomIncentives" HeaderStyle-CssClass="HeaderFreez" OnRowDataBound="grdvwVehicleCustomIncentives_RowDataBound" OnRowCommand="grdvwVehicleCustomIncentives_RowCommand" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-bordered table-hover">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <input type="checkbox" id="chkHeaderCustomIncentivesAll" onclick="return CheckUnCheckAll();" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <input type="checkbox" id="chkSelectCustomIncentive" runat="server" class="chkSelectCustomIncentive" />

                                <asp:Label ID="lblIncentiveTranslatedId" runat="server" Visible="false" Text='<%#Eval("IncentiveTranslatedId") %>'></asp:Label>
                                <asp:Label ID="lblActive" runat="server" Visible="false" Text='<%#Eval("Active") %>'></asp:Label>
                                <asp:Label ID="lblProgramId" runat="server" Visible="false" Text='<%#Eval("ProgramId") %>'></asp:Label>
                                <asp:Label ID="lblDiscountType" runat="server" Visible="false" Text='<%#Eval("DiscountType") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Id" ItemStyle-HorizontalAlign="Left" HeaderText="Program Number" ItemStyle-CssClass="filter"/>
                        <asp:BoundField DataField="Name" ItemStyle-HorizontalAlign="Left" HeaderText="Name" ItemStyle-CssClass="filter" />
                        <asp:BoundField DataField="StartDate" ItemStyle-HorizontalAlign="Left" HeaderText="Start Date" />
                        <asp:BoundField DataField="EndDate" ItemStyle-HorizontalAlign="Left" HeaderText="End Date" />
                        <asp:BoundField DataField="Amount" ItemStyle-HorizontalAlign="Left" HeaderText="Amount" />
                        <asp:BoundField DataField="Position" ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-CssClass="filter" />
                        <asp:BoundField DataField="IncentiveType" ItemStyle-HorizontalAlign="Left" HeaderText="Incentive Type" ItemStyle-CssClass="filter" />
                        <asp:BoundField DataField="Vin" ItemStyle-HorizontalAlign="Left" HeaderText="Vin" ItemStyle-CssClass="filter" />
                        <asp:TemplateField HeaderText="Standalone" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsHideOtherIncentives" runat="server" Checked='<%# Convert.ToBoolean(Eval("HideOtherIncentives")) %>' Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Master" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsMasterIncentives" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsMasterIncentive")) %>' Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Vehicles" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTotalVehicles" runat="server" Text='<%#Eval("TotalVehicles") %>' data-item-text='<%#Eval("Name") %>' OnClientClick="SetFilterValue(this);" CommandName="TotalVehicles" CommandArgument='<%#Eval("IncentiveTranslatedId") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


            </div>
            <div style="clear: both; height: 40px;"></div>
            <div class="col-lg-12 row">
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:DropDownList ID="ddlyear" runat="server" CssClass="form-control" DataTextField="Id" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:DropDownList ID="ddlMake" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="ddlMake_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="ddlModel_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:DropDownList ID="ddlTrims" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id" AutoPostBack="true" OnSelectedIndexChanged="ddlTrims_SelectedIndexChanged"></asp:DropDownList>
                </div>

            </div>
            <div style="clear: both; height: 20px;"></div>
            <div class="col-lg-12 row">
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:DropDownList ID="ddlChromeStyles" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Name" AutoPostBack="true" OnSelectedIndexChanged="ddlChromeStyles_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:TextBox ID="txtVin" runat="server" CssClass="form-control" placeholder="VIN or Stock #"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                    <asp:Button ID="btnSearch" ClientIDMode="Static" runat="server" Text="Search" CssClass="btn btn-primary hide" OnClick="btnSearch_Click" />
                    &nbsp;<asp:Button ID="btnClearSearch" ClientIDMode="Static" runat="server" Text="Clear" CssClass="btn btn-primary" OnClick="btnClearSearch_Click" />
                </div>
            </div>
            <div style="clear: both; height: 40px;"></div>
            <div class="col-md-12">
                <div class="col-lg-8 text-left">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    <div style="clear: both; height: 10px;"></div>
                    <div class="col-md-12 hide" id="divFilterContent" runat="server">
                        <asp:HiddenField ID="hdnFilteredText" runat="server" />
                        Showing vehicles for "<asp:Label ID="lblFilterItem" runat="server" Font-Bold="true"> </asp:Label>"&nbsp;<a runat="server" id="lnkRemoveFilter" onserverclick="lnkRemoveFilter_ServerClick" title="Remove Filter">Remove Filter<i class="fa fa-remove" style="color: red"></i></a>

                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-xs-12 text-right">
                    <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>

                </div>
            </div>

            <div style="clear: both; height: 20px;"></div>
            <div class="col-lg-12 text-left">
                <asp:Button ID="btnApplyIncentives_Top" runat="server" Text="Save Incentives" OnClick="btnApplyIncentives_Top_Click" OnClientClick="return ValidateCheckBoxSelected();" CssClass="btn btn-primary" Visible="false" />&nbsp;
                <%--                <asp:Button ID="btnRemoveIncentives_Top" runat="server" Text="Remove Incentives" OnClick="btnRemoveIncentives_Top_Click" OnClientClick="return ValidateCheckBoxSelected();" CssClass="btn btn-primary" Visible="false" />--%>
            </div>
            <div style="clear: both; height: 20px;"></div>
            <div class="col-lg-12" style="background-color: #fff; height: 600px; overflow: auto">

                <asp:GridView ID="grdvwVehicleBulkUpdateDashboard" AllowSorting="true" HeaderStyle-CssClass="HeaderFreez" DataKeyNames="AgileVehicleID" OnSorting="grdvwVehicleBulkUpdateDashboard_Sorting" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-bordered table-hover table-striped dataTable">
                    
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="5%">
                            <HeaderTemplate>
                                <input type="checkbox" id="chkHeaderSelectAll" onclick="return CheckUnCheckAllVehicles()" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <input type="checkbox" id="chkSelectAllVehicles" runat="server" class="chkSelectAllVehicles" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AgileVehicleID" Visible="false" ItemStyle-HorizontalAlign="Left" />
                        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Vin" SortExpression="Vin" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="filter" HeaderStyle-CssClass="sorting">
                            <ItemTemplate>
                                <a href='<%#Eval("VDPPageLink") %>' target="_blank"><%#Eval("Vin") %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-Width="8%" DataField="Stock" HeaderText="Stock #" ItemStyle-HorizontalAlign="Left" SortExpression="Stock" ItemStyle-CssClass="filter" HeaderStyle-CssClass="sorting" />
                        <asp:BoundField ItemStyle-Width="5%" DataField="Year" HeaderText="Year" ItemStyle-HorizontalAlign="Left" SortExpression="Year" HeaderStyle-CssClass="sorting_desc" />
                        <asp:BoundField ItemStyle-Width="5%" DataField="IsNew" HeaderText="New" ItemStyle-HorizontalAlign="Left" SortExpression="IsNew" HeaderStyle-CssClass="sorting" />

                        <asp:BoundField ItemStyle-Width="10%" DataField="Make" HeaderText="Make" ItemStyle-HorizontalAlign="Left" SortExpression="Make" HeaderStyle-CssClass="sorting" />
                        <asp:BoundField ItemStyle-Width="10%" DataField="Model" HeaderText="Model" ItemStyle-HorizontalAlign="Left" SortExpression="Model" HeaderStyle-CssClass="sorting" />
                        <asp:BoundField ItemStyle-Width="10%" DataField="Trim" HeaderText="Trim" ItemStyle-HorizontalAlign="Left" SortExpression="Trim" HeaderStyle-CssClass="sorting" />
                        <asp:BoundField ItemStyle-Width="10%" DataField="standardstyle" HeaderText="Style" ItemStyle-HorizontalAlign="Left" SortExpression="standardstyle" HeaderStyle-CssClass="sorting" />
                        <asp:BoundField ItemStyle-Width="10%" DataField="MSRP" HeaderText="MSRP ($)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:C0}" SortExpression="MSRP" HeaderStyle-CssClass="sorting" />
                        <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="sorting">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lnkSortOriginalSellingPrice" runat="server" Text="Selling Price ($)" CommandName="Sort" CommandArgument="OriginalSellingPrice" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblVin" runat="server" Text='<%#Eval("Vin") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblMSRP" runat="server" Text='<%#Eval("MSRP") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblOriginalSellingPrice" runat="server" Text='<%#Eval("OriginalSellingPrice") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblSellingPrice" runat="server" Text='<%#Eval("OriginalSellingPrice","{0:C0}") %>'></asp:Label><br />
                                <br />
                                <asp:Label ID="lblDealerSaving" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <a href='<%#Eval("VehicleEditLink") %>' target="_blank">Edit</a>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>
                    <EmptyDataTemplate>
                        <div class="col-md-12 text-center">
                            <asp:Label ID="lblNoDataFound" runat="server" Text="No Vehicles Found!!" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </EmptyDataTemplate>
                </asp:GridView>

            </div>
            <div class="col-lg-12 text-left">
                <asp:Button ID="btnApplyIncentives_Bottom" runat="server" Text="Save Incentives" OnClick="btnApplyIncentives_Top_Click" OnClientClick="return ValidateCheckBoxSelected();" CssClass="btn btn-primary" Visible="false" />&nbsp;
                <%--                <asp:Button ID="btnRemoveIncentives_Bottom" runat="server" Text="Remove Incentives" OnClick="btnRemoveIncentives_Top_Click" OnClientClick="return ValidateCheckBoxSelected();" CssClass="btn btn-primary" Visible="false" />--%>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnApplyIncentives_Top" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnApplyIncentives_Bottom" EventName="Click" />
            <%--            <asp:AsyncPostBackTrigger ControlID="btnRemoveIncentives_Top" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRemoveIncentives_Bottom" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="chkIncludeUsed" EventName="CheckedChanged" />

            <asp:AsyncPostBackTrigger ControlID="btnClearSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="grdvwVehicleBulkUpdateDashboard" EventName="RowCommand" />

        </Triggers>

    </asp:UpdatePanel>
    <div class="clear-20"></div>
    <div class="clear-20"></div>
</div>
<style>
    #loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        display: block;
        opacity: .9;
        background-color: #666;
        z-index: 99;
        text-align: center;
    }

    #loading-image {
        position: absolute;
        top: 50%;
        left: 48%;
        z-index: 600;
        width: 50px;
        height: 50px;
    }

    .HeaderFreez {
        position: sticky;
        z-index: 10;
        top: 0;
    }

    table.dataTable thead .sorting,
    table.dataTable th.sorting {
        background-image: url("https://cdn.datatables.net/1.10.13/images/sort_both.png");
    }

    table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc_disabled, table.dataTable thead .sorting_desc_disabled,
    table.dataTable th.sorting, table.dataTable th.sorting_asc, table.dataTable th.sorting_desc, table.dataTable th.sorting_asc_disabled, table.dataTable th.sorting_desc_disabled {
        background-repeat: no-repeat;
        background-position: center right;
    }

    table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc,
    table.dataTable th.sorting, table.dataTable th.sorting_asc, table.dataTable th.sorting_desc {
        cursor: pointer;
    }

    table.dataTable thead .sorting_asc,
    table.dataTable th.sorting_asc {
        background-image: url("https://cdn.datatables.net/1.10.13/images/sort_asc.png");
    }

    table.dataTable thead .sorting_desc,
    table.dataTable th.sorting_desc {
        background-image: url("https://cdn.datatables.net/1.10.13/images/sort_desc.png");
    }
</style>
<script>


    function CheckUnCheckAll() {
        if ($("#chkHeaderCustomIncentivesAll").is(":checked")) {
            $(".chkSelectCustomIncentive").attr("checked", "checked");
        }
        else
            $(".chkSelectCustomIncentive").removeAttr("checked");

    }
    function CheckUnCheckAllVehicles() {
        if ($("#chkHeaderSelectAll").is(":checked")) {
            $(".chkSelectAllVehicles").attr("checked", "checked");
        }
        else
            $(".chkSelectAllVehicles").removeAttr("checked");

    }

    function SetFilterValue(objLinkButton) {
        $('[id$="hdnFilteredText"]').val($(objLinkButton).attr("data-item-text"));
        $('[id$="lblFilterItem"]').text($(objLinkButton).attr("data-item-text"));
        $('[id$="divFilterContent"]').removeClass("hide");
    }

    function ValidateCheckBoxSelected() {
        var anyBoxesChecked = false;
        $("input.chkSelectCustomIncentive").each(function () {
            if ($(this).is(":checked")) {
                anyBoxesChecked = true;
            }
        });

        if (!anyBoxesChecked) {
            alert('Please select at least one incentive to bulk update.');
            return false;
        }
        //else {
        //    anyBoxesChecked = false;
        //    $("input.chkSelectAllVehicles").each(function () {
        //        if ($(this).is(":checked")) {
        //            anyBoxesChecked = true;
        //        }
        //    });
        //    if (!anyBoxesChecked) {
        //        alert('Please select at least one vehicle to Apply/Remove Bulk Incentive.');
        //        return false;
        //    }
        //}
        return confirm('Are you sure you want to Apply/Remove Incentives to selected vehicles?');
    }
    $(document).ready(LoadScripts);

    function LoadScripts() {
        $("#txtSearchCustomIncentives").on("keyup", function () {
            var isTextExists = false;
            $('[id$="grdvwVehicleCustomIncentives"]').find('tr').each(function (i, el) {
                if (!$(el).hasClass("HeaderFreez")) {
                    $(this).find('td.filter').each(function (i, tdel) {
                        var inputEl = $(tdel).text().toLowerCase();
                        if (inputEl.indexOf($("#txtSearchCustomIncentives").val().toLowerCase()) > -1) {
                            isTextExists = true;
                        }
                    });
                    if (isTextExists)
                        $(el).css("display", "");
                    else
                        $(el).css("display", "none");

                    isTextExists = false;
                }
            });

        });

        $('[id$="txtVin"]').on("keyup", function () {
            var isTextExists = false;
            var vinText = $(this).val();
            var TotalRecords = 0;
            $('[id$="grdvwVehicleBulkUpdateDashboard"]').find('tr').each(function (i, el) {
                if (!$(el).hasClass("HeaderFreez")) {
                    $(this).find('td.filter').each(function (i, tdel) {
                        var inputEl = $(tdel).text().toLowerCase();
                        if (inputEl.indexOf(vinText.toLowerCase()) > -1) {
                            isTextExists = true;
                        }
                    });
                    if (isTextExists) {
                        TotalRecords++;
                        $(el).css("display", "");
                    }
                    else
                        $(el).css("display", "none");

                    isTextExists = false;
                }
            });
            $('[id$="lblTotalRecords"]').text("Total Rows: " + parseInt(TotalRecords));
        });

    }


</script>

