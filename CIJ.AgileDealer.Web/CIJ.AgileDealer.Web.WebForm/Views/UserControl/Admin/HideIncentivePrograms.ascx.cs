﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Threading.Tasks;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class HideIncentivePrograms : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!IsPostBack)
            {
                BindModelDropdown();
                BindHiddenPrograms();
            }
        }

        protected void btnSaveHideIncentiveProgram_Click(object sender, EventArgs e)
        {
            HideIncentiveProgram(txtProgramNumber.Text.Trim(), txtVIN.Text.Trim(),Convert.ToInt32(ddlHideIncentiveModel.SelectedValue), Convert.ToInt32(ddlHideIncentiveModel.SelectedValue)==0 ? "" : ddlHideIncentiveModel.SelectedItem.Text);

        }
        private async void HideIncentiveProgram(string ProgramNumber, string VIN,int ModelId, string ModelName)
        {
            bool result = await HideIncentiveProgramAsync(ProgramNumber, VIN,ModelId,ModelName);
            if (result)
            {
                BindHiddenPrograms();
                lblError.Text = "Record Updated Successfully!";
            }
        }
        private Task<bool> HideIncentiveProgramAsync(string ProgramNumber, string VIN, int ModelId, string ModelName)
        {
            return Task.Run<bool>(() => VehicleHelper.HideIncentiveProgramAsync(ProgramNumber, VIN, ModelId, ModelName));
        }

        private void BindHiddenPrograms()
        {
            grdvwHideIncentivePrograms.DataSource = VehicleHelper.GetHiddenIncentivePrograms();
            grdvwHideIncentivePrograms.DataBind();
        }

        private void BindModelDropdown()
        {
            List<MasterData> modelTranslateds = MasterDataHelper.GetModelTranslated(new List<int>());
            ddlHideIncentiveModel.DataTextField = "Name";
            ddlHideIncentiveModel.DataValueField = "Id";
            ddlHideIncentiveModel.DataSource= modelTranslateds.OrderBy(x => x.Name);
            ddlHideIncentiveModel.DataBind();
            ddlHideIncentiveModel.Items.Insert(0, new ListItem("Select Model", "0"));
        }

        protected void grdvwHideIncentivePrograms_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteProgram")
            {
                DeleteIncentiveProgram(Convert.ToInt32(e.CommandArgument));


            }
        }
        private async void DeleteIncentiveProgram(int Id)
        {
            bool result = await DeleteIncentiveProgramAsync(Id);
            BindHiddenPrograms();
            lblError.Text = "Record Deleted Successfully!";
        }
        private Task<bool> DeleteIncentiveProgramAsync(int Id)
        {
            return Task.Run<bool>(() => VehicleHelper.DeleteHiddenIncentiveProgram(Id));
        }

    }
}