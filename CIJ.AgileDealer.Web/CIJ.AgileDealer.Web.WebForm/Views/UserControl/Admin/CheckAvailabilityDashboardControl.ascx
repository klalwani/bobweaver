﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckAvailabilityDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.CheckAvailabilityDashboardControl" %>

<script src="../../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.CheckAvailability.js"></script>
<div id="checkAvailabilityDashboard" class="container">
    <table id="tableCheckAvailability" class="hover row-border cell-border display stripe" data-order='[[7, "desc" ]]' data-page-length='10' style="width: 100%;">
        <thead>
            <tr>
                <th>VehicleFeedBackId</th>
                <th>Customer Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Message</th>
                <th>Vin</th>
                <th>Dealer Name</th>
                <th>Submitted Date</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>VehicleFeedBackId</th>
                <th>Customer Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Message</th>
                <th>Vin</th>
                <th>Dealer Name</th>
                <th>Submitted Date</th>
                <th>Detail</th>
            </tr>
        </tfoot>
    </table>
</div>