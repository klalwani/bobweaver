﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleBulkUpdateDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.VehicleBulkUpdateDashboardControl" %>
<div class="clear-20"></div>
<div id="vehicleFeatureDashboard" class="container-fluid"> 
            <div class="row form-title">
                <span>Vehicle Bulk Update Dashboard</span>
                <asp:Button ID="btnSaveDetails" ClientIDMode="Static" runat="server" Text="Update Data" CssClass="btn btn-primary" OnClick="btnSaveDetails_Click" OnClientClick="ShowProgressBar();" />
                <asp:Button ID="btnResetPriceOverride" ClientIDMode="Static" runat="server" Text="Reset Price Overrides" CssClass="btn btn-primary" OnClick="btnResetPriceOverride_Click" OnClientClick="return ResetPriceOverride();" />
            </div>
            <asp:UpdatePanel ID="upnlVehicleBulkUpdateDashboard" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 row">
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:DropDownList ID="ddlyear" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:DropDownList ID="ddlMake" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
                        </div>
                       <%-- <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:DropDownList ID="ddlTrims" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
                        </div>--%>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:Button ID="btnSearch" ClientIDMode="Static" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btnSearch_Click" OnClientClick="ShowProgressBar();" />&nbsp;<asp:Button ID="btnClearSearch" ClientIDMode="Static" runat="server" Text="Clear" CssClass="btn btn-primary" OnClick="btnClearSearch_Click" OnClientClick="ShowProgressBar();"  />
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div class="col-lg-12 row">
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <label for="EmployeePriceLabelText"></label>
                            <asp:TextBox ID="EmployeePriceLabelText" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <label for="EmployeePricePercentageAmount"></label>
                            <asp:TextBox ID="EmployeePricePercentageAmount" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                             <label for="EmployeePricePercentageMakes"></label>
                            <asp:TextBox ID="EmployeePricePercentageMakes" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                       <%-- <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:DropDownList ID="ddlTrims" runat="server" CssClass="form-control" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
                        </div>--%>
                        <div class="col-lg-3 col-sm-4 col-xs-12 text-left">
                            <asp:Button ID="btnSaveEmployeePriceSettings" ClientIDMode="Static" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btnSearch_Click" OnClientClick="ShowProgressBar();" />&nbsp;<asp:Button ID="Button2" ClientIDMode="Static" runat="server" Text="Clear" CssClass="btn btn-primary" OnClick="btnClearSearch_Click" OnClientClick="ShowProgressBar();"  />
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div class="col-lg-12 text-right">
                        <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                    <div style="clear: both"></div>
                    <div class="col-lg-12">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="grdvwVehicleBulkUpdateDashboard" AllowSorting="true" OnRowCommand="grdvwVehicleBulkUpdateDashboard_RowCommand" OnSorting="grdvwVehicleBulkUpdateDashboard_Sorting"  DataKeyNames="AgileVehicleID" OnRowDataBound="grdvwVehicleBulkUpdateDashboard_RowDataBound" AutoGenerateColumns="false" AllowPaging="false" runat="server" CssClass="table table-hover hover row-border cell-border display stripe">
                            <Columns>
                                <asp:BoundField DataField="AgileVehicleID" Visible="false" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Vin" HeaderText="Vin" ItemStyle-HorizontalAlign="Left" SortExpression="Vin" />
                                <asp:BoundField DataField="Stock" HeaderText="Stock #" ItemStyle-HorizontalAlign="Left"  SortExpression="Stock" />
                                <asp:BoundField DataField="Year" HeaderText="Year" ItemStyle-HorizontalAlign="Left"  SortExpression="Year" />
                                <asp:BoundField DataField="IsNew" HeaderText="IsNew" ItemStyle-HorizontalAlign="Left"  SortExpression="IsNew" />
                                
                                <asp:BoundField DataField="Make" HeaderText="Make" ItemStyle-HorizontalAlign="Left"  SortExpression="Make" />
                                <asp:BoundField DataField="Model" HeaderText="Model" ItemStyle-HorizontalAlign="Left"  SortExpression="Model" />
                                <asp:BoundField DataField="Trim" HeaderText="Trim" ItemStyle-HorizontalAlign="Left"  SortExpression="Trim" />
                                <asp:BoundField DataField="MSRP" HeaderText="MSRP ($)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:C}" SortExpression="MSRP"   />
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkSortOriginalSellingPrice" runat="server" Text="Selling Price ($)" CommandName="Sort" CommandArgument="OriginalSellingPrice" />
                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMSRP" runat="server"  Text='<%#Eval("MSRP") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblOriginalSellingPrice" runat="server"  Text='<%#Eval("OriginalSellingPrice") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblSellingPrice" runat="server"  Text='<%#Eval("OriginalSellingPrice","{0:C}") %>' ></asp:Label><br /><br />
                                        <asp:Label ID="lblDealerSaving" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Price Override ($)
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="input-group vfd-label-group">
                                            <asp:HiddenField ID="hdnPriceOverride" runat="server" Value='<%#Eval("overrideprice") %>' />
                                            <div class="input-group-addon">$</div>
                                            <asp:TextBox ID="txtPriceOverride" runat="server" Text='<%#Eval("overrideprice","{0:n}") %>' CssClass="form-control input-lg priceval" onblur="FormatTextValue(this);"></asp:TextBox>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        MSRP Override ($)
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="input-group vfd-label-group">
                                            <asp:HiddenField ID="hdnMSRPOverridePrice" runat="server" Value='<%#Eval("msrpoverride") %>' />
                                            <div class="input-group-addon">$</div>
                                            <asp:TextBox ID="txtMSRPOverridePrice" runat="server" Text='<%#Eval("msrpoverride","{0:n}") %>' CssClass="form-control input-lg priceval" onblur="FormatTextValue(this);"></asp:TextBox>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Employee Price ($)
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="input-group vfd-label-group">
                                            <asp:HiddenField ID="hdnEmployeePrice" runat="server" Value='<%#Eval("employeeprice") %>' />
                                            <div class="input-group-addon">$</div>
                                            <asp:TextBox ID="txtEmployeePrice" runat="server" Text='<%#Eval("employeeprice","{0:n}") %>' CssClass="form-control input-lg priceval" onblur="FormatTextValue(this);"></asp:TextBox>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSaveDetails" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnClearSearch" EventName="Click" />

                </Triggers>

            </asp:UpdatePanel>

</div>
<style>
    #MainContent_VehicleBulkUpdateDashboardControl_upnlVehicleBulkUpdateDashboard,
    #MainContent_VehicleBulkUpdateDashboardControl_upnlVehicleBulkUpdateDashboard input {
        font-size:14px;
    }
</style>
<script>
    //function CheckUnCheckItems(obj) {
    //    if (document.getElementById(obj.id).checked) {
    //        $(".cbVehicleFeature input").prop('checked', true);
    //    }
    //    else
    //        $(".cbVehicleFeature input").prop('checked', false);
    //}
    function ShowProgressBar() {
        $.blockUI(siteMaster.loadingWheel());
        window.setTimeout('HideLoadingWheetl()', 3000);
    }
    function HideLoadingWheetl() {
        $.unblockUI(siteMaster.loadingWheel());
    }

    function FormatTextValue(obj) {
        document.getElementById(obj.id).value = CommaFormatted(document.getElementById(obj.id).value);
    }
    function CommaFormatted(nStr) {

        if (typeof nStr != "undefined" && nStr !== '' && nStr !== 0) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }


        return nStr;
    }
    function ResetPriceOverride()
    {
        var returnValue;
        returnValue = confirm('Are you sure you want to reset?');
        if (returnValue)
            ShowProgressBar();
        else
            return false;
    }
</script>
