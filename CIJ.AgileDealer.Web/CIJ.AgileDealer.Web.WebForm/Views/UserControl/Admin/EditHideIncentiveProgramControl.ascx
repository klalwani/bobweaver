﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditHideIncentiveProgramControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.EditHideIncentiveProgramControl" %>

<script src="../../Scripts/Pages/AgileDealer.HideIncentiveProgram.js"></script>
<div id="EditHideIncentiveProgramControl" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="lblProgramNameSpecialProgram">Add Program Number to Hide from Incentive</h4>
            </div>
        </div>

        <div class="modal-body col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div class="container-fluid">
                <div class="offer-cards row-fluid">
                     
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="label-group marginTop5">Program Number</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="txtProgramNumber" />
                        </div>
                    </div>
   
                </div>
            </div>
        </div>
        <div class="modal-footer clear">
            <div class="col-xs-12 text-right">
                     <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveHideIncentiveProgram">Save Changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseHideIncentiveProgram">Close</button>
             </div>
        </div>
    </div>
    <div class="clear-20"></div>
 
</div>
 