﻿using CJI.AgileDealer.Web.WebForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Vehicles;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class VehicleFeatureDashboardControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindVehicleFeatures();
        }
        private void BindVehicleFeatures()
        {
            var VehicleFeatures = VehicleHelper.GetVehicleFeatureMasterData();
            grdvwVehicleFeature.DataSource = VehicleFeatures;
            grdvwVehicleFeature.DataBind();
        }

        protected async void btnSaveDetails_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.Visible = false;
            try
            {
                foreach (GridViewRow item in grdvwVehicleFeature.Rows)
                {
                    if (((CheckBox)item.FindControl("chkIsSearchEnabled")).Checked)
                    {
                        var vehicleFeatureMaster = new VehicleFeatureMaster()
                        {
                            FeatureId = Convert.ToInt32(grdvwVehicleFeature.DataKeys[item.RowIndex].Value),
                            LabelOverride = ((TextBox)item.FindControl("txtLabelOverride")).Text,
                            IsSearchable = true
                        };
                        await VehicleHelper.UpdateVehicleFeatureMasterData(vehicleFeatureMaster);
                    }
                    else
                    {
                        var vehicleFeatureMaster = new VehicleFeatureMaster()
                        {
                            FeatureId = Convert.ToInt32(grdvwVehicleFeature.DataKeys[item.RowIndex].Value),
                            LabelOverride = ((TextBox)item.FindControl("txtLabelOverride")).Text,
                            IsSearchable = false
                        };
                        await VehicleHelper.UpdateVehicleFeatureMasterData(vehicleFeatureMaster);
                    }
                }
                lblError.Text = "Records updated successfully.";
                lblError.Visible = true;
            }

            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
            //finally
            //{
            //    ScriptManager.RegisterStartupScript(btnSaveDetails, this.GetType(), "HideLoadingWheel", "$.unblockUI(siteMaster.loadingWheel());", true);
            //}
        }
    }
}