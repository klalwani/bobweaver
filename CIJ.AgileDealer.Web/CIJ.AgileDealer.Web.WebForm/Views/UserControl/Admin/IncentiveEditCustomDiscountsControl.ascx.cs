﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class IncentiveEditCustomDiscountsControl : BaseUserControl
    {
        public bool IsEditForVin { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            HdfIsEditForVinCustomDiscounts.Value = IsEditForVin.ToString();
        }
    }
}