﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MissingIncentiveProgramsControl.ascx.cs"
    Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.MissingIncentiveProgramsControl" %>
<script src="../../../Scripts/Pages/Admin/AgileDealer.JobManagement.js"></script>
<script src="../../../Scripts/Pages/Enums/Job.Enum.js"></script>
<div id="divMissingIncentivePrograms" class="container-fluid">
    <div class="body-content-wrapper">
        <div class="container-fluid">
            <div class="row form-title">
                <span>Missing Incentive Programs Dashboard</span>
                <input type="button" id="btnTriggerIncentivePuller" name="btnTriggerIncentivePuller" value="Update Incentives" class="btn btn-success" style="float: right;margin: 10px;line-height: 2;" />
            </div>
            <asp:UpdatePanel ID="upnlvehicleOptionsDashboard" runat="server">
                <ContentTemplate>
                    <div class="col-xs-12 col-lg-8 col-md-8 col-sm-12">
                        <div class="container-fluid">
                            <div class="offer-cards row-fluid">
                                <div class="form-group row">
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label class="label-group marginTop5">Program Number</label>
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="txtProgramNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-8 text-left">
                                        <asp:Button ID="btnSaveMissingIncentiveProgram" runat="server" Text="Save"
                                            CssClass="btn btn-primary"
                                            OnClick="btnSaveMissingIncentiveProgram_Click" />
                                        <%-- <asp:Button ID="btnClear" runat="server" Text="Clear"
                                            CssClass="btn"
                                            OnClientClick="clearControls();" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-left">
                        <label style="font-weight:bold;color: red">**Note: Once a program is added, Incentives will need to be run for it to display. Please click on "Update Incentives" button to run the incentives.</label>
                    </div>
                    <div class="clear-20">&nbsp;</div>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                        <asp:GridView ID="grdvwMissingIncentivePrograms" OnRowCommand="grdvwMissingIncentivePrograms_RowCommand"
                            DataKeyNames="ProgramNumber" AutoGenerateColumns="false" AllowPaging="false" runat="server"
                            CssClass="table table-bordered table-hover table-striped">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ProgramNumber" ItemStyle-HorizontalAlign="Left" HeaderText="Program Number" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteProgram"
                                            CommandArgument='<%#Eval("Id")%>' Text="Delete"
                                            OnClientClick="return confirm('Are you sure you want to delete?');"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</div>
<script>
    function clearControls() {
        $("#MainContent_divMissingIncentivePrograms_lblError").html("");
        $("#MainContent_MissingIncentiveProgramsControl_txtProgramNumber").val("");
    }

    function ShowProgressBar() {
        $("#MainContent_divMissingIncentivePrograms_lblError").html("");
        $.blockUI(siteMaster.loadingWheel());
        window.setTimeout('HideLoadingWheetl()', 1000);
    }
    function HideLoadingWheetl() {
        $.unblockUI(siteMaster.loadingWheel());
    }
</script>
