﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class IncentiveDashboardSpecialProgramControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<MasterData> modelTranslateds = MasterDataHelper.GetModelTranslated(new List<int>());
            ModelList.Value = JsonHelper.Serialize(modelTranslateds.OrderBy(x => x.Name));
            List<MasterData> EnginesList = MasterDataHelper.GetEnginesForExclude();
            EngineList.Value = JsonHelper.Serialize(EnginesList.OrderBy(x => x.Name));
            List<MasterData> VinList = MasterDataHelper.GetVinList();
            ExcludeVinList.Value = JsonHelper.Serialize(VinList.OrderBy(x => x.Name));

            List<MasterData> lstTrims = MasterDataHelper.GetTrimList();
            TrimList.Value = JsonHelper.Serialize(lstTrims.OrderBy(x => x.Name));

            List<MasterData> lstYear = new List<MasterData>();

            for (int iCounter = 0; iCounter < 15; iCounter++)
            {
                lstYear.Add(new MasterData()
                {
                    Id = DateTime.Now.AddYears(-iCounter).Year,
                    Name = DateTime.Now.AddYears(-iCounter).Year.ToString()
                });
            }

            YearList.Value = JsonHelper.Serialize(lstYear.OrderBy(x => x.Name));


            List<MasterData> lstBodyStyle = MasterDataHelper.GetBodyStyleList();
            BodyStyleList.Value = JsonHelper.Serialize(lstBodyStyle.OrderBy(x => x.Name));

            IncentiveEditControlSpecialProgram.IsEditForVin = false;
        }
    }
}