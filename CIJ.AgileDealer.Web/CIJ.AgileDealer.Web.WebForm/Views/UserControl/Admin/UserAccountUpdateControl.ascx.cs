﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.Base.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class UserAccountUpdateControl : BaseUserControl
    {
        private List<MasterData> _roles;
        private List<MasterData> Roles
        {
            get
            {
                if (_roles == null)
                {
                    _roles = RoleHelper.GetRoles();
                }

                return _roles;
            }
            set { _roles = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindingMasterData();
                string userId = Page.RouteData.Values["UserId"] as string;
                if (!string.IsNullOrEmpty(userId))
                {
                    ApplicationUser user = UserAccountHelper.GetUserById(userId);
                    BindingDataForUser(user);
                }
            }
        }

        private void BindingDataForUser(ApplicationUser user)
        {
            if (user != null)
            {
                userName.Value = user.UserName;
                email.Value = user.Email;
                phoneNumber.Value = user.PhoneNumber;
                IdentityRole role = RoleHelper.GetRoleByUserId(user.Id);
                if (role != null)
                {
                    currentRoleData.SelectedValue = role.Id;
                }
            }
        }

        private void BindingMasterData()
        {
            currentRoleData.DataSource = Roles;
            currentRoleData.DataBind();
        }

        protected void btnSubmit_OnServerClick(object sender, EventArgs e)
        {
            string userId = Page.RouteData.Values["UserId"] as string;
            var role = Roles.FirstOrDefault(x => x.IdString == currentRoleData.SelectedItem.Value);
            if (role != null)
            {
                string roleName = role.Name;
                bool isSuccess = RoleHelper.AssignUserToRole(userId, roleName);
                if(isSuccess)
                    Response.Redirect("~/Admin/UserAccount");
            }

        }
    }
}