﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncentiveDashboardControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin.IncentiveDashboardControl" %>
<%@ Register Src="~/Views/UserControl/Admin/IncentiveEditControl.ascx" TagPrefix="IncentiveEditControl" TagName="IncentiveEditControl" %>


<script src="../../Scripts/Pages/Dashboards/AgileDealer.Dashboard.IncentiveManagement.js"></script>
<div id="incentiveManagementDashboard" class="container-fluid">
    
        <div class="container-fluid">
            <div class="row form-title">
                <span>Incentive Dashboard</span>
            </div>
            <table id="tableIncentiveManagement" class="table table-hover hover row-border cell-border display stripe" style="width: 100% !important;" data-order='[[3, "desc" ]]' data-page-length='10'>
                <thead>
                <tr>
                    <th>Section</th>
                    <th>Translated Name</th>
                    <th>Program Name</th>
                    <th>Type</th>
                    <th>Id</th>
                    <th>Amount</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Section</th>
                    <th>Translated Name</th>
                    <th>Program Name</th>
                    <th>Type</th>
                    <th>Id</th>
                    <th>Amount</th>
                    <th></th>
                    <th></th>
                    <th></th>
                   <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <IncentiveEditControl:IncentiveEditControl runat="server" ID="IncentiveEditControl" />
        <asp:HiddenField runat="server" ID="ModelList"/>
  
</div>






