﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Admin
{
    public partial class IncentiveDashboardCustomDiscountsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<MasterData> modelTranslateds = MasterDataHelper.GetModelTranslated(new List<int>());
            ModelListCustomDiscounts.Value = JsonHelper.Serialize(modelTranslateds.OrderBy(x => x.Name));
            List<MasterData> EnginesList = MasterDataHelper.GetEnginesForExclude();
            EngineListCustomDiscounts.Value = JsonHelper.Serialize(EnginesList.OrderBy(x => x.Name));
            List<MasterData> VinList = MasterDataHelper.GetVinList();
            ExcludeVinListCustomDiscounts.Value = JsonHelper.Serialize(VinList.OrderBy(x => x.Name));

            List<MasterData> TrimList = MasterDataHelper.GetTrimList();
            TrimListCustomDiscounts.Value = JsonHelper.Serialize(TrimList.OrderBy(x => x.Name));

            List<MasterData> YearList = new List<MasterData>();

            for (int iCounter = 0; iCounter < 15; iCounter++)
            {
                YearList.Add(new MasterData()
                {
                    Id = DateTime.Now.AddYears(-iCounter).Year,
                    Name = DateTime.Now.AddYears(-iCounter).Year.ToString()
                });
            }

            YearListCustomDiscounts.Value = JsonHelper.Serialize(YearList.OrderBy(x => x.Name));


            List<MasterData> BodyStyleList = MasterDataHelper.GetBodyStyleList();
            BodyStyleListCustomDiscounts.Value = JsonHelper.Serialize(BodyStyleList.OrderBy(x => x.Name));

            IncentiveEditControlCustomDiscounts.IsEditForVin = false;
        }
    }
}