﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.ContactUs.ContactUs" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.ContactUs" %>
<input type="hidden" id="dealerName" value="<%= DealerName %>" />
<input type="hidden" data-attribute="validation" name="SuccessMessage" value="<%=ContactUs.SuccessMessage %>" />
<section id="contact-us-page" class="container">
    <div class="box col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="title">
            <p><%=ContactUs.Title_P %></p>
            <div class="border"></div>
        </div>

        <div class="register-form">
            <div class="form-group row">
                <label><%=ContactUs.Register_Form_Label_1 %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtFirstName" runat="server" ID="txtFirstName" data-val="true" data-val-required="true" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row">
                <label><%=ContactUs.Register_Form_Label_2 %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtLastName" runat="server" ID="txtLastName" data-val="true" data-val-required="true" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row">
                <label><%=ContactUs.Register_Form_Label_3 %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtEmail" runat="server" ID="txtEmail" data-val="true" data-val-required="true" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row">
                <label><%=ContactUs.Register_Form_Label_4 %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtPhoneNumber" runat="server" ID="txtPhoneNumber" data-val="true" data-val-required="true" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row">
                <label><%=ContactUs.PostalCode_Label %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtPostalCode" runat="server" ID="txtPostalCode" data-val="true" data-val-required="true" MaxLength="6" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row text-area">
                <label><%=ContactUs.Register_Form_Label_5 %></label>
                <div class="col-md-12">
                    <asp:TextBox aria-label="txtQuestionComment" runat="server" ID="txtQuestionComment" TextMode="MultiLine" Rows="5" CssClass="form-control" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <%--<asp:Button runat="server" Text="Submit" ID="btnSubmit" CssClass="btn btn-default" />--%>
                    <input type="submit" aria-label="submit" class="btn btn-default" value="<%=ContactUs.Col_Md_12_Button %>" id="btnSubmit" />
                </div>
            </div>
        </div>
    </div>

    <div class="box col-xs-12 col-sm-12 col-md-6 col-lg-6 mdl-box-shadow">
        <div class="title">
            <p><%=ContactUs.Title_P_1 %></p>
            <div class="border"></div>
        </div>
        <div class="dealer-information col-md-12">
            <div class="address">
                <div>
                    <span class="dealerinfoname">
                        <strong>
                            <asp:Literal runat="server" ID="ltrName" />
                        </strong>
                    </span>
                </div>
                <div>
                    <span class="dealerinfoaddress">
                        <asp:Literal runat="server" ID="ltrAddress1" />
                    </span>
                </div>
                <div>
                    <span class="dealerinfoaddress2">
                        <asp:Literal runat="server" ID="ltrAddress2" />
                    </span>
                </div>
                <div>
                    <span class=""><%=ContactUs.Address_Div_Span %></span>
                    <span class="dealerinfophone">
                        <span class="dynamic-phone-number">
                            <asp:Literal runat="server" ID="ltrPhone" />
                        </span>
                    </span>
                </div>
            </div>

            <h3 class="hourstable"><%=ContactUs.Hourstable %></h3>

            <asp:Repeater runat="server" ID="rptSalesHours">
                <HeaderTemplate>
                    <table class="hourstable">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("ShortDayName") %></td>
                        <td>
                            <span><%#Eval("Time") %></span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                                </table>
                </FooterTemplate>
            </asp:Repeater>

            <h3 class="hourstable"><%=ContactUs.Hourstable_1 %></h3>

            <asp:Repeater runat="server" ID="rptServicesHours">
                <HeaderTemplate>
                    <table class="hourstable">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("ShortDayName") %></td>
                        <td>
                            <span><%#Eval("Time") %></span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                                </table>
                </FooterTemplate>
            </asp:Repeater>

            <h3 class="hourstable"><%=ContactUs.Hourstable_2 %></h3>

            <asp:Repeater runat="server" ID="rptPartHours">
                <HeaderTemplate>
                    <table class="hourstable">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("ShortDayName") %></td>
                        <td>
                            <span><%#Eval("Time") %></span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                                </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>
    </div>

    <div class="box col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="title">
            <p><%=ContactUs.Title_P_2 %></p>
            <div class="border"></div>
        </div>

        <div class="direction-form col-md-12">
            <div id="googleMap"></div>
        </div>
    </div>


</section>
