﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.ContactUs;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.Base.Models;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using AgileDealer.Data.Entities.Buyer;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.ContactUs
{
    public partial class ContactUs : BaseUserControl
    {
        private string mailFrom = ConfigurationManager.AppSettings["smtpAcct"];
        private string bccEmail = ConfigurationManager.AppSettings["BCCEmail"];
        public Dealer Dealer => MasterDataHelper.GetDefaultDealer(DealerID);
        private bool useADF = bool.Parse(ConfigurationManager.AppSettings["UseADF"]);

        public string EleadXmlFilePath { get; set; } = @"~\XMLs\EleadTrack.xml";

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadDealerInfo();
        }

        void LoadDealerInfo()
        {
            using (var db = new ServicePageDbContext())
            {
                var dealer = db.Dealers.FirstOrDefault();
                if (dealer != null)
                {
                    ltrName.Text = dealer.Name;

                    var location = db.Locations.FirstOrDefault(l => l.DealerID == dealer.DealerID);
                    if (location != null)
                    {
                        ltrAddress1.Text = location.Street1;
                        ltrAddress2.Text = location.Street2;
                        ltrPhone.Text = location.MainPhone;
                    }

                    LoadHour(dealer.DealerID);
                }
            }
        }

        void LoadHour(int dealerId)
        {
            GetHours(dealerId, (int)Enum.ServiceHourType.SalesShort, rptSalesHours);
            GetHours(dealerId, (int)Enum.ServiceHourType.ServiceShort, rptServicesHours);
            GetHours(dealerId, (int)Enum.ServiceHourType.PartsShort, rptPartHours);
        }

        void GetHours(int dealerId, int type, Repeater rpt)
        {
            List<HoursModel> result = MasterDataHelper.GetHours(dealerId, LanguageId, type);
            if (result != null)
            {
                rpt.DataSource = result.ToList();
                rpt.DataBind();
            }
        }
    }
}