﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Blog;
using CJI.AgileDealer.Web.Base.Context;
using WebGrease.Css.Extensions;
using System.Web.Routing;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Views.VCP;
using Newtonsoft.Json;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog
{
    public partial class BlogEntries : BaseUserControl
    {
        public BlogDefaultFilter BlogDefaultFilter
        {
            get
            {
                if (ViewState["BlogDefaultFilter"] == null)
                    return new BlogDefaultFilter();
                return (BlogDefaultFilter) ViewState["BlogDefaultFilter"];
            }
            set { ViewState["BlogDefaultFilter"] = value; }
        }

        private List<BlogCategory> _blogCategories;

        public List<BlogCategory> BlogCategories
        {
            get
            {
                if (_blogCategories != null) return _blogCategories;
                string categoriesString = Page.RouteData.Values["CategorySelected"] as string;
                _blogCategories = MasterDataHelper.GetBlogCategoriesByName(categoriesString, DealerID);
                return _blogCategories;
            }
            set { _blogCategories = value; }
        }


        private string _blogId;

        public string BlogId
        {
            get
            {
                _blogId = hdfPostId.Value;
                return _blogId;
            }
            set { _blogId = value; }
        }

        private List<BlogTag> _blogTags;

        private List<BlogTag> BlogTags
        {
            get
            {
                if (_blogTags != null) return _blogTags;
                string blogTagsString = Page.RouteData.Values["TagSelected"] as string;

                _blogTags = MasterDataHelper.GetBlogTagByName(blogTagsString, DealerID);
                return _blogTags;
            }
            set { _blogTags = value; }
        }


        private List<int> _blogTagIds;

        private List<int> BlogTagIds
        {
            get
            {
                if (BlogDefaultFilter.DefaultTag != null)
                {
                    _blogTagIds = BlogDefaultFilter.DefaultTag;
                }
                else
                {
                    List<int> blogTagSelected = Session["TagsSelected"] as List<int>;

                    if (blogTagSelected != null && blogTagSelected.Any())
                        _blogTagIds = blogTagSelected.ToList();
                }

                return _blogTagIds ?? (_blogTagIds = BlogTags.Select(x => x.BlogTagId).ToList());
            }
            set { _blogTagIds = value; }
        }

        private List<int> _blogCategorieIds;

        private List<int> BlogCategorieIds
        {
            get
            {

                string categoriesString = Page.RouteData.Values["CategorySelected"] as string;
                if (string.IsNullOrEmpty(categoriesString))
                {
                    _blogCategorieIds = BlogDefaultFilter.DefaultCategory;
                }

                if (_blogCategorieIds == null)
                {
                    _blogCategorieIds = BlogCategories.Select(x => x.BlogCategoryId).ToList();
                }

                return _blogCategorieIds;
            }
            set { _blogCategorieIds = value; }
        }

        private int _baseModelId;
        public int BaseModelId
        {
            get
            {
                return _baseModelId;
            }
            set { _baseModelId = value; }
        }

        private BlogSearchControl _blogSearchControl;

        private BlogSearchControl BlogSearchControl
        {
            get
            {
                if (_blogSearchControl == null)
                {
                    Blogs.Blog currentPage = this.Page as Blogs.Blog;
                    if (currentPage != null)
                    {
                        BlogContent blogContent = currentPage.GetBlogContent();
                        if (blogContent != null)
                        {
                            _blogSearchControl = blogContent.GetBlogSearchControl();
                        }
                    }
                }

                return _blogSearchControl;
            }
            set { _blogSearchControl = value; }
        }

        private string Query => Request.QueryString["page"];

        protected void Page_Load(object sender, EventArgs e)
        {
            string parameter = Request["__EVENTARGUMENT"];
            string postId = Page.RouteData.Values["PostID"] as string;
            if (!string.IsNullOrEmpty(parameter) && !string.IsNullOrEmpty(postId))
            {
                BlogData blogData = JsonHelper.Deserialize<BlogData>(parameter);
                if (blogData != null)
                {
                    Session["PostId"] = postId;
                    Response.Redirect(blogData.BlogUrl);
                }
            }

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Query))
                {
                    Session["TagsSelected"] = null;
                }
            }

            if (BlogSearchControl != null)
            {
                BlogSearchControl.GetSelectedTags(Query);
                BlogSearchControl.PrepairDataForSelection();
            }
            LoadListBlogEntry();
        }

        private void LoadListBlogEntry()
        {
            using (var db = new ServicePageDbContext())
            {
                int languageid = SiteInfoHelper.GetLanguageId();
                hdfDealerShipTitle.Value = string.Empty;
                List<BlogEntryResult> list = new List<BlogEntryResult>();

                string strSearch = Page.RouteData.Values["SearchTerm"] as string;

                if (BlogTagIds.Count > 1)
                    list = VehicleHelper.GetBlogEntryIdsByTag(BlogTagIds,languageid,DealerID, BaseModelId);
                else
                    list = GetBlogEntry(db, strSearch,DealerID);

                rptEntry.DataSource = list.OrderByDescending(x => x.DateEnter).ToList();
                rptEntry.DataBind();

                if (!list.Any())
                {
                    var parent = this.Page as VCP.VcpPage;
                    if (parent != null)
                        parent.VisibleReviewTab(false);
                }

                if (!string.IsNullOrEmpty(strSearch))
                {
                    hdfCategoryID.Value = strSearch;
                    hdfDealerShipTitle.Value = $"Dealership Research > Search > {strSearch}";
                }

                else if (BlogCategorieIds != null && BlogCategorieIds.Any())
                {
                    BlogCategory category =
                        MasterDataHelper.GetBlogCategoriesById(BlogCategorieIds.FirstOrDefault(), DealerID).FirstOrDefault();
                    if (category != null)
                    {
                        hdfCategoryID.Value = category.CategoryText;
                        hdfDealerShipTitle.Value =
                            $"Dealership Research > Category > {category.CategoryText}";
                    }
                }

                else if (BlogTagIds != null && BlogTagIds.Any())
                {
                    BlogTag tag = MasterDataHelper.GetBlogTagById(BlogTagIds.FirstOrDefault(), DealerID).FirstOrDefault();
                    if (tag != null)
                    {
                        hdfTagID.Value = tag.TagText;
                        hdfDealerShipTitle.Value = $"Dealership Research > Tag > {tag.TagText}";
                    }
                }
            }
        }


        private List<BlogEntryResult> GetBlogEntry(ServicePageDbContext db, string strSearch,int dealerId)
        {
            int languageId = SiteInfoHelper.GetLanguageId();
            List<BlogEntryResult> result;
            strSearch = HttpUtility.HtmlEncode(strSearch);

            if (string.IsNullOrEmpty(strSearch))
                strSearch = string.Empty;

            var temp = (from blogEntries in db.BlogEntrys.Where(x=>x.DealerID == dealerId)

                join blogEntriesCategories in db.BlogEntries_BlogCategories on blogEntries.BlogEntryId equals
                    blogEntriesCategories.BlogEntryId into bcJoin
                from leftBcJoin in bcJoin.DefaultIfEmpty()
                join categories in db.BlogCategoryies on leftBcJoin.BlogCategoryId equals categories.BlogCategoryId into
                    cJoin
                from leftCJoin in cJoin.DefaultIfEmpty()

                join blogEntriesTags in db.BlogEntries_BlogTags on blogEntries.BlogEntryId equals
                    blogEntriesTags.BlogEntryId into btJoin
                from leftBtJoin in btJoin.DefaultIfEmpty()
                join tags in db.BlogTags on leftBtJoin.BlogTagId equals tags.BlogTagId into tJoin
                from leftTJoin in tJoin.DefaultIfEmpty()

                join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                                on blogEntries.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                from blog_Tran in blogTranJoin.DefaultIfEmpty()

                orderby blogEntries.DateEnter

                select new
                {
                    BlogEntryId = blogEntries.BlogEntryId,
                    PostTitle = blog_Tran.PostTitle ?? blogEntries.PostTitle,
                    PostUrl = blog_Tran.PostUrl ?? blogEntries.PostUrl,
                    PostSummary = blog_Tran.PostSummary ?? blogEntries.PostSummary,
                    PostFeatureImage = blogEntries.PostFeatureImage,
                    PostText = blog_Tran.PostText ?? blogEntries.PostText,
                    BlogCategoryId = leftBcJoin.BlogCategoryId,
                    BlogTagId = leftBtJoin.BlogTagId,
                    DateEnter = blogEntries.CreatedDt
                });

            if (BlogCategorieIds.Any())
                temp = temp.Where(x => BlogCategorieIds.Contains(x.BlogCategoryId));

            if (BlogTagIds.Any())
                temp = temp.Where(x => BlogTagIds.Contains(x.BlogTagId));

            if (!string.IsNullOrEmpty(strSearch))
            {
                temp =
                    temp.Where(
                        x =>
                            x.PostTitle.ToLower().Contains(strSearch.ToLower()) ||
                            x.PostText.ToLower().Contains(strSearch.ToLower()));
            }

            result = temp.Select(x =>
                new BlogEntryResult
                {
                    BlogEntryId = x.BlogEntryId,
                    PostTitle = x.PostTitle,
                    PostUrl = x.PostUrl,
                    PostSummary = x.PostSummary,
                    PostFeatureImage = x.PostFeatureImage,
                    DateEnter = x.DateEnter
                }).Distinct().ToList();

            return result;
        }


        private List<BlogEntry> GetBlogEntryByBlogCaterogy(ServicePageDbContext db, string blogCategory)
        {
            List<BlogEntry> result = null;
            blogCategory = HttpUtility.HtmlEncode(blogCategory);
            result = (from b in db.BlogEntrys
                join bc in db.BlogEntries_BlogCategories on b.BlogEntryId equals bc.BlogEntryId
                join cat in db.BlogCategoryies on bc.BlogCategoryId equals cat.BlogCategoryId
                where BlogCategorieIds.Contains(cat.BlogCategoryId)
                select b).ToList();
            return result;
        }

        private List<BlogEntry> GetBlogEntryByBlogTag(ServicePageDbContext db, string blogTag)
        {
            List<BlogEntry> result = null;
            blogTag = HttpUtility.HtmlEncode(blogTag);
            result = (from b in db.BlogEntrys
                join bt in db.BlogEntries_BlogTags on b.BlogEntryId equals bt.BlogEntryId
                join tag in db.BlogTags on bt.BlogTagId equals tag.BlogTagId
                where BlogTagIds.Contains(tag.BlogTagId)
                orderby b.DateEnter
                select b).ToList();
            return result;
        }

        protected void OnServerClick(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            string postId = btn.CommandArgument;
            string url = btn.Attributes["url"];
            Session["PostId"] = postId;
        }
    }

    public class BlogData
    {
        [JsonProperty(PropertyName = "BlogId")]
        public string BlogId { get; set; }
        [JsonProperty(PropertyName = "BlogUrl")]
        public string BlogUrl { get; set; }
    }
}

