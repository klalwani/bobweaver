﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Blog;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog
{
    public partial class BlogEntryDetail : BaseUserControl
    {
        private int _blogId;
        public int BlogId
        {
            get
            {
                if (_blogId > 0)
                {
                    return _blogId;
                }
                string blogIdString = Page.RouteData.Values["PostID"] as string;
                if(!string.IsNullOrEmpty(blogIdString))
                    _blogId = int.Parse(blogIdString);
                return _blogId;
            }
            set { _blogId = value; }
        }

        private string _blogUrl;

        public string BlogUrl
        {
            get
            {
                _blogUrl = Page.RouteData.Values["PostUrl"] as string;
                return _blogUrl;
            }
            set { _blogUrl = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDetail();
            }
        }

        private void LoadDetail()
        {
            try
            {
                using (var db = new ServicePageDbContext())
                {
                    int languageId = SiteInfoHelper.GetLanguageId();
                    var blogEntry = (from blog in db.BlogEntrys
                                    join blogTrans in db.BlogEntry_Trans.Where(x => x.LanguageId == languageId)
                                    on blog.BlogEntryId equals blogTrans.BlogEntryId into blogTranJoin
                                    from blog_Tran in blogTranJoin.DefaultIfEmpty()
                                     where blog.PostUrl == BlogUrl && blog.DealerID == DealerID
                                     select new
                                     {
                                         PostTitle = blog_Tran.PostTitle ?? blog.PostTitle,
                                         PostText = blog_Tran.PostText ?? blog.PostText,
                                         PostFeatureImage = blog.PostFeatureImage,
                                         Button1Text = blog_Tran.Button1Text ?? blog.Button1Text,
                                         Button1Path = blog_Tran.Button1Path ?? blog.Button1Path,
                                         Button2Text = blog_Tran.Button2Text ?? blog.Button2Text,
                                         Button2Path = blog_Tran.Button2Path ?? blog.Button2Path,
                                         PostUrl = blog.PostUrl,

                                     }).FirstOrDefault();
                    if (blogEntry != null)
                    {
                        ltrMessages.Visible = false;

                        ltrPostTitle.Text = blogEntry.PostTitle;
                        ltrPostText.Text = blogEntry.PostText;
                        imgPostFeatureImage.ImageUrl = blogEntry.PostFeatureImage;
                        btnText1.Text = blogEntry.Button1Text;
                        if (!string.IsNullOrEmpty(btnText1.Text))
                        {
                            btnText1.PostBackUrl = blogEntry.Button1Path;
                        }
                        else
                        {
                            btnText1.PostBackUrl = blogEntry.PostUrl.Contains("Used") ? "/used" : "/new";
                        }
                        btnText2.Text = blogEntry.Button2Text;

                        if (!string.IsNullOrEmpty(btnText2.Text))
                        {
                            btnText2.PostBackUrl = blogEntry.Button2Path;
                        }
                        else
                        {
                            btnText2.PostBackUrl = blogEntry.PostUrl.Contains("Used") ? "/used" : "/new";
                        }
                    }
                    else
                    {
                        ltrMessages.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ltrMessages.Visible = true;
                var el = ExceptionHelper.LogException("BlogEntryDetail.LoadDetail() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: LoadDetail with BlogEntryId: "+ BlogId);
                EmailHelper.SendEmailForNotificationWhenFailing(el);
            }
            finally
            {
                ltrMessages.DataBind();
            }
        }
    }
}
