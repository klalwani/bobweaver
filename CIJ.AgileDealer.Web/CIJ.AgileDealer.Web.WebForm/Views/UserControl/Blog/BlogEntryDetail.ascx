﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogEntryDetail.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog.BlogEntryDetail" %>
<%@ Register Src="~/Views/UserControl/ContactUs/ContactUs.ascx" TagName="ContactUs" TagPrefix="contactUsUserControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.Blog" %>
<script src="../../Scripts/Pages/AgileDealer.Validation.js"></script>
<script src="../../Scripts/Pages/AgileDealer.ContactUs.js"></script>
<script src="../../Scripts/Pages/AgileDealer.BlogDetail.js"></script>
<%--<%: Scripts.Render("~/bundles/BlogDetail") %>--%>
<div id="blog-detail" class="container">
    <section class="section">
        <div class="section-inner">
            <div class="content">
                <div class="item featured text-left">
                    <h1 class="blog-title">
                        <asp:Literal ID="ltrPostTitle" runat="server" />
                    </h1>
                    <div class="blog-herobox">
                        <div class="featured-image">
                            <asp:Image ID="imgPostFeatureImage" CssClass="img-responsive project-image" runat="server" />
                        </div>
                        <div class="button-link">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <asp:LinkButton runat="server" ID="btnText1" CssClass="btn btn-cta-ford"> <%=BlogEntryDetail.Button_Link_Text_1 %> </asp:LinkButton>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <asp:LinkButton runat="server" ID="btnText2" CssClass="btn btn-def-ford"> <%=BlogEntryDetail.Button_Link_Text_2 %> </asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="desc blog-card">
                        <asp:Literal ID="ltrPostText" runat="server" />
                    </div>
                    <h3>
                        <asp:Literal ID="ltrMessages" Visible="False" runat="server" Text="<%#BlogEntryDetail.LtrMessages %>" />
                    </h3>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="hidden">
    <div id="ucContact" class="contact-us">
        <div class="row text-center contact-title">
            <div class="close-button">
                <h2 id="lblOffer"><%=BlogEntryDetail.LblOffer %></h2>
                <input type="button" value="x" id="btnCloseContact" class="btn" />
            </div>
        </div>
        <contactUsUserControl:ContactUs ID="UCContactUs" runat="server" />
    </div>
</div>
