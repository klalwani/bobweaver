﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Blog;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Blog
{
    public partial class BlogContent : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadView();
            //BlogDefaultFilter filters = new BlogDefaultFilter
            //{
            //    DefaultCategory = "Review"
            //};
            //UCBlogEntries.BlogDefaultFilter = filters;
        }

        public BlogSearchControl GetBlogSearchControl()
        {
            return BlogSearchControl;
        }

        private void LoadView()
        {
            if (!string.IsNullOrEmpty(Page.RouteData.Values["PostUrl"] as string))
            {
                MultiBlog.SetActiveView(vwBlogEntryDetail);
            }
        }
    }
}