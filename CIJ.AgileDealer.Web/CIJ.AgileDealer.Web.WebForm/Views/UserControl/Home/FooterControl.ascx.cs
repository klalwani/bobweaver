﻿using CJI.AgileDealer.Web.Base.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using static CIJ.AgileDealer.Web.WebForm.Common.Enum;
using AgileDealer.Data.Entities.Content;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Home
{
    public partial class FooterControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var db = new ServicePageDbContext())
                {
                   
                    BindFooter(db);
                     
                }

            }
        }
        public List<DealerModel> GetHours(int hourtypeId)
        {
            var langid = SiteInfoHelper.GetLanguageId();

            List<DealerModel> ddm = new List<DealerModel>();

            var hours = MasterDataHelper.GetHours(DealerInfoHelper.DealerID, langid, hourtypeId);

            if (hours != null)
            {

                foreach (var hr in hours)
                {
                    ddm.Add(new DealerModel { DayHour = hr.ShortDayName + "  " + hr.Time });
                }
            }

            return ddm;
        }
        private void BindFooter(ServicePageDbContext db)
        {

            var location = db.Locations.FirstOrDefault(c => c.LocationTypeID == 1 && c.DealerID == DealerInfoHelper.DealerID);
            var saleHours = GetHours((int)ServiceHourType.Sales);
            var serviceHours = GetHours((int)ServiceHourType.Service);

            var footer = db.Footers.Where(x => x.DealerID == DealerInfoHelper.DealerID).Distinct().FirstOrDefault();
            var footers = new List<Footer> { footer };

            // consolidate calls
            

            rptSocial.DataSource = footers;
            rptSale.DataSource = saleHours;
            rptService.DataSource = serviceHours;
            rptService.DataBind();
            rptSale.DataBind();
            rptSocial.DataBind();
            rptStoreInfo.DataSource = new List<Location>() { location };
            rptStoreInfo.DataBind();

        }


    }
}