﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.Home.CategoryControl" %>
<%@ OutputCache Duration="300" VaryByParam="None" %>

<div class="row category mdl-top-shadow">
            <!-- Vehicle Slider  -->
            
            <ul id="category-tabs" class="nav nav-tabs siteBg">
                <asp:Repeater runat="server" ID="rptCategory">
                    <ItemTemplate>
                        <li class="vcp-nav-tab <%# Eval("Active") %>"><a class="primaryLink" href="#<%# Eval("HomePageVehicleCatoryId") %>"><%# Eval("HomePageVehicleCategoryName") %></a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <div class="row tab-content bottom-shadow">
                <asp:Repeater runat="server" ID="rptVehicleGroup" OnItemDataBound="rptVehicleGroup_OnItemDataBound">
                    <ItemTemplate>
                        <div id="<%# Eval("HomePageVehicleCatoryId") %>" class="tab-pane <%# Eval("Active") %>">
                            <asp:HiddenField runat="server" ID="hdfCategoryID" Value='<%# Eval("ID") %>' />
                            <ul class="vehicle-list">
                                <asp:Repeater runat="server" ID="rptVehicle">
                                    <ItemTemplate>
                                        <li class="vehicle">
                                            <a href="<%# Eval("VehicleRelativeUrl") %>">
                                                <div class="image vehicImg">
                                                    <img src="https://agiledealer.blob.core.windows.net/generic/dealership/brighton-ford-co-logo.jpg" data-original="<%# Eval("VehicleImage") %>" class="lazy" alt='<%# Eval("VehicleText") %>'  />
                                                </div>
                                                <div class="info">
                                                    <div class="border-top">
                                                        <h4 class="name ctaLink"><%# Eval("VehicleName") %></h4>
                                                        <!--<p class="item">
                                                            <%# Eval("VehicleText") %>
                                                        </p>
                                                        <span class="right">More Info</span>-->
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        </div>
        <!-- End Vehicle Slider  -->
