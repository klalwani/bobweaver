﻿using CJI.AgileDealer.Web.Base.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Home
{
    public partial class CategoryControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCategory();
            }
        }
        private void LoadCategory()
        {
            using (var db = new ServicePageDbContext())
            {
                var temp = db.HomePageVehicleCategorys.Where(x => x.DealerID == DealerID).ToList();

                var category = temp.Select(t => new
                {
                    t.HomePageVehicleCategoryName,
                    ID = t.HomePageVehicleCatoryId,
                    HomePageVehicleCatoryId = t.HomePageVehicleCategoryName.Replace(" ", "").Trim().ToLower() + "_" + t.HomePageVehicleCatoryId,
                    Active = CreateCategoryActive(t.HomePageVehicleCatoryId)
                }).ToList();


                rptCategory.DataSource = category;
                rptCategory.DataBind();

                rptVehicleGroup.DataSource = category;
                rptVehicleGroup.DataBind();
            }
        }
        private string CreateCategoryActive(int Id)
        {
            using (var db = new ServicePageDbContext())
            {
                if (Id == db.HomePageVehicleCategorys.Where(x => x.DealerID == DealerID).ToList()[0].HomePageVehicleCatoryId)
                {
                    return "active";
                }
                return string.Empty;
            }
        }

        protected void rptVehicleGroup_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                using (var db = new ServicePageDbContext())
                {
                    Repeater childRepeater = (Repeater)e.Item.FindControl("rptVehicle");
                    int categoryID = int.Parse(((HiddenField)e.Item.FindControl("hdfCategoryID")).Value);
                    var vehicle = db.HomePageVehicles.Where(t => t.HomePageVehicleCatoryId == categoryID).Select(t => new
                    {
                        t.HomePageVehicleId,
                        t.VehicleImage,
                        t.VehicleName,
                        VehicleRelativeUrl = string.IsNullOrEmpty(t.VehicleRelativeUrl) ? "javascript:void(0)" : t.VehicleRelativeUrl,
                        t.VehicleText
                    }).ToList();

                    childRepeater.DataSource = vehicle;
                    childRepeater.DataBind();
                }

            }
        }

    }
}