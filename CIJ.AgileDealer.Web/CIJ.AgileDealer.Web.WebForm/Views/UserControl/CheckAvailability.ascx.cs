﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Content;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using AgileDealer.Data.Entities.ScheduleService;
using CJI.AgileDealer.Web.WebForm.Utilities;
using static CIJ.AgileDealer.Web.WebForm.Common.Enum;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl
{
    [Serializable]
    public partial class CheckAvailability : BaseUserControl
    {
        public VehicleData SelectedVehicle
        {
            get
            {
                if (ViewState["SelectedVehicle"] == null)
                    return new VehicleData();
                return (VehicleData)ViewState["SelectedVehicle"];
            }
            set
            {
                ViewState["SelectedVehicle"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SelectedVehicle != null)
            {
                MapDataForSelectedVehicle(SelectedVehicle);
                MapPlaceHolderForAllFields(SelectedVehicle);

                rptScheduleTimeNormal.DataSource = ScheduleTimeNormal;
                rptScheduleTimeNormal.DataBind();

                rptScheduleTimeSaturday.DataSource = ScheduleTimeSaturday;
                rptScheduleTimeSaturday.DataBind();
            }
        }
        private List<ScheduleTime> _scheduleTimeNormal;
        private List<ScheduleTime> ScheduleTimeNormal
        {
            get
            {
                if (_scheduleTimeNormal == null)
                {
                    _scheduleTimeNormal = MasterDataHelper.GetScheduleTime(false, DealerID).Where(x => x.ScheduleTimeId <= 21).ToList();
                }

                return _scheduleTimeNormal;
            }
            set { _scheduleTimeNormal = value; }
        }

        private List<ScheduleTime> _scheduleTimeSaturday;
        private List<ScheduleTime> ScheduleTimeSaturday
        {
            get
            {
                if (_scheduleTimeSaturday == null)
                {
                    _scheduleTimeSaturday = MasterDataHelper.GetScheduleTime(true, DealerID).Where(x=>x.ScheduleTimeId<= 34).ToList();
                }

                return _scheduleTimeSaturday;
            }
            set { _scheduleTimeSaturday = value; }
        }
       
        private void MapPlaceHolderForAllFields(VehicleData selectedVehicle)
        {
            if (selectedVehicle != null)
            {
                using (ServicePageDbContext db = new ServicePageDbContext())
                {
                    Dealer dealer = db.Dealers.FirstOrDefault(x => x.DealerID == DealerID);
                    if (dealer != null)
                    {
                        VehicleInfoPage vehicleInfoPageItem = GetVehicleInfoPage(dealer, selectedVehicle);
                        if (vehicleInfoPageItem != null)
                        {
                            imgLogo.Src = vehicleInfoPageItem.Dealer.DealerLogo;

                            txtFullName.Attributes.Add("placeholder", vehicleInfoPageItem.FullNamePlaceHolder);
                            txtPhone.Attributes.Add("placeholder", vehicleInfoPageItem.PhonePlaceHolder);
                            txtEmail.Attributes.Add("placeholder", vehicleInfoPageItem.EmailPlaceHolder);
                            txtMessage.Attributes.Add("placeholder", vehicleInfoPageItem.MessagePlaceHolder);
                            txtPostalCode.Attributes.Add("placeholder", vehicleInfoPageItem.PostalcodePlaceHolder);

                            lblTitle.InnerText = vehicleInfoPageItem.TitlePlaceHolder;
                            btnSend.Value = vehicleInfoPageItem.ButtonPlaceHolder;

                            lblMessageFromDealer.InnerText = vehicleInfoPageItem.ExpectInformation;
                        }
                    }
                }
            }
        }

        private VehicleInfoPage GetVehicleInfoPage(Dealer dealer, VehicleData selectedVehicle)
        {
            VehicleInfoPage vehicleInfoPageItem = null;
            using (ServicePageDbContext db = new ServicePageDbContext())
            {
                vehicleInfoPageItem =
                    db.VehicleInfoPages.Include("Dealer").FirstOrDefault(
                        x => x.DealerID == dealer.DealerID && x.IsNew == selectedVehicle.IsNew);
            }
            return vehicleInfoPageItem;
        }

        private void MapDataForSelectedVehicle(VehicleData selectedVehicle)
        {
            if (selectedVehicle != null)
            {
                hdfAgileVehicleId.Value = selectedVehicle.AgileVehicleId.ToString();
                imgVehicle.Src = SelectedVehicle.ImageFile;

                lblCarName.Text = $"{SelectedVehicle.Year} {SelectedVehicle.Model} {SelectedVehicle.Trim}";

                lblExtColor.Text = SelectedVehicle.ExtColor;
                lblVin.Text = SelectedVehicle.Vin;
                lblEngine.Text = SelectedVehicle.EngDescription;
                lblDrive.Text = SelectedVehicle.DriveTrain;
            }
        }

        private void ResetAllField()
        {

        }
    }
}