﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Applicants;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.ScheduleService
{
    public partial class CompleteInfo : BaseUserControl
    {
        private const int PageIndex = 4;
        private List<NotificationModel> _notifications;
        private List<NotificationModel> Notifications
        {
            get
            {
                if (_notifications == null)
                {
                    _notifications = MasterDataHelper.GetNotifications(PageIndex,LanguageId, DealerID);
                }

                return _notifications;
            }
            set { _notifications = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindingMasterData();
        }

        private void BindingMasterData()
        {
            //Notifications
            rptNotification.DataSource = Notifications;
            rptNotification.DataBind();
        }
    }
}