﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl
{
    public partial class ServicePageControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new ServicePageDbContext())
            {
                Title = EnumHelper.GetDescriptionFromEnumValue(ServiceType);

                //var services = db.Services.Include(c => c.ServiceType)
                //    .Where(c => c.ServiceType.Name.Equals(Title, StringComparison.OrdinalIgnoreCase)).ToList();
                var serviceQuery = from service in db.Services
                                   join serviceType in db.ServiceTypes on service.ServiceTypeID equals serviceType.ServiceTypeID
                                   join service_Trans in db.Service_Trans.Where(c=>c.LanguageId == LanguageId) on service.ServiceID equals service_Trans.ServiceId into tbl_Trans
                                   from tbl in tbl_Trans.DefaultIfEmpty()
                                   where serviceType.Name.ToLower() == Title.ToLower() && service.DealerID == DealerID
                                   select new ServiceModel
                                   {
                                       Title = tbl.Title ?? service.Title,
                                       Description = tbl.Description ?? service.Description,
                                       Disclaimer = tbl.Disclaimer ?? service.Disclaimer,
                                       Price = service.Price,
                                       Image = service.Image // skip localization for image as it is generic not language specific
                                   };
                var services = serviceQuery.ToList();
                //var specials =db.Specials.Include(c => c.ServiceType).
                //    Where(c => c.ServiceType.Name.Equals(Title, StringComparison.OrdinalIgnoreCase)).ToList();
                var specialQuery = from special in db.Specials
                                   join serviceType in db.ServiceTypes on special.ServiceTypeID equals serviceType.ServiceTypeID
                                   join special_Trans in db.Special_Trans.Where(c => c.LanguageId == LanguageId) 
                                   on special.SpecialID equals special_Trans.SpecialId into tbl_Trans
                                   from tbl in tbl_Trans.DefaultIfEmpty()
                                   where serviceType.Name.ToLower() == Title.ToLower() && special.DealerID == DealerID
                                   select new ServiceModel
                                   {
                                       Title = tbl.Title ?? special.Title,
                                       Description = tbl.Description ?? special.Description,
                                       Disclaimer = tbl.Disclaimer ?? special.Disclaimer,
                                       Price = special.Price,
                                       Image = tbl.Image ?? special.Image
                                   };
                var specials = specialQuery.ToList();

                var display = db.DisplayPages.Include(a => a.Dealer).SingleOrDefault(x=>x.DealerID == DealerID);
                display.Dealer.Locations = new List<Location>();
                display.Dealer.Locations.Add(db.Locations.Where(a => a.DealerID == display.DealerID).FirstOrDefault());
                display.Dealer.Manufacture = db.Manufactures.SingleOrDefault(a => a.ManufactureID == display.Dealer.ManufactureID && a.DealerID == DealerID);

                servicePageControl.DataSource = new List<DisplayPage> { display };
                servicePageControl.DataBind();

                var rpterService = servicePageControl.FindControl("rpterService") as Repeater;
                if (rpterService != null)
                {
                    rpterService.DataSource = services.ToList();
                    rpterService.DataBind();
                }
                var rpterSpecial = servicePageControl.FindControl("rpterSpecial") as Repeater;
                if (rpterSpecial != null)
                {
                    rpterSpecial.DataSource = specials.ToList();
                    rpterSpecial.DataBind();
                }
            }
        }

        public string Title { get; set; }

        public ServiceTypeEnum ServiceType { get; set; }

        protected string ConvertDecimalToString(object value)
        {
            return String.Format("{0:C}", value);
        }
    }
}