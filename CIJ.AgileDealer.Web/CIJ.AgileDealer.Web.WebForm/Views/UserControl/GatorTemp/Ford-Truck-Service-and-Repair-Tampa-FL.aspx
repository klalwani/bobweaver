﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Ford-Truck-Service-and-Repair-Tampa-FL.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.GatorTemp.Ford_Truck_Service_and_Repair_Tampa_FL" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


<style>
#gator-rv-cards {
    clear: both;
}

#gator-rv-cards ul {
  border-top: 1px solid #b3b3b3;
  float: left;
  background-color: #EEE;
  color: #FFF;
  margin: 1%;
  padding: 10px 0;
  font-family: fordngbs-antenna-bold,Arial,sans-serif;
  border-radius: 3px;
  -webkit-box-shadow: 0 1px 2px #777;
  -moz-box-shadow: 0 2px 1px #777;
  box-shadow: 0 2px 1px #777;
}

#gator-rv-cards ul span {
  text-align: center;
  border-radius: 2px 2px 2px 2px;
  display: inline-block;
  width: 100%;
  font-weight: bold;
  border-radius: 0;
  font-size: 16px;
  background-color: #2d96cd;
  margin: 0;
  margin-bottom: 5px;
  padding: 7px 0;
  text-shadow: 1px 1px 1px #000;
  -webkit-box-shadow: 1px 1px 1px #777;
  -moz-box-shadow: 1px 1px 1px #777;
  box-shadow: 1px 1px 1px #777;
}


#gator-rv-cards li {
  list-style: none;
  color: #7F7F7F;
  margin-left: 20%;
  display: block;
  letter-spacing: .01em;
  word-spacing: .05em;
  color: #111;
}

.rv-outer-rows {
  clear: both;
  display: inline;
}

.rv-outer-rows ul {
  width: 23%;
}


.rv-inner-rows {
   display: inline;
}

.rv-inner-rows ul {
  width: 18%;
}

#rv-menu {
  height: 145px;
}

#rv-menu-start {
  padding: 30px 25px; 
  margin-top: 15px; 
  border-right: 2px solid #000;
}

#rv-menu-start span {
  display: block;
  color: #000;
  font-size: 16px;
  line-height: 24px;
  text-shadow: .25px .5px 1px rgba(0, 0, 0, 0.4);
}

.rv-size-menu {
  float: left; 
  text-align: center; 
}

.rv-size-menu a {
  text-decoration: none;
}

.rv-menu-image {
  height: 90px; 
  margin: 12px 10px;
}

.rv-title-class {
  display: block;
  color: #000;
  font-weight: 800;
  font-size: 24px;
  line-height: 24px;
  text-shadow: .25px .5px 1px rgba(0, 0, 0, 0.4);
}

.rv-title-weight {
  display: block;
  color: #999;
  font-size: 14px;
  line-height: 24px;
  text-shadow: .25px .25px 0px rgba(0, 0, 0, 0.4);
}

</style>


    <div class="container">

        <div id="rv-menu">

            <div class="rv-size-menu">
                <a href=""><div id="rv-menu-start"><span>Truck Size</span></div></a>
            </div>

            <div class="rv-size-menu">
                <a href="#light_duty">
                    <div><img class="rv-menu-image" src="http://storage.googleapis.com/www.savvydealer.com/public/type_thumbs/Light-Duty-Truck-Thumb.jpg" alt="Light Duty Truck"></div>
                    <span class="rv-title-class">Light Duty</span>
                    <span class="rv-title-weight"> <10k GVW</span>
                </a>
            </div>

            <div class="rv-size-menu">
                <a href="#medium_duty">
                    <div><img class="rv-menu-image" src="http://storage.googleapis.com/www.savvydealer.com/public/type_thumbs/Medium-Duty-Truck-Thumb.jpg" alt="Medium Duty Truck"></div>
                    <span class="rv-title-class">Medium Duty</span>
                    <span class="rv-title-weight">10k - 26k GVW</span>
                </a>
            </div>

            <div class="rv-size-menu">
                <a href="#heavy_duty">
                    <div><img class="rv-menu-image" src="http://storage.googleapis.com/www.savvydealer.com/public/type_thumbs/Heavy-Duty-Truck-Thumb.jpg" alt="Light Duty Truck"></div>
                    <span class="rv-title-class">Heavy Duty</span>
                    <span class="rv-title-weight">26k GVW+</span>
                </a>
            </div>

            <div class="rv-size-menu">
                <a href="#rv">
                    <div><img class="rv-menu-image" src="http://storage.googleapis.com/www.savvydealer.com/public/type_thumbs/RV-Thumb.jpg" alt="RV Truck"></div>
                    <span class="rv-title-class">RV</span>
                    <span class="rv-title-weight">All Sizes</span>
                </a>
            </div>

        </div>



        <div id="gator-rv-cards">
            <div class="rv-outer-rows">
            <ul><span>Preventative Maintenance</span>
	            <li>Oil &amp; Filter Change</li>
	            <li>Chassis Lube</li>
	            <li>Transmission Service</li>
	            <li>Filter Replacement</li>
	            <li>Check Fluids</li>
	            <li>Battery Service</li>
	            <li>Multi-Point Vehicle Inspection</li>
	            <li>Wiper Blades</li>
	            <li>Pack Wheel Bearings</li>
	            <li>Brake Adjustments</li>
	            <li>Clutch Adjustments</li>
            </ul>

            <ul><span>Diesel Engines</span>
	            <li>Diagnosis</li>
	            <li>In-Frame Overhaul</li>
	            <li>Out of Frame Overhaul</li>
	            <li>Main and Rod Bearings</li>
	            <li>Tune-Ups</li>
	            <li>Fuel Injection Service</li>
	            <li>Oil Cooler Service</li>
	            <li>Glow Plug Service</li>
	            <li>Liner &amp; Piston Replacement</li>
	            <li>Brake Adjustments</li>
	            <li>Counter Bores </li>
            </ul>

            <ul><span>Trailer</span>
	            <li>Roll-up Door Repair</li>
	            <li>Hinged Door Repair</li>
	            <li>Landing Leg Service</li>
	            <li>Body Panels</li>
	            <li>Refrigeration</li>
	            <li>Suspension Repair</li>
	            <li>Alignment Tracking</li>
	            <li>Lighting</li>
	            <li>Air Brakes</li>
	            <li>Electric Brakes</li>
	            <li>Frame Repair </li>
            </ul>

            <ul><span>Suspension</span>
	            <li>Diagnosis</li>
	            <li>Shocks/Struts</li>
	            <li>Ball Joints</li>
	            <li>Tie Rod Ends</li>
	            <li>Drag Links</li>
	            <li>Sway Bars / Springs</li>
	            <li>Air Ride Systems</li>
	            <li>Steering Racks/Boxes</li>
	            <li>Tires/Wheels</li>
	            <li>Alignment</li>
	            <li>Control Arms</li>
            </ul>
            <div style="clear: both;"></div>
            </div>

            <div class="rv-inner-rows">
            <ul><span>Cooling &amp; Heating</span>
	            <li>Diagnosis</li>
	            <li>Radiators</li>
	            <li>Belts &amp; Hoses</li>
	            <li>Thermostats</li>
	            <li>Head Gaskets</li>
	            <li>Water Pump</li>
	            <li>Air Conditioning Service</li>
	            <li>Flush &amp; Fill</li>
	            <li>R134A Conversions</li>
            </ul>

            <ul><span>Drive Line</span>
	            <li>Clutches</li>
	            <li>U-Joints</li>
	            <li>Drive Shafts</li>
	            <li>Transmissions</li>
	            <li>Differentials</li>
	            <li>Hanger Bearings</li>
	            <li>Engine Mounts</li>
	            <li>Flywheel Machining</li>
            </ul>

            <ul><span>Electrical</span>
	            <li>Diagnosis</li>
	            <li>Alternators</li>
	            <li>Starters</li>
	            <li>Batteries</li>
	            <li>Cables</li>
	            <li>Isolators</li>
	            <li>Parallel Switches</li>
	            <li>Lights / Wiring</li>
	            <li>Power Accessories</li>
            </ul>

            <ul><span>Brakes</span>
	            <li>Diagnosis</li>
	            <li>Pad/Shoe Replacement</li>
	            <li>Anti-Lock Brake Service</li>
	            <li>Emergency Brakes</li>
	            <li>Rotor/Drum Machining</li>
	            <li>Power Boosters</li>
	            <li>Master Cylinders</li>
	            <li>Wheel Cylinders</li>
            </ul>

            <ul><span>Exhaust</span>
	            <li>Diagnosis</li>
	            <li>Muffler</li>
	            <li>Pipes</li>
	            <li>Turbo Chargers</li>
	            <li>Clamps &amp; Hangers</li>
	            <li>Exhaust Manifolds</li>
	            <li>Catalytic Converters</li>
	            <li>Stacks</li>
	            <li>Rain Caps</li>
            </ul>
            <div style="clear: both;"></div>
            </div>

            <div class="rv-outer-rows">
            <ul><span>Drivability</span>
	            <li>Diagnosis</li>
	            <li>Engine &amp; 4 Gas Analyzer</li>
	            <li>Scanner Hook-Up</li>
	            <li>Computer</li>
	            <li>Prom Updates</li>
            </ul>

            <ul><span>Metal Repair</span>
	            <li>Fabrication</li>
	            <li>Cutting</li>
	            <li>Plasma Arc</li>
	            <li>Brazing</li>
	            <li>Drilling</li>
	            <li>Tapping</li>
	            <li>Heli-Coil</li>
	            <li>Mig/Tig/ Gas &amp; Stick Welding</li>
	            <li>Aluminum Welding</li>
            </ul>

            <ul><span>Fuel</span>
	            <li>Diagnosis</li>
	            <li>Carburetors</li>
	            <li>TBI Fuel Injection</li>
	            <li>MPI Fuel Injection</li>
	            <li>Fuel Pumps</li>
	            <li>Fuel Tanks</li>
	            <li>Fuel Gauges</li>
	            <li>Fuel Lines</li>
            </ul>

            <ul><span>Other Services</span>
	            <li>Road Service</li>
	            <li>24 Hour Towing</li>
	            <li>Hot Starts</li>
	            <li>Glass Replacement</li>
	            <li>Fifth Wheels</li>
	            <li>Lift Gate Repair</li>
	            <li>Hydraulic Hose</li>
            </ul>
            <div style="clear: both;"></div>
            </div>

        </div>





    </div>


</asp:Content>