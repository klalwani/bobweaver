﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicePageControl.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.ServicePageControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls" %>
<div id="services-page" class="">
    <asp:FormView runat="server" ID="servicePageControl" DefaultMode="ReadOnly" DataKeyNames="DisplayPageID" RenderOuterTable="false">
        <InsertItemTemplate></InsertItemTemplate>
        <EditItemTemplate></EditItemTemplate>
        <ItemTemplate>
            <div class="svc-top-container row mdl-bottom-shadow">
                <div class="inner-container">
                    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
                        <h1><%#HttpUtility.HtmlDecode((string)Eval("Dealer.Locations[0].City"))%>&nbsp;<%#HttpUtility.HtmlDecode((string)Eval("Dealer.Manufacture.Name"))%>&nbsp;<%#Title%></h1>
                        <%--<span><%#HttpUtility.HtmlDecode((string)Eval("MainContent1"))%></span>
                        <span><%#HttpUtility.HtmlDecode((string)Eval("MainContent2"))%></span>
                        <span><%#HttpUtility.HtmlDecode((string)Eval("MainContent3"))%></span>--%>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
                        <div class="svc_header">
                            <h5><%=ServicePageControl.Svc_Header_H5 %></h5>
                            <a href="/service/appointment"><i class="glyphicon glyphicon-calendar"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="clear-20"></div>
                <div class="row">
                    <div class="col-md-7">
                        <asp:Repeater runat="server" ID="rpterService">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-4">
                                        <img runat="server" src='<%#Eval("Image") %>' alt="" />
                                    </div>
                                    <div class="col-md-8">
                                        <h3><%#HttpUtility.HtmlDecode((string)Eval("Title"))%></h3>
                                        <p><%#HttpUtility.HtmlDecode((string)Eval("Description"))%></p>
                                        <p><%#HttpUtility.HtmlDecode((string)Eval("Disclaimer"))%></p>
                                        <h4><%#ConvertDecimalToString(Eval("Price"))%></h4>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <asp:Repeater runat="server" ID="rpterSpecial">
                            <ItemTemplate>
                                <div class="row coupon-stitch">
                                    <div class="col-md-4">
                                        <img runat="server" src='<%#Eval("Image") %>' alt="" />
                                    </div>
                                    <div class="col-md-8">
                                        <h3><%#HttpUtility.HtmlDecode((string)Eval("Title"))%></h3>
                                        <%--<p><%#HttpUtility.HtmlDecode((string)Eval("Description"))%></p>--%>
                                        <p><%#HttpUtility.HtmlDecode((string)Eval("Disclaimer"))%></p>
                                        <h4><%#ConvertDecimalToString(Eval("Price"))%></h4>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
</div>
