﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.Base.Context;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
using CJI.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CJI.AgileDealer.Web.WebForm.Models;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl
{
    public partial class AboutUs : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAboutUs();
                LoadDealerInfo();
            }
        }

        void LoadAboutUs()
        {
            using (var db = new ServicePageDbContext())
            {
                var query = from aboutUS in db.AboutUses.Where(x=>x.DealerID == DealerID)
                            join aboutUS_Tran in db.AboutUs_Trans.Where(c => c.LanguageId == LanguageId)
                            on aboutUS.AboutUsId equals aboutUS_Tran.AboutUsId into tblTrans
                            from tbl in tblTrans.DefaultIfEmpty()
                            select new AboutUsModel()
                            {
                                Title = tbl.Title ?? aboutUS.Title,
                                Content = tbl.Content ?? aboutUS.Content
                            };
                var aboutus = query.FirstOrDefault();
                if (aboutus != null)
                {
                    ltrTitle.Text = aboutus.Title;
                    ltrContent.Text = aboutus.Content;
                }
            }
        }

        void LoadDealerInfo()
        {
            using (var db = new ServicePageDbContext())
            {
                var dealer = db.Dealers.FirstOrDefault(x=>x.DealerID == DealerID);
                if (dealer != null)
                {
                    ltrName.Text = dealer.Name;

                    var location = db.Locations.FirstOrDefault(l => l.DealerID == dealer.DealerID);
                    if (location != null)
                    {
                        ltrAddress1.Text = location.Street1;
                        ltrAddress2.Text = location.Street2;
                        ltrPhone.Text = location.MainPhone;
                    }

                    LoadHour(dealer.DealerID);
                }
            }
        }

        void LoadHour(int dealerId)
        {
            GetHours(dealerId, (int)Enum.ServiceHourType.Service, rptServicesHours);
            GetHours(dealerId, (int)Enum.ServiceHourType.Sales, rptSalesHours);
            GetHours(dealerId, (int)Enum.ServiceHourType.Parts, rptPartHours);
        }

        void GetHours(int dealerId, int type, Repeater rpt)
        {
            var result = MasterDataHelper.GetHours(dealerId, LanguageId, type);
            if (result != null)
            {
                rpt.DataSource = result.ToList();
                rpt.DataBind();
            }
        }
    }
}