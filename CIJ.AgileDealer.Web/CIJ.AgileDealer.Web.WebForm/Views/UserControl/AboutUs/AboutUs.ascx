﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutUs.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.AboutUs" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.AboutUs" %>
<div id="about-us-page" class="container">
    <section class="section">
        <div class="section-inner">
            <div class="inner">
                <h1 class="title">
                    <asp:Literal runat="server" ID="ltrTitle" />
                </h1>
                <div class="custompage col-lg-9">
                    <asp:Literal runat="server" ID="ltrContent" />
                </div>
                <div class="sidebar col-lg-2">
                    <h2 class="header"><%=AboutUs.Col_Lg_2_Header %></h2>
                    <div class="address">
                        <div>
                            <span class="dealerinfoname">
                                <strong>
                                    <asp:Literal runat="server" ID="ltrName" />
                                </strong>
                            </span>
                        </div>
                        <div>
                            <span class="dealerinfoaddress">
                                <asp:Literal runat="server" ID="ltrAddress1" />
                            </span>
                        </div>
                        <div>
                            <span class="dealerinfoaddress2">
                                <asp:Literal runat="server" ID="ltrAddress2" />
                            </span>
                        </div>
                        <div>
                            <span class="dealerinfophonelabel"><%=AboutUs.DealerInfoPhoneLabel %></span>
                            <span class="dealerinfophone">
                                <span class="dynamic-phone-number">
                                    <asp:Literal runat="server" ID="ltrPhone" />
                                </span>
                            </span>
                        </div>
                    </div>
                    <h3 class="title"><%=AboutUs.Title %></h3>

                    <asp:Repeater runat="server" ID="rptSalesHours">
                        <HeaderTemplate>
                            <table class="hourstable">
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("ShortDayName") %></td>
                                <td>
                                    <span><%#Eval("Time") %></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <h3 class="title"><%=AboutUs.Title_1 %></h3>

                    <asp:Repeater runat="server" ID="rptServicesHours">
                        <HeaderTemplate>
                            <table class="hourstable">
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("ShortDayName") %></td>
                                <td>
                                    <span><%#Eval("Time") %></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <h3 class="title"><%=AboutUs.Title_2 %></h3>

                    <asp:Repeater runat="server" ID="rptPartHours">
                        <HeaderTemplate>
                            <table class="hourstable">
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("ShortDayName") %></td>
                                <td>
                                    <span><%#Eval("Time") %></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="buttons">
                        <a href='Contactus'>
                            <label class='btn btn-primary'><%=AboutUs.Btn_Primary_1 %></label></a>
                        <a href='Contactus'>
                            <label class='btn btn-primary'><%=AboutUs.Btn_Primary_2 %></label></a>
                        <a href='Contactus'>
                            <label class='btn btn-primary'><%=AboutUs.Btn_Primary_2 %></label></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
