﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.Inventory
{
    public partial class Inventory : System.Web.UI.UserControl
    {
        public string DealerCity { get; set; }
        public string DealerState { get; set; }
        public string TypeString { get; set; }

        private DefaultFilter _defaultFilter;
        public DefaultFilter DefaultFilter
        {
            get
            {
                if (_defaultFilter == null)
                    return new DefaultFilter();
                return _defaultFilter;
            }
            set
            {
                _defaultFilter = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DefaultFilter filters = new DefaultFilter
            {
                DefaultTypes = DefaultFilter.DefaultTypes,
                DefaultModels = DefaultFilter.DefaultModels,
                DefaultBodyStyleTypes = DefaultFilter.DefaultBodyStyleTypes,
                IsExpandType = DefaultFilter.IsExpandType,
                IsExpandModel = DefaultFilter.IsExpandModel,
                IsExpandBodyStyle = DefaultFilter.IsExpandBodyStyle,
                IsLifted = DefaultFilter.IsLifted,
                IsUpfitted = DefaultFilter.IsUpfitted,
                DefaultMakes = DefaultFilter.DefaultMakes,
                DefaultModelTrims = DefaultFilter.DefaultModelTrims,
                InLitmitedMode = DefaultFilter.InLitmitedMode,
                IsRentalSpecial= DefaultFilter.IsRentalSpecial
            };
            SearchControl.DefaultFilter = filters;
        }
    }
}