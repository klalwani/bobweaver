﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.Ajax.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.RebateControl
{
    public partial class RebateControl : BaseUserControl
    {
        public VehicleData SelectedVehicle
        {
            get
            {
                if (ViewState["SelectedVehicle"] == null)
                    return new VehicleData();
                return (VehicleData)ViewState["SelectedVehicle"];
            }
            set
            {
                ViewState["SelectedVehicle"] = value;
            }
        }

        public List<VehicleData> Images
        {
            get
            {
                if (ViewState["Images"] == null)
                    return new List<VehicleData>();
                return (List<VehicleData>)ViewState["Images"];
            }
            set
            {
                ViewState["Images"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SelectedVehicle != null)
            {
                //List<Program> programs = VehicleHelper.GetProgramOfSpecificVehicle(SelectedVehicle.AgileVehicleId);
                //string imageUrl = string.Empty;
                //var firstOrDefault = Images.FirstOrDefault(x => x.ImageOrder == 1);
                //if (firstOrDefault != null)
                //{
                //    imageUrl = firstOrDefault.ImageFile;
                //}
                //if (programs.Any())
                //{
                //    List<string> programNames = programs.Where(x => x.Type == ProgramTypeEnum.Cash.ToString()).Select(x => x.Name).Distinct().ToList();
                //    List<VehicleProgramData> programData = new List<VehicleProgramData>();
                //    List<VehicleProgramData> aprAndLeaseData = new List<VehicleProgramData>();
                //    foreach (string name in programNames)
                //    {
                //        VehicleProgramData item =
                //            programs.Where(x => x.Name == name).OrderByDescending(o => o.Amount).Select(a =>
                //                new VehicleProgramData
                //                {
                //                    ProgramName = a.Name,
                //                    Amount = Math.Abs(decimal.Parse(a.Amount)).ToString(CultureInfo.InvariantCulture),
                //                    ImageUrl = imageUrl,
                //                    Disclaimer = a.Disclaimer
                //                }).FirstOrDefault();
                //        if (item != null)
                //        {
                //            programData.Add(item);
                //        }
                //    }


                    //rptProgram.DataSource = programData;
                    //rptProgram.DataBind();

                    //List<Program> aprAndLeasePrograms = programs.Where(x => x.Type == ProgramTypeEnum.APR.ToString() || x.Type == ProgramTypeEnum.Lease.ToString()).DistinctBy(a=>a.Id).ToList();
                    //foreach (Program item in aprAndLeasePrograms)
                    //{
                    //    if (item.Type == ProgramTypeEnum.APR.ToString())
                    //    {
                    //        item.AprTermList = VehicleHelper.GetAprTermListForSpecificProgram(item);
                    //        if (item.AprTermList != null)
                    //        {
                    //            if (item.AprTermList.AprTerms.Any())
                    //            {
                    //                VehicleProgramData newItem = new VehicleProgramData();
                    //                newItem.ProgramName = item.Name;
                    //                newItem.Type = item.Type;
                    //                newItem.Disclaimer = item.Disclaimer;
                    //                string amountFormated = string.Empty;
                    //                foreach (AprTerm apr in item.AprTermList.AprTerms)
                    //                {
                    //                    string amount = Math.Round(double.Parse(apr.Apr), 3).ToString();
                    //                    if (amount == "0")
                    //                        amount = "0.0";
                    //                    amountFormated = $"{amountFormated}{amount}% for {apr.Term} mo</br>";
                    //                }
                    //                newItem.Amount = amountFormated;
                    //                aprAndLeaseData.Add(newItem);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            VehicleProgramData newItem = new VehicleProgramData();
                    //            newItem.ProgramName = item.Name;
                    //            newItem.Amount = Math.Abs(decimal.Parse(item.Amount)).ToString(CultureInfo.InvariantCulture);
                    //            newItem.ImageUrl = string.Empty;
                    //            newItem.Disclaimer = item.Disclaimer;
                    //            newItem.Type = item.Type;
                    //            aprAndLeaseData.Add(newItem);
                    //        }
                    //    }
                        //if (item.Type == ProgramTypeEnum.Lease.ToString())
                        //{
                        //    item.LeaseTermList = VehicleHelper.GetLeaseTermListForSpecificProgram(item);
                        //    if (item.LeaseTermList.LeaseTerms.Any())
                        //    {
                        //        VehicleProgramData newItem = new VehicleProgramData();
                        //        newItem.ProgramName = item.Name;
                        //        newItem.Type = item.Type;
                        //        newItem.Disclaimer = item.Disclaimer;
                        //        string amountFormated = string.Empty;
                        //        foreach (LeaseTerm apr in item.LeaseTermList.LeaseTerms)
                        //        {
                        //            string amount = Math.Round(double.Parse(apr.Apr), 3).ToString();
                        //            if (amount == "0")
                        //                amount = "0.0";
                        //            amountFormated = $"{amountFormated}{amount}% for {apr.Term} mo</br>";
                        //        }
                        //        newItem.Amount = amountFormated;
                        //        aprAndLeaseData.Add(newItem);
                        //    }
                        //}
                        //lblOrApr.Visible = aprAndLeaseData.Any(x => x.Type == ProgramTypeEnum.APR.ToString());
                        //rptApr.DataSource = aprAndLeaseData.Where(x => x.Type == ProgramTypeEnum.APR.ToString());
                        //rptApr.DataBind();

                        //rptLease.DataSource = aprAndLeaseData.Where(x => x.Type == ProgramTypeEnum.Lease.ToString());
                        //rptLease.DataBind();
                    }
                //}
            //}
        }
    }
}