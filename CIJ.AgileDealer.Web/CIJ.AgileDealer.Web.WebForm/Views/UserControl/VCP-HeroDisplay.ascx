﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VCP-HeroDisplay.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.VCP_HeroDisplay" %>


<style>/* VCP Style Info */
body {
  background-color: #eee;
  background: radial-gradient(white, #eee, white);
}

.vcp-hero {
  list-style: none;
  padding-top: 36px;
}

.vcp-hero li {
  text-align: left;
  color: #264b68;  
  font-weight: 500;
  font-size: 24px;

}

.vcp-hero-small {
  font-size: 16px !important;
  color: #91a4b1 !important;
  padding-top: 30px;
}


.vcp-main-image {
  width: 80%;
}

.vcp-nav h4 {
  line-height: 1.1;
  font-size: 30px;
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-weight: 500;    
}


.vcp-nav p {
  clear: both;
  display: block;
  padding: 10px 15px 0px 15px;
  font-size: 16px;
}

.vcp-nav div {
  text-align: center;
}


.vcp-tabs {
  min-height: 600px;
  margin-top: 30px;
}


#tabs {
  height: 40px !important;
  font-size: 24px;
}

#tabs li.active a {
  background-color: #2d96cd;
  color: #fff;
  border-radius: 0;
  border-bottom: 1px solid #2d96cd;
}

.tab-pane {
  padding: 30px;
}

div.vcp-cta:hover, div.vcp-default:hover {
  background-color: #264b68;
  color: #fff;
}

.vcp-cta {
  height: 50px;  
  color: #fff;
  background-color: #2d96cd;
}

.vcp-default {
  height: 50px;  
  background-color: #fff;
  border-bottom: 1px solid #ddd;
}


.mdl-bottom-shadow {
  background-color: #fff;
    -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
     -moz-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
          box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
}

.clear {
  clear: both;
  height: 0px;
}

</style>


<div id="vcp-ford-f-150" class="vcp">
  <div class="container">

      <div class="row vcp-nav">
          <h1>New Ford F-150 <small> in <%= DealerCity %> for Sale and Lease</small></h1>                        
          
          <div class="mdl-bottom-shadow">                  
          <a href="/vcp/ford-new-f-150"><div class="col-md-4 vcp-cta"><h4>New</h4></div></a>
          <a href="#"><div class="col-md-4 vcp-default"><h4>Certified</h4></div></a>
          <a href="/vcp/ford-used-f-150"><div class="col-md-4 vcp-default"><h4>Used</h4></div></a>
          <div class="clear"></div>              
            <div class="row">
            <div class="col-md-4">
              <ul class="vcp-hero">
                <li class="vcp-hero-small">Starting MSRP</li>
                <li>$26,428</li>
                <li class="vcp-hero-small">EPA-Est MPG</li>
                <li><i class="fa fa-building-o"></i> 18  | 25 <i class="fa fa-road"></i></li>
                <li class="vcp-hero-small">Max when properly equipped</li>
                <li><i class="fa fa-download"></i> 1,920 lbs Payload</li>
                <li><i class="fa fa-truck"></i> 5,100 lbs Towing</li>
              </ul>
              
              
            </div>
            <div class="col-md-8"><img class="vcp-main-image" alt="" 
                                       src="<%= DealerImageGeneric %>content/hero/Ford-F-150-XL.png">
              <p>When F-150 added high-strength, military-grade, aluminum alloy, it got stronger, 
             more capable and more efficient than ever. It also got some very prestigious awards.</p></div>
            </div>
          <div class="clear"></div>              
          </div>        
          
  
      </div>        
    
      </div>
</div>