﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffView.ascx.cs" Inherits="CIJ.AgileDealer.Web.WebForm.Views.UserControl.StaffView" %>
<%@ Register TagPrefix="staffEditControl" TagName="StaffEditControl" Src="~/Views/UserControl/Admin/StaffEditControl.ascx" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.Common" %>
<script src="../../Scripts/Pages/AgileDealer.Staff.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<input type="hidden" data-attribute="validation" name="ButtonYes" value="<%=StaffView.ButtonYes %>" />
<input type="hidden" data-attribute="validation" name="ButtonNo" value="<%=StaffView.ButtonNo %>" />
<input type="hidden" data-attribute="validation" name="EmployeeUpdateTitle" value="<%=StaffView.EmployeeUpdateTitle %>" />
<input type="hidden" data-attribute="validation" name="UpdateFailed" value="<%=StaffView.UpdateFailed %>" />
<input type="hidden" data-attribute="validation" name="ErrorMessage" value="<%=Common.ErrorMessage %>" />
<input type="hidden" value="<%=DepartmentId %>" id="departmentId" />
<section id="staff" class="container">
    <div class="row">
        <div class="col-lg-6">
            <h1><%=StaffView.Col_Lg_4_H1 %></h1>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <asp:Repeater runat="server" ID="rptDepartment">
                <HeaderTemplate>
                    <ul id="staff-tabs" class="nav nav-tabs">
                </HeaderTemplate>
                <ItemTemplate>
                    <li id="<%#Eval("DepartmentId") %>" class="primaryBg"><a href="<%#Eval("Url") %>"><%# Eval("Name") %></a></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
            <div class="row tab-content mdl-box-shadow">
                <div class="tab-pane active">
                    <div class="row-fluid">
                        <asp:Repeater ID="rptStaff" runat="server">
                            <ItemTemplate>

                                <div class="col-md-3 col-sm-4 col-xs-6 staff-card">
                                    <asp:HiddenField runat="server" ID="hdfIDStaff" Value='<%# Eval("StaffID") %>' />
                                    <div class="mdl-box-shadow">
                                        <a href="mailto:<%# Eval("Email") %>" title="<%=StaffView.All_Mdl_Box_Shadow_A_Title_1 %>">
                                            <div class="mail ctaBg">
                                                <span class="glyphicon glyphicon-envelope ctaBg"></span>
                                                <span style="display: none">email</span>
                                            </div>
                                        </a>
                                        <a href="" id='<%# Eval("StaffID") %>' class="adminSection editEmployee" 
                                            title="<%=StaffView.All_Mdl_Box_Shadow_A_Title_2 %>">
                                            <div class="edit"><span style="display: none">edit</span></div>
                                        </a>
                                        <a href="" title="<%=StaffView.All_Mdl_Box_Shadow_A_Title_3 %>" class="adminSection">
                                            <div class="delete" id="<%# Eval("StaffID") %>"><span style="display: none">delete</span></div>
                                        </a>
                                        <img alt="<%# Eval("StaffID") %>" <%--class='<%# Eval("IsHasAvata") %>'--%> src='<%# Eval("Avata") %>' />
                                    </div>
                                    <div class="mdl-box-shadow detail">
                                        <p class="name"><%# Eval("StaffName") %></p>
                                        <p class="role"><%# Eval("StaffRole") %></p>
                                        <p class="role"><%# Eval("Phone") %></p>
                                    </div>
                                </div>

                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clear-20"></div>
<asp:HiddenField runat="server" ID="CanBeEdit" />
<script>
    var departmentId = $('#departmentId').val();
    $('#staff-tabs li').each(function () {
        $(this).removeClass("active");
    });
    $('#staff-tabs li').each(function () {
        var id = $(this).attr("id");
        if (departmentId == id) {
            $(this).addClass("active");
            return false;
        }
    });
</script>

<div class="hidden">
    <div id="employee-update" class="col-xs-12">
        <staffEditControl:StaffEditControl runat="server" ID="StaffEditControl" />
    </div>
</div>
<div class="hidden">
    <div id="dialog-confirm" title="<%=StaffView.Dialog_Confirm_Title %>">
        <p>
            <span class="ui-icon ui-icon-alert"></span><%=StaffView.Ui_Icon_Ui_Icon_Alert %>
        </p>
    </div>
</div>
