﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.CommonControls
{
    public partial class MessagePage : BaseUserControl
    {
        private string _content;
        public string Content
        {
            get
            {
                if (_content == null)
                {
                    _content = string.Empty;
                }

                return _content;
            }
            set { _content = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MessageContent.InnerText = Content;
        }
    }
}