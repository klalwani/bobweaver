﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Buyer;
using AgileDealer.Data.Entities.Content;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Controllers;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.AspNet.SignalR;
using AgileDealer.Data.Entities.Incentives;
using AgileDealer.Data.Entities.Vehicles;
namespace CJI.AgileDealer.Web.WebForm.Views.UserControl.VehicleInfo
{
    public partial class VehicleInfo : BaseUserControl
    {
        public string Vin { get; set; }
        public int AgileVehicleId { get; set; }
        public int IncentiveDisplayTypeId { get; set; }
        public int ModelTranslatedId { get; set; }
        public bool IsNew { get; set; }
        public string VehicleImage { get; set; }
        public bool IsDisplayOfferDetail { get; set; }
        public bool IsUpfitted { get; set; }
        public Dealer Dealer { get; set; }
        public delegate void SendMessageToThePageHandler(int dealerId);
        public event SendMessageToThePageHandler SendMessageToThePage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Vin))
                Vin = Page.RouteData.Values["Vin"] as string;
            VehicleData vehicle = VehicleHelper.GetVehicleByVin(Vin);

            if (vehicle == null)
            {
                Dealer = MasterDataHelper.GetDefaultDealer(DealerID);
                string redirectUrl = VehicleHelper.GenerateVcpLinkForSoldVehicle(Vin, DealerID);
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    Response.Redirect(redirectUrl);
                }
                else
                {
                    if (Page.RouteData.Values.Count() == 9)
                    {
                        Response.Redirect("/vcp/" + Page.RouteData.Values["type"] as string + "/" + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model2"] as string + "-" + Page.RouteData.Values["model3"] as string + "-" + Page.RouteData.Values["model"] as string + "-" + Page.RouteData.Values["model1"] as string);
                    }
                    else if (Page.RouteData.Values.Count() == 8)
                    {
                        Response.Redirect("/vcp/" + Page.RouteData.Values["type"] as string + "/" + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model"] as string + "-" + Page.RouteData.Values["model1"] as string + "-" + Page.RouteData.Values["model2"] as string + "-" + Page.RouteData.Values["trim"] as string);
                    }
                    else if (Page.RouteData.Values.Count() == 7)
                    {
                        Response.Redirect("/vcp/" + Page.RouteData.Values["type"] as string + "/" + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model"] as string + "-" + Page.RouteData.Values["model1"] as string + "-" + Page.RouteData.Values["trim"] as string);
                    }
                    else if (Page.RouteData.Values.Count() == 6)
                    {
                        Response.Redirect("/vcp/" + Page.RouteData.Values["type"] as string + "/" + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model"] as string + "-" + Page.RouteData.Values["trim"] as string);
                    }
                    else if (Page.RouteData.Values.Count() == 4)
                    {
                        Response.Redirect("/vcp/used-certified/"  + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model"] as string);
                    }
                    else
                    {
                        Response.Redirect("/vcp/" + Page.RouteData.Values["type"] as string + "/" + Page.RouteData.Values["make"] as string + "/" + Page.RouteData.Values["model"] as string);
                    }

                }
                return;
            }
            List<IncentiveTranslated> translateds = VehicleHelper.GetIncentiveTranslateds();

            //if (!vehicle.HideOtherIncentives)
            //{
            //    VehicleHelper.SortOutProgramsForSearch(vehicle.NormalOffers, vehicle.ConditionalOffers,
            //        vehicle.AprOffers, translateds);
            //}
            AgileVehicleId = vehicle.AgileVehicleId;
            ModelTranslatedId = vehicle.ModelTranslatedId;
            IsNew = vehicle.IsNew;
            
            if (vehicle.IsCert)
            {
                lblMSRPTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Label;
            }
            if (IsNew)
            {
                lblMSRPTitle.Text= CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.New_Vehicle_MSRP_Label;
            }
            
            else if (!vehicle.IsNew)
            {
                lblMSRPTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Label;
            }
           


            Dealer = MasterDataHelper.GetDefaultDealer(vehicle.DealerId);
            SendMessageToThePage(Dealer.DealerID);
            PaymentCalculatorControl.IsHideHeader = false;

            // had to do this in the code behind for it to work
            imageofgas.ImageUrl = DealerImageGeneric + "content/icons/gas-pump-40.png";

            VehiclePrice.Value = string.Format("{0:n0}", vehicle.SellingPrice ?? 0);

            CheckAvailabilityControl.SelectedVehicle = vehicle;

            BindData(vehicle);
            List<VehicleData> images = VehicleHelper.GetAllImagesOfVehicle(Vin, Dealer.DealerID);
            dlImgList.DataSource = images;
            dlImgList.DataBind();
          //  btnSticker.DataBind();
            RebateControl.SelectedVehicle = vehicle;
            RebateControl.Images = images;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Global.CurrentSession))
                {
                    int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(this.Session, this.Request);
                    HttpHelper.SetVehicleDisplayPage(vehicle, buyerSessionId);
                }
            }
        }

        private void BindData(VehicleData vehicle)
        {
            VehicleVin.Value = vehicle.Vin;
            string trim = vehicle.Trim == "Other" ? "" : vehicle.Trim;
            //lblVehicle. = vehicle.Year + " " + vehicle.Model + " " + trim;
            lblYear.InnerText = vehicle.Year.ToString() + " ";
            lblModel.InnerText = vehicle.Model + " ";
            lblMake.InnerText = vehicle.Make + " ";
            lblTrim.InnerText = trim;
            lblDescription.Text = vehicle.AutowriterDescription;
            divDescription.Visible = !string.IsNullOrEmpty(vehicle.AutowriterDescription);
            VehicleImage = vehicle.ImageFile;
            lblVin.Text = vehicle.Vin;
            lblStockNum.Text = vehicle.StockNum;

            if (vehicle.MSRPOverride > 0)
            {
                if (vehicle.MSRPOverride + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
                    lblMSRP.Text = $"{vehicle.OriginalSellingPrice - vehicle.UpfittedCost:C}";
                else
                    lblMSRP.Text = (vehicle.MSRPOverride > 0 ? $"{vehicle.MSRPOverride:C}" : "$0");
            }
            else
            {
                if (vehicle.MSRP + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
                    lblMSRP.Text = $"{vehicle.OriginalSellingPrice - vehicle.UpfittedCost:C}";
                else
                    lblMSRP.Text = (vehicle.MSRP > 0 ? $"{vehicle.MSRP:C}" : "$0");
            }

            lblRebate.Text = $@"- {Math.Abs(vehicle.Rebate):C}";
            lblRebate.Attributes.Add("agilevehicleid", AgileVehicleId.ToString());

            lblPrice.Text = vehicle.SellingPrice > 0 ? $"{vehicle.SellingPrice:C}" : "$0";

            IsUpfitted = vehicle.IsUpfitted;
            hdnIsShowUpfittedMSRP.Value = "0";

            if (IsUpfitted)
            {
                lblUpfittedCost.Text = $"+ {vehicle.UpfittedCost:C}";
                lblUpfittedCost.Attributes.Add("modelId", ModelTranslatedId.ToString());
                lblUpfittedCost.Attributes.Add("vin", vehicle.Vin);

                //if (vehicle.MSRP + vehicle.UpfittedCost > vehicle.SellingPrice)
                hdnIsShowUpfittedMSRP.Value = "1";
            }

            lblUpfittedCostTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Upfitting_Label;

            if (vehicle.EmployeePrice > 0)
            {
                employeePriceSection.Visible = true;
                lblEmployeePrice.Text = $"{vehicle.EmployeePrice:C}";
                vehicle.OriginalSellingPrice = Convert.ToDecimal(vehicle.EmployeePrice);
            }
            else
                employeePriceSection.Visible = false;

            lblNormalOfferPrice.Text = vehicle.NormalSellingPrice > 0 ? $"{vehicle.NormalSellingPrice:C}" : "$0";
            lblConditionalPrice.Text = vehicle.ConditionalSellingPrice > 0 ? $"{vehicle.ConditionalSellingPrice:C}" : "$0";
            conditionalSection.Visible = false; //vehicle.ConditionalOffers != null && vehicle.ConditionalOffers.Any();
           
            if (vehicle.NormalOffers != null && vehicle.NormalOffers.Any())
            {
                var normalOffers = vehicle.NormalOffers.Select(x => new
                {
                    Name = x.Name,
                    DoubleAmount = $"- {x.DoubleAmount:C}",
                    ProgramId = x.ProgramId.ToString(),
                    Amount = x.Amount,
                    Type = x.Type,
                    Disclaimer = (x.IsCashCoupon ? x.Disclaimer.Replace("'", "") : "Program #" + x.Id + ":" + x.Disclaimer.Replace("'", "")) + "  See dealer for complete details.",
                    IsCashCoupon = x.IsCashCoupon.ToString().ToLower()

                });
                rptNormalOffer.DataSource = normalOffers;
                rptNormalOffer.DataBind();

                hdnIsShowUpfittedMSRP.Value = "1";
            }

            if (vehicle.ConditionalOffers != null && vehicle.ConditionalOffers.Any())
            {
                var conditionalOffers = vehicle.ConditionalOffers.Select(x => new
                {
                    Name = x.Name,
                    DoubleAmount = $"- {x.DoubleAmount:C}",
                    ProgramId = x.ProgramId.ToString(),
                    Amount = x.Amount,
                    Type = x.Type,
                    Disclaimer = (x.IsCashCoupon ? x.Disclaimer.Replace("'", "") : "Program #" + x.Id + ":" + x.Disclaimer.Replace("'", "")) + "  See dealer for complete details.",
                    IsCashCoupon = x.IsCashCoupon.ToString().ToLower()

                });
                rptConditionalOffer.DataSource = conditionalOffers;
                rptConditionalOffer.DataBind();
            }

            //if (vehicle.MSRPOverride > 0)
            //    vehicle.DealerSaving = (vehicle.MSRPOverride > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRPOverride > vehicle.OriginalSellingPrice) ? (vehicle.MSRPOverride - vehicle.OriginalSellingPrice) : 0;
            //else
            //    vehicle.DealerSaving = (vehicle.MSRP > 0 && vehicle.OriginalSellingPrice > 0 && vehicle.MSRP > vehicle.OriginalSellingPrice) ? (vehicle.MSRP - vehicle.OriginalSellingPrice) : 0;

            lblDealerSave.Text = vehicle.DealerSaving > 0 ? $"- {vehicle.DealerSaving:C}" : "$0";

            //if (vehicle.IsOverridePrice)
            //{
            //    lblNormalOfferPrice.Text = vehicle.OriginalSellingPrice > 0 ? $"{vehicle.OriginalSellingPrice:C}" : "$0";
                
            //    lblPrice.Text = vehicle.OriginalSellingPrice > 0 ? $"{vehicle.OriginalSellingPrice:C}" : "$0";

            //    conditionalSection.Visible = false;
            //    rptNormalOffer.Visible = false;
            //    rptConditionalOffer.Visible = false;
            //}

            if (vehicle.MSRP + vehicle.UpfittedCost < vehicle.OriginalSellingPrice)
            {
                //  lblMSRP.Visible = false;
                if (vehicle.MSRPOverride < vehicle.OriginalSellingPrice)
                {
                    lblDealerSave.Text = "$0";
                }
                if (vehicle.IsCert)
                {
                    lblMSRPTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Certified_Vehicle_MSRP_Override_Label;
                }
                if (IsNew)
                {
                    lblMSRPTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Asking_Price_Label;
                }

                else if (!vehicle.IsNew)
                {
                    lblMSRPTitle.Text = CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo.VehicleInfo.Used_Vehicle_MSRP_Override_Label;
                }
            }

            HdfIncentiveDisplayTypeId.Value = Dealer.IncentiveDisplayTypeId.HasValue ? Dealer.IncentiveDisplayTypeId.Value.ToString() : ((int)DealerIncentiveTypeEnum.Normal).ToString();
            IncentiveDisplayTypeId = Dealer.IncentiveDisplayTypeId.HasValue ? Dealer.IncentiveDisplayTypeId.Value : ((int)DealerIncentiveTypeEnum.Normal);
            if (vehicle.MSRP <= 0 && vehicle.OriginalSellingPrice <= 0 && vehicle.MSRPOverride <= 0 && vehicle.SellingPrice <= 0)
                IsDisplayOfferDetail = false;
            else
                IsDisplayOfferDetail = true;

            lblBody.Text = vehicle.Make;
            lblExtColor.Text = vehicle.ExtColor;
            lblIntColor.Text = vehicle.IntColor;
            lblEngineDescript.Text = vehicle.EngDescription;
            lblTranmission.Text = vehicle.Trans;
            lblDriveTrain.Text = vehicle.DriveTrain;
            lblMileage.Text = vehicle.Mileage.ToString();
            lblMPGCity.Text = vehicle.MPGCity.ToString();
            lblMPGCityTwo.Text = vehicle.MPGCity.ToString();
            lblMPGHwy.Text = vehicle.MPGHighway.ToString();
            lblMPGHwyTwo.Text = vehicle.MPGHighway.ToString();

            if (!string.IsNullOrEmpty(vehicle.Convenience))
            {
                vehicle.Convenience = vehicle.Convenience.Replace("Interior@", string.Empty);
                var tempConvenience = vehicle.Convenience.Split('~').OrderBy(x => x.Length).ToList();
                if (tempConvenience.Any())
                {
                    var convenienceSource = tempConvenience.Select(x => new
                    {
                        Content = x
                    });
                    rptConvenience.DataSource = convenienceSource;
                    rptConvenience.DataBind();

                    rptConveniencePrint.DataSource = convenienceSource;
                    rptConveniencePrint.DataBind();
                }
            }


            if (!string.IsNullOrEmpty(vehicle.ExtEquipmt))
            {
                vehicle.ExtEquipmt = vehicle.ExtEquipmt.Replace("Exterior@", string.Empty);
                var tempExtEquipmt = vehicle.ExtEquipmt.Split('~').OrderBy(x => x.Length).ToList();
                if (tempExtEquipmt.Any())
                {
                    var extEquipmtSource = tempExtEquipmt.Select(x => new
                    {
                        Content = x
                    });
                    rptExtEquip.DataSource = extEquipmtSource;
                    rptExtEquip.DataBind();

                    rptExtEquipPrint.DataSource = extEquipmtSource;
                    rptExtEquipPrint.DataBind();
                }
            }


            if (!string.IsNullOrEmpty(vehicle.Safety))
            {
                vehicle.Safety = vehicle.Safety.Replace("Safety@", string.Empty);
                var tempSafety = vehicle.Safety.Split('~').OrderBy(x => x.Length).ToList();
                if (tempSafety.Any())
                {
                    var safetySource = tempSafety.Select(x => new
                    {
                        Content = x
                    });
                    rptSafety.DataSource = safetySource;
                    rptSafety.DataBind();

                    rptSafetyPrint.DataSource = safetySource;
                    rptSafetyPrint.DataBind();
                }
            }


            if (!string.IsNullOrEmpty(vehicle.Technical))
            {
                vehicle.Technical = vehicle.Technical.Replace("Mechanical@", string.Empty);
                var tempTechnical = vehicle.Technical.Split('~').OrderBy(x => x.Length).ToList();
                if (tempTechnical.Any())
                {
                    var technicalSource = tempTechnical.Select(x => new
                    {
                        Content = x
                    });
                    rptTechnical.DataSource = technicalSource;
                    rptTechnical.DataBind();

                    rptTechnicalPrint.DataSource = technicalSource;
                    rptTechnicalPrint.DataBind();
                }
            }


            if (!string.IsNullOrEmpty(vehicle.Options))
            {
                var tempOptions = vehicle.Options.Split(',').OrderByDescending(x => x.Length).ToList();
                if (tempOptions.Any())
                {
                    var optionSource = tempOptions.Select(x => new
                    {
                        Content = x
                    });
                    rptOption.DataSource = optionSource;
                    rptOption.DataBind();

                }
            }

            // btnSticker.PostBackUrl = $"http://www.windowsticker.forddirect.com/windowsticker.pdf?vin={vehicle.Vin}";
            // btnSticker.Visible = !vehicle.IsNew;
            lblRebate.Text = $"- {Math.Abs(vehicle.Rebate):C}";
            IsVisible.Value = (Page.User.IsInRole(RoleEnum.Administrator) || Page.User.IsInRole(RoleEnum.Dealer)).ToString();
            IsOverridePrice.Value = vehicle.IsOverridePrice.ToString();
            OverrideStartDate.Value = vehicle.OverrideStartDate.ToString();
            OverrideEndDate.Value = vehicle.OverrideEndDate.ToString();
            HasSetDateRange.Value = vehicle.HasSetDateRange.ToString();
            VehicleType.Value = vehicle.IsNew.ToString();
            if (!string.IsNullOrEmpty(Dealer.VehicleLookupLink) && !string.IsNullOrEmpty(Dealer.VehicleLookupLink))
            {
                string loopLink = Dealer.VehicleLookupLink.Replace("{Vin}", vehicle.Vin);
                VehicleLookupLink.HRef = loopLink;
                VehicleLookupLogo.Src = Dealer.VehicleLookupLogo;
            }
            carfaxSection.Visible = true;
            certifiedPreOwned.Visible = vehicle.IsCert;

            lblVehicleType.InnerText = vehicle.IsNew ? "New " : vehicle.IsCert ? "Certified Pre-Owned " : "Used ";
        }
    }
}