﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleInfo.ascx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UserControl.VehicleInfo.VehicleInfo" %>

<%@ Register Src="~/Views/UserControl/CheckAvailability.ascx" TagName="CheckAvailabilityControl" TagPrefix="checkAvailabilityUserControl" %>
<%@ Register Src="~/Views/UserControl/PaymentCalculator.ascx" TagName="PaymentCalculatorControl" TagPrefix="paymentCalculatorControl" %>
<%@ Register Src="~/Views/UserControl/RebateControl/RebateControl.ascx" TagPrefix="RebateControl" TagName="rebateControl" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Enums" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.VehicleInfo" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>

<script src="../../../Scripts/Pages/AgileDealer.VehicleInfo.js"></script>
<script src="../../../Scripts/Pages/Enums/Dealer.Enum.js"></script>

<div id="vehicle-display" class="container vehicle-info">
    <div class="row">
        <div class="col-md-12">
            <h1>
                <asp:Label runat="server" ID="lblVehicle">
                    <span runat="server" id="lblVehicleType"></span>
                    <span runat="server" id="lblYear"></span>
                    <span runat="server" id="lblMake"></span>
                    <span runat="server" id="lblModel"></span>
                    <span runat="server" id="lblTrim"></span>
                </asp:Label>
            </h1>
            <button type="button" class="btn btn-primary" onclick="history.back(-1)"
                style="margin-bottom: 10px; margin-top: -10px;">
                Back to Inventory</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <div class="bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="main-image" id="imgVehicle" style="cursor: pointer;"
                            alt="<%#VehicleInfo.Main_Image_Tooltip %>"
                            src="<%=VehicleImage %>" title="<%#VehicleInfo.Main_Image_Tooltip %>" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 ">
                        <div class="over-scroll">
                            <asp:DataList runat="server" ID="dlImgList" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="product">
                                        <img alt="<%#VehicleInfo.Product_ImageButton_Alt %>"
                                            src='<%# Eval("ImageFile") %> '
                                            style="cursor: pointer; max-width: 100px"
                                            title="<%#VehicleInfo.Product_ImageButton_Tooltip %>" />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel pricing">
                <% if (IsDisplayOfferDetail == true)
                    { %>
                <div class="row" id="msrpSection">
                    <div class="col-xs-6 text-left">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblMSRPTitle" Style="font-weight: 700"></asp:Label>
                    </div>
                    <div class="col-xs-6 text-right">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblMSRP"></asp:Label>
                    </div>
                </div>
                 <div class="row" id="employeePriceSection" runat="server">
                    <div class="col-xs-6 text-left">
                         <label style="font-weight: 700">Employee Price</label>
                    </div>
                    <div class="col-xs-6 text-right">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblEmployeePrice"></asp:Label>
                    </div>
                </div>
                 <% if (IsUpfitted == true)
                    { %>
                <div class="row">
                    <div class="col-xs-6 text-left">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblUpfittedCostTitle" Style="font-weight: 700"></asp:Label>
                    </div>
                    <div class="col-xs-6 text-right">
                        <asp:Label runat="server" CssClass="msrp-price rebate openRebate" data-toggle="modal" isUpfitted="true" ID="lblUpfittedCost"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 text-left">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblMarketValueTitle" Style="font-weight: 700"></asp:Label>
                    </div>
                    <div class="col-xs-6 text-right">
                        <asp:Label runat="server" CssClass="msrp-price" ID="lblMarketValue"></asp:Label>
                    </div>
                </div>

                <% } %>
                <% if (1 == 2) //(IncentiveDisplayTypeId == (int)DealerIncentiveTypeEnum.Normal || !IsNew)
                    { %>
                <div class="row hide" id="rebateSection">
                    <div class="col-xs-6 text-left">
                        <label><%=VehicleInfo.Row_Col_Xs_6_Label_1 %></label>
                    </div>
                    <div class="col-md-6 text-right">
                        <asp:Label runat="server" CssClass="rebate openRebate" isUpfitted="false"  vin="<%#Vin %>" modelId="<%# ModelTranslatedId %>" data-toggle="modal" ID="lblRebate"></asp:Label>
                        <%--<label runat="server" id="lblRebate" class="rebate openRebate" agilevehicleid="<%# AgileVehicleId %>" "></label>--%>
                    </div>
                </div>
                <% }%>

                <div class="row" id="normalOfferSection">
                    <asp:Repeater runat="server" ID="rptNormalOffer">
                        <ItemTemplate>
                            <div class="col-xs-12 nopadding">
                                <div class="col-xs-6 text-left ">
                                    <label class="lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='<%# Eval("Disclaimer") %>' style="cursor: pointer;"><%# Eval("Name") %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="rebate openRebate" modelid="<%# ModelTranslatedId %>" vin="<%#Vin %>" isupfitted="false" iscashcoupon="<%# Eval("IsCashCoupon") %>" data-toggle="modal" data-keyboard="true" agilevehicleid="<%# AgileVehicleId %>" programid="<%# Eval("ProgramId") %>"><%# Eval("DoubleAmount") %></label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div class="row border-bottom" id="totalSavingSection">
                    <div class="col-xs-6 text-left">
                        <label class="lablTooltip" style="cursor: pointer;" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='Price includes Dealer Discount, please see dealer for details.'><%=VehicleInfo.Row_Col_Xs_6_Label_2 %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>

                    </div>
                    <div class="col-md-6 text-right">
                        <asp:Label runat="server" CssClass="dealersaving" ID="lblDealerSave"></asp:Label>
                    </div>
                </div>


                <% if (IncentiveDisplayTypeId == (int)DealerIncentiveTypeEnum.Normal || !IsNew)
                    { %>
                <div class="row">
                    <div class="col-xs-12 nopadding">
                        <div class="col-xs-6 text-left">
                            <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_5 %>**</label>
                        </div>
                        <div class="col-xs-6 text-right">
                            <asp:Label CssClass="price-main" runat="server" ID="lblPrice"></asp:Label>
                        </div>
                    </div>
                    <div class="col-xs-12 nopadding text-center vdp-disclaimer">
                        <small>*Price includes Dealer Discount.  See dealer for details.</small>
                    </div>
                </div>
                <% }
                    else
                    { %>
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
                        <div class="col-xs-6 col-lg-6">
                            <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_3 %>**</label>
                        </div>
                        <div class="col-xs-6 col-lg-6 text-right">
                            <asp:Label CssClass="price-main" runat="server" ID="lblNormalOfferPrice"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row margintop10" id="conditionalOfferSection">
                    <asp:Repeater runat="server" ID="rptConditionalOffer">
                        <HeaderTemplate>
                            <div class="col-xs-12 nopadding">
                                <div class="col-xs-12 text-left">
                                    <span>Conditional offers: </span>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="col-xs-12  col-lg-12">
                                <div class="col-xs-6 col-lg-6">
                                    <label class="lablTooltip" data-placement="bottom" data-html="true" data-toggle="tooltip" data-original-title='<%# Eval("Disclaimer") %>' style="cursor: pointer;"><%# Eval("Name") %> <a style="cursor: pointer;" class="lablTooltip"><i class="fa fa-question-circle" aria-hidden="true"></i></a></label>
                                </div>
                                <div class="col-xs-6 col-lg-6 text-right">
                                    <label class="rebate openRebate" isupfitted="false" data-toggle="modal" data-keyboard="true" vin="<%#Vin %>" modelid="<%# ModelTranslatedId %>" iscashcoupon="<%# Eval("IsCashCoupon") %>" agilevehicleid="<%# AgileVehicleId %>" programid="<%# Eval("ProgramId") %>"><%# Eval("DoubleAmount") %></label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="row border-top" runat="server" id="conditionalSection">
                    <div class="col-xs-12 nopadding">
                        <div class="col-xs-6 text-left">
                            <label class="price-label"><%=VehicleInfo.Row_Col_Xs_6_Label_4 %> up to*</label>
                        </div>
                        <div class="col-xs-6 text-right">
                            <asp:Label CssClass="price-main" runat="server" ID="lblConditionalPrice"></asp:Label>
                        </div>
                    </div>
                    <%--                   --%>
                </div>
                <div class="col-xs-12 nopadding text-center vdp-disclaimer">
                    <small>*Price includes Dealer Discount.  See dealer for details.</small>
                </div>
                <% } %>
                <% }
                    else
                    { %>
                <div class="row">
                    <div class="col-xs-12 nopadding">
                        <div class="col-xs-6 text-left">
                            <label class="price-label"><%=VehicleInfo.Price_Label %></label>
                        </div>
                        <div class="col-xs-6 text-right">
                            <strong>Call</strong>
                        </div>
                    </div>
                </div>
                <% } %>
                <%--<div class="row">
                    <div class="col-md-12">
                        <label>Estimated Payment</label>
                        <asp:Label runat="server" ID="lblEstPayment"></asp:Label>
                    </div>
                </div>--%>
                <div class="row no-print">
                    <div class="col-xs-12 text-center">
                        <a class="btn btn-danger" style="width: 100%; margin-top: 10px" id="btnEditVehicle" title="<%=VehicleInfo.Row_No_Print_Btn_Danger_A_Title %>"><%=VehicleInfo.Row_No_Print_Btn_Danger_A %></a>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-primary" id="btnCheckAvailability" title="<%=VehicleInfo.Row_No_Print_Btn_Danger_Input_Title %>"><%=VehicleInfo.Row_No_Print_Btn_Danger_Input_Value %></button>
                    </div>

                </div>
                <div class="row small-text text-center">
                    <div class="col-xs-12">
                        <span><strong><%=VehicleInfo.Text_Center_Span_Strong %></strong> </span>
                        <asp:Label runat="server" ID="lblVin"></asp:Label>
                        <span>| </span>
                        <span><strong><%=VehicleInfo.Text_Center_Span_Strong_1 %></strong></span>
                        <asp:Label runat="server" ID="lblStockNum"></asp:Label>
                    </div>

                </div>
            </div>
            <div class="panel text-center fuel-efficiency" id="fuelRating">
                <div class="row">
                    <h4><%=VehicleInfo.Fuel_Efficiency_Row_H4 %></h4>
                </div>
                <div class="row small">
                    <div class="col-xs-4">
                        <label><%=VehicleInfo.Row_Small_Col_Xs_4_Label %></label>
                        <asp:Label CssClass="mileage" runat="server" ID="lblMPGCityTwo"></asp:Label>
                    </div>
                    <div class="col-xs-4">
                        <asp:Image ID="imageofgas" runat="server" alt="imageofgas" />
                    </div>
                    <div class="col-xs-4">
                        <label><%=VehicleInfo.Row_Small_Col_Xs_4_Label_1 %></label>
                        <asp:Label CssClass="mileage" runat="server" ID="lblMPGHwyTwo"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <p class="small-text mpg-disclaimer"><%=VehicleInfo.Small_Text_Mpg_Disclaimer %> <a data-toggle="modal" href="#epaModal" class="">https://www.fueleconomy.gov/feg/label/learn-more-PHEV-label.shtml</a>.</p>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="epaModal" tabindex="-1" role="dialog" aria-labelledby="epaModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">EPA Mileage Information</h4>
                </div>
                <div class="modal-body" style="overflow-y: hidden;">
                    <iframe src="https://www.fueleconomy.gov/feg/label/learn-more-PHEV-label.shtml" style="width: 100%; height: 450px;" frameborder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="row">
        <div class="col-md-7">

            <div class="panel bottom-0">
                <div class="row">
                    <h2 class="carbon-bg"><%=VehicleInfo.Panel_Bottom_0_H2 %></h2>
                </div>
                <div class="row">
                    <div class="col-xs-5">
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label %></label>
                            <asp:Label runat="server" ID="lblBody"></asp:Label>
                        </div>
                        <div class="content-group" id="hwyMPG">
                            <label><%=VehicleInfo.Content_Group_Label_1 %></label>
                            <asp:Label runat="server" ID="lblMPGHwy"></asp:Label>
                        </div>
                        <div class="content-group" id="cityMPG">
                            <label><%=VehicleInfo.Content_Group_Label_2 %></label>
                            <asp:Label runat="server" ID="lblMPGCity"></asp:Label>
                        </div>
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_3 %></label>
                            <asp:Label runat="server" ID="lblExtColor"></asp:Label>
                        </div>
                    </div>
                    <div class="col-xs-7">
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_4 %></label>
                            <asp:Label runat="server" ID="lblIntColor"></asp:Label>
                        </div>
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_5 %></label>
                            <asp:Label runat="server" ID="lblEngineDescript"></asp:Label>
                        </div>
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_6 %></label>
                            <asp:Label runat="server" ID="lblTranmission"></asp:Label>
                        </div>
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_9 %></label>
                            <asp:Label runat="server" ID="lblDriveTrain"></asp:Label>
                        </div>
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_7 %></label>
                            <asp:Label runat="server" ID="lblMileage"></asp:Label>
                        </div>
                    </div>
                    <div class="clear-20"></div>
                    <div class="col-xs-12" runat="server" id="divDescription">
                        <div class="content-group">
                            <label><%=VehicleInfo.Content_Group_Label_8 %></label>
                            <asp:Label runat="server" ID="lblDescription"></asp:Label>
                            <div class="clear-20"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-5">

            <div class="panel no-print bottom-0">
                <div class="row">
                    <h2 class="carbon-bg"><%=VehicleInfo.Panel_No_Print_Bottom_0_Row_H2 %></h2>
                </div>

                <%--   <!-- Auto Check Integration - Should only show for used  -->
                <div class="row">
                    <div class="col-md-12">
                        <a href="" class="autocheck">
                            <img src="https://agiledealer.blob.core.windows.net/generic/AutoCheckFree.png" id="AutoCheckVehicleButton" alt="Experian AutoCheck" />
                        </a>
                    </div>
                </div>
                <!-- End Auto Check Integration -->  --%>

                <!-- True Car Integration - Should only show for used  -->
                <div class="row">
                    <div class="col-md-12">
                        <a href="/content/get-cash-trade-offer" class="btn btn-primary" target="_blank" id="btnTrueCar" title="Get Cash Trade Offer">Get Cash Trade Offer</a>
                    </div>
                </div>
                <!-- True Car Integration -->

                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" id="btnPayment" title="<%=VehicleInfo.Row_Col_Md_12_Input_Title %>"><%=VehicleInfo.Row_Col_Md_12_Input_Value %></button>
                    </div>
                </div>
                <%-- <div class="row">
                    <div class="col-md-12">
                         <button type="button" class="btn btn-primary" ID="btnSticker" title="<%=VehicleInfo.Row_Col_Md_12_Button_ToolTip %>"><%= VehicleInfo.Row_Col_Md_12_Button_Value %></button>
                    </div>
                </div>--%>

                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" id="btnPrint" title="<%=VehicleInfo.Row_Col_Md_12_Input_1_Title %>"><%=VehicleInfo.Row_Col_Md_12_Input_1_Value %></button>
                    </div>
                </div>

                <div class="row">
                    <div id="carfaxSection" runat="server" class="col-xs-12 padded-15 autocheck">
                        <div class="col-md-12">
                            <a runat="server" id="VehicleLookupLink" href='' target="_blank">
                                <img class="img-50-responsive" runat="server" id="VehicleLookupLogo" src='' border='0' alt="autocheck" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="certifiedPreOwned" runat="server" class="col-xs-12 padded-15">
                        <a href="" target="_blank">
                            <img class="cpo-logo img-50-responsive" 
                                src="https://swopemitsubishi.blob.core.windows.net/images/dealership/swope-certified-pre-owned.jpg"
                                alt="Certified-Pre-Owned" />
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="panel no-print" id="mainFeature">
                <div class="row">
                    <h2 class="carbon-bg"><%=VehicleInfo.Panel_No_Print_Row_H2 %></h2>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ajax:Accordion runat="server" ID="accFeatures" CssClass="accordion">
                            <Panes>
                                <ajax:AccordionPane runat="server" ID="conveniencePane">
                                    <Header>
                                        <div class="header"><%=VehicleInfo.Accordion_Header_1 %></div>
                                    </Header>
                                    <Content>
                                        <ul runat="server" class="vdp-feature-list">
                                            <asp:Repeater runat="server" ID="rptConvenience">
                                                <ItemTemplate>
                                                    <li><%# Eval("Content") %></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </Content>
                                </ajax:AccordionPane>
                                <ajax:AccordionPane runat="server" ID="extPane">
                                    <Header>
                                        <div class="header"><%=VehicleInfo.Accordion_Header_2 %></div>
                                    </Header>
                                    <Content>
                                        <ul runat="server" class="vdp-feature-list">
                                            <asp:Repeater runat="server" ID="rptExtEquip">
                                                <ItemTemplate>
                                                    <li><%# Eval("Content") %></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </Content>
                                </ajax:AccordionPane>
                                <ajax:AccordionPane runat="server" ID="safetyPane">
                                    <Header>
                                        <div class="header"><%=VehicleInfo.Accordion_Header_3 %> </div>
                                    </Header>
                                    <Content>
                                        <ul runat="server" class="vdp-feature-list">
                                            <asp:Repeater runat="server" ID="rptSafety">
                                                <ItemTemplate>
                                                    <li><%# Eval("Content") %></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </Content>
                                </ajax:AccordionPane>
                                <ajax:AccordionPane runat="server" ID="technicalPane">
                                    <Header>
                                        <div class="header"><%=VehicleInfo.Accordion_Header_4 %> </div>
                                    </Header>
                                    <Content>
                                        <ul runat="server" class="vdp-feature-list">
                                            <asp:Repeater runat="server" ID="rptTechnical">
                                                <ItemTemplate>
                                                    <li><%# Eval("Content") %></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </Content>
                                </ajax:AccordionPane>
                            </Panes>
                        </ajax:Accordion>
                    </div>
                </div>
            </div>
            <div class="panel hidden" id="printFeature">
                <div class="row">
                    <h2 class="carbon-bg">Features</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="accFeaturesPrint" class="accordion" style="height: auto; overflow: auto;">
                            <div id="MainContent">
                                <div class="header"><%=VehicleInfo.Accordion_Header_1 %></div>
                            </div>
                            <div style="height: auto; overflow: auto; display: block;">
                                <div style="display: block; height: auto; overflow: auto;">
                                    <ul runat="server" class="vdp-feature-list">
                                        <asp:Repeater runat="server" ID="rptConveniencePrint">
                                            <ItemTemplate>
                                                <li><%# Eval("Content") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <div id="MainContent">
                                <div class="header"><%=VehicleInfo.Accordion_Header_2 %></div>
                            </div>
                            <div style="height: auto; overflow: auto; display: block;">
                                <div style="display: block; height: auto; overflow: auto;">
                                    <ul runat="server" class="vdp-feature-list">
                                        <asp:Repeater runat="server" ID="rptExtEquipPrint">
                                            <ItemTemplate>
                                                <li><%# Eval("Content") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <div id="MainContent">
                                <div class="header"><%=VehicleInfo.Accordion_Header_3 %></div>
                            </div>
                            <div style="height: auto; overflow: auto; display: block;">
                                <div style="display: block; height: auto; overflow: auto;">
                                    <ul runat="server" class="vdp-feature-list">
                                        <asp:Repeater runat="server" ID="rptSafetyPrint">
                                            <ItemTemplate>
                                                <li><%# Eval("Content") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <div id="MainContent">
                                <div class="header"><%=VehicleInfo.Accordion_Header_4 %></div>
                            </div>
                            <div style="height: auto; overflow: auto; display: block;">
                                <div style="display: block; height: auto; overflow: auto;">
                                    <ul runat="server" class="vdp-feature-list">
                                        <asp:Repeater runat="server" ID="rptTechnicalPrint">
                                            <ItemTemplate>
                                                <li><%# Eval("Content") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div class="col-md-5">
            <div class="panel">
                <div class="row">
                    <h2 class="carbon-bg"><%=VehicleInfo.Col_Md_5_Panel_Row_H2 %></h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul runat="server" class="vdp-feature-list option">
                            <asp:Repeater runat="server" ID="rptOption">
                                <ItemTemplate>
                                    <li><%# Eval("Content") %></li>
                                    <br>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div id="divBackground" onclick="HideDiv()" class="modal">
    </div>

    <div id="divImage">
        <table style="height: 100%; width: 100%">
            <tr>
                <td style="vertical-align: middle; text-align: center;">
                    <%--<img id="imgLoader" alt="" src="../../Content/Images/hourglass.png" />--%>
                    <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom; text-align: center;">
                    <input id="btnClose" type="button" class="btn btn-primary" value="Close" onclick="HideDiv()" />
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField runat="server" ID="VehiclePrice" />
    <asp:HiddenField runat="server" ID="VehicleType" />
    <asp:HiddenField runat="server" ID="VehicleVin" />
    <asp:HiddenField runat="server" ID="IsVisible" />
    <asp:HiddenField runat="server" ID="IsOverridePrice" />
    <asp:HiddenField runat="server" ID="OverrideStartDate" />
    <asp:HiddenField runat="server" ID="OverrideEndDate" />
    <asp:HiddenField runat="server" ID="HasSetDateRange" />
    <asp:HiddenField runat="server" ID="HdfIncentiveDisplayTypeId" />
      <asp:HiddenField ID="hdnIsShowUpfittedMSRP" runat="server" />
</div>
<div class="hidden">
    <div id="ucPaymentCalculator">
        <paymentCalculatorControl:PaymentCalculatorControl runat="server" ID="PaymentCalculatorControl" />
    </div>
</div>
<div class="">
    <div id="">
        <checkAvailabilityUserControl:CheckAvailabilityControl runat="server" ID="CheckAvailabilityControl" />
    </div>
</div>
<div id="ucRebate">
    <RebateControl:rebateControl runat="server" ID="RebateControl" />
</div>

<script>
    $("#btnSticker").on("click", function () {
        document.forms[0].action = "http://www.windowsticker.forddirect.com/windowsticker.pdf?vin=<%= Vin %>";
        document.forms[0].target = "_blank";
        document.forms[0].submit();
    });
    $('[data-toggle="tooltip"]').tooltip();
</script>
<script>


    $(function () {
        if ($("#MainContent_VehicleInfo_hdnIsShowUpfittedMSRP").val() == "1") {
            $("#MainContent_VehicleInfo_msrpSection").show();
            // alert($("#MainContent_VehicleInfo_hdnIsShowUpfittedMSRP").val());
        }
    });</script>