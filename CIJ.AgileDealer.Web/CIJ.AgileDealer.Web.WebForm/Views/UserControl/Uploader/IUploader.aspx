﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IUploader.aspx.cs" Inherits="CIJ.AgileDealer.Web.WebForm.Views.UserControl.Uploader.IUploader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        html
        {
            overflow: hidden;
        }
    </style>
    <title></title>
    <script src="../../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
</head>
<body style="background-color: White">
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" />
        <asp:HiddenField ID="hdfFileUploadedPath" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdfFileUploadedName" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdfResult" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdfFileSize" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdfFileDimension" ClientIDMode="Static" runat="server" />
        <script type="text/javascript">
            function chooseFileUpload() {
                $('#<%=FileUpload1.ClientID %>').trigger('click');
            }
            $(document).ready(function () {
                $('#<%=FileUpload1.ClientID %>').change(function () {
                    window.parent.showFileUploading<%=Request.QueryString["cfid"] %>();
                    $('#<%=Button1.ClientID %>').click();
                });
                if ($('#hdfResult').val() != '') {
                    window.parent.callbackUploadFile<%=Request.QueryString["cfid"] %>($('#hdfFileUploadedName').val(), $('#hdfFileUploadedPath').val(), $('#hdfResult').val(), $('#hdfFileSize').val(), $('#hdfFileDimension').val());
                }
            });
        </script>
    </div>
    </form>
</body>
</html>
