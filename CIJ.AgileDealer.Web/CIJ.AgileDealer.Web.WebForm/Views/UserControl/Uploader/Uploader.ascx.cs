﻿using CJI.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CIJ.AgileDealer.Web.WebForm.Views.UserControl.Uploader
{
    public partial class Uploader : BaseUserControl
    {
        private string m_Path = string.Empty;

        public string Path
        {
            get { return m_Path; }
            set { m_Path = value; }
        }

        private int m_IsUploadImage = 0;

        public int IsUploadImage
        {
            get { return m_IsUploadImage; }
            set { m_IsUploadImage = value; }
        }

        private string m_ClientFormID = string.Empty;

        public string ClientFormID
        {
            get { return m_ClientFormID; }
            set { m_ClientFormID = value; }
        }

        private string m_CallbackFunction = string.Empty;

        public string CallbackFunction
        {
            get { return m_CallbackFunction; }
            set { m_CallbackFunction = value; }
        }

        private string m_UploadingFunction = string.Empty;

        public string UploadingFunction
        {
            get { return m_UploadingFunction; }
            set { m_UploadingFunction = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}