﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Uploader.ascx.cs" Inherits="CIJ.AgileDealer.Web.WebForm.Views.UserControl.Uploader.Uploader" %>
<iframe id='ifUploadFile<%=ClientFormID %>' width="0px" height="0px" frameborder="0" src='../UserControl/Uploader/IUploader.aspx?path=<%= Path%>&image=<%= IsUploadImage%>&cfid=<%= ClientFormID %>'>
</iframe>
<script type="text/javascript">
    function openFileUpload<%=ClientFormID %>() {
        document.getElementById('ifUploadFile<%=ClientFormID %>').contentWindow.chooseFileUpload();
    }

    function reloadUploadFrameLink<%=ClientFormID %>() {
        document.getElementById('ifUploadFile<%=ClientFormID %>').src = 'Controls/Uploader/IUploader.aspx?path=<%= Path%>&image=<%= IsUploadImage%>&cfid=<%= ClientFormID %>';
    }   
</script>
