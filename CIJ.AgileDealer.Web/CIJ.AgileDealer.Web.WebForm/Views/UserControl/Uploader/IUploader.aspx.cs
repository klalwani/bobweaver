﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.Base.Helpers;
using CIJ.AgileDealer.Web.WebForm.Common;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CIJ.AgileDealer.Web.WebForm.Views.UserControl.Uploader
{
    public partial class IUploader : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    long FileInBytes = FileUpload1.PostedFile.ContentLength;
                    if ((FileInBytes / 1024) > 10240)
                    {
                        hdfResult.Value = "Maximum File Size - 10 MB";
                        return;
                    }

                    string oriFileName = Path.GetFileName(FileUpload1.FileName);
                    string ext = Path.GetExtension(oriFileName);

                    bool isUploadImage = Request.QueryString["image"] == "1" ? true : false;

                    if ((isUploadImage && !Utils.IsValidImage(ext)) || !Utils.SuportFile(ext))
                    {
                        hdfResult.Value = "File type not support";
                        return;
                    }

                    string rootPath = HttpUtility.UrlDecode(Request.QueryString["path"]);
                    if (!Directory.Exists(Server.MapPath("~/" + rootPath)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/" + rootPath));
                    }
                    string newFileUrl = Guid.NewGuid().ToString() + ext;

                    FileUpload1.SaveAs(Server.MapPath("~/" + rootPath + "/" + newFileUrl));
                    hdfFileUploadedName.Value = oriFileName;
                    hdfFileUploadedPath.Value = rootPath + "/" + newFileUrl;
                    hdfResult.Value = "Success";
                    hdfFileSize.Value = FileInBytes.ToString();
                    if (isUploadImage)
                    {
                        FileStream fs = new FileStream(Server.MapPath("~/" + rootPath + "/" + newFileUrl), FileMode.Open, FileAccess.Read);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(fs);
                        fs.Close();
                        hdfFileDimension.Value = string.Format("{0}x{1}", image.Width, image.Height);
                    }
                }
                catch (Exception ex)
                {
                    hdfResult.Value = ex.Message;
                    var el = ExceptionHelper.LogException("IUploader.Button1_Click() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: Button1_Click");
                    EmailHelper.SendEmailForNotificationWhenFailing(el);
                    return;
                }
            }
        }
    }
}