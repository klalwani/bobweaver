﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Inventory
{
    public partial class Inventory : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				DefaultFilter filters = new DefaultFilter
				{
					DefaultTypes = new List<string> { },
					DefaultModels = new List<int>(),
					IsExpandType = true,
					IsExpandModel = true
				};
				InventoryUserControl.DealerCity = DealerCity;
				InventoryUserControl.DealerState = DealerState;
				InventoryUserControl.DefaultFilter = filters;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}