﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Finances
{
    public partial class PaymentCalc : BasedPage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				if (!IsPostBack)
				{
					string callingPage = Request.QueryString["CallingPage"].ToString();
					string vin = Request.QueryString["Vin"].ToString();
					string price = Request.QueryString["Price"].ToString();

					price = price.Replace("$", "");
					price = price.Replace(",", "");
					txtPrice.Text = price;

					// Set close button URL
					if (callingPage.Equals("NewVehicleInfo.aspx"))
						btnClose.PostBackUrl = "../NewCars/NewVehicleInfo.aspx?Vin=" + vin;

					if (callingPage.Equals("UsedVehicleInfo.aspx"))
						btnClose.PostBackUrl = "../UseCars/UsedVehicleInfo.aspx?Vin=" + vin;
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            // Calculate monthly payment
            try
            {
                decimal? price = Convert.ToDecimal(txtPrice.Text);
                decimal? interest = Convert.ToDecimal(txtInterest.Text);
                decimal? down = Convert.ToDecimal(txtDownpay.Text);
                decimal? trade = Convert.ToDecimal(txtTrade.Text);
                decimal? calcPrice = (price - down - trade);
                decimal? totalPrice = (calcPrice * (interest / 100)) + calcPrice;
                decimal? mo12 = totalPrice / 12;
                decimal? mo24 = totalPrice / 24;
                decimal? mo36 = totalPrice / 36;
                decimal? mo48 = totalPrice / 48;
                decimal? mo60 = totalPrice / 60;
                decimal? mo72 = totalPrice / 72;

                lbl12Pay.Text = string.Format("{0:C}", mo12);
                lbl24Pay.Text = string.Format("{0:C}", mo24);
                lbl36Pay.Text = string.Format("{0:C}", mo36);
                lbl48Pay.Text = string.Format("{0:C}", mo48);
                lbl60Pay.Text = string.Format("{0:C}", mo60);
                lbl72Pay.Text = string.Format("{0:C}", mo72);
                lblError.Visible = false;
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                var el = ExceptionHelper.LogException("PaymentCalc.btnCalc_Click() Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", "Error when execute method: btnCalc_Click");
                EmailHelper.SendEmailForNotificationWhenFailing(el);
                return;
            }
        }
     
    }
}