﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PaymentCalc.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Finances.PaymentCalc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="paycalc" class="container">
        <div class="centerContent">
            <div class="row" style="text-align: center; background-color: darkgrey; width:78.25%;">
                <span style="font-size: 1.4em; font-weight: bold; color: whitesmoke;">Payment Calculator</span>
            </div>
            <section id="calc" class="row">
                <div class="col-md-3 left nopadding" style="margin-right: 3px;">
                    <div class="panel panel-default" style="height: 370px;">
                        <div class="panel-body" style="text-align: left; background-color: white; height: 370px;">
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="Label2">Enter all values as numbers</asp:Label>
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: red">
                                    <asp:Label runat="server" ID="lblError" Text="All values must be either decimals or intergers" Visible="false" />
                                </span>
                            </div>
                            <p />
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="lblPrice">Vehicle Price</asp:Label>
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%; margin-bottom: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:TextBox runat="server" ID="txtPrice" ToolTip="Enter price of the vehicle" />
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="lblInterest">Interest Rate</asp:Label>
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%; margin-bottom: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:TextBox runat="server" ID="txtInterest" ToolTip="Enter interest rate as a decimal i.e. 3.56" />
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="lblDownpay">Down Payment</asp:Label>
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%; margin-bottom: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:TextBox runat="server" ID="txtDownpay" ToolTip="Enter your down payment" />
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="lblTrade">Trade In Value</asp:Label>
                                </span>
                            </div>
                            <div class="row" style="margin-left: 5%; margin-bottom: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:TextBox runat="server" ID="txtTrade" ToolTip="Enter your trade in value" />
                                </span>
                            </div>
                            <div class="row" style="text-align: center; margin-top: 10%;">
                                <asp:Button runat="server" class="btn btn-primary" ID="btnCalc" Height="30" Width="150" Text="Calculate" OnClick="btnCalc_Click" ToolTip="Click for vehicle payments" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 midle nopadding" style="margin-right: 3px;">
                    <div class="panel panel-default" style="height: 370px;">
                        <div class="panel-body" style="text-align: left; background-color: whitesmoke; height: 370px;">
                            <div id="div12" class="row" style="margin-left: 10%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="lbl12">12 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl12Pay" />
                                </span>
                            </div>
                            <div id="div24" class="row" style="margin-left: 10%; margin-top: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="lbl24">24 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl24Pay" />
                                </span>
                            </div>
                            <div id="div36" class="row" style="margin-left: 10%; margin-top: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="Label1">36 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl36Pay" />
                                </span>
                            </div>
                            <div id="div48" class="row" style="margin-left: 10%; margin-top: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="Label3">48 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl48Pay" />
                                </span>
                            </div>
                            <div id="div60" class="row" style="margin-left: 10%; margin-top: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="Label5">60 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl60Pay" />
                                </span>
                            </div>
                            <div id="div72" class="row" style="margin-left: 10%; margin-top: 3%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: black;">
                                    <asp:Label runat="server" ID="Label7">72 Mo</asp:Label>
                                </span>
                                <span style="font-weight: bold; font-size: 1.0em; color: black; margin-left: 20px;">
                                    <asp:Label runat="server" ID="lbl72Pay" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 right nopadding">
                    <div class="panel panel-default" style="height: 370px;">
                        <div class="panel-body" style="text-align: left; background-color: white; height: 370px;">
                            <div class="row" style="margin-left: 5%;">
                                <span style="font-weight: bold; font-size: 1.0em; color: darkgray">
                                    <asp:Label runat="server" ID="Label4">Calculated payments are per month for the period</asp:Label>
                                </span>
                            </div>
                            <p />
                            <div class="row" style="text-align: center; margin-top: 10%;">
                                <asp:Button runat="server" class="btn btn-primary" ID="btnClose" Height="30" Width="150" Text="Close" ToolTip="Click to return to vehicle information" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>



        </div>
    </div>

</asp:Content>
