﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoryContent.aspx.cs" MasterPageFile="~/Site.Master" Inherits="CJI.AgileDealer.Web.WebForm.Views.UseCars.CategoryContent" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UseCars" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1><%=CategoryContent.Container_H1 %></h1>
    </div>
</asp:Content>
