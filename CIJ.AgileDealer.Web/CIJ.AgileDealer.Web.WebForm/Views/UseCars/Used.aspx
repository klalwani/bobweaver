﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Used.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.UseCars.Used" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.UseCars" %>
<%@ Register Src="~/Views/UserControl/Inventory/Inventory.ascx" TagName="InventoryUserControl" TagPrefix="inventoryuserControl" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <inventoryuserControl:InventoryUserControl runat="server" id="InventoryUserControl" />
</asp:Content>
