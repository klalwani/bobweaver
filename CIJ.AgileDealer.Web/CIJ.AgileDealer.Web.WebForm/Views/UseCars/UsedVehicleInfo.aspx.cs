﻿using CIJ.AgileDealer.Web.WebForm;
using System;

namespace CJI.AgileDealer.Web.WebForm.Views.UseCars
{
    public partial class UsedVehicleInfo : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				string vin = Request.QueryString["Vin"];

				if (string.IsNullOrEmpty(vin))
					Response.Redirect("Used.aspx?Id=1", true);

				VehicleInfo.Vin = vin;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}