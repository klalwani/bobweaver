﻿using AgileDealer.Data.Entities;
using AgileDealer.Data.Entities.Nagivation;
using CIJ.AgileDealer.Web.Base.Model;
using CIJ.AgileDealer.Web.Base.Repositories;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.UseCars
{
    public partial class Used : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				DefaultFilter filters = new DefaultFilter()
				{
					DefaultTypes = new List<string>() { "used", "certified" },
					DefaultModels = new List<int>(),
					IsExpandType = true,
					IsExpandBodyStyle = true
				};
                
				InventoryUserControl.DealerCity = DealerCity;
				InventoryUserControl.DealerState = DealerState;
				InventoryUserControl.TypeString = "Used & Certified ";
				InventoryUserControl.DefaultFilter = filters;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}
