﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CJI.AgileDealer.Web.WebForm.Views.PaymentCalculator
{
    public partial class PaymentCalculator : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				PaymentCalculatorControl.IsHideHeader = false;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}