﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="VehicleContent.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.NewCars.VehicleContent" %>
<%@ Import Namespace="CJI.AgileDealer.Web.WebForm.Resources.Views.NewCars" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="vehicle-content" class="container">
        <div class="row" style="text-align: center;">
            <h1><%=VehicleContent.Container_H1 %></h1>
        </div>
        <div class="row">
            <div class="car-configurator">
                <div class="row configurator" style="line-height: 292px; text-align: center; font-size: 22px;">
                    <p><%=VehicleContent.Row_Configurator_P %></p>
                </div>
                <div class="row">
                    <p>
                       <%=VehicleContent.Car_Configurator_Row_Configurator_Row %>
                    </p>
                </div>
            </div>
            <div class="right-menu">
                <div class="row">
                    <ul class="nav nav-pills nav-stacked">
                        <%=VehicleContent.Nav_Stacked %>
                    </ul>
                </div>
                <div class="row">
                    <div style="height: 50px; line-height: 50px;">
                        <button class="btn btn-default" style="background-color: #ffc7ce; color: #a8191f; width: 145px; height: 50px;"><%=VehicleContent.Btn_Default %></button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.right-menu .nav li').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            } else {
                $(this).addClass("active");
            }
        });
    </script>
</asp:Content>

