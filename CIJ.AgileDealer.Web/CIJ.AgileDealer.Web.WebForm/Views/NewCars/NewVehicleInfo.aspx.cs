﻿using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Web.UI.WebControls;
using CJI.AgileDealer.Web.WebForm.Models;
using System.Collections.Generic;
using System.Linq;
using CJI.AgileDealer.Web.WebForm.Utilities;

namespace CJI.AgileDealer.Web.WebForm.Views.NewCars
{
    public partial class NewVehicleInfo : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				string vin = Page.RouteData.Values["Vin"] as string;
				if (string.IsNullOrEmpty(vin))
					Response.Redirect("New.aspx?Id=1", true);

				VehicleInfo.Vin = vin;
				VehicleInfo.SendMessageToThePage += VehicleInfo_SendMessageToThePage;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

		private void VehicleInfo_SendMessageToThePage(int dealerId)
		{
            // only do this if we have more than one dealer
            if (DealerInfoHelper.DealerHeaders.Count > 1)
            {
                var headers = DealerInfoHelper.DealerHeaders.Where(c => c.HeaderID == dealerId && c.LanguageId == LanguageId).ToList();
                var header = headers.FirstOrDefault();
               // header.HrefPhone = "tel:" + header.SalesPhone;
                var headerControl = Master.FindControl("headerFormView") as FormView;
                if (headerControl != null)
                {
                    headerControl.DataSource = headers;
                    headerControl.DataBind();
                }
            }
			
		}
	}
}