﻿using CIJ.AgileDealer.Web.WebForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using AgileDealer.Data.Entities;
using CJI.AgileDealer.Web.Base.Context;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.NewCars
{
    public partial class VehicleDisplay : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				var vin = Page.RouteData.Values["Vin"] as string;

				if (string.IsNullOrEmpty(vin))
					return;

				var vehicle = GetVehicle(vin);
				BindingData(vehicle);
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }

        private void BindingData(HomeNetVehicle vehicle)
        {
            //panel-body
            lblBody.Text = vehicle.body;
            lblModelNo.Text = vehicle.modelnumber;
            var tranLiter = string.IsNullOrEmpty(vehicle.engliters) ? string.Empty : string.Format(" - {0:0.0} L", vehicle.engliters);
            lblEngine.Text = string.Format("{0} Cly{1}", vehicle.engcyls, tranLiter);
            lblTrans.Text = vehicle.trans;
            lblDriveType.Text = vehicle.standardbody;
            lblExtColor.Text = vehicle.extcolor;
            lblIntColor.Text = vehicle.intcolor;
            lblVin.Text = vehicle.vin;
            lblStock.Text = vehicle.stock;

            lblcarinfo.Text = string.Format("BUILD YOUR {0} {1}", vehicle.year, vehicle.make);
            lblmodel.Text = string.Format("{0}®", vehicle.model);
            lblcity.Text = string.Format("{0}", vehicle.epacity);
            lblhwy.Text = string.Format("{0}", vehicle.epahighway);
            
            lblTextStyle.Text = vehicle.trim;

            lblModel1.Text = string.Format("{0}", vehicle.model);
            lblTextStyle1.Text = vehicle.trim;

            imgChrome.Src = vehicle.chromemultiviewext1url;
            if (!string.IsNullOrEmpty(vehicle.imagelist))
            {
                var imgList = vehicle.imagelist.Split('|');

                rptImgList.DataSource = imgList.Skip(0).Take(8).ToList();
                rptImgList.DataBind();
            }
        }

        private HomeNetVehicle GetVehicle(string vin)
        {
            using (var db = new ServicePageDbContext())
            {
                var vehicle = db.Vehicles.FirstOrDefault(v => v.vin.Equals(vin, StringComparison.OrdinalIgnoreCase) && v.DealerID == DealerID);
                return vehicle;
            }
        }
    }
}