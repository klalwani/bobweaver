﻿using AgileDealer.Data.Entities;
using CIJ.AgileDealer.Web.WebForm;
using System;
using System.Collections.Generic;

namespace CJI.AgileDealer.Web.WebForm.Views.NewCars
{
    public partial class New : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			try
			{
				DefaultFilter filters = new DefaultFilter
				{
					DefaultTypes = new List<string> { "new" },
					DefaultModels = new List<int>(),
					IsExpandType = true,
					IsExpandModel = true
				};
				InventoryUserControl.DealerCity = DealerCity;
				InventoryUserControl.DealerState = DealerState;
				InventoryUserControl.TypeString = "New ";
				InventoryUserControl.DefaultFilter = filters;
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
        }
    }
}
