﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="NewVehicleInfo.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.NewCars.NewVehicleInfo" %>

<%@ Register Src="~/Views/UserControl/VehicleInfo/VehicleInfo.ascx" TagPrefix="vehicleInfoUserControl" TagName="VehicleInfo" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <vehicleInfoUserControl:VehicleInfo runat="server" ID="VehicleInfo" />
</asp:Content>
