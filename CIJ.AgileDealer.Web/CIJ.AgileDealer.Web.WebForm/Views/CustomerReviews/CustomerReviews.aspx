﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CustomerReviews.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.CustomerReviews.CustomerReviews" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">

        <div class="top-container row">
            <div class="inner-container">
                <div class="col-md-12">
                    <h1>Dealership Reviews<br>
                        <small>for Bob Weaver Auto in Pottsville, PA</small></h1>
                </div>

            </div>
        </div>

        <div id="dealership-reviews-page" class="row">
            <div class="body-content-wrapper">
                <div class="body-content-card padded-15">
                    <h1>How Are We Doing?</h1>
                    <p>You can always expect world-class service and selection at Bob Weaver Auto. In fact, it’s our personal approach that continues to bring our customers back time and time again. Just what is it about Bob Weaver Auto that sets it apart from the competition? There is no one more qualified to answer that question than our customers. Here you will find reviews from Bob Weaver Auto customers, many of who have been shopping with us for many years. We invite you to look around and hope you will leave a review of your own once you have been to see us. Thank you in advance for taking the time to do so. </p>
                    <div>
                        <div class="row">
                            <div class="col-sm-3 col-md-3 text-center google">
                                <div class="logo-wrap">
                                    <div class="service-logo">
                                        <img src="https://storage.googleapis.com/bob-weaver/dealership/google.png" alt="Google" title="Google">
                                    </div>
                                </div>
                                <a href="https://www.google.com/search?q=Bob+Weaver+Auto,+2174+W+Market+St,+Pottsville,+PA+17901&amp;ludocid=7777418425936689521#lrd=0x89c5ee8a443cfca5:0x6beef0da1388b971,3,5" target="_blank" class="btn custom-btn">
                                    <span>Write Review</span>
                                </a>
                            </div>
                            <div class="col-sm-3 col-md-3 text-center cars-com">
                                <div class="logo-wrap">
                                    <div class="service-logo">
                                        <img src="https://storage.googleapis.com/bob-weaver/dealership/cars-com.png" alt="Cars.com" title="Cars.com">
                                    </div>
                                </div>
                                <a href="https://www.cars.com/dealers/181873/bob-weaver/reviews/" target="_blank" class="btn custom-btn">
                                    <span>Write Review</span>
                                </a>
                            </div>
                            <div class="col-sm-3 col-md-3 text-center edmunds">
                                <div class="logo-wrap">
                                    <div class="service-logo">
                                        <img src="https://storage.googleapis.com/bob-weaver/dealership/edmunds.png" alt="Edmunds" title="Edmunds">
                                    </div>
                                </div>
                                <a href="https://www.edmunds.com/dealerships/all/pennsylvania/pottsville/BobWeaverChevroletBuickGMC/" target="_blank" class="btn custom-btn">
                                    <span>Write Review</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="customer-reviews">
                            <div class="review-list-wrap">
                                <ul class="review-list">
                                    <asp:Repeater runat="server" ID="rptCustomerReview">
                                        <ItemTemplate>
                                            <li class="review">
                                                <div class="row">
                                                    <div class="author">
                                                        <%# Eval("ReviewName") %>
                                                    </div>
                                                    <div class="post">
                                                        <%# Eval("ReviewText") %>
                                                    </div>
                                                    <div class="star-rating">
                                                        <%#CreateStarRating(Eval("StartRating")) %>
                                                    </div>
                                                    <div class="review-image">
                                                        <img src="/images/google.png" alt="Google" title="Google" />
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <style>
        .customer-reviews {
            padding: 4em;
            background: #f6f6f6;
        }

            .customer-reviews.row {
                margin-top: 20px;
                margin-left: 0px;
                margin-right: 0px;
            }

            .customer-reviews .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .customer-reviews .title {
                background: none;
            }

                .customer-reviews .title h1 {
                    text-align: center;
                    font-size: 30px;
                    font-weight: bold;
                    text-transform: uppercase;
                }

            .customer-reviews .review-list {
                padding: 10px;
            }

                .customer-reviews .review-list .review {
                    list-style-type: none;
                    float: left;
                    width: 100%;
                    height: auto;
                    background: #fff;
                    margin-bottom: 15px;
                    padding: 15px;
                    border: 1px solid #e7e7e7;
                }

                    .customer-reviews .review-list .review .star-rating s:hover {
                        color: #f75600;
                    }

                    .customer-reviews .review-list .review .star-rating s {
                        color: #f75600;
                        font-size: 18px;
                        cursor: default;
                        text-decoration: none;
                        line-height: 20px;
                    }

                    .customer-reviews .review-list .review .star-rating {
                        padding: 2px;
                        float: left;
                        clear: left;
                        width: 50%;
                    }

                    .customer-reviews .review-list .review .review-image {
                        padding: 2px;
                        float: right;
                        clear: right;
                        width: 50%;
                    }

                        .customer-reviews .review-list .review .review-image img {
                            float: right;
                            width: 80px;
                        }

                    .customer-reviews .review-list .review .star-rating s.rated:before {
                        content: "\2605";
                        color: #f75600;
                    }

                    .customer-reviews .review-list .review .star-rating s:before {
                        content: "\2606";
                    }

                    .customer-reviews .review-list .review .post {
                        font-size: 22px;
                        line-height: 1.4;
                        font-style: italic;
                        padding: 1.5em 0;
                        font-weight: 300;
                        margin-bottom: .5em;
                        outline: none;
                        float: right;
                        clear: right;
                        border-bottom: 1px solid #d4d4d4;
                    }

                    .customer-reviews .review-list .review .author {
                        font-size: 14px;
                        font-weight: bold;
                        float: left;
                        clear: left;
                    }

                    .customer-reviews .review-list .review .post {
                        font-size: 14px;
                        padding: .5em 0;
                    }



            .customer-reviews.row {
                margin-top: 0;
            }

            .customer-reviews.row {
                margin-left: 0;
                margin-right: 0;
            }

        .customer-reviews {
            padding: 0;
        }

        .customer-reviews {
            background: #f6f6f6;
        }

        .map-fill-width {
            width: 100%;
            min-height: 450px;
        }

        .custom-card:hover {
            transition: all .1s ease 0s;
            -webkit-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14), 0 3px 1px -1px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 3px 3px rgba(0,0,0,.14), 0 3px 1px -1px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
        }

        .review-card {
            position: absolute;
            margin-top: -435px;
            margin-bottom: 0;
        }

        .custom-card {
            background-color: #fff;
            -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
        }

        .half-card-container {
            padding: 24px 36px;
        }

        .custom-card:hover {
            transition: all 0.1s ease 0s;
            -webkit-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            -moz-box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 2px 3px 3px rgba(0,0,0,.14),0 3px 1px -1px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
        }

            .custom-card:hover img {
                -webkit-filter: brightness(75%);
                -webkit-transition: all 0.3s ease .1s;
                -moz-transition: all 0.3s ease .1s;
                -o-transition: all 0.3s ease .1s;
                transition: all 0.3s ease .1s;
            }

        .card-link, .card-link:hover, .card-link:visited {
            color: inherit;
            text-decoration: inherit;
        }

        .card-image img {
            width: 100%;
        }

        .custom-card h3 {
            font-size: 24px;
        }

        .custom-card h3 {
            margin-top: 20px;
            margin-left: 10px;
            margin-bottom: 10px;
        }

        .custom-card h3 {
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        .customer-reviews .review-list {
            padding: 0 10px;
        }
    </style>
</asp:Content>

