﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIJ.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;
using System.Configuration;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Views.UserControl.Home;

namespace CJI.AgileDealer.Web.WebForm.CustomerReviews
{
    public partial class CustomerReviews : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                //Show Reviews
                LoadCustomerReview();

            }
        }

        private void LoadCustomerReview()
        {
            using (var db = new ServicePageDbContext())
            {
                var reviews = db.HomePageReviews.Where(x => x.DealerID == DealerID && x.StartRating>=4).Select(t => new
                {
                    t.StartRating,
                    t.ReviewName,
                    t.ReviewText
                }).ToList();
                rptCustomerReview.DataSource = reviews;
                rptCustomerReview.DataBind();
            }
        }

        protected string CreateStarRating(object rating)
        {
            string strRating = string.Empty;

            int numberRating = int.Parse(rating.ToString());

            for (int i = 0; i < 5; i++)
            {
                if (numberRating != 0)
                {
                    strRating += "<s class='rated'>";
                    numberRating--;
                }
                else
                {
                    strRating += "<s>";
                }
            }
            strRating += "</s></s></s></s></s>";
            return strRating;
        }



    }
}