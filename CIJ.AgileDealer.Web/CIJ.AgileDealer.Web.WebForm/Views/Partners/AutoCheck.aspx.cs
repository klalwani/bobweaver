﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm.Views.Partners
{
    public partial class AutoCheck : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Vin))
                {
                    PostAsync(Vin);
                }
            }
            
            catch { }
        }

        public string Vin
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.Form["vin"]))
                    return Request.Form["vin"].ToString();
                return string.Empty;
            }
        }

        private void PostAsync(string vin)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://www.autocheck.com");
                var content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("CID", "7005768")
                ,new KeyValuePair<string, string>("PWD", "wZaLT6u4nN")
                ,new KeyValuePair<string, string>("SID", "5032771")
                ,new KeyValuePair<string, string>("VIN", vin)
                });
                using (var r = client.PostAsync("/DealerWebLink.jsp", content))
                {
                    string result = r.Result.Content.ReadAsStringAsync().Result;
                    PlaceHolder.Controls.Add(new LiteralControl(result));
                    //PlaceHolderContent.Attributes.Add("srcdoc",result);
                }
            }
        }
    }

}