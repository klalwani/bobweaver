﻿using CIJ.AgileDealer.Web.WebForm;
using System;

namespace CJI.AgileDealer.Web.WebForm.Views.Errors
{
    public partial class PageNotFound : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoad(System.EventArgs e)
        {
            Response.TrySkipIisCustomErrors = true; // For IIS 7 Integrated Pipeline -
            Response.Status = "404 Not Found";
            Response.StatusCode = 404; // return 404 so web crawlers know this path isnt exist.
            base.OnLoad(e);
        }
    }
}