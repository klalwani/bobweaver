﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ErrorPage.aspx.cs" Inherits="CJI.AgileDealer.Web.WebForm.Views.Errors.ErrorPage" %>

<%@ Register Src="~/Views/UserControl/CommonControls/MessagePage.ascx" TagPrefix="uc1" TagName="MessagePage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" id="completeCtr mdl-white-box">
        <uc1:MessagePage runat="server" ID="MessagePage" />
    </div>
</asp:Content>
