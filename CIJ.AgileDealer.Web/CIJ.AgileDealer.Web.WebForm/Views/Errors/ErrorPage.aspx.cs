﻿using CIJ.AgileDealer.Web.WebForm;
using System;

namespace CJI.AgileDealer.Web.WebForm.Views.Errors
{
    public partial class ErrorPage : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string errorMessage = Request.QueryString["error"];
			if (!string.IsNullOrEmpty(errorMessage))
			{
				MessagePage.Content = errorMessage;
			}
			else
			{
				MessagePage.Content = "You have encountered an error. Please try again later.";
			}
        }
    }
}