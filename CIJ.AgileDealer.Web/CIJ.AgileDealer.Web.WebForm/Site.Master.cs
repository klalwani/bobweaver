﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Globalization;
using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using AgileDealer.Data.Entities.Blog;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.WebForm.Enums;
using CJI.AgileDealer.Web.WebForm.Resources.Views;
using static CIJ.AgileDealer.Web.WebForm.Common.Enum;
namespace CJI.AgileDealer.Web.WebForm
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        public String MyTest = "Don";
        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            literalMenu.Text = SiteInfoHelper.MenuStringDefaultPage;

            using (var db = new ServicePageDbContext())
            {
                var languageId = SiteInfoHelper.GetLanguageId();
                BuildHeaderAndFooter(db, languageId);
                BuildMetaData(db, languageId);
            }

            if (!string.IsNullOrEmpty(Global.CurrentSession))
            {
                int buyerSessionId = BuyerSessionHelper.GetBuyerSessionBySession(this.Session, this.Request);
                HttpHelper.SetPageVisited(Request, buyerSessionId);
            }
        }

        private void BuildHeaderAndFooter(ServicePageDbContext db, int languageId)
        {

            var location = db.Locations.FirstOrDefault(c => c.LocationTypeID == 1 && c.DealerID == DealerInfoHelper.DealerID);
            var saleHours = GetHours((int)ServiceHourType.Sales);
            var serviceHours = GetHours((int)ServiceHourType.Service);

            var footer = db.Footers.Where(x => x.DealerID == DealerInfoHelper.DealerID).Distinct().FirstOrDefault();
            var footers = new List<Footer> { footer };

            // consolidate calls
            var headerModel = DealerInfoHelper.DealerHeaders.FirstOrDefault
                    (a => a.HeaderID == DealerInfoHelper.DealerID && a.LanguageId == languageId);

            // headerModel.HrefPhone = "tel:" + headerModel.SalesPhone;

            if (location != null)
            {
                if (headerModel != null)
                {
                    var headerModels = new List<HeaderModel>()
                    {
                       headerModel
                    };
                    headerFormView.DataSource = headerModels;
                }
            }

            headerFormView.DataBind();

            BindLanguageDropDown();

            rptSocial.DataSource = footers;
            //  rptSale.DataSource = saleHours;
            // rptService.DataSource = serviceHours;
            // rptService.DataBind();
            //  rptSale.DataBind();
            rptSocial.DataBind();
            rptStoreInfo.DataSource = new List<Location>() { location };
            rptStoreInfo.DataBind();

        }

        private void BindLanguageDropDown()
        {
            if (!IsPostBack)
            {
                bool showDropdown = (ConfigurationManager.AppSettings["ShowLanguageSettingDropDown"] ?? "false").Trim().Equals("true", StringComparison.OrdinalIgnoreCase);
                if (showDropdown)
                {
                    //ddlLanguage.Visible = showDropdown;

                    //ddlLanguage.SelectedIndexChanged -= LanguageSelectedIndexChanged;

                    //var langs = SiteInfoHelper.languages;
                    //ddlLanguage.DataSource = langs;
                    //ddlLanguage.DataBind();
                    //if (Session["CurrentUI"] != null)
                    //	ddlLanguage.SelectedValue = Session["CurrentUI"].ToString();

                    //ddlLanguage.SelectedIndexChanged += LanguageSelectedIndexChanged;
                }
            }

        }

        public void LanguageSelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/setlanguage?lan=" + ((DropDownList)sender).SelectedValue, true);
        }

        private void BuildMetaData(ServicePageDbContext db, int languageId)
        {
            string Amp = String.Empty;
            string Canonical = String.Empty;
            string Json = String.Empty;
            string dealerName = DealerInfoHelper.DealerName;
            string dealerCity = DealerInfoHelper.DealerCity;
            string dealerState = DealerInfoHelper.DealerState;

            string Title = string.Format(Resources.Views.SiteMaster.Title, dealerName, dealerCity);
            string Description = string.Format(Resources.Views.SiteMaster.Description, dealerName, dealerCity, dealerState);


            if (!string.IsNullOrEmpty(Request.QueryString["page"]))
            {
                string currentPage = Request.QueryString["page"];
                Title = Title + ", Page " + currentPage;
            }

            PageMetaDataModel pmd = null;
            if (Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("vcp"))
            {
                Model _model = null;
                Make _make = null;

                BlogTag defaultBlogTag = null;
                BlogTag defaultBlogTagType = null;

                string modelString = Page.RouteData.Values["Model"].ToString();
                string makeString = Page.RouteData.Values["Make"].ToString();
                string typeString = Page.RouteData.Values["Type"].ToString();
                string modelForSEO = modelString.Replace('-', ' ');

                int baseModelid = typeString.ToLower() == "used" ? (int)BlogTagEnums.BaseModelUsed : (int)BlogTagEnums.BaseModel;

                if (!string.IsNullOrEmpty(modelString))
                    _model = MasterDataHelper.GetModelByModelTranslatedName(modelString,DealerInfoHelper.DealerID);

                if (!string.IsNullOrEmpty(makeString))
                    _make = MasterDataHelper.GetMakeByName(makeString, DealerInfoHelper.DealerID);

                if (_model != null)
                {
                    defaultBlogTag = MasterDataHelper.GetBlogTagByName(_model.Name, DealerInfoHelper.DealerID).FirstOrDefault();
                    modelForSEO = _model.Name;
                }

                if (!string.IsNullOrEmpty(typeString))
                    defaultBlogTagType = MasterDataHelper.GetBlogTagByName(typeString, DealerInfoHelper.DealerID).FirstOrDefault();

                var defaultBlogEntry =
                    VehicleHelper.GetBlogEntryIdsByTag(new List<int>
                    {
                        baseModelid,
                        defaultBlogTag != null ? defaultBlogTag.BlogTagId : 0,
                        defaultBlogTagType != null ? defaultBlogTagType.BlogTagId : 0
                    }, languageId, DealerInfoHelper.DealerID).FirstOrDefault(x => defaultBlogTag != null && x.PostTitle.ToLower().Contains(defaultBlogTag.TagText.ToLower())); // Base Model


                //Find Blog
                if (defaultBlogEntry != null)
                {
                    Description = defaultBlogEntry.PostSummary;
                }
                else
                {
                    // Description = "View our inventory of [Type][Make](model) avaialble in (city), (state)";
                    Description = string.Format(Resources.Views.SiteMaster.VcpDescription,
                        CommonHelper.FirstLetterToUpper(typeString), CommonHelper.FirstLetterToUpper(makeString), modelForSEO, dealerCity, dealerState);
                }

                //Title = "[Type] [Make] (model) avaialble in (city), (state) for Sale";
                Title = string.Format(Resources.Views.SiteMaster.VcpTitle,
                    CommonHelper.FirstLetterToUpper(typeString), CommonHelper.FirstLetterToUpper(makeString), modelForSEO, dealerCity, dealerState);
                string QueryParameters = Request.Url.Query;

                if (!string.IsNullOrEmpty(QueryParameters))
                {
                    Canonical = Request.Url.ToString().ToLower().Replace(QueryParameters, "");
                }
                else
                    Canonical = Request.Url.ToString().ToLower();

            }
            else
            {
                var isCMS = CMSHelper.IsCMS(Page);

                if (isCMS)
                {
                    var content = CMSHelper.GetContentPageMetaData(Page, SiteInfoHelper.GetLanguageId(), DealerInfoHelper.DealerID);
                    if (content != null)
                    {
                        Canonical = content.Canonical;
                        Description = content.Description;
                        Title = content.Title;
                        Amp = content.AmpLink;
                    }
                    else
                    {
                        Response.Write("The content page is empty");
                        Response.End();
                    }
                }
                else
                {
                    IQueryable<PageMetaData> pageMetaDatas = db.PageMetaDatas
                                .Where(c => c.DealerID == DealerInfoHelper.DealerID && (c.PageName == Request.AppRelativeCurrentExecutionFilePath ||
                                c.PageName == Page.AppRelativeVirtualPath));

                    if (pageMetaDatas == null || !pageMetaDatas.Any())
                    {
                        pageMetaDatas = db.PageMetaDatas.Where(f => f.DealerID == DealerInfoHelper.DealerID && f.PageName == "~/default.aspx").AsQueryable();
                    }

                    IQueryable<PageMetaDataModel> querypmd = from pageMeta in pageMetaDatas
                                                             join pageMeta_Tran in db.PageMetaDatas_Trans.Where(c => c.LanguageId == languageId)
                                                             on pageMeta.PageMetaDataId equals pageMeta_Tran.PageMetaDataId into tblTrans
                                                             from tbl in tblTrans.DefaultIfEmpty()
                                                             select new PageMetaDataModel()
                                                             {
                                                                 Title = tbl.Title ?? pageMeta.Title,
                                                                 Description = tbl.Description ?? pageMeta.Description,
                                                                 ParentLink = pageMeta.ParentLink,
                                                                 AmpLink = pageMeta.AmpLink
                                                             };
                    pmd = querypmd.FirstOrDefault();
 
                    if (pmd != null)
                    {
                        if (!String.IsNullOrEmpty(pmd.Title))
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["page"]))
                            {
                                string currentPage = Request.QueryString["page"];
                                Title = pmd.Title + ", Page " + currentPage;
                            }
                            else
                            {
                                Title = pmd.Title;
                            }
                        }
                        if (!String.IsNullOrEmpty(pmd.ParentLink))
                            Canonical = pmd.ParentLink;
                        if (!String.IsNullOrEmpty(pmd.AmpLink))
                            Amp = pmd.AmpLink;
                        if (!String.IsNullOrEmpty(pmd.Description))
                            Description = pmd.Description;
                    }
                }

                if (string.IsNullOrEmpty(Canonical))
                {
                    Canonical = Request.Url.ToString();
                }
                Canonical = Canonical.ToLower();// per Dave's request make sure cannonical is all lower cases

                Dealer dealer = db.Dealers.FirstOrDefault(f => f.DealerID == DealerInfoHelper.DealerID);
                if (dealer != null && !String.IsNullOrEmpty(dealer.JsonId))
                    Json = dealer.JsonId.Replace("@@DESCRIPTION", Description);
            }


            if (!String.IsNullOrEmpty(Description))
                MetaDescriptionHolder.Controls.Add(new LiteralControl(String.Format(@"<meta name=""description"" content=""{0}"" />", Description)));

            if (!String.IsNullOrEmpty(Title))
                this.TitlePlaceHolder.Controls.Add(new LiteralControl(Title));
            else
                TitlePlaceHolder.Visible = false;

            if (!String.IsNullOrEmpty(Canonical))
                this.CanonicalPlaceHolder.Controls.Add(new LiteralControl(String.Format(@"<link rel=""canonical"" href=""{0}"" />", Canonical)));
            else
                CanonicalPlaceHolder.Visible = false;

            if (!String.IsNullOrEmpty(Amp))
                this.AmpPlaceHolder.Controls.Add(new LiteralControl(String.Format(@"<link rel=""amphtml"" href=""{0}"" />", Amp)));
            else
                AmpPlaceHolder.Visible = false;

            if (!String.IsNullOrEmpty(Json))
                this.JSONPlaceHolder.Controls.Add(new LiteralControl(Json));
            else
                JSONPlaceHolder.Visible = false;
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
        }

        protected void subNavBar_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var htmlAnchor = (HtmlAnchor)e.Item.FindControl("subNavLink");
            if (htmlAnchor.Name == "Parts" || htmlAnchor.Name == "Employment")
            {
                htmlAnchor.Attributes.Add("target", "external");
            }
        }
        protected void navBar_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var currentRepeaterNode = (SiteMapNode)e.Item.DataItem;
            var nodes = new SiteMapNodeCollection();

            foreach (SiteMapNode n in currentRepeaterNode.ChildNodes)
            {
                // Lets not adding the content pages to the menu
                if (n.Title.EndsWith("Page"))
                    continue;

                n.ReadOnly = false;
                n.Title = SiteMapLocalizations.ResourceManager.GetString(n.ResourceKey + "Title", CultureInfo.CurrentCulture);
                n.Description = SiteMapLocalizations.ResourceManager.GetString(n.ResourceKey + "Description", CultureInfo.CurrentCulture);

                nodes.Add(n);
            }

            ((Repeater)e.Item.FindControl("subNavDropdown")).DataSource = nodes;

            // show dropdown menu here 
            if (nodes != null && nodes.Count > 0)
            {
                var htmlAnchor = (HtmlAnchor)e.Item.FindControl("navLink");
                var htmlGenericControl = (HtmlGenericControl)e.Item.FindControl("subNavLink");
                htmlAnchor.Attributes.Add("class", "dropdown-toggle");
                //htmlAnchor.Attributes.Add("data-toggle", "dropdown");  // comment to enable parent menu clickable
                var span = new LiteralControl("<span class='caret'></span>");
                htmlAnchor.Controls.Add(span);
                htmlGenericControl.Attributes.Add("class", "dropdown");

            }
            else // hide the empty ul element
            {
                var obj = (HtmlGenericControl)e.Item.FindControl("subNavList");
                if (obj != null)
                {
                    obj.Attributes.Add("style", "display:none !important;");
                }
            }

            ((Repeater)e.Item.FindControl("subNavDropdown")).DataBind();
        }

        public List<DealerModel> GetHours(int hourtypeId)
        {
            var langid = SiteInfoHelper.GetLanguageId();

            List<DealerModel> ddm = new List<DealerModel>();

            var hours = MasterDataHelper.GetHours(DealerInfoHelper.DealerID, langid, hourtypeId);

            if (hours != null)
            {

                foreach (var hr in hours)
                {
                    ddm.Add(new DealerModel { DayHour = hr.ShortDayName + "  " + hr.Time });
                }
            }

            return ddm;
        }

        protected void headerFormView_DataBound(object sender, EventArgs e)
        {
            if (headerFormView.CurrentMode == FormViewMode.ReadOnly)
            {
                //Check the RowType to where the Control is placed
                if (headerFormView.Row.RowType == DataControlRowType.DataRow)
                {
                    HtmlAnchor phoneRef = (HtmlAnchor)headerFormView.FindControl("phoneRef");
                    if (phoneRef != null && phoneRef.HRef == "tel:")
                    {
                        phoneRef.Visible = false;
                    }
                    else
                    {
                        var headerModel = DealerInfoHelper.DealerHeaders.FirstOrDefault
                   (a => a.HeaderID == DealerInfoHelper.DealerID && a.LanguageId == 1);
                        phoneRef.HRef = "tel:" + headerModel.SalesPhone;
                    }
                    HtmlAnchor personalizationLogo = (HtmlAnchor)headerFormView.FindControl("personalizationLogo") as HtmlAnchor;
                    if (personalizationLogo == null) return;
                    HtmlImage personalImage = (HtmlImage)personalizationLogo.FindControl("logo");
                    if (personalImage != null && string.IsNullOrEmpty(personalImage.Src))
                    {
                        personalizationLogo.Visible = false;
                    }
                }
            }
        }
    }

}