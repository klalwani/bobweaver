﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;
using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.Base.Context;
using CJI.AgileDealer.Web.WebForm.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.WebForm;

namespace CJI.AgileDealer.Web.WebForm
{
    public partial class _Default : BasedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadSliderImage();
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        private void LoadSliderImage()
        {
            using (var db = new ServicePageDbContext())
            {
                var slideEntry = db.HomePageSliderEntrys.FirstOrDefault(s => s.IsActive && s.DealerID == DealerID);
                if (slideEntry != null)
                {
                    var homeSlide = db.HomePageSlides.Where(s => s.SlideEntryId == slideEntry.SliderEntryId && s.DealerID == DealerID).OrderBy(x => x.SlideNumber).Select(t => new
                    {
                        t.SlideNumber,
                        t.SlideImageName,
                        t.SlideImagePath,
                        t.SlideLink,
                        Active = t.SlideNumber == 0 ? "active" : ""

                    }).ToList();

                    //    rptSliderNumber.DataSource = homeSlide;
                    //    rptSliderNumber.DataBind();

                    rptSliderImage.DataSource = homeSlide;
                    rptSliderImage.DataBind();
                }

            }

        }
    }
}