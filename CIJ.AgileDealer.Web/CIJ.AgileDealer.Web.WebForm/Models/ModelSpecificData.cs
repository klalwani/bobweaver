﻿using AgileDealer.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class ModelSpecificData
    {
       public List<MasterData> EnginesList { get; set; }
        public List<MasterData> VinList { get; set; }
        public List<MasterData> YearList { get; set; }
        public List<MasterData> TrimList { get; set; }

        public List<MasterData> ChromeStyles { get; set; }
        public List<MasterData> BodyStyleList { get; set; }
    }
}