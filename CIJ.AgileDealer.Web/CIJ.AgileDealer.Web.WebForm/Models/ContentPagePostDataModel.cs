﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
	public class ContentPagePostDataModel
	{
		public int ContentPagePostDataId { get; set; }
		public string EmailField { get; set; }
		public string UserEmailField { get; set; }
		public string PostTitle { get; set; }
		public string PostToMethod { get; set; }
		public string EmailTo { get; set; }
		public string EmailCC { get; set; }
		public string DataFormat { get; set; }
		public string ResponseText { get; set; }
		public string EmailBCC { get; set; }
		public int ContentPageId { get; set; }
	}
}