﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class InventoryExcelDataModel
    {

        public string TYPE { get; set; }
        public string VIN { get; set; }
        public string STOCKNUMBER { get; set; }
        public int YEAR { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SELLINGPRICE { get; set; }
        public string MSRP { get; set; }
        public string DISCOUNT { get; set; }
        public string FinalPrice {get;set;}
        public string ConditionalFinalPrice { get; set; }
        public string ManufacturerIncentives { get; set; }
        public string ConditionalIncentives { get; set; }
    }
}