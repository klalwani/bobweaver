﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
	public class ContentPageModel
	{
		public int MenuId { get; set; }
		public int ContentPageId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Canonical { get; set; }
		public string AmpLink { get; set; }
		public int Order { get; set; }
		public bool IsActive { get; set; }
		public string BodyContent { get; set; }
        public string BodyContentBottom { get; set; }
        
        public bool ShowInventory { get; set; }
        public string InventoryFilters { get; set; }
    }
}