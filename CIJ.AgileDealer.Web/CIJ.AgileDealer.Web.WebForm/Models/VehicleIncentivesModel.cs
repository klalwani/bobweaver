﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class VehicleIncentivesModel
    {
        public int AgileVehicleId { get; set; }
        public int ProgramID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal Amount { get; set; }
        public string Id { get; set; }
        public string Vin { get; set; }
        public string Position { get; set; }
        public string IncentiveType { get; set; }
        public int Active { get; set; }
        public int DealerId { get; set; }
        public int IncentivetranslatedId { get; set; }
        public int DiscountType { get; set; }
        public int HideOtherIncentives { get; set; }
        public int IsMasterIncentive { get; set; }
        public string ChromeStyleId { get; set; }
        public int ChromeStyleIdYear { get; set; }
        public int TotalVehicles { get; set; }
    }
}