﻿namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class IncentiveTranslatedModelDapper
    {
        public int IncentiveTranslatedId { get; set; }
        public int DisplaySection { get; set; }
        public int Position { get; set; }
        public string TranslatedName { get; set; }
        public string ProgramName { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Detail { get; set; }
        public string Disclaimer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool IsActive { get; set; }
        public string Vin { get; set; }
        public int ModelId { get; set; }
        public bool IsCashCoupon { get; set; }
        public bool IsCustomDiscount { get; set; }
        public string Amount { get; set; }
        public string ExcludeEngineCodes { get; set; }
        public string ExcludeVINs { get; set; }
        public bool HasSetDateRange { get; set; }
        public string excludeTrimId { get; set; }
        public string ExcludeYear { get; set; }
        public bool HideOtherIncentives { get; set; }
        public int DiscountType { get; set; }
        public bool HideDiscounts { get; set; }
        public string excludeBodyStyleId { get; set; }
        public bool IsMasterIncentive { get; set; }
        public string ChromeStyleId { get; set; }
        public int ChromeStyleIdYear { get; set; }
        public string IncludeMasterIncentiveVins { get; set; }
        public bool IsExcluded { get; set; }
    }
}