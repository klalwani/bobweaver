﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class PeriodTypeModel
    {
        public int TermInMonths { get; set; }
        public string PeriodTypeName { get; set; }
    }
}