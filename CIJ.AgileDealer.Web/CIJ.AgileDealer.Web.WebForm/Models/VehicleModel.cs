﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Content;

namespace CIJ.AgileDealer.Web.WebForm.Models
{
    public class VehicleModel
    {
        public string Vin { get; set; }
        public string Stock { get; set; }
        public string ExtColor { get; set; }
        public string IntColor { get; set; }
        public string Engcyls { get; set; }
        public string EngDescription { get; set; }
        public string Engine
        {
            get
            {
                return Engcyls + " Cyl - " + EngDescription;
            }
        }
        public string Trans { get; set; }
        public string DriveTrain { get; set; }
        public int MPGCity { get; set; }
        public int MPGHighway { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VehicleInfo
        {
            get
            {
                return "BUILD YOUR " + Year.ToString() + " " + Make;
            }
        }
        public string Chromemultiviewext1url { get; set; }
        public string MSRP { get; set; }
        public string SellingPrice { get; set; }
        public int Mileage { get; set; }
        public bool IsNew { get; set; }
        public string ImageFile { get; set; }
    }
}