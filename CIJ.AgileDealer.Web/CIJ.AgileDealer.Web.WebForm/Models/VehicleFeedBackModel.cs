﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class VehicleFeedBackModel
    {
        public int VehicleFeedBackId { get; set; }
        public string AgileVehicleId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int? BuyerSessionId { get; set; }
        public bool IsCompleted { get; set; }
        public virtual string Vin { get; set; }
        public virtual string DealerName { get; set; }
        public virtual int DealerID { get; set; }
    }
}