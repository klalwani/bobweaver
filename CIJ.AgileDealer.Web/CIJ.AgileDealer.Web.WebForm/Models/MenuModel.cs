﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
	public class MenuModel
	{
		public int MenuId { get; set; }
		public string Title { get; set; }
		public string Url { get; set; }
		public string Name { get; set; }
		public int DisplayOrderId { get; set; }
		public string Description { get; set; }
		public bool ShowOnMenu { get; set; }
		public bool ShowOnSiteMap { get; set; }
		public bool ShowOnSiteMapXML { get; set; }
		public bool IsAdminOnly { get; set; }
		public bool IsActive { get; set; }
		public int? ParentId { get; set; }
		public int LanguageId { get; set; }
		public int DealerID { get; set; }
		public List<MenuModel> Childs { get; set; }
	}
}