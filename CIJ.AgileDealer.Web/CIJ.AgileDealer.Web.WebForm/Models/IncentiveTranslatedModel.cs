﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Enum = CIJ.AgileDealer.Web.WebForm.Common.Enum;
namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class IncentiveTranslatedModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Detail { get; set; }
        public string Disclaimer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? DisplaySection { get; set; }
        public string DisplaySectionName
        {
            get
            {
                if (DisplaySection.HasValue && DisplaySection > 0)
                {
                    Enum.DisplaySection section = (Enum.DisplaySection)DisplaySection.Value;
                    var name = EnumHelper.GetDescriptionFromEnumValue(section);
                    if (!string.IsNullOrEmpty(name))
                        return name;
                }
                return string.Empty;
            }
        }
        public string DisplaySectionGroupName { get; set; }
        public int? Position { get; set; }
        public string TranslatedName { get; set; }
        public bool? IsActive { get; set; }
        public string Status
        {
            get
            {
                if (IsActive.HasValue && IsActive == true)
                {
                    return "Active";
                }
                return "Hidden";
            }
        }

        public string StatusString { get; set; }
        public string IsActiveString { get; set; }
        public string Amount { get; set; }
        public int UpdateFor { get; set; }
        public string UpdateForVin { get; set; }
        public int UpdateForModelId { get; set; }
        public string Conditional { get; set; }
        public bool IsResetAll { get; set; }
        public bool IsCashCoupon { get; set; }

        public string[] excludeenginecodes { get; set; }
        public string excludeenginecodesData { get; set; }
        public string ExcludeEngingCodeUserFriendlyName { get; set; }
        public string ExcludeVINs { get; set; }

        public string[] ExcludeVINArrayData { get; set; }

        public string[] excludeTrims { get; set; }
        public string excludeTrimId { get; set; }
        public string[] excludeBodyStyles { get; set; }
        public string excludeBodyStyleId { get; set; }
        public string ExcludeTrimUserFriendlyName { get; set; }
        public string ExcludeYear { get; set; }

        public string[] ExcludeYearsArrayData { get; set; }

        public bool HideOtherIncentives { get; set; }
        public bool HideDiscounts { get; set; }
        public Int16 DiscountType { get; set; }

        public string DiscountTypeFriendlyName { get; set; }

        public bool HasSetDateRange { get; set; }

        public bool IsMasterIncentive { get; set; }

        public string ChromeStyleId { get; set; }

        public int ChromeStyleIdYear { get; set; }

        public string IncludeMasterIncentiveVins { get; set; }
    }
}