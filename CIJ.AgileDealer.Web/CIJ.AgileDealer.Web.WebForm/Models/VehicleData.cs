﻿using AgileDealer.Data.Entities.Vehicles;
using CJI.AgileDealer.Web.WebForm.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    [Serializable()]
    public class VehicleData
    {
        public string Vin { get; set; }
        public int AgileVehicleId { get; set; }
        public string Stock { get; set; }
        public string ExtColor { get; set; }
        public string ExtColorTranslatedName { get; set; }
        public string IntColor { get; set; }
        public string Engcyls { get; set; }
        public string EngDescription { get; set; }
        public string Engine
        {
            get
            {
                return Engcyls + " Cyl - " + EngDescription;
            }
        }
        public string Trans { get; set; }
        public string DriveTrain { get; set; }
        public int MPGCity { get; set; }
        public int MPGHighway { get; set; }
        public int Year { get; set; }
        public int MakeId { get; set; }
        public string Make { get; set; }
        public string MakeTranslatedName { get; set; }
        public string Model { get; set; }
        public int ModelId { get; set; }
        public int ModelTranslatedId { get; set; }
        public int ExteriorColorId { get; set; }
        public int ExteriorColorTranslaredId { get; set; }
        public int FuelTypeId { get; set; }
        public string FuelType { get; set; }
        public int EngineId { get; set; }
        public int DriveId { get; set; }
        public string Trim { get; set; }
        public string VehicleInfo
        {
            get
            {
                return "BUILD YOUR " + Year.ToString() + " " + Make;
            }
        }
        public string Chromemultiviewext1url { get; set; }
        public decimal? MSRP { get; set; }
        public decimal? NonEditedSellingPrice { get; set; }
        public decimal? SellingPrice
        {
            get
            {
                decimal? currentValue = 0;

                currentValue = OriginalSellingPrice;
                if (IncentiveDisplayTypeId == (int)DealerIncentiveTypeEnum.Normal)
                {
                    if (Rebate != null && Rebate > 0)
                    {
                        currentValue = currentValue - Math.Abs(Rebate);
                    }
                }

                return currentValue;
            }
        }

        public decimal OriginalSellingPrice { get; set; }

        [NotMapped]
        public double ConditionalSellingPrice
        {
            //get
            //{
            //    decimal price = ConditionalOfferPrice;
            //    return (double)price;
            //}
            get
            {
                decimal price = 0;
                if (IsUpfitted)
                {
                    if (OriginalSellingPrice > MSRP + UpfittedCost)
                    {
                        price = OriginalSellingPrice - NormalOfferPrice - ConditionalOfferPrice;
                    }
                    else
                    {
                        price = OriginalSellingPrice - NormalOfferPrice - ConditionalOfferPrice;
                    }
                }
                else
                {
                    price = OriginalSellingPrice - NormalOfferPrice - ConditionalOfferPrice;
                }

                return (double)price;
            }
        }

        [NotMapped]
        public double NormalSellingPrice
        {
            //get
            //{
            //    decimal price = OriginalSellingPrice - NormalOfferPrice;
            //    return (double)price;
            //}
            get
            {
                decimal price = 0;
                if (IsUpfitted)
                {
                    if (OriginalSellingPrice > MSRP + UpfittedCost)
                    {
                        price = OriginalSellingPrice - NormalOfferPrice - Convert.ToDecimal(DealerSaving); ;
                    }
                    else
                    {
                        if (EmployeePrice > 0)
                            price = Convert.ToDecimal(OriginalSellingPrice + UpfittedCost) - NormalOfferPrice - Convert.ToDecimal(DealerSaving);
                        else
                            price = Convert.ToDecimal(MSRP + UpfittedCost) - NormalOfferPrice - Convert.ToDecimal(DealerSaving); 

                    }
                }
                else if (EmployeePrice > 0)
                    price = OriginalSellingPrice - NormalOfferPrice - Convert.ToDecimal(DealerSaving);
                else
                {
                    price = OriginalSellingPrice - NormalOfferPrice;
                }
                return (double)price;
            }
        }

        public decimal? DealerSave { get; set; }
        public int Mileage { get; set; }
        public bool IsNew { get; set; }
        public bool IsCert { get; set; }
        public bool IsLift { get; set; }
        public bool IsUpfitted { get; set; }

        public bool HideIncentives { get; set; }
        public bool IsHidden { get; set; }
        public bool IsSpecial { get; set; }

        public bool IsRentalSpecial { get; set; }

        // public bool IsLimoLivery { get; set; }
        public decimal OverridePrice { get; set; }
        public string OverrideDescription { get; set; }
        public string ImageFile { get; set; }
        public string Options { get; set; }
        public string StockNum { get; set; }
        public string Convenience { get; set; }
        public string ExtEquipmt { get; set; }
        public string Safety { get; set; }
        public string Technical { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Certified { get; set; }
        public string Available { get; set; }
        public int BodyStyleId { get; set; }
        public int ImageOrder { get; set; }

        public int? BodyStyleTypeId { get; set; }
        [NotMapped]
        public int? StandardBodyStyleId { get; set; }
        public int? EngineTypeId { get; set; }
        public int? EngineTypeTranslatedId { get; set; }
        public int? MakeTranslatedId { get; set; }

        [NotMapped]
        public virtual bool IsDisplayMsrp { get; set; }

        [NotMapped]
        public virtual int TrimId { get; set; }

        [NotMapped]
        public virtual decimal Rebate { get; set; }

        [NotMapped]
        public virtual double DoubleRebate
        {
            get
            {
                double result = 0;
                if (Rebate != null && Rebate > 0)
                {
                    if (double.TryParse(Rebate.ToString(), out result))
                        return result;
                }
                return 0;
            }
        }

        [NotMapped]
        public virtual string ModelFormated { get; set; }

        [NotMapped]
        public virtual string ModelTranslated { get; set; }

        [NotMapped]
        public virtual decimal? DealerSaving { get; set; }

        [NotMapped]
        public virtual string AutowriterDescription { get; set; }

        [NotMapped]
        public virtual string UpfittedDescription { get; set; }

        [NotMapped]
        public virtual bool IsOverridePrice { get; set; }

        [NotMapped]
        public virtual bool HasSetDateRange { get; set; }

        [NotMapped]
        public virtual DateTime? OverrideStartDate { get; set; }

        [NotMapped]
        public virtual DateTime? OverrideEndDate { get; set; }

        [NotMapped]
        public virtual string TypeString { get; set; }

        [NotMapped]
        public virtual string BodyStyleTypeString { get; set; }

        [NotMapped]
        public virtual string BodyStyleString { get; set; }

        [NotMapped]
        public virtual string StandardBodyString { get; set; }

        [NotMapped]
        public virtual string EngineTypeString { get; set; }

        [NotMapped]
        public virtual string EngineString { get; set; }

        [NotMapped]
        public virtual string FuelTypeString { get; set; }

        [NotMapped]
        public virtual string PriceString { get; set; }

        [NotMapped]
        public virtual int DealerId { get; set; }


        [NotMapped]
        public virtual int IncentiveDisplayTypeId { get; set; }


        [NotMapped]
        public virtual List<ProgramListData> NormalOffers { get; set; }


        [NotMapped]
        public virtual decimal NormalOfferPrice
        {
            get
            {
                if (NormalOffers != null && NormalOffers.Any())
                {
                    var normalPrice = (from program in NormalOffers.Where(x => !x.IsCustomDiscount) select program.Amount).Sum(r => decimal.Parse(r));
                    return normalPrice;
                }
                return 0;
            }
        }



        [NotMapped]
        public virtual List<ProgramListData> ConditionalOffers { get; set; }

        [NotMapped]
        public virtual decimal ConditionalOfferPrice
        {
            get
            {
                if (ConditionalOffers != null && ConditionalOffers.Any())
                {
                    var conditionalPrice = (from program in ConditionalOffers select program.Amount).Sum(r => decimal.Parse(r));
                    return conditionalPrice;
                }
                return 0;
            }
        }

        public decimal? MSRPOverride { get; set; }

        public decimal? UpfittedCost { get; set; }
        public decimal? EmployeePrice { get; set; }

        [NotMapped]
        public virtual List<ProgramListData> AprOffers { get; set; }

        public bool HideOtherIncentives { get; set; }
        public bool HideDiscounts { get; set; }

        public bool IsMasterIncentive { get; set; }

        public string ChromeStyleId { get; set; }

        public int ChromeStyleIdYear { get; set; }


        public string IncludeMasterIncentiveVins { get; set; }

        public string StandardStyle { get; set; }

        public string VDPPageLink { get; set; }

        public string VehicleEditLink { get; set; }

        public DateTime? DateInStock { get; set; }
    }
}