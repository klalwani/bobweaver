﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class NotificationModel
    {
        public string Class { get; set; }
        public string Glyphicon { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}