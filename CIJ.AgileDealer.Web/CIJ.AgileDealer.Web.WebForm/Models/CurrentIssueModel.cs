﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class CurrentIssueModel
    {
        public int CurrentIssueId { get; set; }
        public string Name { get; set; }
    }
}