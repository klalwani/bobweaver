﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class HoursModel
    {
        public string Name { get; set; }
        public string Time { get; set; }
        public string ShortDayName { get; set; }
        public string StartTime { get; set;}
        public string EndTime { get; set; }
        public string OperationStatus { get; set; }
        public int HourTypeID { get; set; }
    }
}