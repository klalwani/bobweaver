﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Content;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class HeaderModel
    {
        public int HeaderID { get; set; }
        public string SalesPhone { get; set; }
        public string Street { get; set; }
        public string AlternatePhone { get; set; }
        public string SiteLogo { get; set; }
        public string SiteLogoAltText { get; set; }
        public string SiteLink { get; set; }
        public string PersonalizationText { get; set; }
        public string PersonalizationLogo { get; set; }
        public string PeronsalizationAltText { get; set; }
        public string PeronsalizationUrl { get; set; }
        public string CTAImage { get; set; }
        public string CTAImageAltText { get; set; }
        public string CTALink { get; set; }
        public string ExitSignButton { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public int LanguageId { get; set; }
    }
}