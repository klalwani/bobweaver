﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class DataTableModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DataTableModel()
        {
            iDisplayLength = 1;
            sSearch = string.Empty;
        }
        /// <summary>
        /// data use binding ui
        /// </summary>
        /// <value>The aa data.</value>
        public object aaData { get; set; }

        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>
        /// <value>The s echo.</value>
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        /// <value>The s search.</value>
        public string sSearch { get; set; }

        /// <summary>
        /// bool for search my task or all
        /// </summary>
        /// <value><c>true</c> if this instance is search my tasks; otherwise, <c>false</c>.</value>
        public bool isSearchMyTasks { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        /// <value>The display length of the i.</value>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        /// <value>The i display start.</value>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        /// <value>The i columns.</value>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        /// <value>The i sorting cols.</value>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        /// <value>The s columns.</value>
        public string sColumns { get; set; }

        /// <summary>
        /// custom param
        /// </summary>
        /// <value>The custom param.</value>
        public string customParam { get; set; }
    }
}