﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Vehicles;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class AgileVehicleFactoryCodeModel
    {
        
        public int AgileVehicleId { get; set; }
        public string Vin { get; set; }
        public string FactoryCodes { get; set; }
    }
}