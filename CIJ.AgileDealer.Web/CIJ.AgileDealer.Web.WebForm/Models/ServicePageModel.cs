﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class ServicePageModel
    {
        public int ServicePageID { get; set; }
        public string OilImage { get; set; }
        public string OilContent { get; set; }
        public string BrakeImage { get; set; }
        public string BrakeContent { get; set; }
        public string TireImage { get; set; }
        public string TireContent { get; set; }
    }
}