﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgileDealer.Data.Entities.Vehicles;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class VehicleSearchCriteria
    {
        public VehicleSearchCriteria()
        {
            IsNew = false;
            IsCertified = false;
            IsUsed = false;
            IsViewAll = false;
            IsSelectingPrice = false;
            IsSelectingTrim = false;
            IsSelectingModel = false;
            IsSelectingBody = false;
            IsSelectingStandardBody = false;
            IsSelectingStandardBody = false;
            Years = new List<int>();
            Makes = new List<int>();
            Prices = new List<int>();
            Models = new List<int>();
            BodyStyles = new List<int>();
            FuelTypes = new List<int>();
            ExteriorColors = new List<int>();
            Transmissions = new List<string>();
            Engines = new List<int>();
            Drives = new List<int>();
            Mileages = new List<int>();
            VehicleFeatures = new List<int>();
            ModelTrims = new List<VehicleModelTrim>();
            RemainingYears = new List<int>();
            RemainingMakes = new List<int>();
            RemainingPrices = new List<int>();
            RemainingModels = new List<int>();
            RemainingBodyStyles = new List<int>();
            RemainingFuelTypes = new List<int>();
            RemainingExteriorColors = new List<int>();
            RemainingTransmissions = new List<string>();
            RemainingEngines = new List<int>();
            RemainingDrives = new List<int>();
            RemainingMileages = new List<int>();
            RemainingModelTrims = new List<VehicleModelTrim>();
            EngineTranslatedEngines = new List<VehicleEngineTranslatedEngine>();
            VehicleBodyStandardBodyStyles = new List<VehicleBodyStandardBodyStyle>();
        }

        public string Vin { get; set; }
        public bool IsNew { get; set; }
        public bool IsUsed { get; set; }
        public bool IsCertified { get; set; }
        public bool IsLift { get; set; }
        public bool IsUpfitted { get; set; }
        public bool IsSpecial { get; set; }

        public bool IsRentalSpecial { get; set; }
        public bool IsHidden { get; set; }
        public bool CanBeEdit { get; set; }
        public bool IsLimo { get; set; }
        public int SortItem { get; set; }
        public List<int> Years { get; set; }
        public List<int> Makes { get; set; }
        public List<int> Prices { get; set; }
        public List<int> Models { get; set; }
        public List<VehicleModelTrim> ModelTrims { get; set; }
        public List<VehicleEngineTranslatedEngine> EngineTranslatedEngines { get; set; }
        public List<VehicleBodyStandardBodyStyle> VehicleBodyStandardBodyStyles { get; set; }
        public List<int> BodyStyles { get; set; }
        public List<int> ExteriorColors { get; set; }
        public List<string> Transmissions { get; set; }
        public List<int> FuelTypes { get; set; }
        public List<int> Engines { get; set; }
        public List<int> Drives { get; set; }
        public List<int> Mileages { get; set; }
        public List<int> VehicleFeatures { get; set; }
        public List<int> RemainingYears { get; set; }
        public List<int> RemainingMakes { get; set; }
        public List<int> RemainingPrices { get; set; }
        public List<int> RemainingModels { get; set; }
        public List<VehicleModelTrim> RemainingModelTrims { get; set; }
        public List<int> RemainingBodyStyles { get; set; }
        public List<int> RemainingExteriorColors { get; set; }
        public List<string> RemainingTransmissions { get; set; }
        public List<int> RemainingFuelTypes { get; set; }
        public List<int> RemainingEngines { get; set; }
        public List<int> RemainingDrives { get; set; }
        public List<int> RemainingMileages { get; set; }
        public List<int> RemainingVehicleFeatures { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public bool IsViewAll { get; set; }
        public bool IsExpandType { get; set; }
        public bool IsExpandModel { get; set; }
        public bool IsExpandBodyStyle { get; set; }
        public bool IsSelectingPrice { get; set; }
        public bool IsSelectingYear { get; set; }
        public bool IsSelectingMake { get; set; }
        public bool IsSelectingModel { get; set; }
        public bool IsSelectingBody { get; set; }
        public bool IsSelectingStandardBody { get; set; }
        public bool IsSelectingFuel { get; set; }
        public bool IsSelectingColor { get; set; }
        public bool IsSelectingTransmission { get; set; }
        public bool IsSelectingEngine { get; set; }
        public bool IsSelectingDrive { get; set; }
        public bool IsSelectingMileage { get; set; }
        public bool IsSelectingTrim { get; set; }
        public bool IsModelChange { get; set; }
        public bool InLitmitedMode { get; set; }
        public bool HideIncentives { get; set; }
        public int Skip => (PageNumber - 1) * PageSize;
    }
}