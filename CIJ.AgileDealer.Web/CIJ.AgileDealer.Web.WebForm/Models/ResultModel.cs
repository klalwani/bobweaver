﻿using System.Collections.Generic;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class ResultModel
    {
        public bool IsSuccess { get; set; } = true;
        public List<string> ErrorMessages { get; set; } = new List<string>();
    }

    public class ResultModel<T> : ResultModel
    {
        public T Data { get; set; }
    }
}