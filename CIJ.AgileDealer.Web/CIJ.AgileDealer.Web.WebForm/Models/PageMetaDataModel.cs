﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class PageMetaDataModel
    {
        public string PageName { get; set; }
        public string Title { get; set; }
        public string ParentLink { get; set; }
        public string AmpLink { get; set; }
        public string Description { get; set; }
    }
}