﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class MonthModel
    {
        public int MonthId { get; set; }
        public bool IsDisabled { get; set; }
        public string Name { get; set; }
    }
}