﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class DealerModel
    {
        public int DealerID { get; set; }
        public string DealerName { get; set; }
        public string DealerStreet1 { get; set; }
        public string DealerStreet2 { get; set; }
        public string DealerCity { get; set; }
        public string DealerState { get; set; }
        public string DealerZip { get; set; }
        public string DealerPhone { get; set; }
        public string DealerDay { get; set; }
        public string DealerHoursStart { get; set; }
        public string DealerHoursEnd { get; set; }
        public int DealerHoursOrder { get; set; }
        public string DayHour { get; set; }
        public string Status { get; set; }
        public int HourType { get; set; }
        public string VcpImageStockServer { get; set; }
        public string ImageServerGeneric { get; set; }
        public string ImageServer { get; set; }
    }
}