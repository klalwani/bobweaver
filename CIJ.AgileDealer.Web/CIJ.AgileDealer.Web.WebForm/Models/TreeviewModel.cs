﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
	public class TreeviewModel
	{
		public string Text { get; set; }
		public List<TreeviewModel> Nodes { get; set; }
		public int NodeId { get; set; }
		public int Index { get; set; }
		public bool IsCMS { get; set; } = false;
		public string Url { get; set; }
		public string Title { get; set; }
	}
}