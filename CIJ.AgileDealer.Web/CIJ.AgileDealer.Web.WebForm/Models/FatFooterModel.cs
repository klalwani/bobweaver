﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class FatFooterModel
    {

        public int MakeId { get; set; }
        public int ModelTranslatedId { get; set; }

        public int ModelId { get; set; }
        public string BodyStyle { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public bool Isnew { get; set; }

        public bool IsCertified { get; set; }

        public string VcpLink { get; set; }

        public int TotalCount { get; set; }

        public string Price { get; set; }
    }
}