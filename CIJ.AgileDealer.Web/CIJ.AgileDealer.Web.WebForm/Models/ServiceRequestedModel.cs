﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CJI.AgileDealer.Web.WebForm.Models
{
    public class ServiceRequestedModel
    {
        public int ServiceRequestedId { get; set; }
        public string Name { get; set; }
    }
}
