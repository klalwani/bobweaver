﻿using System;
using CJI.AgileDealer.Web.WebForm;
using CJI.AgileDealer.Web.WebForm.Utilities;
using CIJ.AgileDealer.Web.Base.Helpers;

namespace CIJ.AgileDealer.Web.WebForm
{
    public class BasedPage : System.Web.UI.Page, IBaseWebItem
    {
        public int DealerID { get { return DealerInfoHelper.DealerID; } }

        public string DealerCity { get { return DealerInfoHelper.DealerCity; } }

        public string DealerState { get { return DealerInfoHelper.DealerState; } }

        public string DealerName { get { return DealerInfoHelper.DealerName; } }

        public string DealerImageGeneric { get { return DealerInfoHelper.DealerImageGeneric; } }

        public string ImageServer { get { return DealerInfoHelper.ImageServer; } }

        public string ManufacturerName { get { return DealerInfoHelper.ManufactureName; } }

        public string EleadTrackAdress { get { return DealerInfoHelper.EleadTrackAdress; } }

        public int LanguageId { get { return SiteInfoHelper.GetLanguageId(); } }

        protected override void InitializeCulture()
        {
            try
            {
                if (Page != null && Page.Request != null && Page.Request.QueryString.Count > 0 && Session != null)
                {
                    string langParam = Page.Request.QueryString["lan"];
                    SiteInfoHelper.SetLanguage(Page, Context, langParam, Session["CurrentUI"]);
                    base.InitializeCulture();
                }
            }
            catch(Exception ex)
            {
                //
            }
        }

		public void HandleError(Exception ex)
		{
			var el = ExceptionHelper.LogException("Error: ", ex, string.Empty, System.Diagnostics.TraceEventType.Error, "CJI.AgileDealer.Web.WebForm", string.Empty);
			EmailHelper.SendEmailForNotificationWhenFailing(el);
			Response.Redirect("Error");
		}
    }
}