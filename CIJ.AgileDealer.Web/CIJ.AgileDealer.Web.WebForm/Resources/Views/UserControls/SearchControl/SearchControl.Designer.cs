﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.SearchControl {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class SearchControl {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SearchControl() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CJI.AgileDealer.Web.WebForm.Resources.Views.UserControls.SearchControl.SearchCont" +
                            "rol", typeof(SearchControl).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Search.
        /// </summary>
        public static string Accordion_Heading_Select_A {
            get {
                return ResourceManager.GetString("Accordion_Heading_Select_A", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;li class=&quot;list-group-item active list-group-custom-default&quot;&gt;&lt;span class=&quot;badge&quot;&gt;0&lt;/span&gt;All Vehicles&lt;/li&gt;
        ///                                        &lt;li class=&quot;list-group-item&quot; value=&quot;new&quot;&gt;&lt;span class=&quot;badge&quot;&gt;0&lt;/span&gt;New&lt;/li&gt;
        ///                                        &lt;li class=&quot;list-group-item&quot; value=&quot;used&quot;&gt;&lt;span class=&quot;badge&quot;&gt;0&lt;/span&gt;Used&lt;/li&gt;
        ///                                        &lt;li class=&quot;list-group-item&quot; value=&quot;certified&quot;&gt;&lt;span class=&quot;badge&quot;&gt;0&lt;/span&gt;Certified&lt;/li&gt;.
        /// </summary>
        public static string Collapse_In_UL {
            get {
                return ResourceManager.GetString("Collapse_In_UL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sort Results .
        /// </summary>
        public static string Container_Fluid_Row_Div_Span {
            get {
                return ResourceManager.GetString("Container_Fluid_Row_Div_Span", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;select id=&quot;ddlSort&quot; aria-label=&quot;sort results&quot; &gt;
        ///                                            &lt;option  value=&quot;1&quot;&gt;Price (low to high)&lt;/option&gt;
        ///                                            &lt;option value=&quot;2&quot;&gt;Price (high to low)&lt;/option&gt;
        ///                                            &lt;option value=&quot;3&quot;&gt;Year (low to high)&lt;/option&gt;
        ///                                            &lt;option value=&quot;4&quot;&gt;Year (high to low)&lt;/option&gt;
        ///                                            &lt;option value=&quot;5&quot;&gt;MPG (low to high)&lt;/option&gt;
        ///      [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Container_Fluid_Row_Div_Span_1 {
            get {
                return ResourceManager.GetString("Container_Fluid_Row_Div_Span_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0 Vehicles Found.
        /// </summary>
        public static string Container_Fluid_Row_Span {
            get {
                return ResourceManager.GetString("Container_Fluid_Row_Span", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Features.
        /// </summary>
        public static string Features_Heading_Text {
            get {
                return ResourceManager.GetString("Features_Heading_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Features.
        /// </summary>
        public static string Features_SubHeading_Text {
            get {
                return ResourceManager.GetString("Features_SubHeading_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ext. Color:.
        /// </summary>
        public static string Label_Group_Label_1 {
            get {
                return ResourceManager.GetString("Label_Group_Label_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mileage:.
        /// </summary>
        public static string Label_Group_Label_2 {
            get {
                return ResourceManager.GetString("Label_Group_Label_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to VIN #:.
        /// </summary>
        public static string Label_Group_Label_3 {
            get {
                return ResourceManager.GetString("Label_Group_Label_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Engine:.
        /// </summary>
        public static string Label_Group_Label_4 {
            get {
                return ResourceManager.GetString("Label_Group_Label_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drive:.
        /// </summary>
        public static string Label_Group_Label_5 {
            get {
                return ResourceManager.GetString("Label_Group_Label_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stock #:.
        /// </summary>
        public static string Label_Group_Label_6 {
            get {
                return ResourceManager.GetString("Label_Group_Label_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Models.
        /// </summary>
        public static string List_Group_Custom_Default_Li {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Trucks.
        /// </summary>
        public static string List_Group_Custom_Default_Li_1 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Colors.
        /// </summary>
        public static string List_Group_Custom_Default_Li_10 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Styles.
        /// </summary>
        public static string List_Group_Custom_Default_Li_2 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Prices.
        /// </summary>
        public static string List_Group_Custom_Default_Li_3 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Years.
        /// </summary>
        public static string List_Group_Custom_Default_Li_4 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Miles.
        /// </summary>
        public static string List_Group_Custom_Default_Li_5 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Makes.
        /// </summary>
        public static string List_Group_Custom_Default_Li_6 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Engines.
        /// </summary>
        public static string List_Group_Custom_Default_Li_7 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Fuels.
        /// </summary>
        public static string List_Group_Custom_Default_Li_8 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Drives.
        /// </summary>
        public static string List_Group_Custom_Default_Li_9 {
            get {
                return ResourceManager.GetString("List_Group_Custom_Default_Li_9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lifted Trucks.
        /// </summary>
        public static string List_Group_Li {
            get {
                return ResourceManager.GetString("List_Group_Li", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View All.
        /// </summary>
        public static string Non_Pagenation {
            get {
                return ResourceManager.GetString("Non_Pagenation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type .
        /// </summary>
        public static string Panel_Heading_A {
            get {
                return ResourceManager.GetString("Panel_Heading_A", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model .
        /// </summary>
        public static string Panel_Heading_A_1 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drive.
        /// </summary>
        public static string Panel_Heading_A_10 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exterior Color.
        /// </summary>
        public static string Panel_Heading_A_11 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lifted Trucks .
        /// </summary>
        public static string Panel_Heading_A_2 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Body Style.
        /// </summary>
        public static string Panel_Heading_A_3 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price.
        /// </summary>
        public static string Panel_Heading_A_4 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Years.
        /// </summary>
        public static string Panel_Heading_A_5 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mileage.
        /// </summary>
        public static string Panel_Heading_A_6 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Make.
        /// </summary>
        public static string Panel_Heading_A_7 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Engine.
        /// </summary>
        public static string Panel_Heading_A_8 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fuel Type.
        /// </summary>
        public static string Panel_Heading_A_9 {
            get {
                return ResourceManager.GetString("Panel_Heading_A_9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search....
        /// </summary>
        public static string Panel_Heading_Input_PlaceHolder {
            get {
                return ResourceManager.GetString("Panel_Heading_Input_PlaceHolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MSRP.
        /// </summary>
        public static string Pricing_Label_Small_1 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rebates Up To.
        /// </summary>
        public static string Pricing_Label_Small_2 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dealer Discount.
        /// </summary>
        public static string Pricing_Label_Small_3 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Price.
        /// </summary>
        public static string Pricing_Label_Small_4 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Final Price.
        /// </summary>
        public static string Pricing_Label_Small_5 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conditional Offers.
        /// </summary>
        public static string Pricing_Label_Small_6 {
            get {
                return ResourceManager.GetString("Pricing_Label_Small_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upfitting.
        /// </summary>
        public static string Upfitting_Label {
            get {
                return ResourceManager.GetString("Upfitting_Label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More Info.
        /// </summary>
        public static string VBox_Button_Col_Xs_12 {
            get {
                return ResourceManager.GetString("VBox_Button_Col_Xs_12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string VBox_Button_Col_Xs_12_Edit {
            get {
                return ResourceManager.GetString("VBox_Button_Col_Xs_12_Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to edit this vehicle.
        /// </summary>
        public static string VBox_Button_Col_Xs_12_Edit_Title {
            get {
                return ResourceManager.GetString("VBox_Button_Col_Xs_12_Edit_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click for more information.
        /// </summary>
        public static string VBox_Button_Col_Xs_12_Title {
            get {
                return ResourceManager.GetString("VBox_Button_Col_Xs_12_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicles Found.
        /// </summary>
        public static string VehiclesFound {
            get {
                return ResourceManager.GetString("VehiclesFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle Search Results.
        /// </summary>
        public static string Vsr_Pagenation_Aria_Label {
            get {
                return ResourceManager.GetString("Vsr_Pagenation_Aria_Label", resourceCulture);
            }
        }
    }
}
