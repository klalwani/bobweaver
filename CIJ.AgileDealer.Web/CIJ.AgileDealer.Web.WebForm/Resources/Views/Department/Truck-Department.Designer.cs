﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CJI.AgileDealer.Web.WebForm.Resources.Views.Department {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Truck_Department {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Truck_Department() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CJI.AgileDealer.Web.WebForm.Resources.Views.Department.Truck-Department", typeof(Truck_Department).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inventory .
        /// </summary>
        public static string Dropdown_Active_A {
            get {
                return ResourceManager.GetString("Dropdown_Active_A", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View All.
        /// </summary>
        public static string Dropdown_Menu_Li_A_1 {
            get {
                return ResourceManager.GetString("Dropdown_Menu_Li_A_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New.
        /// </summary>
        public static string Dropdown_Menu_Li_A_2 {
            get {
                return ResourceManager.GetString("Dropdown_Menu_Li_A_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Used.
        /// </summary>
        public static string Dropdown_Menu_Li_A_3 {
            get {
                return ResourceManager.GetString("Dropdown_Menu_Li_A_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Trucks &lt;br /&gt;
        ///                    &lt;small&gt;{1}&apos;s Truck Department&lt;/small&gt;.
        /// </summary>
        public static string Inner_Container_Col_Md_12 {
            get {
                return ResourceManager.GetString("Inner_Container_Col_Md_12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trucks.
        /// </summary>
        public static string Navbar_Brand {
            get {
                return ResourceManager.GetString("Navbar_Brand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toggle navigation.
        /// </summary>
        public static string Sr_Only {
            get {
                return ResourceManager.GetString("Sr_Only", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Review Content.
        /// </summary>
        public static string Tab_Pane_Padded_15_Review_H2 {
            get {
                return ResourceManager.GetString("Tab_Pane_Padded_15_Review_H2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specials Content.
        /// </summary>
        public static string Tab_Pane_Padded_15_Specials_H2 {
            get {
                return ResourceManager.GetString("Tab_Pane_Padded_15_Specials_H2", resourceCulture);
            }
        }
    }
}
