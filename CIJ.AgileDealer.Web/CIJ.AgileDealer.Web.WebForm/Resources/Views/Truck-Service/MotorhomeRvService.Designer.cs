﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CJI.AgileDealer.Web.WebForm.Resources.Views.Truck_Service {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MotorhomeRvService {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MotorhomeRvService() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CJI.AgileDealer.Web.WebForm.Resources.Views.Truck_Service.MotorhomeRvService", typeof(MotorhomeRvService).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RV Service and Maintenance, {0} {1}.
        /// </summary>
        public static string Bcc_Hero_Image_ALT {
            get {
                return ResourceManager.GetString("Bcc_Hero_Image_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/RV-Serviceand-Repairs-Banner.jpg.
        /// </summary>
        public static string Bcc_Hero_Image_SRC {
            get {
                return ResourceManager.GetString("Bcc_Hero_Image_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RV and Motorhome Services List.
        /// </summary>
        public static string Gator_5Row_Padded_15_H2 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_H2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transmission Service.
        /// </summary>
        public static string Gator_5Row_Padded_15_H4 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_H4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BG Fuel Injection Service.
        /// </summary>
        public static string Gator_5Row_Padded_15_H4_1 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_H4_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BG Air Conditioning Service.
        /// </summary>
        public static string Gator_5Row_Padded_15_H4_2 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_H4_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BG Brake Flush (2 Years).
        /// </summary>
        public static string Gator_5Row_Padded_15_H4_3 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_H4_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/Multiple-RV-Display.PNG.
        /// </summary>
        public static string Gator_5Row_Padded_15_Image_SRC {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_Image_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We are a Certified Warranty Repair Center for Ford Class A, B, and C..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_1 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Removes contaminated fluid from entire hydraulic system. Replace old fluid with ultra-dry high, temperature DOT3 or DOT4 brake fluid. Inspect entire system for leaks..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_10 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;BENEFITS &lt;/strong&gt; - Enhances the life of the brake fluid – increases the boiling point of the fluid – enhances the like of the seals and hoses – increases braking power..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_11 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gas.
        /// </summary>
        public static string Gator_5Row_Padded_15_P_2 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diesel.
        /// </summary>
        public static string Gator_5Row_Padded_15_P_3 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drain old transmission fluid and install new fluid..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_4 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;BENEFITS &lt;/strong&gt; - improves shift patterns. Diesel includes up to 16 qts..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_5 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cleans gas injectors. Cleans intake valves. Controls combustion chamber deposit formation..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_6 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;BENEFITS &lt;/strong&gt; - Improves overall engine performance – improves gas mileage..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_7 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cleans a/c ventilation system, reducing or eliminating harmful allergens..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_8 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;BENEFITS &lt;/strong&gt; - Creates a clean smelling ventilation system – helps quiet the system and reduce wear and tear – helps extend the life of your compressor – protects against corrosion and rust formation created by moisture..
        /// </summary>
        public static string Gator_5Row_Padded_15_P_9 {
            get {
                return ResourceManager.GetString("Gator_5Row_Padded_15_P_9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Battery Service.
        /// </summary>
        public static string Gator5_Left_H4 {
            get {
                return ResourceManager.GetString("Gator5_Left_H4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Coolant Service.
        /// </summary>
        public static string Gator5_Left_H4_1 {
            get {
                return ResourceManager.GetString("Gator5_Left_H4_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Brake and Axle Service.
        /// </summary>
        public static string Gator5_Left_H4_2 {
            get {
                return ResourceManager.GetString("Gator5_Left_H4_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Brake Inspection &amp;amp; Lube.
        /// </summary>
        public static string Gator5_Left_H4_3 {
            get {
                return ResourceManager.GetString("Gator5_Left_H4_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chassis Lubrication Service.
        /// </summary>
        public static string Gator5_Left_H4_4 {
            get {
                return ResourceManager.GetString("Gator5_Left_H4_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Battery.
        /// </summary>
        public static string Gator5_Left_P_1 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gas.
        /// </summary>
        public static string Gator5_Left_P_2 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diesel.
        /// </summary>
        public static string Gator5_Left_P_3 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Single.
        /// </summary>
        public static string Gator5_Left_P_4 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Double.
        /// </summary>
        public static string Gator5_Left_P_5 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Triple.
        /// </summary>
        public static string Gator5_Left_P_6 {
            get {
                return ResourceManager.GetString("Gator5_Left_P_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is the nation&apos;s largest Ford RV Service Facility. We have the on-site ability to handle many issues that traditional corner garages and dealerships simply cannot. Our quick and easy drop off will get you back out and enjoying your RV without the delay from sending out for unavailable parts. Let us worry about your RV, so you won&apos;t have to!.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;h2&gt;RV Service and Maintenance&lt;/h2&gt;
        ///                            &lt;p&gt;Our all-inclusive RV Maintenance and Repair Service for the Get the peace of mind knowing your maintenance is done right to avoid costly repairs down the road. Our technicians are specially trained and certified for service on Ford, ASE, Caterpillar, and Cummins engines. These certifications, special training, advanced diagnostic systems and specialized tools ensure that you&apos;re getting the absolute best service for your RV.&lt;/p&gt;
        ///            [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_1 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Schedule Service.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_A_1 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_A_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Why {0}.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_A_2 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_A_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Annual Maintenance.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_A_3 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_A_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RV Services.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_A_4 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_A_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specials.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_A_5 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_A_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full Size Service Bay and Lift, {0} {1}.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_Bcc_Hero_Image_ALT {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_Bcc_Hero_Image_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/rv-service-bay-lift.jpg.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_Bcc_Hero_Image_SRC {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_Bcc_Hero_Image_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;p&gt;RV&apos;s deserve and demand a level of touch and care somewhat different than traditional vehicles. They have more onboard systems not directly related to driving which can either positively or negatively impact your experience with your vehicle. Your {0} service technicians are trained to not only deal with specific engine types, but every aspect of your vehicle maintenance. They will keep it functioning not only in terms of motors and transmission, but throughout the minor nuances that keep your RV an enjo [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_1 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;p&gt;Some of these differences create inherent risk to an owner looking to maintain their RV at home. Parts, cables, hoses, and engine systems are set-up more complex than on most cars and therefore tend to be difficult to work on. Beyond this, make-shift parts like cables and hoses that might appear at a glance to fit your RV might, and more often than not, will be problematic in overall performance of your vehicle, risking RV&apos;s engine life and everyday workability.&lt;/p&gt;
        ///                            &lt;p&gt;In add [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_2 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;h2 class=&quot;gator-blue5&quot;&gt;Annual Preventative Maintenance&lt;/h2&gt;
        ///                                &lt;p&gt;Preventative maintenance is important, especially if your RV is stored more that it is used.&amp;nbsp; Your RV is a valuable investment and can be costly to repair if not properly maintained.&amp;nbsp; These are our maintenance recommendations based on manufacturer warranty requirements.&lt;/p&gt;.
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_3 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To achieve maximum performance and longer battery life – our service includes removal of house and chassis batteries, cleaning of terminals and posts, test cranking amps/volts, top-off fluid, clean battery tray, install acid neutralizer mat into battery tray, reinstall all batteries, apply anti-corrosive..
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_4 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Visual inspection of hoses, clamps, radiator and heater hoses – drain coolant and fill with new coolant. Prices include up to three gallons of coolant..
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_5 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;Trailers&lt;/strong&gt; - Removal of tires for inspection and testing brakes for proper operation and amp draw – removal of drum and hub, and inspect brake shoes for excessive wear – inspect and repack wheel bearings, as necessary – check and adjust air pressure in tires..
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_6 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;Diesel Motorhomes&lt;/strong&gt; - Removal of tires for inspection of brake linings, rotors and hoses – includes high pressure cleaning of brake sediment and lubrication of slack adjusters..
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_7 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;Motorhomes&lt;/strong&gt; - Only the front grease points are lubricated when engine oil change is performed – this leaves the drive train unattended – our service includes lubrication of steering linkage, drive shaft, u-joints, front axle kingpins and front suspension – plus, inspection, and top-off of wheel bearing and rear axle lubricants..
        /// </summary>
        public static string Row_Padded_15_Bcc_Content_Block_P_8 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Bcc_Content_Block_P_8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other Truck Sizes.
        /// </summary>
        public static string Row_Padded_15_Gator_Blue5 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Gator_Blue5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service Specials.
        /// </summary>
        public static string Row_Padded_15_H2 {
            get {
                return ResourceManager.GetString("Row_Padded_15_H2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/RV-Coupons-4995.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/RV-Coupons-7995.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_1 {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/RV-Coupons-17995.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_2 {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Light Duty Truck.
        /// </summary>
        public static string Row_Padded_15_IMG_3_ALT {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_3_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/Light-Duty-Truck-Thumb.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_3_SRC {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_3_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medium Duty Truck.
        /// </summary>
        public static string Row_Padded_15_IMG_4_ALT {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_4_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/Medium-Duty-Truck-Thumb.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_4_SRC {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_4_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heavy Duty Truck.
        /// </summary>
        public static string Row_Padded_15_IMG_5_ALT {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_5_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/Heavy-Duty-Truck-Thumb.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_5_SRC {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_5_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RV Truck.
        /// </summary>
        public static string Row_Padded_15_IMG_6_ALT {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_6_ALT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}content/RV/RV-Thumb.jpg.
        /// </summary>
        public static string Row_Padded_15_IMG_6_SRC {
            get {
                return ResourceManager.GetString("Row_Padded_15_IMG_6_SRC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Size.
        /// </summary>
        public static string Row_Padded_15_Span {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Light Duty.
        /// </summary>
        public static string Row_Padded_15_Span_1 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &amp;lt;10k GVW.
        /// </summary>
        public static string Row_Padded_15_Span_2 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medium Duty.
        /// </summary>
        public static string Row_Padded_15_Span_3 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 10k - 26k GVW.
        /// </summary>
        public static string Row_Padded_15_Span_4 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heavy Duty.
        /// </summary>
        public static string Row_Padded_15_Span_5 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 26k GVW+.
        /// </summary>
        public static string Row_Padded_15_Span_6 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RV.
        /// </summary>
        public static string Row_Padded_15_Span_7 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Sizes.
        /// </summary>
        public static string Row_Padded_15_Span_8 {
            get {
                return ResourceManager.GetString("Row_Padded_15_Span_8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;h1&gt;{0} RV Service and Repair &lt;br /&gt;
        ///                    &lt;small&gt;Motorhome, RV, and Large Vehicle Service Center&lt;/small&gt;&lt;/h1&gt;.
        /// </summary>
        public static string Top_Container_H1 {
            get {
                return ResourceManager.GetString("Top_Container_H1", resourceCulture);
            }
        }
    }
}
