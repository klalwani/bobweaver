﻿using AgileDealer.Common.Models;
using Dapper.Contrib.Extensions;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace AgileDealer.Common.Helper
{
    public static class ExceptionHelper
    {
        public static void LogException(string exceptionPrefix, Exception exception, string application, string contextQueries, bool isSendOutEmail = false)
        {
            try
            {
                ErrorLog el = new ErrorLog();
                Exception active = exception;
                int LoopCount = 0;
                using (SqlConnection dbLocal = new SqlConnection(ConfigurationManager.ConnectionStrings["AgileDealer"]?.ConnectionString))
                {
                    while (active != null && LoopCount < 10)
                    {
                        if (active == active.InnerException)
                            break;

                        if (active.InnerException != null && active.Message == active.InnerException.Message)
                            break;

                        //Log to the DB

                        el.Message = $"{exceptionPrefix} {active.Message}";
                        el.SequenceId = LoopCount;
                        el.Application = application;
                        el.ContextQueries = contextQueries;
                        el.Source = active.Source;
                        el.StackTrace = active.StackTrace;
                        el.DateCreated = DateTime.Now;
                        if (active.TargetSite != null)
                            el.TargetSite = active.TargetSite.ToString();

                        // dbLocal.ErrorLogs.Add(el);
                        dbLocal.Insert(el);
                        active = active.InnerException;
                        LoopCount++;
                    }
                    if (isSendOutEmail)
                        EmailHelper.SendEmailForNotificationWhenFailing(el);
                }
            }
            catch
            {
                
            }

        }
    }
}
