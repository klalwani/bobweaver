﻿using AgileDealer.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Common.Helper
{
    public class EmailHelper
    {
        private static readonly string SmtpServer = ConfigurationManager.AppSettings["smtpServer"];
        private static readonly int SmtpPort = Convert.ToInt16(ConfigurationManager.AppSettings["smtpPort"]);
        private static readonly string SmtpAcct = ConfigurationManager.AppSettings["smtpAcct"];
        private static readonly string ErrorLogEmail = ConfigurationManager.AppSettings["errorLogEmail"];
        private static readonly string SmtpAcctPwd = ConfigurationManager.AppSettings["smtpAcctPwd"];
        private static readonly string BccEmail = ConfigurationManager.AppSettings["BCCEmail"];
        public static void SendEmailWithAttachment(Email em, Attachment attachment, bool isBodyHtml = true, string bcc = "")
        {
            if (!string.IsNullOrEmpty(bcc))
            {
                em.MailBCC = bcc;
            }
            MailMessage mm = new MailMessage();
            mm.Body = em.MailText;

            if (!string.IsNullOrEmpty(em.MailCC))
            {
                em.MailCC = em.MailCC.Replace(";", ",");
                foreach (var item in em.MailCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.CC.Add(item.Trim());
                }
            }
            if (!string.IsNullOrEmpty(em.MailBCC))
            {
                em.MailBCC = em.MailBCC.Replace(";", ",");
                foreach (var item in em.MailBCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.Bcc.Add(item.Trim());
                }
            }
            mm.From = new MailAddress(SmtpAcct);

            if (!string.IsNullOrEmpty(em.MailTo))
            {
                em.MailTo = em.MailTo.Replace(";", ",");
                foreach (var item in em.MailTo.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.To.Add(item.Trim());
                }
            }

            mm.IsBodyHtml = isBodyHtml;
            if (em.MailFrom != null)
                mm.Sender = mm.From;
            mm.Subject = em.MailSubject;
            mm.Attachments.Add(attachment);
            try
            {
                SmtpClient client = new SmtpClient(SmtpServer)
                {
                    Port = SmtpPort,
                    EnableSsl = true,
                    UseDefaultCredentials = false//important: This line of code must be executed before setting the NetworkCredentials object, otherwise the setting will be reset (a bug in .NET)
                };
                NetworkCredential cred = new NetworkCredential(SmtpAcct, SmtpAcctPwd);
                client.Credentials = cred;
                client.Send(mm);

                mm.Attachments.Dispose();
                mm.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Sending Email, Method: SendEmail, Error:" + ex.Message);
            }
        }

        public static Email CreateEmail(string content, string subject)
        {
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = SmtpAcct,
                MailTo = ErrorLogEmail,
                MailSubject = subject,
                MailText = content
            };

            return email;
        }

        public static async Task SendEmailForNotificationWhenFailingForDownloadImage(string branchName, string content)
        {
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = SmtpAcct,
                MailTo = ErrorLogEmail,
                MailSubject = $"Error Log For Recon-{branchName}: Error when downloading image for vehicles",
                MailText = content
            };

            await SendEmailAsync(email).ConfigureAwait(false);

        }

        public static async Task SendEmailAsync(Email em, bool isBodyHtml = true, string bcc = "")
        {
            if (string.IsNullOrEmpty(bcc))
            {
                em.MailBCC = BccEmail;
            }
            MailMessage mm = new MailMessage
            {
                Body = em.MailText
            };

            if (!string.IsNullOrEmpty(em.MailCC))
            {
                em.MailCC = em.MailCC.Replace(";", ",");
                foreach (var item in em.MailCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.CC.Add(item.Trim());
                }
            }
            if (!string.IsNullOrEmpty(em.MailBCC))
            {
                em.MailBCC = em.MailBCC.Replace(";", ",");
                foreach (var item in em.MailBCC.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.Bcc.Add(item.Trim());
                }
            }
            mm.From = new MailAddress(SmtpAcct);

            if (!string.IsNullOrEmpty(em.MailTo))
            {
                em.MailTo = em.MailTo.Replace(";", ",");
                foreach (var item in em.MailTo.Split(','))
                {
                    if (item.Trim().Length > 0)
                        mm.To.Add(item.Trim());
                }
            }

            mm.IsBodyHtml = isBodyHtml;
            if (em.MailFrom != null)
                mm.Sender = mm.From;
            mm.Subject = em.MailSubject;

            using (var client = new SmtpClient())
            {
                try
                {
                    client.Port = SmtpPort;
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false; //important: This line of code must be executed before setting the NetworkCredentials object, otherwise the setting will be reset (a bug in .NET)
                    client.Credentials = new NetworkCredential(SmtpAcct, SmtpAcctPwd);
                    await client.SendMailAsync(mm).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error Sending Email, Method: SendEmailAsync, Error:" + ex.Message);
                }
            }
        }

        public static string CreateMailForDownloadImageErrorWithTemplate(List<KeyValuePair<string, string>> errorList)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table class=""body-wrap"">");
            sb.AppendLine(@"<tbody>");
            sb.AppendLine(@"<tr>");
            sb.AppendLine(@"<td><strong>Agile Vehicles with image error when download</strong></td>");
            sb.AppendLine(@"</tr>");

            foreach (KeyValuePair<string, string> error in errorList)
            {
                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<td>" + error.Key + "</td>");
                sb.AppendLine(@"<td>" + error.Value + "</td>");
                sb.AppendLine(@"</tr>");
            }

            sb.AppendLine(@"</tbody>");
            sb.AppendLine(@"</table>");

            return sb.ToString();
        }

        public static async Task SendEmailForNotificationWhenFailing(ErrorLog error)
        {
            string errorLog = JsonConvert.SerializeObject(error);
            Email email = new Email
            {
                CreatedUserId = 0,
                LastChangedUserId = 0,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                MailFrom = SmtpAcct,
                MailTo = ErrorLogEmail,
                MailSubject = string.Format("Error Log For Recon: " + error.ContextQueries),
                MailText = errorLog
            };

            await SendEmailAsync(email).ConfigureAwait(false);

        }
    }
}
