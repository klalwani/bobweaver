﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Common.Helper
{
    public static class CloudStorageHelper
    {
        private static CloudBlobContainer PublicContainer { get; set; }

        private static string CacheControl
        {
            get { return "public, max-age=604800"; }
        }

        private static CloudStorageAccount StorageAccount { get; set; }

        internal static string UploadFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["UploadFolder"];
            }
        }

        private static string BlobContainer
        {
            get
            {
                return ConfigurationManager.AppSettings["BlobContainer"];
            }
        }

        static CloudStorageHelper()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["StorageConnectionString"]?.ConnectionString;
            if (!string.IsNullOrEmpty(connectionString))
            {
                StorageAccount = CloudStorageAccount.Parse(connectionString);
                CloudBlobClient blobClient = StorageAccount.CreateCloudBlobClient();
                PublicContainer = blobClient.GetContainerReference(BlobContainer);
            }
        }

        #region Public Functions

        public static async Task<bool> UploadFileToAzureFromMemory(byte[] ImageData, string RelativeUrl)
        {
            bool isSuccess;
            try
            {

                if (!string.IsNullOrEmpty(RelativeUrl))
                {
                    using (var fileStream = new MemoryStream(ImageData))
                    {
                        //Create new blob for new item
                        CloudBlockBlob blockBlob = PublicContainer.GetBlockBlobReference(RelativeUrl);
                        blockBlob.Properties.CacheControl = CacheControl;
                        await blockBlob.UploadFromStreamAsync(fileStream);
                    }
                }
                isSuccess = true;

            }
            catch (Exception ex)
            {
                ExceptionHelper.LogException("Recorn Error: ", ex, "CJI.AgileDealer.Recon", "");
                isSuccess = false;
            }
            return isSuccess;
        }

        #endregion
    }
}
