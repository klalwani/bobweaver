﻿using Dapper.Contrib.Extensions;
using System;

namespace AgileDealer.Common.Models
{
    [Table("ErrorLogs")]
    public class ErrorLog
    {
        public int ErrorLogId { get; set; }
        public int SequenceId { get; set; }
        public string Application { get; set; }

        public string Message { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string TargetSite { get; set; }
        public string ContextQueries { get; set; }
        public int ContextQueryCount { get; set; }

        public DateTime DateCreated { get; set; }

        public int CreatedUserId { get; set; }
    }
}
