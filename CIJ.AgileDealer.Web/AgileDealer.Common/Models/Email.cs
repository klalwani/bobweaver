﻿using System;

namespace AgileDealer.Common.Models
{
    public class Email
    {
        public int EmailId { get; set; }

        public string MailTo { get; set; }

        public string MailFrom { get; set; }

        public string MailCC { get; set; }

        public string MailBCC { get; set; }

        public string MailSubject { get; set; }

        public string MailText { get; set; }

        public int SentByUserId { get; set; }

        public DateTime DateModified { get; set; }

        public DateTime DateCreated { get; set; }

        public int LastChangedUserId { get; set; }

        public int CreatedUserId { get; set; }
    }
}
