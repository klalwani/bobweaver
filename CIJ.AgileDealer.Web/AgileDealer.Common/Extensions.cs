﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileDealer.Common
{
    public static class Extensions
    {
        /// <summary>
        /// Thread Safe async parallel foreach 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">Data Source</param>
        /// <param name="body">Execution body</param>
        /// <param name="degreeOfParallelism">MaxDegreeOfParallelism</param>
        /// <returns></returns>
        public static Task ParallelForEachAsync<T>(this IEnumerable<T> source, Func<T, Task> body, int degreeOfParallelism = 8)
        {
            return Task.WhenAll(
                Partitioner.Create(source).GetPartitions(degreeOfParallelism).AsParallel()
                    .Select(partition => Task.Run(async () =>
                    {
                        using (partition)
                        {
                            while (partition.MoveNext())
                            {
                                await body(partition.Current);
                            }
                        }
                    }))
            );
        }
    }
}
