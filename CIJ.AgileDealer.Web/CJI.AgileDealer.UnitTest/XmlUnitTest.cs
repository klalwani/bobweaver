﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileDealer.Data.Entities.ADF;
using AgileDealer.Data.Entities.ContactUs;
using CIJ.AgileDealer.Web.Base.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CJI.AgileDealer.UnitTest
{
    [TestClass]
    public class XmlUnitTest
    {
        [TestMethod]
        public void TestReadXml()
        {
            string path = @"D:\_Projects\AgileDealer\DEV\code\CIJ.AgileDealer.Web\CIJ.AgileDealer.Web.WebForm\XMLs\EleadTrack.xml";

            //XmlHelper.WriteXml<Customer>(path, "adf","adf");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestWriteXml()
        {
            string path = @"D:\_Projects\AgileDealer\DEV\code\CIJ.AgileDealer.Web\CIJ.AgileDealer.Web.WebForm\XMLs\Temp.xml";
            ADF adf = new ADF()
            {
                Prospect = new Prospect
                {
                    RequestDate = DateTime.Now.ToString(),
                    Customer = new Customer()
                    {
                        Contact = new Contact()
                        {
                            Email = "sang.vo@codejunkiesinc.com",
                            FirstName = "Sang",
                            LastName = "Vo",
                            Address = new Address()
                            {
                                City = "HCM",
                                Country = "VN",
                                PostalCode = "70000",
                                RegionCode = "HCM",
                                Street = "VT"
                            },
                            Names = new List<Name>()
                            {
                                new Name() { Part = "first", Text = "Sang"},
                                new Name() { Part = "last", Text = "Vo"},
                            },
                            Phones = new List<Phone>()
                            {
                                new Phone() { Type = "voice", Time = "day", Text = "093 888 0264"}
                            }
                        }
                    }
                }
            };
             XmlHelper.Serialize<ADF>(adf);
            //XmlHelper.Serialize<Prospect>(adf.Prospect, path, "adf/prospect", "adf");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestGetXmlText()
        {
            string path = @"D:\_Projects\AgileDealer\DEV\code\CIJ.AgileDealer.Web\CIJ.AgileDealer.Web.WebForm\XMLs\EleadTrack.xml";
            string xml = XmlHelper.GetXmlText<Customer>(path, "adf/prospect", "prospect");
            Assert.IsTrue(!string.IsNullOrEmpty(xml));
        }
    }
}
