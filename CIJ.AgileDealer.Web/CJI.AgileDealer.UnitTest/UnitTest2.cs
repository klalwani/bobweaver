﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AgileDealer.Data.Entities.Content;
using CJI.AgileDealer.Web.Base.Models;
using CJI.AgileDealer.Web.Base.Context;

namespace CJI.AgileDealer.UnitTest
{
    /// <summary>
    /// Summary description for UnitTest2
    /// </summary>
    [TestClass]
    public class UnitTest2
    {
        public UnitTest2()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void PopulateDB()
        {
            using (var db = new ServicePageDbContext())
            {
                var user = new ApplicationUser {Id = Guid.NewGuid().ToString()};
               

                #region setup lookup
                var locTyp = new List<LocationType> {
                    new LocationType{ Name = "Type1", Active = true},
                    new LocationType{ Name = "Type2", Active = true},
                    new LocationType{ Name = "Type3", Active = true}
                };

                foreach (var locT in locTyp)
                {
                    if (!db.LocationTypes.Any(x => x.Name.Equals(locT.Name, StringComparison.OrdinalIgnoreCase)))
                        db.LocationTypes.Add(locT);
                }

                db.SaveChanges(user);

                var serviceT = new List<ServiceType> {
                    new ServiceType{ Name = "Oil-Change", Active = true},
                    new ServiceType{ Name = "Break", Active = true},
                    new ServiceType{ Name = "Tire", Active = true}
                };

                foreach (var svct in serviceT)
                {
                    if (!db.ServiceTypes.Any(x => x.Name.Equals(svct.Name, StringComparison.OrdinalIgnoreCase)))
                        db.ServiceTypes.Add(svct);
                }

                db.SaveChanges(user);
                var manu = new Manufacture { Name = "Toyota", Active = true };
                var dealer = new Dealer {  Name = "Demo" };
                dealer.Manufacture = manu;

                var loc = db.LocationTypes.FirstOrDefault();

                dealer.Locations = new List<Location> {
                    new Location{
                        City = "Tampa", State = "FL", Zip = "33614", Street1 = "3333 Dale Mabry Hwy", LocationType = loc},
                   new Location{ 
                        City = "Tampa", State = "FL", Zip = "33543", Street1 = "3333 Dale Mabry Hwy", LocationType = loc}
                };

                if (!db.Manufactures.Any(a => a.Name.Equals(manu.Name, StringComparison.OrdinalIgnoreCase)))
                    db.Manufactures.Add(manu);

                if (!db.Dealers.Any(a => a.Name.Equals(dealer.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    db.Dealers.Add(dealer);
                }

                db.SaveChanges(user);

                dealer = db.Dealers.FirstOrDefault(a => a.Name.Equals(dealer.Name, StringComparison.OrdinalIgnoreCase));

                var breakSvc = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Break", StringComparison.OrdinalIgnoreCase));
                var tireSvc = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Tire", StringComparison.OrdinalIgnoreCase));
                var oilSvc = db.ServiceTypes.SingleOrDefault(a => a.Name.Equals("Oil-Change", StringComparison.OrdinalIgnoreCase));

                var pageLayout = new PageLayout { 
                    Name = "3 column horizontal"
                };

                if (!db.PageLayouts.Any(a => a.Name.Equals(pageLayout.Name, StringComparison.OrdinalIgnoreCase)))
                    db.PageLayouts.Add(pageLayout);

                db.SaveChanges(user);

                pageLayout = db.PageLayouts.FirstOrDefault(a => a.Name.Equals(pageLayout.Name, StringComparison.OrdinalIgnoreCase));


                var displayPage = new DisplayPage { 
                Dealer = dealer,
                Name = "Service",
                Description = "",
                MainContent1 = "",
                MainContent2 = "",
                MainContent3 = "",
                PageLayout = pageLayout
                };

                if (!db.DisplayPages.Any(a => a.Name.Equals(displayPage.Name, StringComparison.OrdinalIgnoreCase)))
                    db.DisplayPages.Add(displayPage);

                db.SaveChanges(user);

                displayPage = db.DisplayPages.FirstOrDefault(a => a.Name.Equals(displayPage.Name, StringComparison.OrdinalIgnoreCase));
                
                displayPage.MainContent1 = "";
                displayPage.MainContent2 = "";
                displayPage.MainContent3 = "";
                
                
                var servicePage = new ServicePage { 
                
                    BrakeContent = @"What You Should Know About Brake Repair
One of the most common routine maintenance tasks are brake repairs and upkeep. There are two types of brakes: disc brakes 
and drum brakes. All modern cars and light trucks have front disc brakes and most have rear discs as well (though some 
lower-priced cars still come with rear drum brakes). The most common types of brake related repairs are brake pad 
replacement, a brake fluid flush, and rotor replacement/repair. 
"
                    ,TireContent = @"Our goal is to be the leaders in affordable, quality tires. We serve customers with 
the latest Dunlop, Cooper, Toyo, Michelin, BF Goodrich, Continental and Pirelli tires."

                   , OilContent = @"Changing your vehicle's oil is one of the most vital things you can do to ensure your 
car runs smooth longer, saving you a lot of money in the long run. While most drivers know it's important, they don't know 
why. When your car, truck, SUV, or van is operating, the oil undergoes thermal breakdown due to high temperatures. "
                   , DisplayPage = displayPage
                   ,Dealer = dealer

                   ,BrakeImage = "~/Content/Images/UI/brake.png"
                   ,TireImage = "~/Content/Images/UI/tires.png"
                    ,OilImage = "~/Content/Images/UI/oilchange.png"
                };


                if (!db.ServicePages.Any(a => a.DealerID == dealer.DealerID))
                    db.ServicePages.Add(servicePage);

                db.SaveChanges(user);

                // do hours here

                if (!db.Hours.Any(a => a.DealerID == dealer.DealerID))
                {
                   var svcHours = new List<Hours>{
                        new Hours{ Order = 0, ShortDayName = "Mon", StartTime= "7:00 AM", EndTime = "7:00 PM" , Dealer = dealer},
                        new Hours{ Order = 1, ShortDayName = "Tues", StartTime= "7:00 AM", EndTime = "7:00 PM" , Dealer = dealer},
                        new Hours{ Order = 2, ShortDayName = "Wed", StartTime= "7:00 AM", EndTime = "7:00 PM" , Dealer = dealer},
                        new Hours{ Order = 3, ShortDayName = "Thurs", StartTime= "7:00 AM", EndTime = "7:00 PM" , Dealer = dealer},
                        new Hours{ Order = 4, ShortDayName = "Fri", StartTime= "7:00 AM", EndTime = "7:00 PM" , Dealer = dealer},
                        new Hours{ Order = 5, ShortDayName = "Sat", StartTime= "10:00 AM", EndTime = "5:00 PM" , Dealer = dealer},
                        new Hours{ Order = 6, ShortDayName = "Sun", StartTime= "", EndTime = "", OperationStatus = "Closed" , Dealer = dealer}
                    };

                   db.Hours.AddRange(svcHours);
                   db.SaveChanges(user);
                }

                var svcTypeOil = db.ServiceTypes.SingleOrDefault(a=>a.Name.Equals("Oil-Change", StringComparison.OrdinalIgnoreCase));
                var svcTypeBreak = db.ServiceTypes.SingleOrDefault(a=>a.Name.Equals("Break", StringComparison.OrdinalIgnoreCase));
                var svcTypeTire = db.ServiceTypes.SingleOrDefault(a=>a.Name.Equals("Tire", StringComparison.OrdinalIgnoreCase));
                var svcOilList = new List<Service>();

                svcOilList.Add(new Service { Price = 22.99m,
                    Title = "Synthetic Blend only",
                    Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                    Description = "Synthetic Blend only.",
                });

                svcOilList.Add(new Service{ Price = 27.99m,
                    Title = "Synthetic Oil with 27pts inspection",
                    Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                Description = "Synthetic Oil with 27pts inspection",
                });

                svcOilList.Add(new Service{Price = 39.99m,
                    Title = "Full Synthetic Oil with Tire Rotation & 27pts inspection",
                    Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                                           Description = "Full Synthetic Oil with Tire Rotation & 27pts inspection",
                });

                foreach (var svc in svcOilList)
                {
                    svc.Image = @"~/Content/Images/UI/oilchange.png";
                    svc.ServiceType = svcTypeOil;
                    svc.Dealer = dealer;
                }

                if (!db.Services.Any(a => a.DealerID == dealer.DealerID && a.ServiceTypeID == svcTypeOil.ServiceTypeID))
                    db.Services.AddRange(svcOilList);

                var spcOilList = new List<Special>();

                    spcOilList.Add(new Special{Price = 10.99m,
                    Title = "Discount Synthetic Oil with Tire Rotation & 27pts inspection",
                    Disclaimer = "Difficulty: ... *If you plan to change your oil regularly, consider investing in a small tool set, an oil filter wrench set and a quality floor jack and stands.",
                                               Description = "Discount Synthetic Oil with Tire Rotation & 27pts inspection.",
                });

                foreach (var svc in spcOilList)
                {
                    svc.Image = @"~/Content/Images/UI/oilchange_special.png";
                    svc.ServiceType = svcTypeOil;
                    svc.Expire = DateTime.Now.AddDays(100);
                    svc.Dealer = dealer;
                }

                if (!db.Specials.Any(a => a.DealerID == dealer.DealerID && a.ServiceTypeID == svcTypeOil.ServiceTypeID))
                    db.Specials.AddRange(spcOilList);

                db.SaveChanges(user);
                #endregion


            }
        }
    }
}
