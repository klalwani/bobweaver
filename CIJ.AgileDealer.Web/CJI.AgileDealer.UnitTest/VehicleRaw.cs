﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CJI.AgileDealer.UnitTest
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("V")]
    public partial class VehicleRaw
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_0")]
        public string vehicleid { get; set; }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_1")]
        public string vin { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_2")]
        public string stock { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_3")]
        public string type { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_4")]
        public string miles { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_5")]
        public string certified { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_6")]
        public string description { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_7")]
        public string description2 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_8")]
        public string options { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_9")]
        public string dateinstock { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_10")]
        public string sellingprice { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_11")]
        public string msrp { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_12")]
        public string bookvalue { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_13")]
        public string invoice { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_14")]
        public string internetprice { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_15")]
        public string miscprice1 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_16")]
        public string miscprice2 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_17")]
        public string miscprice3 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_18")]
        public string isspecial { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_19")]
        public string ishomepagespecial { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_20")]
        public string specialprice { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_21")]
        public string specials_start_date { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_22")]
        public string specials_end_date { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_23")]
        public string specials_monthly_payment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_24")]
        public string specials_disclaimer { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_25")]
        public string year { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_26")]
        public string make { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_27")]
        public string model { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_28")]
        public string modelnumber { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_29")]
        public string trim { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_30")]
        public string styledescription { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_31")]
        public string friendlystyle { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_32")]
        public string body { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_33")]
        public string doors { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_34")]
        public string extcolor { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_35")]
        public string extcolorgeneric { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_36")]
        public string extcolor_code { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_37")]
        public string intcolor { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_38")]
        public string int_colorgeneric { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_39")]
        public string int_color_code { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_40")]
        public string extcolorhexcode { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_41")]
        public string intcolorhexcode { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_42")]
        public string intupholstery { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_43")]
        public string engcyls { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_44")]
        public string engliters { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_45")]
        public string engblock { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_46")]
        public string engaspiration { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_47")]
        public string engdescription { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_48")]
        public string engcubicinches { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_49")]
        public string trans { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_50")]
        public string transspeed { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_51")]
        public string transdescription { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_52")]
        public string drivetrain { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_53")]
        public string fueltype { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_54")]
        public string epacity { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_55")]
        public string epahighway { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_56")]
        public string epaclassification { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_57")]
        public string wheelbase { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_58")]
        public string passengercapacity { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_59")]
        public string marketclass { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_60")]
        public string comment1 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_61")]
        public string comment2 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_62")]
        public string comment3 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_63")]
        public string comment4 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_64")]
        public string comment5 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_65")]
        public string soldflag { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_66")]
        public string imagesmodifieddate { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_67")]
        public string datamodifieddate { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_68")]
        public string datamodifieddateDeprecated { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_69")]
        public string datecreated { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_70")]
        public string techspecs { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_71")]
        public string standardequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_72")]
        public string Standard { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_73")]
        public string exteriorequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_74")]
        public string interiorequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_75")]
        public string mechanicalequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_76")]
        public string safetyequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_77")]
        public string optionalequipment { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_78")]
        public string optionalequipmentshort { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_79")]
        public string factorycodes { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_80")]
        public string packagecodes { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_81")]
        public string Package { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_82")]
        public string dealeroptions { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_83")]
        public string iolvideolink { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_84")]
        public string iolmedialink { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_85")]
        public string stockimage { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_86")]
        public string imagelist { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_87")]
        public string evoxvif { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_88")]
        public string evoxsend { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_89")]
        public string evoxcolor1 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_90")]
        public string evoxcolor2 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_91")]
        public string evoxcolor3 { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_92")]
        public string standardmake { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_93")]
        public string standardmodel { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_94")]
        public string standardbody { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_95")]
        public string standardtrim { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_96")]
        public string standardyear { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_97")]
        public string standardstyle { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_98")]
        public string daysinstock { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_99")]
        public string imagecount { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_100")]
        public string dealername { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_101")]
        public string dealeraddress { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_102")]
        public string dealercity { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_103")]
        public string dealerstate { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_104")]
        public string dealerzip { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_105")]
        public string dealercontact { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_106")]
        public string dealerphone { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_107")]
        public string dealeremail { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_108")]
        public string dealerurl { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_109")]
        public string vehicletitle { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_110")]
        public string carfaxoneowner { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_111")]
        public string carfaxhistoryreporturl { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_112")]
        public string vehiclestestvideo { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_113")]
        public string toyotapackagedescriptions { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_114")]
        public string dateremoved { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_115")]
        public string flickfusionyoutubeurl { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_116")]
        public string flickfusionuploadedvideobasicurl { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_117")]
        public string sistervideolink { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_118")]
        public string chromecolorizedimagename { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_119")]
        public string chromemultiviewext1url { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_120")]
        public string chromemultiviewext2url { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_121")]
        public string chromemultiviewext3url { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_122")]
        public string chromemultiviewext1imagename { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_123")]
        public string chromemultiviewext2imagename { get; set; }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("_124")]
        public string chromemultiviewext3imagename { get; set; }

    }
}
