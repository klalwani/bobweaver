﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgileDealer.Data.Entities.Vehicles;
using CIJ.AgileDealer.Web.Base.Helpers;
using CJI.AgileDealer.Web.Base.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CJI.AgileDealer.Web.Base.Migrations;
using CJI.AgileDealer.Web.Base.Models;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CJI.AgileDealer.UnitTest
{
    [TestClass]
    public class TestConfigurations
    {
        [TestMethod]
        public void TestMapDataForEngineTranslated()
        {
            try
            {
                MasterDataGenerator masterDataGenerator = new MasterDataGenerator();
                masterDataGenerator.CreateDate();
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }


        [TestMethod]
        public void TestMapDataForMakeTranslated()
        {
            try
            {
                MasterDataGenerator masterDataGenerator = new MasterDataGenerator();
                //masterDataGenerator.MapDataForMakeTranslated();
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestCreateRoles()
        {
            try
            {
                RoleHelper roleHelper = new RoleHelper();
                roleHelper.CreateRoles(new ServicePageDbContext());
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestAssignUserToRole()
        {
            try
            {
                string userEmail = "don@codejunkiesinc.com";
                RoleHelper roleHelper = new RoleHelper();
                UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ServicePageDbContext()));
                ApplicationUser user = userManager.FindByEmail(userEmail);
                //roleHelper.AssignUserToRole(user.Id,"Admin");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestMapDataForModelTranslated()
        {
            try
            {
                MasterDataGenerator masterDataGenerator = new MasterDataGenerator();
                //masterDataGenerator.MapDataForModelTranslated();
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestCreateDealer_HomeNetVehicle()
        {
            try
            {
                MasterDataGenerator masterDataGenerator = new MasterDataGenerator();
                masterDataGenerator.CreateManufacturer();
                masterDataGenerator.CreateDealer_HomeNetVehicle(new ServicePageDbContext());
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

    }
}
