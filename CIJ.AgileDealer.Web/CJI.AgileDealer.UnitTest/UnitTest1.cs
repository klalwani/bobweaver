﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CJI.AgileDealer.Infra;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using CIJ.AgileDealer.Infra.Repositories;
using System.Collections.Generic;
using AgileDealer.Data.Entities;

namespace CJI.AgileDealer.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoadCompressedVehicles()
        {
            var homenetRepo = new HomeNetiolRepository();
            var vRepo = new VehicleRepository();
          
            var dealerIdList = new string[] { "Test123" };
            var updatedSince = new DateTime(2015, 5, 25);
            var isNew = true;
            
            var vImportList = homenetRepo.LoadCompressedVehicles(isNew, updatedSince, dealerIdList);
            
            vRepo.AddVehicle(vImportList);
            
        }

        [TestMethod]
        public void LoadVehicleFromXml()
        {
            var homenetRepo = new HomeNetiolRepository();
            var vImportList = homenetRepo.LoadVehicleFromXML(str);

        }


        string str = @"<F
  _0=""vehicleid""
  _1=""vin""
  _2=""stock""
  _3=""type""
  _4=""miles""
  _5=""certified""
  _6=""description""
  _7=""description2""
  _8=""options""
  _9=""dateinstock""
  _10=""sellingprice""
  _11=""msrp""
  _12=""bookvalue""
  _13=""invoice""
  _14=""internetprice""
  _15=""miscprice1""
  _16=""miscprice2""
  _17=""miscprice3""
  _18=""isspecial""
  _19=""ishomepagespecial""
  _20=""specialprice""
  _21=""specials_start_date""
  _22=""specials_end_date""
  _23=""specials_monthly_payment""
  _24=""specials_disclaimer""
  _25=""year""
  _26=""make""
  _27=""model""
  _28=""modelnumber""
  _29=""trim""
  _30=""styledescription""
  _31=""friendlystyle""
  _32=""body""
  _33=""doors""
  _34=""extcolor""
  _35=""extcolorgeneric""
  _36=""extcolor_code""
  _37=""intcolor""
  _38=""int_colorgeneric""
  _39=""int_color_code""
  _40=""extcolorhexcode""
  _41=""intcolorhexcode""
  _42=""intupholstery""
  _43=""engcyls""
  _44=""engliters""
  _45=""engblock""
  _46=""engaspiration""
  _47=""engdescription""
  _48=""engcubicinches""
  _49=""trans""
  _50=""transspeed""
  _51=""transdescription""
  _52=""drivetrain""
  _53=""fueltype""
  _54=""epacity""
  _55=""epahighway""
  _56=""epaclassification""
  _57=""wheelbase""
  _58=""passengercapacity""
  _59=""marketclass""
  _60=""comment1""
  _61=""comment2""
  _62=""comment3""
  _63=""comment4""
  _64=""comment5""
  _65=""soldflag""
  _66=""imagesmodifieddate""
  _67=""datamodifieddate""
  _68=""datamodifieddateDeprecated""
  _69=""datecreated""
  _70=""techspecs""
  _71=""standardequipment""
  _72=""Standard""
  _73=""exteriorequipment""
  _74=""interiorequipment""
  _75=""mechanicalequipment""
  _76=""safetyequipment""
  _77=""optionalequipment""
  _78=""optionalequipmentshort""
  _79=""factorycodes""
  _80=""packagecodes""
  _81=""Package""
  _82=""dealeroptions""
  _83=""iolvideolink""
  _84=""iolmedialink""
  _85=""stockimage""
  _86=""imagelist""
  _87=""evoxvif""
  _88=""evoxsend""
  _89=""evoxcolor1""
  _90=""evoxcolor2""
  _91=""evoxcolor3""
  _92=""standardmake""
  _93=""standardmodel""
  _94=""standardbody""
  _95=""standardtrim""
  _96=""standardyear""
  _97=""standardstyle""
  _98=""daysinstock""
  _99=""imagecount""
  _100=""dealername""
  _101=""dealeraddress""
  _102=""dealercity""
  _103=""dealerstate""
  _104=""dealerzip""
  _105=""dealercontact""
  _106=""dealerphone""
  _107=""dealeremail""
  _108=""dealerurl""
  _109=""vehicletitle""
  _110=""carfaxoneowner""
  _111=""carfaxhistoryreporturl""
  _112=""vehiclestestvideo""
  _113=""toyotapackagedescriptions""
  _114=""dateremoved""
  _115=""flickfusionyoutubeurl""
  _116=""flickfusionuploadedvideobasicurl""
  _117=""sistervideolink""
  _118=""chromecolorizedimagename""
  _119=""chromemultiviewext1url""
  _120=""chromemultiviewext2url""
  _121=""chromemultiviewext3url""
  _122=""chromemultiviewext1imagename""
  _123=""chromemultiviewext2imagename""
  _124=""chromemultiviewext3imagename"">
  <RS>
    <R
      _ID=""Test123"">
      <VS>
        
        <V
          _0=""525086976""
          _1=""ZAM45MMA4D0069448""
          _2=""F69448""
          _3=""New""
          _4=""206""
          _5=""False""
          _6=""this is a testoifdghjiuodseafgjohertgioerwtgjhioregtihojoerwtgeiuhoerwgthiuo.""
          _7=""&lt;div&gt;
	this is a testoifdghjiuodseafgjohertgioerwtgjhioregtihojoerwtgeiuhoerwgthiuo.&lt;/div&gt;
&lt;div data-iol-content=&quot;PricingMatrixDisclaimer&quot;&gt;&lt;/div&gt;
&lt;div data-iol-content=&quot;PricingMatrixDisclaimer&quot;&gt;&lt;/div&gt;""
          _8=""20&quot; birdcage design silver alloy wheels, Tire sealing compound &amp; electric air compressor, Convertible pwr soft top, Automatic bi-xenon headlights -inc: washers  adaptive light control system, Foglamps, LED taillights, Rain sensing windshield wipers, Dual auto-dimming pwr heated mirrors, Front grille w/chrome vertical bars &amp; side ornamental grilles, Rain sensor, Anti-lock braking system (ABS) w/electronic brake distribution (EBD), Roll bar system, Traction control, Stability control, 6 airbags -inc: front driver &amp; passenger 2-stage airbags  front/rear side &amp; window airbags, Front parking sensors, Rear park sensors, Front/rear seatbelts w/pretensioners, Tire pressure monitoring system, 20&quot; birdcage design silver alloy wheels, Tire sealing compound &amp; electric air compressor, Convertible pwr soft top, Automatic bi-xenon headlights -inc: washers  adaptive light control system, Foglamps, LED taillights, Rain sensing windshield wipers, Dual auto-dimming pwr heated mirrors, Front grille w/chrome vertical bars &amp; side ornamental grilles, Rain sensor, Anti-lock braking system (ABS) w/electronic brake distribution (EBD), Roll bar system, Traction control, Stability control, 6 airbags -inc: front driver &amp; passenger 2-stage airbags  front/rear side &amp; window airbags, Front parking sensors, Rear park sensors, Front/rear seatbelts w/pretensioners, Tire pressure monitoring system, Rear Wheel Drive, Power Steering, 4-Wheel Disc Brakes, Aluminum Wheels, Convertible Soft Top, HID headlights, Automatic Headlights, Fog Lamps, Intermittent Wipers, Variable Speed Intermittent Wipers, Rain Sensing Wipers, Heated Mirrors, Power Mirror(s), CD Player, Navigation System, Satellite Radio, MP3 Player, Auxiliary Audio Input, Hard Disk Drive Media Storage, Premium Sound System, Bluetooth Connection, Leather Seats, Power Driver Seat, Power Passenger Seat, Heated Front Seat(s), Seat Memory, Woodgrain Interior Trim, Power Outlet, Leather Steering Wheel, Adjustable Steering Wheel, Security System, Keyless Entry, Power Door Locks, Power Windows, Cruise Control, Climate Control, Multi-Zone A/C, Rear Defrost, Universal Garage Door Opener, Auto-Dimming Rearview Mirror, ABS, Rollover Protection Bars, Traction Control, Stability Control, Rear Side Air Bag, Rear Parking Aid, Tire Pressure Monitor""
          _9=""5/30/2013 12:00:00 AM""
          _10=""12332132""
          _11=""12312""
          _12=""0""
          _13=""0""
          _14=""0""
          _15=""0""
          _16=""12330632""
          _17=""12331132""
          _18=""False""
          _19=""False""
          _20=""0""
          _21=""""
          _22=""""
          _23=""0""
          _24=""""
          _25=""2013""
          _26=""Maserati""
          _27=""GranTurismo Convertible""
          _28=""B""
          _29=""""
          _30=""2dr Conv GranTurismo""
          _31=""2dr Conv GranTurismo""
          _32=""Convertible""
          _33=""2""
          _34=""White""
          _35=""""
          _36=""""
          _37=""TAN""
          _38=""""
          _39=""""
          _40=""""
          _41=""""
          _42=""""
          _43=""8""
          _44=""4.7L""
          _45=""V""
          _46=""MPFI""
          _47=""V8 4.7L""
          _48=""286""
          _49=""Automatic""
          _50=""6""
          _51=""6-Speed Automatic""
          _52=""RWD""
          _53=""Gasoline Fuel""
          _54=""13""
          _55=""20""
          _56=""Subcompact cars""
          _57=""115.8""
          _58=""4""
          _59=""2-door Mid-Size Passenger Car""
          _60=""http://blah/ZAM45MMA4D0069448.morestuff""
          _61=""""
          _62=""""
          _63=""""
          _64=""""
          _65=""False""
          _66=""6/2/2014 12:43:41 PM""
          _67=""5/26/2015 1:52:44 PM""
          _68=""""
          _69=""5/30/2013 2:07:58 PM""
          _70=""Summary@Vehicle Name@Maserati GranTurismo Convertible~Weight Information@Base Curb Weight (lbs)@4365~Emissions@EPA Greenhouse Gas Score@1~Trailering@Wt Distributing Hitch - Max Trailer Wt. (lbs)@~Engine@Engine Type@Gas V8~Engine@Displacement@4.7L/286~Transmission@Fifth Gear Ratio (:1)@~Transmission@Final Drive Axle Ratio (:1)@~Suspension@Suspension Type - Rear@Double-wishbone~Suspension@Suspension Type - Rear (Cont.)@Skyhook~Suspension@Stabilizer Bar Diameter - Front (in)@~Tires@Spare Tire Order Code@~Tires@Spare Tire Size@~Wheels@Rear Wheel Size (in)@20 x ~Wheels@Rear Wheel Material@Aluminum~Steering@Lock to Lock Turns (Steering)@~Steering@Turning Diameter - Wall to Wall (ft)@~Brakes@Rear Drum Diam x Width (in)@~Interior Dimensions@Front Head Room (in)@~Interior Dimensions@Front Hip Room (in)@~Interior Dimensions@Second Head Room (in)@~Exterior Dimensions@Wheelbase (in)@115.8~Exterior Dimensions@Width, Max w/o mirrors (in)@75.4~Exterior Dimensions@Liftover Height (in)@~Mileage@Fuel Economy Est-Combined (MPG)@15~Trailering@Dead Weight Hitch - Max Trailer Wt. (lbs)@~Engine@Engine Order Code@~Engine@SAE Net Horsepower @ RPM@440 @ 7000~Transmission@Trans Description Cont.@Automatic~Transmission@Second Gear Ratio (:1)@~Transmission@Reverse Ratio (:1)@~Electrical@Maximum Alternator Capacity (amps)@~Tires@Front Tire Order Code@~Tires@Rear Tire Size@P285/35ZR20~Wheels@Front Wheel Size (in)@20 x ~Wheels@Spare Wheel Size (in)@~Steering@Turning Diameter - Curb to Curb (ft)@40.4~Brakes@Brake ABS System@4-wheel~Brakes@Disc - Rear (Yes or   )@Yes~Brakes@Rear Brake Rotor Diam x Thickness (in)@13 x ~Fuel Tank@Fuel Tank Capacity, Approx (gal)@19.8~Interior Dimensions@Front Shoulder Room (in)@~Interior Dimensions@Second Shoulder Room (in)@~Exterior Dimensions@Track Width, Front (in)@62.4~Exterior Dimensions@Track Width, Rear (in)@62.6~Exterior Dimensions@Min Ground Clearance (in)@~Cargo Area Dimensions@Trunk Volume (ft³)@6.1~Summary@Body Style@2 Door Convertible~Transmission@Drivetrain@Rear Wheel Drive~Interior Dimensions@Passenger Capacity@4~Mileage@EPA Fuel Economy Est - City (MPG)@13~Mileage@EPA Fuel Economy Est - Hwy (MPG)@20~Trailering@Wt Distributing Hitch - Max Tongue Wt. (lbs)@~Engine@SAE Net Torque @ RPM@361 @ 4750~Transmission@Trans Order Code@~Transmission@Trans Type@6~Transmission@Trans Description Cont. Again@~Transmission@First Gear Ratio (:1)@~Transmission@Fourth Gear Ratio (:1)@~Electrical@Cold Cranking Amps @ 0° F (Primary)@~Emissions@Tons/yr of CO2 Emissions @ 15K mi/year@9.8~Suspension@Suspension Type - Front@Double-wishbone~Suspension@Shock Absorber Diameter - Rear (mm)@~Suspension@Stabilizer Bar Diameter - Rear (in)@~Tires@Rear Tire Order Code@~Tires@Front Tire Size@P245/35ZR20~Wheels@Front Wheel Material@Aluminum~Wheels@Spare Wheel Material@~Steering@Steering Type@Pwr~Brakes@Disc - Front (Yes or   )@Yes~Interior Dimensions@Second Leg Room (in)@~Exterior Dimensions@Length, Overall (in)@192.2~Exterior Dimensions@Height, Overall (in)@53.3~Vehicle@EPA Classification@Subcompact cars~Interior Dimensions@Passenger Volume (ft³)@~Trailering@Dead Weight Hitch - Max Tongue Wt. (lbs)@~Engine@Fuel System@MPFI~Transmission@Third Gear Ratio (:1)@~Transmission@Sixth Gear Ratio (:1)@~Transmission@Clutch Size (in)@~Cooling System@Total Cooling System Capacity (qts)@~Suspension@Suspension Type - Front (Cont.)@Skyhook~Suspension@Shock Absorber Diameter - Front (mm)@~Steering@Steering Ratio (:1), Overall@~Brakes@Brake Type@Pwr~Brakes@Front Brake Rotor Diam x Thickness (in)@13 x ~Brakes@Drum - Rear (Yes or   )@~Interior Dimensions@Front Leg Room (in)@~Interior Dimensions@Second Hip Room (in)@~Transmission@Seventh Gear Ratio (:1)@""
          _71=""Exterior@Tire sealing compound &amp; electric air compressor~Mechanical@4.7L V8 engine~Interior@Pwr adjustable heated front seats w/high lateral support, memory~Interior@Steering column w/easy entry/exit feature~Interior@Center console -inc: mounted clock w/chrome-plated trim~Interior@Smoked quartz interior highlights w/rosewood trim, fabric headliner~Mechanical@13&quot; cross-drilled brake discs w/6-piston calipers~Interior@Alarm system~Interior@Auxiliary pwr outlets~Exterior@LED taillights~Safety@Rear park sensors~Exterior@Foglamps~Exterior@Rain sensor~Interior@Remote key-fob -inc: on/off alarm system, lock/unlock doors, trunklid~Interior@Multimedia system -inc: 7&quot; screen, navigation w/30GB hard drive, CD player, voice control, MP3 player input, RDS tuner, Sirius satellite radio, iPod interface *Sirius satellite radio subscription required*~Interior@Pwr open-assist doors &amp; trunk~Safety@Front parking sensors~Exterior@Convertible pwr soft top~Safety@Tire pressure monitoring system~Mechanical@Red colored calipers~Interior@Folding front armrests w/illuminated storage compartment~Exterior@20&quot; birdcage design silver alloy wheels~Safety@Traction control~Mechanical@Chrome plated stainless steel twin dual exhaust tips~Interior@Poltrona Frau leather for central zones~Exterior@Dual auto-dimming pwr heated mirrors~Interior@Bluetooth~Interior@HomeLink garage door opener~Interior@Cupholders -inc: (2) front/(2) rear~Safety@Anti-lock braking system (ABS) w/electronic brake distribution (EBD)~Interior@Dual-zone automatic climate control w/rear air outlets~Exterior@Front grille w/chrome vertical bars &amp; side ornamental grilles~Mechanical@Sport mode -inc: gear, accelerator, MSP system~Interior@Auto-dimming rearview mirror~Interior@Leather seat trim~Exterior@Automatic bi-xenon headlights -inc: washers, adaptive light control system~Safety@6 airbags -inc: front driver &amp; passenger 2-stage airbags, front/rear side &amp; window airbags~Interior@3-spoke black leather sports steering wheel~Mechanical@Rear wheel drive~Interior@Bose (11) speaker surround-sound system~Interior@Pwr parking brake~Interior@Pwr windows~Mechanical@Dual wishbone Skyhook front/rear suspension~Mechanical@Pwr speed-sensitive steering~Safety@Roll bar system~Safety@Stability control~Exterior@Rain sensing windshield wipers~Interior@Tone on tone stitching~Interior@Folding rear armrests~Interior@Rear window defogger~Safety@Front/rear seatbelts w/pretensioners~Mechanical@6-speed automatic transmission w/paddle shifters""
          _72=""""
          _73=""Exterior@Tire sealing compound &amp; electric air compressor~Exterior@LED taillights~Exterior@Foglamps~Exterior@Rain sensor~Exterior@Convertible pwr soft top~Exterior@20&quot; birdcage design silver alloy wheels~Exterior@Dual auto-dimming pwr heated mirrors~Exterior@Front grille w/chrome vertical bars &amp; side ornamental grilles~Exterior@Automatic bi-xenon headlights -inc: washers, adaptive light control system~Exterior@Rain sensing windshield wipers""
          _74=""Interior@Pwr adjustable heated front seats w/high lateral support, memory~Interior@Steering column w/easy entry/exit feature~Interior@Center console -inc: mounted clock w/chrome-plated trim~Interior@Smoked quartz interior highlights w/rosewood trim, fabric headliner~Interior@Alarm system~Interior@Auxiliary pwr outlets~Interior@Remote key-fob -inc: on/off alarm system, lock/unlock doors, trunklid~Interior@Multimedia system -inc: 7&quot; screen, navigation w/30GB hard drive, CD player, voice control, MP3 player input, RDS tuner, Sirius satellite radio, iPod interface *Sirius satellite radio subscription required*~Interior@Pwr open-assist doors &amp; trunk~Interior@Folding front armrests w/illuminated storage compartment~Interior@Poltrona Frau leather for central zones~Interior@Bluetooth~Interior@HomeLink garage door opener~Interior@Cupholders -inc: (2) front/(2) rear~Interior@Dual-zone automatic climate control w/rear air outlets~Interior@Auto-dimming rearview mirror~Interior@Leather seat trim~Interior@3-spoke black leather sports steering wheel~Interior@Bose (11) speaker surround-sound system~Interior@Pwr parking brake~Interior@Pwr windows~Interior@Tone on tone stitching~Interior@Folding rear armrests~Interior@Rear window defogger""
          _75=""Mechanical@4.7L V8 engine~Mechanical@13&quot; cross-drilled brake discs w/6-piston calipers~Mechanical@Red colored calipers~Mechanical@Chrome plated stainless steel twin dual exhaust tips~Mechanical@Sport mode -inc: gear, accelerator, MSP system~Mechanical@Rear wheel drive~Mechanical@Dual wishbone Skyhook front/rear suspension~Mechanical@Pwr speed-sensitive steering~Mechanical@6-speed automatic transmission w/paddle shifters""
          _76=""Safety@Rear park sensors~Safety@Front parking sensors~Safety@Tire pressure monitoring system~Safety@Traction control~Safety@Anti-lock braking system (ABS) w/electronic brake distribution (EBD)~Safety@6 airbags -inc: front driver &amp; passenger 2-stage airbags, front/rear side &amp; window airbags~Safety@Roll bar system~Safety@Stability control~Safety@Front/rear seatbelts w/pretensioners""
          _77=""""
          _78=""""
          _79=""""
          _80=""""
          _81=""""
          _82=""""
          _83=""http://media.flickfusion.net/videos/hn/player.php?vehicle_fkey=D82E29C6-2F90-168B-07DD-FD1FA7BC5111""
          _84=""http://4044ca6f7c9ee69346d6-830a4b80a90acec90a167a307f9c546c.r72.cf1.rackcdn.com/7A57F5A0-2F28-F8F3-93F3-7EC19459A175.mp4""
          _85=""16027.jpg""
          _86=""https://content.homenetiol.com/stock_images/2/16027.jpg""
          _87=""""
          _88=""""
          _89=""""
          _90=""""
          _91=""""
          _92=""Maserati""
          _93=""GranTurismo Convertible""
          _94=""Convertible""
          _95=""""
          _96=""2013""
          _97=""2dr Conv GranTurismo""
          _98=""726""
          _99=""0""
          _100=""JoeSimcoxTestZone temp test""
          _101=""102 Xtreme Lane""
          _102=""West Chester""
          _103=""PA""
          _104=""19460""
          _105=""""
          _106=""(610) 555-5555""
          _107=""""
          _108=""""
          _109=""2013 Maserati GranTurismo Convertible 2dr Conv GranTurismo V8 4.7L""
          _110=""False""
          _111=""http://www.carfax.com/VehicleHistory/p/Report.cfx?partner=HNR_0&amp;vin=ZAM45MMA4D0069448""
          _112=""""
          _113=""""
          _114=""""
          _115=""""
          _116=""""
          _117=""""
          _118=""""
          _119=""""
          _120=""""
          _121=""""
          _122=""""
          _123=""""
          _124="""" /> </VS>

    </R>

  </RS>

</F>";

       
    }

}