﻿using System;
using System.Collections.Generic;
using CJI.AgileDealer.Web.WebForm.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CJI.AgileDealer.UnitTest
{
    [TestClass]
    public class BlogTest
    {
        [TestMethod]
        public void TestGetBlogEntryByTags()
        {
            List<int> tagIds = new List<int>() { 1,2 };
            var result = VehicleHelper.GetBlogEntryIdsByTag(tagIds,1,1);
            Assert.IsNotNull(result);
        }
    }
}
